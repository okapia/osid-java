//
// AbstractAssemblyAssessmentQuery.java
//
//     An AssessmentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssessmentQuery that stores terms.
 */

public abstract class AbstractAssemblyAssessmentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.assessment.AssessmentQuery,
               org.osid.assessment.AssessmentQueryInspector,
               org.osid.assessment.AssessmentSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.AssessmentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAssessmentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAssessmentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the level grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getLevelIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears all level <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        getAssembler().clearTerms(getLevelIdColumn());
        return;
    }


    /**
     *  Gets the level <code> Id </code> query terms. 
     *
     *  @return the level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (getAssembler().getIdTerms(getLevelIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the level of 
     *  difficulty. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLevelColumn(), style);
        return;
    }


    /**
     *  Gets the LevelId column name.
     *
     * @return the column name
     */

    protected String getLevelIdColumn() {
        return ("level_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches an assessment that has any level assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any level, 
     *          <code> false </code> to match assessments with no level 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        getAssembler().addIdWildcardTerm(getLevelColumn(), match);
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        getAssembler().clearTerms(getLevelColumn());
        return;
    }


    /**
     *  Gets the level query terms. 
     *
     *  @return the level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }


    /**
     *  Gets the Level column name.
     *
     * @return the column name
     */

    protected String getLevelColumn() {
        return ("level");
    }


    /**
     *  Sets the rubric assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRubricId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getRubricIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears all rubric assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRubricIdTerms() {
        getAssembler().clearTerms(getRubricIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRubricIdTerms() {
        return (getAssembler().getIdTerms(getRubricIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the rubric 
     *  assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRubric(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRubricColumn(), style);
        return;
    }


    /**
     *  Gets the RubricId column name.
     *
     * @return the column name
     */

    protected String getRubricIdColumn() {
        return ("rubric_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if a rubric assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rubric assessment. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> supportsRubricQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getRubricQuery() {
        throw new org.osid.UnimplementedException("supportsRubricQuery() is false");
    }


    /**
     *  Matches an assessment that has any rubric assessment assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any 
     *          rubric, <code> false </code> to match assessments with no 
     *          rubric 
     */

    @OSID @Override
    public void matchAnyRubric(boolean match) {
        getAssembler().addIdWildcardTerm(getRubricColumn(), match);
        return;
    }


    /**
     *  Clears all rubric assessment terms. 
     */

    @OSID @Override
    public void clearRubricTerms() {
        getAssembler().clearTerms(getRubricColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getRubricTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment search order. 
     *
     *  @return a rubric assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRubricSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getRubricSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRubricSearchOrder() is false");
    }


    /**
     *  Gets the Rubric column name.
     *
     * @return the column name
     */

    protected String getRubricColumn() {
        return ("rubric");
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears all item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches an assessment that has any item. 
     *
     *  @param  match <code> true </code> to match assessments with any item, 
     *          <code> false </code> to match assessments with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears all item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.assessment.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.assessment.ItemQueryInspector[0]);
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment offered <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentOfferedId(org.osid.id.Id assessmentOfferedId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAssessmentOfferedIdColumn(), assessmentOfferedId, match);
        return;
    }


    /**
     *  Clears all assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedIdTerms() {
        getAssembler().clearTerms(getAssessmentOfferedIdColumn());
        return;
    }


    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentOfferedIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentOfferedIdColumn()));
    }


    /**
     *  Gets the AssessmentOfferedId column name.
     *
     * @return the column name
     */

    protected String getAssessmentOfferedIdColumn() {
        return ("assessment_offered_id");
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment offered. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getAssessmentOfferedQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedQuery() is false");
    }


    /**
     *  Matches an assessment that has any offering. 
     *
     *  @param  match <code> true </code> to match assessments with any 
     *          offering, <code> false </code> to match assessments with no 
     *          offerings 
     */

    @OSID @Override
    public void matchAnyAssessmentOffered(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentOfferedColumn(), match);
        return;
    }


    /**
     *  Clears all assessment offered terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedTerms() {
        getAssembler().clearTerms(getAssessmentOfferedColumn());
        return;
    }


    /**
     *  Gets the assessment offered query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getAssessmentOfferedTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Gets the AssessmentOffered column name.
     *
     * @return the column name
     */

    protected String getAssessmentOfferedColumn() {
        return ("assessment_offered");
    }


    /**
     *  Sets the assessment taken <code> Id </code> for this query. 
     *
     *  @param  assessmentTakenId an assessment taken <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentTakenId(org.osid.id.Id assessmentTakenId, 
                                       boolean match) {
        getAssembler().addIdTerm(getAssessmentTakenIdColumn(), assessmentTakenId, match);
        return;
    }


    /**
     *  Clears all assessment taken <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenIdTerms() {
        getAssembler().clearTerms(getAssessmentTakenIdColumn());
        return;
    }


    /**
     *  Gets the assessment taken <code> Id </code> query terms. 
     *
     *  @return the assessment taken <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentTakenIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentTakenIdColumn()));
    }


    /**
     *  Gets the AssessmentTakenId column name.
     *
     * @return the column name
     */

    protected String getAssessmentTakenIdColumn() {
        return ("assessment_taken_id");
    }


    /**
     *  Tests if an <code> AssessmentTakenQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment taken query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment taken. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment taken query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuery getAssessmentTakenQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentTakenQuery() is false");
    }


    /**
     *  Matches an assessment that has any taken version. 
     *
     *  @param  match <code> true </code> to match assessments with any taken 
     *          assessments, <code> false </code> to match assessments with no 
     *          taken assessments 
     */

    @OSID @Override
    public void matchAnyAssessmentTaken(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentTakenColumn(), match);
        return;
    }


    /**
     *  Clears all assessment taken terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenTerms() {
        getAssembler().clearTerms(getAssessmentTakenColumn());
        return;
    }


    /**
     *  Gets the assessment taken query terms. 
     *
     *  @return the assessment taken terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQueryInspector[] getAssessmentTakenTerms() {
        return (new org.osid.assessment.AssessmentTakenQueryInspector[0]);
    }


    /**
     *  Gets the AssessmentTaken column name.
     *
     * @return the column name
     */

    protected String getAssessmentTakenColumn() {
        return ("assessment_taken");
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        getAssembler().clearTerms(getBankIdColumn());
        return;
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (getAssembler().getIdTerms(getBankIdColumn()));
    }


    /**
     *  Gets the BankId column name.
     *
     * @return the column name
     */

    protected String getBankIdColumn() {
        return ("bank_id");
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        getAssembler().clearTerms(getBankColumn());
        return;
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the Bank column name.
     *
     * @return the column name
     */

    protected String getBankColumn() {
        return ("bank");
    }


    /**
     *  Tests if this assessment supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentRecordType an assessment record type 
     *  @return <code>true</code> if the assessmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentRecordType) {
        for (org.osid.assessment.records.AssessmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  assessmentRecordType the assessment record type 
     *  @return the assessment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentQueryRecord getAssessmentQueryRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  assessmentRecordType the assessment record type 
     *  @return the assessment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentQueryInspectorRecord getAssessmentQueryInspectorRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param assessmentRecordType the assessment record type
     *  @return the assessment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentSearchOrderRecord getAssessmentSearchOrderRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this assessment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentQueryRecord the assessment query record
     *  @param assessmentQueryInspectorRecord the assessment query inspector
     *         record
     *  @param assessmentSearchOrderRecord the assessment search order record
     *  @param assessmentRecordType assessment record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentQueryRecord</code>,
     *          <code>assessmentQueryInspectorRecord</code>,
     *          <code>assessmentSearchOrderRecord</code> or
     *          <code>assessmentRecordTypeassessment</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentRecords(org.osid.assessment.records.AssessmentQueryRecord assessmentQueryRecord, 
                                      org.osid.assessment.records.AssessmentQueryInspectorRecord assessmentQueryInspectorRecord, 
                                      org.osid.assessment.records.AssessmentSearchOrderRecord assessmentSearchOrderRecord, 
                                      org.osid.type.Type assessmentRecordType) {

        addRecordType(assessmentRecordType);

        nullarg(assessmentQueryRecord, "assessment query record");
        nullarg(assessmentQueryInspectorRecord, "assessment query inspector record");
        nullarg(assessmentSearchOrderRecord, "assessment search odrer record");

        this.queryRecords.add(assessmentQueryRecord);
        this.queryInspectorRecords.add(assessmentQueryInspectorRecord);
        this.searchOrderRecords.add(assessmentSearchOrderRecord);
        
        return;
    }
}
