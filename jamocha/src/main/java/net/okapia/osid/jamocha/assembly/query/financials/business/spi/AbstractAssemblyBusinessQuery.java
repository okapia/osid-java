//
// AbstractAssemblyBusinessQuery.java
//
//     A BusinessQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.business.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BusinessQuery that stores terms.
 */

public abstract class AbstractAssemblyBusinessQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.financials.BusinessQuery,
               org.osid.financials.BusinessQueryInspector,
               org.osid.financials.BusinessSearchOrder {

    private final java.util.Collection<org.osid.financials.records.BusinessQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.BusinessQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.BusinessSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBusinessQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBusinessQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the account <code> Id </code> for this query. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        getAssembler().addIdTerm(getAccountIdColumn(), accountId, match);
        return;
    }


    /**
     *  Clears the account <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        getAssembler().clearTerms(getAccountIdColumn());
        return;
    }


    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (getAssembler().getIdTerms(getAccountIdColumn()));
    }


    /**
     *  Gets the AccountId column name.
     *
     * @return the column name
     */

    protected String getAccountIdColumn() {
        return ("account_id");
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Matches businesses that have any account. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          account, <code> false </code> to match businesses with no 
     *          accounts 
     */

    @OSID @Override
    public void matchAnyAccount(boolean match) {
        getAssembler().addIdWildcardTerm(getAccountColumn(), match);
        return;
    }


    /**
     *  Clears the account query terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        getAssembler().clearTerms(getAccountColumn());
        return;
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account query terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the Account column name.
     *
     * @return the column name
     */

    protected String getAccountColumn() {
        return ("account");
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches businesses that have any activity. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          activity, <code> false </code> to match businesses with no 
     *          activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity query terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        getAssembler().addIdTerm(getFiscalPeriodIdColumn(), fiscalPeriodId, match);
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        getAssembler().clearTerms(getFiscalPeriodIdColumn());
        return;
    }


    /**
     *  Gets the fiscal period <code> Id </code> query terms. 
     *
     *  @return the fiscal period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFiscalPeriodIdTerms() {
        return (getAssembler().getIdTerms(getFiscalPeriodIdColumn()));
    }


    /**
     *  Gets the FiscalPeriodId column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodIdColumn() {
        return ("fiscal_period_id");
    }


    /**
     *  Tests if a <code> FiscalPeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Matches businesses that have any fiscal period. 
     *
     *  @param  match <code> true </code> to match businesses with any fiscal 
     *          period, <code> false </code> to match businesses with no 
     *          fiscal period 
     */

    @OSID @Override
    public void matchAnyFiscalPeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getFiscalPeriodColumn(), match);
        return;
    }


    /**
     *  Clears the fiscal period query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        getAssembler().clearTerms(getFiscalPeriodColumn());
        return;
    }


    /**
     *  Gets the fiscal period query terms. 
     *
     *  @return the fiscal period query terms 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQueryInspector[] getFiscalPeriodTerms() {
        return (new org.osid.financials.FiscalPeriodQueryInspector[0]);
    }


    /**
     *  Gets the FiscalPeriod column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodColumn() {
        return ("fiscal_period");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an ancestor. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorBusinessId(org.osid.id.Id businessId, 
                                        boolean match) {
        getAssembler().addIdTerm(getAncestorBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the ancestor business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessIdTerms() {
        getAssembler().clearTerms(getAncestorBusinessIdColumn());
        return;
    }


    /**
     *  Gets the ancestor business <code> Id </code> query terms. 
     *
     *  @return the ancestor business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBusinessIdTerms() {
        return (getAssembler().getIdTerms(getAncestorBusinessIdColumn()));
    }


    /**
     *  Gets the AncestorBusinessId column name.
     *
     * @return the column name
     */

    protected String getAncestorBusinessIdColumn() {
        return ("ancestor_business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getAncestorBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBusinessQuery() is false");
    }


    /**
     *  Matches businesses with any business ancestor. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          ancestor, <code> false </code> to match root businesses 
     */

    @OSID @Override
    public void matchAnyAncestorBusiness(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorBusinessColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor business query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessTerms() {
        getAssembler().clearTerms(getAncestorBusinessColumn());
        return;
    }


    /**
     *  Gets the ancestor business query terms. 
     *
     *  @return the ancestor business terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getAncestorBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the AncestorBusiness column name.
     *
     * @return the column name
     */

    protected String getAncestorBusinessColumn() {
        return ("ancestor_business");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an descendant. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantBusinessId(org.osid.id.Id businessId, 
                                          boolean match) {
        getAssembler().addIdTerm(getDescendantBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the descendant business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessIdTerms() {
        getAssembler().clearTerms(getDescendantBusinessIdColumn());
        return;
    }


    /**
     *  Gets the descendant business <code> Id </code> query terms. 
     *
     *  @return the descendant business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBusinessIdTerms() {
        return (getAssembler().getIdTerms(getDescendantBusinessIdColumn()));
    }


    /**
     *  Gets the DescendantBusinessId column name.
     *
     * @return the column name
     */

    protected String getDescendantBusinessIdColumn() {
        return ("descendant_business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getDescendantBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBusinessQuery() is false");
    }


    /**
     *  Matches agencies with any descendant business. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          descendant, <code> false </code> to match leaf businesses 
     */

    @OSID @Override
    public void matchAnyDescendantBusiness(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantBusinessColumn(), match);
        return;
    }


    /**
     *  Clears the descendant business query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessTerms() {
        getAssembler().clearTerms(getDescendantBusinessColumn());
        return;
    }


    /**
     *  Gets the descendant business query terms. 
     *
     *  @return the descendant business terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getDescendantBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the DescendantBusiness column name.
     *
     * @return the column name
     */

    protected String getDescendantBusinessColumn() {
        return ("descendant_business");
    }


    /**
     *  Tests if this business supports the given record
     *  <code>Type</code>.
     *
     *  @param  businessRecordType a business record type 
     *  @return <code>true</code> if the businessRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type businessRecordType) {
        for (org.osid.financials.records.BusinessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  businessRecordType the business record type 
     *  @return the business query record 
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.BusinessQueryRecord getBusinessQueryRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.BusinessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  businessRecordType the business record type 
     *  @return the business query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.BusinessQueryInspectorRecord getBusinessQueryInspectorRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.BusinessQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param businessRecordType the business record type
     *  @return the business search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.BusinessSearchOrderRecord getBusinessSearchOrderRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.BusinessSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this business. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param businessQueryRecord the business query record
     *  @param businessQueryInspectorRecord the business query inspector
     *         record
     *  @param businessSearchOrderRecord the business search order record
     *  @param businessRecordType business record type
     *  @throws org.osid.NullArgumentException
     *          <code>businessQueryRecord</code>,
     *          <code>businessQueryInspectorRecord</code>,
     *          <code>businessSearchOrderRecord</code> or
     *          <code>businessRecordTypebusiness</code> is
     *          <code>null</code>
     */
            
    protected void addBusinessRecords(org.osid.financials.records.BusinessQueryRecord businessQueryRecord, 
                                      org.osid.financials.records.BusinessQueryInspectorRecord businessQueryInspectorRecord, 
                                      org.osid.financials.records.BusinessSearchOrderRecord businessSearchOrderRecord, 
                                      org.osid.type.Type businessRecordType) {

        addRecordType(businessRecordType);

        nullarg(businessQueryRecord, "business query record");
        nullarg(businessQueryInspectorRecord, "business query inspector record");
        nullarg(businessSearchOrderRecord, "business search odrer record");

        this.queryRecords.add(businessQueryRecord);
        this.queryInspectorRecords.add(businessQueryInspectorRecord);
        this.searchOrderRecords.add(businessSearchOrderRecord);
        
        return;
    }
}
