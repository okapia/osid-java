//
// AbstractSystemQuery.java
//
//     A template for making a System Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.system.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for systems.
 */

public abstract class AbstractSystemQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.control.SystemQuery {

    private final java.util.Collection<org.osid.control.records.SystemQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the device <code> Id </code> for this query to match systems that 
     *  have a related device. 
     *
     *  @param  deviceId a device <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> deviceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDeviceId(org.osid.id.Id deviceId, boolean match) {
        return;
    }


    /**
     *  Clears the device <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDeviceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DeviceQuery </code> is available. 
     *
     *  @return <code> true </code> if a device query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a device. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the device query 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuery getDeviceQuery() {
        throw new org.osid.UnimplementedException("supportsDeviceQuery() is false");
    }


    /**
     *  Matches systems that have any device. 
     *
     *  @param  match <code> true </code> to match systems with any device, 
     *          <code> false </code> to match systems with no device 
     */

    @OSID @Override
    public void matchAnyDevice(boolean match) {
        return;
    }


    /**
     *  Clears the device query terms. 
     */

    @OSID @Override
    public void clearDeviceTerms() {
        return;
    }


    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId the controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a controller. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Matches systems with any controller. 
     *
     *  @param  match <code> true </code> to match systems with any 
     *          controller, <code> false </code> to match systems with no 
     *          controller 
     */

    @OSID @Override
    public void matchAnyController(boolean match) {
        return;
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        return;
    }


    /**
     *  Sets the input <code> Id </code> for this query. 
     *
     *  @param  inputId the input <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inputId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInputId(org.osid.id.Id inputId, boolean match) {
        return;
    }


    /**
     *  Clears the input <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInputIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InputQuery </code> is available. 
     *
     *  @return <code> true </code> if an input query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputQuery() {
        return (false);
    }


    /**
     *  Gets the query for an input. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the input query 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuery getInputQuery() {
        throw new org.osid.UnimplementedException("supportsInputQuery() is false");
    }


    /**
     *  Matches systems with any input. 
     *
     *  @param  match <code> true </code> to match systems with any input, 
     *          <code> false </code> to match systems with no inputs 
     */

    @OSID @Override
    public void matchAnyInput(boolean match) {
        return;
    }


    /**
     *  Clears the input query terms. 
     */

    @OSID @Override
    public void clearInputTerms() {
        return;
    }


    /**
     *  Sets the setting <code> Id </code> for this query. 
     *
     *  @param  settingId the setting <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> settingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettingId(org.osid.id.Id settingId, boolean match) {
        return;
    }


    /**
     *  Clears the setting <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSettingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SettingQuery </code> is available. 
     *
     *  @return <code> true </code> if a setting query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a setting. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the setting query 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuery getSettingQuery() {
        throw new org.osid.UnimplementedException("supportsSettingQuery() is false");
    }


    /**
     *  Matches systems with any setting. 
     *
     *  @param  match <code> true </code> to match systems with any setting, 
     *          <code> false </code> to match systems with no settings 
     */

    @OSID @Override
    public void matchAnySetting(boolean match) {
        return;
    }


    /**
     *  Clears the setting query terms. 
     */

    @OSID @Override
    public void clearSettingTerms() {
        return;
    }


    /**
     *  Sets the scene <code> Id </code> for this query. 
     *
     *  @param  sceneId the scene <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sceneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSceneId(org.osid.id.Id sceneId, boolean match) {
        return;
    }


    /**
     *  Clears the scene <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSceneIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SceneQuery </code> is available. 
     *
     *  @return <code> true </code> if a scene query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a scene. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the scene query 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuery getSceneQuery() {
        throw new org.osid.UnimplementedException("supportsSceneQuery() is false");
    }


    /**
     *  Matches systems with any scene. 
     *
     *  @param  match <code> true </code> to match systems with any scene, 
     *          <code> false </code> to match systems with no scenes 
     */

    @OSID @Override
    public void matchAnyScene(boolean match) {
        return;
    }


    /**
     *  Clears the scene query terms. 
     */

    @OSID @Override
    public void clearSceneTerms() {
        return;
    }


    /**
     *  Sets the trigger <code> Id </code> for this query. 
     *
     *  @param  triggerId the trigger <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> triggerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTriggerId(org.osid.id.Id triggerId, boolean match) {
        return;
    }


    /**
     *  Clears the trigger <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTriggerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TriggerQuery </code> is available. 
     *
     *  @return <code> true </code> if a trigger query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a trigger. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the trigger query 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuery getTriggerQuery() {
        throw new org.osid.UnimplementedException("supportsTriggerQuery() is false");
    }


    /**
     *  Matches systems with any trigger. 
     *
     *  @param  match <code> true </code> to match systems with any trigger, 
     *          <code> false </code> to match systems with no trigger 
     */

    @OSID @Override
    public void matchAnyTrigger(boolean match) {
        return;
    }


    /**
     *  Clears the trigger query terms. 
     */

    @OSID @Override
    public void clearTriggerTerms() {
        return;
    }


    /**
     *  Sets the action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId the action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionGroupId(org.osid.id.Id actionGroupId, boolean match) {
        return;
    }


    /**
     *  Clears the action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionGroupIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActionGroup </code> is available. 
     *
     *  @return <code> true </code> if an action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsActionGroupQuery() is false");
    }


    /**
     *  Matches systems with any action group. 
     *
     *  @param  match <code> true </code> to match systems with any action 
     *          group, <code> false </code> to match systems with no action 
     *          groups 
     */

    @OSID @Override
    public void matchAnyActionGroup(boolean match) {
        return;
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearActionGroupTerms() {
        return;
    }


    /**
     *  Sets the system <code> Id </code> for this query to match systems that 
     *  have the specified system as an ancestor. 
     *
     *  @param  systemId a system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getAncestorSystemQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorSystemQuery() is false");
    }


    /**
     *  Matches systems with any ancestor. 
     *
     *  @param  match <code> true </code> to match systems with any ancestor, 
     *          <code> false </code> to match root systems 
     */

    @OSID @Override
    public void matchAnyAncestorSystem(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor system query terms. 
     */

    @OSID @Override
    public void clearAncestorSystemTerms() {
        return;
    }


    /**
     *  Sets the system <code> Id </code> for this query to match systems that 
     *  have the specified system as a descendant. 
     *
     *  @param  systemId a system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantSystemQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getDescendantSystemQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantSystemQuery() is false");
    }


    /**
     *  Matches systems with any descendant. 
     *
     *  @param  match <code> true </code> to match systems with any 
     *          descendant, <code> false </code> to match leaf systems 
     */

    @OSID @Override
    public void matchAnyDescendantSystem(boolean match) {
        return;
    }


    /**
     *  Clears the descendant system query terms. 
     */

    @OSID @Override
    public void clearDescendantSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given system query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a system implementing the requested record.
     *
     *  @param systemRecordType a system record type
     *  @return the system query record
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(systemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SystemQueryRecord getSystemQueryRecord(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SystemQueryRecord record : this.records) {
            if (record.implementsRecordType(systemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(systemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this system query. 
     *
     *  @param systemQueryRecord system query record
     *  @param systemRecordType system record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSystemQueryRecord(org.osid.control.records.SystemQueryRecord systemQueryRecord, 
                                          org.osid.type.Type systemRecordType) {

        addRecordType(systemRecordType);
        nullarg(systemQueryRecord, "system query record");
        this.records.add(systemQueryRecord);        
        return;
    }
}
