//
// AbstractNodeMailboxHierarchySession.java
//
//     Defines a Mailbox hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a mailbox hierarchy session for delivering a hierarchy
 *  of mailboxes using the MailboxNode interface.
 */

public abstract class AbstractNodeMailboxHierarchySession
    extends net.okapia.osid.jamocha.messaging.spi.AbstractMailboxHierarchySession
    implements org.osid.messaging.MailboxHierarchySession {

    private java.util.Collection<org.osid.messaging.MailboxNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root mailbox <code> Ids </code> in this hierarchy.
     *
     *  @return the root mailbox <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootMailboxIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.messaging.mailboxnode.MailboxNodeToIdList(this.roots));
    }


    /**
     *  Gets the root mailboxes in the mailbox hierarchy. A node
     *  with no parents is an orphan. While all mailbox <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root mailboxes 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getRootMailboxes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.messaging.mailboxnode.MailboxNodeToMailboxList(new net.okapia.osid.jamocha.messaging.mailboxnode.ArrayMailboxNodeList(this.roots)));
    }


    /**
     *  Adds a root mailbox node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootMailbox(org.osid.messaging.MailboxNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root mailbox nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootMailboxes(java.util.Collection<org.osid.messaging.MailboxNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root mailbox node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootMailbox(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.messaging.MailboxNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Mailbox </code> has any parents. 
     *
     *  @param  mailboxId a mailbox <code> Id </code> 
     *  @return <code> true </code> if the mailbox has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentMailboxes(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getMailboxNode(mailboxId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  mailbox.
     *
     *  @param  id an <code> Id </code> 
     *  @param  mailboxId the <code> Id </code> of a mailbox 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> mailboxId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> mailboxId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfMailbox(org.osid.id.Id id, org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.messaging.MailboxNodeList parents = getMailboxNode(mailboxId).getParentMailboxNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextMailboxNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given mailbox. 
     *
     *  @param  mailboxId a mailbox <code> Id </code> 
     *  @return the parent <code> Ids </code> of the mailbox 
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentMailboxIds(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.messaging.mailbox.MailboxToIdList(getParentMailboxes(mailboxId)));
    }


    /**
     *  Gets the parents of the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> to query 
     *  @return the parents of the mailbox 
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getParentMailboxes(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.messaging.mailboxnode.MailboxNodeToMailboxList(getMailboxNode(mailboxId).getParentMailboxNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  mailbox.
     *
     *  @param  id an <code> Id </code> 
     *  @param  mailboxId the Id of a mailbox 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> mailboxId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfMailbox(org.osid.id.Id id, org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfMailbox(id, mailboxId)) {
            return (true);
        }

        try (org.osid.messaging.MailboxList parents = getParentMailboxes(mailboxId)) {
            while (parents.hasNext()) {
                if (isAncestorOfMailbox(id, parents.getNextMailbox().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a mailbox has any children. 
     *
     *  @param  mailboxId a mailbox <code> Id </code> 
     *  @return <code> true </code> if the <code> mailboxId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildMailboxes(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getMailboxNode(mailboxId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  mailbox.
     *
     *  @param  id an <code> Id </code> 
     *  @param mailboxId the <code> Id </code> of a 
     *         mailbox
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> mailboxId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> mailboxId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfMailbox(org.osid.id.Id id, org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfMailbox(mailboxId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  mailbox.
     *
     *  @param  mailboxId the <code> Id </code> to query 
     *  @return the children of the mailbox 
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildMailboxIds(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.messaging.mailbox.MailboxToIdList(getChildMailboxes(mailboxId)));
    }


    /**
     *  Gets the children of the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> to query 
     *  @return the children of the mailbox 
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getChildMailboxes(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.messaging.mailboxnode.MailboxNodeToMailboxList(getMailboxNode(mailboxId).getChildMailboxNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  mailbox.
     *
     *  @param  id an <code> Id </code> 
     *  @param mailboxId the <code> Id </code> of a 
     *         mailbox
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> mailboxId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfMailbox(org.osid.id.Id id, org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfMailbox(mailboxId, id)) {
            return (true);
        }

        try (org.osid.messaging.MailboxList children = getChildMailboxes(mailboxId)) {
            while (children.hasNext()) {
                if (isDescendantOfMailbox(id, children.getNextMailbox().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  mailbox.
     *
     *  @param  mailboxId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified mailbox node 
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getMailboxNodeIds(org.osid.id.Id mailboxId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.messaging.mailboxnode.MailboxNodeToNode(getMailboxNode(mailboxId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given mailbox.
     *
     *  @param  mailboxId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified mailbox node 
     *  @throws org.osid.NotFoundException <code> mailboxId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> mailboxId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxNode getMailboxNodes(org.osid.id.Id mailboxId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getMailboxNode(mailboxId));
    }


    /**
     *  Closes this <code>MailboxHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a mailbox node.
     *
     *  @param mailboxId the id of the mailbox node
     *  @throws org.osid.NotFoundException <code>mailboxId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>mailboxId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.messaging.MailboxNode getMailboxNode(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(mailboxId, "mailbox Id");
        for (org.osid.messaging.MailboxNode mailbox : this.roots) {
            if (mailbox.getId().equals(mailboxId)) {
                return (mailbox);
            }

            org.osid.messaging.MailboxNode r = findMailbox(mailbox, mailboxId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(mailboxId + " is not found");
    }


    protected org.osid.messaging.MailboxNode findMailbox(org.osid.messaging.MailboxNode node, 
                                                         org.osid.id.Id mailboxId) 
	throws org.osid.OperationFailedException {

        try (org.osid.messaging.MailboxNodeList children = node.getChildMailboxNodes()) {
            while (children.hasNext()) {
                org.osid.messaging.MailboxNode mailbox = children.getNextMailboxNode();
                if (mailbox.getId().equals(mailboxId)) {
                    return (mailbox);
                }
                
                mailbox = findMailbox(mailbox, mailboxId);
                if (mailbox != null) {
                    return (mailbox);
                }
            }
        }

        return (null);
    }
}
