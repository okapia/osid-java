//
// AbstractQueueConstrainerEnablerSearch.java
//
//     A template for making a QueueConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.queueconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing queue constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractQueueConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.tracking.rules.QueueConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.tracking.rules.QueueConstrainerEnablerSearchOrder queueConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of queue constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  queueConstrainerEnablerIds list of queue constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongQueueConstrainerEnablers(org.osid.id.IdList queueConstrainerEnablerIds) {
        while (queueConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(queueConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongQueueConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of queue constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getQueueConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  queueConstrainerEnablerSearchOrder queue constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>queueConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderQueueConstrainerEnablerResults(org.osid.tracking.rules.QueueConstrainerEnablerSearchOrder queueConstrainerEnablerSearchOrder) {
	this.queueConstrainerEnablerSearchOrder = queueConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.tracking.rules.QueueConstrainerEnablerSearchOrder getQueueConstrainerEnablerSearchOrder() {
	return (this.queueConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given queue constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue constrainer enabler implementing the requested record.
     *
     *  @param queueConstrainerEnablerSearchRecordType a queue constrainer enabler search record
     *         type
     *  @return the queue constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerEnablerSearchRecord getQueueConstrainerEnablerSearchRecord(org.osid.type.Type queueConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.tracking.rules.records.QueueConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue constrainer enabler search. 
     *
     *  @param queueConstrainerEnablerSearchRecord queue constrainer enabler search record
     *  @param queueConstrainerEnablerSearchRecordType queueConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueConstrainerEnablerSearchRecord(org.osid.tracking.rules.records.QueueConstrainerEnablerSearchRecord queueConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type queueConstrainerEnablerSearchRecordType) {

        addRecordType(queueConstrainerEnablerSearchRecordType);
        this.records.add(queueConstrainerEnablerSearchRecord);        
        return;
    }
}
