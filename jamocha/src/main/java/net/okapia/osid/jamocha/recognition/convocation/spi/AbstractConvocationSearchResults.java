//
// AbstractConvocationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractConvocationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recognition.ConvocationSearchResults {

    private org.osid.recognition.ConvocationList convocations;
    private final org.osid.recognition.ConvocationQueryInspector inspector;
    private final java.util.Collection<org.osid.recognition.records.ConvocationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractConvocationSearchResults.
     *
     *  @param convocations the result set
     *  @param convocationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>convocations</code>
     *          or <code>convocationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractConvocationSearchResults(org.osid.recognition.ConvocationList convocations,
                                            org.osid.recognition.ConvocationQueryInspector convocationQueryInspector) {
        nullarg(convocations, "convocations");
        nullarg(convocationQueryInspector, "convocation query inspectpr");

        this.convocations = convocations;
        this.inspector = convocationQueryInspector;

        return;
    }


    /**
     *  Gets the convocation list resulting from a search.
     *
     *  @return a convocation list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocations() {
        if (this.convocations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recognition.ConvocationList convocations = this.convocations;
        this.convocations = null;
	return (convocations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recognition.ConvocationQueryInspector getConvocationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  convocation search record <code> Type. </code> This method must
     *  be used to retrieve a convocation implementing the requested
     *  record.
     *
     *  @param convocationSearchRecordType a convocation search 
     *         record type 
     *  @return the convocation search
     *  @throws org.osid.NullArgumentException
     *          <code>convocationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(convocationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationSearchResultsRecord getConvocationSearchResultsRecord(org.osid.type.Type convocationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recognition.records.ConvocationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(convocationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(convocationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record convocation search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addConvocationRecord(org.osid.recognition.records.ConvocationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "convocation record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
