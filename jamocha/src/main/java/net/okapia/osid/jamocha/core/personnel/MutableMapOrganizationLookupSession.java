//
// MutableMapOrganizationLookupSession
//
//    Implements an Organization lookup service backed by a collection of
//    organizations that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements an Organization lookup service backed by a collection of
 *  organizations. The organizations are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of organizations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapOrganizationLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapOrganizationLookupSession
    implements org.osid.personnel.OrganizationLookupSession {


    /**
     *  Constructs a new {@code MutableMapOrganizationLookupSession}
     *  with no organizations.
     *
     *  @param realm the realm
     *  @throws org.osid.NullArgumentException {@code realm} is
     *          {@code null}
     */

      public MutableMapOrganizationLookupSession(org.osid.personnel.Realm realm) {
        setRealm(realm);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOrganizationLookupSession} with a
     *  single organization.
     *
     *  @param realm the realm  
     *  @param organization an organization
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code organization} is {@code null}
     */

    public MutableMapOrganizationLookupSession(org.osid.personnel.Realm realm,
                                           org.osid.personnel.Organization organization) {
        this(realm);
        putOrganization(organization);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOrganizationLookupSession}
     *  using an array of organizations.
     *
     *  @param realm the realm
     *  @param organizations an array of organizations
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code organizations} is {@code null}
     */

    public MutableMapOrganizationLookupSession(org.osid.personnel.Realm realm,
                                           org.osid.personnel.Organization[] organizations) {
        this(realm);
        putOrganizations(organizations);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOrganizationLookupSession}
     *  using a collection of organizations.
     *
     *  @param realm the realm
     *  @param organizations a collection of organizations
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code organizations} is {@code null}
     */

    public MutableMapOrganizationLookupSession(org.osid.personnel.Realm realm,
                                           java.util.Collection<? extends org.osid.personnel.Organization> organizations) {

        this(realm);
        putOrganizations(organizations);
        return;
    }

    
    /**
     *  Makes an {@code Organization} available in this session.
     *
     *  @param organization an organization
     *  @throws org.osid.NullArgumentException {@code organization{@code  is
     *          {@code null}
     */

    @Override
    public void putOrganization(org.osid.personnel.Organization organization) {
        super.putOrganization(organization);
        return;
    }


    /**
     *  Makes an array of organizations available in this session.
     *
     *  @param organizations an array of organizations
     *  @throws org.osid.NullArgumentException {@code organizations{@code 
     *          is {@code null}
     */

    @Override
    public void putOrganizations(org.osid.personnel.Organization[] organizations) {
        super.putOrganizations(organizations);
        return;
    }


    /**
     *  Makes collection of organizations available in this session.
     *
     *  @param organizations a collection of organizations
     *  @throws org.osid.NullArgumentException {@code organizations{@code  is
     *          {@code null}
     */

    @Override
    public void putOrganizations(java.util.Collection<? extends org.osid.personnel.Organization> organizations) {
        super.putOrganizations(organizations);
        return;
    }


    /**
     *  Removes an Organization from this session.
     *
     *  @param organizationId the {@code Id} of the organization
     *  @throws org.osid.NullArgumentException {@code organizationId{@code 
     *          is {@code null}
     */

    @Override
    public void removeOrganization(org.osid.id.Id organizationId) {
        super.removeOrganization(organizationId);
        return;
    }    
}
