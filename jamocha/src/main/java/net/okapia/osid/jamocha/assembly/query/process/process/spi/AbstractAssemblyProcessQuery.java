//
// AbstractAssemblyProcessQuery.java
//
//     A ProcessQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.process.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProcessQuery that stores terms.
 */

public abstract class AbstractAssemblyProcessQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.process.ProcessQuery,
               org.osid.process.ProcessQueryInspector,
               org.osid.process.ProcessSearchOrder {

    private final java.util.Collection<org.osid.process.records.ProcessQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.process.records.ProcessQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.process.records.ProcessSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProcessQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProcessQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the state <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStateIdTerms() {
        getAssembler().clearTerms(getStateIdColumn());
        return;
    }


    /**
     *  Gets the state <code> Id </code> terms. 
     *
     *  @return the state <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStateIdTerms() {
        return (getAssembler().getIdTerms(getStateIdColumn()));
    }


    /**
     *  Gets the StateId column name.
     *
     * @return the column name
     */

    protected String getStateIdColumn() {
        return ("state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getStateQuery() {
        throw new org.osid.UnimplementedException("supportsStateQuery() is false");
    }


    /**
     *  Matches processes with any states. 
     *
     *  @param  match <code> true </code> to match processes with any states, 
     *          <code> false </code> to match processes with no states 
     */

    @OSID @Override
    public void matchAnyState(boolean match) {
        getAssembler().addIdWildcardTerm(getStateColumn(), match);
        return;
    }


    /**
     *  Clears the state terms. 
     */

    @OSID @Override
    public void clearStateTerms() {
        getAssembler().clearTerms(getStateColumn());
        return;
    }


    /**
     *  Gets the state terms. 
     *
     *  @return the state terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the State column name.
     *
     * @return the column name
     */

    protected String getStateColumn() {
        return ("state");
    }


    /**
     *  Sets the process <code> Id </code> for this query to match processes 
     *  that have the specified process as an ancestor. 
     *
     *  @param  processId a process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorProcessId(org.osid.id.Id processId, boolean match) {
        getAssembler().addIdTerm(getAncestorProcessIdColumn(), processId, match);
        return;
    }


    /**
     *  Clears the ancestor process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorProcessIdTerms() {
        getAssembler().clearTerms(getAncestorProcessIdColumn());
        return;
    }


    /**
     *  Gets the ancestor process <code> Id </code> terms. 
     *
     *  @return the ancestor process <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorProcessIdTerms() {
        return (getAssembler().getIdTerms(getAncestorProcessIdColumn()));
    }


    /**
     *  Gets the AncestorProcessId column name.
     *
     * @return the column name
     */

    protected String getAncestorProcessIdColumn() {
        return ("ancestor_process_id");
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorProcessQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuery getAncestorProcessQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorProcessQuery() is false");
    }


    /**
     *  Matches processes with any ancestor. 
     *
     *  @param  match <code> true </code> to match process with any ancestor, 
     *          <code> false </code> to match root processes 
     */

    @OSID @Override
    public void matchAnyAncestorProcess(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorProcessColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor process terms. 
     */

    @OSID @Override
    public void clearAncestorProcessTerms() {
        getAssembler().clearTerms(getAncestorProcessColumn());
        return;
    }


    /**
     *  Gets the ancestor process terms. 
     *
     *  @return the ancestor process terms 
     */

    @OSID @Override
    public org.osid.process.ProcessQueryInspector[] getAncestorProcessTerms() {
        return (new org.osid.process.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the AncestorProcess column name.
     *
     * @return the column name
     */

    protected String getAncestorProcessColumn() {
        return ("ancestor_process");
    }


    /**
     *  Sets the process <code> Id </code> for this query to match that have 
     *  the specified process as a descendant. 
     *
     *  @param  processId a process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantProcessId(org.osid.id.Id processId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantProcessIdColumn(), processId, match);
        return;
    }


    /**
     *  Clears the descendant process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantProcessIdTerms() {
        getAssembler().clearTerms(getDescendantProcessIdColumn());
        return;
    }


    /**
     *  Gets the descendant process <code> Id </code> terms. 
     *
     *  @return the descendant process <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantProcessIdTerms() {
        return (getAssembler().getIdTerms(getDescendantProcessIdColumn()));
    }


    /**
     *  Gets the DescendantProcessId column name.
     *
     * @return the column name
     */

    protected String getDescendantProcessIdColumn() {
        return ("descendant_process_id");
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantProcessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuery getDescendantProcessQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantProcessQuery() is false");
    }


    /**
     *  Matches processes with any descendant. 
     *
     *  @param  match <code> true </code> to match process with any 
     *          descendant, <code> false </code> to match leaf processes 
     */

    @OSID @Override
    public void matchAnyDescendantProcess(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantProcessColumn(), match);
        return;
    }


    /**
     *  Clears the descendant process terms. 
     */

    @OSID @Override
    public void clearDescendantProcessTerms() {
        getAssembler().clearTerms(getDescendantProcessColumn());
        return;
    }


    /**
     *  Gets the descendant process terms. 
     *
     *  @return the descendant process terms 
     */

    @OSID @Override
    public org.osid.process.ProcessQueryInspector[] getDescendantProcessTerms() {
        return (new org.osid.process.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the DescendantProcess column name.
     *
     * @return the column name
     */

    protected String getDescendantProcessColumn() {
        return ("descendant_process");
    }


    /**
     *  Tests if this process supports the given record
     *  <code>Type</code>.
     *
     *  @param  processRecordType a process record type 
     *  @return <code>true</code> if the processRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type processRecordType) {
        for (org.osid.process.records.ProcessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  processRecordType the process record type 
     *  @return the process query record 
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.ProcessQueryRecord getProcessQueryRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.ProcessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  processRecordType the process record type 
     *  @return the process query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.ProcessQueryInspectorRecord getProcessQueryInspectorRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.ProcessQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param processRecordType the process record type
     *  @return the process search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.ProcessSearchOrderRecord getProcessSearchOrderRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.ProcessSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this process. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param processQueryRecord the process query record
     *  @param processQueryInspectorRecord the process query inspector
     *         record
     *  @param processSearchOrderRecord the process search order record
     *  @param processRecordType process record type
     *  @throws org.osid.NullArgumentException
     *          <code>processQueryRecord</code>,
     *          <code>processQueryInspectorRecord</code>,
     *          <code>processSearchOrderRecord</code> or
     *          <code>processRecordTypeprocess</code> is
     *          <code>null</code>
     */
            
    protected void addProcessRecords(org.osid.process.records.ProcessQueryRecord processQueryRecord, 
                                      org.osid.process.records.ProcessQueryInspectorRecord processQueryInspectorRecord, 
                                      org.osid.process.records.ProcessSearchOrderRecord processSearchOrderRecord, 
                                      org.osid.type.Type processRecordType) {

        addRecordType(processRecordType);

        nullarg(processQueryRecord, "process query record");
        nullarg(processQueryInspectorRecord, "process query inspector record");
        nullarg(processSearchOrderRecord, "process search odrer record");

        this.queryRecords.add(processQueryRecord);
        this.queryInspectorRecords.add(processQueryInspectorRecord);
        this.searchOrderRecords.add(processSearchOrderRecord);
        
        return;
    }
}
