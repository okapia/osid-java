//
// AbstractAdapterInstructionLookupSession.java
//
//    An Instruction lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.check.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Instruction lookup session adapter.
 */

public abstract class AbstractAdapterInstructionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.rules.check.InstructionLookupSession {

    private final org.osid.rules.check.InstructionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterInstructionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInstructionLookupSession(org.osid.rules.check.InstructionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Engine/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Engine Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.session.getEngineId());
    }


    /**
     *  Gets the {@code Engine} associated with this session.
     *
     *  @return the {@code Engine} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getEngine());
    }


    /**
     *  Tests if this user can perform {@code Instruction} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupInstructions() {
        return (this.session.canLookupInstructions());
    }


    /**
     *  A complete view of the {@code Instruction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInstructionView() {
        this.session.useComparativeInstructionView();
        return;
    }


    /**
     *  A complete view of the {@code Instruction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInstructionView() {
        this.session.usePlenaryInstructionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include instructions in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.session.useFederatedEngineView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.session.useIsolatedEngineView();
        return;
    }
    

    /**
     *  Only active instructions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInstructionView() {
        this.session.useActiveInstructionView();
        return;
    }


    /**
     *  Active and inactive instructions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInstructionView() {
        this.session.useAnyStatusInstructionView();
        return;
    }
    
     
    /**
     *  Gets the {@code Instruction} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Instruction} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Instruction} and
     *  retained for compatibility.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param instructionId {@code Id} of the {@code Instruction}
     *  @return the instruction
     *  @throws org.osid.NotFoundException {@code instructionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code instructionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Instruction getInstruction(org.osid.id.Id instructionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstruction(instructionId));
    }


    /**
     *  Gets an {@code InstructionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  instructions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Instructions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Instruction} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code instructionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByIds(org.osid.id.IdList instructionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsByIds(instructionIds));
    }


    /**
     *  Gets an {@code InstructionList} corresponding to the given
     *  instruction genus {@code Type} which does not include
     *  instructions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionGenusType an instruction genus type 
     *  @return the returned {@code Instruction} list
     *  @throws org.osid.NullArgumentException
     *          {@code instructionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByGenusType(org.osid.type.Type instructionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsByGenusType(instructionGenusType));
    }


    /**
     *  Gets an {@code InstructionList} corresponding to the given
     *  instruction genus {@code Type} and include any additional
     *  instructions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionGenusType an instruction genus type 
     *  @return the returned {@code Instruction} list
     *  @throws org.osid.NullArgumentException
     *          {@code instructionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByParentGenusType(org.osid.type.Type instructionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsByParentGenusType(instructionGenusType));
    }


    /**
     *  Gets an {@code InstructionList} containing the given
     *  instruction record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  instructionRecordType an instruction record type 
     *  @return the returned {@code Instruction} list
     *  @throws org.osid.NullArgumentException
     *          {@code instructionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByRecordType(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsByRecordType(instructionRecordType));
    }


    /**
     *  Gets an {@code InstructionList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *  
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Instruction} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsOnDate(org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsOnDate(from, to));
    }
        

    /**
     *  Gets a list of instructions corresponding to an agenda
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  agendaId the {@code Id} of the agenda
     *  @return the returned {@code InstructionList}
     *  @throws org.osid.NullArgumentException {@code agendaId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgenda(org.osid.id.Id agendaId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsForAgenda(agendaId));
    }


    /**
     *  Gets a list of instructions corresponding to an agenda
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  agendaId the {@code Id} of the agenda
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code InstructionList}
     *  @throws org.osid.NullArgumentException {@code agendaId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaOnDate(org.osid.id.Id agendaId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsForAgendaOnDate(agendaId, from, to));
    }


    /**
     *  Gets a list of instructions corresponding to a check
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  checkId the {@code Id} of the check
     *  @return the returned {@code InstructionList}
     *  @throws org.osid.NullArgumentException {@code checkId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForCheck(org.osid.id.Id checkId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsForCheck(checkId));
    }


    /**
     *  Gets a list of instructions corresponding to a check
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  checkId the {@code Id} of the check
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code InstructionList}
     *  @throws org.osid.NullArgumentException {@code checkId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForCheckOnDate(org.osid.id.Id checkId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsForCheckOnDate(checkId, from, to));
    }


    /**
     *  Gets a list of instructions corresponding to agenda and check
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective.  In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  agendaId the {@code Id} of the agenda
     *  @param  checkId the {@code Id} of the check
     *  @return the returned {@code InstructionList}
     *  @throws org.osid.NullArgumentException {@code agendaId},
     *          {@code checkId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaAndCheck(org.osid.id.Id agendaId,
                                                                                 org.osid.id.Id checkId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsForAgendaAndCheck(agendaId, checkId));
    }


    /**
     *  Gets a list of instructions corresponding to agenda and check
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible
     *  through this session.
     *
     *  In effective mode, instructions are returned that are
     *  currently effective. In any effective mode, effective
     *  instructions and those currently expired are returned.
     *
     *  @param  checkId the {@code Id} of the check
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code InstructionList}
     *  @throws org.osid.NullArgumentException {@code agendaId},
     *          {@code checkId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsForAgendaAndCheckOnDate(org.osid.id.Id agendaId,
                                                                                       org.osid.id.Id checkId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructionsForAgendaAndCheckOnDate(agendaId, checkId, from, to));
    }


    /**
     *  Gets all {@code Instructions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  instructions or an error results. Otherwise, the returned list
     *  may contain only those instructions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, instructions are returned that are currently
     *  active. In any status mode, active and inactive instructions
     *  are returned.
     *
     *  @return a list of {@code Instructions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstructions());
    }
}
