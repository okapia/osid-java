//
// AbstractAssemblyOsidTemporalQuery.java
//
//     An OsidTemporalQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidTemporalQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidTemporalQuery
    extends AbstractAssemblyOsidQuery
    implements org.osid.OsidTemporalQuery,
               org.osid.OsidTemporalQueryInspector,
               org.osid.OsidTemporalSearchOrder {

    
    /** 
     *  Constructs a new
     *  <code>AbstractAssemblyOsidTemporalQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidTemporalQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }

    
    /**
     *  Match effective objects where the current date falls within
     *  the start and end dates inclusive.
     *
     *  @param  match <code> true </code> to match any effective, <code> false 
     *          </code> to match ineffective 
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        getAssembler().addBooleanTerm(getEffectiveColumn(), match);
        return;
    }


    /**
     *  Clears the effective query terms. 
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        getAssembler().clearTerms(getEffectiveColumn());
        return;
    }


    /**
     *  Gets the effective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (getAssembler().getBooleanTerms(getEffectiveColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the effective 
     *  status. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEffective(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEffectiveColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the effective field.
     *
     *  @return the column name
     */

    protected String getEffectiveColumn() {
        return ("effective");
    }    


    /**
     *  Matches temporals whose start date falls in between the given
     *  dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getStartDateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getStartDateColumn(), match);
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        getAssembler().clearTerms(getStartDateColumn());
        return;
    }


    /**
     *  Gets the start date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getStartDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the start date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartDateColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the start date field.
     *
     *  @return the column name
     */

    protected String getStartDateColumn() {
        return ("start_date");
    }    


    /**
     *  Matches temporals whose effective end date falls in between
     *  the given dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param match <code> true </code> if a positive match, <code>
     *          false </code> for negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code>
     *          is less than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end, boolean match) {

        getAssembler().addDateTimeRangeTerm(getEndDateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches temporals with any end date set. 
     *
     *  @param  match <code> true </code> to match any end date, <code> false 
     *          </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getEndDateColumn(), match);
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        getAssembler().clearTerms(getEndDateColumn());
        return;
    }


    /**
     *  Gets the end date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getEndDateColumn()));
    }


    
    /**
     *  Specifies a preference for ordering the result set by the end date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndDateColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the end date field.
     *
     *  @return the column name
     */

    protected String getEndDateColumn() {
        return ("end_date");
    }    


    /**
     *  Matches temporals where the given date range falls entirely between 
     *  the start and end dates inclusive. 
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is less 
     *          than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {

        getAssembler().addDateTimeRangeTerm(getDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        getAssembler().clearTerms(getDateColumn());
        return;
    }

    
    /**
     *  Gets the date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateColumn()));
    }


    /**
     *  Gets the column name for the date field.
     *
     *  @return the column name
     */

    protected String getDateColumn() {
        return ("date");
    }    
}
