//
// AbstractResource.java
//
//     Defines a Resource.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Resource</code>.
 */

public abstract class AbstractResource
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.resource.Resource {

    private boolean group       = false;
    private boolean demographic = false;
    private org.osid.repository.Asset avatar;

    private final java.util.Collection<org.osid.resource.records.ResourceRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this resource is a group. A resource that is a group can be 
     *  used in the group sessions. 
     *
     *  @return <code> true </code> if this resource is a group,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isGroup() {
        return (this.group);
    }


    /**
     *  Sets the group flag.
     *
     *  @param group <code> true </code> if this resource is a group,
     *         <code> false </code> otherwise
     */

    protected void setGroup(boolean group) {
        this.group = group;
        return;
    }


    /**
     *  Tests if this resource is a demographic. A resource that is a
     *  demographic can be used in the demographic service and the
     *  group sessions.
     *
     *  @return <code> true </code> if this resource is a demographic,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isDemographic() {
        return (this.demographic);
    }


    /**
     *  Sets the demographic flag.
     *
     *  @param demographic <code> true </code> if this resource is a
     *         demographic, <code> false </code> otherwise
     */

    protected void setDemographic(boolean demographic) {
        this.demographic = demographic;
        return;
    }


    /**
     *  Tests if this resource has an avatar. 
     *
     *  @return <code> true </code> if this resource has an avatar,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasAvatar() {
        return (this.avatar != null);
    }


    /**
     *  Gets the asset <code> Id. </code> 
     *
     *  @return the asset <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAvatar()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getAvatarId() {
        if (!hasAvatar()) {
            throw new org.osid.IllegalStateException("hasAvatar() is false");
        }

        return (this.avatar.getId());
    }


    /**
     *  Gets the asset. 
     *
     *  @return the asset 
     *  @throws org.osid.IllegalStateException <code> hasAvatar() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.Asset getAvatar()
        throws org.osid.OperationFailedException {

        if (!hasAvatar()) {
            throw new org.osid.IllegalStateException("hasAvatar() is false");
        }

        return (this.avatar);
    }


    /**
     *  Sets the avatar.
     *
     *  @param avatar an avatar
     *  @throws org.osid.NullArgumentException <code>avatar</code> is
     *          <code>null</code>
     */

    protected void setAvatar(org.osid.repository.Asset avatar) {
        nullarg(avatar, "avatar");
        this.avatar = avatar;
        return;
    }


    /**
     *  Tests if this resource supports the given record
     *  <code>Type</code>.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return <code>true</code> if the resourceRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceRecordType) {
        for (org.osid.resource.records.ResourceRecord record : this.records) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Resource</code> record <code>Type</code>.
     *
     *  @param  resourceRecordType the resource record type 
     *  @return the resource record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRecord getResourceRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRecord record : this.records) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceRecord the resource record
     *  @param resourceRecordType resource record type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecord</code> or
     *          <code>resourceRecordTyperesource</code> is
     *          <code>null</code>
     */
            
    protected void addResourceRecord(org.osid.resource.records.ResourceRecord resourceRecord, 
                                     org.osid.type.Type resourceRecordType) {

        nullarg(resourceRecord, "resource record");
        addRecordType(resourceRecordType);
        this.records.add(resourceRecord);
        
        return;
    }
}
