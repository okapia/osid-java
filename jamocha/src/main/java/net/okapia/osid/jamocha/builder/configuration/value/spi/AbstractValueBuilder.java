//
// AbstractValue.java
//
//     Defines a Value builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.value.spi;


/**
 *  Defines a <code>Value</code> builder.
 */

public abstract class AbstractValueBuilder<T extends AbstractValueBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.configuration.value.ValueMiter value;


    /**
     *  Constructs a new <code>AbstractValueBuilder</code>.
     *
     *  @param value the value to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractValueBuilder(net.okapia.osid.jamocha.builder.configuration.value.ValueMiter value) {
        super(value);
        this.value = value;
        return;
    }


    /**
     *  Builds the value.
     *
     *  @return the new value
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.configuration.Value build() {
        (new net.okapia.osid.jamocha.builder.validator.configuration.value.ValueValidator(getValidations())).validate(this.value);
        return (new net.okapia.osid.jamocha.builder.configuration.value.ImmutableValue(this.value));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the value miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.configuration.value.ValueMiter getMiter() {
        return (this.value);
    }


    /**
     *  Sets the parameter.
     *
     *  @param parameter a parameter
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>parameter</code> is <code>null</code>
     */

    public T parameter(org.osid.configuration.Parameter parameter) {
        getMiter().setParameter(parameter);
        return (self());
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>priority</code> is <code>null</code>
     */

    public T priority(long priority) {
        getMiter().setPriority(priority);
        return (self());
    }


    /**
     *  Sets the boolean value.
     *
     *  @param value a boolean value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T booleanValue(boolean value) {
        getMiter().setBooleanValue(value);
        return (self());
    }


    /**
     *  Sets the bytes value.
     *
     *  @param value a bytes value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T bytesValue(byte[] value) {
        getMiter().setBytesValue(value);
        return (self());
    }


    /**
     *  Sets the cardinal value.
     *
     *  @param value a cardinal value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T cardinalValue(long value) {
        getMiter().setCardinalValue(value);
        return (self());
    }


    /**
     *  Sets the coordinate value.
     *
     *  @param value a coordinate value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T coordinateValue(org.osid.mapping.Coordinate value) {
        getMiter().setCoordinateValue(value);
        return (self());
    }


    /**
     *  Sets the currency value.
     *
     *  @param value a currency value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T currencyValue(org.osid.financials.Currency value) {
        getMiter().setCurrencyValue(value);
        return (self());
    }


    /**
     *  Sets the date time value.
     *
     *  @param value a date time value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T dateTimeValue(org.osid.calendaring.DateTime value) {
        getMiter().setDateTimeValue(value);
        return (self());
    }


    /**
     *  Sets the decimal value.
     *
     *  @param value a decimal value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T decimalValue(java.math.BigDecimal value) {
        getMiter().setDecimalValue(value);
        return (self());
    }


    /**
     *  Sets the distance value.
     *
     *  @param value a distance value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T distanceValue(org.osid.mapping.Distance value) {
        getMiter().setDistanceValue(value);
        return (self());
    }


    /**
     *  Sets the duration value.
     *
     *  @param value a duration value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T durationValue(org.osid.calendaring.Duration value) {
        getMiter().setDurationValue(value);
        return (self());
    }


    /**
     *  Sets the heading value.
     *
     *  @param value a heading value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T headingValue(org.osid.mapping.Heading value) {
        getMiter().setHeadingValue(value);
        return (self());
    }


    /**
     *  Sets the id value.
     *
     *  @param value an id value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T idValue(org.osid.id.Id value) {
        getMiter().setIdValue(value);
        return (self());
    }


    /**
     *  Sets the integer value.
     *
     *  @param value an integer value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T integerValue(long value) {
        getMiter().setIntegerValue(value);
        return (self());
    }


    /**
     *  Sets the object value.
     *
     *  @param value an object value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T objectValue(java.lang.Object value) {
        getMiter().setObjectValue(value);
        return (self());
    }


    /**
     *  Sets the spatial unit value.
     *
     *  @param value a spatial unit value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T spatialUnitValue(org.osid.mapping.SpatialUnit value) {
        getMiter().setSpatialUnitValue(value);
        return (self());
    }


    /**
     *  Sets the speed value.
     *
     *  @param value a speed value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T speedValue(org.osid.mapping.Speed value) {
        getMiter().setSpeedValue(value);
        return (self());
    }


    /**
     *  Sets the string value.
     *
     *  @param value a string value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T stringValue(String value) {
        getMiter().setStringValue(value);
        return (self());
    }


    /**
     *  Sets the time value.
     *
     *  @param value a time value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T timeValue(org.osid.calendaring.Time value) {
        getMiter().setTimeValue(value);
        return (self());
    }


    /**
     *  Sets the type value.
     *
     *  @param value a type value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T typeValue(org.osid.type.Type value) {
        getMiter().setTypeValue(value);
        return (self());
    }


    /**
     *  Sets the version value.
     *
     *  @param value a version value
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>value</code> is <code>null</code>
     */

    public T versionValue(org.osid.installation.Version value) {
        getMiter().setVersionValue(value);
        return (self());
    }
}       


