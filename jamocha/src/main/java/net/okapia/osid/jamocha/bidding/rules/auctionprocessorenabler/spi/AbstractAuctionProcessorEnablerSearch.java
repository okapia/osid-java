//
// AbstractAuctionProcessorEnablerSearch.java
//
//     A template for making an AuctionProcessorEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing auction processor enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuctionProcessorEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.bidding.rules.AuctionProcessorEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.bidding.rules.AuctionProcessorEnablerSearchOrder auctionProcessorEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of auction processor enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  auctionProcessorEnablerIds list of auction processor enablers
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuctionProcessorEnablers(org.osid.id.IdList auctionProcessorEnablerIds) {
        while (auctionProcessorEnablerIds.hasNext()) {
            try {
                this.ids.add(auctionProcessorEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuctionProcessorEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of auction processor enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuctionProcessorEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  auctionProcessorEnablerSearchOrder auction processor enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>auctionProcessorEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuctionProcessorEnablerResults(org.osid.bidding.rules.AuctionProcessorEnablerSearchOrder auctionProcessorEnablerSearchOrder) {
	this.auctionProcessorEnablerSearchOrder = auctionProcessorEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.bidding.rules.AuctionProcessorEnablerSearchOrder getAuctionProcessorEnablerSearchOrder() {
	return (this.auctionProcessorEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given auction processor enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction processor enabler implementing the requested record.
     *
     *  @param auctionProcessorEnablerSearchRecordType an auction processor enabler search record
     *         type
     *  @return the auction processor enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorEnablerSearchRecord getAuctionProcessorEnablerSearchRecord(org.osid.type.Type auctionProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.bidding.rules.records.AuctionProcessorEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction processor enabler search. 
     *
     *  @param auctionProcessorEnablerSearchRecord auction processor enabler search record
     *  @param auctionProcessorEnablerSearchRecordType auctionProcessorEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionProcessorEnablerSearchRecord(org.osid.bidding.rules.records.AuctionProcessorEnablerSearchRecord auctionProcessorEnablerSearchRecord, 
                                           org.osid.type.Type auctionProcessorEnablerSearchRecordType) {

        addRecordType(auctionProcessorEnablerSearchRecordType);
        this.records.add(auctionProcessorEnablerSearchRecord);        
        return;
    }
}
