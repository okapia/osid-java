//
// AbstractRepositoryBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRepositoryBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.repository.batch.RepositoryBatchManager,
               org.osid.repository.batch.RepositoryBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractRepositoryBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRepositoryBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of assets is available. 
     *
     *  @return <code> true </code> if an asset bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of compositions is available. 
     *
     *  @return <code> true </code> if a composition bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of repositories is available. 
     *
     *  @return <code> true </code> if an repository bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk asset 
     *  administration service. 
     *
     *  @return an <code> AssetBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.AssetBatchAdminSession getAssetBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchManager.getAssetBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk asset 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.AssetBatchAdminSession getAssetBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchProxyManager.getAssetBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk asset 
     *  administration service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return an <code> AssetBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.AssetBatchAdminSession getAssetBatchAdminSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchManager.getAssetBatchAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk asset 
     *  administration service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssetBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.AssetBatchAdminSession getAssetBatchAdminSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchProxyManager.getAssetBatchAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  composition administration service. 
     *
     *  @return a <code> CompositionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.CompositionBatchAdminSession getCompositionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchManager.getCompositionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  composition administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.CompositionBatchAdminSession getCompositionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchProxyManager.getCompositionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  composition administration service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.CompositionBatchAdminSession getCompositionBatchAdminSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchManager.getCompositionBatchAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  composition administration service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.CompositionBatchAdminSession getCompositionBatchAdminSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchProxyManager.getCompositionBatchAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  repository administration service. 
     *
     *  @return a <code> RepositoryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.RepositoryBatchAdminSession getRepositoryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchManager.getRepositoryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  repository administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.RepositoryBatchAdminSession getRepositoryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.batch.RepositoryBatchProxyManager.getRepositoryBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
