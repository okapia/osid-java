//
// AbstractAssemblyAnswerQuery.java
//
//     An AnswerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.answer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AnswerQuery that stores terms.
 */

public abstract class AbstractAssemblyAnswerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.assessment.AnswerQuery,
               org.osid.assessment.AnswerQueryInspector,
               org.osid.assessment.AnswerSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.AnswerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AnswerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AnswerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAnswerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAnswerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    




    /**
     *  Tests if this answer supports the given record
     *  <code>Type</code>.
     *
     *  @param  answerRecordType an answer record type 
     *  @return <code>true</code> if the answerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>answerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type answerRecordType) {
        for (org.osid.assessment.records.AnswerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(answerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  answerRecordType the answer record type 
     *  @return the answer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>answerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(answerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AnswerQueryRecord getAnswerQueryRecord(org.osid.type.Type answerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AnswerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(answerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(answerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  answerRecordType the answer record type 
     *  @return the answer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>answerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(answerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AnswerQueryInspectorRecord getAnswerQueryInspectorRecord(org.osid.type.Type answerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AnswerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(answerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(answerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param answerRecordType the answer record type
     *  @return the answer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>answerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(answerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AnswerSearchOrderRecord getAnswerSearchOrderRecord(org.osid.type.Type answerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AnswerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(answerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(answerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this answer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param answerQueryRecord the answer query record
     *  @param answerQueryInspectorRecord the answer query inspector
     *         record
     *  @param answerSearchOrderRecord the answer search order record
     *  @param answerRecordType answer record type
     *  @throws org.osid.NullArgumentException
     *          <code>answerQueryRecord</code>,
     *          <code>answerQueryInspectorRecord</code>,
     *          <code>answerSearchOrderRecord</code> or
     *          <code>answerRecordTypeanswer</code> is
     *          <code>null</code>
     */
            
    protected void addAnswerRecords(org.osid.assessment.records.AnswerQueryRecord answerQueryRecord, 
                                      org.osid.assessment.records.AnswerQueryInspectorRecord answerQueryInspectorRecord, 
                                      org.osid.assessment.records.AnswerSearchOrderRecord answerSearchOrderRecord, 
                                      org.osid.type.Type answerRecordType) {

        addRecordType(answerRecordType);

        nullarg(answerQueryRecord, "answer query record");
        nullarg(answerQueryInspectorRecord, "answer query inspector record");
        nullarg(answerSearchOrderRecord, "answer search odrer record");

        this.queryRecords.add(answerQueryRecord);
        this.queryInspectorRecords.add(answerQueryInspectorRecord);
        this.searchOrderRecords.add(answerSearchOrderRecord);
        
        return;
    }
}
