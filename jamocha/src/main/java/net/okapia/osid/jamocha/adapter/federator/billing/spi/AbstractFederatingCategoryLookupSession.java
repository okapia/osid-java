//
// AbstractFederatingCategoryLookupSession.java
//
//     An abstract federating adapter for a CategoryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CategoryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCategoryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.billing.CategoryLookupSession>
    implements org.osid.billing.CategoryLookupSession {

    private boolean parallel = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingCategoryLookupSession</code>.
     */

    protected AbstractFederatingCategoryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.billing.CategoryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Category</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCategories() {
        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            if (session.canLookupCategories()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Category</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCategoryView() {
        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            session.useComparativeCategoryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Category</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCategoryView() {
        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            session.usePlenaryCategoryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include categories in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Category</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Category</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Category</code> and
     *  retained for compatibility.
     *
     *  @param  categoryId <code>Id</code> of the
     *          <code>Category</code>
     *  @return the category
     *  @throws org.osid.NotFoundException <code>categoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>categoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Category getCategory(org.osid.id.Id categoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            try {
                return (session.getCategory(categoryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(categoryId + " not found");
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  categories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Categories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  categoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>categoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByIds(org.osid.id.IdList categoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.billing.category.MutableCategoryList ret = new net.okapia.osid.jamocha.billing.category.MutableCategoryList();

        try (org.osid.id.IdList ids = categoryIds) {
            while (ids.hasNext()) {
                ret.addCategory(getCategory(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  category genus <code>Type</code> which does not include
     *  categories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  categoryGenusType a category genus type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByGenusType(org.osid.type.Type categoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList ret = getCategoryList();

        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            ret.addCategoryList(session.getCategoriesByGenusType(categoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CategoryList</code> corresponding to the given
     *  category genus <code>Type</code> and include any additional
     *  categories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  categoryGenusType a category genus type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByParentGenusType(org.osid.type.Type categoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList ret = getCategoryList();

        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            ret.addCategoryList(session.getCategoriesByParentGenusType(categoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CategoryList</code> containing the given
     *  category record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  categoryRecordType a category record type 
     *  @return the returned <code>Category</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategoriesByRecordType(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList ret = getCategoryList();

        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            ret.addCategoryList(session.getCategoriesByRecordType(categoryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Categories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  categories or an error results. Otherwise, the returned list
     *  may contain only those categories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Categories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList ret = getCategoryList();

        for (org.osid.billing.CategoryLookupSession session : getSessions()) {
            ret.addCategoryList(session.getCategories());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.billing.category.FederatingCategoryList getCategoryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.billing.category.ParallelCategoryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.billing.category.CompositeCategoryList());
        }
    }
}
