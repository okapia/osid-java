//
// AbstractOsidOperableQuery
//
//     Defines an operable OsidQuery.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an Operable OsidQuery.
 */

public abstract class AbstractOsidOperableQuery
    extends AbstractOsidQuery
    implements org.osid.OsidOperableQuery {

    
    /**
     *  Matches active. 
     *
     *  @param  match <code> true </code> to match active, <code> false 
     *          </code> to match inactive 
     */

    @OSID @Override
    public void matchActive(boolean match) {
        return;
    }


    /**
     *  Clears the active query terms. 
     */

    @OSID @Override
    public void clearActiveTerms() {
        return;
    }


    /**
     *  Matches administratively enabled. 
     *
     *  @param  match <code> true </code> to match administratively enabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        return;
    }


    /**
     *  Clears the administratively enabled query terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        return;
    }


    /**
     *  Matches administratively disabled. 
     *
     *  @param  match <code> true </code> to match administratively disabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchDisabled(boolean match) {
        return;
    }


    /**
     *  Clears the administratively disabled query terms. 
     */

    @OSID @Override
    public void clearDisabledTerms() {
        return;
    }


    /**
     *  Matches operational operables. 
     *
     *  @param  match <code> true </code> to match operational, <code> false 
     *          </code> to match not operational 
     */

    @OSID @Override
    public void matchOperational(boolean match) {
        return;
    }


    /**
     *  Clears the operational query terms. 
     */

    @OSID @Override
    public void clearOperationalTerms() {
        return;
    }
}
