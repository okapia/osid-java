//
// AbstractQueryProfileEntryEnablerLookupSession.java
//
//    A ProfileEntryEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProfileEntryEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterProfileEntryEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.profile.rules.ProfileEntryEnablerQuerySession {

    private final org.osid.profile.rules.ProfileEntryEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterProfileEntryEnablerQuerySession.
     *
     *  @param session the underlying profile entry enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProfileEntryEnablerQuerySession(org.osid.profile.rules.ProfileEntryEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeProfile</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeProfile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.session.getProfileId());
    }


    /**
     *  Gets the {@codeProfile</code> associated with this 
     *  session.
     *
     *  @return the {@codeProfile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getProfile());
    }


    /**
     *  Tests if this user can perform {@codeProfileEntryEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchProfileEntryEnablers() {
        return (this.session.canSearchProfileEntryEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile entry enablers in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.session.useFederatedProfileView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this profile only.
     */
    
    @OSID @Override
    public void useIsolatedProfileView() {
        this.session.useIsolatedProfileView();
        return;
    }
    
      
    /**
     *  Gets a profile entry enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the profile entry enabler query 
     */
      
    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerQuery getProfileEntryEnablerQuery() {
        return (this.session.getProfileEntryEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  profileEntryEnablerQuery the profile entry enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code profileEntryEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code profileEntryEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersByQuery(org.osid.profile.rules.ProfileEntryEnablerQuery profileEntryEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getProfileEntryEnablersByQuery(profileEntryEnablerQuery));
    }
}
