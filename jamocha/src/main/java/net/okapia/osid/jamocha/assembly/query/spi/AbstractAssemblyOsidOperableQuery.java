//
// AbstractAssemblyOsidOperableQuery.java
//
//     An OsidOperableQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OsidOperableQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidOperableQuery
    extends AbstractAssemblyOsidQuery
    implements org.osid.OsidOperableQuery,
               org.osid.OsidOperableQueryInspector,
               org.osid.OsidOperableSearchOrder {

    
    /** 
     *  Constructs a new <code>AbstractAssemblyOsidOperableQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidOperableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches active operables.
     *
     *  @param match <code> true </code> to match active objects,
     *          <code> false </code> to match inactive objects
     */

    @OSID @Override
    public void matchActive(boolean match) {
        getAssembler().addBooleanTerm(getActiveColumn(), match);
        return;
    }


    /**
     *  Clears all match active terms. 
     */

    @OSID @Override
    public void clearActiveTerms() {
        getAssembler().clearTerms(getActiveColumn());
        return;
    }


    /**
     *  Gets the active query terms. 
     *
     *  @return the active terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getActiveTerms() {
        return (getAssembler().getBooleanTerms(getActiveColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  active flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActive(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActiveColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the active field.
     *
     *  @return the column name
     */

    protected String getActiveColumn() {
        return ("active");
    }


    /**
     *  Matches enabled operables.
     *
     *  @param match <code> true </code> to match enabled objects,
     *          <code> false </code> to match inenabled objects
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        getAssembler().addBooleanTerm(getEnabledColumn(), match);
        return;
    }


    /**
     *  Clears all match enabled terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        getAssembler().clearTerms(getEnabledColumn());
        return;
    }


    /**
     *  Gets the enabled query terms. 
     *
     *  @return the enabled terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (getAssembler().getBooleanTerms(getEnabledColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  enabled flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnabled(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEnabledColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the enabled field.
     *
     *  @return the column name
     */

    protected String getEnabledColumn() {
        return ("enabled");
    }
    

    /**
     *  Matches disabled operables.
     *
     *  @param match <code> true </code> to match disabled objects,
     *          <code> false </code> to match indisabled objects
     */

    @OSID @Override
    public void matchDisabled(boolean match) {
        getAssembler().addBooleanTerm(getDisabledColumn(), match);
        return;
    }


    /**
     *  Clears all match disabled terms. 
     */

    @OSID @Override
    public void clearDisabledTerms() {
        getAssembler().clearTerms(getDisabledColumn());
        return;
    }


    /**
     *  Gets the disabled query terms. 
     *
     *  @return the disabled terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDisabledTerms() {
        return (getAssembler().getBooleanTerms(getDisabledColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  disabled flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisabled(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDisabledColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the disabled field.
     *
     *  @return the column name
     */

    protected String getDisabledColumn() {
        return ("disabled");
    }


    /**
     *  Matches operational operables.
     *
     *  @param match <code> true </code> to match operational objects,
     *          <code> false </code> to match inoperational objects
     */

    @OSID @Override
    public void matchOperational(boolean match) {
        getAssembler().addBooleanTerm(getOperationalColumn(), match);
        return;
    }


    /**
     *  Clears all match operational terms. 
     */

    @OSID @Override
    public void clearOperationalTerms() {
        getAssembler().clearTerms(getOperationalColumn());
        return;
    }


    /**
     *  Gets the operational query terms. 
     *
     *  @return the operational terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOperationalTerms() {
        return (getAssembler().getBooleanTerms(getOperationalColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  operational flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOperational(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOperationalColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the operational field.
     *
     *  @return the column name
     */

    protected String getOperationalColumn() {
        return ("operational");
    }
}
