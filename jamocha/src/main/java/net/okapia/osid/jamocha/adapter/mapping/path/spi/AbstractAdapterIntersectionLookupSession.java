//
// AbstractAdapterIntersectionLookupSession.java
//
//    An Intersection lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Intersection lookup session adapter.
 */

public abstract class AbstractAdapterIntersectionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.IntersectionLookupSession {

    private final org.osid.mapping.path.IntersectionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterIntersectionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterIntersectionLookupSession(org.osid.mapping.path.IntersectionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code Intersection} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupIntersections() {
        return (this.session.canLookupIntersections());
    }


    /**
     *  A complete view of the {@code Intersection} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeIntersectionView() {
        this.session.useComparativeIntersectionView();
        return;
    }


    /**
     *  A complete view of the {@code Intersection} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryIntersectionView() {
        this.session.usePlenaryIntersectionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include intersections in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    
     
    /**
     *  Gets the {@code Intersection} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Intersection} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Intersection} and
     *  retained for compatibility.
     *
     *  @param intersectionId {@code Id} of the {@code Intersection}
     *  @return the intersection
     *  @throws org.osid.NotFoundException {@code intersectionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code intersectionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Intersection getIntersection(org.osid.id.Id intersectionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIntersection(intersectionId));
    }


    /**
     *  Gets an {@code IntersectionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  intersections specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Intersections} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  intersectionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Intersection} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code intersectionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByIds(org.osid.id.IdList intersectionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIntersectionsByIds(intersectionIds));
    }


    /**
     *  Gets an {@code IntersectionList} corresponding to the given
     *  intersection genus {@code Type} which does not include
     *  intersections of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  intersectionGenusType an intersection genus type 
     *  @return the returned {@code Intersection} list
     *  @throws org.osid.NullArgumentException
     *          {@code intersectionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByGenusType(org.osid.type.Type intersectionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIntersectionsByGenusType(intersectionGenusType));
    }


    /**
     *  Gets an {@code IntersectionList} corresponding to the given
     *  intersection genus {@code Type} and include any additional
     *  intersections with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  intersectionGenusType an intersection genus type 
     *  @return the returned {@code Intersection} list
     *  @throws org.osid.NullArgumentException
     *          {@code intersectionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByParentGenusType(org.osid.type.Type intersectionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIntersectionsByParentGenusType(intersectionGenusType));
    }


    /**
     *  Gets an {@code IntersectionList} containing the given
     *  intersection record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  intersectionRecordType an intersection record type 
     *  @return the returned {@code Intersection} list
     *  @throws org.osid.NullArgumentException
     *          {@code intersectionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByRecordType(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIntersectionsByRecordType(intersectionRecordType));
    }


    /**
     *  Gets an {@code IntersectionList} connected to the given {@code
     *  Path}. In plenary mode, the returned list contains all of the
     *  intersections, or an error results if an intersection along
     *  the path is not found or inaccessible. Otherwise, inaccessible
     *  {@code Intersections} may be omitted from the list.
     *
     *  @param  pathId a path {@code Id} 
     *  @return the returned {@code Intersection} list 
     *  @throws org.osid.NullArgumentException {@code pathId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getIntersectionsForPath(pathId));
    }


    /**
     *  Gets an {@code IntersectionList} connected to the given {@code
     *  Path} within a distance of a given coordinate. In plenary
     *  mode, the returned list contains all of the paths, or an error
     *  results if a path connected to the location is not found or
     *  inaccessible.  Otherwise, inaccessible {@code Paths} may be
     *  omitted from the list.
     *
     *  @param  pathId a path {@code Id} 
     *  @param  coordinate a coordinate 
     *  @param  distance a distance 
     *  @return the returned {@code Intersection} list 
     *  @throws org.osid.NullArgumentException {@code pathId,
     *         coordinate}, or {@code distance} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                                      org.osid.mapping.Coordinate coordinate, 
                                                                                      org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIntersectionsForPathAtCoordinate(pathId, coordinate, distance));
    }


    /**
     *  Gets the {@code Intersections} of both given paths. In plenary
     *  mode, the returned list contains all of the intersections, or
     *  an error results if an intersection along the path is not
     *  found or inaccessible. Otherwise, inaccessible {@code
     *  Intersections} may be omitted from the list.
     *
     *  @param  pathId a path {@code Id} 
     *  @param  crossingPathId another path {@code Id} 
     *  @return the returned {@code Intersection} list 
     *  @throws org.osid.NullArgumentException {@code pathId} or {@code 
     *          crossingPathId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsForPaths(org.osid.id.Id pathId, 
                                                                           org.osid.id.Id crossingPathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getIntersectionsForPaths(pathId, crossingPathId));
    }


    /**
     *  Gets all {@code Intersections}. 
     *
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Intersections} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersections()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIntersections());
    }
}
