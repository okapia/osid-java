//
// AbstractFederatingBranchLookupSession.java
//
//     An abstract federating adapter for a BranchLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BranchLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBranchLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.journaling.BranchLookupSession>
    implements org.osid.journaling.BranchLookupSession {

    private boolean parallel = false;
    private org.osid.journaling.Journal journal = new net.okapia.osid.jamocha.nil.journaling.journal.UnknownJournal();


    /**
     *  Constructs a new <code>AbstractFederatingBranchLookupSession</code>.
     */

    protected AbstractFederatingBranchLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.journaling.BranchLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Journal/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Journal Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.journal.getId());
    }


    /**
     *  Gets the <code>Journal</code> associated with this 
     *  session.
     *
     *  @return the <code>Journal</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.journal);
    }


    /**
     *  Sets the <code>Journal</code>.
     *
     *  @param  journal the journal for this session
     *  @throws org.osid.NullArgumentException <code>journal</code>
     *          is <code>null</code>
     */

    protected void setJournal(org.osid.journaling.Journal journal) {
        nullarg(journal, "journal");
        this.journal = journal;
        return;
    }


    /**
     *  Tests if this user can perform <code>Branch</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBranches() {
        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            if (session.canLookupBranches()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Branch</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBranchView() {
        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            session.useComparativeBranchView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Branch</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBranchView() {
        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            session.usePlenaryBranchView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include branches in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            session.useFederatedJournalView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            session.useIsolatedJournalView();
        }

        return;
    }


    /**
     *  Only active branches are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBranchView() {
        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            session.useActiveBranchView();
        }

        return;
    }


    /**
     *  Active and inactive branches are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBranchView() {
        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            session.useAnyStatusBranchView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Branch</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Branch</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Branch</code> and
     *  retained for compatibility.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchId <code>Id</code> of the
     *          <code>Branch</code>
     *  @return the branch
     *  @throws org.osid.NotFoundException <code>branchId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>branchId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Branch getBranch(org.osid.id.Id branchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            try {
                return (session.getBranch(branchId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(branchId + " not found");
    }


    /**
     *  Gets a <code>BranchList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  branches specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Branches</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>branchIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByIds(org.osid.id.IdList branchIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.journaling.branch.MutableBranchList ret = new net.okapia.osid.jamocha.journaling.branch.MutableBranchList();

        try (org.osid.id.IdList ids = branchIds) {
            while (ids.hasNext()) {
                ret.addBranch(getBranch(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BranchList</code> corresponding to the given
     *  branch genus <code>Type</code> which does not include
     *  branches of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchGenusType a branch genus type 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByGenusType(org.osid.type.Type branchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.branch.FederatingBranchList ret = getBranchList();

        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            ret.addBranchList(session.getBranchesByGenusType(branchGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BranchList</code> corresponding to the given
     *  branch genus <code>Type</code> and include any additional
     *  branches with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchGenusType a branch genus type 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByParentGenusType(org.osid.type.Type branchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.branch.FederatingBranchList ret = getBranchList();

        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            ret.addBranchList(session.getBranchesByParentGenusType(branchGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BranchList</code> containing the given
     *  branch record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchRecordType a branch record type 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByRecordType(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.branch.FederatingBranchList ret = getBranchList();

        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            ret.addBranchList(session.getBranchesByRecordType(branchRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Branches</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @return a list of <code>Branches</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranches()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.branch.FederatingBranchList ret = getBranchList();

        for (org.osid.journaling.BranchLookupSession session : getSessions()) {
            ret.addBranchList(session.getBranches());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.journaling.branch.FederatingBranchList getBranchList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.journaling.branch.ParallelBranchList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.journaling.branch.CompositeBranchList());
        }
    }
}
