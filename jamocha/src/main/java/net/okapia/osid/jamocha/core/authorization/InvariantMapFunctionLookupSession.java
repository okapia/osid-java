//
// InvariantMapFunctionLookupSession
//
//    Implements a Function lookup service backed by a fixed collection of
//    functions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements a Function lookup service backed by a fixed
 *  collection of functions. The functions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapFunctionLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractMapFunctionLookupSession
    implements org.osid.authorization.FunctionLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapFunctionLookupSession</code> with no
     *  functions.
     *  
     *  @param vault the vault
     *  @throws org.osid.NullArgumnetException {@code vault} is
     *          {@code null}
     */

    public InvariantMapFunctionLookupSession(org.osid.authorization.Vault vault) {
        setVault(vault);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFunctionLookupSession</code> with a single
     *  function.
     *  
     *  @param vault the vault
     *  @param function a single function
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code function} is <code>null</code>
     */

      public InvariantMapFunctionLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.Function function) {
        this(vault);
        putFunction(function);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFunctionLookupSession</code> using an array
     *  of functions.
     *  
     *  @param vault the vault
     *  @param functions an array of functions
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code functions} is <code>null</code>
     */

      public InvariantMapFunctionLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.Function[] functions) {
        this(vault);
        putFunctions(functions);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFunctionLookupSession</code> using a
     *  collection of functions.
     *
     *  @param vault the vault
     *  @param functions a collection of functions
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code functions} is <code>null</code>
     */

      public InvariantMapFunctionLookupSession(org.osid.authorization.Vault vault,
                                               java.util.Collection<? extends org.osid.authorization.Function> functions) {
        this(vault);
        putFunctions(functions);
        return;
    }
}
