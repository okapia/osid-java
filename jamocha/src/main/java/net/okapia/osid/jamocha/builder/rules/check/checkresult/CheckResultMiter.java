//
// CheckResultMiter.java
//
//     Defines a CheckResult miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.checkresult;


/**
 *  Defines a <code>CheckResult</code> miter for use with the builders.
 */

public interface CheckResultMiter
    extends net.okapia.osid.jamocha.builder.spi.BrowsableMiter,
            org.osid.rules.check.CheckResult {


    /**
     *  Sets the instruction.
     *
     *  @param instruction an instruction
     *  @throws org.osid.NullArgumentException
     *          <code>instruction</code> is <code>null</code>
     */

    public void setInstruction(org.osid.rules.check.Instruction instruction);


    /**
     *  Sets the failed flag.
     *
     *  @param failed <code> true </code> if this check failed, <code> false
     *         </code> otherwise
     */

    public void setFailed(boolean failed);


    /**
     *  Sets the warning flag.
     *
     *  @param warning <code> true </code> if this check is a warning,
     *         <code> false </code> otherwise
     */

    public void setWarning(boolean warning);
    

    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    public void setMessage(org.osid.locale.DisplayText message);


    /**
     *  Adds a CheckResult record.
     *
     *  @param record a checkResult record
     *  @param recordType the type of checkResult record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCheckResultRecord(org.osid.rules.check.records.CheckResultRecord record, org.osid.type.Type recordType);
}       


