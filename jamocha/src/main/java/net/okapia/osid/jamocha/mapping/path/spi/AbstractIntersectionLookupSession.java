//
// AbstractIntersectionLookupSession.java
//
//    A starter implementation framework for providing an Intersection
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Intersection
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getIntersections(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractIntersectionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.path.IntersectionLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();
    

    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Intersection</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupIntersections() {
        return (true);
    }


    /**
     *  A complete view of the <code>Intersection</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeIntersectionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Intersection</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryIntersectionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include intersections in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Intersection</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Intersection</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Intersection</code> and
     *  retained for compatibility.
     *
     *  @param  intersectionId <code>Id</code> of the
     *          <code>Intersection</code>
     *  @return the intersection
     *  @throws org.osid.NotFoundException <code>intersectionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>intersectionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Intersection getIntersection(org.osid.id.Id intersectionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.path.IntersectionList intersections = getIntersections()) {
            while (intersections.hasNext()) {
                org.osid.mapping.path.Intersection intersection = intersections.getNextIntersection();
                if (intersection.getId().equals(intersectionId)) {
                    return (intersection);
                }
            }
        } 

        throw new org.osid.NotFoundException(intersectionId + " not found");
    }


    /**
     *  Gets an <code>IntersectionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  intersections specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Intersections</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getIntersections()</code>.
     *
     *  @param  intersectionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Intersection</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByIds(org.osid.id.IdList intersectionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.path.Intersection> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = intersectionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getIntersection(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("intersection " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.path.intersection.LinkedIntersectionList(ret));
    }


    /**
     *  Gets an <code>IntersectionList</code> corresponding to the given
     *  intersection genus <code>Type</code> which does not include
     *  intersections of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getIntersections()</code>.
     *
     *  @param  intersectionGenusType an intersection genus type 
     *  @return the returned <code>Intersection</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByGenusType(org.osid.type.Type intersectionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.intersection.IntersectionGenusFilterList(getIntersections(), intersectionGenusType));
    }


    /**
     *  Gets an <code>IntersectionList</code> corresponding to the given
     *  intersection genus <code>Type</code> and include any additional
     *  intersections with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getIntersections()</code>.
     *
     *  @param  intersectionGenusType an intersection genus type 
     *  @return the returned <code>Intersection</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByParentGenusType(org.osid.type.Type intersectionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getIntersectionsByGenusType(intersectionGenusType));
    }


    /**
     *  Gets an <code>IntersectionList</code> containing the given
     *  intersection record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getIntersections()</code>.
     *
     *  @param  intersectionRecordType an intersection record type 
     *  @return the returned <code>Intersection</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsByRecordType(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.intersection.IntersectionRecordFilterList(getIntersections(), intersectionRecordType));
    }


    /**
     *  Gets an <code>IntersectionList</code> connected to the given
     *  <code>Path</code>. In plenary mode, the returned list contains
     *  all of the intersections, or an error results if an
     *  intersection along the path is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Intersections</code> may be omitted from the list.
     *
     *  @param  pathId a path <code>Id</code> 
     *  @return the returned <code>Intersection</code> list 
     *  @throws org.osid.NullArgumentException <code>pathId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.path.Intersection> ret = new java.util.ArrayList<>();

        try (org.osid.mapping.path.IntersectionList intersections = getIntersections()) {
            while (intersections.hasNext()) {
                org.osid.mapping.path.Intersection intersection = intersections.getNextIntersection();
                try (org.osid.id.IdList ids = intersection.getPathIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(pathId)) {
                            ret.add(intersection);
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.path.intersection.LinkedIntersectionList(ret));
    }


    /**
     *  Gets an <code>IntersectionList</code> connected to the given
     *  <code>Path</code> within a distance of a given coordinate. In
     *  plenary mode, the returned list contains all of the paths, or
     *  an error results if a path connected to the location is not
     *  found or inaccessible.  Otherwise, inaccessible
     *  <code>Paths</code> may be omitted from the list.
     *
     *  This method should be implemented.
     *
     *  @param  pathId a path <code>Id</code> 
     *  @param  coordinate a coordinate 
     *  @param  distance a distance 
     *  @return the returned <code>Intersection</code> list 
     *  @throws org.osid.NullArgumentException <code>pathId</code>,
     *          <code>coordinate</code> or <code>distance</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                                      org.osid.mapping.Coordinate coordinate, 
                                                                                      org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getIntersectionsForPath(pathId));
    }


    /**
     *  Gets the <code>Intersections</code> of both given paths. In
     *  plenary mode, the returned list contains all of the
     *  intersections, or an error results if an intersection along
     *  the path is not found or inaccessible. Otherwise, inaccessible
     *  <code>Intersections</code> may be omitted from the list.
     *
     *  @param  pathId a path <code>Id</code> 
     *  @param  crossingPathId another path <code>Id</code> 
     *  @return the returned <code> ntersection</code> list 
     *  @throws org.osid.NullArgumentException <code>pathId</code> or
     *          <code>crossingPathId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionList getIntersectionsForPaths(org.osid.id.Id pathId, 
                                                                           org.osid.id.Id crossingPathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.path.Intersection> ret = new java.util.ArrayList<>();

        try (org.osid.mapping.path.IntersectionList intersections = getIntersectionsForPath(pathId)) {
            while (intersections.hasNext()) {
                org.osid.mapping.path.Intersection intersection = intersections.getNextIntersection();
                try (org.osid.id.IdList ids = intersection.getPathIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(crossingPathId)) {
                            ret.add(intersection);
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.path.intersection.LinkedIntersectionList(ret));
    }


    /**
     *  Gets all <code>Intersections</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  intersections or an error results. Otherwise, the returned list
     *  may contain only those intersections that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Intersections</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.mapping.path.IntersectionList getIntersections()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the intersection list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of intersections
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.mapping.path.IntersectionList filterIntersectionsOnViews(org.osid.mapping.path.IntersectionList list)
        throws org.osid.OperationFailedException {

        org.osid.mapping.path.IntersectionList ret = list;

        return (ret);
    }
}
