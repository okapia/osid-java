//
// AbstractFloorLookupSession.java
//
//    A starter implementation framework for providing a Floor
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Floor lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getFloors(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractFloorLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.FloorLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();
    

    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this session.
     *
     *  @return the <code>Campus</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Floor</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFloors() {
        return (true);
    }


    /**
     *  A complete view of the <code>Floor</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFloorView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Floor</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFloorView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include floors in campuses which are children of
     *  this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only floors whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveFloorView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All floors of any effective dates are returned by all methods
     *  in this session.
     */

    @OSID @Override
    public void useAnyEffectiveFloorView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Floor</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Floor</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Floor</code> and retained for
     *  compatibility.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  floorId <code>Id</code> of the
     *          <code>Floor</code>
     *  @return the floor
     *  @throws org.osid.NotFoundException <code>floorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>floorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Floor getFloor(org.osid.id.Id floorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.FloorList floors = getFloors()) {
            while (floors.hasNext()) {
                org.osid.room.Floor floor = floors.getNextFloor();
                if (floor.getId().equals(floorId)) {
                    return (floor);
                }
            }
        } 

        throw new org.osid.NotFoundException(floorId + " not found");
    }


    /**
     *  Gets a <code>FloorList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the floors
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Floors</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getFloors()</code>.
     *
     *  @param floorIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>floorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByIds(org.osid.id.IdList floorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.Floor> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = floorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getFloor(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("floor " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.floor.LinkedFloorList(ret));
    }


    /**
     *  Gets a <code>FloorList</code> corresponding to the given floor
     *  genus <code>Type</code> which does not include floors of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known floors
     *  or an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getFloors()</code>.
     *
     *  @param floorGenusType a floor genus type
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByGenusType(org.osid.type.Type floorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.floor.FloorGenusFilterList(getFloors(), floorGenusType));
    }


    /**
     *  Gets a <code>FloorList</code> corresponding to the given floor
     *  genus <code>Type</code> and include any additional floors with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known floors
     *  or an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFloors()</code>.
     *
     *  @param floorGenusType a floor genus type
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByParentGenusType(org.osid.type.Type floorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFloorsByGenusType(floorGenusType));
    }


    /**
     *  Gets a <code>FloorList</code> containing the given floor
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known floors
     *  or an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFloors()</code>.
     *
     *  @param  floorRecordType a floor record type 
     *  @return the returned <code>Floor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByRecordType(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.floor.FloorRecordFilterList(getFloors(), floorRecordType));
    }


    /**
     *  Gets a <code>FloorList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known floors
     *  or an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session.
     *  
     *  In active mode, floors are returned that are currently
     *  active. In any status mode, active and inactive floors are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Floor</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.FloorList getFloorsOnDate(org.osid.calendaring.DateTime from, 
                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.floor.TemporalFloorFilterList(getFloors(), from, to));
    }
        

    /**
     *  Gets a list of all floors corresponding to a building
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known floors or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  floors that are accessible through this session. 
     *  
     *  In effective mode, floors are returned that are currently effective. 
     *  In any effective mode, effective floors and those currently expired 
     *  are returned. 
     *
     *  @param  buildingId the <code>Id</code> of the building 
     *  @return the returned <code>FloorList</code> 
     *  @throws org.osid.NullArgumentException <code>buildingId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.floor.FloorFilterList(new BuildingFilter(buildingId), getFloors()));
    }        


    /**
     *  Gets a list of all floors corresponding to a building <code> Id 
     *  </code> and effective during the entire given date range inclusive but 
     *  not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known floors or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  floors that are accessible through this session. 
     *  
     *  In effective mode, floors are returned that are currently effective. 
     *  In any effective mode, effective floors and those currently expired 
     *  are returned. 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> FloorList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> buildingId, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                              org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.floor.TemporalFloorFilterList(getFloorsForBuilding(buildingId), from, to));
    }


    /**
     *  Gets a <code>FloorList</code> containing of the given
     *  floor number.
     *  
     *  In plenary mode, the returned list contains all known
     *  floors or an error results. Otherwise, the returned list
     *  may contain only those floors that are accessible through
     *  this session.
     *  
     *  In effective mode, floors are returned that are currently
     *  effective. In any effective mode, effective floors and
     *  those currently expired are returned.
     *
     *  @param  number a floor number 
     *  @return the returned <code>Floor</code> list 
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloorsByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.floor.FloorFilterList(new NumberFilter(number), getFloors()));
    }


    /**
     *  Gets all <code>Floors</code>.
     *
     *  In plenary mode, the returned list contains all known floors
     *  or an error results. Otherwise, the returned list may contain
     *  only those floors that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, floors are returned that are currently
     *  effective.  In any effective mode, effective floors and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Floors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.room.FloorList getFloors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the floor list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of floors
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.room.FloorList filterFloorsOnViews(org.osid.room.FloorList list)
        throws org.osid.OperationFailedException {

        org.osid.room.FloorList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.room.floor.EffectiveFloorFilterList(ret);
        }

        return (ret);
    }


    public static class BuildingFilter
        implements net.okapia.osid.jamocha.inline.filter.room.floor.FloorFilter {
         
        private final org.osid.id.Id buildingId;
         
         
        /**
         *  Constructs a new <code>BuildingFilter</code>.
         *
         *  @param buildingId the building to filter
         *  @throws org.osid.NullArgumentException
         *          <code>buildingId</code> is <code>null</code>
         */
        
        public BuildingFilter(org.osid.id.Id buildingId) {
            nullarg(buildingId, "building Id");
            this.buildingId = buildingId;
            return;
        }

         
        /**
         *  Used by the FloorFilterList to filter the 
         *  floor list based on building.
         *
         *  @param floor the floor
         *  @return <code>true</code> to pass the floor,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.Floor floor) {
            return (floor.getBuildingId().equals(this.buildingId));
        }
    }


    public static class NumberFilter
        implements net.okapia.osid.jamocha.inline.filter.room.floor.FloorFilter {
         
        private final String number;
         
         
        /**
         *  Constructs a new <code>NumberFilter</code>.
         *
         *  @param number the number to filter
         *  @throws org.osid.NullArgumentException
         *          <code>number</code> is <code>null</code>
         */
        
        public NumberFilter(String number) {
            nullarg(number, "number");
            this.number = number;
            return;
        }

         
        /**
         *  Used by the FloorFilterList to filter the 
         *  floor list based on number.
         *
         *  @param floor the floor
         *  @return <code>true</code> to pass the floor,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.Floor floor) {
            return (floor.getNumber().equals(this.number));
        }
    }    
}
