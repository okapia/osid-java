//
// AbstractIndexedMapSettingLookupSession.java
//
//    A simple framework for providing a Setting lookup service
//    backed by a fixed collection of settings with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Setting lookup service backed by a
 *  fixed collection of settings. The settings are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some settings may be compatible
 *  with more types than are indicated through these setting
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Settings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSettingLookupSession
    extends AbstractMapSettingLookupSession
    implements org.osid.control.SettingLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.Setting> settingsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Setting>());
    private final MultiMap<org.osid.type.Type, org.osid.control.Setting> settingsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Setting>());


    /**
     *  Makes a <code>Setting</code> available in this session.
     *
     *  @param  setting a setting
     *  @throws org.osid.NullArgumentException <code>setting<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSetting(org.osid.control.Setting setting) {
        super.putSetting(setting);

        this.settingsByGenus.put(setting.getGenusType(), setting);
        
        try (org.osid.type.TypeList types = setting.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.settingsByRecord.put(types.getNextType(), setting);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of settings available in this session.
     *
     *  @param  settings an array of settings
     *  @throws org.osid.NullArgumentException <code>settings<code>
     *          is <code>null</code>
     */

    @Override
    protected void putSettings(org.osid.control.Setting[] settings) {
        for (org.osid.control.Setting setting : settings) {
            putSetting(setting);
        }

        return;
    }


    /**
     *  Makes a collection of settings available in this session.
     *
     *  @param  settings a collection of settings
     *  @throws org.osid.NullArgumentException <code>settings<code>
     *          is <code>null</code>
     */

    @Override
    protected void putSettings(java.util.Collection<? extends org.osid.control.Setting> settings) {
        for (org.osid.control.Setting setting : settings) {
            putSetting(setting);
        }

        return;
    }


    /**
     *  Removes a setting from this session.
     *
     *  @param settingId the <code>Id</code> of the setting
     *  @throws org.osid.NullArgumentException <code>settingId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSetting(org.osid.id.Id settingId) {
        org.osid.control.Setting setting;
        try {
            setting = getSetting(settingId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.settingsByGenus.remove(setting.getGenusType());

        try (org.osid.type.TypeList types = setting.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.settingsByRecord.remove(types.getNextType(), setting);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSetting(settingId);
        return;
    }


    /**
     *  Gets a <code>SettingList</code> corresponding to the given
     *  setting genus <code>Type</code> which does not include
     *  settings of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known settings or an error results. Otherwise,
     *  the returned list may contain only those settings that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  settingGenusType a setting genus type 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByGenusType(org.osid.type.Type settingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.setting.ArraySettingList(this.settingsByGenus.get(settingGenusType)));
    }


    /**
     *  Gets a <code>SettingList</code> containing the given
     *  setting record <code>Type</code>. In plenary mode, the
     *  returned list contains all known settings or an error
     *  results. Otherwise, the returned list may contain only those
     *  settings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  settingRecordType a setting record type 
     *  @return the returned <code>setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByRecordType(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.setting.ArraySettingList(this.settingsByRecord.get(settingRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.settingsByGenus.clear();
        this.settingsByRecord.clear();

        super.close();

        return;
    }
}
