//
// AbstractFoundryQuery.java
//
//     A template for making a Foundry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.foundry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for foundries.
 */

public abstract class AbstractFoundryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.resourcing.FoundryQuery {

    private final java.util.Collection<org.osid.resourcing.records.FoundryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the effort <code> Id </code> for this query to match foundries 
     *  containing jobs. 
     *
     *  @param  jobId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchJobId(org.osid.id.Id jobId, boolean match) {
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getJobQuery() {
        throw new org.osid.UnimplementedException("supportsJobQuery() is false");
    }


    /**
     *  Matches foundries that have any job. 
     *
     *  @param  match <code> true </code> to match foundries with any job, 
     *          <code> false </code> to match foundries with no job 
     */

    @OSID @Override
    public void matchAnyJob(boolean match) {
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobTerms() {
        return;
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches foundries that have any work. 
     *
     *  @param  match <code> true </code> to match foundries with any work, 
     *          <code> false </code> to match foundries with no job 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        return;
    }


    /**
     *  Sets the availability <code> Id </code> for this query to match 
     *  foundries that have a related availability. 
     *
     *  @param  availabilityId a availability <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAvailabilityId(org.osid.id.Id availabilityId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAvailabilityIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsAvailabilityQuery() is false");
    }


    /**
     *  Matches foundries that have any availability. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          availability, <code> false </code> to match foundries with no 
     *          availability 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        return;
    }


    /**
     *  Sets the commission <code> Id </code> for this query. 
     *
     *  @param  commissionId the commission <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commissionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommissionId(org.osid.id.Id commissionId, boolean match) {
        return;
    }


    /**
     *  Clears the commission <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCommissionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CommissionQuery </code> is available. 
     *
     *  @return <code> true </code> if a commission query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commission. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commission query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuery getCommissionQuery() {
        throw new org.osid.UnimplementedException("supportsCommissionQuery() is false");
    }


    /**
     *  Matches foundries that have any commission. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          commission, <code> false </code> to match foundries with no 
     *          commission 
     */

    @OSID @Override
    public void matchAnyCommission(boolean match) {
        return;
    }


    /**
     *  Clears the commission query terms. 
     */

    @OSID @Override
    public void clearCommissionTerms() {
        return;
    }


    /**
     *  Sets the effort <code> Id </code> for this query. 
     *
     *  @param  effortId the effort <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> effortId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEffortId(org.osid.id.Id effortId, boolean match) {
        return;
    }


    /**
     *  Clears the effort <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEffortIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EffortQuery </code> is available. 
     *
     *  @return <code> true </code> if an effort query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortQuery() {
        return (false);
    }


    /**
     *  Gets the query for an effort. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the effort query 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuery getEffortQuery() {
        throw new org.osid.UnimplementedException("supportsEffortQuery() is false");
    }


    /**
     *  Matches foundries with any effort. 
     *
     *  @param  match <code> true </code> to match foundries with any effort, 
     *          <code> false </code> to match foundries with no effort 
     */

    @OSID @Override
    public void matchAnyEffort(boolean match) {
        return;
    }


    /**
     *  Clears the effort query terms. 
     */

    @OSID @Override
    public void clearEffortTerms() {
        return;
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match foundries 
     *  that have the specified foundry as an ancestor. 
     *
     *  @param  foundryId a foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorFoundryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getAncestorFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorFoundryQuery() is false");
    }


    /**
     *  Matches foundries with any ancestor. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          ancestor, <code> false </code> to match root foundries 
     */

    @OSID @Override
    public void matchAnyAncestorFoundry(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor foundry query terms. 
     */

    @OSID @Override
    public void clearAncestorFoundryTerms() {
        return;
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match foundries 
     *  that have the specified foundry as a descendant. 
     *
     *  @param  foundryId a foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantFoundryId(org.osid.id.Id foundryId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantFoundryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getDescendantFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantFoundryQuery() is false");
    }


    /**
     *  Matches foundries with any descendant. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          descendant, <code> false </code> to match leaf foundries 
     */

    @OSID @Override
    public void matchAnyDescendantFoundry(boolean match) {
        return;
    }


    /**
     *  Clears the descendant foundry query terms. 
     */

    @OSID @Override
    public void clearDescendantFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given foundry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a foundry implementing the requested record.
     *
     *  @param foundryRecordType a foundry record type
     *  @return the foundry query record
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(foundryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.FoundryQueryRecord getFoundryQueryRecord(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.FoundryQueryRecord record : this.records) {
            if (record.implementsRecordType(foundryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(foundryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this foundry query. 
     *
     *  @param foundryQueryRecord foundry query record
     *  @param foundryRecordType foundry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFoundryQueryRecord(org.osid.resourcing.records.FoundryQueryRecord foundryQueryRecord, 
                                          org.osid.type.Type foundryRecordType) {

        addRecordType(foundryRecordType);
        nullarg(foundryQueryRecord, "foundry query record");
        this.records.add(foundryQueryRecord);        
        return;
    }
}
