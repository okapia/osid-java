//
// AbstractPayment.java
//
//     Defines a Payment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Payment</code>.
 */

public abstract class AbstractPayment
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.billing.payment.Payment {

    private org.osid.billing.payment.Payer payer;
    private org.osid.billing.Customer customer;
    private org.osid.calendaring.DateTime periodId;
    private org.osid.billing.Period period;
    private org.osid.calendaring.DateTime paymentDate;
    private org.osid.calendaring.DateTime processDate;
    private org.osid.financials.Currency amount;

    private final java.util.Collection<org.osid.billing.payment.records.PaymentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the payer <code> Id. </code> 
     *
     *  @return the payer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPayerId() {
        return (this.payer.getId());
    }


    /**
     *  Gets the payer. 
     *
     *  @return the payer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.payment.Payer getPayer()
        throws org.osid.OperationFailedException {

        return (this.payer);
    }


    /**
     *  Sets the payer.
     *
     *  @param payer a payer
     *  @throws org.osid.NullArgumentException <code>payer</code> is
     *          <code>null</code>
     */

    protected void setPayer(org.osid.billing.payment.Payer payer) {
        nullarg(payer, "payer");
        this.payer = payer;
        return;
    }


    /**
     *  Gets the customer <code> Id </code> for which this payment applies. 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.customer.getId());
    }


    /**
     *  Gets the customer for which this payment applies. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        return (this.customer);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    protected void setCustomer(org.osid.billing.Customer customer) {
        nullarg(customer, "customer");
        this.customer = customer;
        return;
    }


    /**
     *  Gets the billing period <code> Id </code> to which this payment 
     *  applies. 
     *
     *  @return the period <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPeriodId() {
        return (this.period.getId());
    }


    /**
     *  Gets the billing period to which this payment applied. 
     *
     *  @return the period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod()
        throws org.osid.OperationFailedException {

        return (this.period);
    }


    /**
     *  Sets the billing period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    protected void setPeriod(org.osid.billing.Period period) {
        nullarg(period, "billing period");
        this.period = period;
        return;
    }


    /**
     *  Gets the date the payment was made. 
     *
     *  @return the payment date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPaymentDate() {
        return (this.paymentDate);
    }


    /**
     *  Sets the payment date.
     *
     *  @param date a payment date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setPaymentDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "payment date");
        this.paymentDate = date;
        return;
    }


    /**
     *  Gets the date the payment was or will be processed. 
     *
     *  @return the process date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getProcessDate() {
        return (this.processDate);
    }


    /**
     *  Sets the process date.
     *
     *  @param date a process date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setProcessDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "processed date");
        this.processDate = date;
        return;
    }


    /**
     *  Gets the amount of this payment. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.amount);
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }


    /**
     *  Tests if this payment supports the given record
     *  <code>Type</code>.
     *
     *  @param  paymentRecordType a payment record type 
     *  @return <code>true</code> if the paymentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type paymentRecordType) {
        for (org.osid.billing.payment.records.PaymentRecord record : this.records) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Payment</code> record <code>Type</code>.
     *
     *  @param  paymentRecordType the payment record type 
     *  @return the payment record 
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(paymentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentRecord getPaymentRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PaymentRecord record : this.records) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param paymentRecord the payment record
     *  @param paymentRecordType payment record type
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecord</code> or
     *          <code>paymentRecordType</code> is <code>null</code>
     */
            
    protected void addPaymentRecord(org.osid.billing.payment.records.PaymentRecord paymentRecord, 
                                    org.osid.type.Type paymentRecordType) {

        nullarg(paymentRecord, "payment record");
        addRecordType(paymentRecordType);
        this.records.add(paymentRecord);
        
        return;
    }
}
