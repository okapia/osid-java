//
// AbstractTriggerLookupSession.java
//
//    A starter implementation framework for providing a Trigger
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Trigger
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getTriggers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractTriggerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.TriggerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();
    

    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Trigger</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTriggers() {
        return (true);
    }


    /**
     *  A complete view of the <code>Trigger</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTriggerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Trigger</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTriggerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include triggers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active triggers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTriggerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive triggers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTriggerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Trigger</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Trigger</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Trigger</code> and
     *  retained for compatibility.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @param  triggerId <code>Id</code> of the
     *          <code>Trigger</code>
     *  @return the trigger
     *  @throws org.osid.NotFoundException <code>triggerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>triggerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Trigger getTrigger(org.osid.id.Id triggerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.TriggerList triggers = getTriggers()) {
            while (triggers.hasNext()) {
                org.osid.control.Trigger trigger = triggers.getNextTrigger();
                if (trigger.getId().equals(triggerId)) {
                    return (trigger);
                }
            }
        } 

        throw new org.osid.NotFoundException(triggerId + " not found");
    }


    /**
     *  Gets a <code>TriggerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  triggers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Triggers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getTriggers()</code>.
     *
     *  @param  triggerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Trigger</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>triggerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByIds(org.osid.id.IdList triggerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.control.Trigger> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = triggerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getTrigger(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("trigger " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.control.trigger.LinkedTriggerList(ret));
    }


    /**
     *  Gets a <code>TriggerList</code> corresponding to the given
     *  trigger genus <code>Type</code> which does not include
     *  triggers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getTriggers()</code>.
     *
     *  @param  triggerGenusType a trigger genus type 
     *  @return the returned <code>Trigger</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByGenusType(org.osid.type.Type triggerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.trigger.TriggerGenusFilterList(getTriggers(), triggerGenusType));
    }


    /**
     *  Gets a <code>TriggerList</code> corresponding to the given
     *  trigger genus <code>Type</code> and include any additional
     *  triggers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTriggers()</code>.
     *
     *  @param  triggerGenusType a trigger genus type 
     *  @return the returned <code>Trigger</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByParentGenusType(org.osid.type.Type triggerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTriggersByGenusType(triggerGenusType));
    }


    /**
     *  Gets a <code>TriggerList</code> containing the given
     *  trigger record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTriggers()</code>.
     *
     *  @param  triggerRecordType a trigger record type 
     *  @return the returned <code>Trigger</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByRecordType(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.trigger.TriggerRecordFilterList(getTriggers(), triggerRecordType));
    }


    /**
     *  Gets a list of triggers for the given controller. <code> </code>
     *
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list may contain
     *  only those triggers that are accessible through this session.
     *
     *  In active mode, triggers are returned that are currently active. In
     *  any status mode, active and inactive triggers are returned.
     *
     *  @param  controllerId a controller <code> Id </code>
     *  @return the returned <code> Trigger </code> list
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersForController(org.osid.id.Id controllerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.trigger.TriggerFilterList(new ControllerFilter(controllerId), getTriggers()));
    }


    /**
     *  Gets all <code>Triggers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @return a list of <code>Triggers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.control.TriggerList getTriggers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the trigger list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of triggers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.control.TriggerList filterTriggersOnViews(org.osid.control.TriggerList list)
        throws org.osid.OperationFailedException {

        org.osid.control.TriggerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.control.trigger.ActiveTriggerFilterList(ret);
        }

        return (ret);
    }


    public static class ControllerFilter
        implements net.okapia.osid.jamocha.inline.filter.control.trigger.TriggerFilter {
         
        private final org.osid.id.Id controllerId;
         
         
        /**
         *  Constructs a new <code>ControllerFilter</code>.
         *
         *  @param controllerId the controller to filter
         *  @throws org.osid.NullArgumentException
         *          <code>controllerId</code> is <code>null</code>
         */
        
        public ControllerFilter(org.osid.id.Id controllerId) {
            nullarg(controllerId, "controller Id");
            this.controllerId = controllerId;
            return;
        }

         
        /**
         *  Used by the TriggerFilterList to filter the trigger list
         *  based on controller.
         *
         *  @param trigger the trigger
         *  @return <code>true</code> to pass the trigger,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.control.Trigger trigger) {
            return (trigger.getControllerId().equals(this.controllerId));
        }
    }
}
