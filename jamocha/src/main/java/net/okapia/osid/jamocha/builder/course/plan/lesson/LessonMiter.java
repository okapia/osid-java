//
// LessonMiter.java
//
//     Defines a Lesson miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.lesson;


/**
 *  Defines a <code>Lesson</code> miter for use with the builders.
 */

public interface LessonMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.plan.Lesson {


    /**
     *  Sets the plan.
     *
     *  @param plan a plan
     *  @throws org.osid.NullArgumentException <code>plan</code> is
     *          <code>null</code>
     */

    public void setPlan(org.osid.course.plan.Plan plan);


    /**
     *  Sets the docet.
     *
     *  @param docet a docet
     *  @throws org.osid.NullArgumentException <code>docet</code> is
     *          <code>null</code>
     */

    public void setDocet(org.osid.course.syllabus.Docet docet);


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public void addActivity(org.osid.course.Activity activity);


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    public void setActivities(java.util.Collection<org.osid.course.Activity> activities);


    /**
     *  Sets the planned start time.
     *
     *  @param time a planned start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setPlannedStartTime(org.osid.calendaring.Duration time);


    /**
     *  Sets the actual start time.
     *
     *  @param time an actual start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setActualStartTime(org.osid.calendaring.Duration time);


    /**
     *  Sets the actual starting activity.
     *
     *  @param activity an actual starting activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public void setActualStartingActivity(org.osid.course.Activity activity);


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code>true</code> if this lesson is complete,
     *         <code>false</code> if not completed
     */

    public void setComplete(boolean complete);


    /**
     *  Sets the skipped flag.
     *
     *  @param skipped <code>true</code> if this lesson is skipped,
     *         <code>false</code> if not completed
     */

    public void setSkipped(boolean skipped);


    /**
     *  Sets the actual end time.
     *
     *  @param time an actual end time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setActualEndTime(org.osid.calendaring.Duration time);


    /**
     *  Sets the actual ending activity.
     *
     *  @param activity an actual ending activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public void setActualEndingActivity(org.osid.course.Activity activity);


    /**
     *  Sets the actual time spent.
     *
     *  @param timeSpent an actual time spent
     *  @throws org.osid.NullArgumentException <code>timeSpent</code>
     *          is <code>null</code>
     */

    public void setActualTimeSpent(org.osid.calendaring.Duration timeSpent);


    /**
     *  Adds a Lesson record.
     *
     *  @param record a lesson record
     *  @param recordType the type of lesson record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addLessonRecord(org.osid.course.plan.records.LessonRecord record, org.osid.type.Type recordType);
}       


