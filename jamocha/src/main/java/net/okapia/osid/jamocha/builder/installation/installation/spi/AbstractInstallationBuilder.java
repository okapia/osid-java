//
// AbstractInstallation.java
//
//     Defines an Installation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.installation.spi;


/**
 *  Defines an <code>Installation</code> builder.
 */

public abstract class AbstractInstallationBuilder<T extends AbstractInstallationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.installation.installation.InstallationMiter installation;


    /**
     *  Constructs a new <code>AbstractInstallationBuilder</code>.
     *
     *  @param installation the installation to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractInstallationBuilder(net.okapia.osid.jamocha.builder.installation.installation.InstallationMiter installation) {
        super(installation);
        this.installation = installation;
        return;
    }


    /**
     *  Builds the installation.
     *
     *  @return the new installation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.installation.Installation build() {
        (new net.okapia.osid.jamocha.builder.validator.installation.installation.InstallationValidator(getValidations())).validate(this.installation);
        return (new net.okapia.osid.jamocha.builder.installation.installation.ImmutableInstallation(this.installation));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the installation miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.installation.installation.InstallationMiter getMiter() {
        return (this.installation);
    }


    /**
     *  Sets the site.
     *
     *  @param site a site
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>site</code> is
     *          <code>null</code>
     */

    public T site(org.osid.installation.Site site) {
        getMiter().setSite(site);
        return (self());
    }


    /**
     *  Sets the package.
     *
     *  @param pkg a package
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>pkg</code> is
     *          <code>null</code>
     */

    public T pkg(org.osid.installation.Package pkg) {
        getMiter().setPackage(pkg);
        return (self());
    }


    /**
     *  Sets the depot.
     *
     *  @param depot a depot
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>depot</code> is
     *          <code>null</code>
     */

    public T depot(org.osid.installation.Depot depot) {
        getMiter().setDepot(depot);
        return (self());
    }


    /**
     *  Sets the install date.
     *
     *  @param date an install date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T installDate(org.osid.calendaring.DateTime date) {
        getMiter().setInstallDate(date);
        return (self());
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Sets the last check date.
     *
     *  @param date a last check date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T lastCheckDate(org.osid.calendaring.DateTime date) {
        getMiter().setLastCheckDate(date);
        return (self());
    }


    /**
     *  Adds an Installation record.
     *
     *  @param record an installation record
     *  @param recordType the type of installation record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.installation.records.InstallationRecord record, org.osid.type.Type recordType) {
        getMiter().addInstallationRecord(record, recordType);
        return (self());
    }
}       


