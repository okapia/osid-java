//
// AbstractImmutableProgramEntry.java
//
//     Wraps a mutable ProgramEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ProgramEntry</code> to hide modifiers. This
 *  wrapper provides an immutized ProgramEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying programEntry whose state changes are visible.
 */

public abstract class AbstractImmutableProgramEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.chronicle.ProgramEntry {

    private final org.osid.course.chronicle.ProgramEntry programEntry;


    /**
     *  Constructs a new <code>AbstractImmutableProgramEntry</code>.
     *
     *  @param programEntry the program entry to immutablize
     *  @throws org.osid.NullArgumentException <code>programEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProgramEntry(org.osid.course.chronicle.ProgramEntry programEntry) {
        super(programEntry);
        this.programEntry = programEntry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.programEntry.getStudentId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.programEntry.getStudent());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.programEntry.getProgramId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.programEntry.getProgram());
    }


    /**
     *  Gets the date in which the student was admitted. 
     *
     *  @return the admission date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getAdmissionDate() {
        return (this.programEntry.getAdmissionDate());
    }


    /**
     *  Tests if the program has been completed. If this entry is for summary 
     *  information an a past term, <code> isComplete() </code> may be <code> 
     *  true. </code> 
     *
     *  @return <code> true </code> if the program has been completed, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.programEntry.isComplete());
    }


    /**
     *  Tests if this entry is a progression entry applying to a single term. 
     *  A program entry may provide summary information for the entire 
     *  duration or for a single term. If <code> isForTerm() </code> is <code> 
     *  false, </code> this entry applies to the entire enrollement period. 
     *
     *  @return <code> true </code> if the program has a term, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isForTerm() {
        return (this.programEntry.isForTerm());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term. </code> 
     *
     *  @return the term <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isForTerm() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.programEntry.getTermId());
    }


    /**
     *  Gets the <code> Term. </code> 
     *
     *  @return the term 
     *  @throws org.osid.IllegalStateException <code> isForTerm() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.programEntry.getTerm());
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreditScaleId() {
        return (this.programEntry.getCreditScaleId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getCreditScale()
        throws org.osid.OperationFailedException {

        return (this.programEntry.getCreditScale());
    }


    /**
     *  Gets the number of credits earned in this program or earned within the 
     *  included term. 
     *
     *  @return the credits earned 
     */

    @OSID @Override
    public java.math.BigDecimal getCreditsEarned() {
        return (this.programEntry.getCreditsEarned());
    }


    /**
     *  Tests if a cumulative GPA in this program of the GPA for the included 
     *  term. 
     *
     *  @return <code> true </code> if a GPA is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasGPA() {
        return (this.programEntry.hasGPA());
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasGPA() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGPAScaleId() {
        return (this.programEntry.getGPAScaleId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> hasGPA() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGPAScale()
        throws org.osid.OperationFailedException {

        return (this.programEntry.getGPAScale());
    }


    /**
     *  Gets the cumulative GPA in this porgram or within the included term. 
     *
     *  @return the GPA 
     *  @throws org.osid.IllegalStateException <code> hasGPA() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getGPA() {
        return (this.programEntry.getGPA());
    }


    /**
     *  Tests if <code> Enrollments </code> are available. 
     *
     *  @return <code> true </code> if enrollments are available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasEnrollments() {
        return (this.programEntry.hasEnrollments());
    }


    /**
     *  Gets the <code> Ids </code> of the <code> Enrollments. </code> 
     *
     *  @return the enrollment <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasEnrollments() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getEnrollmentIds() {
        return (this.programEntry.getEnrollmentIds());
    }


    /**
     *  Gets the <code> Enrollments. </code> 
     *
     *  @return the enrollments 
     *  @throws org.osid.IllegalStateException <code> hasEnrollments() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollments()
        throws org.osid.OperationFailedException {

        return (this.programEntry.getEnrollments());
    }


    /**
     *  Gets the program entry record corresponding to the given <code> 
     *  ProgramEntry </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  programEntryRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(programEntryRecordType) </code> is <code> true </code> . 
     *
     *  @param  programEntryRecordType the type of program entry record to 
     *          retrieve 
     *  @return the program entry record 
     *  @throws org.osid.NullArgumentException <code> programEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(programEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntryRecord getProgramEntryRecord(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.programEntry.getProgramEntryRecord(programEntryRecordType));
    }
}

