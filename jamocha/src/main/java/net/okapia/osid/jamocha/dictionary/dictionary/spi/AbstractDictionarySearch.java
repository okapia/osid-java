//
// AbstractDictionarySearch.java
//
//     A template for making a Dictionary Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.dictionary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing dictionary searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDictionarySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.dictionary.DictionarySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.dictionary.records.DictionarySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.dictionary.DictionarySearchOrder dictionarySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of dictionaries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  dictionaryIds list of dictionaries
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDictionaries(org.osid.id.IdList dictionaryIds) {
        while (dictionaryIds.hasNext()) {
            try {
                this.ids.add(dictionaryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDictionaries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of dictionary Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDictionaryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  dictionarySearchOrder dictionary search order 
     *  @throws org.osid.NullArgumentException
     *          <code>dictionarySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>dictionarySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDictionaryResults(org.osid.dictionary.DictionarySearchOrder dictionarySearchOrder) {
	this.dictionarySearchOrder = dictionarySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.dictionary.DictionarySearchOrder getDictionarySearchOrder() {
	return (this.dictionarySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given dictionary search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a dictionary implementing the requested record.
     *
     *  @param dictionarySearchRecordType a dictionary search record
     *         type
     *  @return the dictionary search record
     *  @throws org.osid.NullArgumentException
     *          <code>dictionarySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dictionarySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.DictionarySearchRecord getDictionarySearchRecord(org.osid.type.Type dictionarySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.dictionary.records.DictionarySearchRecord record : this.records) {
            if (record.implementsRecordType(dictionarySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dictionarySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this dictionary search. 
     *
     *  @param dictionarySearchRecord dictionary search record
     *  @param dictionarySearchRecordType dictionary search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDictionarySearchRecord(org.osid.dictionary.records.DictionarySearchRecord dictionarySearchRecord, 
                                           org.osid.type.Type dictionarySearchRecordType) {

        addRecordType(dictionarySearchRecordType);
        this.records.add(dictionarySearchRecord);        
        return;
    }
}
