//
// AbstractMutableCanonicalUnit.java
//
//     Defines a mutable CanonicalUnit.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>CanonicalUnit</code>.
 */

public abstract class AbstractMutableCanonicalUnit
    extends net.okapia.osid.jamocha.offering.canonicalunit.spi.AbstractCanonicalUnit
    implements org.osid.offering.CanonicalUnit,
               net.okapia.osid.jamocha.builder.offering.canonicalunit.CanonicalUnitMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this canonical unit. 
     *
     *  @param record canonical unit record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addCanonicalUnitRecord(org.osid.offering.records.CanonicalUnitRecord record, org.osid.type.Type recordType) {
        super.addCanonicalUnitRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this canonical unit. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if disabled
     */
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (isDisabled()) {
            setDisabled(false);
        }

        return;
    }


    /**
     *  Disables this canonical unit. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *          disabled, <code> false </code> otherwise
     */
    
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);

        if (isEnabled()) {
            setEnabled(false);
        }

        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this canonical unit.
     *
     *  @param displayName the name for this canonical unit
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this canonical unit.
     *
     *  @param description the description of this canonical unit
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    @Override
    public void setTitle(org.osid.locale.DisplayText title) {
        super.setTitle(title);
        return;
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    @Override
    public void setCode(String code) {
        super.setCode(code);
        return;
    }


    /**
     *  Adds an offered cyclic time period.
     *
     *  @param offeredCyclicTimePeriod an offered cyclic time period
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriod</code> is <code>null</code>
     */

    @Override
    public void addOfferedCyclicTimePeriod(org.osid.calendaring.cycle.CyclicTimePeriod offeredCyclicTimePeriod) {
        super.addOfferedCyclicTimePeriod(offeredCyclicTimePeriod);
        return;
    }


    /**
     *  Sets all the offered cyclic time periods.
     *
     *  @param offeredCyclicTimePeriods a collection of offered cyclic time periods
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriods</code> is <code>null</code>
     */

    @Override
    public void setOfferedCyclicTimePeriods(java.util.Collection<org.osid.calendaring.cycle.CyclicTimePeriod> offeredCyclicTimePeriods) {
        super.setOfferedCyclicTimePeriods(offeredCyclicTimePeriods);
        return;
    }


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    @Override
    public void addResultOption(org.osid.grading.GradeSystem resultOption) {
        super.addResultOption(resultOption);
        return;
    }


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    @Override
    public void setResultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions) {
        super.setResultOptions(resultOptions);
        return;
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    @Override
    public void addSponsor(org.osid.resource.Resource sponsor) {
        super.addSponsor(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    @Override
    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        super.setSponsors(sponsors);
        return;
    }


}

