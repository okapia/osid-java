//
// AbstractInstallationContent.java
//
//     Defines an InstallationContent.
//
//
// Tom Coppeto
// Okapia
// 8 May 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installationcontent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>InstallationContent</code>.
 */

public abstract class AbstractInstallationContent
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.installation.InstallationContent {

    private org.osid.installation.Package pkg;
    private long dataLength = -1;
    private java.nio.ByteBuffer data;

    private final java.util.Collection<org.osid.installation.records.InstallationContentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Package Id </code> corresponding to this content. 
     *
     *  @return the package <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPackageId() {
        return (this.pkg.getId());
    }


    /**
     *  Gets the <code> Package </code> corresponding to this content. 
     *
     *  @return the package 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage() {
        return (this.pkg);
    }


    /**
     *  Sets the package.
     *
     *  @param pkg a package
     *  @throws org.osid.NullArgumentException
     *          <code>pkg</code> is <code>null</code>
     */

    protected void setPackage(org.osid.installation.Package pkg) {
        this.pkg = pkg;
        return;
    }


    /**
     *  Tests if a data length is available. 
     *
     *  @return <code> true </code> if a length is available for this content, 
     *          <code> false </code> otherwise. 
     */

    @OSID @Override
    public boolean hasDataLength() {
        if (this.dataLength >= 0) {
            return (true);
        } else if (this.data != null) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the length of the data represented by this content in bytes. 
     *
     *  @return the length of the data stream 
     *  @throws org.osid.IllegalStateException <code> hasDataLength() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public long getDataLength() {
        if (!hasDataLength()) {
            throw new org.osid.IllegalStateException("hasDataLength() is false");
        }

        if (this.dataLength >= 0) {
            return (this.dataLength);
        } else {
            return (this.data.limit());
        }
    }


    /**
     *  Sets the data length.
     *
     *  @param length a data length
     *  @throws org.osid.InvalidArgumentException <code>length</code>
     *          is negative
     */

    protected void setDataLength(long length) {
        cardinalarg(length, "data length");
        this.dataLength = length;
        return;
    }


    /**
     *  Gets the content data. 
     *
     *  @return the length of the content data 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.transport.DataInputStream getData()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.torrefacto.transport.ByteBufferDataInputStream(this.data));
    }


    /**
     *  Sets the content data.
     *
     *  @param data a flipped byte buffer
     *  @throws org.osid.NullArgumentException <code>data</code> is
     *          <code>null</code>
     */

    protected void setData(java.nio.ByteBuffer data) {
        nullarg(data, "data");
        this.data = data;
        return;
    }


    /**
     *  Tests if this installationContent supports the given record
     *  <code>Type</code>.
     *
     *  @param  installationContentRecordType an installation content record type 
     *  @return <code>true</code> if the installationContentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>installationContentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type installationContentRecordType) {
        for (org.osid.installation.records.InstallationContentRecord record : this.records) {
            if (record.implementsRecordType(installationContentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  installationContentRecordType the installation content record type 
     *  @return the installation content record 
     *  @throws org.osid.NullArgumentException
     *          <code>installationContentRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationContentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationContentRecord getInstallationContentRecord(org.osid.type.Type installationContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationContentRecord record : this.records) {
            if (record.implementsRecordType(installationContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationContentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this installation content. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param installationContentRecord the installation content record
     *  @param installationContentRecordType installation content record type
     *  @throws org.osid.NullArgumentException
     *          <code>installationContentRecord</code> or
     *          <code>installationContentRecordTypeinstallationContent</code> is
     *          <code>null</code>
     */
            
    protected void addInstallationContentRecord(org.osid.installation.records.InstallationContentRecord installationContentRecord, 
                                     org.osid.type.Type installationContentRecordType) {

        addRecordType(installationContentRecordType);
        this.records.add(installationContentRecord);
        
        return;
    }
}
