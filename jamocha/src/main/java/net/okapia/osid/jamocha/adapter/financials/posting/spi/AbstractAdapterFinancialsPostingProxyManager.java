//
// AbstractFinancialsPostingProxyManager.java
//
//     An adapter for a FinancialsPostingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FinancialsPostingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFinancialsPostingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.financials.posting.FinancialsPostingProxyManager>
    implements org.osid.financials.posting.FinancialsPostingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsPostingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFinancialsPostingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsPostingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFinancialsPostingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up posts is supported. 
     *
     *  @return <code> true </code> if post lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostLookup() {
        return (getAdapteeManager().supportsPostLookup());
    }


    /**
     *  Tests if querying posts is supported. 
     *
     *  @return <code> true </code> if post query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (getAdapteeManager().supportsPostQuery());
    }


    /**
     *  Tests if searching posts is supported. 
     *
     *  @return <code> true </code> if post search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSearch() {
        return (getAdapteeManager().supportsPostSearch());
    }


    /**
     *  Tests if post <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if post administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostAdmin() {
        return (getAdapteeManager().supportsPostAdmin());
    }


    /**
     *  Tests if a post <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if post notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostNotification() {
        return (getAdapteeManager().supportsPostNotification());
    }


    /**
     *  Tests if a post cataloging service is supported. 
     *
     *  @return <code> true </code> if post catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostBusiness() {
        return (getAdapteeManager().supportsPostBusiness());
    }


    /**
     *  Tests if a post cataloging service is supported. A cataloging service 
     *  maps posts to catalogs. 
     *
     *  @return <code> true </code> if post cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostBusinessAssignment() {
        return (getAdapteeManager().supportsPostBusinessAssignment());
    }


    /**
     *  Tests if a post smart business session is available. 
     *
     *  @return <code> true </code> if a post smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSmartBusiness() {
        return (getAdapteeManager().supportsPostSmartBusiness());
    }


    /**
     *  Tests if looking up post entries is supported. 
     *
     *  @return <code> true </code> if post entry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryLookup() {
        return (getAdapteeManager().supportsPostEntryLookup());
    }


    /**
     *  Tests if querying post entries is supported. 
     *
     *  @return <code> true </code> if post entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryQuery() {
        return (getAdapteeManager().supportsPostEntryQuery());
    }


    /**
     *  Tests if searching post entries is supported. 
     *
     *  @return <code> true </code> if post entry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntrySearch() {
        return (getAdapteeManager().supportsPostEntrySearch());
    }


    /**
     *  Tests if post entry administrative service is supported. 
     *
     *  @return <code> true </code> if post entry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryAdmin() {
        return (getAdapteeManager().supportsPostEntryAdmin());
    }


    /**
     *  Tests if an entry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if post entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryNotification() {
        return (getAdapteeManager().supportsPostEntryNotification());
    }


    /**
     *  Tests if an post entry cataloging service is supported. 
     *
     *  @return <code> true </code> if post entry catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryBusiness() {
        return (getAdapteeManager().supportsPostEntryBusiness());
    }


    /**
     *  Tests if an post entry cataloging service is supported. A cataloging 
     *  service maps post entries to catalogs. 
     *
     *  @return <code> true </code> if post entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryBusinessAssignment() {
        return (getAdapteeManager().supportsPostEntryBusinessAssignment());
    }


    /**
     *  Tests if an post entry smart business session is available. 
     *
     *  @return <code> true </code> if an post entry smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntrySmartBusiness() {
        return (getAdapteeManager().supportsPostEntrySmartBusiness());
    }


    /**
     *  Tests if a posting batch service is available. 
     *
     *  @return <code> true </code> if a posting batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsPostingBatch() {
        return (getAdapteeManager().supportsFinancialsPostingBatch());
    }


    /**
     *  Gets the supported <code> Post </code> record types. 
     *
     *  @return a list containing the supported <code> Post </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostRecordTypes() {
        return (getAdapteeManager().getPostRecordTypes());
    }


    /**
     *  Tests if the given <code> Post </code> record type is supported. 
     *
     *  @param  postRecordType a <code> Type </code> indicating an <code> Post 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostRecordType(org.osid.type.Type postRecordType) {
        return (getAdapteeManager().supportsPostRecordType(postRecordType));
    }


    /**
     *  Gets the supported <code> Post </code> search record types. 
     *
     *  @return a list containing the supported <code> Post </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostSearchRecordTypes() {
        return (getAdapteeManager().getPostSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Post </code> search record type is 
     *  supported. 
     *
     *  @param  postSearchRecordType a <code> Type </code> indicating an 
     *          <code> Post </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        return (getAdapteeManager().supportsPostSearchRecordType(postSearchRecordType));
    }


    /**
     *  Gets the supported <code> PostEntry </code> record types. 
     *
     *  @return a list containing the supported <code> PostEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostEntryRecordTypes() {
        return (getAdapteeManager().getPostEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> PostEntry </code> record type is supported. 
     *
     *  @param  postEntryRecordType a <code> Type </code> indicating an <code> 
     *          PostEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostEntryRecordType(org.osid.type.Type postEntryRecordType) {
        return (getAdapteeManager().supportsPostEntryRecordType(postEntryRecordType));
    }


    /**
     *  Gets the supported <code> PostEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> PostEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostEntrySearchRecordTypes() {
        return (getAdapteeManager().getPostEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> PostEntry </code> search record type is 
     *  supported. 
     *
     *  @param  postEntrySearchRecordType a <code> Type </code> indicating an 
     *          <code> PostEntry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          postEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostEntrySearchRecordType(org.osid.type.Type postEntrySearchRecordType) {
        return (getAdapteeManager().supportsPostEntrySearchRecordType(postEntrySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostLookupSession getPostLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostLookupSession getPostLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostLookupSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuerySession getPostQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuerySession getPostQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostQuerySessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchSession getPostSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchSession getPostSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostSearchSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostAdminSession getPostAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostAdminSession getPostAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostAdminSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service. 
     *
     *  @param  postReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostNotificationSession getPostNotificationSession(org.osid.financials.posting.PostReceiver postReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostNotificationSession(postReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service for the given business. 
     *
     *  @param  postReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver, businessId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostNotificationSession getPostNotificationSessionForBusiness(org.osid.financials.posting.PostReceiver postReceiver, 
                                                                                                     org.osid.id.Id businessId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostNotificationSessionForBusiness(postReceiver, businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup post/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostBusinessSession getPostBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostBusinessSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning posts to 
     *  businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PostBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostBusinessAssignmentSession getPostBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostBusinessAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PostSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSmartBusinessSession getPostSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostSmartBusinessSession(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryLookupSession getPostEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryLookupSession getPostEntryLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryLookupSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuerySession getPostEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuerySession getPostEntryQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryQuerySessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySearchSession getPostEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySearchSession getPostEntrySearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntrySearchSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryAdminSession getPostEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryAdminSession getPostEntryAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryAdminSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  notification service. 
     *
     *  @param  postEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryNotificationSession getPostEntryNotificationSession(org.osid.financials.posting.PostEntryReceiver postEntryReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryNotificationSession(postEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  notification service for the given business. 
     *
     *  @param  postEntryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postEntryReceiver, 
     *          businessId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryNotificationSession getPostEntryNotificationSessionForBusiness(org.osid.financials.posting.PostEntryReceiver postEntryReceiver, 
                                                                                                               org.osid.id.Id businessId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryNotificationSessionForBusiness(postEntryReceiver, businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryBusinessSession getPostEntryBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryBusinessSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning post 
     *  entries to businesses. 
     *
     *  @param  proxy proxy 
     *  @return an <code> PostEntryCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryBusinessAssignmentSession getPostEntryBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryBusinessAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post entry 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return an <code> PostEntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntrySmartBusinessSession getPostEntrySmartBusinessSession(org.osid.id.Id businessId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntrySmartBusinessSession(businessId, proxy));
    }


    /**
     *  Gets a <code> FinancialsPostingBatchProxyManager. </code> 
     *
     *  @return a <code> FinancialsPostingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsPostingBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.batch.FinancialsPostingBatchProxyManager getFinancialsPostingBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFinancialsPostingBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
