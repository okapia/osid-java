//
// AbstractQueryQueueConstrainerLookupSession.java
//
//    A QueueConstrainerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QueueConstrainerQuerySession adapter.
 */

public abstract class AbstractAdapterQueueConstrainerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.tracking.rules.QueueConstrainerQuerySession {

    private final org.osid.tracking.rules.QueueConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterQueueConstrainerQuerySession.
     *
     *  @param session the underlying queue constrainer query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterQueueConstrainerQuerySession(org.osid.tracking.rules.QueueConstrainerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeFrontOffice</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeFrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the {@codeFrontOffice</code> associated with this 
     *  session.
     *
     *  @return the {@codeFrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform {@codeQueueConstrainer</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchQueueConstrainers() {
        return (this.session.canSearchQueueConstrainers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainers in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this front office only.
     */
    
    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    
      
    /**
     *  Gets a queue constrainer query. The returned query will not have an
     *  extension query.
     *
     *  @return the queue constrainer query 
     */
      
    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuery getQueueConstrainerQuery() {
        return (this.session.getQueueConstrainerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  queueConstrainerQuery the queue constrainer query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code queueConstrainerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code queueConstrainerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByQuery(org.osid.tracking.rules.QueueConstrainerQuery queueConstrainerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getQueueConstrainersByQuery(queueConstrainerQuery));
    }
}
