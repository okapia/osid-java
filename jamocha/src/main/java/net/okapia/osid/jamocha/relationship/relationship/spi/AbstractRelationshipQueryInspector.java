//
// AbstractRelationshipQueryInspector.java
//
//     A template for making a RelationshipQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for relationships.
 */

public abstract class AbstractRelationshipQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.relationship.RelationshipQueryInspector {

    private final java.util.Collection<org.osid.relationship.records.RelationshipQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the peer <code> Id </code> terms. 
     *
     *  @return the peer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the other peer <code> Id </code> terms. 
     *
     *  @return the other peer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDestinationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the same peer terms. 
     *
     *  @return the same peer terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSamePeerIdTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the family <code> Id </code> terms. 
     *
     *  @return the family <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFamilyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the family terms. 
     *
     *  @return the family terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given relationship query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a relationship implementing the requested record.
     *
     *  @param relationshipRecordType a relationship record type
     *  @return the relationship query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipQueryInspectorRecord getRelationshipQueryInspectorRecord(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.RelationshipQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relationship query. 
     *
     *  @param relationshipQueryInspectorRecord relationship query inspector
     *         record
     *  @param relationshipRecordType relationship record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelationshipQueryInspectorRecord(org.osid.relationship.records.RelationshipQueryInspectorRecord relationshipQueryInspectorRecord, 
                                                   org.osid.type.Type relationshipRecordType) {

        addRecordType(relationshipRecordType);
        nullarg(relationshipRecordType, "relationship record type");
        this.records.add(relationshipQueryInspectorRecord);        
        return;
    }
}
