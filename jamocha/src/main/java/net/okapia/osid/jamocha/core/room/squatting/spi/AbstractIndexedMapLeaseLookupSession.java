//
// AbstractIndexedMapLeaseLookupSession.java
//
//    A simple framework for providing a Lease lookup service
//    backed by a fixed collection of leases with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Lease lookup service backed by a
 *  fixed collection of leases. The leases are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some leases may be compatible
 *  with more types than are indicated through these lease
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Leases</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapLeaseLookupSession
    extends AbstractMapLeaseLookupSession
    implements org.osid.room.squatting.LeaseLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.squatting.Lease> leasesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.squatting.Lease>());
    private final MultiMap<org.osid.type.Type, org.osid.room.squatting.Lease> leasesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.squatting.Lease>());


    /**
     *  Makes a <code>Lease</code> available in this session.
     *
     *  @param  lease a lease
     *  @throws org.osid.NullArgumentException <code>lease<code> is
     *          <code>null</code>
     */

    @Override
    protected void putLease(org.osid.room.squatting.Lease lease) {
        super.putLease(lease);

        this.leasesByGenus.put(lease.getGenusType(), lease);
        
        try (org.osid.type.TypeList types = lease.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.leasesByRecord.put(types.getNextType(), lease);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a lease from this session.
     *
     *  @param leaseId the <code>Id</code> of the lease
     *  @throws org.osid.NullArgumentException <code>leaseId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeLease(org.osid.id.Id leaseId) {
        org.osid.room.squatting.Lease lease;
        try {
            lease = getLease(leaseId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.leasesByGenus.remove(lease.getGenusType());

        try (org.osid.type.TypeList types = lease.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.leasesByRecord.remove(types.getNextType(), lease);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeLease(leaseId);
        return;
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  lease genus <code>Type</code> which does not include
     *  leases of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known leases or an error results. Otherwise,
     *  the returned list may contain only those leases that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.squatting.lease.ArrayLeaseList(this.leasesByGenus.get(leaseGenusType)));
    }


    /**
     *  Gets a <code>LeaseList</code> containing the given
     *  lease record <code>Type</code>. In plenary mode, the
     *  returned list contains all known leases or an error
     *  results. Otherwise, the returned list may contain only those
     *  leases that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return the returned <code>lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByRecordType(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.squatting.lease.ArrayLeaseList(this.leasesByRecord.get(leaseRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.leasesByGenus.clear();
        this.leasesByRecord.clear();

        super.close();

        return;
    }
}
