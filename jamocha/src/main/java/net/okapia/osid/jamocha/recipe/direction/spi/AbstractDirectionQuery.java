//
// AbstractDirectionQuery.java
//
//     A template for making a Direction Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for directions.
 */

public abstract class AbstractDirectionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.recipe.DirectionQuery {

    private final java.util.Collection<org.osid.recipe.records.DirectionQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the recipe <code> Id </code> for this query. 
     *
     *  @param  recipeId the recipe <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recipeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipeId(org.osid.id.Id recipeId, boolean match) {
        return;
    }


    /**
     *  Clears the recipe <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRecipeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RecipeQuery </code> is available. 
     *
     *  @return <code> true </code> if a recipe query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipe. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the recipe query 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuery getRecipeQuery() {
        throw new org.osid.UnimplementedException("supportsRecipeQuery() is false");
    }


    /**
     *  Clears the recipe query terms. 
     */

    @OSID @Override
    public void clearRecipeTerms() {
        return;
    }


    /**
     *  Sets the procedure <code> Id </code> for this query. 
     *
     *  @param  procedureId the procedure <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> procedureId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcedureId(org.osid.id.Id procedureId, boolean match) {
        return;
    }


    /**
     *  Clears the procedure <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcedureIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcedureQuery </code> is available. 
     *
     *  @return <code> true </code> if a procedure query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureQuery() {
        return (false);
    }


    /**
     *  Gets the query for a procedure. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the procedure query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuery getProcedureQuery() {
        throw new org.osid.UnimplementedException("supportsProcedureQuery() is false");
    }


    /**
     *  Matches directions with any procedure. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          procedure, <code> false </code> to match directions with no 
     *          procedures 
     */

    @OSID @Override
    public void matchAnyProcedure(boolean match) {
        return;
    }


    /**
     *  Clears the procedure query terms. 
     */

    @OSID @Override
    public void clearProcedureTerms() {
        return;
    }


    /**
     *  Sets the ingredient <code> Id </code> for the ingredient. 
     *
     *  @param  ingredientId the ingredient <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ingredientId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIngredientId(org.osid.id.Id ingredientId, boolean match) {
        return;
    }


    /**
     *  Clears the ingredient <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIngredientIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IngredientQuery </code> is available. 
     *
     *  @return <code> true </code> if a stocjk query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIngredientQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ingredient item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ingredient query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIngredientItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.IngredientQuery getIngredientQuery() {
        throw new org.osid.UnimplementedException("supportsIngredientQuery() is false");
    }


    /**
     *  Matches directions with any ingredient. 
     *
     *  @param  match <code> true </code> to match directions with any ingredient, 
     *          <code> false </code> to match directions with no ingredient 
     */

    @OSID @Override
    public void matchAnyIngredient(boolean match) {
        return;
    }


    /**
     *  Clears the ingredient query terms. 
     */

    @OSID @Override
    public void clearIngredientTerms() {
        return;
    }


    /**
     *  Matches directions with an estimated duration between the given range 
     *  inclusive. 
     *
     *  @param  start starting duration 
     *  @param  end ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEstimatedDuration(org.osid.calendaring.Duration start, 
                                       org.osid.calendaring.Duration end, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches directions with any estimated duration. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          estimated duration, <code> false </code> to match directions 
     *          with no estimated duration 
     */

    @OSID @Override
    public void matchAnyEstimatedDuration(boolean match) {
        return;
    }


    /**
     *  Clears the duration query terms. 
     */

    @OSID @Override
    public void clearEstimatedDurationTerms() {
        return;
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        return;
    }


    /**
     *  Clears the asset <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches directions with any asset. 
     *
     *  @param  match <code> true </code> to match directions with any asset, 
     *          <code> false </code> to match directions with no assets 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        return;
    }


    /**
     *  Clears the asset query terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match directions 
     *  assigned to cookbooks. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCookbookId(org.osid.id.Id cookbookId, boolean match) {
        return;
    }


    /**
     *  Clears the cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCookbookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsCookbookQuery() is false");
    }


    /**
     *  Clears the cook book query terms. 
     */

    @OSID @Override
    public void clearCookbookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given direction query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a direction implementing the requested record.
     *
     *  @param directionRecordType a direction record type
     *  @return the direction query record
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionQueryRecord getDirectionQueryRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.DirectionQueryRecord record : this.records) {
            if (record.implementsRecordType(directionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this direction query. 
     *
     *  @param directionQueryRecord direction query record
     *  @param directionRecordType direction record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDirectionQueryRecord(org.osid.recipe.records.DirectionQueryRecord directionQueryRecord, 
                                          org.osid.type.Type directionRecordType) {

        addRecordType(directionRecordType);
        nullarg(directionQueryRecord, "direction query record");
        this.records.add(directionQueryRecord);        
        return;
    }
}
