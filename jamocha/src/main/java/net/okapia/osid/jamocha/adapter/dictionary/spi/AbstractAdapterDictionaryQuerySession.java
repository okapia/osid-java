//
// AbstractQueryDictionaryLookupSession.java
//
//    A DictionaryQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DictionaryQuerySession adapter.
 */

public abstract class AbstractAdapterDictionaryQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.dictionary.DictionaryQuerySession {

    private final org.osid.dictionary.DictionaryQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterDictionaryQuerySession.
     *
     *  @param session the underlying dictionary query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDictionaryQuerySession(org.osid.dictionary.DictionaryQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeDictionary</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchDictionaries() {
        return (this.session.canSearchDictionaries());
    }

      
    /**
     *  Gets a dictionary query. The returned query will not have an
     *  extension query.
     *
     *  @return the dictionary query 
     */
      
    @OSID @Override
    public org.osid.dictionary.DictionaryQuery getDictionaryQuery() {
        return (this.session.getDictionaryQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  dictionaryQuery the dictionary query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code dictionaryQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code dictionaryQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByQuery(org.osid.dictionary.DictionaryQuery dictionaryQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getDictionariesByQuery(dictionaryQuery));
    }
}
