//
// AbstractQueryLeaseLookupSession.java
//
//    An inline adapter that maps a LeaseLookupSession to
//    a LeaseQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a LeaseLookupSession to
 *  a LeaseQuerySession.
 */

public abstract class AbstractQueryLeaseLookupSession
    extends net.okapia.osid.jamocha.room.squatting.spi.AbstractLeaseLookupSession
    implements org.osid.room.squatting.LeaseLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.room.squatting.LeaseQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryLeaseLookupSession.
     *
     *  @param querySession the underlying lease query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryLeaseLookupSession(org.osid.room.squatting.LeaseQuerySession querySession) {
        nullarg(querySession, "lease query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Campus</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform <code>Lease</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLeases() {
        return (this.session.canSearchLeases());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include leases in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only leases whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveLeaseView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All leases of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveLeaseView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Lease</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Lease</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Lease</code> and
     *  retained for compatibility.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  leaseId <code>Id</code> of the
     *          <code>Lease</code>
     *  @return the lease
     *  @throws org.osid.NotFoundException <code>leaseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>leaseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Lease getLease(org.osid.id.Id leaseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchId(leaseId, true);
        org.osid.room.squatting.LeaseList leases = this.session.getLeasesByQuery(query);
        if (leases.hasNext()) {
            return (leases.getNextLease());
        } 
        
        throw new org.osid.NotFoundException(leaseId + " not found");
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  leases specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Leases</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, leases are returned that are currently effective.
     *  In any effective mode, effective leases and those currently expired
     *  are returned.
     *
     *  @param  leaseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>leaseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByIds(org.osid.id.IdList leaseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();

        try (org.osid.id.IdList ids = leaseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given lease
     *  genus <code>Type</code> which does not include leases of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchGenusType(leaseGenusType, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  lease genus <code>Type</code> and include any additional
     *  leases with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByParentGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchParentGenusType(leaseGenusType, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a <code>LeaseList</code> containing the given
     *  lease record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByRecordType(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRecordType(leaseRecordType, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a <code>LeaseList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Lease</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }

    
    /**
     *  Gets a list of all leases of a genus type effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> LeaseList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> leaseGenusType, from 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeOnDate(org.osid.type.Type leaseGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchGenusType(leaseGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of leases corresponding to a room
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.LeaseList getLeasesForRoom(org.osid.id.Id roomId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of all leases for a room and a lease genus type.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code> LeaseList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> roomId </code>
     *          or <code> leaseGenusType </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoom(org.osid.id.Id roomId, 
                                                                         org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchGenusType(leaseGenusType, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of leases corresponding to a room
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomOnDate(org.osid.id.Id roomId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of all leases for a room and of a lease genus type
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> LeaseList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> roomId, leaseGenusType, 
     *          from </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomOnDate(org.osid.id.Id roomId, 
                                                                               org.osid.type.Type leaseGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchGenusType(leaseGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of leases corresponding to a tenant
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.LeaseList getLeasesForTenant(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchTenantId(resourceId, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of all leases corresponding to a tenant <code> Id
     *  </code> and of a lease genus type.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resourceId <code> Id </code> 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code> LeaseList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> leaseGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenant(org.osid.id.Id resourceId, 
                                                                           org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchTenantId(resourceId, true);
        query.matchGenusType(leaseGenusType, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of leases corresponding to a tenant
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForTenantOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchTenantId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }

    
    /**
     *  Gets a list of all leases for a tenant and of a lease genus
     *  type effective during the entire given date range inclusive
     *  but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> LeaseList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          leaseGenusType, from, </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenantOnDate(org.osid.id.Id resourceId, 
                                                                                 org.osid.type.Type leaseGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchTenantId(resourceId, true);
        query.matchGenusType(leaseGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of leases corresponding to room and tenant
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenant(org.osid.id.Id roomId,
                                                                       org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchTenantId(resourceId, true);
        return (this.session.getLeasesByQuery(query));
    }

    
    /**
     *  Gets a list of all leases for a room, resource, and of a lease
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  resourceId a tenant <code> Id </code> 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code> LeaseList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> roomId,
     *          resourceId, </code> or <code> leaseGenusType </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenant(org.osid.id.Id roomId, 
                                                                                  org.osid.id.Id resourceId, 
                                                                                  org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchTenantId(resourceId, true);
        query.matchGenusType(leaseGenusType, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets a list of leases corresponding to room and tenant
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenantOnDate(org.osid.id.Id roomId,
                                                                             org.osid.id.Id resourceId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchTenantId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }

    
    /**
     *  Gets a list of all leases for a room, tenant, and of a lease
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> LeaseList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> roomId,
     *          resourceId, leaseGenusType, from, </code> or <code> to
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenantOnDate(org.osid.id.Id roomId, 
                                                                                        org.osid.id.Id resourceId, 
                                                                                        org.osid.type.Type leaseGenusType, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchTenantId(resourceId, true);
        query.matchGenusType(leaseGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets all <code>Leases</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Leases</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeases()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.squatting.LeaseQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getLeasesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.room.squatting.LeaseQuery getQuery() {
        org.osid.room.squatting.LeaseQuery query = this.session.getLeaseQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
