//
// AbstractAcknowledgementBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAcknowledgementBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.acknowledgement.batch.AcknowledgementBatchManager,
               org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractAcknowledgementBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAcknowledgementBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of credits is available. 
     *
     *  @return <code> true </code> if a credit bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of billings is available. 
     *
     *  @return <code> true </code> if a billing bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk credit 
     *  administration service. 
     *
     *  @return a <code> CreditBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchAdminSession getCreditBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.batch.AcknowledgementBatchManager.getCreditBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk credit 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CreditBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchAdminSession getCreditBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager.getCreditBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk credit 
     *  administration service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @return a <code> CreditBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchAdminSession getCreditBatchAdminSessionForBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.batch.AcknowledgementBatchManager.getCreditBatchAdminSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk credit 
     *  administration service for the given billing. 
     *
     *  @param  billingId the <code> Id </code> of the <code> Billing </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CreditBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Billing </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> billingId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchAdminSession getCreditBatchAdminSessionForBilling(org.osid.id.Id billingId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager.getCreditBatchAdminSessionForBilling not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk billing 
     *  administration service. 
     *
     *  @return a <code> BillingBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.BillingBatchAdminSession getBillingBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.batch.AcknowledgementBatchManager.getBillingBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk billing 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BillingBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.BillingBatchAdminSession getBillingBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager.getBillingBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
