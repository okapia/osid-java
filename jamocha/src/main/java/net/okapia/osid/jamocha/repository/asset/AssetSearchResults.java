//
// AssetSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset;


/**
 *  A template for implementing a search results.
 */

public final class AssetSearchResults
    extends net.okapia.osid.jamocha.repository.asset.spi.AbstractAssetSearchResults
    implements org.osid.repository.AssetSearchResults {


    /**
     *  Constructs a new <code>AssetSearchResults.
     *
     *  @param assets the result set
     *  @param assetQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>assets</code>
     *          or <code>assetQueryInspector</code> is
     *          <code>null</code>
     */

    public AssetSearchResults(org.osid.repository.AssetList assets,
                                 org.osid.repository.AssetQueryInspector assetQueryInspector) {
        super(assets, assetQueryInspector);
        return;
    }
}
