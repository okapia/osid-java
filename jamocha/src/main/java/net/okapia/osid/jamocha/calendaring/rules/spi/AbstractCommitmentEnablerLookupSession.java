//
// AbstractCommitmentEnablerLookupSession.java
//
//    A starter implementation framework for providing a CommitmentEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CommitmentEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCommitmentEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCommitmentEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.rules.CommitmentEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>CommitmentEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommitmentEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>CommitmentEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommitmentEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CommitmentEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommitmentEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commitment enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active commitment enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCommitmentEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive commitment enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCommitmentEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CommitmentEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CommitmentEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CommitmentEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerId <code>Id</code> of the
     *          <code>CommitmentEnabler</code>
     *  @return the commitment enabler
     *  @throws org.osid.NotFoundException <code>commitmentEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commitmentEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnabler getCommitmentEnabler(org.osid.id.Id commitmentEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.rules.CommitmentEnablerList commitmentEnablers = getCommitmentEnablers()) {
            while (commitmentEnablers.hasNext()) {
                org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler = commitmentEnablers.getNextCommitmentEnabler();
                if (commitmentEnabler.getId().equals(commitmentEnablerId)) {
                    return (commitmentEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(commitmentEnablerId + " not found");
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commitmentEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CommitmentEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCommitmentEnablers()</code>.
     *
     *  @param  commitmentEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByIds(org.osid.id.IdList commitmentEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.rules.CommitmentEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = commitmentEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCommitmentEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("commitment enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.LinkedCommitmentEnablerList(ret));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> corresponding to the given
     *  commitment enabler genus <code>Type</code> which does not include
     *  commitment enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCommitmentEnablers()</code>.
     *
     *  @param  commitmentEnablerGenusType a commitmentEnabler genus type 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByGenusType(org.osid.type.Type commitmentEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.rules.commitmentenabler.CommitmentEnablerGenusFilterList(getCommitmentEnablers(), commitmentEnablerGenusType));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> corresponding to the given
     *  commitment enabler genus <code>Type</code> and include any additional
     *  commitment enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommitmentEnablers()</code>.
     *
     *  @param  commitmentEnablerGenusType a commitmentEnabler genus type 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByParentGenusType(org.osid.type.Type commitmentEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCommitmentEnablersByGenusType(commitmentEnablerGenusType));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> containing the given
     *  commitment enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommitmentEnablers()</code>.
     *
     *  @param  commitmentEnablerRecordType a commitmentEnabler record type 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByRecordType(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.rules.commitmentenabler.CommitmentEnablerRecordFilterList(getCommitmentEnablers(), commitmentEnablerRecordType));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible
     *  through this session.
     *  
     *  In active mode, commitment enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive commitment enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CommitmentEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.rules.commitmentenabler.TemporalCommitmentEnablerFilterList(getCommitmentEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>CommitmentEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible
     *  through this session.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive commitment enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getCommitmentEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>CommitmentEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @return a list of <code>CommitmentEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the commitment enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of commitment enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.rules.CommitmentEnablerList filterCommitmentEnablersOnViews(org.osid.calendaring.rules.CommitmentEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.calendaring.rules.CommitmentEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.rules.commitmentenabler.ActiveCommitmentEnablerFilterList(ret);
        }

        return (ret);
    }
}
