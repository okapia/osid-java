//
// MutableMapLocationLookupSession
//
//    Implements a Location lookup service backed by a collection of
//    locations that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping;


/**
 *  Implements a Location lookup service backed by a collection of
 *  locations. The locations are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of locations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapLocationLookupSession
    extends net.okapia.osid.jamocha.core.mapping.spi.AbstractMapLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {


    /**
     *  Constructs a new {@code MutableMapLocationLookupSession}
     *  with no locations.
     *
     *  @param map the map
     *  @throws org.osid.NullArgumentException {@code map} is
     *          {@code null}
     */

      public MutableMapLocationLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLocationLookupSession} with a
     *  single location.
     *
     *  @param map the map  
     *  @param location a location
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code location} is {@code null}
     */

    public MutableMapLocationLookupSession(org.osid.mapping.Map map,
                                           org.osid.mapping.Location location) {
        this(map);
        putLocation(location);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLocationLookupSession}
     *  using an array of locations.
     *
     *  @param map the map
     *  @param locations an array of locations
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code locations} is {@code null}
     */

    public MutableMapLocationLookupSession(org.osid.mapping.Map map,
                                           org.osid.mapping.Location[] locations) {
        this(map);
        putLocations(locations);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLocationLookupSession}
     *  using a collection of locations.
     *
     *  @param map the map
     *  @param locations a collection of locations
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code locations} is {@code null}
     */

    public MutableMapLocationLookupSession(org.osid.mapping.Map map,
                                           java.util.Collection<? extends org.osid.mapping.Location> locations) {

        this(map);
        putLocations(locations);
        return;
    }

    
    /**
     *  Makes a {@code Location} available in this session.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException {@code location{@code  is
     *          {@code null}
     */

    @Override
    public void putLocation(org.osid.mapping.Location location) {
        super.putLocation(location);
        return;
    }


    /**
     *  Makes an array of locations available in this session.
     *
     *  @param locations an array of locations
     *  @throws org.osid.NullArgumentException {@code locations{@code 
     *          is {@code null}
     */

    @Override
    public void putLocations(org.osid.mapping.Location[] locations) {
        super.putLocations(locations);
        return;
    }


    /**
     *  Makes collection of locations available in this session.
     *
     *  @param locations a collection of locations
     *  @throws org.osid.NullArgumentException {@code locations{@code  is
     *          {@code null}
     */

    @Override
    public void putLocations(java.util.Collection<? extends org.osid.mapping.Location> locations) {
        super.putLocations(locations);
        return;
    }


    /**
     *  Removes a Location from this session.
     *
     *  @param locationId the {@code Id} of the location
     *  @throws org.osid.NullArgumentException {@code locationId{@code 
     *          is {@code null}
     */

    @Override
    public void removeLocation(org.osid.id.Id locationId) {
        super.removeLocation(locationId);
        return;
    }    
}
