//
// AbstractAssemblySystemQuery.java
//
//     A SystemQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.system.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SystemQuery that stores terms.
 */

public abstract class AbstractAssemblySystemQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.control.SystemQuery,
               org.osid.control.SystemQueryInspector,
               org.osid.control.SystemSearchOrder {

    private final java.util.Collection<org.osid.control.records.SystemQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.SystemQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.SystemSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySystemQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySystemQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the device <code> Id </code> for this query to match systems that 
     *  have a related device. 
     *
     *  @param  deviceId a device <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> deviceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDeviceId(org.osid.id.Id deviceId, boolean match) {
        getAssembler().addIdTerm(getDeviceIdColumn(), deviceId, match);
        return;
    }


    /**
     *  Clears the device <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDeviceIdTerms() {
        getAssembler().clearTerms(getDeviceIdColumn());
        return;
    }


    /**
     *  Gets the device <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDeviceIdTerms() {
        return (getAssembler().getIdTerms(getDeviceIdColumn()));
    }


    /**
     *  Gets the DeviceId column name.
     *
     * @return the column name
     */

    protected String getDeviceIdColumn() {
        return ("device_id");
    }


    /**
     *  Tests if a <code> DeviceQuery </code> is available. 
     *
     *  @return <code> true </code> if a device query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a device. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the device query 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuery getDeviceQuery() {
        throw new org.osid.UnimplementedException("supportsDeviceQuery() is false");
    }


    /**
     *  Matches systems that have any device. 
     *
     *  @param  match <code> true </code> to match systems with any device, 
     *          <code> false </code> to match systems with no device 
     */

    @OSID @Override
    public void matchAnyDevice(boolean match) {
        getAssembler().addIdWildcardTerm(getDeviceColumn(), match);
        return;
    }


    /**
     *  Clears the device query terms. 
     */

    @OSID @Override
    public void clearDeviceTerms() {
        getAssembler().clearTerms(getDeviceColumn());
        return;
    }


    /**
     *  Gets the device query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.DeviceQueryInspector[] getDeviceTerms() {
        return (new org.osid.control.DeviceQueryInspector[0]);
    }


    /**
     *  Gets the Device column name.
     *
     * @return the column name
     */

    protected String getDeviceColumn() {
        return ("device");
    }


    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId the controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        getAssembler().addIdTerm(getControllerIdColumn(), controllerId, match);
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        getAssembler().clearTerms(getControllerIdColumn());
        return;
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (getAssembler().getIdTerms(getControllerIdColumn()));
    }


    /**
     *  Gets the ControllerId column name.
     *
     * @return the column name
     */

    protected String getControllerIdColumn() {
        return ("controller_id");
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a controller. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Matches systems with any controller. 
     *
     *  @param  match <code> true </code> to match systems with any 
     *          controller, <code> false </code> to match systems with no 
     *          controller 
     */

    @OSID @Override
    public void matchAnyController(boolean match) {
        getAssembler().addIdWildcardTerm(getControllerColumn(), match);
        return;
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        getAssembler().clearTerms(getControllerColumn());
        return;
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Gets the Controller column name.
     *
     * @return the column name
     */

    protected String getControllerColumn() {
        return ("controller");
    }


    /**
     *  Sets the input <code> Id </code> for this query. 
     *
     *  @param  inputId the input <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inputId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInputId(org.osid.id.Id inputId, boolean match) {
        getAssembler().addIdTerm(getInputIdColumn(), inputId, match);
        return;
    }


    /**
     *  Clears the input <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInputIdTerms() {
        getAssembler().clearTerms(getInputIdColumn());
        return;
    }


    /**
     *  Gets the input <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInputIdTerms() {
        return (getAssembler().getIdTerms(getInputIdColumn()));
    }


    /**
     *  Gets the InputId column name.
     *
     * @return the column name
     */

    protected String getInputIdColumn() {
        return ("input_id");
    }


    /**
     *  Tests if an <code> InputQuery </code> is available. 
     *
     *  @return <code> true </code> if an input query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputQuery() {
        return (false);
    }


    /**
     *  Gets the query for an input. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the input query 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuery getInputQuery() {
        throw new org.osid.UnimplementedException("supportsInputQuery() is false");
    }


    /**
     *  Matches systems with any input. 
     *
     *  @param  match <code> true </code> to match systems with any input, 
     *          <code> false </code> to match systems with no inputs 
     */

    @OSID @Override
    public void matchAnyInput(boolean match) {
        getAssembler().addIdWildcardTerm(getInputColumn(), match);
        return;
    }


    /**
     *  Clears the input query terms. 
     */

    @OSID @Override
    public void clearInputTerms() {
        getAssembler().clearTerms(getInputColumn());
        return;
    }


    /**
     *  Gets the input query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.InputQueryInspector[] getInputTerms() {
        return (new org.osid.control.InputQueryInspector[0]);
    }


    /**
     *  Gets the Input column name.
     *
     * @return the column name
     */

    protected String getInputColumn() {
        return ("input");
    }


    /**
     *  Sets the setting <code> Id </code> for this query. 
     *
     *  @param  settingId the setting <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> settingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettingId(org.osid.id.Id settingId, boolean match) {
        getAssembler().addIdTerm(getSettingIdColumn(), settingId, match);
        return;
    }


    /**
     *  Clears the setting <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSettingIdTerms() {
        getAssembler().clearTerms(getSettingIdColumn());
        return;
    }


    /**
     *  Gets the setting <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSettingIdTerms() {
        return (getAssembler().getIdTerms(getSettingIdColumn()));
    }


    /**
     *  Gets the SettingId column name.
     *
     * @return the column name
     */

    protected String getSettingIdColumn() {
        return ("setting_id");
    }


    /**
     *  Tests if a <code> SettingQuery </code> is available. 
     *
     *  @return <code> true </code> if a setting query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a setting. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the setting query 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuery getSettingQuery() {
        throw new org.osid.UnimplementedException("supportsSettingQuery() is false");
    }


    /**
     *  Matches systems with any setting. 
     *
     *  @param  match <code> true </code> to match systems with any setting, 
     *          <code> false </code> to match systems with no settings 
     */

    @OSID @Override
    public void matchAnySetting(boolean match) {
        getAssembler().addIdWildcardTerm(getSettingColumn(), match);
        return;
    }


    /**
     *  Clears the setting query terms. 
     */

    @OSID @Override
    public void clearSettingTerms() {
        getAssembler().clearTerms(getSettingColumn());
        return;
    }


    /**
     *  Gets the setting query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SettingQueryInspector[] getSettingTerms() {
        return (new org.osid.control.SettingQueryInspector[0]);
    }


    /**
     *  Gets the Setting column name.
     *
     * @return the column name
     */

    protected String getSettingColumn() {
        return ("setting");
    }


    /**
     *  Sets the scene <code> Id </code> for this query. 
     *
     *  @param  sceneId the scene <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sceneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSceneId(org.osid.id.Id sceneId, boolean match) {
        getAssembler().addIdTerm(getSceneIdColumn(), sceneId, match);
        return;
    }


    /**
     *  Clears the scene <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSceneIdTerms() {
        getAssembler().clearTerms(getSceneIdColumn());
        return;
    }


    /**
     *  Gets the scene <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSceneIdTerms() {
        return (getAssembler().getIdTerms(getSceneIdColumn()));
    }


    /**
     *  Gets the SceneId column name.
     *
     * @return the column name
     */

    protected String getSceneIdColumn() {
        return ("scene_id");
    }


    /**
     *  Tests if a <code> SceneQuery </code> is available. 
     *
     *  @return <code> true </code> if a scene query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a scene. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the scene query 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuery getSceneQuery() {
        throw new org.osid.UnimplementedException("supportsSceneQuery() is false");
    }


    /**
     *  Matches systems with any scene. 
     *
     *  @param  match <code> true </code> to match systems with any scene, 
     *          <code> false </code> to match systems with no scenes 
     */

    @OSID @Override
    public void matchAnyScene(boolean match) {
        getAssembler().addIdWildcardTerm(getSceneColumn(), match);
        return;
    }


    /**
     *  Clears the scene query terms. 
     */

    @OSID @Override
    public void clearSceneTerms() {
        getAssembler().clearTerms(getSceneColumn());
        return;
    }


    /**
     *  Gets the scene query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SceneQueryInspector[] getSceneTerms() {
        return (new org.osid.control.SceneQueryInspector[0]);
    }


    /**
     *  Gets the Scene column name.
     *
     * @return the column name
     */

    protected String getSceneColumn() {
        return ("scene");
    }


    /**
     *  Sets the trigger <code> Id </code> for this query. 
     *
     *  @param  triggerId the trigger <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> triggerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTriggerId(org.osid.id.Id triggerId, boolean match) {
        getAssembler().addIdTerm(getTriggerIdColumn(), triggerId, match);
        return;
    }


    /**
     *  Clears the trigger <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTriggerIdTerms() {
        getAssembler().clearTerms(getTriggerIdColumn());
        return;
    }


    /**
     *  Gets the trigger <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTriggerIdTerms() {
        return (getAssembler().getIdTerms(getTriggerIdColumn()));
    }


    /**
     *  Gets the TriggerId column name.
     *
     * @return the column name
     */

    protected String getTriggerIdColumn() {
        return ("trigger_id");
    }


    /**
     *  Tests if a <code> TriggerQuery </code> is available. 
     *
     *  @return <code> true </code> if a trigger query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a trigger. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the trigger query 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuery getTriggerQuery() {
        throw new org.osid.UnimplementedException("supportsTriggerQuery() is false");
    }


    /**
     *  Matches systems with any trigger. 
     *
     *  @param  match <code> true </code> to match systems with any trigger, 
     *          <code> false </code> to match systems with no trigger 
     */

    @OSID @Override
    public void matchAnyTrigger(boolean match) {
        getAssembler().addIdWildcardTerm(getTriggerColumn(), match);
        return;
    }


    /**
     *  Clears the trigger query terms. 
     */

    @OSID @Override
    public void clearTriggerTerms() {
        getAssembler().clearTerms(getTriggerColumn());
        return;
    }


    /**
     *  Gets the trigger query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.TriggerQueryInspector[] getTriggerTerms() {
        return (new org.osid.control.TriggerQueryInspector[0]);
    }


    /**
     *  Gets the Trigger column name.
     *
     * @return the column name
     */

    protected String getTriggerColumn() {
        return ("trigger");
    }


    /**
     *  Sets the action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId the action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionGroupId(org.osid.id.Id actionGroupId, boolean match) {
        getAssembler().addIdTerm(getActionGroupIdColumn(), actionGroupId, match);
        return;
    }


    /**
     *  Clears the action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionGroupIdTerms() {
        getAssembler().clearTerms(getActionGroupIdColumn());
        return;
    }


    /**
     *  Gets the action group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActionGroupIdTerms() {
        return (getAssembler().getIdTerms(getActionGroupIdColumn()));
    }


    /**
     *  Gets the ActionGroupId column name.
     *
     * @return the column name
     */

    protected String getActionGroupIdColumn() {
        return ("action_group_id");
    }


    /**
     *  Tests if an <code> ActionGroup </code> is available. 
     *
     *  @return <code> true </code> if an action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsActionGroupQuery() is false");
    }


    /**
     *  Matches systems with any action group. 
     *
     *  @param  match <code> true </code> to match systems with any action 
     *          group, <code> false </code> to match systems with no action 
     *          groups 
     */

    @OSID @Override
    public void matchAnyActionGroup(boolean match) {
        getAssembler().addIdWildcardTerm(getActionGroupColumn(), match);
        return;
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearActionGroupTerms() {
        getAssembler().clearTerms(getActionGroupColumn());
        return;
    }


    /**
     *  Gets the action group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQueryInspector[] getActionGroupTerms() {
        return (new org.osid.control.ActionGroupQueryInspector[0]);
    }


    /**
     *  Gets the ActionGroup column name.
     *
     * @return the column name
     */

    protected String getActionGroupColumn() {
        return ("action_group");
    }


    /**
     *  Sets the system <code> Id </code> for this query to match systems that 
     *  have the specified system as an ancestor. 
     *
     *  @param  systemId a system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getAncestorSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the ancestor system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorSystemIdTerms() {
        getAssembler().clearTerms(getAncestorSystemIdColumn());
        return;
    }


    /**
     *  Gets the ancestor system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorSystemIdTerms() {
        return (getAssembler().getIdTerms(getAncestorSystemIdColumn()));
    }


    /**
     *  Gets the AncestorSystemId column name.
     *
     * @return the column name
     */

    protected String getAncestorSystemIdColumn() {
        return ("ancestor_system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getAncestorSystemQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorSystemQuery() is false");
    }


    /**
     *  Matches systems with any ancestor. 
     *
     *  @param  match <code> true </code> to match systems with any ancestor, 
     *          <code> false </code> to match root systems 
     */

    @OSID @Override
    public void matchAnyAncestorSystem(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorSystemColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor system query terms. 
     */

    @OSID @Override
    public void clearAncestorSystemTerms() {
        getAssembler().clearTerms(getAncestorSystemColumn());
        return;
    }


    /**
     *  Gets the ancestor system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getAncestorSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the AncestorSystem column name.
     *
     * @return the column name
     */

    protected String getAncestorSystemColumn() {
        return ("ancestor_system");
    }


    /**
     *  Sets the system <code> Id </code> for this query to match systems that 
     *  have the specified system as a descendant. 
     *
     *  @param  systemId a system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getDescendantSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the descendant system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantSystemIdTerms() {
        getAssembler().clearTerms(getDescendantSystemIdColumn());
        return;
    }


    /**
     *  Gets the descendant system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantSystemIdTerms() {
        return (getAssembler().getIdTerms(getDescendantSystemIdColumn()));
    }


    /**
     *  Gets the DescendantSystemId column name.
     *
     * @return the column name
     */

    protected String getDescendantSystemIdColumn() {
        return ("descendant_system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantSystemQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getDescendantSystemQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantSystemQuery() is false");
    }


    /**
     *  Matches systems with any descendant. 
     *
     *  @param  match <code> true </code> to match systems with any 
     *          descendant, <code> false </code> to match leaf systems 
     */

    @OSID @Override
    public void matchAnyDescendantSystem(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantSystemColumn(), match);
        return;
    }


    /**
     *  Clears the descendant system query terms. 
     */

    @OSID @Override
    public void clearDescendantSystemTerms() {
        getAssembler().clearTerms(getDescendantSystemColumn());
        return;
    }


    /**
     *  Gets the descendant system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getDescendantSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the DescendantSystem column name.
     *
     * @return the column name
     */

    protected String getDescendantSystemColumn() {
        return ("descendant_system");
    }


    /**
     *  Tests if this system supports the given record
     *  <code>Type</code>.
     *
     *  @param  systemRecordType a system record type 
     *  @return <code>true</code> if the systemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type systemRecordType) {
        for (org.osid.control.records.SystemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(systemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  systemRecordType the system record type 
     *  @return the system query record 
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(systemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SystemQueryRecord getSystemQueryRecord(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SystemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(systemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(systemRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  systemRecordType the system record type 
     *  @return the system query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(systemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SystemQueryInspectorRecord getSystemQueryInspectorRecord(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SystemQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(systemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(systemRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param systemRecordType the system record type
     *  @return the system search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(systemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SystemSearchOrderRecord getSystemSearchOrderRecord(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SystemSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(systemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(systemRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this system. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param systemQueryRecord the system query record
     *  @param systemQueryInspectorRecord the system query inspector
     *         record
     *  @param systemSearchOrderRecord the system search order record
     *  @param systemRecordType system record type
     *  @throws org.osid.NullArgumentException
     *          <code>systemQueryRecord</code>,
     *          <code>systemQueryInspectorRecord</code>,
     *          <code>systemSearchOrderRecord</code> or
     *          <code>systemRecordTypesystem</code> is
     *          <code>null</code>
     */
            
    protected void addSystemRecords(org.osid.control.records.SystemQueryRecord systemQueryRecord, 
                                      org.osid.control.records.SystemQueryInspectorRecord systemQueryInspectorRecord, 
                                      org.osid.control.records.SystemSearchOrderRecord systemSearchOrderRecord, 
                                      org.osid.type.Type systemRecordType) {

        addRecordType(systemRecordType);

        nullarg(systemQueryRecord, "system query record");
        nullarg(systemQueryInspectorRecord, "system query inspector record");
        nullarg(systemSearchOrderRecord, "system search odrer record");

        this.queryRecords.add(systemQueryRecord);
        this.queryInspectorRecords.add(systemQueryInspectorRecord);
        this.searchOrderRecords.add(systemSearchOrderRecord);
        
        return;
    }
}
