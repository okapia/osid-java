//
// AbstractBrokerConstrainerSearch.java
//
//     A template for making a BrokerConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing broker constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBrokerConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.BrokerConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.BrokerConstrainerSearchOrder brokerConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of broker constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  brokerConstrainerIds list of broker constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBrokerConstrainers(org.osid.id.IdList brokerConstrainerIds) {
        while (brokerConstrainerIds.hasNext()) {
            try {
                this.ids.add(brokerConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBrokerConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of broker constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBrokerConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  brokerConstrainerSearchOrder broker constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>brokerConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBrokerConstrainerResults(org.osid.provisioning.rules.BrokerConstrainerSearchOrder brokerConstrainerSearchOrder) {
	this.brokerConstrainerSearchOrder = brokerConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.BrokerConstrainerSearchOrder getBrokerConstrainerSearchOrder() {
	return (this.brokerConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given broker constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker constrainer implementing the requested record.
     *
     *  @param brokerConstrainerSearchRecordType a broker constrainer search record
     *         type
     *  @return the broker constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerSearchRecord getBrokerConstrainerSearchRecord(org.osid.type.Type brokerConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.BrokerConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(brokerConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker constrainer search. 
     *
     *  @param brokerConstrainerSearchRecord broker constrainer search record
     *  @param brokerConstrainerSearchRecordType brokerConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerConstrainerSearchRecord(org.osid.provisioning.rules.records.BrokerConstrainerSearchRecord brokerConstrainerSearchRecord, 
                                           org.osid.type.Type brokerConstrainerSearchRecordType) {

        addRecordType(brokerConstrainerSearchRecordType);
        this.records.add(brokerConstrainerSearchRecord);        
        return;
    }
}
