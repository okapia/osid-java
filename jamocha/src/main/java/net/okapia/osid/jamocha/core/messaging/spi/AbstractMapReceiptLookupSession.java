//
// AbstractMapReceiptLookupSession
//
//    A simple framework for providing a Receipt lookup service
//    backed by a fixed collection of receipts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Receipt lookup service backed by a
 *  fixed collection of receipts. The receipts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Receipts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapReceiptLookupSession
    extends net.okapia.osid.jamocha.messaging.spi.AbstractReceiptLookupSession
    implements org.osid.messaging.ReceiptLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.messaging.Receipt> receipts = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.messaging.Receipt>());


    /**
     *  Makes a <code>Receipt</code> available in this session.
     *
     *  @param  receipt a receipt
     *  @throws org.osid.NullArgumentException <code>receipt<code>
     *          is <code>null</code>
     */

    protected void putReceipt(org.osid.messaging.Receipt receipt) {
        this.receipts.put(receipt.getId(), receipt);
        return;
    }


    /**
     *  Makes an array of receipts available in this session.
     *
     *  @param  receipts an array of receipts
     *  @throws org.osid.NullArgumentException <code>receipts<code>
     *          is <code>null</code>
     */

    protected void putReceipts(org.osid.messaging.Receipt[] receipts) {
        putReceipts(java.util.Arrays.asList(receipts));
        return;
    }


    /**
     *  Makes a collection of receipts available in this session.
     *
     *  @param  receipts a collection of receipts
     *  @throws org.osid.NullArgumentException <code>receipts<code>
     *          is <code>null</code>
     */

    protected void putReceipts(java.util.Collection<? extends org.osid.messaging.Receipt> receipts) {
        for (org.osid.messaging.Receipt receipt : receipts) {
            this.receipts.put(receipt.getId(), receipt);
        }

        return;
    }


    /**
     *  Removes a Receipt from this session.
     *
     *  @param  receiptId the <code>Id</code> of the receipt
     *  @throws org.osid.NullArgumentException <code>receiptId<code> is
     *          <code>null</code>
     */

    protected void removeReceipt(org.osid.id.Id receiptId) {
        this.receipts.remove(receiptId);
        return;
    }


    /**
     *  Gets the <code>Receipt</code> specified by its <code>Id</code>.
     *
     *  @param  receiptId <code>Id</code> of the <code>Receipt</code>
     *  @return the receipt
     *  @throws org.osid.NotFoundException <code>receiptId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>receiptId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Receipt getReceipt(org.osid.id.Id receiptId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.messaging.Receipt receipt = this.receipts.get(receiptId);
        if (receipt == null) {
            throw new org.osid.NotFoundException("receipt not found: " + receiptId);
        }

        return (receipt);
    }


    /**
     *  Gets all <code>Receipts</code>. In plenary mode, the returned
     *  list contains all known receipts or an error
     *  results. Otherwise, the returned list may contain only those
     *  receipts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Receipts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceipts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.receipt.ArrayReceiptList(this.receipts.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.receipts.clear();
        super.close();
        return;
    }
}
