//
// AbstractImmutableOffsetEvent.java
//
//     Wraps a mutable OffsetEvent to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>OffsetEvent</code> to hide modifiers. This
 *  wrapper provides an immutized OffsetEvent from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying offsetEvent whose state changes are visible.
 */

public abstract class AbstractImmutableOffsetEvent
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.calendaring.OffsetEvent {

    private final org.osid.calendaring.OffsetEvent offsetEvent;


    /**
     *  Constructs a new <code>AbstractImmutableOffsetEvent</code>.
     *
     *  @param offsetEvent the offset event to immutablize
     *  @throws org.osid.NullArgumentException <code>offsetEvent</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOffsetEvent(org.osid.calendaring.OffsetEvent offsetEvent) {
        super(offsetEvent);
        this.offsetEvent = offsetEvent;
        return;
    }


    /**
     *  Tests if this offset event is based on a fixed start time. 
     *
     *  @return <code> true </code> if this offset is based on a fixed start 
     *          time, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasFixedStartTime() {
        return (this.offsetEvent.hasFixedStartTime());
    }


    /**
     *  Gets the fixed start time for this event. 
     *
     *  @return the fixed start time 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartIme() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getFixedStartTime() {
        return (this.offsetEvent.getFixedStartTime());
    }


    /**
     *  Gets the <code> Event Id </code> to which the start of this event is 
     *  offset. 
     *
     *  @return the relative event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartTime() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStartReferenceEventId() {
        return (this.offsetEvent.getStartReferenceEventId());
    }


    /**
     *  Gets the <code> Event </code> to which the start of this event is 
     *  offset. 
     *
     *  @return the relative event 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartTime() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getStartReferenceEvent()
        throws org.osid.OperationFailedException {

        return (this.offsetEvent.getStartReferenceEvent());
    }


    /**
     *  Tests if this offset start time based on a fixed period of time. 
     *
     *  @return <code> true </code> if this offset is based on a fixed period 
     *          of time, <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartTime() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public boolean hasFixedStartOffset() {
        return (this.offsetEvent.hasFixedStartOffset());
    }


    /**
     *  Gets the fixed starting time offset. 
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartOffset() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedStartOffset() {
        return (this.offsetEvent.getFixedStartOffset());
    }


    /**
     *  Tests if this starting time offset is a based on a relative weekday. 
     *
     *  @return <code> true </code> if this offset is based on a relative 
     *          weekday, <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasFixedStartOffset() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public boolean hasRelativeWeekdayStartOffset() {
        return (this.offsetEvent.hasRelativeWeekdayStartOffset());
    }


    /**
     *  Gets the starting offset as the nth weekday from the relative event. 
     *  Zero is no offset. 
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasRelativeWeekdayStartOffset() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long getRelativeWeekdayStartOffset() {
        return (this.offsetEvent.getRelativeWeekdayStartOffset());
    }


    /**
     *  Gets the starting weekday number. The weekday is based on the calendar 
     *  type. On the Gregorian calendar, 0 is Sunday. 
     *
     *  @return the weekday number 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasRelativeWeekdayStartOffset() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long getRelativeStartWeekday() {
        return (this.offsetEvent.getRelativeStartWeekday());
    }


    /**
     *  Tests if this offset event is based on a fixed duration. 
     *
     *  @return <code> true </code> if this offset is based on a fixed 
     *          duration, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasFixedDuration() {
        return (this.offsetEvent.hasFixedDuration());
    }


    /**
     *  Gets the duration of the offset event. 
     *
     *  @param  units the units of the duration 
     *  @return the duration 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.NullArgumentException <code> units </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedDuration(org.osid.calendaring.DateTimeResolution units) {
        return (this.offsetEvent.getFixedDuration(units));
    }


    /**
     *  Gets the <code> Event Id </code> to which the end of this event is 
     *  offset. 
     *
     *  @return the relative event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndReferenceEventId() {
        return (this.offsetEvent.getEndReferenceEventId());
    }


    /**
     *  Gets the <code> Event </code> to which the end of this event is 
     *  offset. 
     *
     *  @return the relative event 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEndReferenceEvent()
        throws org.osid.OperationFailedException {

        return (this.offsetEvent.getEndReferenceEvent());
    }


    /**
     *  Tests if this offset end time based on a fixed period of time. 
     *
     *  @return <code> true </code> if this offset is based on a fixed period 
     *          of time, <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasFixedDuration() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public boolean hasFixedEndOffset() {
        return (this.offsetEvent.hasFixedEndOffset());
    }


    /**
     *  Gets the fixed ending time offset. 
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code> hasFixedEndOffset() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedEndOffset() {
        return (this.offsetEvent.getFixedEndOffset());
    }


    /**
     *  Tests if this ending time offset is a based on a relative weekday. 
     *
     *  @return <code> true </code> if this offset is based on a relative 
     *          weekday, <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasFixedEndOffset() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public boolean hasRelativeWeekdayEndOffset() {
        return (this.offsetEvent.hasRelativeWeekdayEndOffset());
    }


    /**
     *  Gets the ending offset as the nth weekday from the relative event. 
     *  Zero is no offset. 
     *
     *  @return the offset 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasRelativeWeekdayEndOffset() </code> is <code> false </code> 
     */

    @OSID @Override
    public long getRelativeWeekdayEndOffset() {
        return (this.offsetEvent.getRelativeWeekdayEndOffset());
    }


    /**
     *  Gets the ending weekday number. The weekday is based on the calendar 
     *  type. On the Gregorian calendar, 0 is Sunday. 
     *
     *  @return the weekday number 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasRelativeWeekdayEndOffset() </code> is <code> false </code> 
     */

    @OSID @Override
    public long getRelativeEndWeekday() {
        return (this.offsetEvent.getRelativeEndWeekday());
    }


    /**
     *  Gets a descriptive location. 
     *
     *  @return the location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.offsetEvent.getLocationDescription());
    }


    /**
     *  Tests if a location is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated location, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.offsetEvent.hasLocation());
    }


    /**
     *  Gets the location <code> Id </code> . 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        return (this.offsetEvent.getLocationId());
    }


    /**
     *  Gets the <code> Location. </code> 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        return (this.offsetEvent.getLocation());
    }


    /**
     *  Tests if a sponsor is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated sponsor. <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.offsetEvent.hasSponsors());
    }


    /**
     *  Gets the <code> Id </code> of the event sponsors. 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.offsetEvent.getSponsorIds());
    }


    /**
     *  Gets the <code> Sponsors. </code> 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.offsetEvent.getSponsors());
    }


    /**
     *  Gets the offset event record corresponding to the given <code> 
     *  OffsetEvent </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  offsetEventRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(offsetEventRecordType) </code> is <code> true </code> . 
     *
     *  @param  offsetEventRecordType the type of the record to retrieve 
     *  @return the offset event record 
     *  @throws org.osid.NullArgumentException <code> offsetEventRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(offsetEventRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventRecord getOffsetEventRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        return (this.offsetEvent.getOffsetEventRecord(offsetEventRecordType));
    }
}

