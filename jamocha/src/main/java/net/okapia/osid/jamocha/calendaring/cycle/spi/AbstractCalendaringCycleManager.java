//
// AbstractCalendaringCycleManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCalendaringCycleManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.calendaring.cycle.CalendaringCycleManager,
               org.osid.calendaring.cycle.CalendaringCycleProxyManager {

    private final Types cyclicEventRecordTypes             = new TypeRefSet();
    private final Types cyclicEventSearchRecordTypes       = new TypeRefSet();

    private final Types cyclicTimePeriodRecordTypes        = new TypeRefSet();
    private final Types cyclicTimePeriodSearchRecordTypes  = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCalendaringCycleManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCalendaringCycleManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a cyclic event lookup service is supported. a cyclic event 
     *  lookup service defines methods to access cyclic events. 
     *
     *  @return true if cyclic event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventLookup() {
        return (false);
    }


    /**
     *  Tests if a cyclic event query service is supported. 
     *
     *  @return <code> true </code> if cyclic event query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventQuery() {
        return (false);
    }


    /**
     *  Tests if a cyclic event search service is supported. 
     *
     *  @return <code> true </code> if cyclic event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventSearch() {
        return (false);
    }


    /**
     *  Tests if a cyclic event administrative service is supported. 
     *
     *  @return <code> true </code> if cyclic event admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventAdmin() {
        return (false);
    }


    /**
     *  Tests if cyclic event notification is supported. Messages may be sent 
     *  when cyclic events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if cyclic event notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventNotification() {
        return (false);
    }


    /**
     *  Tests if a cyclic event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if cyclic event calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventCalendar() {
        return (false);
    }


    /**
     *  Tests if a cyclic event to calendar assignment session is available. 
     *
     *  @return <code> true </code> if cyclic event calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if cyclic event smart calendaring is available. 
     *
     *  @return <code> true </code> if cyclic event smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a session to look up associations between events and cyclic 
     *  events is available. 
     *
     *  @return <code> true </code> if an event to cyclic event lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCyclicEventLookup() {
        return (false);
    }


    /**
     *  Tests if a session for manually assigning events to cyclic events is 
     *  available. 
     *
     *  @return <code> true </code> if an event to cyclic event assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCyclicEventAssignment() {
        return (false);
    }


    /**
     *  Tests if a cyclic time period lookup service is supported. 
     *
     *  @return <code> true </code> if cyclic time period lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodLookup() {
        return (false);
    }


    /**
     *  Tests if a cyclic time period search service is supported. 
     *
     *  @return <code> true </code> if cyclic time period search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodSearch() {
        return (false);
    }


    /**
     *  Tests if a cyclic time period administrative service is supported. 
     *
     *  @return <code> true </code> if cyclic time period admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodAdmin() {
        return (false);
    }


    /**
     *  Tests if cyclic time period notification is supported. Messages may be 
     *  sent when cyclic time periods are created, modified, or deleted. 
     *
     *  @return <code> true </code> if cyclic time period notification is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodNotification() {
        return (false);
    }


    /**
     *  Tests if a cyclic time period to calendar lookup session is available. 
     *
     *  @return <code> true </code> if cyclic time period calendar lookup 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodCalendar() {
        return (false);
    }


    /**
     *  Tests if a cyclic time period to calendar assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if cyclic time period calendar assignment 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if cyclic time period smart calendaring is available. 
     *
     *  @return <code> true </code> if cyclic time period smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a session to look up associations between time periods and 
     *  cyclic time periods is available. 
     *
     *  @return <code> true </code> if a time period to cyclic time period 
     *          lookup session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCyclicTimePeriodLookup() {
        return (false);
    }


    /**
     *  Tests if a session for manually assigning time periods to cyclic time 
     *  periods is available. 
     *
     *  @return <code> true </code> if a time period to cyclic time period 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCyclicTimePeriodAssignment() {
        return (false);
    }


    /**
     *  Tests if a calendaring cycle batch service is available. 
     *
     *  @return <code> true </code> if a calndaring cycle batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringCycleBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> CyclicEvent </code> record types. 
     *
     *  @return a list containing the supported <code> CyclicEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicEventRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.cyclicEventRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CyclicEvent </code> record type is 
     *  supported. 
     *
     *  @param  cyclicEventRecordType a <code> Type </code> indicating a 
     *          <code> CyclicEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cyclicEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCyclicEventRecordType(org.osid.type.Type cyclicEventRecordType) {
        return (this.cyclicEventRecordTypes.contains(cyclicEventRecordType));
    }


    /**
     *  Adds support for a cyclic event record type.
     *
     *  @param cyclicEventRecordType a cyclic event record type
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicEventRecordType</code> is <code>null</code>
     */

    protected void addCyclicEventRecordType(org.osid.type.Type cyclicEventRecordType) {
        this.cyclicEventRecordTypes.add(cyclicEventRecordType);
        return;
    }


    /**
     *  Removes support for a cyclic event record type.
     *
     *  @param cyclicEventRecordType a cyclic event record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicEventRecordType</code> is <code>null</code>
     */

    protected void removeCyclicEventRecordType(org.osid.type.Type cyclicEventRecordType) {
        this.cyclicEventRecordTypes.remove(cyclicEventRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CyclicEvent </code> search record types. 
     *
     *  @return a list containing the supported <code> CyclicEvent </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicEventSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.cyclicEventSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CyclicEvent </code> search record type is 
     *  supported. 
     *
     *  @param  cyclicEventSearchRecordType a <code> Type </code> indicating a 
     *          <code> CyclicEvent </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          cyclicEventSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCyclicEventSearchRecordType(org.osid.type.Type cyclicEventSearchRecordType) {
        return (this.cyclicEventSearchRecordTypes.contains(cyclicEventSearchRecordType));
    }


    /**
     *  Adds support for a cyclic event search record type.
     *
     *  @param cyclicEventSearchRecordType a cyclic event search record type
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicEventSearchRecordType</code> is <code>null</code>
     */

    protected void addCyclicEventSearchRecordType(org.osid.type.Type cyclicEventSearchRecordType) {
        this.cyclicEventSearchRecordTypes.add(cyclicEventSearchRecordType);
        return;
    }


    /**
     *  Removes support for a cyclic event search record type.
     *
     *  @param cyclicEventSearchRecordType a cyclic event search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicEventSearchRecordType</code> is <code>null</code>
     */

    protected void removeCyclicEventSearchRecordType(org.osid.type.Type cyclicEventSearchRecordType) {
        this.cyclicEventSearchRecordTypes.remove(cyclicEventSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CyclicTimePeriod </code> record types. 
     *
     *  @return a list containing the supported <code> CyclicTimePeriod 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicTimePeriodRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.cyclicTimePeriodRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CyclicTimePeriod </code> record type is 
     *  supported. 
     *
     *  @param  cyclicTimePeriodRecordType a <code> Type </code> indicating a 
     *          <code> CyclicTimePeriod </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          cyclicTimePeriodRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodRecordType(org.osid.type.Type cyclicTimePeriodRecordType) {
        return (this.cyclicTimePeriodRecordTypes.contains(cyclicTimePeriodRecordType));
    }


    /**
     *  Adds support for a cyclic time period record type.
     *
     *  @param cyclicTimePeriodRecordType a cyclic time period record type
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     */

    protected void addCyclicTimePeriodRecordType(org.osid.type.Type cyclicTimePeriodRecordType) {
        this.cyclicTimePeriodRecordTypes.add(cyclicTimePeriodRecordType);
        return;
    }


    /**
     *  Removes support for a cyclic time period record type.
     *
     *  @param cyclicTimePeriodRecordType a cyclic time period record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     */

    protected void removeCyclicTimePeriodRecordType(org.osid.type.Type cyclicTimePeriodRecordType) {
        this.cyclicTimePeriodRecordTypes.remove(cyclicTimePeriodRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CyclicTimePeriod </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CyclicTimePeriod 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicTimePeriodSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.cyclicTimePeriodSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CyclicTimePeriod </code> search record type 
     *  is supported. 
     *
     *  @param  cyclicTimePeriodSearchRecordType a <code> Type </code> 
     *          indicating a <code> CyclicTimePeriod </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          cyclicTimePeriodSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodSearchRecordType(org.osid.type.Type cyclicTimePeriodSearchRecordType) {
        return (this.cyclicTimePeriodSearchRecordTypes.contains(cyclicTimePeriodSearchRecordType));
    }


    /**
     *  Adds support for a cyclic time period search record type.
     *
     *  @param cyclicTimePeriodSearchRecordType a cyclic time period search record type
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicTimePeriodSearchRecordType</code> is <code>null</code>
     */

    protected void addCyclicTimePeriodSearchRecordType(org.osid.type.Type cyclicTimePeriodSearchRecordType) {
        this.cyclicTimePeriodSearchRecordTypes.add(cyclicTimePeriodSearchRecordType);
        return;
    }


    /**
     *  Removes support for a cyclic time period search record type.
     *
     *  @param cyclicTimePeriodSearchRecordType a cyclic time period search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>cyclicTimePeriodSearchRecordType</code> is <code>null</code>
     */

    protected void removeCyclicTimePeriodSearchRecordType(org.osid.type.Type cyclicTimePeriodSearchRecordType) {
        this.cyclicTimePeriodSearchRecordTypes.remove(cyclicTimePeriodSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  lookup service. 
     *
     *  @return a <code> CyclicEventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventLookupSession getCyclicEventLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventLookupSession getCyclicEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventLookupSession getCyclicEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventLookupSession getCyclicEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  query service. 
     *
     *  @return a <code> CyclicEventQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuerySession getCyclicEventQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuerySession getCyclicEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuerySession getCyclicEventQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuerySession getCyclicEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  search service. 
     *
     *  @return a <code> CyclicEventSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchSession getCyclicEventSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchSession getCyclicEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchSession getCyclicEventSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchSession getCyclicEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  administration service. 
     *
     *  @return a <code> CyclicEventAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventAdminSession getCyclicEventAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventAdminSession getCyclicEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventAdminSession getCyclicEventAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventAdminSession getCyclicEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to cyclic 
     *  event changes. 
     *
     *  @param  cyclicEventReceiver the cyclic event receiver 
     *  @return a <code> CyclicEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> cyclicEventReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventNotificationSession getCyclicEventNotificationSession(org.osid.calendaring.cycle.CyclicEventReceiver cyclicEventReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to event 
     *  changes. 
     *
     *  @param  eventReceiver the cyclic event receiver 
     *  @param  proxy a proxy 
     *  @return an <code> EventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> eventReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventNotificationSession getCyclicEventNotificationSession(org.osid.calendaring.cycle.CyclicEventReceiver eventReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  notification service for the given calendar. 
     *
     *  @param  cyclicEventReceiver the cyclic event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> cyclicEventReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventNotificationSession getCyclicEventNotificationSessionForCalendar(org.osid.calendaring.cycle.CyclicEventReceiver cyclicEventReceiver, 
                                                                                                                  org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  notification service for the given calendar. 
     *
     *  @param  eventReceiver the cyclic event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> eventReceiver, </code> 
     *          <code> calendarId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventNotificationSession getCyclicEventNotificationSessionForCalendar(org.osid.calendaring.cycle.CyclicEventReceiver eventReceiver, 
                                                                                                                  org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving cyclic event to calendar mappings. 
     *
     *  @return a <code> CyclicEventCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventCalendarSession getCyclicEventCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventCalendar() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventCalendarSession getCyclicEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning event to calendar mappings. 
     *
     *  @return a <code> CyclicEventCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventCalendarAssignmentSession getCyclicEventCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventCalendarAssignmentSession getCyclicEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the cyclic event smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSmartCalendarSession getCyclicEventSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the cyclic event smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSmartCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSmartCalendarSession getCyclicEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving event to cyclic event associations. 
     *
     *  @return an <code> EventCyclicEventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventLookupSession getEventCyclicEventLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getEventCyclicEventLookupSession not implemented");
    }


    /**
     *  Gets the session for retrieving event to cyclic event associations. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventLookupSession getEventCyclicEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getEventCyclicEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event cyclic 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventCyclicEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventLookupSession getEventCyclicEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getEventCyclicEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event cyclic 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventLookupSession getEventCyclicEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getEventCyclicEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for manually making event to cyclic event 
     *  associations. 
     *
     *  @return an <code> EventCyclicEventAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventAssignmentSession getEventCyclicEventAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getEventCyclicEventAssignmentSession not implemented");
    }


    /**
     *  Gets the session for manually making event to cyclic event 
     *  associations. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventAssignmentSession getEventCyclicEventAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getEventCyclicEventAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event cyclic 
     *  event assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventCyclicEventAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventAssignmentSession getEventCyclicEventAssignmentSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getEventCyclicEventAssignmentSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event cyclic 
     *  event assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventAssignmentSession getEventCyclicEventAssignmentSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getEventCyclicEventAssignmentSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period lookup service. 
     *
     *  @return a <code> CyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodLookupSession getCyclicTimePeriodLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodLookupSession getCyclicTimePeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodLookupSession getCyclicTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodLookupSession getCyclicTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period query service. 
     *
     *  @return a <code> CyclicTimePeriodQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuerySession getCyclicTimePeriodQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuerySession getCyclicTimePeriodQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicTimePeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuerySession getCyclicTimePeriodQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuerySession getCyclicTimePeriodQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period search service. 
     *
     *  @return a <code> CyclicTimePeriodSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSearchSession getCyclicTimePeriodSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSearchSession getCyclicTimePeriodSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicTimePeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSearchSession getCyclicTimePeriodSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSearchSession getCyclicTimePeriodSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period administration service. 
     *
     *  @return a <code> CyclicTimePeriodAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodAdminSession getCyclicTimePeriodAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodAdminSession getCyclicTimePeriodAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicTimePeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodAdminSession getCyclicTimePeriodAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> calendarId </code> not found 
     *  @throws org.osid.NotFoundException a <code> 
     *          CyclicTimePeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodAdminSession getCyclicTimePeriodAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to cyclic 
     *  time period changes. 
     *
     *  @param  cyclicTimePeriodReceiver the cyclic time period receiver 
     *  @return a <code> CyclicTimePeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodNotificationSession getCyclicTimePeriodNotificationSession(org.osid.calendaring.cycle.CyclicTimePeriodReceiver cyclicTimePeriodReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to cyclic 
     *  time period changes. 
     *
     *  @param  cyclicTimePeriodReceiver the cyclic time period receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodNotificationSession getCyclicTimePeriodNotificationSession(org.osid.calendaring.cycle.CyclicTimePeriodReceiver cyclicTimePeriodReceiver, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period notification service for the given calendar. 
     *
     *  @param  cyclicTimePeriodReceiver the cyclic time period receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return <code> a CyclicTimePeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodNotificationSession getCyclicTimePeriodNotificationSessionForCalendar(org.osid.calendaring.cycle.CyclicTimePeriodReceiver cyclicTimePeriodReceiver, 
                                                                                                                            org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period notification service for the given calendar. 
     *
     *  @param  cyclicTimePeriodReceiver the cyclic time period receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> a CyclicTimePeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodReceiver 
     *          </code> or <code> calendarId </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodNotificationSession getCyclicTimePeriodNotificationSessionForCalendar(org.osid.calendaring.cycle.CyclicTimePeriodReceiver cyclicTimePeriodReceiver, 
                                                                                                                            org.osid.id.Id calendarId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving cyclic time period to calendar 
     *  mappings. 
     *
     *  @return a <code> CyclicTimePeriodCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodCalendarSession getCyclicTimePeriodCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving cyclic time period to calendar 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodCalendarSession getCyclicTimePeriodCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning cyclic time period to calendar 
     *  mappings. 
     *
     *  @return a <code> CyclicTimePeriodCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodCalendarAssignmentSession getCyclicTimePeriodCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning cyclic time period to calendar 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodCalendarAssignmentSession getCyclicTimePeriodCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the cyclic time period smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CyclicTimePeriodSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSmartCalendarSession getCyclicTimePeriodSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCyclicTimePeriodSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the cyclic time period smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSmartCalendarSession getCyclicTimePeriodSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCyclicTimePeriodSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving time period to cyclic time period 
     *  associations. 
     *
     *  @return a <code> TimePeriodCyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodLookupSession getTimePeriodCyclicTimePeriodLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getTimePeriodCyclicTimePeriodLookupSession not implemented");
    }


    /**
     *  Gets the session for retrieving time period to cyclic time period 
     *  associations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodLookupSession getTimePeriodCyclicTimePeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getTimePeriodCyclicTimePeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  cyclic time period lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> TimePeriodCyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodLookupSession getTimePeriodCyclicTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getTimePeriodCyclicTimePeriodLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  cyclic time period lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodLookupSession getTimePeriodCyclicTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getTimePeriodCyclicTimePeriodLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for manually making time period to cyclic time period 
     *  associations. 
     *
     *  @return a <code> TimePeriodCyclicTimePeriodAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodAssignmentSession getTimePeriodCyclicTimePeriodAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getTimePeriodCyclicTimePeriodAssignmentSession not implemented");
    }


    /**
     *  Gets the session for manually making time period to cyclic time period 
     *  associations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodAssignmentSession getTimePeriodCyclicTimePeriodAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getTimePeriodCyclicTimePeriodAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  cyclic time period assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> TimePeriodCyclicTimePeriodAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodAssignment() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodAssignmentSession getTimePeriodCyclicTimePeriodAssignmentSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getTimePeriodCyclicTimePeriodAssignmentSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  cyclic time period assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodAssignment() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodAssignmentSession getTimePeriodCyclicTimePeriodAssignmentSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getTimePeriodCyclicTimePeriodAssignmentSessionForCalendar not implemented");
    }


    /**
     *  Gets a <code> CalendaringCycleBatchManager. </code> 
     *
     *  @return a <code> CalendaringCycleBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringCycleBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CalendaringCycleBatchManager getCalendaringCycleBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleManager.getCalendaringCycleBatchManager not implemented");
    }


    /**
     *  Gets a <code> CalendaringCycleBatchProxyManager. </code> 
     *
     *  @return a <code> CalendaringCycleBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringCycleBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager getCalendaringCycleBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.CalendaringCycleProxyManager.getCalendaringCycleBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.cyclicEventRecordTypes.clear();
        this.cyclicEventRecordTypes.clear();

        this.cyclicEventSearchRecordTypes.clear();
        this.cyclicEventSearchRecordTypes.clear();

        this.cyclicTimePeriodRecordTypes.clear();
        this.cyclicTimePeriodRecordTypes.clear();

        this.cyclicTimePeriodSearchRecordTypes.clear();
        this.cyclicTimePeriodSearchRecordTypes.clear();

        return;
    }
}
