//
// AbstractOrderLookupSession.java
//
//    A starter implementation framework for providing an Order
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Order
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getOrders(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractOrderLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ordering.OrderLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.ordering.Store store = new net.okapia.osid.jamocha.nil.ordering.store.UnknownStore();
    

    /**
     *  Gets the <code>Store/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.store.getId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.store);
    }


    /**
     *  Sets the <code>Store</code>.
     *
     *  @param  store the store for this session
     *  @throws org.osid.NullArgumentException <code>store</code>
     *          is <code>null</code>
     */

    protected void setStore(org.osid.ordering.Store store) {
        nullarg(store, "store");
        this.store = store;
        return;
    }


    /**
     *  Tests if this user can perform <code>Order</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOrders() {
        return (true);
    }


    /**
     *  A complete view of the <code>Order</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOrderView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Order</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOrderView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include orders in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Order</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Order</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Order</code> and
     *  retained for compatibility.
     *
     *  @param  orderId <code>Id</code> of the
     *          <code>Order</code>
     *  @return the order
     *  @throws org.osid.NotFoundException <code>orderId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>orderId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder(org.osid.id.Id orderId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ordering.OrderList orders = getOrders()) {
            while (orders.hasNext()) {
                org.osid.ordering.Order order = orders.getNextOrder();
                if (order.getId().equals(orderId)) {
                    return (order);
                }
            }
        } 

        throw new org.osid.NotFoundException(orderId + " not found");
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  orders specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Orders</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getOrders()</code>.
     *
     *  @param  orderIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>orderIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByIds(org.osid.id.IdList orderIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ordering.Order> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = orderIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getOrder(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("order " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ordering.order.LinkedOrderList(ret));
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  order genus <code>Type</code> which does not include
     *  orders of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getOrders()</code>.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderGenusFilterList(getOrders(), orderGenusType));
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  order genus <code>Type</code> and include any additional
     *  orders with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOrders()</code>.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByParentGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOrdersByGenusType(orderGenusType));
    }


    /**
     *  Gets an <code>OrderList</code> containing the given
     *  order record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOrders()</code>.
     *
     *  @param  orderRecordType an order record type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByRecordType(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderRecordFilterList(getOrders(), orderRecordType));
    }


    /**
     *  Gets a list of all orders corresponding to a customer
     *  <code>Id</code> In plenary mode, the returned list contains
     *  all known orders or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId the <code>Id</code> of the customer 
     *  @return the returned <code>OrderList /code> 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilterList(new CustomerFilter(resourceId), getOrders()));
    }


    /**
     *  Gets a list of all orders corresponding to a date range. Entries are 
     *  returned with a submitted date that falsl between the requested dates 
     *  inclusive. In plenary mode, the returned list contains all known 
     *  orders or an error results. Otherwise, the returned list may contain 
     *  only those entries that are accessible through this session. 
     *
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>OrderList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilterList(new SubmitDateFilter(from, to), getOrders()));
    }


    /**
     *  Gets a list of all orders corresponding to a customer
     *  <code>Id</code> and date range. Entries are returned with
     *  submit dates that fall between the requested dates
     *  inclusive. In plenary mode, the returned list contains all
     *  known orders or an error results.  Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId the <code>Id</code> of the customer 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>OrderList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomerAndDate(org.osid.id.Id resourceId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilterList(new SubmitDateFilter(from, to), getOrdersByCustomer(resourceId)));
    }


    /**
     *  Gets a list of all orders corresponding to a product
     *  <code>Id</code> In plenary mode, the returned list contains
     *  all known orders or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  productId the <code>Id</code> of the product 
     *  @return the returned <code>OrderList /code> 
     *  @throws org.osid.NullArgumentException <code>productId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProduct(org.osid.id.Id productId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ordering.Order> ret = new java.util.ArrayList<>();

        try (org.osid.ordering.OrderList orders = getOrders()) {
            while (orders.hasNext()) {
                org.osid.ordering.Order order = orders.getNextOrder();
                try (org.osid.ordering.ItemList items = order.getItems()) {
                    while (items.hasNext()) {
                        if (items.getNextItem().getProductId().equals(productId)) {
                            ret.add(order);
                        }
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ordering.order.LinkedOrderList(ret));
    }


    /**
     *  Gets a list of all orders corresponding to a product
     *  <code>Id</code> and date range. Entries are returned with
     *  submit dates that fall between the requested dates
     *  inclusive. In plenary mode, the returned list contains all
     *  known orders or an error results.  Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  productId the <code>Id</code> of the product 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>OrderList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>productId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProductAndDate(org.osid.id.Id productId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilterList(new SubmitDateFilter(from, to), getOrdersForProduct(productId)));
    }


    /**
     *  Gets all <code>Orders</code>. 
     *
     *  In plenary mode, the returned list contains all known orders
     *  or an error results. Otherwise, the returned list may contain
     *  only those orders that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Orders</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.ordering.OrderList getOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets all submitted and not closed orders. In plenary mode, the
     *  returned list contains all known entries or an error results.
     *  Otherwise, the returned list may contain only those entries
     *  that are accessible through this session.
     *
     *  @return a list of orders 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOpenOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilterList(new OpenFilter(), getOrders()));
    }


    /**
     *  Gets all closed orders. In plenary mode, the returned list
     *  contains all known entries or an error results. Otherwise, the
     *  returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @return a list of orders 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getClosedOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilterList(new ClosedFilter(), getOrders()));
    }        


    /**
     *  Filters the order list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of orders
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.ordering.OrderList filterOrdersOnViews(org.osid.ordering.OrderList list)
        throws org.osid.OperationFailedException {

        return (list);
    }

    
    public static class CustomerFilter
        implements net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>CustomerFilterList</code>.
         *
         *  @param resourceId the customer to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public CustomerFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the OrderFilterList to filter the order list
         *  based on customer.
         *
         *  @param order the order
         *  @return <code>true</code> to pass the order,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.ordering.Order order) {
            return (order.getCustomerId().equals(this.resourceId));
        }
    }    


    public static class SubmitDateFilter
        implements net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;

        
        /**
         *  Constructs a new <code>SubmitDateFilterList</code>.
         *
         *  @param from start date
         *  @param to end date
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public SubmitDateFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to = to;
;
            return;
        }


        /**
         *  Used by the SubmitDateFilterList to filter the order list
         *  based on submit date.
         *
         *  @param order the order
         *  @return <code>true</code> to pass the order,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.ordering.Order order) {
            if (order.getSubmitDate().isLess(this.from)) {
                return (false);
            }

            if (order.getSubmitDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }    


    public static class OpenFilter
        implements net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilter {


        /**
         *  Used by the OrderFilterList to filter the order list
         *  based on open status.
         *
         *  @param order the order
         *  @return <code>true</code> to pass the order,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.ordering.Order order) {
            return (order.isSubmitted() && !order.isClosed());
        }
    }    


    public static class ClosedFilter
        implements net.okapia.osid.jamocha.inline.filter.ordering.order.OrderFilter {


        /**
         *  Used by the OrderFilterList to filter the order list
         *  based on open status.
         *
         *  @param order the order
         *  @return <code>true</code> to pass the order,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.ordering.Order order) {
            return (order.isClosed());
        }
    }    
}
