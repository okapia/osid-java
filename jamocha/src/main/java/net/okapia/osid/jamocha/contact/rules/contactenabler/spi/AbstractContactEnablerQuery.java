//
// AbstractContactEnablerQuery.java
//
//     A template for making a ContactEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.rules.contactenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for contact enablers.
 */

public abstract class AbstractContactEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.contact.rules.ContactEnablerQuery {

    private final java.util.Collection<org.osid.contact.rules.records.ContactEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the contact. 
     *
     *  @param  contactId the contact book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> contactBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledContactId(org.osid.id.Id contactId, boolean match) {
        return;
    }


    /**
     *  Clears the contact <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledContactIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ContactBookQuery </code> is available. 
     *
     *  @return <code> true </code> if a contact query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledContactQuery() {
        return (false);
    }


    /**
     *  Gets the query for a contact. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuery getRuledContactQuery() {
        throw new org.osid.UnimplementedException("supportsRuledContactQuery() is false");
    }


    /**
     *  Matches enablers mapped to any contact. 
     *
     *  @param  match <code> true </code> for enablers mapped to any contact, 
     *          <code> false </code> to match enablers mapped to no contact 
     */

    @OSID @Override
    public void matchAnyRuledContact(boolean match) {
        return;
    }


    /**
     *  Clears the contact query terms. 
     */

    @OSID @Override
    public void clearRuledContactTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the address book. 
     *
     *  @param  addressBookId the address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressBookId(org.osid.id.Id addressBookId, boolean match) {
        return;
    }


    /**
     *  Clears the address book <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAddressBookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAddressBookQuery() is false");
    }


    /**
     *  Clears the address book query terms. 
     */

    @OSID @Override
    public void clearAddressBookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given contact enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a contact enabler implementing the requested record.
     *
     *  @param contactEnablerRecordType a contact enabler record type
     *  @return the contact enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.rules.records.ContactEnablerQueryRecord getContactEnablerQueryRecord(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.rules.records.ContactEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(contactEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this contact enabler query. 
     *
     *  @param contactEnablerQueryRecord contact enabler query record
     *  @param contactEnablerRecordType contactEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addContactEnablerQueryRecord(org.osid.contact.rules.records.ContactEnablerQueryRecord contactEnablerQueryRecord, 
                                          org.osid.type.Type contactEnablerRecordType) {

        addRecordType(contactEnablerRecordType);
        nullarg(contactEnablerQueryRecord, "contact enabler query record");
        this.records.add(contactEnablerQueryRecord);        
        return;
    }
}
