//
// AbstractBudgetLookupSession.java
//
//    A starter implementation framework for providing a Budget
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Budget
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBudgets(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBudgetLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.budgeting.BudgetLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Budget</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBudgets() {
        return (true);
    }


    /**
     *  A complete view of the <code>Budget</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBudgetView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Budget</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBudgetView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include budgets in businesses which are children of
     *  this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only budgets whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveBudgetView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All budgets of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveBudgetView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Budget</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Budget</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Budget</code> and retained for
     *  compatibility.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  budgetId <code>Id</code> of the
     *          <code>Budget</code>
     *  @return the budget
     *  @throws org.osid.NotFoundException <code>budgetId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>budgetId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.Budget getBudget(org.osid.id.Id budgetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.budgeting.BudgetList budgets = getBudgets()) {
            while (budgets.hasNext()) {
                org.osid.financials.budgeting.Budget budget = budgets.getNextBudget();
                if (budget.getId().equals(budgetId)) {
                    return (budget);
                }
            }
        } 

        throw new org.osid.NotFoundException(budgetId + " not found");
    }


    /**
     *  Gets a <code>BudgetList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  budgets specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Budgets</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, budgets are returned that are currently effective.
     *  In any effective mode, effective budgets and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBudgets()</code>.
     *
     *  @param  budgetIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByIds(org.osid.id.IdList budgetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.budgeting.Budget> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = budgetIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBudget(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("budget " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.budgeting.budget.LinkedBudgetList(ret));
    }


    /**
     *  Gets a <code>BudgetList</code> corresponding to the given
     *  budget genus <code>Type</code> which does not include
     *  budgets of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently effective.
     *  In any effective mode, effective budgets and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBudgets()</code>.
     *
     *  @param  budgetGenusType a budget genus type 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByGenusType(org.osid.type.Type budgetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.BudgetGenusFilterList(getBudgets(), budgetGenusType));
    }


    /**
     *  Gets a <code>BudgetList</code> corresponding to the given
     *  budget genus <code>Type</code> and include any additional
     *  budgets with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBudgets()</code>.
     *
     *  @param  budgetGenusType a budget genus type 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByParentGenusType(org.osid.type.Type budgetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBudgetsByGenusType(budgetGenusType));
    }


    /**
     *  Gets a <code>BudgetList</code> containing the given
     *  budget record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBudgets()</code>.
     *
     *  @param  budgetRecordType a budget record type 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByRecordType(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.BudgetRecordFilterList(getBudgets(), budgetRecordType));
    }


    /**
     *  Gets a <code>BudgetList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible
     *  through this session.
     *  
     *  In active mode, budgets are returned that are currently
     *  active. In any status mode, active and inactive budgets
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsOnDate(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.TemporalBudgetFilterList(getBudgets(), from, to));
    }
        

    /**
     *  Gets a <code>BudgetList</code> for the given activity. 
     *  
     *  In plenary mode, the returned list contains all known mode,s or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  budgets that are accessible through this session. 
     *  
     *  In effective mode, budgets are returned that are currently effective. 
     *  In any effective mode, effective budgets and those currently expired 
     *  are returned. 
     *
     *  @param  activityId an activity <code>Id /code> 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.BudgetFilterList(new ActivityFilter(activityId), getBudgets()));
    }        


    /**
     *  Gets a <code>BudgetList</code> for the given activity and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityId</code>, <code>from</code> or
     *          <code>to</code> </code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityOnDate(org.osid.id.Id activityId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.TemporalBudgetFilterList(getBudgetsForActivity(activityId), from, to));
    }


    /**
     *  Gets a <code>BudgetList</code> for the given fiscal period. 
     *  
     *  In plenary mode, the returned list contains all known mode,s
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  fiscalPeriodId a fiscal period <code>Id</code> 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.BudgetFilterList(new FiscalPeriodFilter(fiscalPeriodId), getBudgets()));
    }


    /**
     *  Gets a <code>BudgetList</code> for the given fiscal period and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  fiscalPeriodId a fiscal period <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodId</code>, <code>from</code> or to
     *          </code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForFiscalPeriodOnDate(org.osid.id.Id fiscalPeriodId, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.TemporalBudgetFilterList(getBudgetsForFiscalPeriod(fiscalPeriodId), from, to));
    }
        

    /**
     *  Gets a <code>BudgetList</code> for the given activity and
     *  fiscal period.
     *  
     *  In plenary mode, the returned list contains all known mode,s
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity <code>Id</code> 
     *  @param  fiscalPeriodId a fiscal period <code>Id</code> 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          or <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityAndFiscalPeriod(org.osid.id.Id activityId, 
                                                                                         org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.BudgetFilterList(new FiscalPeriodFilter(fiscalPeriodId), getBudgetsForActivity(activityId)));
    }


    /**
     *  Gets a <code>BudgetList</code> for the given activity, fiscal
     *  period, and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known budgets
     *  or an error results. Otherwise, the returned list may contain
     *  only those budgets that are accessible through this session.
     *  
     *  In effective mode, budgets are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective budgets and those
     *  currently expired are returned.
     *
     *  @param  activityId an activity <code>Id</code> 
     *  @param  fiscalPeriodId a fiscal period <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Budget</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>activityId</code>, <code>fiscalPeriodId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsForActivityAndFiscalPeriodOnDate(org.osid.id.Id activityId, 
                                                                                               org.osid.id.Id fiscalPeriodId, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.TemporalBudgetFilterList(getBudgetsForActivityAndFiscalPeriod(activityId, fiscalPeriodId), from, to));
    }        


    /**
     *  Gets all <code>Budgets</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  budgets or an error results. Otherwise, the returned list
     *  may contain only those budgets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budgets are returned that are currently
     *  effective.  In any effective mode, effective budgets and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Budgets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.financials.budgeting.BudgetList getBudgets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the budget list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of budgets
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.financials.budgeting.BudgetList filterBudgetsOnViews(org.osid.financials.budgeting.BudgetList list)
        throws org.osid.OperationFailedException {

        org.osid.financials.budgeting.BudgetList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.EffectiveBudgetFilterList(ret);
        }

        return (ret);
    }

    
    public static class FiscalPeriodFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.BudgetFilter {

        private final org.osid.id.Id fiscalPeriodId;

        
        /**
         *  Constructs a new <code>FiscalPeriodFilterList</code>.
         *
         *  @param fiscalPeriodId the fiscal period to filter
         *  @throws org.osid.NullArgumentException
         *          <code>fiscalPeriodId</code> is <code>null</code>
         */

        public FiscalPeriodFilter(org.osid.id.Id fiscalPeriodId) {
            nullarg(fiscalPeriodId, "fiscal period Id");
            this.fiscalPeriodId = fiscalPeriodId;
            return;
        }


        /**
         *  Used by the BudgetFilterList to filter the post entry
         *  list based on fiscal period.
         *
         *  @param budget the post entry
         *  @return <code>true</code> to pass the post entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.budgeting.Budget budget) {
            return (budget.getFiscalPeriodId().equals(this.fiscalPeriodId));
        }
    }    


    public static class ActivityFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.budgeting.budget.BudgetFilter {

        private final org.osid.id.Id activityId;

        
        /**
         *  Constructs a new <code>ActivityFilterList</code>.
         *
         *  @param activityId the activity to filter
         *  @throws org.osid.NullArgumentException
         *          <code>activityId</code> is <code>null</code>
         */

        public ActivityFilter(org.osid.id.Id activityId) {
            nullarg(activityId, "activity Id");
            this.activityId = activityId;
            return;
        }


        /**
         *  Used by the BudgetFilterList to filter the post entry
         *  list based on activity.
         *
         *  @param budget the post entry
         *  @return <code>true</code> to pass the post entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.budgeting.Budget budget) {
            return (budget.getActivityId().equals(this.activityId));
        }
    }    
}
