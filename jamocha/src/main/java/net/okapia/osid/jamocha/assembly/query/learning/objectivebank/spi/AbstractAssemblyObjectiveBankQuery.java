//
// AbstractAssemblyObjectiveBankQuery.java
//
//     An ObjectiveBankQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.learning.objectivebank.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ObjectiveBankQuery that stores terms.
 */

public abstract class AbstractAssemblyObjectiveBankQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.learning.ObjectiveBankQuery,
               org.osid.learning.ObjectiveBankQueryInspector,
               org.osid.learning.ObjectiveBankSearchOrder {

    private final java.util.Collection<org.osid.learning.records.ObjectiveBankQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ObjectiveBankQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ObjectiveBankSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyObjectiveBankQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyObjectiveBankQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveId(org.osid.id.Id objectiveId, boolean match) {
        getAssembler().addIdTerm(getObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveIdTerms() {
        getAssembler().clearTerms(getObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getObjectiveIdColumn()));
    }


    /**
     *  Gets the ObjectiveId column name.
     *
     * @return the column name
     */

    protected String getObjectiveIdColumn() {
        return ("objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveQuery() is false");
    }


    /**
     *  Matches an objective bank that has any objective assigned. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          objective, <code> false </code> to match objective banks with 
     *          no objectives 
     */

    @OSID @Override
    public void matchAnyObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the objective terms. 
     */

    @OSID @Override
    public void clearObjectiveTerms() {
        getAssembler().clearTerms(getObjectiveColumn());
        return;
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the Objective column name.
     *
     * @return the column name
     */

    protected String getObjectiveColumn() {
        return ("objective");
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if a <code> ActivityQuery </code> is available for querying 
     *  activities. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches an objective bank that has any activity assigned. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          activity, <code> false </code> to match objective banks with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.learning.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.learning.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query to match 
     *  objective banks that have the specified objective bank as an ancestor. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                             boolean match) {
        getAssembler().addIdTerm(getAncestorObjectiveBankIdColumn(), objectiveBankId, match);
        return;
    }


    /**
     *  Clears the ancestor objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveBankIdTerms() {
        getAssembler().clearTerms(getAncestorObjectiveBankIdColumn());
        return;
    }


    /**
     *  Gets the ancestor objective bank <code> Id </code> query terms. 
     *
     *  @return the ancestor objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorObjectiveBankIdTerms() {
        return (getAssembler().getIdTerms(getAncestorObjectiveBankIdColumn()));
    }


    /**
     *  Gets the AncestorObjectiveBankId column name.
     *
     * @return the column name
     */

    protected String getAncestorObjectiveBankIdColumn() {
        return ("ancestor_objective_bank_id");
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  ancestor objective banks. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorObjectiveBankQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getAncestorObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorObjectiveBankQuery() is false");
    }


    /**
     *  Matches an objective bank that has any ancestor. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          ancestor, <code> false </code> to match root objective banks 
     */

    @OSID @Override
    public void matchAnyAncestorObjectiveBank(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorObjectiveBankColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor objective bank terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveBankTerms() {
        getAssembler().clearTerms(getAncestorObjectiveBankColumn());
        return;
    }


    /**
     *  Gets the ancestor objective bank query terms. 
     *
     *  @return the ancestor objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getAncestorObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }


    /**
     *  Gets the AncestorObjectiveBank column name.
     *
     * @return the column name
     */

    protected String getAncestorObjectiveBankColumn() {
        return ("ancestor_objective_bank");
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query to match 
     *  objective banks that have the specified objective bank as a 
     *  descendant. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                               boolean match) {
        getAssembler().addIdTerm(getDescendantObjectiveBankIdColumn(), objectiveBankId, match);
        return;
    }


    /**
     *  Clears the descendant objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveBankIdTerms() {
        getAssembler().clearTerms(getDescendantObjectiveBankIdColumn());
        return;
    }


    /**
     *  Gets the descendant objective bank <code> Id </code> query terms. 
     *
     *  @return the descendant objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantObjectiveBankIdTerms() {
        return (getAssembler().getIdTerms(getDescendantObjectiveBankIdColumn()));
    }


    /**
     *  Gets the DescendantObjectiveBankId column name.
     *
     * @return the column name
     */

    protected String getDescendantObjectiveBankIdColumn() {
        return ("descendant_objective_bank_id");
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  descendant objective banks. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantObjectiveBankQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getDescendantObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantObjectiveBankQuery() is false");
    }


    /**
     *  Matches an objective bank that has any descendant. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          descendant, <code> false </code> to match leaf objective banks 
     */

    @OSID @Override
    public void matchAnyDescendantObjectiveBank(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantObjectiveBankColumn(), match);
        return;
    }


    /**
     *  Clears the descendant objective bank terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveBankTerms() {
        getAssembler().clearTerms(getDescendantObjectiveBankColumn());
        return;
    }


    /**
     *  Gets the descendant objective bank query terms. 
     *
     *  @return the descendant objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getDescendantObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }


    /**
     *  Gets the DescendantObjectiveBank column name.
     *
     * @return the column name
     */

    protected String getDescendantObjectiveBankColumn() {
        return ("descendant_objective_bank");
    }


    /**
     *  Tests if this objectiveBank supports the given record
     *  <code>Type</code>.
     *
     *  @param  objectiveBankRecordType an objective bank record type 
     *  @return <code>true</code> if the objectiveBankRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type objectiveBankRecordType) {
        for (org.osid.learning.records.ObjectiveBankQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(objectiveBankRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  objectiveBankRecordType the objective bank record type 
     *  @return the objective bank query record 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveBankRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveBankQueryRecord getObjectiveBankQueryRecord(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveBankQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(objectiveBankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveBankRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  objectiveBankRecordType the objective bank record type 
     *  @return the objective bank query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveBankRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveBankQueryInspectorRecord getObjectiveBankQueryInspectorRecord(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveBankQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(objectiveBankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveBankRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param objectiveBankRecordType the objective bank record type
     *  @return the objective bank search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveBankRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveBankSearchOrderRecord getObjectiveBankSearchOrderRecord(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveBankSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(objectiveBankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveBankRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this objective bank. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param objectiveBankQueryRecord the objective bank query record
     *  @param objectiveBankQueryInspectorRecord the objective bank query inspector
     *         record
     *  @param objectiveBankSearchOrderRecord the objective bank search order record
     *  @param objectiveBankRecordType objective bank record type
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankQueryRecord</code>,
     *          <code>objectiveBankQueryInspectorRecord</code>,
     *          <code>objectiveBankSearchOrderRecord</code> or
     *          <code>objectiveBankRecordTypeobjectiveBank</code> is
     *          <code>null</code>
     */
            
    protected void addObjectiveBankRecords(org.osid.learning.records.ObjectiveBankQueryRecord objectiveBankQueryRecord, 
                                      org.osid.learning.records.ObjectiveBankQueryInspectorRecord objectiveBankQueryInspectorRecord, 
                                      org.osid.learning.records.ObjectiveBankSearchOrderRecord objectiveBankSearchOrderRecord, 
                                      org.osid.type.Type objectiveBankRecordType) {

        addRecordType(objectiveBankRecordType);

        nullarg(objectiveBankQueryRecord, "objective bank query record");
        nullarg(objectiveBankQueryInspectorRecord, "objective bank query inspector record");
        nullarg(objectiveBankSearchOrderRecord, "objective bank search odrer record");

        this.queryRecords.add(objectiveBankQueryRecord);
        this.queryInspectorRecords.add(objectiveBankQueryInspectorRecord);
        this.searchOrderRecords.add(objectiveBankSearchOrderRecord);
        
        return;
    }
}
