//
// MutableMapProxyBudgetLookupSession
//
//    Implements a Budget lookup service backed by a collection of
//    budgets that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.budgeting;


/**
 *  Implements a Budget lookup service backed by a collection of
 *  budgets. The budgets are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of budgets can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBudgetLookupSession
    extends net.okapia.osid.jamocha.core.financials.budgeting.spi.AbstractMapBudgetLookupSession
    implements org.osid.financials.budgeting.BudgetLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyBudgetLookupSession}
     *  with no budgets.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyBudgetLookupSession(org.osid.financials.Business business,
                                                  org.osid.proxy.Proxy proxy) {
        setBusiness(business);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBudgetLookupSession} with a
     *  single budget.
     *
     *  @param business the business
     *  @param budget a budget
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code budget}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBudgetLookupSession(org.osid.financials.Business business,
                                                org.osid.financials.budgeting.Budget budget, org.osid.proxy.Proxy proxy) {
        this(business, proxy);
        putBudget(budget);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBudgetLookupSession} using an
     *  array of budgets.
     *
     *  @param business the business
     *  @param budgets an array of budgets
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code budgets}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBudgetLookupSession(org.osid.financials.Business business,
                                                org.osid.financials.budgeting.Budget[] budgets, org.osid.proxy.Proxy proxy) {
        this(business, proxy);
        putBudgets(budgets);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBudgetLookupSession} using a
     *  collection of budgets.
     *
     *  @param business the business
     *  @param budgets a collection of budgets
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code budgets}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBudgetLookupSession(org.osid.financials.Business business,
                                                java.util.Collection<? extends org.osid.financials.budgeting.Budget> budgets,
                                                org.osid.proxy.Proxy proxy) {
   
        this(business, proxy);
        setSessionProxy(proxy);
        putBudgets(budgets);
        return;
    }

    
    /**
     *  Makes a {@code Budget} available in this session.
     *
     *  @param budget an budget
     *  @throws org.osid.NullArgumentException {@code budget{@code 
     *          is {@code null}
     */

    @Override
    public void putBudget(org.osid.financials.budgeting.Budget budget) {
        super.putBudget(budget);
        return;
    }


    /**
     *  Makes an array of budgets available in this session.
     *
     *  @param budgets an array of budgets
     *  @throws org.osid.NullArgumentException {@code budgets{@code 
     *          is {@code null}
     */

    @Override
    public void putBudgets(org.osid.financials.budgeting.Budget[] budgets) {
        super.putBudgets(budgets);
        return;
    }


    /**
     *  Makes collection of budgets available in this session.
     *
     *  @param budgets
     *  @throws org.osid.NullArgumentException {@code budget{@code 
     *          is {@code null}
     */

    @Override
    public void putBudgets(java.util.Collection<? extends org.osid.financials.budgeting.Budget> budgets) {
        super.putBudgets(budgets);
        return;
    }


    /**
     *  Removes a Budget from this session.
     *
     *  @param budgetId the {@code Id} of the budget
     *  @throws org.osid.NullArgumentException {@code budgetId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBudget(org.osid.id.Id budgetId) {
        super.removeBudget(budgetId);
        return;
    }    
}
