//
// AbstractIndexedMapSubscriptionEnablerLookupSession.java
//
//    A simple framework for providing a SubscriptionEnabler lookup service
//    backed by a fixed collection of subscription enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SubscriptionEnabler lookup service backed by a
 *  fixed collection of subscription enablers. The subscription enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some subscription enablers may be compatible
 *  with more types than are indicated through these subscription enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SubscriptionEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSubscriptionEnablerLookupSession
    extends AbstractMapSubscriptionEnablerLookupSession
    implements org.osid.subscription.rules.SubscriptionEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.subscription.rules.SubscriptionEnabler> subscriptionEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.rules.SubscriptionEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.subscription.rules.SubscriptionEnabler> subscriptionEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.rules.SubscriptionEnabler>());


    /**
     *  Makes a <code>SubscriptionEnabler</code> available in this session.
     *
     *  @param  subscriptionEnabler a subscription enabler
     *  @throws org.osid.NullArgumentException <code>subscriptionEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSubscriptionEnabler(org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler) {
        super.putSubscriptionEnabler(subscriptionEnabler);

        this.subscriptionEnablersByGenus.put(subscriptionEnabler.getGenusType(), subscriptionEnabler);
        
        try (org.osid.type.TypeList types = subscriptionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.subscriptionEnablersByRecord.put(types.getNextType(), subscriptionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a subscription enabler from this session.
     *
     *  @param subscriptionEnablerId the <code>Id</code> of the subscription enabler
     *  @throws org.osid.NullArgumentException <code>subscriptionEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSubscriptionEnabler(org.osid.id.Id subscriptionEnablerId) {
        org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler;
        try {
            subscriptionEnabler = getSubscriptionEnabler(subscriptionEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.subscriptionEnablersByGenus.remove(subscriptionEnabler.getGenusType());

        try (org.osid.type.TypeList types = subscriptionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.subscriptionEnablersByRecord.remove(types.getNextType(), subscriptionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSubscriptionEnabler(subscriptionEnablerId);
        return;
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> corresponding to the given
     *  subscription enabler genus <code>Type</code> which does not include
     *  subscription enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known subscription enablers or an error results. Otherwise,
     *  the returned list may contain only those subscription enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  subscriptionEnablerGenusType a subscription enabler genus type 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByGenusType(org.osid.type.Type subscriptionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.ArraySubscriptionEnablerList(this.subscriptionEnablersByGenus.get(subscriptionEnablerGenusType)));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> containing the given
     *  subscription enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known subscription enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  subscription enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  subscriptionEnablerRecordType a subscription enabler record type 
     *  @return the returned <code>subscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByRecordType(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.ArraySubscriptionEnablerList(this.subscriptionEnablersByRecord.get(subscriptionEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.subscriptionEnablersByGenus.clear();
        this.subscriptionEnablersByRecord.clear();

        super.close();

        return;
    }
}
