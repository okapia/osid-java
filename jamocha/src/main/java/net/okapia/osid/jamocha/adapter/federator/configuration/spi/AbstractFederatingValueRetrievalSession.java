//
// AbstractFederatingValueRetrievalSession.java
//
//     An abstract federating adapter for a ValueRetrievalSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ValueRetrievalSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingValueRetrievalSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.configuration.ValueRetrievalSession>
    implements org.osid.configuration.ValueRetrievalSession {

    private boolean parallel = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();


    /**
     *  Constructs a new <code>AbstractFederatingValueRetrievalSession</code>.
     */

    protected AbstractFederatingValueRetrievalSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.configuration.ValueRetrievalSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>Value</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupValues() {
        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            if (session.canLookupValues()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Value</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeValueView() {
        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            session.useComparativeValueView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Value</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryValueView() {
        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            session.usePlenaryValueView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include values in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            session.useFederatedConfigurationView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            session.useIsolatedConfigurationView();
        }

        return;
    }


    /**
     *  Returns only values that pass the defined parameter
     *  condition. Some parameter conditions do not require explicit
     *  conditional data to be passed and the <code> Values </code>
     *  returned from any method in this session are filtered on an
     *  implicit condition.
     */

    @OSID @Override
    public void useConditionalView() {
        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            session.useConditionalView();
        }

        return;
    }


    /**
     *  Values that are filtered based on an implicit condition are
     *  not filtered out from methods in this session. Methods that
     *  take an explicit condition as a parameter are filtered on only
     *  those conditions that are specified.
     */

    @OSID @Override
    public void useUnconditionalView() {
        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            session.useUnconditionalView();
        }

        return;
    }

     
    /**
     *  Gets a <code> Value </code> for the given parameter <code>
     *  Id. </code> If more than one value exists for the given
     *  parameter, the most preferred value is returned. This method
     *  can be used as a convenience when only one value is
     *  expected. <code> getValuesByParameters() </code> should be
     *  used for getting all the active values.
     *
     *  @param parameterId the <code> Id </code> of the <code>
     *         Parameter </code> to retrieve
     *  @return the value
     *  @throws org.osid.NotFoundException the <code> parameterId </code> not
     *          found or no value available
     *  @throws org.osid.NullArgumentException the <code> parameterId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.configuration.Value getValueByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            try {
                return (session.getValueByParameter(parameterId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException("no value for " + parameterId);
    }


    /**
     *  Gets all the <code> Values </code> for the given parameter
     *  <code>Id</code>.
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter
     *          </code> to retrieve
     *  @return the value list
     *  @throws org.osid.NotFoundException the <code> parameterId </code> not
     *          found
     *  @throws org.osid.NullArgumentException the <code> parameterId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.value.FederatingValueList ret = getValueList();

        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            try {
                ret.addValueList(session.getValuesByParameter(parameterId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        ret.noMore();

        if (!ret.hasNext()) {
            throw new org.osid.NotFoundException("no value for " + parameterId);
        }

        return (ret);
    }


    /**
     *  Gets the <code> Values </code> for the given parameter <code>
     *  Ids.  </code> In plenary mode, the values for all parameters
     *  are returned in the order requested or an error results. In
     *  comparative mode, inaccessible values may be omitted or the
     *  values reordered.
     *
     *  @param  parameterIds the <code> Id </code> of the <code> Parameter
     *          </code> to retrieve
     *  @return the value list
     *  @throws org.osid.NotFoundException a parameter <code> Id </code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code> parameterIds </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameters(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.value.FederatingValueList ret = getValueList();

        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            ret.addValueList(session.getValuesByParameters(parameterIds));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a value condition for the given parameter.
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter
     *          </code>
     *  @return a value condition
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public org.osid.configuration.ValueCondition getValueCondition(org.osid.id.Id parameterId) {
        return (null);
    }


    /**
     *  Gets a value in this configuration based on a condition. If multiple
     *  values are available the most preferred one is returned. The condition
     *  specified is applied to any or all parameters in this configuration as
     *  applicable.
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter
     *          </code>
     *  @param  valueCondition the condition
     *  @return the value
     *  @throws org.osid.NotFoundException parameter <code> Id </code> is not
     *          found
     *  @throws org.osid.NullArgumentException <code> parameterId </code> or
     *          <code> valueCondition </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.UnsupportedException <code> valueCondition </code>
     *          not of this service
     */

    @OSID @Override
    public org.osid.configuration.Value getValueByParameterOnCondition(org.osid.id.Id parameterId,
                                                                       org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            try {
                return (session.getValueByParameterOnCondition(parameterId, valueCondition));
            } catch (org.osid.NotFoundException | org.osid.UnsupportedException nue) {
                continue;
            }
        }

        throw new org.osid.NotFoundException("no value for " + parameterId);
    }


    /**
     *  Gets all the values for a parameter based on a condition. In plenary
     *  mode, all values are returned or an error results. In comparative
     *  mode, inaccessible values may be omitted.
     *
     *  @param  parameterId the <code> Id </code> of a <code> Parameter
     *          </code>
     *  @param  valueCondition the condition
     *  @return the value list
     *  @throws org.osid.NotFoundException parameter <code> Id </code> is not
     *          found
     *  @throws org.osid.NullArgumentException <code> parameterId </code> or
     *          <code> valueCondition </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.UnsupportedException <code> valueCondition </code> is
     *          not of this service
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameterOnCondition(org.osid.id.Id parameterId,
                                                                            org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.value.FederatingValueList ret = getValueList();

        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            try {
                ret.addValueList(session.getValuesByParameterOnCondition(parameterId, valueCondition));
                if (!useAllResults() && ret.hasNext()) {  
                    break;
                }
            } catch (org.osid.NotFoundException | org.osid.UnsupportedException nue) {}
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the values for parameters based on a condition. The specified
     *  condition is applied to any or all of the parameters as applicable. In
     *  plenary mode, all values are returned or an error results. In
     *  comparative mode, inaccessible values may be omitted.
     *
     *  @param  parameterIds the <code> Id </code> of a <code> Parameter
     *          </code>
     *  @param  valueCondition the condition
     *  @return the value list
     *  @throws org.osid.NotFoundException a parameter <code> Id </code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code> parameterIds </code> or
     *          <code> valueCondition </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *  @throws org.osid.UnsupportedException <code> valueCondition </code>
     *          not of this service
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParametersOnCondition(org.osid.id.IdList parameterIds,
                                                                             org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.configuration.value.FederatingValueList ret = getValueList();

        for (org.osid.configuration.ValueRetrievalSession session : getSessions()) {
            ret.addValueList(session.getValuesByParameters(parameterIds));
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.configuration.value.FederatingValueList getValueList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.value.ParallelValueList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.configuration.value.CompositeValueList());
        }
    }
}
