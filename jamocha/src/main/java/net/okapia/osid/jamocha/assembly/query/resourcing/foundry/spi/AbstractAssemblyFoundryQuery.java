//
// AbstractAssemblyFoundryQuery.java
//
//     A FoundryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.foundry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FoundryQuery that stores terms.
 */

public abstract class AbstractAssemblyFoundryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.resourcing.FoundryQuery,
               org.osid.resourcing.FoundryQueryInspector,
               org.osid.resourcing.FoundrySearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.FoundryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.FoundryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.FoundrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyFoundryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyFoundryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the effort <code> Id </code> for this query to match foundries 
     *  containing jobs. 
     *
     *  @param  jobId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchJobId(org.osid.id.Id jobId, boolean match) {
        getAssembler().addIdTerm(getJobIdColumn(), jobId, match);
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobIdTerms() {
        getAssembler().clearTerms(getJobIdColumn());
        return;
    }


    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJobIdTerms() {
        return (getAssembler().getIdTerms(getJobIdColumn()));
    }


    /**
     *  Gets the JobId column name.
     *
     * @return the column name
     */

    protected String getJobIdColumn() {
        return ("job_id");
    }


    /**
     *  Tests if a <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getJobQuery() {
        throw new org.osid.UnimplementedException("supportsJobQuery() is false");
    }


    /**
     *  Matches foundries that have any job. 
     *
     *  @param  match <code> true </code> to match foundries with any job, 
     *          <code> false </code> to match foundries with no job 
     */

    @OSID @Override
    public void matchAnyJob(boolean match) {
        getAssembler().addIdWildcardTerm(getJobColumn(), match);
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobTerms() {
        getAssembler().clearTerms(getJobColumn());
        return;
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Gets the Job column name.
     *
     * @return the column name
     */

    protected String getJobColumn() {
        return ("job");
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        getAssembler().addIdTerm(getWorkIdColumn(), workId, match);
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        getAssembler().clearTerms(getWorkIdColumn());
        return;
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (getAssembler().getIdTerms(getWorkIdColumn()));
    }


    /**
     *  Gets the WorkId column name.
     *
     * @return the column name
     */

    protected String getWorkIdColumn() {
        return ("work_id");
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches foundries that have any work. 
     *
     *  @param  match <code> true </code> to match foundries with any work, 
     *          <code> false </code> to match foundries with no job 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        getAssembler().addIdWildcardTerm(getWorkColumn(), match);
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        getAssembler().clearTerms(getWorkColumn());
        return;
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.resourcing.WorkQueryInspector[0]);
    }


    /**
     *  Gets the Work column name.
     *
     * @return the column name
     */

    protected String getWorkColumn() {
        return ("work");
    }


    /**
     *  Sets the availability <code> Id </code> for this query to match 
     *  foundries that have a related availability. 
     *
     *  @param  availabilityId a availability <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAvailabilityId(org.osid.id.Id availabilityId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAvailabilityIdColumn(), availabilityId, match);
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAvailabilityIdTerms() {
        getAssembler().clearTerms(getAvailabilityIdColumn());
        return;
    }


    /**
     *  Gets the availability <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAvailabilityIdTerms() {
        return (getAssembler().getIdTerms(getAvailabilityIdColumn()));
    }


    /**
     *  Gets the AvailabilityId column name.
     *
     * @return the column name
     */

    protected String getAvailabilityIdColumn() {
        return ("availability_id");
    }


    /**
     *  Tests if a <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsAvailabilityQuery() is false");
    }


    /**
     *  Matches foundries that have any availability. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          availability, <code> false </code> to match foundries with no 
     *          availability 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        getAssembler().addIdWildcardTerm(getAvailabilityColumn(), match);
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        getAssembler().clearTerms(getAvailabilityColumn());
        return;
    }


    /**
     *  Gets the availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQueryInspector[] getAvailabilityTerms() {
        return (new org.osid.resourcing.AvailabilityQueryInspector[0]);
    }


    /**
     *  Gets the Availability column name.
     *
     * @return the column name
     */

    protected String getAvailabilityColumn() {
        return ("availability");
    }


    /**
     *  Sets the commission <code> Id </code> for this query. 
     *
     *  @param  commissionId the commission <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commissionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommissionId(org.osid.id.Id commissionId, boolean match) {
        getAssembler().addIdTerm(getCommissionIdColumn(), commissionId, match);
        return;
    }


    /**
     *  Clears the commission <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCommissionIdTerms() {
        getAssembler().clearTerms(getCommissionIdColumn());
        return;
    }


    /**
     *  Gets the commission <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommissionIdTerms() {
        return (getAssembler().getIdTerms(getCommissionIdColumn()));
    }


    /**
     *  Gets the CommissionId column name.
     *
     * @return the column name
     */

    protected String getCommissionIdColumn() {
        return ("commission_id");
    }


    /**
     *  Tests if a <code> CommissionQuery </code> is available. 
     *
     *  @return <code> true </code> if a commission query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commission. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commission query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuery getCommissionQuery() {
        throw new org.osid.UnimplementedException("supportsCommissionQuery() is false");
    }


    /**
     *  Matches foundries that have any commission. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          commission, <code> false </code> to match foundries with no 
     *          commission 
     */

    @OSID @Override
    public void matchAnyCommission(boolean match) {
        getAssembler().addIdWildcardTerm(getCommissionColumn(), match);
        return;
    }


    /**
     *  Clears the commission query terms. 
     */

    @OSID @Override
    public void clearCommissionTerms() {
        getAssembler().clearTerms(getCommissionColumn());
        return;
    }


    /**
     *  Gets the commission query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQueryInspector[] getCommissionTerms() {
        return (new org.osid.resourcing.CommissionQueryInspector[0]);
    }


    /**
     *  Gets the Commission column name.
     *
     * @return the column name
     */

    protected String getCommissionColumn() {
        return ("commission");
    }


    /**
     *  Sets the effort <code> Id </code> for this query. 
     *
     *  @param  effortId the effort <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> effortId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEffortId(org.osid.id.Id effortId, boolean match) {
        getAssembler().addIdTerm(getEffortIdColumn(), effortId, match);
        return;
    }


    /**
     *  Clears the effort <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEffortIdTerms() {
        getAssembler().clearTerms(getEffortIdColumn());
        return;
    }


    /**
     *  Gets the effort <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEffortIdTerms() {
        return (getAssembler().getIdTerms(getEffortIdColumn()));
    }


    /**
     *  Gets the EffortId column name.
     *
     * @return the column name
     */

    protected String getEffortIdColumn() {
        return ("effort_id");
    }


    /**
     *  Tests if an <code> EffortQuery </code> is available. 
     *
     *  @return <code> true </code> if an effort query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortQuery() {
        return (false);
    }


    /**
     *  Gets the query for an effort. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the effort query 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuery getEffortQuery() {
        throw new org.osid.UnimplementedException("supportsEffortQuery() is false");
    }


    /**
     *  Matches foundries with any effort. 
     *
     *  @param  match <code> true </code> to match foundries with any effort, 
     *          <code> false </code> to match foundries with no effort 
     */

    @OSID @Override
    public void matchAnyEffort(boolean match) {
        getAssembler().addIdWildcardTerm(getEffortColumn(), match);
        return;
    }


    /**
     *  Clears the effort query terms. 
     */

    @OSID @Override
    public void clearEffortTerms() {
        getAssembler().clearTerms(getEffortColumn());
        return;
    }


    /**
     *  Gets the effort query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQueryInspector[] getEffortTerms() {
        return (new org.osid.resourcing.EffortQueryInspector[0]);
    }


    /**
     *  Gets the Effort column name.
     *
     * @return the column name
     */

    protected String getEffortColumn() {
        return ("effort");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match foundries 
     *  that have the specified foundry as an ancestor. 
     *
     *  @param  foundryId a foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getAncestorFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the ancestor foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorFoundryIdTerms() {
        getAssembler().clearTerms(getAncestorFoundryIdColumn());
        return;
    }


    /**
     *  Gets the ancestor foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorFoundryIdTerms() {
        return (getAssembler().getIdTerms(getAncestorFoundryIdColumn()));
    }


    /**
     *  Gets the AncestorFoundryId column name.
     *
     * @return the column name
     */

    protected String getAncestorFoundryIdColumn() {
        return ("ancestor_foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorFoundryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getAncestorFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorFoundryQuery() is false");
    }


    /**
     *  Matches foundries with any ancestor. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          ancestor, <code> false </code> to match root foundries 
     */

    @OSID @Override
    public void matchAnyAncestorFoundry(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorFoundryColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor foundry query terms. 
     */

    @OSID @Override
    public void clearAncestorFoundryTerms() {
        getAssembler().clearTerms(getAncestorFoundryColumn());
        return;
    }


    /**
     *  Gets the ancestor foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getAncestorFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the AncestorFoundry column name.
     *
     * @return the column name
     */

    protected String getAncestorFoundryColumn() {
        return ("ancestor_foundry");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match foundries 
     *  that have the specified foundry as a descendant. 
     *
     *  @param  foundryId a foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantFoundryId(org.osid.id.Id foundryId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the descendant foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantFoundryIdTerms() {
        getAssembler().clearTerms(getDescendantFoundryIdColumn());
        return;
    }


    /**
     *  Gets the descendant foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantFoundryIdTerms() {
        return (getAssembler().getIdTerms(getDescendantFoundryIdColumn()));
    }


    /**
     *  Gets the DescendantFoundryId column name.
     *
     * @return the column name
     */

    protected String getDescendantFoundryIdColumn() {
        return ("descendant_foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantFoundryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getDescendantFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantFoundryQuery() is false");
    }


    /**
     *  Matches foundries with any descendant. 
     *
     *  @param  match <code> true </code> to match foundries with any 
     *          descendant, <code> false </code> to match leaf foundries 
     */

    @OSID @Override
    public void matchAnyDescendantFoundry(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantFoundryColumn(), match);
        return;
    }


    /**
     *  Clears the descendant foundry query terms. 
     */

    @OSID @Override
    public void clearDescendantFoundryTerms() {
        getAssembler().clearTerms(getDescendantFoundryColumn());
        return;
    }


    /**
     *  Gets the descendant foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getDescendantFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the DescendantFoundry column name.
     *
     * @return the column name
     */

    protected String getDescendantFoundryColumn() {
        return ("descendant_foundry");
    }


    /**
     *  Tests if this foundry supports the given record
     *  <code>Type</code>.
     *
     *  @param  foundryRecordType a foundry record type 
     *  @return <code>true</code> if the foundryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type foundryRecordType) {
        for (org.osid.resourcing.records.FoundryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(foundryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  foundryRecordType the foundry record type 
     *  @return the foundry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(foundryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.FoundryQueryRecord getFoundryQueryRecord(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.FoundryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(foundryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(foundryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  foundryRecordType the foundry record type 
     *  @return the foundry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(foundryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.FoundryQueryInspectorRecord getFoundryQueryInspectorRecord(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.FoundryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(foundryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(foundryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param foundryRecordType the foundry record type
     *  @return the foundry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(foundryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.FoundrySearchOrderRecord getFoundrySearchOrderRecord(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.FoundrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(foundryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(foundryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this foundry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param foundryQueryRecord the foundry query record
     *  @param foundryQueryInspectorRecord the foundry query inspector
     *         record
     *  @param foundrySearchOrderRecord the foundry search order record
     *  @param foundryRecordType foundry record type
     *  @throws org.osid.NullArgumentException
     *          <code>foundryQueryRecord</code>,
     *          <code>foundryQueryInspectorRecord</code>,
     *          <code>foundrySearchOrderRecord</code> or
     *          <code>foundryRecordTypefoundry</code> is
     *          <code>null</code>
     */
            
    protected void addFoundryRecords(org.osid.resourcing.records.FoundryQueryRecord foundryQueryRecord, 
                                      org.osid.resourcing.records.FoundryQueryInspectorRecord foundryQueryInspectorRecord, 
                                      org.osid.resourcing.records.FoundrySearchOrderRecord foundrySearchOrderRecord, 
                                      org.osid.type.Type foundryRecordType) {

        addRecordType(foundryRecordType);

        nullarg(foundryQueryRecord, "foundry query record");
        nullarg(foundryQueryInspectorRecord, "foundry query inspector record");
        nullarg(foundrySearchOrderRecord, "foundry search odrer record");

        this.queryRecords.add(foundryQueryRecord);
        this.queryInspectorRecords.add(foundryQueryInspectorRecord);
        this.searchOrderRecords.add(foundrySearchOrderRecord);
        
        return;
    }
}
