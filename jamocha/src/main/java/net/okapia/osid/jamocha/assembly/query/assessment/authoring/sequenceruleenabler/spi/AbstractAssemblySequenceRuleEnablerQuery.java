//
// AbstractAssemblySequenceRuleEnablerQuery.java
//
//     A SequenceRuleEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.authoring.sequenceruleenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SequenceRuleEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblySequenceRuleEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.assessment.authoring.SequenceRuleEnablerQuery,
               org.osid.assessment.authoring.SequenceRuleEnablerQueryInspector,
               org.osid.assessment.authoring.SequenceRuleEnablerSearchOrder {

    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySequenceRuleEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySequenceRuleEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the sequence rule. 
     *
     *  @param  sequenceRuleId the sequence rule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSequenceRuleId(org.osid.id.Id sequenceRuleId, 
                                         boolean match) {
        getAssembler().addIdTerm(getRuledSequenceRuleIdColumn(), sequenceRuleId, match);
        return;
    }


    /**
     *  Clears the sequence rule <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSequenceRuleIdTerms() {
        getAssembler().clearTerms(getRuledSequenceRuleIdColumn());
        return;
    }


    /**
     *  Gets the sequence rule <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSequenceRuleIdTerms() {
        return (getAssembler().getIdTerms(getRuledSequenceRuleIdColumn()));
    }


    /**
     *  Gets the RuledSequenceRuleId column name.
     *
     * @return the column name
     */

    protected String getRuledSequenceRuleIdColumn() {
        return ("ruled_sequence_rule_id");
    }


    /**
     *  Tests if a <code> SequenceRuleQuery </code> is available. 
     *
     *  @return <code> true </code> if a sequence rule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSequenceRuleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sequence rule. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the sequence rule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSequenceRuleQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuery getRuledSequenceRuleQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSequenceRuleQuery() is false");
    }


    /**
     *  Matches enablers mapped to any sequence rule. 
     *
     *  @param  match <code> true </code> for enablers mapped to any sequence 
     *          rule, <code> false </code> to match enablers mapped to no 
     *          sequence rules 
     */

    @OSID @Override
    public void matchAnyRuledSequenceRule(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledSequenceRuleColumn(), match);
        return;
    }


    /**
     *  Clears the sequence rule query terms. 
     */

    @OSID @Override
    public void clearRuledSequenceRuleTerms() {
        getAssembler().clearTerms(getRuledSequenceRuleColumn());
        return;
    }


    /**
     *  Gets the sequence rule query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQueryInspector[] getRuledSequenceRuleTerms() {
        return (new org.osid.assessment.authoring.SequenceRuleQueryInspector[0]);
    }


    /**
     *  Gets the RuledSequenceRule column name.
     *
     * @return the column name
     */

    protected String getRuledSequenceRuleColumn() {
        return ("ruled_sequence_rule");
    }


    /**
     *  Matches enablers mapped to the bank. 
     *
     *  @param  bankId the bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears the bank <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        getAssembler().clearTerms(getBankIdColumn());
        return;
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (getAssembler().getIdTerms(getBankIdColumn()));
    }


    /**
     *  Gets the BankId column name.
     *
     * @return the column name
     */

    protected String getBankIdColumn() {
        return ("bank_id");
    }


    /**
     *  Tests if an <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears the bank query terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        getAssembler().clearTerms(getBankColumn());
        return;
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the Bank column name.
     *
     * @return the column name
     */

    protected String getBankColumn() {
        return ("bank");
    }


    /**
     *  Tests if this sequenceRuleEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  sequenceRuleEnablerRecordType a sequence rule enabler record type 
     *  @return <code>true</code> if the sequenceRuleEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type sequenceRuleEnablerRecordType) {
        for (org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(sequenceRuleEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  sequenceRuleEnablerRecordType the sequence rule enabler record type 
     *  @return the sequence rule enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord getSequenceRuleEnablerQueryRecord(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(sequenceRuleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  sequenceRuleEnablerRecordType the sequence rule enabler record type 
     *  @return the sequence rule enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleEnablerQueryInspectorRecord getSequenceRuleEnablerQueryInspectorRecord(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(sequenceRuleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param sequenceRuleEnablerRecordType the sequence rule enabler record type
     *  @return the sequence rule enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleEnablerSearchOrderRecord getSequenceRuleEnablerSearchOrderRecord(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(sequenceRuleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this sequence rule enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param sequenceRuleEnablerQueryRecord the sequence rule enabler query record
     *  @param sequenceRuleEnablerQueryInspectorRecord the sequence rule enabler query inspector
     *         record
     *  @param sequenceRuleEnablerSearchOrderRecord the sequence rule enabler search order record
     *  @param sequenceRuleEnablerRecordType sequence rule enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerQueryRecord</code>,
     *          <code>sequenceRuleEnablerQueryInspectorRecord</code>,
     *          <code>sequenceRuleEnablerSearchOrderRecord</code> or
     *          <code>sequenceRuleEnablerRecordTypesequenceRuleEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addSequenceRuleEnablerRecords(org.osid.assessment.authoring.records.SequenceRuleEnablerQueryRecord sequenceRuleEnablerQueryRecord, 
                                      org.osid.assessment.authoring.records.SequenceRuleEnablerQueryInspectorRecord sequenceRuleEnablerQueryInspectorRecord, 
                                      org.osid.assessment.authoring.records.SequenceRuleEnablerSearchOrderRecord sequenceRuleEnablerSearchOrderRecord, 
                                      org.osid.type.Type sequenceRuleEnablerRecordType) {

        addRecordType(sequenceRuleEnablerRecordType);

        nullarg(sequenceRuleEnablerQueryRecord, "sequence rule enabler query record");
        nullarg(sequenceRuleEnablerQueryInspectorRecord, "sequence rule enabler query inspector record");
        nullarg(sequenceRuleEnablerSearchOrderRecord, "sequence rule enabler search odrer record");

        this.queryRecords.add(sequenceRuleEnablerQueryRecord);
        this.queryInspectorRecords.add(sequenceRuleEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(sequenceRuleEnablerSearchOrderRecord);
        
        return;
    }
}
