//
// MutableMapProxyBillingLookupSession
//
//    Implements a Billing lookup service backed by a collection of
//    billings that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement;


/**
 *  Implements a Billing lookup service backed by a collection of
 *  billings. The billings are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of billings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBillingLookupSession
    extends net.okapia.osid.jamocha.core.acknowledgement.spi.AbstractMapBillingLookupSession
    implements org.osid.acknowledgement.BillingLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBillingLookupSession} with no
     *  billings.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyBillingLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBillingLookupSession} with a
     *  single billing.
     *
     *  @param billing a billing
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBillingLookupSession(org.osid.acknowledgement.Billing billing, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBilling(billing);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBillingLookupSession} using an
     *  array of billings.
     *
     *  @param billings an array of billings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billings} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBillingLookupSession(org.osid.acknowledgement.Billing[] billings, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBillings(billings);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBillingLookupSession} using
     *  a collection of billings.
     *
     *  @param billings a collection of billings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billings} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBillingLookupSession(java.util.Collection<? extends org.osid.acknowledgement.Billing> billings,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBillings(billings);
        return;
    }

    
    /**
     *  Makes a {@code Billing} available in this session.
     *
     *  @param billing an billing
     *  @throws org.osid.NullArgumentException {@code billing{@code 
     *          is {@code null}
     */

    @Override
    public void putBilling(org.osid.acknowledgement.Billing billing) {
        super.putBilling(billing);
        return;
    }


    /**
     *  Makes an array of billings available in this session.
     *
     *  @param billings an array of billings
     *  @throws org.osid.NullArgumentException {@code billings{@code 
     *          is {@code null}
     */

    @Override
    public void putBillings(org.osid.acknowledgement.Billing[] billings) {
        super.putBillings(billings);
        return;
    }


    /**
     *  Makes collection of billings available in this session.
     *
     *  @param billings
     *  @throws org.osid.NullArgumentException {@code billing{@code 
     *          is {@code null}
     */

    @Override
    public void putBillings(java.util.Collection<? extends org.osid.acknowledgement.Billing> billings) {
        super.putBillings(billings);
        return;
    }


    /**
     *  Removes a Billing from this session.
     *
     *  @param billingId the {@code Id} of the billing
     *  @throws org.osid.NullArgumentException {@code billingId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBilling(org.osid.id.Id billingId) {
        super.removeBilling(billingId);
        return;
    }    
}
