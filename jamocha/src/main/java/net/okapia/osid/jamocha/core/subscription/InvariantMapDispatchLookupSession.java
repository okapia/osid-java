//
// InvariantMapDispatchLookupSession
//
//    Implements a Dispatch lookup service backed by a fixed collection of
//    dispatches.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription;


/**
 *  Implements a Dispatch lookup service backed by a fixed
 *  collection of dispatches. The dispatches are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapDispatchLookupSession
    extends net.okapia.osid.jamocha.core.subscription.spi.AbstractMapDispatchLookupSession
    implements org.osid.subscription.DispatchLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapDispatchLookupSession</code> with no
     *  dispatches.
     *  
     *  @param publisher the publisher
     *  @throws org.osid.NullArgumnetException {@code publisher} is
     *          {@code null}
     */

    public InvariantMapDispatchLookupSession(org.osid.subscription.Publisher publisher) {
        setPublisher(publisher);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDispatchLookupSession</code> with a single
     *  dispatch.
     *  
     *  @param publisher the publisher
     *  @param dispatch a single dispatch
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code dispatch} is <code>null</code>
     */

      public InvariantMapDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                               org.osid.subscription.Dispatch dispatch) {
        this(publisher);
        putDispatch(dispatch);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDispatchLookupSession</code> using an array
     *  of dispatches.
     *  
     *  @param publisher the publisher
     *  @param dispatches an array of dispatches
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code dispatches} is <code>null</code>
     */

      public InvariantMapDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                               org.osid.subscription.Dispatch[] dispatches) {
        this(publisher);
        putDispatches(dispatches);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDispatchLookupSession</code> using a
     *  collection of dispatches.
     *
     *  @param publisher the publisher
     *  @param dispatches a collection of dispatches
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code dispatches} is <code>null</code>
     */

      public InvariantMapDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                               java.util.Collection<? extends org.osid.subscription.Dispatch> dispatches) {
        this(publisher);
        putDispatches(dispatches);
        return;
    }
}
