//
// AbstractSubjectQuery.java
//
//     A template for making a Subject Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.subject.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for subjects.
 */

public abstract class AbstractSubjectQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.ontology.SubjectQuery {

    private final java.util.Collection<org.osid.ontology.records.SubjectQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the subject <code> Id </code> for this query to match subjects 
     *  that have the specified subject as an ancestor. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorSubjectId(org.osid.id.Id subjectId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor subject <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearAncestorSubjectIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorSubjectQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getAncestorSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorSubjectQuery() is false");
    }


    /**
     *  Matches subjects with any ancestor. 
     *
     *  @param  match <code> true </code> to match subjects with any ancestor, 
     *          <code> false </code> to match root subjects 
     */

    @OSID @Override
    public void matchAnyAncestorSubject(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor subject terms for this query. 
     */

    @OSID @Override
    public void clearAncestorSubjectTerms() {
        return;
    }


    /**
     *  Sets the subject <code> Id </code> for this query to match subjects 
     *  that have the specified subject as a descendant. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantSubjectId(org.osid.id.Id subjectId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant subject <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearDescendantSubjectIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantSubjectQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getDescendantSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantSubjectQuery() is false");
    }


    /**
     *  Matches subjects with any descendant. 
     *
     *  @param  match <code> true </code> to match subjects with any 
     *          descendant, <code> false </code> to match leaf subjects 
     */

    @OSID @Override
    public void matchAnyDescendantSubject(boolean match) {
        return;
    }


    /**
     *  Clears the descendant subject terms for this query. 
     */

    @OSID @Override
    public void clearDescendantSubjectTerms() {
        return;
    }


    /**
     *  Sets the relevancy <code> Id </code> for this query. 
     *
     *  @param  relevancyId a relevancy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relevancyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRelevancyId(org.osid.id.Id relevancyId, boolean match) {
        return;
    }


    /**
     *  Clears the relevancy <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available. 
     *
     *  @return <code> true </code> if a relevancy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relevancy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the relevancy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRelevancyQuery() is false");
    }


    /**
     *  Matches ontologies that have any relevancy. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          relevancy, <code> false </code> to match ontologies with no 
     *          relevancy 
     */

    @OSID @Override
    public void matchAnyRelevancy(boolean match) {
        return;
    }


    /**
     *  Clears the relevancy terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyTerms() {
        return;
    }


    /**
     *  Sets the ontology <code> Id </code> for this query. 
     *
     *  @param  ontologyId a ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOntologyId(org.osid.id.Id ontologyId, boolean match) {
        return;
    }


    /**
     *  Clears the ontology <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearOntologyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OntologyQuery </code> is available for querying 
     *  ontologies. 
     *
     *  @return <code> true </code> if a ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsOntologyQuery() is false");
    }


    /**
     *  Clears the ontology terms for this query. 
     */

    @OSID @Override
    public void clearOntologyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given subject query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a subject implementing the requested record.
     *
     *  @param subjectRecordType a subject record type
     *  @return the subject query record
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subjectRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.SubjectQueryRecord getSubjectQueryRecord(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.SubjectQueryRecord record : this.records) {
            if (record.implementsRecordType(subjectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subjectRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subject query. 
     *
     *  @param subjectQueryRecord subject query record
     *  @param subjectRecordType subject record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubjectQueryRecord(org.osid.ontology.records.SubjectQueryRecord subjectQueryRecord, 
                                          org.osid.type.Type subjectRecordType) {

        addRecordType(subjectRecordType);
        nullarg(subjectQueryRecord, "subject query record");
        this.records.add(subjectQueryRecord);        
        return;
    }
}
