//
// AbstractScheduleQueryInspector.java
//
//     A template for making a ScheduleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for schedules.
 */

public abstract class AbstractScheduleQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.calendaring.ScheduleQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.ScheduleQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the schedule slot <code> Id </code> terms. 
     *
     *  @return the schedule slot <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleSlotIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the schedule slot terms. 
     *
     *  @return the schedule slot terms 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQueryInspector[] getScheduleSlotTerms() {
        return (new org.osid.calendaring.ScheduleSlotQueryInspector[0]);
    }


    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the schedule start terms. 
     *
     *  @return the schedule start terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getScheduleStartTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the schedule end terms. 
     *
     *  @return the schedule end terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getScheduleEndTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the schedule time terms. 
     *
     *  @return the schedule time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getScheduleTimeTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the schedule inclusive time terms. 
     *
     *  @return the schedule time range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getScheduleTimeInclusiveTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the limit terms. 
     *
     *  @return the limit terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getLimitTerms() {
        return (new org.osid.search.terms.CardinalTerm[0]);
    }


    /**
     *  Gets the location description terms. 
     *
     *  @return the location description terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the location <code> Id </code> terms. 
     *
     *  @return the location <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the location terms. 
     *
     *  @return the location terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the total duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given schedule query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a schedule implementing the requested record.
     *
     *  @param scheduleRecordType a schedule record type
     *  @return the schedule query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleQueryInspectorRecord getScheduleQueryInspectorRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule query. 
     *
     *  @param scheduleQueryInspectorRecord schedule query inspector
     *         record
     *  @param scheduleRecordType schedule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addScheduleQueryInspectorRecord(org.osid.calendaring.records.ScheduleQueryInspectorRecord scheduleQueryInspectorRecord, 
                                                   org.osid.type.Type scheduleRecordType) {

        addRecordType(scheduleRecordType);
        nullarg(scheduleRecordType, "schedule record type");
        this.records.add(scheduleQueryInspectorRecord);        
        return;
    }
}
