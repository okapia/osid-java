//
// QueryAssembler.java
//
//     An interface for assembling a search query.
//
//
// Tom Coppeto
// OnTapSolutions
// 23 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc.postgres;

import net.okapia.osid.torrefacto.collect.TypeEnumSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A utility interface for assembling jdbc search queries.
 */

public class QueryAssembler 
    extends net.okapia.osid.jamocha.assembly.query.jdbc.QueryAssembler
    implements net.okapia.osid.jamocha.assembly.query.QueryAssembler {

    private final static TypeEnumSet<PostgresStringMatchTypes> stringMatchTypes = new TypeEnumSet<>(PostgresStringMatchTypes.class);


    /**
     *  Constructs a new <code>QueryAssembler</code>.
     */

    public QueryAssembler() {
        super.addStringMatchTypes(stringMatchTypes.toCollection());
        return;
    }


    /**
     *  Adds a SQL string search query term.
     *
     *  @param column query column
     *  @param value value to match
     *  @param stringMatchType type of string matching
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */
    
    public void addStringTerm(String column, String value, 
                              org.osid.type.Type stringMatchType, boolean match) {
        addTerm(new StringQueryTerm(column, value, stringMatchType, match));
        return;
    }


    /**
     *  Gets the string matching types supported. A string match type
     *  specifies the syntax of the string query, such as matching a
     *  word or including a wildcard or regular expression.
     *
     *  @return a list containing the supported string match types
     *  @throws org.osid.NullArgumentException <code>type</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException <code>type</code> not
     *          supported
     */

    static PostgresStringMatchTypes getStringMatchType(org.osid.type.Type type) {
        return (stringMatchTypes.getEnum(type));
    }
}
