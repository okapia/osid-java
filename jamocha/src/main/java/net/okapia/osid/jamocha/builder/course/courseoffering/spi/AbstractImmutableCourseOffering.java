//
// AbstractImmutableCourseOffering.java
//
//     Wraps a mutable CourseOffering to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>CourseOffering</code> to hide modifiers. This
 *  wrapper provides an immutized CourseOffering from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying courseOffering whose state changes are visible.
 */

public abstract class AbstractImmutableCourseOffering
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.CourseOffering {

    private final org.osid.course.CourseOffering courseOffering;


    /**
     *  Constructs a new <code>AbstractImmutableCourseOffering</code>.
     *
     *  @param courseOffering the course offering to immutablize
     *  @throws org.osid.NullArgumentException <code>courseOffering</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCourseOffering(org.osid.course.CourseOffering courseOffering) {
        super(courseOffering);
        this.courseOffering = courseOffering;
        return;
    }


    /**
     *  Gets the canonical course <code> Id </code> associated with this 
     *  course offering. 
     *
     *  @return the course <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.courseOffering.getCourseId());
    }


    /**
     *  Gets the canonical course associated with this course offering. 
     *
     *  @return the course 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getCourse());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term </code> of this 
     *  offering. 
     *
     *  @return the <code> Term </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.courseOffering.getTermId());
    }


    /**
     *  Gets the <code> Term </code> of this offering. 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getTerm());
    }


    /**
     *  Gets the formal title of this course. It may be the same as the 
     *  display name or it may be used to more formally label the course. A 
     *  display name might be Physics 102 where the title is Introduction to 
     *  Electromagentism. 
     *
     *  @return the course title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.courseOffering.getTitle());
    }


    /**
     *  Gets the course number which is a label generally used to index the 
     *  course offering in a catalog, such as T101 or 16.004. 
     *
     *  @return the course number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.courseOffering.getNumber());
    }


    /**
     *  Gets the instructor <code> Ids. </code> If each activity has its own 
     *  instructor, the headlining instructors may be returned. 
     *
     *  @return the instructor <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getInstructorIds() {
        return (this.courseOffering.getInstructorIds());
    }


    /**
     *  Gets the instructors. If each activity has its own instructor, the 
     *  headlining instructors may be returned. 
     *
     *  @return the sponsors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getInstructors()
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getInstructors());
    }


    /**
     *  Tests if this course has sponsors. 
     *
     *  @return <code> true </code> if this course has sponsors, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.courseOffering.hasSponsors());
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.courseOffering.getSponsorIds());
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getSponsors());
    }


    /**
     *  Gets the credit amount <code>Ids</code>.
     *
     *  @return the grading system <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCreditAmountIds() {
        return (this.courseOffering.getCreditAmountIds());
    }


    /**
     *  Gets the credit scale. 
     *
     *  @return the grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getCreditAmounts()
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getCreditAmounts());
    }


    /**
     *  Tests if this course offering is graded. 
     *
     *  @return <code> true </code> if this course is graded, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.courseOffering.isGraded());
    }


    /**
     *  Gets the various grading option <code> Ids </code> available to 
     *  register in this course. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getGradingOptionIds() {
        return (this.courseOffering.getGradingOptionIds());
    }


    /**
     *  Gets the various grading options available to register in this course. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradingOptions()
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getGradingOptions());
    }


    /**
     *  Tests if this course offering requires advanced registration. 
     *
     *  @return <code> true </code> if this course requires advance 
     *          registration, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean requiresRegistration() {
        return (this.courseOffering.requiresRegistration());
    }


    /**
     *  Gets the minimum number of students this offering can have. 
     *
     *  @return the minimum seats 
     */

    @OSID @Override
    public long getMinimumSeats() {
        return (this.courseOffering.getMinimumSeats());
    }


    /**
     *  Tests if this course offering has limited seating. 
     *
     *  @return <code> true </code> if this has limietd seating
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isSeatingLimited() {
        return (this.courseOffering.isSeatingLimited());
    }


    /**
     *  Gets the maximum number of students this offering can have. 
     *
     *  @return the maximum seats 
     *  @throws org.osid.IllegalStateException
     *          <code>isSeatingLimited()</code> is <code> false
     *          </code>
     */

    @OSID @Override
    public long getMaximumSeats() {
        return (this.courseOffering.getMaximumSeats());
    }


    /**
     *  Gets an external resource, such as a class web site, associated with 
     *  this offering. 
     *
     *  @return a URL string 
     */

    @OSID @Override
    public String getURL() {
        return (this.courseOffering.getURL());
    }


    /**
     *  Gets the an informational string for the course offering schedule. 
     *
     *  @return the schedule info 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getScheduleInfo() {
        return (this.courseOffering.getScheduleInfo());
    }


    /**
     *  Tests if a calendaring event is available for this course offering 
     *  with the schedule details. 
     *
     *  @return <code> true </code> if this course is graded, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasEvent() {
        return (this.courseOffering.hasEvent());
    }


    /**
     *  Gets the calendaring event <code> Id. </code> 
     *
     *  @return the event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEvent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEventId() {
        return (this.courseOffering.getEventId());
    }


    /**
     *  Gets the calendaring event with the schedule details. 
     *
     *  @return an event 
     *  @throws org.osid.IllegalStateException <code> hasEvent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent()
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getEvent());
    }


    /**
     *  Gets the course offering record corresponding to the given <code> 
     *  CourseOffering </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  courseOfferingRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(courseOfferingRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  courseOfferingRecordType the type of course offering record to 
     *          retrieve 
     *  @return the course offering record 
     *  @throws org.osid.NullArgumentException <code> courseOfferingRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(courseOfferingRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingRecord getCourseOfferingRecord(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException {

        return (this.courseOffering.getCourseOfferingRecord(courseOfferingRecordType));
    }
}

