//
// AbstractPackageList
//
//     Implements a filter for a PackageList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a PackageList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedPackageList
 *  to improve performance.
 */

public abstract class AbstractPackageFilterList
    extends net.okapia.osid.jamocha.installation.pkg.spi.AbstractPackageList
    implements org.osid.installation.PackageList,
               net.okapia.osid.jamocha.inline.filter.installation.pkg.PackageFilter {

    private org.osid.installation.Package pkg;
    private final org.osid.installation.PackageList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractPackageFilterList</code>.
     *
     *  @param pkgList a <code>PackageList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>pkgList</code> is <code>null</code>
     */

    protected AbstractPackageFilterList(org.osid.installation.PackageList pkgList) {
        nullarg(pkgList, "package list");
        this.list = pkgList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.pkg == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Package </code> in this list. 
     *
     *  @return the next <code> Package </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Package </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.Package getNextPackage()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.installation.Package pkg = this.pkg;
            this.pkg = null;
            return (pkg);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in package list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.pkg = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Packages.
     *
     *  @param pkg the package to filter
     *  @return <code>true</code> if the package passes the filter,
     *          <code>false</code> if the package should be filtered
     */

    public abstract boolean pass(org.osid.installation.Package pkg);


    protected void prime() {
        if (this.pkg != null) {
            return;
        }

        org.osid.installation.Package pkg = null;

        while (this.list.hasNext()) {
            try {
                pkg = this.list.getNextPackage();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(pkg)) {
                this.pkg = pkg;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
