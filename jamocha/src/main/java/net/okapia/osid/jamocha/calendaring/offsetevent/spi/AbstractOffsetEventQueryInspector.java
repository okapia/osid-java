//
// AbstractOffsetEventQueryInspector.java
//
//     A template for making an OffsetEventQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for offset events.
 */

public abstract class AbstractOffsetEventQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.calendaring.OffsetEventQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.OffsetEventQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the fixed start time terms. 
     *
     *  @return the fixed start time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFixedStartTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStartReferenceEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getStartReferenceEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the fixed offset terms. 
     *
     *  @return the fixed offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getFixedStartOffsetTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the relative weekday offset terms. 
     *
     *  @return the relative weekday offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getRelativeWeekdayStartOffsetTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the relative weekday terms. 
     *
     *  @return the relative weekday terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getRelativeStartWeekdayTerms() {
        return (new org.osid.search.terms.CardinalTerm[0]);
    }


    /**
     *  Gets the fixed duration terms. 
     *
     *  @return the fixed duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getFixedDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndReferenceEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEndReferenceEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the fixed offset terms. 
     *
     *  @return the fixed offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getFixedEndOffsetTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the relative weekday offset terms. 
     *
     *  @return the relative weekday offset terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getRelativeWeekdayEndOffsetTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the relative weekday terms. 
     *
     *  @return the relative weekday terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getRelativeEndWeekdayTerms() {
        return (new org.osid.search.terms.CardinalTerm[0]);
    }


    /**
     *  Gets the location description terms. 
     *
     *  @return the location description terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the location <code> Id </code> terms. 
     *
     *  @return the location <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the location terms. 
     *
     *  @return the location terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the sponsor <code> Id </code> terms. 
     *
     *  @return the sponsor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the sponsor terms. 
     *
     *  @return the sponsor terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given offset event query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an offset event implementing the requested record.
     *
     *  @param offsetEventRecordType an offset event record type
     *  @return the offset event query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventQueryInspectorRecord getOffsetEventQueryInspectorRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.OffsetEventQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offset event query. 
     *
     *  @param offsetEventQueryInspectorRecord offset event query inspector
     *         record
     *  @param offsetEventRecordType offsetEvent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOffsetEventQueryInspectorRecord(org.osid.calendaring.records.OffsetEventQueryInspectorRecord offsetEventQueryInspectorRecord, 
                                                   org.osid.type.Type offsetEventRecordType) {

        addRecordType(offsetEventRecordType);
        nullarg(offsetEventRecordType, "offset event record type");
        this.records.add(offsetEventQueryInspectorRecord);        
        return;
    }
}
