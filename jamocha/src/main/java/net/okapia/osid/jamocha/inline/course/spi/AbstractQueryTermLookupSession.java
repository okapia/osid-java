//
// AbstractQueryTermLookupSession.java
//
//    An inline adapter that maps a TermLookupSession to
//    a TermQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a TermLookupSession to
 *  a TermQuerySession.
 */

public abstract class AbstractQueryTermLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractTermLookupSession
    implements org.osid.course.TermLookupSession {

    private final org.osid.course.TermQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryTermLookupSession.
     *
     *  @param querySession the underlying term query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryTermLookupSession(org.osid.course.TermQuerySession querySession) {
        nullarg(querySession, "term query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Term</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTerms() {
        return (this.session.canSearchTerms());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include terms in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the <code>Term</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Term</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Term</code> and
     *  retained for compatibility.
     *
     *  @param  termId <code>Id</code> of the
     *          <code>Term</code>
     *  @return the term
     *  @throws org.osid.NotFoundException <code>termId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>termId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Term getTerm(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.TermQuery query = getQuery();
        query.matchId(termId, true);
        org.osid.course.TermList terms = this.session.getTermsByQuery(query);
        if (terms.hasNext()) {
            return (terms.getNextTerm());
        } 
        
        throw new org.osid.NotFoundException(termId + " not found");
    }


    /**
     *  Gets a <code>TermList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  terms specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Terms</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  termIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>termIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByIds(org.osid.id.IdList termIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.TermQuery query = getQuery();

        try (org.osid.id.IdList ids = termIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getTermsByQuery(query));
    }


    /**
     *  Gets a <code>TermList</code> corresponding to the given
     *  term genus <code>Type</code> which does not include
     *  terms of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  termGenusType a term genus type 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByGenusType(org.osid.type.Type termGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.TermQuery query = getQuery();
        query.matchGenusType(termGenusType, true);
        return (this.session.getTermsByQuery(query));
    }


    /**
     *  Gets a <code>TermList</code> corresponding to the given
     *  term genus <code>Type</code> and include any additional
     *  terms with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  termGenusType a term genus type 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByParentGenusType(org.osid.type.Type termGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.TermQuery query = getQuery();
        query.matchParentGenusType(termGenusType, true);
        return (this.session.getTermsByQuery(query));
    }


    /**
     *  Gets a <code>TermList</code> containing the given
     *  term record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  termRecordType a term record type 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByRecordType(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.TermQuery query = getQuery();
        query.matchRecordType(termRecordType, true);
        return (this.session.getTermsByQuery(query));
    }


    /**
     *  Gets a <code> TermList </code> where to the given <code>
     *  DateTime </code> falls within the classes date range
     *  inclusive. Terms containing the given date are matched. In
     *  plenary mode, the returned list contains all of the terms
     *  specified in the <code> Id </code> list, in the order of the
     *  list, including duplicates, or an error results if an <code>
     *  Id </code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Terms </code> may
     *  be omitted from the list including returning a unique set.
     *
     *  @param  datetime a date 
     *  @return the returned <code> Term </code> list 
     *  @throws org.osid.NullArgumentException <code> datetime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByClassesDate(org.osid.calendaring.DateTime datetime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.TermQuery query = getQuery();
        query.matchClassesPeriod(datetime, true);
        return (this.session.getTermsByQuery(query));
    }


    /**
     *  Gets all <code>Terms</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Terms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTerms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.TermQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getTermsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.TermQuery getQuery() {
        org.osid.course.TermQuery query = this.session.getTermQuery();
        
        return (query);
    }
}
