//
// AbstractPriceEnabler.java
//
//     Defines a PriceEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.rules.priceenabler.spi;


/**
 *  Defines a <code>PriceEnabler</code> builder.
 */

public abstract class AbstractPriceEnablerBuilder<T extends AbstractPriceEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ordering.rules.priceenabler.PriceEnablerMiter priceEnabler;


    /**
     *  Constructs a new <code>AbstractPriceEnablerBuilder</code>.
     *
     *  @param priceEnabler the price enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPriceEnablerBuilder(net.okapia.osid.jamocha.builder.ordering.rules.priceenabler.PriceEnablerMiter priceEnabler) {
        super(priceEnabler);
        this.priceEnabler = priceEnabler;
        return;
    }


    /**
     *  Builds the price enabler.
     *
     *  @return the new price enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ordering.rules.PriceEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.ordering.rules.priceenabler.PriceEnablerValidator(getValidations())).validate(this.priceEnabler);
        return (new net.okapia.osid.jamocha.builder.ordering.rules.priceenabler.ImmutablePriceEnabler(this.priceEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the price enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ordering.rules.priceenabler.PriceEnablerMiter getMiter() {
        return (this.priceEnabler);
    }


    /**
     *  Adds a PriceEnabler record.
     *
     *  @param record a price enabler record
     *  @param recordType the type of price enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ordering.rules.records.PriceEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addPriceEnablerRecord(record, recordType);
        return (self());
    }
}       


