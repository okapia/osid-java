//
// AbstractAssemblyIntersectionQuery.java
//
//     An IntersectionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.path.intersection.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An IntersectionQuery that stores terms.
 */

public abstract class AbstractAssemblyIntersectionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.mapping.path.IntersectionQuery,
               org.osid.mapping.path.IntersectionQueryInspector,
               org.osid.mapping.path.IntersectionSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.records.IntersectionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.IntersectionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.IntersectionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyIntersectionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyIntersectionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Match intersections overlapping with the given coordinate.
     *
     *  @param  coordinate a coordinate
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> coordinate
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, boolean match) {
        getAssembler().addCoordinateTerm(getCoordinateColumn(), coordinate, match);
        return;
    }


    /**
     *  Clears the coordinate query terms.
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        getAssembler().clearTerms(getCoordinateColumn());
        return;
    }


    /**
     *  Gets the coordinate query terms.
     *
     *  @return the query terms
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (getAssembler().getCoordinateTerms(getCoordinateColumn()));
    }

    
    /**
     *  Gets the Coordinate column name.
     *
     *  @return the column name
     */

    protected String getCoordinateColumn() {
        return ("coordinate");
    }


    /**
     *  Matches intersections contained within the specified spatial
     *  unit.
     *
     *  @param  spatialUnit a spatial unit
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnit</code> is <code> null </code>
     */

    @OSID @Override
    public void matchContainingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit,
                                         boolean match) {
        getAssembler().addSpatialUnitTerm(getContainingSpatialUnitColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Clears the containing spatialunit query terms. 
     */

    @OSID @Override
    public void clearContainingSpatialUnitTerms() {
        getAssembler().clearTerms(getContainingSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the containing spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getContainingSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getContainingSpatialUnitColumn()));
    }


    /**
     *  Gets the ContainingSpatialUnit column name.
     *
     * @return the column name
     */

    protected String getContainingSpatialUnitColumn() {
        return ("containing_spatial_unit");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match
     *  intersections using a designated path.
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        getAssembler().clearTerms(getPathIdColumn());
        return;
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (getAssembler().getIdTerms(getPathIdColumn()));
    }


    /**
     *  Gets the Path Id column name.
     *
     *  @return the column name
     */

    protected String getPathIdColumn() {
        return ("path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches intersections using any designated path. 
     *
     *  @param match <code> true </code> to match intersections with
     *          any path, <code> false </code> to match intersections
     *          with no path
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        getAssembler().addIdWildcardTerm(getPathColumn(), match);
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Tests if a search order for the path is available. 
     *
     *  @return <code> true </code> if a path search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearchOrder() {
        return (false);
    }


    /**
     *  Gets a path search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchOrder getPathSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPathSearchOrder() is false");
    }


    /**
     *  Gets the Path column name.
     *
     *  @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Matches rotaries.
     *
     *  @param match <code> true </code> to match rotaries, <code>
     *         false </code> otherwise
     */

    @OSID @Override
    public void matchRotary(boolean match) {
        getAssembler().addBooleanTerm(getRotaryColumn(), match);
        return;
    }


    /**
     *  Clears the rotary query terms. 
     */

    @OSID @Override
    public void clearRotaryTerms() {
        getAssembler().clearTerms(getRotaryColumn());
        return;
    }


    /**
     *  Gets the rotary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRotaryTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Specifies a preference for ordering the results by rotary..
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRotary(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRotaryColumn(), style);
        return;
    }


    /**
     *  Gets the Rotary column name.
     *
     *  @return the column name
     */

    protected String getRotaryColumn() {
        return ("rotary");
    }


    /**
     *  Matches forks.
     *
     *  @param match <code> true </code> to match forks, <code>
     *         false </code> otherwise
     */

    @OSID @Override
    public void matchFork(boolean match) {
        getAssembler().addBooleanTerm(getForkColumn(), match);
        return;
    }


    /**
     *  Clears the fork query terms. 
     */

    @OSID @Override
    public void clearForkTerms() {
        getAssembler().clearTerms(getForkColumn());
        return;
    }


    /**
     *  Gets the fork query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getForkTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Specifies a preference for ordering the results by fork..
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFork(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getForkColumn(), style);
        return;
    }


    /**
     *  Gets the Fork column name.
     *
     *  @return the column name
     */

    protected String getForkColumn() {
        return ("fork");
    }


    /**
     *  Sets the map <code> Id </code> for this query to match routes assigned 
     *  to maps. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the Map Id column name.
     *
     *  @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     *  @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this intersection supports the given record
     *  <code>Type</code>.
     *
     *  @param  intersectionRecordType an intersection record type 
     *  @return <code>true</code> if the intersectionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type intersectionRecordType) {
        for (org.osid.mapping.path.records.IntersectionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(intersectionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  intersectionRecordType the intersection record type 
     *  @return the intersection query record 
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(intersectionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.IntersectionQueryRecord getIntersectionQueryRecord(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.IntersectionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(intersectionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(intersectionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  intersectionRecordType the intersection record type 
     *  @return the intersection query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(intersectionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.IntersectionQueryInspectorRecord getIntersectionQueryInspectorRecord(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.IntersectionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(intersectionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(intersectionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param intersectionRecordType the intersection record type
     *  @return the intersection search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(intersectionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.IntersectionSearchOrderRecord getIntersectionSearchOrderRecord(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.IntersectionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(intersectionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(intersectionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this intersection. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param intersectionQueryRecord the intersection query record
     *  @param intersectionQueryInspectorRecord the intersection query inspector
     *         record
     *  @param intersectionSearchOrderRecord the intersection search order record
     *  @param intersectionRecordType intersection record type
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionQueryRecord</code>,
     *          <code>intersectionQueryInspectorRecord</code>,
     *          <code>intersectionSearchOrderRecord</code> or
     *          <code>intersectionRecordTypeintersection</code> is
     *          <code>null</code>
     */
            
    protected void addIntersectionRecords(org.osid.mapping.path.records.IntersectionQueryRecord intersectionQueryRecord, 
                                      org.osid.mapping.path.records.IntersectionQueryInspectorRecord intersectionQueryInspectorRecord, 
                                      org.osid.mapping.path.records.IntersectionSearchOrderRecord intersectionSearchOrderRecord, 
                                      org.osid.type.Type intersectionRecordType) {

        addRecordType(intersectionRecordType);

        nullarg(intersectionQueryRecord, "intersection query record");
        nullarg(intersectionQueryInspectorRecord, "intersection query inspector record");
        nullarg(intersectionSearchOrderRecord, "intersection search odrer record");

        this.queryRecords.add(intersectionQueryRecord);
        this.queryInspectorRecords.add(intersectionQueryInspectorRecord);
        this.searchOrderRecords.add(intersectionSearchOrderRecord);
        
        return;
    }
}
