//
// AbstractAdapterOsidSession.java
//
//     A basic adapter for OsidSessions.
//
//
// Tom Coppeto
// Okapia
// 27 January 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This is a abstract class for implementing a basic OsidSession
 *  adapter.
 */

public abstract class AbstractAdapterOsidSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.OsidSession {

    private final org.osid.OsidSession session;
    private org.osid.proxy.Proxy proxy;


    /**
     *  Constructs an {@code AbstractAdapterOsidSession}.
     *
     *  @param session an OsidSession
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOsidSession(org.osid.OsidSession session) {
        nullarg(session, "session");
        this.session = session;
        return;
    }


    /**
     *  Gets the locale indicating the localization preferences in
     *  effect for this session. 
     *
     *  Uses a specific locale set, then falls back to the proxy.
     *
     *  @return the locale 
     */

    @OSID @Override
    public org.osid.locale.Locale getLocale() {
        if (getProxy() != null) {
            return (super.getLocale());
        } else {
            return (this.session.getLocale());
        }
    }


    /**
     *  Tests if there are valid authentication credentials used by
     *  this service.
     *
     *  Valid credentials exist of the agent is explicitly set, or an
     *  Authentication was provided where isValid() is true, or a
     *  proxy was provided that contains an Authentication where
     *  isValid() is true.
     *
     *  @return {@code true} if valid authentication
     *          credentials exist, {@code false} otherwise
     */

    @OSID @Override
    public boolean isAuthenticated() {        
        if (getProxy() != null) {
            return (super.isAuthenticated());
        } else {
            return (this.session.isAuthenticated());
        }
    }


    /**
     *  Gets the {@code Id} of the agent authenticated to this
     *  session.  
     *
     *  The agent may be explicitly set, or through Authentication
     *  provided where isValid() is true, or through proxy provided
     *  that contains an Authentication where isValid() is true.
     *
     *  @return the authenticated agent {@code Id} 
     *  @throws org.osid.IllegalStateException
     *          {@code isAuthenticated()} is {@code false}
     */

    @OSID @Override
    public org.osid.id.Id getAuthenticatedAgentId() {
        if (getProxy() != null) {
            return (super.getAuthenticatedAgentId());
        } else {
            return (this.session.getAuthenticatedAgentId());
        }
    }


    /**
     *  Gets the agent authenticated to this session. 
     *
     *  The agent may be explicitly set, or through Authentication
     *  provided where isValid() is true, or through proxy provided
     *  that contains an Authentication where isValid() is true.
     *
     *  @return the authenticated agent 
     *  @throws org.osid.IllegalStateException {@code
     *          isAuthenticated()} is {@code false}
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAuthenticatedAgent()
        throws org.osid.OperationFailedException {

        if (getProxy() != null) {
            return (super.getAuthenticatedAgent());
        } else {
            return (this.session.getAuthenticatedAgent());
        }
    }


    /**
     *  Gets the {@code Id} of the effective agent in use by this
     *  session. If {@code isAuthenticated()} is true, then the
     *  effective agent may be the same as the agent returned by
     *  {@code getAuthenticatedAgent()}. If {@code isAuthenticated()}
     *  is {@code false}, then the effective agent may be a default
     *  agent used for authorization by an unknwon or anonymous user.
     *
     *  @return the effective agent 
     */

    @OSID @Override
    public org.osid.id.Id getEffectiveAgentId() {
        if (getProxy() != null) {
            return (super.getEffectiveAgentId());
        } else {
            return (this.session.getEffectiveAgentId());
        }
    }


    /**
     *  Gets the effective agent in use by this session.
     *
     *  This method returns the effective agent if explicitly set,
     *  then the Proxy. If none is found and the session is
     *  authenticated, the authenticated agent is used.
     *
     *  @return the effective agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getEffectiveAgent()
        throws org.osid.OperationFailedException {

        if (getProxy() != null) {
            return (super.getEffectiveAgent());
        } else {
            return (this.session.getEffectiveAgent());
        }
    }


    /**
     *  Gets the service date which may be the current date or the
     *  effective date in which this session exists.
     *
     *  @return the service date
     */

    @OSID @Override
    public java.util.Date getDate() {
        if (getProxy() != null) {
            return (super.getDate());
        } else {
            return (this.session.getDate());
        }
    }


    /**
     *  Gets the rate of the service clock.
     *
     *  @return the clock rate
     */

    @OSID @Override
    public java.math.BigDecimal getClockRate() {
        if (getProxy() != null) {
            return (super.getClockRate());
        } else {
            return (this.session.getClockRate());
        }
    }


    /**
     *  Gets the {@code DisplayText} format {@code Type}
     *  preference in effect for this session.
     *
     *  @return the effective {@code DisplayText} format {@code Type}
     */

    @OSID @Override
    public org.osid.type.Type getFormatType() {
        if (getProxy() != null) {
            return (super.getFormatType());
        } else {
            return (this.session.getFormatType());
        }
    }

    
    /**
     *  Tests for the availability of transactions. 
     *
     *  @return {@code true} if transaction methods are available, 
     *          {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsTransactions() {
        return (this.session.supportsTransactions());
    }


    /**
     *  Starts a new transaction for this sesson. Transactions are a
     *  means for an OSID to provide an all-or-nothing set of
     *  operations within a session and may be used to coordinate this
     *  service from an external transaction manager. A session
     *  supports one transaction at a time.  Starting a second
     *  transaction before the previous has been committed or aborted
     *  results in an {@code ILLEGAL_STATE} error.
     *
     *  @return a new transaction 
     *  @throws org.osid.IllegalStateException a transaction is already open 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException transactions not supported 
     */

    @OSID @Override
    public org.osid.transaction.Transaction startTransaction()
        throws org.osid.IllegalStateException,
               org.osid.OperationFailedException,
               org.osid.UnsupportedException {

        return (this.session.startTransaction());
    }


    /**
     *  Closes this {@code osid.OsidSession}
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.session.close();
        return;
    }
}

