//
// AbstractOsidSearch.java
//
//     Defines a simple OSID search to draw from.
//
//
// Tom Coppeto
// OnTapSolutions
// 31 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalrangearg;


/**
 *  Defines a simple OsidSearch to extend. This class does absolutely
 *  nothing.
 */

public abstract class AbstractOsidSearch
    implements org.osid.OsidSearch {

    private final Types recordTypes = new TypeSet();
    private long start = 0;
    private long end   = 9223372036854775807L;
    

    /**
     *  By default, searches return all matching results. This method
     *  restricts the number of results by setting the start and end
     *  of the result set, starting from 1. The starting and ending
     *  results can be used for paging results when a certain ordering
     *  is requested. The ending position must be greater than the
     *  starting position.
     *
     *  @param  start the start of the result set 
     *  @param  end the end of the result set 
     *  @throws org.osid.InvalidArgumentException <code>end</code> is
     *          less than or equal to <code>start</code>
     */

    @OSID @Override
    public void limitResultSet(long start, long end) {
        cardinalrangearg(start, end, "start", "end");

	this.start = start;
	this.end = end;

	return;
    }


    /**
     *  Gets the start of the result set desired.
     *
     *  @return the start
     */

    protected long getStart() {
	return (this.start);
    }


    /**
     *  Gets the end of the result set desired.
     *
     *  @return the end
     */

    protected long getEnd() {
	return (this.end);
    }


    /**
     *  Tests if this search supports the given record <code> Type. </code> 
     *  The given record type may be supported by the object through 
     *  interface/type inheritence. This method should be checked before 
     *  retrieving the record interface. 
     *
     *  @param  recordType a type 
     *  @return <code> true </code> if a search record the given record <code> 
     *          Type </code> is available, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
	return (this.recordTypes.contains(recordType));
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
	return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recordTypes.toCollection()));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        nullarg(recordType, "record type");
	this.recordTypes.add(recordType);
	return;
    }
}
