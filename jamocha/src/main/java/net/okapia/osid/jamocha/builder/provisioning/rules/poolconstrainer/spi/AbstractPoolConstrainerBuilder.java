//
// AbstractPoolConstrainer.java
//
//     Defines a PoolConstrainer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.poolconstrainer.spi;


/**
 *  Defines a <code>PoolConstrainer</code> builder.
 */

public abstract class AbstractPoolConstrainerBuilder<T extends AbstractPoolConstrainerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidConstrainerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.rules.poolconstrainer.PoolConstrainerMiter poolConstrainer;


    /**
     *  Constructs a new <code>AbstractPoolConstrainerBuilder</code>.
     *
     *  @param poolConstrainer the pool constrainer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPoolConstrainerBuilder(net.okapia.osid.jamocha.builder.provisioning.rules.poolconstrainer.PoolConstrainerMiter poolConstrainer) {
        super(poolConstrainer);
        this.poolConstrainer = poolConstrainer;
        return;
    }


    /**
     *  Builds the pool constrainer.
     *
     *  @return the new pool constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.rules.PoolConstrainer build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.rules.poolconstrainer.PoolConstrainerValidator(getValidations())).validate(this.poolConstrainer);
        return (new net.okapia.osid.jamocha.builder.provisioning.rules.poolconstrainer.ImmutablePoolConstrainer(this.poolConstrainer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the pool constrainer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.rules.poolconstrainer.PoolConstrainerMiter getMiter() {
        return (this.poolConstrainer);
    }


    /**
     *  Adds a PoolConstrainer record.
     *
     *  @param record a pool constrainer record
     *  @param recordType the type of pool constrainer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.rules.records.PoolConstrainerRecord record, org.osid.type.Type recordType) {
        getMiter().addPoolConstrainerRecord(record, recordType);
        return (self());
    }
}       


