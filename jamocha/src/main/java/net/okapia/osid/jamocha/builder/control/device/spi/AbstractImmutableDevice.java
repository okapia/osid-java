//
// AbstractImmutableDevice.java
//
//     Wraps a mutable Device to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.device.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Device</code> to hide modifiers. This
 *  wrapper provides an immutized Device from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying device whose state changes are visible.
 */

public abstract class AbstractImmutableDevice
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.control.Device {

    private final org.osid.control.Device device;


    /**
     *  Constructs a new <code>AbstractImmutableDevice</code>.
     *
     *  @param device the device to immutablize
     *  @throws org.osid.NullArgumentException <code>device</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDevice(org.osid.control.Device device) {
        super(device);
        this.device = device;
        return;
    }


    /**
     *  Gets the device record corresponding to the given <code> Device 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> deviceRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(deviceRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  deviceRecordType the type of device record to retrieve 
     *  @return the device record 
     *  @throws org.osid.NullArgumentException <code> deviceRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(deviceRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.records.DeviceRecord getDeviceRecord(org.osid.type.Type deviceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.device.getDeviceRecord(deviceRecordType));
    }
}

