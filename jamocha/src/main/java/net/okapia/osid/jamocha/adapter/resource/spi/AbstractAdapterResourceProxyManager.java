//
// AbstractResourceProxyManager.java
//
//     An adapter for a ResourceProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ResourceProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterResourceProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.resource.ResourceProxyManager>
    implements org.osid.resource.ResourceProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterResourceProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterResourceProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterResourceProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterResourceProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if resource lookup is supported. 
     *
     *  @return <code> true </code> if resource lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLookup() {
        return (getAdapteeManager().supportsResourceLookup());
    }


    /**
     *  Tests if resource query is supported. 
     *
     *  @return <code> true </code> if resource query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (getAdapteeManager().supportsResourceQuery());
    }


    /**
     *  Tests if resource search is supported. 
     *
     *  @return <code> true </code> if resource search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearch() {
        return (getAdapteeManager().supportsResourceSearch());
    }


    /**
     *  Tests if resource administration is supported. 
     *
     *  @return <code> true </code> if resource administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceAdmin() {
        return (getAdapteeManager().supportsResourceAdmin());
    }


    /**
     *  Tests if resource notification is supported. Messages may be sent when 
     *  resources are created, modified, or deleted. 
     *
     *  @return <code> true </code> if resource notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceNotification() {
        return (getAdapteeManager().supportsResourceNotification());
    }


    /**
     *  Tests if retrieving mappings of resource and bins is supported. 
     *
     *  @return <code> true </code> if resource bin mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceBin() {
        return (getAdapteeManager().supportsResourceBin());
    }


    /**
     *  Tests if managing mappings of resource and bins is supported. 
     *
     *  @return <code> true </code> if resource bin assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceBinAssignment() {
        return (getAdapteeManager().supportsResourceBinAssignment());
    }


    /**
     *  Tests if resource smart bins are available. 
     *
     *  @return <code> true </code> if resource smart bins are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSmartBin() {
        return (getAdapteeManager().supportsResourceSmartBin());
    }


    /**
     *  Tests if membership queries are supported. 
     *
     *  @return <code> true </code> if membership queries are supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMembership() {
        return (getAdapteeManager().supportsMembership());
    }


    /**
     *  Tests if group resources are supported. 
     *
     *  @return <code> true </code> if group resources are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroup() {
        return (getAdapteeManager().supportsGroup());
    }


    /**
     *  Tests if group resource assignment is supported. 
     *
     *  @return <code> true </code> if group resource assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroupAssignment() {
        return (getAdapteeManager().supportsGroupAssignment());
    }


    /**
     *  Tests if group resource notification is supported. 
     *
     *  @return <code> true </code> if group resource notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroupNotification() {
        return (getAdapteeManager().supportsGroupNotification());
    }


    /**
     *  Tests if a group resource hierarchy service is supported. 
     *
     *  @return <code> true </code> if group resource hierarchy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroupHierarchy() {
        return (getAdapteeManager().supportsGroupHierarchy());
    }


    /**
     *  Tests if retrieving mappings of resource and agents is supported. 
     *
     *  @return <code> true </code> if resource agent mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceAgent() {
        return (getAdapteeManager().supportsResourceAgent());
    }


    /**
     *  Tests if managing mappings of resources and agents is supported. 
     *
     *  @return <code> true </code> if resource agent assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceAgentAssignment() {
        return (getAdapteeManager().supportsResourceAgentAssignment());
    }


    /**
     *  Tests if looking up resource relationships is supported. 
     *
     *  @return <code> true </code> if resource relationships lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipLookup() {
        return (getAdapteeManager().supportsResourceRelationshipLookup());
    }


    /**
     *  Tests if querying resource relationships is supported. 
     *
     *  @return <code> true </code> if resource relationships query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipQuery() {
        return (getAdapteeManager().supportsResourceRelationshipQuery());
    }


    /**
     *  Tests if searching resource relationships is supported. 
     *
     *  @return <code> true </code> if resource relationships search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipSearch() {
        return (getAdapteeManager().supportsResourceRelationshipSearch());
    }


    /**
     *  Tests if a resource relationships <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if resource relationships administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipAdmin() {
        return (getAdapteeManager().supportsResourceRelationshipAdmin());
    }


    /**
     *  Tests if a resource relationships <code> </code> notification service 
     *  is supported. 
     *
     *  @return <code> true </code> if resource relationships notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipNotification() {
        return (getAdapteeManager().supportsResourceRelationshipNotification());
    }


    /**
     *  Tests if retrieving mappings of resource relationships and bins is 
     *  supported. 
     *
     *  @return <code> true </code> if resource relationship bin mapping 
     *          retrieval is supported <code> , </code> <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipBin() {
        return (getAdapteeManager().supportsResourceRelationshipBin());
    }


    /**
     *  Tests if managing mappings of resource relationships and bins is 
     *  supported. 
     *
     *  @return <code> true </code> if resource relationship bin assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipBinAssignment() {
        return (getAdapteeManager().supportsResourceRelationshipBinAssignment());
    }


    /**
     *  Tests if resource relationship smart bins are available. 
     *
     *  @return <code> true </code> if resource relationship smart bins are 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipSmartBin() {
        return (getAdapteeManager().supportsResourceRelationshipSmartBin());
    }


    /**
     *  Tests if bin lookup is supported. 
     *
     *  @return <code> true </code> if bin lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinLookup() {
        return (getAdapteeManager().supportsBinLookup());
    }


    /**
     *  Tests if bin query is supported. 
     *
     *  @return <code> true </code> if bin query is supported <code> , </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (getAdapteeManager().supportsBinQuery());
    }


    /**
     *  Tests if bin search is supported. 
     *
     *  @return <code> true </code> if bin search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinSearch() {
        return (getAdapteeManager().supportsBinSearch());
    }


    /**
     *  Tests if bin administration is supported. 
     *
     *  @return <code> true </code> if bin administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinAdmin() {
        return (getAdapteeManager().supportsBinAdmin());
    }


    /**
     *  Tests if bin notification is supported. Messages may be sent when 
     *  <code> Bin </code> objects are created, deleted or updated. 
     *  Notifications for resources within bins are sent via the resource 
     *  notification session. 
     *
     *  @return <code> true </code> if bin notification is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinNotification() {
        return (getAdapteeManager().supportsBinNotification());
    }


    /**
     *  Tests if a bin hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a bin hierarchy traversal is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinHierarchy() {
        return (getAdapteeManager().supportsBinHierarchy());
    }


    /**
     *  Tests if a bin hierarchy design is supported. 
     *
     *  @return <code> true </code> if a bin hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinHierarchyDesign() {
        return (getAdapteeManager().supportsBinHierarchyDesign());
    }


    /**
     *  Tests if a resource batch service is available. 
     *
     *  @return <code> true </code> if a resource batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceBatch() {
        return (getAdapteeManager().supportsResourceBatch());
    }


    /**
     *  Tests if a resource demographic service is available. 
     *
     *  @return <code> true </code> if a resource demographic service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceDemographic() {
        return (getAdapteeManager().supportsResourceDemographic());
    }


    /**
     *  Gets all the resource record types supported. 
     *
     *  @return the list of supported resource record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceRecordTypes() {
        return (getAdapteeManager().getResourceRecordTypes());
    }


    /**
     *  Tests if a given resource record type is supported. 
     *
     *  @param  resourceRecordType the resource type 
     *  @return <code> true </code> if the resource record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resourceRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceRecordType(org.osid.type.Type resourceRecordType) {
        return (getAdapteeManager().supportsResourceRecordType(resourceRecordType));
    }


    /**
     *  Gets all the resource search record types supported. 
     *
     *  @return the list of supported resource search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceSearchRecordTypes() {
        return (getAdapteeManager().getResourceSearchRecordTypes());
    }


    /**
     *  Tests if a given resource search type is supported. 
     *
     *  @param  resourceSearchRecordType the resource search type 
     *  @return <code> true </code> if the resource search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resourceSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceSearchRecordType(org.osid.type.Type resourceSearchRecordType) {
        return (getAdapteeManager().supportsResourceSearchRecordType(resourceSearchRecordType));
    }


    /**
     *  Gets the supported <code> ResourceRelationship </code> record types. 
     *
     *  @return a list containing the supported <code> ResourceRelationship 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceRelationshipRecordTypes() {
        return (getAdapteeManager().getResourceRelationshipRecordTypes());
    }


    /**
     *  Tests if the given <code> ResourceRelationship </code> record type is 
     *  supported. 
     *
     *  @param  resourceRelationshipRecordType a <code> Type </code> 
     *          indicating a <code> ResourceRelationship </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipRecordType(org.osid.type.Type resourceRelationshipRecordType) {
        return (getAdapteeManager().supportsResourceRelationshipRecordType(resourceRelationshipRecordType));
    }


    /**
     *  Gets the supported <code> ResourceRelationship </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> ResourceRelationship 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceRelationshipSearchRecordTypes() {
        return (getAdapteeManager().getResourceRelationshipSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ResourceRelationship </code> search record 
     *  type is supported. 
     *
     *  @param  resourceRelationshipSearchRecordType a <code> Type </code> 
     *          indicating a <code> ResourceRelationship </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipSearchRecordType(org.osid.type.Type resourceRelationshipSearchRecordType) {
        return (getAdapteeManager().supportsResourceRelationshipSearchRecordType(resourceRelationshipSearchRecordType));
    }


    /**
     *  Gets all the bin record types supported. 
     *
     *  @return the list of supported bin record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBinRecordTypes() {
        return (getAdapteeManager().getBinRecordTypes());
    }


    /**
     *  Tests if a given bin record type is supported. 
     *
     *  @param  binRecordType the bin record type 
     *  @return <code> true </code> if the bin record type is supported <code> 
     *          , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> binRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBinRecordType(org.osid.type.Type binRecordType) {
        return (getAdapteeManager().supportsBinRecordType(binRecordType));
    }


    /**
     *  Gets all the bin search record types supported. 
     *
     *  @return the list of supported bin search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBinSearchRecordTypes() {
        return (getAdapteeManager().getBinSearchRecordTypes());
    }


    /**
     *  Tests if a given bin search record type is supported. 
     *
     *  @param  binSearchRecordType the bin search record type 
     *  @return <code> true </code> if the bin search record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> binSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBinSearchRecordType(org.osid.type.Type binSearchRecordType) {
        return (getAdapteeManager().supportsBinSearchRecordType(binSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceLookupSession getResourceLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy <code> a proxy </code> 
     *  @return <code> a ResourceLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceLookupSession getResourceLookupSessionForBin(org.osid.id.Id binId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceLookupSessionForBin(binId, proxy));
    }


    /**
     *  Gets a resource query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuerySession getResourceQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceQuerySession(proxy));
    }


    /**
     *  Gets a resource query session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuerySession getResourceQuerySessionForBin(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceQuerySessionForBin(binId, proxy));
    }


    /**
     *  Gets a resource search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchSession getResourceSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceSearchSession(proxy));
    }


    /**
     *  Gets a resource search session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchSession getResourceSearchSessionForBin(org.osid.id.Id binId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceSearchSessionForBin(binId, proxy));
    }


    /**
     *  Gets a resource administration session for creating, updating and 
     *  deleting resources. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAdminSession getResourceAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceAdminSession(proxy));
    }


    /**
     *  Gets a resource administration session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAdminSession getResourceAdminSessionForBin(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceAdminSessionForBin(binId, proxy));
    }


    /**
     *  Gets the resource notification session for the given bin. 
     *
     *  @param  resourceReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceNotificationSession getResourceNotificationSession(org.osid.resource.ResourceReceiver resourceReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceNotificationSession(resourceReceiver, proxy));
    }


    /**
     *  Gets the resource notification session for the given bin. 
     *
     *  @param  resourceReceiver notification callback 
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> resourceReceiver, binId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceNotificationSession getResourceNotificationSessionForBin(org.osid.resource.ResourceReceiver resourceReceiver, 
                                                                                              org.osid.id.Id binId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceNotificationSessionForBin(resourceReceiver, binId, proxy));
    }


    /**
     *  Gets the session for retrieving resource to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceBinSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceBin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceBinSession getResourceBinSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceBinSession(proxy));
    }


    /**
     *  Gets the session for assigning resource to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceBinAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceBinAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceBinAssignmentSession getResourceBinAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceBinAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic resource bins. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceSmartBinSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSmartBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSmartBinSession getResourceSmartBinSession(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceSmartBinSession(binId, proxy));
    }


    /**
     *  Gets the session for querying memberships. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MembershipSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMembership() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.MembershipSession getMembershipSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMembershipSession(proxy));
    }


    /**
     *  Gets a resource membership session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a MembershipSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMembership() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.MembershipSession getMembershipSessionForBin(org.osid.id.Id binId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMembershipSessionForBin(binId, proxy));
    }


    /**
     *  Gets the session for retrieving gropup memberships. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGroups() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupSession getGroupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupSession(proxy));
    }


    /**
     *  Gets a group session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGroup() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupSession getGroupSessionForBin(org.osid.id.Id binId, 
                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupSessionForBin(binId, proxy));
    }


    /**
     *  Gets the session for assigning resources to groups. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GroupAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupAssignmentSession getGroupAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupAssignmentSession(proxy));
    }


    /**
     *  Gets a group assignment session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> GroupAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupAssignmentSession getGroupAssignmentSessionForBin(org.osid.id.Id binId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupAssignmentSessionForBin(binId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to resource 
     *  changes. 
     *
     *  @param  groupRceeiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a GroupNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> groupReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupNotificationSession getGroupNotificationSession(org.osid.resource.GroupReceiver groupRceeiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupNotificationSession(groupRceeiver, proxy));
    }


    /**
     *  Gets the group notification session for the given bin. 
     *
     *  @param  groupRceeiver the notification callback 
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a GroupNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> groupReceiver, binId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupNotificationSession getGroupNotificationSessionForBin(org.osid.resource.GroupReceiver groupRceeiver, 
                                                                                        org.osid.id.Id binId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupNotificationSessionForBin(groupRceeiver, binId, proxy));
    }


    /**
     *  Gets the group hierarchy traversal session for the given resource 
     *  group. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a GroupHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchySession getGroupHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupHierarchySession(proxy));
    }


    /**
     *  Gets a group hierarchy session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> GroupHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupHierarchySession getGroupHierarchySessionForBin(org.osid.id.Id binId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGroupHierarchySessionForBin(binId, proxy));
    }


    /**
     *  Gets the session for retrieving resource agent mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgents() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentSession getResourceAgentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceAgentSession(proxy));
    }


    /**
     *  Gets a resource agent session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceAgentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAgent() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentSession getResourceAgentSessionForBin(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceAgentSessionForBin(binId, proxy));
    }


    /**
     *  Gets the session for assigning agents to resources. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceAgentAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgentAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentAssignmentSession getResourceAgentAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceAgentAssignmentSession(proxy));
    }


    /**
     *  Gets a resource agent session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceAgentAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgentAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentAssignmentSession getResourceAgentAssignmentSessionForBin(org.osid.id.Id binId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceAgentAssignmentSessionForBin(binId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipLookupSession getResourceRelationshipLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipLookupSession getResourceRelationshipLookupSessionForBin(org.osid.id.Id binId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipLookupSessionForBin(binId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuerySession getResourceRelationshipQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuerySession getResourceRelationshipQuerySessionForBin(org.osid.id.Id binId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipQuerySessionForBin(binId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSearchSession getResourceRelationshipSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship search service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipSearchSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSearchSession getResourceRelationshipSearchSessionForBin(org.osid.id.Id binId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipSearchSessionForBin(binId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipAdminSession getResourceRelationshipAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipAdminSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipAdminSession getResourceRelationshipAdminSessionForBin(org.osid.id.Id binId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipAdminSessionForBin(binId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship notification service. 
     *
     *  @param  resourceRelationshipReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipNotificationSession getResourceRelationshipNotificationSession(org.osid.resource.ResourceRelationshipReceiver resourceRelationshipReceiver, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipNotificationSession(resourceRelationshipReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship notification service for the given bin. 
     *
     *  @param  resourceRelationshipReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipReceiver, binId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshiptNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipNotificationSession getResourceRelationshipNotificationSessionForBin(org.osid.resource.ResourceRelationshipReceiver resourceRelationshipReceiver, 
                                                                                                                      org.osid.id.Id binId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipNotificationSessionForBin(resourceRelationshipReceiver, binId, proxy));
    }


    /**
     *  Gets the session for retrieving resource relationship to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipBinSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipBinSession getResourceRelationshipBinSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipBinSession(proxy));
    }


    /**
     *  Gets the session for assigning resource relationship to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipBinAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBinAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipBinAssignmentSession getResourceRelationshipBinAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipBinAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic resource relationship bins. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipSmartBinSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSmartBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSmartBinSession getResourceRelationshipSmartBinSession(org.osid.id.Id binId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipSmartBinSession(binId, proxy));
    }


    /**
     *  Gets the bin lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinLookupSession getBinLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBinLookupSession(proxy));
    }


    /**
     *  Gets the bin query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuerySession getBinQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBinQuerySession(proxy));
    }


    /**
     *  Gets the bin search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinSearchSession getBinSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBinSearchSession(proxy));
    }


    /**
     *  Gets the bin administrative session for creating, updating and 
     *  deleteing bins. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinAdminSession getBinAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBinAdminSession(proxy));
    }


    /**
     *  Gets the notification session for subscribing to changes to a bin. 
     *
     *  @param  binReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BinNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> binReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBinNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinNotificationSession getBinNotificationSession(org.osid.resource.BinReceiver binReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBinNotificationSession(binReceiver, proxy));
    }


    /**
     *  Gets the bin hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BinHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnimplementedException <code> supportsBinHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchySession getBinHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAdapteeManager().getBinHierarchySession(proxy));
    }


    /**
     *  Gets the bin hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBinHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchyDesignSession getBinHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAdapteeManager().getBinHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> ResourceBatchProxyManager. </code> 
     *
     *  @return a <code> ResourceBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.ResourceBatchProxyManager getResourceBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceBatchProxyManager());
    }


    /**
     *  Gets the <code> ResourceDemographicProxyManager. </code> 
     *
     *  @return a <code> ResourceDemographicProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceDemographic() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.ResourceDemographicProxyManager getResourceDemographicProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceDemographicProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
