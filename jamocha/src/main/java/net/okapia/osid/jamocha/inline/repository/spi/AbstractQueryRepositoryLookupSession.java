//
// AbstractQueryRepositoryLookupSession.java
//
//    An inline adapter that maps a RepositoryLookupSession to
//    a RepositoryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RepositoryLookupSession to
 *  a RepositoryQuerySession.
 */

public abstract class AbstractQueryRepositoryLookupSession
    extends net.okapia.osid.jamocha.repository.spi.AbstractRepositoryLookupSession
    implements org.osid.repository.RepositoryLookupSession {

    private final org.osid.repository.RepositoryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRepositoryLookupSession.
     *
     *  @param querySession the underlying repository query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRepositoryLookupSession(org.osid.repository.RepositoryQuerySession querySession) {
        nullarg(querySession, "repository query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Repository</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRepositories() {
        return (this.session.canSearchRepositories());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Repository</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Repository</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Repository</code> and
     *  retained for compatibility.
     *
     *  @param  repositoryId <code>Id</code> of the
     *          <code>Repository</code>
     *  @return the repository
     *  @throws org.osid.NotFoundException <code>repositoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>repositoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.RepositoryQuery query = getQuery();
        query.matchId(repositoryId, true);
        org.osid.repository.RepositoryList repositories = this.session.getRepositoriesByQuery(query);
        if (repositories.hasNext()) {
            return (repositories.getNextRepository());
        } 
        
        throw new org.osid.NotFoundException(repositoryId + " not found");
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  repositories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Repositories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  repositoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByIds(org.osid.id.IdList repositoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.RepositoryQuery query = getQuery();

        try (org.osid.id.IdList ids = repositoryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRepositoriesByQuery(query));
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  repository genus <code>Type</code> which does not include
     *  repositories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.RepositoryQuery query = getQuery();
        query.matchGenusType(repositoryGenusType, true);
        return (this.session.getRepositoriesByQuery(query));
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  repository genus <code>Type</code> and include any additional
     *  repositories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByParentGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.RepositoryQuery query = getQuery();
        query.matchParentGenusType(repositoryGenusType, true);
        return (this.session.getRepositoriesByQuery(query));
    }


    /**
     *  Gets a <code>RepositoryList</code> containing the given
     *  repository record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryRecordType a repository record type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByRecordType(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.RepositoryQuery query = getQuery();
        query.matchRecordType(repositoryRecordType, true);
        return (this.session.getRepositoriesByQuery(query));
    }


    /**
     *  Gets a <code>RepositoryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known repositories or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  repositories that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Repository</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.RepositoryQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getRepositoriesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Repositories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Repositories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.RepositoryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRepositoriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.repository.RepositoryQuery getQuery() {
        org.osid.repository.RepositoryQuery query = this.session.getRepositoryQuery();
        return (query);
    }
}
