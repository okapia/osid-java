//
// AbstractAssemblySpeedZoneQuery.java
//
//     A SpeedZoneQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SpeedZoneQuery that stores terms.
 */

public abstract class AbstractAssemblySpeedZoneQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.mapping.path.SpeedZoneQuery,
               org.osid.mapping.path.SpeedZoneQueryInspector,
               org.osid.mapping.path.SpeedZoneSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySpeedZoneQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySpeedZoneQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    


    /**
     *  Sets the path <code> Id </code> for this query to match speed zones 
     *  along the given path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        getAssembler().clearTerms(getPathIdColumn());
        return;
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (getAssembler().getIdTerms(getPathIdColumn()));
    }


    /**
     *  Orders the results by path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPath(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPathColumn(), style);
        return;
    }


    /**
     *  Gets the PathId column name.
     *
     * @return the column name
     */

    protected String getPathIdColumn() {
        return ("path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Tests if a path search order is available. 
     *
     *  @return <code> true </code> if a path search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearchOrder() {
        return (false);
    }


    /**
     *  Gets a path search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchOrder getPathSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPathSearchOrder() is false");
    }


    /**
     *  Gets the Path column name.
     *
     * @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Matches speed zones overlapping with the specified <code>
     *  Coordinate.  </code>
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        getAssembler().addCoordinateTerm(getCoordinateColumn(), coordinate, match);
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        getAssembler().clearTerms(getCoordinateColumn());
        return;
    }


    /**
     *  Gets the coordinate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (getAssembler().getCoordinateTerms(getCoordinateColumn()));
    }


    /**
     *  Gets the Coordinate column name.
     *
     * @return the column name
     */

    protected String getCoordinateColumn() {
        return ("coordinate");
    }


    /**
     *  Matches speed zones contained within the specified spatial
     *  unit.
     *
     *  @param  spatialUnit a spatial unit
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnit</code> is <code> null </code>
     */

    @OSID @Override
    public void matchContainingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit,
                                         boolean match) {
        getAssembler().addSpatialUnitTerm(getContainingSpatialUnitColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Clears the containing spatialunit query terms. 
     */

    @OSID @Override
    public void clearContainingSpatialUnitTerms() {
        getAssembler().clearTerms(getContainingSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the containing spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getContainingSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getContainingSpatialUnitColumn()));
    }


    /**
     *  Gets the ContainingSpatialUnit column name.
     *
     * @return the column name
     */

    protected String getContainingSpatialUnitColumn() {
        return ("containing_spatial_unit");
    }


    /**
     *  Orders the results by the length of the zone.
     *
     *  @param  style search order style
     *  @throws org.osid.NullArgumentException <code> style </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public void orderByLength(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLengthColumn(), style);
        return;
    }


    /**
     *  Gets the Length column name.
     *
     * @return the column name
     */

    protected String getLengthColumn() {
        return ("length");
    }


    /**
     *  Matches implicitly defined speed zones. 
     *
     *  @param  match <code> true </code> to match explcit speed zones with 
     *          any boundary, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        getAssembler().addBooleanTerm(getImplicitColumn(), match);
        return;
    }


    /**
     *  Clears the implicit query terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        getAssembler().clearTerms(getImplicitColumn());
        return;
    }


    /**
     *  Gets the implicit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getImplicitTerms() {
        return (getAssembler().getBooleanTerms(getImplicitColumn()));
    }


    /**
     *  Gets the Implicit column name.
     *
     * @return the column name
     */

    protected String getImplicitColumn() {
        return ("implicit");
    }


    /**
     *  Matches speed zones with speed limite within the given range 
     *  inclusive. 
     *
     *  @param  from starting speed range 
     *  @param  to ending speed range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSpeedLimit(org.osid.mapping.Speed from, 
                                org.osid.mapping.Speed to, boolean match) {
        getAssembler().addSpeedRangeTerm(getSpeedLimitColumn(), from, to, match);
        return;
    }


    /**
     *  Matches speed zones with any speed limit set. 
     *
     *  @param  match <code> true </code> to match implicit speed zones with 
     *          any speed limit, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnySpeedLimit(boolean match) {
        getAssembler().addSpeedRangeWildcardTerm(getSpeedLimitColumn(), match);
        return;
    }


    /**
     *  Clears the speed limit query terms. 
     */

    @OSID @Override
    public void clearSpeedLimitTerms() {
        getAssembler().clearTerms(getSpeedLimitColumn());
        return;
    }


    /**
     *  Gets the speed limit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpeedRangeTerm[] getSpeedLimitTerms() {
        return (getAssembler().getSpeedRangeTerms(getSpeedLimitColumn()));
    }


    /**
     *  Orders the results by speed limit.
     *
     *  @param  style search order style
     *  @throws org.osid.NullArgumentException <code> style </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public void orderBySpeedLimit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSpeedLimitColumn(), style);
        return;
    }


    /**
     *  Gets the SpeedLimit column name.
     *
     * @return the column name
     */

    protected String getSpeedLimitColumn() {
        return ("speed_limit");
    }


    /**
     *  Sets the map <code> Id </code> for this query. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this speedZone supports the given record
     *  <code>Type</code>.
     *
     *  @param  speedZoneRecordType a speed zone record type 
     *  @return <code>true</code> if the speedZoneRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type speedZoneRecordType) {
        for (org.osid.mapping.path.records.SpeedZoneQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  speedZoneRecordType the speed zone record type 
     *  @return the speed zone query record 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneQueryRecord getSpeedZoneQueryRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SpeedZoneQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  speedZoneRecordType the speed zone record type 
     *  @return the speed zone query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord getSpeedZoneQueryInspectorRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param speedZoneRecordType the speed zone record type
     *  @return the speed zone search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneSearchOrderRecord getSpeedZoneSearchOrderRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SpeedZoneSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this speed zone. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param speedZoneQueryRecord the speed zone query record
     *  @param speedZoneQueryInspectorRecord the speed zone query inspector
     *         record
     *  @param speedZoneSearchOrderRecord the speed zone search order record
     *  @param speedZoneRecordType speed zone record type
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneQueryRecord</code>,
     *          <code>speedZoneQueryInspectorRecord</code>,
     *          <code>speedZoneSearchOrderRecord</code> or
     *          <code>speedZoneRecordTypespeedZone</code> is
     *          <code>null</code>
     */
            
    protected void addSpeedZoneRecords(org.osid.mapping.path.records.SpeedZoneQueryRecord speedZoneQueryRecord, 
                                      org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord speedZoneQueryInspectorRecord, 
                                      org.osid.mapping.path.records.SpeedZoneSearchOrderRecord speedZoneSearchOrderRecord, 
                                      org.osid.type.Type speedZoneRecordType) {

        addRecordType(speedZoneRecordType);

        nullarg(speedZoneQueryRecord, "speed zone query record");
        nullarg(speedZoneQueryInspectorRecord, "speed zone query inspector record");
        nullarg(speedZoneSearchOrderRecord, "speed zone search odrer record");

        this.queryRecords.add(speedZoneQueryRecord);
        this.queryInspectorRecords.add(speedZoneQueryInspectorRecord);
        this.searchOrderRecords.add(speedZoneSearchOrderRecord);
        
        return;
    }
}
