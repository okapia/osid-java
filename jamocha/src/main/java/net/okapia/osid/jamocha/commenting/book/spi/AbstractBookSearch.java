//
// AbstractBookSearch.java
//
//     A template for making a Book Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.book.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing book searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBookSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.commenting.BookSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.commenting.records.BookSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.commenting.BookSearchOrder bookSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of books. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  bookIds list of books
     *  @throws org.osid.NullArgumentException
     *          <code>bookIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBooks(org.osid.id.IdList bookIds) {
        while (bookIds.hasNext()) {
            try {
                this.ids.add(bookIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBooks</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of book Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBookIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  bookSearchOrder book search order 
     *  @throws org.osid.NullArgumentException
     *          <code>bookSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>bookSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBookResults(org.osid.commenting.BookSearchOrder bookSearchOrder) {
	this.bookSearchOrder = bookSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.commenting.BookSearchOrder getBookSearchOrder() {
	return (this.bookSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given book search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a book implementing the requested record.
     *
     *  @param bookSearchRecordType a book search record
     *         type
     *  @return the book search record
     *  @throws org.osid.NullArgumentException
     *          <code>bookSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bookSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.BookSearchRecord getBookSearchRecord(org.osid.type.Type bookSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.commenting.records.BookSearchRecord record : this.records) {
            if (record.implementsRecordType(bookSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bookSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this book search. 
     *
     *  @param bookSearchRecord book search record
     *  @param bookSearchRecordType book search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBookSearchRecord(org.osid.commenting.records.BookSearchRecord bookSearchRecord, 
                                           org.osid.type.Type bookSearchRecordType) {

        addRecordType(bookSearchRecordType);
        this.records.add(bookSearchRecord);        
        return;
    }
}
