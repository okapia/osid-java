//
// PollsElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.polls.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PollsElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the PollsElement Id.
     *
     *  @return the polls element Id
     */

    public static org.osid.id.Id getPollsEntityId() {
        return (makeEntityId("osid.voting.Polls"));
    }


    /**
     *  Gets the CandidateId element Id.
     *
     *  @return the CandidateId element Id
     */

    public static org.osid.id.Id getCandidateId() {
        return (makeQueryElementId("osid.voting.polls.CandidateId"));
    }


    /**
     *  Gets the Candidate element Id.
     *
     *  @return the Candidate element Id
     */

    public static org.osid.id.Id getCandidate() {
        return (makeQueryElementId("osid.voting.polls.Candidate"));
    }


    /**
     *  Gets the AncestorPollsId element Id.
     *
     *  @return the AncestorPollsId element Id
     */

    public static org.osid.id.Id getAncestorPollsId() {
        return (makeQueryElementId("osid.voting.polls.AncestorPollsId"));
    }


    /**
     *  Gets the AncestorPolls element Id.
     *
     *  @return the AncestorPolls element Id
     */

    public static org.osid.id.Id getAncestorPolls() {
        return (makeQueryElementId("osid.voting.polls.AncestorPolls"));
    }


    /**
     *  Gets the DescendantPollsId element Id.
     *
     *  @return the DescendantPollsId element Id
     */

    public static org.osid.id.Id getDescendantPollsId() {
        return (makeQueryElementId("osid.voting.polls.DescendantPollsId"));
    }


    /**
     *  Gets the DescendantPolls element Id.
     *
     *  @return the DescendantPolls element Id
     */

    public static org.osid.id.Id getDescendantPolls() {
        return (makeQueryElementId("osid.voting.polls.DescendantPolls"));
    }
}
