//
// AbstractEntry.java
//
//     Defines an Entry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.shipment.entry.spi;


/**
 *  Defines an <code>Entry</code> builder.
 */

public abstract class AbstractEntryBuilder<T extends AbstractEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inventory.shipment.entry.EntryMiter entry;


    /**
     *  Constructs a new <code>AbstractEntryBuilder</code>.
     *
     *  @param entry the entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEntryBuilder(net.okapia.osid.jamocha.builder.inventory.shipment.entry.EntryMiter entry) {
        super(entry);
        this.entry = entry;
        return;
    }


    /**
     *  Builds the entry.
     *
     *  @return the new entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inventory.shipment.Entry build() {
        (new net.okapia.osid.jamocha.builder.validator.inventory.shipment.entry.EntryValidator(getValidations())).validate(this.entry);
        return (new net.okapia.osid.jamocha.builder.inventory.shipment.entry.ImmutableEntry(this.entry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.inventory.shipment.entry.EntryMiter getMiter() {
        return (this.entry);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>stock</code> is
     *          <code>null</code>
     */

    public T stock(org.osid.inventory.Stock stock) {
        getMiter().setStock(stock);
        return (self());
    }


    /**
     *  Sets the model.
     *
     *  @param model a model
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>model</code> is
     *          <code>null</code>
     */

    public T model(org.osid.inventory.Model model) {
        getMiter().setModel(model);
        return (self());
    }


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    public T item(org.osid.inventory.Item item) {
        getMiter().setItem(item);
        return (self());
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>quantity</code>
     *          is <code>null</code>
     */

    public T quantity(java.math.BigDecimal quantity) {
        getMiter().setQuantity(quantity);
        return (self());
    }


    /**
     *  Sets the unit type.
     *
     *  @param unitType an unit type
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>unitType</code>
     *          is <code>null</code>
     */

    public T unitType(org.osid.type.Type unitType) {
        getMiter().setUnitType(unitType);
        return (self());
    }


    /**
     *  Adds an Entry record.
     *
     *  @param record an entry record
     *  @param recordType the type of entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inventory.shipment.records.EntryRecord record, org.osid.type.Type recordType) {
        getMiter().addEntryRecord(record, recordType);
        return (self());
    }
}       


