//
// AbstractMapShipmentLookupSession
//
//    A simple framework for providing a Shipment lookup service
//    backed by a fixed collection of shipments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Shipment lookup service backed by a
 *  fixed collection of shipments. The shipments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Shipments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapShipmentLookupSession
    extends net.okapia.osid.jamocha.inventory.shipment.spi.AbstractShipmentLookupSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inventory.shipment.Shipment> shipments = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inventory.shipment.Shipment>());


    /**
     *  Makes a <code>Shipment</code> available in this session.
     *
     *  @param  shipment a shipment
     *  @throws org.osid.NullArgumentException <code>shipment<code>
     *          is <code>null</code>
     */

    protected void putShipment(org.osid.inventory.shipment.Shipment shipment) {
        this.shipments.put(shipment.getId(), shipment);
        return;
    }


    /**
     *  Makes an array of shipments available in this session.
     *
     *  @param  shipments an array of shipments
     *  @throws org.osid.NullArgumentException <code>shipments<code>
     *          is <code>null</code>
     */

    protected void putShipments(org.osid.inventory.shipment.Shipment[] shipments) {
        putShipments(java.util.Arrays.asList(shipments));
        return;
    }


    /**
     *  Makes a collection of shipments available in this session.
     *
     *  @param  shipments a collection of shipments
     *  @throws org.osid.NullArgumentException <code>shipments<code>
     *          is <code>null</code>
     */

    protected void putShipments(java.util.Collection<? extends org.osid.inventory.shipment.Shipment> shipments) {
        for (org.osid.inventory.shipment.Shipment shipment : shipments) {
            this.shipments.put(shipment.getId(), shipment);
        }

        return;
    }


    /**
     *  Removes a Shipment from this session.
     *
     *  @param  shipmentId the <code>Id</code> of the shipment
     *  @throws org.osid.NullArgumentException <code>shipmentId<code> is
     *          <code>null</code>
     */

    protected void removeShipment(org.osid.id.Id shipmentId) {
        this.shipments.remove(shipmentId);
        return;
    }


    /**
     *  Gets the <code>Shipment</code> specified by its <code>Id</code>.
     *
     *  @param  shipmentId <code>Id</code> of the <code>Shipment</code>
     *  @return the shipment
     *  @throws org.osid.NotFoundException <code>shipmentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>shipmentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.Shipment getShipment(org.osid.id.Id shipmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inventory.shipment.Shipment shipment = this.shipments.get(shipmentId);
        if (shipment == null) {
            throw new org.osid.NotFoundException("shipment not found: " + shipmentId);
        }

        return (shipment);
    }


    /**
     *  Gets all <code>Shipments</code>. In plenary mode, the returned
     *  list contains all known shipments or an error
     *  results. Otherwise, the returned list may contain only those
     *  shipments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Shipments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.shipment.shipment.ArrayShipmentList(this.shipments.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.shipments.clear();
        super.close();
        return;
    }
}
