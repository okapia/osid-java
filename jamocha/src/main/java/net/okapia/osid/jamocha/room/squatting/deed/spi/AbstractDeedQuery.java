//
// AbstractDeedQuery.java
//
//     A template for making a Deed Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for deeds.
 */

public abstract class AbstractDeedQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.room.squatting.DeedQuery {

    private final java.util.Collection<org.osid.room.squatting.records.DeedQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a building <code> Id. </code> 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        return;
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOwnerId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the owner resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOwnerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an owner resource query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsOwnerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getOwnerQuery() {
        throw new org.osid.UnimplementedException("supportsOwnerQuery() is false");
    }


    /**
     *  Clears the owner terms. 
     */

    @OSID @Override
    public void clearOwnerTerms() {
        return;
    }


    /**
     *  Sets the deed <code> Id </code> for this query to match rooms assigned 
     *  to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given deed query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a deed implementing the requested record.
     *
     *  @param deedRecordType a deed record type
     *  @return the deed query record
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deedRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedQueryRecord getDeedQueryRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.DeedQueryRecord record : this.records) {
            if (record.implementsRecordType(deedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedRecordType + " is not supported");
    }


    /**
     *  Adds a record to this deed query. 
     *
     *  @param deedQueryRecord deed query record
     *  @param deedRecordType deed record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDeedQueryRecord(org.osid.room.squatting.records.DeedQueryRecord deedQueryRecord, 
                                          org.osid.type.Type deedRecordType) {

        addRecordType(deedRecordType);
        nullarg(deedQueryRecord, "deed query record");
        this.records.add(deedQueryRecord);        
        return;
    }
}
