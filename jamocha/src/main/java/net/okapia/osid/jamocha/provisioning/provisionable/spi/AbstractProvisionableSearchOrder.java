//
// AbstractProvisionableSearchOdrer.java
//
//     Defines a ProvisionableSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionable.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProvisionableSearchOrder}.
 */

public abstract class AbstractProvisionableSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.provisioning.ProvisionableSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionableSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by pool. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPool(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a pool search order is available. 
     *
     *  @return <code> true </code> if a pool search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSearchOrder() {
        return (false);
    }


    /**
     *  Gets the pool search order. 
     *
     *  @return the pool search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsPoolSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchOrder getPoolSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPoolSearchOrder() is false");
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Orders the results by use. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByUse(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  provisionableRecordType a provisionable record type 
     *  @return {@code true} if the provisionableRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code provisionableRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionableRecordType) {
        for (org.osid.provisioning.records.ProvisionableSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  provisionableRecordType the provisionable record type 
     *  @return the provisionable search order record
     *  @throws org.osid.NullArgumentException
     *          {@code provisionableRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(provisionableRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableSearchOrderRecord getProvisionableSearchOrderRecord(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionableSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionableRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this provisionable. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionableRecord the provisionable search odrer record
     *  @param provisionableRecordType provisionable record type
     *  @throws org.osid.NullArgumentException
     *          {@code provisionableRecord} or
     *          {@code provisionableRecordTypeprovisionable} is
     *          {@code null}
     */
            
    protected void addProvisionableRecord(org.osid.provisioning.records.ProvisionableSearchOrderRecord provisionableSearchOrderRecord, 
                                     org.osid.type.Type provisionableRecordType) {

        addRecordType(provisionableRecordType);
        this.records.add(provisionableSearchOrderRecord);
        
        return;
    }
}
