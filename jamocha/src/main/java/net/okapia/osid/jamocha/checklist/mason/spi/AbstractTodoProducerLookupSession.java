//
// AbstractTodoProducerLookupSession.java
//
//    A starter implementation framework for providing a TodoProducer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a TodoProducer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getTodoProducers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractTodoProducerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.checklist.mason.TodoProducerLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.checklist.Checklist checklist = new net.okapia.osid.jamocha.nil.checklist.checklist.UnknownChecklist();
    

    /**
     *  Gets the <code>Checklist/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Checklist Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.checklist.getId());
    }


    /**
     *  Gets the <code>Checklist</code> associated with this 
     *  session.
     *
     *  @return the <code>Checklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.checklist);
    }


    /**
     *  Sets the <code>Checklist</code>.
     *
     *  @param  checklist the checklist for this session
     *  @throws org.osid.NullArgumentException <code>checklist</code>
     *          is <code>null</code>
     */

    protected void setChecklist(org.osid.checklist.Checklist checklist) {
        nullarg(checklist, "checklist");
        this.checklist = checklist;
        return;
    }


    /**
     *  Tests if this user can perform <code>TodoProducer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTodoProducers() {
        return (true);
    }


    /**
     *  A complete view of the <code>TodoProducer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTodoProducerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>TodoProducer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTodoProducerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todo producers in checklists which are
     *  children of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active todo producers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTodoProducerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive todo producers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTodoProducerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>TodoProducer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>TodoProducer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>TodoProducer</code> and
     *  retained for compatibility.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerId <code>Id</code> of the
     *          <code>TodoProducer</code>
     *  @return the todo producer
     *  @throws org.osid.NotFoundException <code>todoProducerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>todoProducerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducer(org.osid.id.Id todoProducerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.checklist.mason.TodoProducerList todoProducers = getTodoProducers()) {
            while (todoProducers.hasNext()) {
                org.osid.checklist.mason.TodoProducer todoProducer = todoProducers.getNextTodoProducer();
                if (todoProducer.getId().equals(todoProducerId)) {
                    return (todoProducer);
                }
            }
        } 

        throw new org.osid.NotFoundException(todoProducerId + " not found");
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  todoProducers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>TodoProducers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getTodoProducers()</code>.
     *
     *  @param  todoProducerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByIds(org.osid.id.IdList todoProducerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.checklist.mason.TodoProducer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = todoProducerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getTodoProducer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("todo producer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.checklist.mason.todoproducer.LinkedTodoProducerList(ret));
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the
     *  given todo producer genus <code>Type</code> which does not
     *  include todo producers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known todo
     *  producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getTodoProducers()</code>.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.mason.todoproducer.TodoProducerGenusFilterList(getTodoProducers(), todoProducerGenusType));
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the
     *  given todo producer genus <code>Type</code> and include any
     *  additional todo producers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known todo
     *  producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTodoProducers()</code>.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByParentGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTodoProducersByGenusType(todoProducerGenusType));
    }


    /**
     *  Gets a <code>TodoProducerList</code> containing the given todo
     *  producer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known todo
     *  producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTodoProducers()</code>.
     *
     *  @param  todoProducerRecordType a todoProducer record type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByRecordType(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.mason.todoproducer.TodoProducerRecordFilterList(getTodoProducers(), todoProducerRecordType));
    }


    /**
     *  Gets a <code>TodoProducer</code> by <code>Todo</code>. In
     *  plenary mode, the returned list contains all known todo
     *  producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible
     *  through this session.
     *
     *  @param  todoId a todo <code>Id</code> 
     *  @return the returned <code>TodoProducer</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NotFoundException <code>todoId</code> is not
     *          found or has no producer
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducerByTodo(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.NotFoundException("no producer found");
    }


    /**
     *  Gets all <code>TodoProducers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @return a list of <code>TodoProducers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.checklist.mason.TodoProducerList getTodoProducers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the todo producer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of todo producers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.checklist.mason.TodoProducerList filterTodoProducersOnViews(org.osid.checklist.mason.TodoProducerList list)
        throws org.osid.OperationFailedException {

        org.osid.checklist.mason.TodoProducerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.checklist.mason.todoproducer.ActiveTodoProducerFilterList(ret);
        }

        return (ret);
    }
}
