//
// QueueMiter.java
//
//     Defines a Queue miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.queue;


/**
 *  Defines a <code>Queue</code> miter for use with the builders.
 */

public interface QueueMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidGovernatorMiter,
            org.osid.provisioning.Queue {


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException <code>broker</code> is
     *          <code>null</code>
     */

    public void setBroker(org.osid.provisioning.Broker broker);


    /**
     *  Sets the size.
     *
     *  @param size a size
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    public void setSize(long size);


    /**
     *  Sets the ewa.
     *
     *  @param ewa the estimated waiting time
     *  @throws org.osid.NullArgumentException <code>ewa</code> is
     *          <code>null</code>
     */

    public void setEWA(org.osid.calendaring.Duration ewa);


    /**
     *  Sets the can specify provisionable.
     *
     *  @param canSpecifyProvisionable <code> true </code> if
     *         provisionables can be specified, <code> false </code>
     *         otherwise
     */

    public void setCanSpecifyProvisionable(boolean canSpecifyProvisionable);


    /**
     *  Adds a Queue record.
     *
     *  @param record a queue record
     *  @param recordType the type of queue record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addQueueRecord(org.osid.provisioning.records.QueueRecord record, org.osid.type.Type recordType);
}       


