//
// AbstractAssemblyProficiencyQuery.java
//
//     A ProficiencyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProficiencyQuery that stores terms.
 */

public abstract class AbstractAssemblyProficiencyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.learning.ProficiencyQuery,
               org.osid.learning.ProficiencyQueryInspector,
               org.osid.learning.ProficiencySearchOrder {

    private final java.util.Collection<org.osid.learning.records.ProficiencyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ProficiencyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ProficiencySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProficiencyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProficiencyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets a resource search order. 
     *
     *  @return a resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveId(org.osid.id.Id objectiveId, boolean match) {
        getAssembler().addIdTerm(getObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveIdTerms() {
        getAssembler().clearTerms(getObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the objective <code> Id </code> terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getObjectiveIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the objective. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByObjective(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getObjectiveColumn(), style);
        return;
    }


    /**
     *  Gets the ObjectiveId column name.
     *
     * @return the column name
     */

    protected String getObjectiveIdColumn() {
        return ("objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  objectives. 
     *
     *  @return <code> true </code> if an robjective query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveQuery() is false");
    }


    /**
     *  Matches an activity that has any objective assigned. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          objective, <code> false </code> to match activities with no 
     *          objective 
     */

    @OSID @Override
    public void matchAnyObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the objective terms. 
     */

    @OSID @Override
    public void clearObjectiveTerms() {
        getAssembler().clearTerms(getObjectiveColumn());
        return;
    }


    /**
     *  Gets the objective terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Tests if an objective search order is available. 
     *
     *  @return <code> true </code> if an objective search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSearchOrder() {
        return (false);
    }


    /**
     *  Gets an objective search order. 
     *
     *  @return an objective search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchOrder getObjectiveSearchOrder() {
        throw new org.osid.UnimplementedException("supportsObjectiveSearchOrder() is false");
    }


    /**
     *  Gets the Objective column name.
     *
     * @return the column name
     */

    protected String getObjectiveColumn() {
        return ("objective");
    }


    /**
     *  Sets the completion for this query to match completion percentages 
     *  between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchCompletion(java.math.BigDecimal start, 
                                java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getCompletionColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the completion terms. 
     */

    @OSID @Override
    public void clearCompletionTerms() {
        getAssembler().clearTerms(getCompletionColumn());
        return;
    }


    /**
     *  Gets the completion terms. 
     *
     *  @return the completion terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCompletionTerms() {
        return (getAssembler().getDecimalRangeTerms(getCompletionColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the completion. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompletion(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompletionColumn(), style);
        return;
    }


    /**
     *  Gets the Completion column name.
     *
     * @return the column name
     */

    protected String getCompletionColumn() {
        return ("completion");
    }


    /**
     *  Sets the minimum completion for this query. 
     *
     *  @param  completion completion percentage 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumCompletion(java.math.BigDecimal completion, 
                                       boolean match) {
        getAssembler().addDecimalTerm(getMinimumCompletionColumn(), completion, match);
        return;
    }


    /**
     *  Clears the minimum completion terms. 
     */

    @OSID @Override
    public void clearMinimumCompletionTerms() {
        getAssembler().clearTerms(getMinimumCompletionColumn());
        return;
    }


    /**
     *  Gets the minimum completion terms. 
     *
     *  @return the minimum completion terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumCompletionTerms() {
        return (getAssembler().getDecimalTerms(getMinimumCompletionColumn()));
    }


    /**
     *  Gets the MinimumCompletion column name.
     *
     * @return the column name
     */

    protected String getMinimumCompletionColumn() {
        return ("minimum_completion");
    }


    /**
     *  Sets the level grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getLevelIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears all level <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        getAssembler().clearTerms(getLevelIdColumn());
        return;
    }


    /**
     *  Gets the level <code> Id </code> query terms. 
     *
     *  @return the level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (getAssembler().getIdTerms(getLevelIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the level. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLevelColumn(), style);
        return;
    }


    /**
     *  Gets the LevelId column name.
     *
     * @return the column name
     */

    protected String getLevelIdColumn() {
        return ("level_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches an assessment offered that has any level assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any level, 
     *          <code> false </code> to match offerings with no levsls 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        getAssembler().addIdWildcardTerm(getLevelColumn(), match);
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        getAssembler().clearTerms(getLevelColumn());
        return;
    }


    /**
     *  Gets the level query terms. 
     *
     *  @return the level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }


    /**
     *  Gets the Level column name.
     *
     * @return the column name
     */

    protected String getLevelColumn() {
        return ("level");
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                     boolean match) {
        getAssembler().addIdTerm(getObjectiveBankIdColumn(), objectiveBankId, match);
        return;
    }


    /**
     *  Clears the objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveBankIdTerms() {
        getAssembler().clearTerms(getObjectiveBankIdColumn());
        return;
    }


    /**
     *  Gets the objective bank <code> Id </code> query terms. 
     *
     *  @return the objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveBankIdTerms() {
        return (getAssembler().getIdTerms(getObjectiveBankIdColumn()));
    }


    /**
     *  Gets the ObjectiveBankId column name.
     *
     * @return the column name
     */

    protected String getObjectiveBankIdColumn() {
        return ("objective_bank_id");
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveBankQuery() is false");
    }


    /**
     *  Clears the objective bank terms. 
     */

    @OSID @Override
    public void clearObjectiveBankTerms() {
        getAssembler().clearTerms(getObjectiveBankColumn());
        return;
    }


    /**
     *  Gets the objective bank query terms. 
     *
     *  @return the objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }


    /**
     *  Gets the ObjectiveBank column name.
     *
     * @return the column name
     */

    protected String getObjectiveBankColumn() {
        return ("objective_bank");
    }


    /**
     *  Tests if this proficiency supports the given record
     *  <code>Type</code>.
     *
     *  @param  proficiencyRecordType a proficiency record type 
     *  @return <code>true</code> if the proficiencyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type proficiencyRecordType) {
        for (org.osid.learning.records.ProficiencyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  proficiencyRecordType the proficiency record type 
     *  @return the proficiency query record 
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proficiencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencyQueryRecord getProficiencyQueryRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ProficiencyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  proficiencyRecordType the proficiency record type 
     *  @return the proficiency query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proficiencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencyQueryInspectorRecord getProficiencyQueryInspectorRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ProficiencyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param proficiencyRecordType the proficiency record type
     *  @return the proficiency search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proficiencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencySearchOrderRecord getProficiencySearchOrderRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ProficiencySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this proficiency. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param proficiencyQueryRecord the proficiency query record
     *  @param proficiencyQueryInspectorRecord the proficiency query inspector
     *         record
     *  @param proficiencySearchOrderRecord the proficiency search order record
     *  @param proficiencyRecordType proficiency record type
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyQueryRecord</code>,
     *          <code>proficiencyQueryInspectorRecord</code>,
     *          <code>proficiencySearchOrderRecord</code> or
     *          <code>proficiencyRecordTypeproficiency</code> is
     *          <code>null</code>
     */
            
    protected void addProficiencyRecords(org.osid.learning.records.ProficiencyQueryRecord proficiencyQueryRecord, 
                                      org.osid.learning.records.ProficiencyQueryInspectorRecord proficiencyQueryInspectorRecord, 
                                      org.osid.learning.records.ProficiencySearchOrderRecord proficiencySearchOrderRecord, 
                                      org.osid.type.Type proficiencyRecordType) {

        addRecordType(proficiencyRecordType);

        nullarg(proficiencyQueryRecord, "proficiency query record");
        nullarg(proficiencyQueryInspectorRecord, "proficiency query inspector record");
        nullarg(proficiencySearchOrderRecord, "proficiency search odrer record");

        this.queryRecords.add(proficiencyQueryRecord);
        this.queryInspectorRecords.add(proficiencyQueryInspectorRecord);
        this.searchOrderRecords.add(proficiencySearchOrderRecord);
        
        return;
    }
}
