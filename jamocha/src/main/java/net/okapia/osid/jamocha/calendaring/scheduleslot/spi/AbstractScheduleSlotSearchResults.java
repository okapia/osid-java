//
// AbstractScheduleSlotSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractScheduleSlotSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.ScheduleSlotSearchResults {

    private org.osid.calendaring.ScheduleSlotList scheduleSlots;
    private final org.osid.calendaring.ScheduleSlotQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractScheduleSlotSearchResults.
     *
     *  @param scheduleSlots the result set
     *  @param scheduleSlotQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>scheduleSlots</code>
     *          or <code>scheduleSlotQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractScheduleSlotSearchResults(org.osid.calendaring.ScheduleSlotList scheduleSlots,
                                            org.osid.calendaring.ScheduleSlotQueryInspector scheduleSlotQueryInspector) {
        nullarg(scheduleSlots, "schedule slots");
        nullarg(scheduleSlotQueryInspector, "schedule slot query inspectpr");

        this.scheduleSlots = scheduleSlots;
        this.inspector = scheduleSlotQueryInspector;

        return;
    }


    /**
     *  Gets the schedule slot list resulting from a search.
     *
     *  @return a schedule slot list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlots() {
        if (this.scheduleSlots == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.ScheduleSlotList scheduleSlots = this.scheduleSlots;
        this.scheduleSlots = null;
	return (scheduleSlots);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.ScheduleSlotQueryInspector getScheduleSlotQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  schedule slot search record <code> Type. </code> This method must
     *  be used to retrieve a scheduleSlot implementing the requested
     *  record.
     *
     *  @param scheduleSlotSearchRecordType a scheduleSlot search 
     *         record type 
     *  @return the schedule slot search
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(scheduleSlotSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotSearchResultsRecord getScheduleSlotSearchResultsRecord(org.osid.type.Type scheduleSlotSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.records.ScheduleSlotSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(scheduleSlotSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(scheduleSlotSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record schedule slot search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addScheduleSlotRecord(org.osid.calendaring.records.ScheduleSlotSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "schedule slot record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
