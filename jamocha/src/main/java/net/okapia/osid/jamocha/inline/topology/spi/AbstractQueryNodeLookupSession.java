//
// AbstractQueryNodeLookupSession.java
//
//    An inline adapter that maps a NodeLookupSession to
//    a NodeQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a NodeLookupSession to
 *  a NodeQuerySession.
 */

public abstract class AbstractQueryNodeLookupSession
    extends net.okapia.osid.jamocha.topology.spi.AbstractNodeLookupSession
    implements org.osid.topology.NodeLookupSession {

    private final org.osid.topology.NodeQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryNodeLookupSession.
     *
     *  @param querySession the underlying node query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryNodeLookupSession(org.osid.topology.NodeQuerySession querySession) {
        nullarg(querySession, "node query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Graph</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform <code>Node</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupNodes() {
        return (this.session.canSearchNodes());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include nodes in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    
     
    /**
     *  Gets the <code>Node</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Node</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Node</code> and
     *  retained for compatibility.
     *
     *  @param  nodeId <code>Id</code> of the
     *          <code>Node</code>
     *  @return the node
     *  @throws org.osid.NotFoundException <code>nodeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>nodeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Node getNode(org.osid.id.Id nodeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.NodeQuery query = getQuery();
        query.matchId(nodeId, true);
        org.osid.topology.NodeList nodes = this.session.getNodesByQuery(query);
        if (nodes.hasNext()) {
            return (nodes.getNextNode());
        } 
        
        throw new org.osid.NotFoundException(nodeId + " not found");
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  nodes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Nodes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  nodeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>nodeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByIds(org.osid.id.IdList nodeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.NodeQuery query = getQuery();

        try (org.osid.id.IdList ids = nodeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getNodesByQuery(query));
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  node genus <code>Type</code> which does not include
     *  nodes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.NodeQuery query = getQuery();
        query.matchGenusType(nodeGenusType, true);
        return (this.session.getNodesByQuery(query));
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  node genus <code>Type</code> and include any additional
     *  nodes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByParentGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.NodeQuery query = getQuery();
        query.matchParentGenusType(nodeGenusType, true);
        return (this.session.getNodesByQuery(query));
    }


    /**
     *  Gets a <code>NodeList</code> containing the given
     *  node record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  nodeRecordType a node record type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByRecordType(org.osid.type.Type nodeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.NodeQuery query = getQuery();
        query.matchRecordType(nodeRecordType, true);
        return (this.session.getNodesByQuery(query));
    }

    
    /**
     *  Gets all <code>Nodes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  nodes or an error results. Otherwise, the returned list
     *  may contain only those nodes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Nodes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.NodeQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getNodesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.topology.NodeQuery getQuery() {
        org.osid.topology.NodeQuery query = this.session.getNodeQuery();
        return (query);
    }
}
