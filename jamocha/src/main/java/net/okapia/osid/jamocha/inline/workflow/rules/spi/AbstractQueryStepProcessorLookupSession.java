//
// AbstractQueryStepProcessorLookupSession.java
//
//    An inline adapter that maps a StepProcessorLookupSession to
//    a StepProcessorQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a StepProcessorLookupSession to
 *  a StepProcessorQuerySession.
 */

public abstract class AbstractQueryStepProcessorLookupSession
    extends net.okapia.osid.jamocha.workflow.rules.spi.AbstractStepProcessorLookupSession
    implements org.osid.workflow.rules.StepProcessorLookupSession {

    private boolean activeonly    = false;
    private final org.osid.workflow.rules.StepProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryStepProcessorLookupSession.
     *
     *  @param querySession the underlying step processor query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryStepProcessorLookupSession(org.osid.workflow.rules.StepProcessorQuerySession querySession) {
        nullarg(querySession, "step processor query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Office</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform <code>StepProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStepProcessors() {
        return (this.session.canSearchStepProcessors());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step processors in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active step processors are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveStepProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive step processors are returned by methods in
     *  this session.
     */
    
    @OSID @Override
    public void useAnyStatusStepProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>StepProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>StepProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>StepProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param  stepProcessorId <code>Id</code> of the
     *          <code>StepProcessor</code>
     *  @return the step processor
     *  @throws org.osid.NotFoundException <code>stepProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessor getStepProcessor(org.osid.id.Id stepProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepProcessorQuery query = getQuery();
        query.matchId(stepProcessorId, true);
        org.osid.workflow.rules.StepProcessorList stepProcessors = this.session.getStepProcessorsByQuery(query);
        if (stepProcessors.hasNext()) {
            return (stepProcessors.getNextStepProcessor());
        } 
        
        throw new org.osid.NotFoundException(stepProcessorId + " not found");
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  stepProcessors specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>StepProcessors</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByIds(org.osid.id.IdList stepProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepProcessorQuery query = getQuery();

        try (org.osid.id.IdList ids = stepProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getStepProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the
     *  given step processor genus <code>Type</code> which does not
     *  include step processors of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepProcessorQuery query = getQuery();
        query.matchGenusType(stepProcessorGenusType, true);
        return (this.session.getStepProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the
     *  given step processor genus <code>Type</code> and include any
     *  additional step processors with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByParentGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepProcessorQuery query = getQuery();
        query.matchParentGenusType(stepProcessorGenusType, true);
        return (this.session.getStepProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>StepProcessorList</code> containing the given
     *  step processor record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known step
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorRecordType a stepProcessor record type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByRecordType(org.osid.type.Type stepProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepProcessorQuery query = getQuery();
        query.matchRecordType(stepProcessorRecordType, true);
        return (this.session.getStepProcessorsByQuery(query));
    }

    
    /**
     *  Gets all <code>StepProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known step
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @return a list of <code>StepProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepProcessorQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getStepProcessorsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.workflow.rules.StepProcessorQuery getQuery() {
        org.osid.workflow.rules.StepProcessorQuery query = this.session.getStepProcessorQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
