//
// InvariantMapProductLookupSession
//
//    Implements a Product lookup service backed by a fixed collection of
//    products.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements a Product lookup service backed by a fixed
 *  collection of products. The products are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProductLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractMapProductLookupSession
    implements org.osid.ordering.ProductLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProductLookupSession</code> with no
     *  products.
     *  
     *  @param store the store
     *  @throws org.osid.NullArgumnetException {@code store} is
     *          {@code null}
     */

    public InvariantMapProductLookupSession(org.osid.ordering.Store store) {
        setStore(store);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProductLookupSession</code> with a single
     *  product.
     *  
     *  @param store the store
     *  @param product a single product
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code product} is <code>null</code>
     */

      public InvariantMapProductLookupSession(org.osid.ordering.Store store,
                                               org.osid.ordering.Product product) {
        this(store);
        putProduct(product);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProductLookupSession</code> using an array
     *  of products.
     *  
     *  @param store the store
     *  @param products an array of products
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code products} is <code>null</code>
     */

      public InvariantMapProductLookupSession(org.osid.ordering.Store store,
                                               org.osid.ordering.Product[] products) {
        this(store);
        putProducts(products);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProductLookupSession</code> using a
     *  collection of products.
     *
     *  @param store the store
     *  @param products a collection of products
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code products} is <code>null</code>
     */

      public InvariantMapProductLookupSession(org.osid.ordering.Store store,
                                               java.util.Collection<? extends org.osid.ordering.Product> products) {
        this(store);
        putProducts(products);
        return;
    }
}
