//
// AbstractItem.java
//
//     Defines an Item.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Item</code>.
 */

public abstract class AbstractItem
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.billing.Item {

    private org.osid.billing.Category category;
    private org.osid.financials.Account account;
    private org.osid.ordering.Product product;
    private org.osid.financials.Currency amount;
    private org.osid.calendaring.Duration recurringInterval;
    private boolean debit = true;

    private final java.util.Collection<org.osid.billing.records.ItemRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the category <code> Id. </code> 
     *
     *  @return the category <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCategoryId() {
        return (this.category.getId());
    }


    /**
     *  Gets the item category. 
     *
     *  @return the category 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Category getCategory()
        throws org.osid.OperationFailedException {

        return (this.category);
    }


    /**
     *  Sets the category.
     *
     *  @param category a category
     *  @throws org.osid.NullArgumentException <code>category</code>
     *          is <code>null</code>
     */

    protected void setCategory(org.osid.billing.Category category) {
        nullarg(category, "category");
        this.category = category;
        return;
    }


    /**
     *  Tests if this item relates to s specific general ledger account. 
     *
     *  @return <code> true </code> if this item has an account, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasAccount() {
        return (this.account != null);
    }


    /**
     *  Gets the account <code> Id. </code> 
     *
     *  @return the account <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAccount() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        if (!hasAccount()) {
            throw new org.osid.IllegalStateException("hasAccount() is false");
        }

        return (this.account.getId());
    }


    /**
     *  Gets the item account. 
     *
     *  @return the account 
     *  @throws org.osid.IllegalStateException <code> hasAccount() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {

        if (!hasAccount()) {
            throw new org.osid.IllegalStateException("hasAccount() is false");
        }

        return (this.account);
    }


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    protected void setAccount(org.osid.financials.Account account) {
        nullarg(account, "account");
        this.account = account;
        return;
    }


    /**
     *  Tests if this item relates to s specific product. 
     *
     *  @return <code> true </code> if this item has a product, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean hasProduct() {
        return (this.product != null);
    }


    /**
     *  Gets the product <code> Id. </code> 
     *
     *  @return the product <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProduct() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProductId() {
        if (!hasProduct()) {
            throw new org.osid.IllegalStateException("hasProduct() is false");
        }

        return (this.product.getId());
    }


    /**
     *  Gets the product. 
     *
     *  @return the product 
     *  @throws org.osid.IllegalStateException <code> hasProduct() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct()
        throws org.osid.OperationFailedException {

        if (!hasProduct()) {
            throw new org.osid.IllegalStateException("hasProduct() is false");
        }

        return (this.product);
    }


    /**
     *  Sets the product.
     *
     *  @param product a product
     *  @throws org.osid.NullArgumentException <code>product</code> is
     *          <code>null</code>
     */

    protected void setProduct(org.osid.ordering.Product product) {
        nullarg(product, "product");
        this.product = product;
        return;
    }


    /**
     *  Gets the amount of this item. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.amount);
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }


    /**
     *  Tests if the amount is a debit or a credit. 
     *
     *  @return <code> true </code> if this item amount is a debit,
     *          <code> false </code> if it is a credit
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.debit);
    }


    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this item amount is a
     *         debit, <code> false </code> if it is a credit
     */

    protected void setDebit(boolean debit) {
        this.debit = debit;
        return;
    }


    /**
     *  Tests if this item is a recurring charge. 
     *
     *  @return <code> true </code> if this item is recurring, <code>
     *          false </code> if one-time
     */

    @OSID @Override
    public boolean isRecurring() {
        return (this.recurringInterval != null);
    }


    /**
     *  Gets the recurring interval. 
     *
     *  @return the interval 
     *  @throws org.osid.IllegalStateException <code> isRecurring() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRecurringInterval() {
        if (!isRecurring()) {
            throw new org.osid.IllegalStateException("isRecurring() is false");
        }

        return (this.recurringInterval);
    }


    /**
     *  Sets the recurring interval.
     *
     *  @param interval a recurring interval
     *  @throws org.osid.NullArgumentException
     *          <code>interval</code> is <code>null</code>
     */

    protected void setRecurringInterval(org.osid.calendaring.Duration interval) {
        nullarg(interval, "recurring interval");
        this.recurringInterval = interval;
        return;
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.billing.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Item</code>
     *  record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemRecord the item record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException <code>itemRecord</code>
     *          or <code>itemRecordType</code> is <code>null</code>
     */
            
    protected void addItemRecord(org.osid.billing.records.ItemRecord itemRecord, 
                                 org.osid.type.Type itemRecordType) {

        nullarg(itemRecord, "item record");
        addRecordType(itemRecordType);
        this.records.add(itemRecord);
        
        return;
    }
}
