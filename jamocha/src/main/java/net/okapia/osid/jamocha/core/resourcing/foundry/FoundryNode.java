//
// FoundryNode.java
//
//     Defines a Foundry node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  A class for managing a hierarchy of foundry nodes in core.
 */

public final class FoundryNode
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractFoundryNode
    implements org.osid.resourcing.FoundryNode {


    /**
     *  Constructs a new <code>FoundryNode</code> from a single
     *  foundry.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException <code>foundry</code> is 
     *          <code>null</code>.
     */

    public FoundryNode(org.osid.resourcing.Foundry foundry) {
        super(foundry);
        return;
    }


    /**
     *  Constructs a new <code>FoundryNode</code>.
     *
     *  @param foundry the foundry
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>.
     */

    public FoundryNode(org.osid.resourcing.Foundry foundry, boolean root, boolean leaf) {
        super(foundry, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this foundry.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.resourcing.FoundryNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this foundry.
     *
     *  @param foundry the foundry to add as a parent
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.resourcing.Foundry foundry) {
        addParent(new FoundryNode(foundry));
        return;
    }


    /**
     *  Adds a child to this foundry.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.resourcing.FoundryNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this foundry.
     *
     *  @param foundry the foundry to add as a child
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.resourcing.Foundry foundry) {
        addChild(new FoundryNode(foundry));
        return;
    }
}
