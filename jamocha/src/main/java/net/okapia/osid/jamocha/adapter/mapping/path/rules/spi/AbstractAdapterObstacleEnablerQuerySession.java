//
// AbstractQueryObstacleEnablerLookupSession.java
//
//    An ObstacleEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ObstacleEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterObstacleEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.rules.ObstacleEnablerQuerySession {

    private final org.osid.mapping.path.rules.ObstacleEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterObstacleEnablerQuerySession.
     *
     *  @param session the underlying obstacle enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterObstacleEnablerQuerySession(org.osid.mapping.path.rules.ObstacleEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeMap</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeMap Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@codeMap</code> associated with this 
     *  session.
     *
     *  @return the {@codeMap</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@codeObstacleEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchObstacleEnablers() {
        return (this.session.canSearchObstacleEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include obstacle enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this map only.
     */
    
    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    
      
    /**
     *  Gets an obstacle enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the obstacle enabler query 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerQuery getObstacleEnablerQuery() {
        return (this.session.getObstacleEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  obstacleEnablerQuery the obstacle enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code obstacleEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code obstacleEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByQuery(org.osid.mapping.path.rules.ObstacleEnablerQuery obstacleEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getObstacleEnablersByQuery(obstacleEnablerQuery));
    }
}
