//
// MutableMapSupersedingEventLookupSession
//
//    Implements a SupersedingEvent lookup service backed by a collection of
//    supersedingEvents that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a SupersedingEvent lookup service backed by a collection of
 *  superseding events. The superseding events are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of superseding events can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapSupersedingEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapSupersedingEventLookupSession
    implements org.osid.calendaring.SupersedingEventLookupSession {


    /**
     *  Constructs a new {@code MutableMapSupersedingEventLookupSession}
     *  with no superseding events.
     *
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumentException {@code calendar} is
     *          {@code null}
     */

      public MutableMapSupersedingEventLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSupersedingEventLookupSession} with a
     *  single supersedingEvent.
     *
     *  @param calendar the calendar  
     *  @param supersedingEvent a superseding event
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code supersedingEvent} is {@code null}
     */

    public MutableMapSupersedingEventLookupSession(org.osid.calendaring.Calendar calendar,
                                           org.osid.calendaring.SupersedingEvent supersedingEvent) {
        this(calendar);
        putSupersedingEvent(supersedingEvent);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSupersedingEventLookupSession}
     *  using an array of superseding events.
     *
     *  @param calendar the calendar
     *  @param supersedingEvents an array of superseding events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code supersedingEvents} is {@code null}
     */

    public MutableMapSupersedingEventLookupSession(org.osid.calendaring.Calendar calendar,
                                           org.osid.calendaring.SupersedingEvent[] supersedingEvents) {
        this(calendar);
        putSupersedingEvents(supersedingEvents);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSupersedingEventLookupSession}
     *  using a collection of superseding events.
     *
     *  @param calendar the calendar
     *  @param supersedingEvents a collection of superseding events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code supersedingEvents} is {@code null}
     */

    public MutableMapSupersedingEventLookupSession(org.osid.calendaring.Calendar calendar,
                                           java.util.Collection<? extends org.osid.calendaring.SupersedingEvent> supersedingEvents) {

        this(calendar);
        putSupersedingEvents(supersedingEvents);
        return;
    }

    
    /**
     *  Makes a {@code SupersedingEvent} available in this session.
     *
     *  @param supersedingEvent a superseding event
     *  @throws org.osid.NullArgumentException {@code supersedingEvent{@code  is
     *          {@code null}
     */

    @Override
    public void putSupersedingEvent(org.osid.calendaring.SupersedingEvent supersedingEvent) {
        super.putSupersedingEvent(supersedingEvent);
        return;
    }


    /**
     *  Makes an array of superseding events available in this session.
     *
     *  @param supersedingEvents an array of superseding events
     *  @throws org.osid.NullArgumentException {@code supersedingEvents{@code 
     *          is {@code null}
     */

    @Override
    public void putSupersedingEvents(org.osid.calendaring.SupersedingEvent[] supersedingEvents) {
        super.putSupersedingEvents(supersedingEvents);
        return;
    }


    /**
     *  Makes collection of superseding events available in this session.
     *
     *  @param supersedingEvents a collection of superseding events
     *  @throws org.osid.NullArgumentException {@code supersedingEvents{@code  is
     *          {@code null}
     */

    @Override
    public void putSupersedingEvents(java.util.Collection<? extends org.osid.calendaring.SupersedingEvent> supersedingEvents) {
        super.putSupersedingEvents(supersedingEvents);
        return;
    }


    /**
     *  Removes a SupersedingEvent from this session.
     *
     *  @param supersedingEventId the {@code Id} of the superseding event
     *  @throws org.osid.NullArgumentException {@code supersedingEventId{@code 
     *          is {@code null}
     */

    @Override
    public void removeSupersedingEvent(org.osid.id.Id supersedingEventId) {
        super.removeSupersedingEvent(supersedingEventId);
        return;
    }    
}
