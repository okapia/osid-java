//
// AbstractImmutableScene.java
//
//     Wraps a mutable Scene to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.scene.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Scene</code> to hide modifiers. This
 *  wrapper provides an immutized Scene from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying scene whose state changes are visible.
 */

public abstract class AbstractImmutableScene
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.control.Scene {

    private final org.osid.control.Scene scene;


    /**
     *  Constructs a new <code>AbstractImmutableScene</code>.
     *
     *  @param scene the scene to immutablize
     *  @throws org.osid.NullArgumentException <code>scene</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableScene(org.osid.control.Scene scene) {
        super(scene);
        this.scene = scene;
        return;
    }


    /**
     *  Gets the setting item <code> Ids. </code> 
     *
     *  @return the setting item <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSettingIds() {
        return (this.scene.getSettingIds());
    }


    /**
     *  Gets the settings. 
     *
     *  @return list of settings part of this scene 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException {

        return (this.scene.getSettings());
    }


    /**
     *  Gets the scene record corresponding to the given <code> Scene
     *  </code> record <code> Type. </code> This method is used to
     *  retrieve an object implementing the requested record. The
     *  <code> sceneRecordType </code> may be the <code> Type </code>
     *  returned in <code> getRecordTypes() </code> or any of its
     *  parents in a <code> Type </code> hierarchy where <code>
     *  hasRecordType(sceneRecordType) </code> is <code> true </code>
     *  .
     *
     *  @param  sceneRecordType the type of scene record to retrieve 
     *  @return the scene record 
     *  @throws org.osid.NullArgumentException <code> sceneRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(sceneRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.records.SceneRecord getSceneRecord(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException {

        return (this.scene.getSceneRecord(sceneRecordType));
    }
}

