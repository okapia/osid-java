//
// AbstractAssessmentOfferedSearch.java
//
//     A template for making an AssessmentOffered Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing assessment offered searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAssessmentOfferedSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.assessment.AssessmentOfferedSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.assessment.AssessmentOfferedSearchOrder assessmentOfferedSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of assessments offered. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  assessmentOfferedIds list of assessments offered
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAssessmentsOffered(org.osid.id.IdList assessmentOfferedIds) {
        while (assessmentOfferedIds.hasNext()) {
            try {
                this.ids.add(assessmentOfferedIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAssessmentsOffered</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of assessment offered Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAssessmentOfferedIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  assessmentOfferedSearchOrder assessment offered search order 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>assessmentOfferedSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAssessmentOfferedResults(org.osid.assessment.AssessmentOfferedSearchOrder assessmentOfferedSearchOrder) {
	this.assessmentOfferedSearchOrder = assessmentOfferedSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.assessment.AssessmentOfferedSearchOrder getAssessmentOfferedSearchOrder() {
	return (this.assessmentOfferedSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given assessment offered search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment offered implementing the requested record.
     *
     *  @param assessmentOfferedSearchRecordType an assessment offered search record
     *         type
     *  @return the assessment offered search record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentOfferedSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedSearchRecord getAssessmentOfferedSearchRecord(org.osid.type.Type assessmentOfferedSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.assessment.records.AssessmentOfferedSearchRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment offered search. 
     *
     *  @param assessmentOfferedSearchRecord assessment offered search record
     *  @param assessmentOfferedSearchRecordType assessmentOffered search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentOfferedSearchRecord(org.osid.assessment.records.AssessmentOfferedSearchRecord assessmentOfferedSearchRecord, 
                                           org.osid.type.Type assessmentOfferedSearchRecordType) {

        addRecordType(assessmentOfferedSearchRecordType);
        this.records.add(assessmentOfferedSearchRecord);        
        return;
    }
}
