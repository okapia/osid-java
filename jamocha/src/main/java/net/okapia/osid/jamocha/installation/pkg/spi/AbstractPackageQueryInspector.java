//
// AbstractPackageQueryInspector.java
//
//     A template for making a PackageQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for packages.
 */

public abstract class AbstractPackageQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQueryInspector
    implements org.osid.installation.PackageQueryInspector {

    private final java.util.Collection<org.osid.installation.records.PackageQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the version query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionTerms() {
        return (new org.osid.search.terms.VersionTerm[0]);
    }


    /**
     *  Gets the version since terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionSinceTerms() {
        return (new org.osid.search.terms.VersionTerm[0]);
    }


    /**
     *  Gets the copyright terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCopyrightTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the requires license acknowledgement terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRequiresLicenseAcknowledgementTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the creator <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreatorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the creator query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCreatorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the release date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReleaseDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the package dependency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the package dependency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getDependencyTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the url terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getURLTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the installation <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstallationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the installation query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.InstallationQueryInspector[] getInstallationTerms() {
        return (new org.osid.installation.InstallationQueryInspector[0]);
    }


    /**
     *  Gets the dependent package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the dependent package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getDependentTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the versioned package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVersionedPackageIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the versioned package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getVersionedPackageTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the installation content <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstallationContentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the installation content query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.InstallationContentQueryInspector[] getInstallationContentTerms() {
        return (new org.osid.installation.InstallationContentQueryInspector[0]);
    }


    /**
     *  Gets the depot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDepotIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the depot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.DepotQueryInspector[] getDepotTerms() {
        return (new org.osid.installation.DepotQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given package query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a package implementing the requested record.
     *
     *  @param pkgRecordType a package record type
     *  @return the package query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pkgRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageQueryInspectorRecord getPackageQueryInspectorRecord(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.PackageQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgRecordType + " is not supported");
    }


    /**
     *  Adds a record to this package query. 
     *
     *  @param pkgQueryInspectorRecord package query inspector
     *         record
     *  @param pkgRecordType pkg record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPackageQueryInspectorRecord(org.osid.installation.records.PackageQueryInspectorRecord pkgQueryInspectorRecord, 
                                                   org.osid.type.Type pkgRecordType) {

        addRecordType(pkgRecordType);
        nullarg(pkgRecordType, "package record type");
        this.records.add(pkgQueryInspectorRecord);        
        return;
    }
}
