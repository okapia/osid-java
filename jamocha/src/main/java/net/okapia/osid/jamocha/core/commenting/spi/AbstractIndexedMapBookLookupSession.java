//
// AbstractIndexedMapBookLookupSession.java
//
//    A simple framework for providing a Book lookup service
//    backed by a fixed collection of books with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Book lookup service backed by a
 *  fixed collection of books. The books are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some books may be compatible
 *  with more types than are indicated through these book
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Books</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBookLookupSession
    extends AbstractMapBookLookupSession
    implements org.osid.commenting.BookLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.commenting.Book> booksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.commenting.Book>());
    private final MultiMap<org.osid.type.Type, org.osid.commenting.Book> booksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.commenting.Book>());


    /**
     *  Makes a <code>Book</code> available in this session.
     *
     *  @param  book a book
     *  @throws org.osid.NullArgumentException <code>book<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBook(org.osid.commenting.Book book) {
        super.putBook(book);

        this.booksByGenus.put(book.getGenusType(), book);
        
        try (org.osid.type.TypeList types = book.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.booksByRecord.put(types.getNextType(), book);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a book from this session.
     *
     *  @param bookId the <code>Id</code> of the book
     *  @throws org.osid.NullArgumentException <code>bookId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBook(org.osid.id.Id bookId) {
        org.osid.commenting.Book book;
        try {
            book = getBook(bookId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.booksByGenus.remove(book.getGenusType());

        try (org.osid.type.TypeList types = book.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.booksByRecord.remove(types.getNextType(), book);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBook(bookId);
        return;
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  book genus <code>Type</code> which does not include
     *  books of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known books or an error results. Otherwise,
     *  the returned list may contain only those books that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.commenting.book.ArrayBookList(this.booksByGenus.get(bookGenusType)));
    }


    /**
     *  Gets a <code>BookList</code> containing the given
     *  book record <code>Type</code>. In plenary mode, the
     *  returned list contains all known books or an error
     *  results. Otherwise, the returned list may contain only those
     *  books that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  bookRecordType a book record type 
     *  @return the returned <code>book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByRecordType(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.commenting.book.ArrayBookList(this.booksByRecord.get(bookRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.booksByGenus.clear();
        this.booksByRecord.clear();

        super.close();

        return;
    }
}
