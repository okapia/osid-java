//
// AbstractSourceableOsidObjectQuery.java
//
//     A sourceable OisdObjectQuery with stored terms.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A sourceable OsidObjectQuery with stored terms.
 */

public abstract class AbstractSourceableOsidObjectQuery
    extends AbstractOsidObjectQuery
    implements org.osid.OsidSourceableQuery,
               org.osid.OsidObjectQuery {

    private final OsidSourceableQuery query;


    /**
     *  Constructs a new <code>AbstractSourceableOsidObjectQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractSourceableOsidObjectQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        super(factory);
        this.query = new OsidSourceableQuery(factory);
        return;
    }


    /**
     *  Match the <code> Id </code> of the provider resource. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProviderId(org.osid.id.Id resourceId, boolean match) {
        this.query.matchProviderId(resourceId, match);
        return;
    }


    /**
     *  Clears all provider <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProviderIdTerms() {
        this.query.clearProviderIdTerms();
        return;
    }


    /**
     *  Gets all the provider Id query terms.
     *
     *  @return a collection of the provider Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getProviderIdTerms() {
        return (this.query.getProviderIdTerms());
    }


    /**
     *  Tests if a <code> ResourceQuery </code> for the provider is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProviderQuery() {
        return (this.query.supportsProviderQuery());
    }


    /**
     *  Gets the query for the provider. Each retrieval performs a boolean 
     *  <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the provider query 
     *  @throws org.osid.UnimplementedException <code> supportsProviderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getProviderQuery(boolean match) {
        return (this.query.getProviderQuery(match));
    }


    /**
     *  Match compositions with a provider value. 
     *
     *  @param  match <code> true </code> to match any provider, <code> false 
     *          </code> to match compositions with no providers 
     */

    @OSID @Override
    public void matchAnyProvider(boolean match) {
        this.query.matchAnyProvider(match);
        return;
    }


    /**
     *  Clears all provider terms. 
     */

    @OSID @Override
    public void clearProviderTerms() {
        this.query.clearProviderTerms();
        return;
    }


    /**
     *  Match the <code> Id </code> of an asset used for branding. 
     *
     *  @param  assetId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrandingId(org.osid.id.Id assetId, boolean match) {
        this.query.matchBrandingId(assetId, match);
        return;
    }


    /**
     *  Clears all asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBrandingIdTerms() {
        this.query.clearBrandingTerms();
        return;
    }


    /**
     *  Gets all the branding Id query terms.
     *
     *  @return a collection of the branding Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getBrandingIdTerms() {
        return (this.query.getBrandingIdTerms());
    }


    /**
     *  Tests if an <code> AssetQuery </code> for the branding is available. 
     *
     *  @return <code> true </code> if a asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrandingQuery() {
        return (this.query.supportsBrandingQuery());
    }


    /**
     *  Gets the query for an asset. Each retrieval performs a boolean <code> 
     *  OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsBrandingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getBrandingQuery(boolean match) {
        return (this.query.getBrandingQuery(match));
    }


    /**
     *  Match objects with any branding. 
     *
     *  @param  match <code> true </code> to match any asset, <code> false 
     *          </code> to match no assets 
     */

    @OSID @Override
    public void matchAnyBranding(boolean match) {
        this.query.matchAnyBranding(match);
        return;
    }


    /**
     *  Clears all branding terms. 
     */

    @OSID @Override
    public void clearBrandingTerms() {
        this.query.clearBrandingTerms();
        return;
    }


    /**
     *  Adds a license to match. Multiple license matches can be added
     *  to perform a boolean <code> OR </code> among them.
     *
     *  @param  license a string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> license </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> license </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLicense(String license, org.osid.type.Type stringMatchType, boolean match) {
        this.query.matchLicense(license, stringMatchType, match);
        return;
    }


    /**
     *  Matches any object with a license. 
     *
     *  @param  match <code> true </code> to match any license, <code> false 
     *          </code> to match objects with no license 
     */

    @OSID @Override
    public void matchAnyLicense(boolean match) {
        this.query.matchAnyLicense(match);
        return;
    }


    /**
     *  Clears all license terms. 
     */

    @OSID @Override
    public void clearLicenseTerms() {
        this.query.clearLicenseTerms();
        return;
    }


    /**
     *  Gets all the license query terms.
     *
     *  @return a collection of the license query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getLicenseTerms() {
        return (this.query.getLicenseTerms());
    }


    protected class OsidSourceableQuery
        extends AbstractOsidSourceableQuery
        implements org.osid.OsidSourceableQuery {


        /**
         *  Constructs a new <code>OsidSourceableQuery</code>.
         *
         *  @param factory the term factory
         *  @throws org.osid.NullArgumentException
         *          <code>factory</code> is <code>null</code>
         */
        
        protected OsidSourceableQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
            super(factory);
            return;
        }


        /**
         *  Match the <code> Id </code> of the provider resource. 
         *
         *  @param  resourceId <code> Id </code> to match 
         *  @param  match <code> true </code> if for a positive match, <code> 
         *          false </code> for a negative match 
         *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
         *          <code> null </code> 
         */

        @OSID @Override
        public void matchProviderId(org.osid.id.Id resourceId, boolean match) {
            super.matchProviderId(resourceId, match);
            return;
        }


        /**
         *  Clears all provider <code> Id </code> terms. 
         */

        @OSID @Override
        public void clearProviderIdTerms() {
            super.clearProviderIdTerms();
            return;
        }


        /**
         *  Gets all the provider Id query terms.
         *
         *  @return a collection of the provider Id query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.IdTerm> getProviderIdTerms() {
            return (super.getProviderIdTerms());
        }


        /**
         *  Tests if a <code> ResourceQuery </code> for the provider is available. 
         *
         *  @return <code> true </code> if a resource query is available, <code> 
         *          false </code> otherwise 
         */

        @OSID @Override
        public boolean supportsProviderQuery() {
            return (super.supportsProviderQuery());
        }


        /**
         *  Gets the query for the provider. Each retrieval performs a boolean 
         *  <code> OR. </code> 
         *
         *  @param  match <code> true </code> if for a positive match, <code> 
         *          false </code> for a negative match 
         *  @return the provider query 
         *  @throws org.osid.UnimplementedException <code> supportsProviderQuery() 
         *          </code> is <code> false </code> 
         */

        @OSID @Override
        public org.osid.resource.ResourceQuery getProviderQuery(boolean match) {
            return (super.getProviderQuery(match));
        }


        /**
         *  Match compositions with a provider value. 
         *
         *  @param  match <code> true </code> to match any provider, <code> false 
         *          </code> to match compositions with no providers 
         */

        @OSID @Override
        public void matchAnyProvider(boolean match) {
            super.matchAnyProvider(match);
            return;
        }


        /**
         *  Clears all provider terms. 
         */

        @OSID @Override
        public void clearProviderTerms() {
            super.clearProviderTerms();
            return;
        }


        /**
         *  Match the <code> Id </code> of an asset used for branding. 
         *
         *  @param  assetId <code> Id </code> to match 
         *  @param  match <code> true </code> if for a positive match, <code> 
         *          false </code> for a negative match 
         *  @throws org.osid.NullArgumentException <code> assetId </code> is 
         *          <code> null </code> 
         */

        @OSID @Override
        public void matchBrandingId(org.osid.id.Id assetId, boolean match) {
            super.matchBrandingId(assetId, match);
            return;
        }


        /**
         *  Clears all asset <code> Id </code> terms. 
         */

        @OSID @Override
        public void clearBrandingIdTerms() {
            super.clearBrandingTerms();
            return;
        }


        /**
         *  Gets all the branding Id query terms.
         *
         *  @return a collection of the branding Id query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.IdTerm> getBrandingIdTerms() {
            return (super.getBrandingIdTerms());
        }


        /**
         *  Tests if an <code> AssetQuery </code> for the branding is available. 
         *
         *  @return <code> true </code> if a asset query is available, <code> 
         *          false </code> otherwise 
         */

        @OSID @Override
        public boolean supportsBrandingQuery() {
            return (super.supportsBrandingQuery());
        }


        /**
         *  Gets the query for an asset. Each retrieval performs a boolean <code> 
         *  OR. </code> 
         *
         *  @param  match <code> true </code> if for a positive match, <code> 
         *          false </code> for a negative match 
         *  @return the asset query 
         *  @throws org.osid.UnimplementedException <code> supportsBrandingQuery() 
         *          </code> is <code> false </code> 
         */

        @OSID @Override
        public org.osid.repository.AssetQuery getBrandingQuery(boolean match) {
            return (super.getBrandingQuery(match));
        }


        /**
         *  Match objects with any branding. 
         *
         *  @param  match <code> true </code> to match any asset, <code> false 
         *          </code> to match no assets 
         */

        @OSID @Override
        public void matchAnyBranding(boolean match) {
            super.matchAnyBranding(match);
            return;
        }


        /**
         *  Clears all branding terms. 
         */

        @OSID @Override
        public void clearBrandingTerms() {
            super.clearBrandingTerms();
            return;
        }


        /**
         *  Adds a license to match. Multiple license matches can be added
         *  to perform a boolean <code> OR </code> among them.
         *
         *  @param  license a string to match 
         *  @param  stringMatchType the string match type 
         *  @param  match <code> true </code> for a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException <code> license </code> is 
         *          not of <code> stringMatchType </code> 
         *  @throws org.osid.NullArgumentException <code> license </code> or 
         *          <code> stringMatchType </code> is <code> null </code> 
         *  @throws org.osid.UnsupportedException <code> 
         *          supportsStringMatchType(stringMatchType) </code> is <code> 
         *          false </code> 
         */

        @OSID @Override
        public void matchLicense(String license, org.osid.type.Type stringMatchType, boolean match) {
            super.matchLicense(license, stringMatchType, match);
            return;
        }


        /**
         *  Matches any object with a license. 
         *
         *  @param  match <code> true </code> to match any license, <code> false 
         *          </code> to match objects with no license 
         */

        @OSID @Override
        public void matchAnyLicense(boolean match) {
            super.matchAnyLicense(match);
            return;
        }


        /**
         *  Clears all license terms. 
         */

        @OSID @Override
        public void clearLicenseTerms() {
            super.clearLicenseTerms();
            return;
        }


        /**
         *  Gets all the license query terms.
         *
         *  @return a collection of the license query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.StringTerm> getLicenseTerms() {
            return (super.getLicenseTerms());
        }
    }
}
