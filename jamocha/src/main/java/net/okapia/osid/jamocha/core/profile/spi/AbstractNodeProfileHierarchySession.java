//
// AbstractNodeProfileHierarchySession.java
//
//     Defines a Profile hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a profile hierarchy session for delivering a hierarchy
 *  of profiles using the ProfileNode interface.
 */

public abstract class AbstractNodeProfileHierarchySession
    extends net.okapia.osid.jamocha.profile.spi.AbstractProfileHierarchySession
    implements org.osid.profile.ProfileHierarchySession {

    private java.util.Collection<org.osid.profile.ProfileNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root profile <code> Ids </code> in this hierarchy.
     *
     *  @return the root profile <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootProfileIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.profile.profilenode.ProfileNodeToIdList(this.roots));
    }


    /**
     *  Gets the root profiles in the profile hierarchy. A node
     *  with no parents is an orphan. While all profile <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root profiles 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getRootProfiles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.profile.profilenode.ProfileNodeToProfileList(new net.okapia.osid.jamocha.profile.profilenode.ArrayProfileNodeList(this.roots)));
    }


    /**
     *  Adds a root profile node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootProfile(org.osid.profile.ProfileNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root profile nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootProfiles(java.util.Collection<org.osid.profile.ProfileNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root profile node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootProfile(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.profile.ProfileNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Profile </code> has any parents. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @return <code> true </code> if the profile has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentProfiles(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getProfileNode(profileId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  profile.
     *
     *  @param  id an <code> Id </code> 
     *  @param  profileId the <code> Id </code> of a profile 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> profileId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> profileId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfProfile(org.osid.id.Id id, org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.profile.ProfileNodeList parents = getProfileNode(profileId).getParentProfileNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextProfileNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given profile. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @return the parent <code> Ids </code> of the profile 
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentProfileIds(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.profile.profile.ProfileToIdList(getParentProfiles(profileId)));
    }


    /**
     *  Gets the parents of the given profile. 
     *
     *  @param  profileId the <code> Id </code> to query 
     *  @return the parents of the profile 
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getParentProfiles(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.profile.profilenode.ProfileNodeToProfileList(getProfileNode(profileId).getParentProfileNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  profile.
     *
     *  @param  id an <code> Id </code> 
     *  @param  profileId the Id of a profile 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> profileId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfProfile(org.osid.id.Id id, org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfProfile(id, profileId)) {
            return (true);
        }

        try (org.osid.profile.ProfileList parents = getParentProfiles(profileId)) {
            while (parents.hasNext()) {
                if (isAncestorOfProfile(id, parents.getNextProfile().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a profile has any children. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @return <code> true </code> if the <code> profileId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildProfiles(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProfileNode(profileId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  profile.
     *
     *  @param  id an <code> Id </code> 
     *  @param profileId the <code> Id </code> of a 
     *         profile
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> profileId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> profileId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfProfile(org.osid.id.Id id, org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfProfile(profileId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  profile.
     *
     *  @param  profileId the <code> Id </code> to query 
     *  @return the children of the profile 
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildProfileIds(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.profile.profile.ProfileToIdList(getChildProfiles(profileId)));
    }


    /**
     *  Gets the children of the given profile. 
     *
     *  @param  profileId the <code> Id </code> to query 
     *  @return the children of the profile 
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getChildProfiles(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.profile.profilenode.ProfileNodeToProfileList(getProfileNode(profileId).getChildProfileNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  profile.
     *
     *  @param  id an <code> Id </code> 
     *  @param profileId the <code> Id </code> of a 
     *         profile
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> profileId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfProfile(org.osid.id.Id id, org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfProfile(profileId, id)) {
            return (true);
        }

        try (org.osid.profile.ProfileList children = getChildProfiles(profileId)) {
            while (children.hasNext()) {
                if (isDescendantOfProfile(id, children.getNextProfile().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  profile.
     *
     *  @param  profileId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified profile node 
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getProfileNodeIds(org.osid.id.Id profileId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.profile.profilenode.ProfileNodeToNode(getProfileNode(profileId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given profile.
     *
     *  @param  profileId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified profile node 
     *  @throws org.osid.NotFoundException <code> profileId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> profileId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileNode getProfileNodes(org.osid.id.Id profileId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProfileNode(profileId));
    }


    /**
     *  Closes this <code>ProfileHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a profile node.
     *
     *  @param profileId the id of the profile node
     *  @throws org.osid.NotFoundException <code>profileId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>profileId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.profile.ProfileNode getProfileNode(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(profileId, "profile Id");
        for (org.osid.profile.ProfileNode profile : this.roots) {
            if (profile.getId().equals(profileId)) {
                return (profile);
            }

            org.osid.profile.ProfileNode r = findProfile(profile, profileId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(profileId + " is not found");
    }


    protected org.osid.profile.ProfileNode findProfile(org.osid.profile.ProfileNode node, 
                                                       org.osid.id.Id profileId) 
	throws org.osid.OperationFailedException {

        try (org.osid.profile.ProfileNodeList children = node.getChildProfileNodes()) {
            while (children.hasNext()) {
                org.osid.profile.ProfileNode profile = children.getNextProfileNode();
                if (profile.getId().equals(profileId)) {
                    return (profile);
                }
                
                profile = findProfile(profile, profileId);
                if (profile != null) {
                    return (profile);
                }
            }
        }

        return (null);
    }
}
