//
// AbstractAdapterCommentLookupSession.java
//
//    A Comment lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.commenting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Comment lookup session adapter.
 */

public abstract class AbstractAdapterCommentLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.commenting.CommentLookupSession {

    private final org.osid.commenting.CommentLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCommentLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCommentLookupSession(org.osid.commenting.CommentLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Book/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Book Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBookId() {
        return (this.session.getBookId());
    }


    /**
     *  Gets the {@code Book} associated with this session.
     *
     *  @return the {@code Book} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBook());
    }


    /**
     *  Tests if this user can perform {@code Comment} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupComments() {
        return (this.session.canLookupComments());
    }


    /**
     *  A complete view of the {@code Comment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommentView() {
        this.session.useComparativeCommentView();
        return;
    }


    /**
     *  A complete view of the {@code Comment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommentView() {
        this.session.usePlenaryCommentView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include comments in books which are children
     *  of this book in the book hierarchy.
     */

    @OSID @Override
    public void useFederatedBookView() {
        this.session.useFederatedBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this book only.
     */

    @OSID @Override
    public void useIsolatedBookView() {
        this.session.useIsolatedBookView();
        return;
    }
    

    /**
     *  Only comments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCommentView() {
        this.session.useEffectiveCommentView();
        return;
    }
    

    /**
     *  All comments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCommentView() {
        this.session.useAnyEffectiveCommentView();
        return;
    }

     
    /**
     *  Gets the {@code Comment} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Comment} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Comment} and
     *  retained for compatibility.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param commentId {@code Id} of the {@code Comment}
     *  @return the comment
     *  @throws org.osid.NotFoundException {@code commentId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code commentId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Comment getComment(org.osid.id.Id commentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getComment(commentId));
    }


    /**
     *  Gets a {@code CommentList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  comments specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Comments} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Comment} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code commentIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByIds(org.osid.id.IdList commentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByIds(commentIds));
    }


    /**
     *  Gets a {@code CommentList} corresponding to the given
     *  comment genus {@code Type} which does not include
     *  comments of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @return the returned {@code Comment} list
     *  @throws org.osid.NullArgumentException
     *          {@code commentGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusType(org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByGenusType(commentGenusType));
    }


    /**
     *  Gets a {@code CommentList} corresponding to the given
     *  comment genus {@code Type} and include any additional
     *  comments with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @return the returned {@code Comment} list
     *  @throws org.osid.NullArgumentException
     *          {@code commentGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByParentGenusType(org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByParentGenusType(commentGenusType));
    }


    /**
     *  Gets a {@code CommentList} containing the given
     *  comment record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentRecordType a comment record type 
     *  @return the returned {@code Comment} list
     *  @throws org.osid.NullArgumentException
     *          {@code commentRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByRecordType(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByRecordType(commentRecordType));
    }


    /**
     *  Gets a {@code CommentList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *  
     *  In active mode, comments are returned that are currently
     *  active. In any status mode, active and inactive comments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Comment} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.commenting.CommentList getCommentsOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsOnDate(from, to));
    }
        

    /**
     *  Gets a {@code CommentList} of a given genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Comment} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code commentGenusType, from, 
     *         } or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeOnDate(org.osid.type.Type commentGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByGenusTypeOnDate(commentGenusType, from, to));
    }


    /**
     *  Gets a list of comments corresponding to a commentor
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the commentor
     *  @return the returned {@code CommentList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentor(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsForCommentor(resourceId));
    }


    /**
     *  Gets a list of comments corresponding to a commentor
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the commentor
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CommentList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorOnDate(org.osid.id.Id resourceId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsForCommentorOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of comments of the given genus type corresponding
     *  to a resource {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  commentGenusType the comment genus type 
     *  @return the returned {@code CommentList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code commentGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentor(org.osid.id.Id resourceId, 
                                                                              org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByGenusTypeForCommentor(resourceId, commentGenusType));
    }


    /**
     *  Gets a list of all comments of the given genus type
     *  corresponding to a resource {@code Id} and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  commentGenusType the comment genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code CommentList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          commentGenusType, from,} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorOnDate(org.osid.id.Id resourceId, 
                                                                                    org.osid.type.Type commentGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCommentsByGenusTypeForCommentorOnDate(resourceId, commentGenusType, from, to));
    }


    /**
     *  Gets a list of comments corresponding to a reference
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @return the returned {@code CommentList}
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsForReference(referenceId));
    }


    /**
     *  Gets a list of comments corresponding to a reference
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CommentList}
     *  @throws org.osid.NullArgumentException {@code referenceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsForReferenceOnDate(referenceId, from, to));
    }


    /**
     *  Gets a list of comments of the given genus type corresponding
     *  to a reference {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference 
     *  @param  commentGenusType the comment genus type 
     *  @return the returned {@code CommentList} 
     *  @throws org.osid.NullArgumentException {@code referenceId} or 
     *          {@code commentGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForReference(org.osid.id.Id referenceId, 
                                                                              org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByGenusTypeForReference(referenceId, commentGenusType));
    }


    /**
     *  Gets a list of all comments of the given genus type
     *  corresponding to a reference {@code Id} and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  referenceId a reference {@code Id} 
     *  @param  commentGenusType the comment genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code CommentList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code referenceId, 
     *          commentGenusType, from,} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId, 
                                                                                    org.osid.type.Type commentGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByGenusTypeForReferenceOnDate(referenceId, commentGenusType, from, to));
    }


    /**
     *  Gets a list of comments corresponding to commentor and reference
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the commentor
     *  @param  referenceId the {@code Id} of the reference
     *  @return the returned {@code CommentList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code referenceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorAndReference(org.osid.id.Id resourceId,
                                                                               org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsForCommentorAndReference(resourceId, referenceId));
    }


    /**
     *  Gets a list of comments corresponding to commentor and reference
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are currently
     *  effective. In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CommentList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code referenceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                     org.osid.id.Id referenceId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsForCommentorAndReferenceOnDate(resourceId, referenceId, from, to));
    }


    /**
     *  Gets a list of comments of the given genus type corresponding
     *  to a resource and reference {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  referenceId the {@code Id} of the reference 
     *  @param  commentGenusType the comment genus type 
     *  @return the returned {@code CommentList} 
     *  @throws org.osid.NullArgumentException {@code resourceId, referenceId 
     *         } or {@code commentGenusType} is {@code null 
     *         } 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorAndReference(org.osid.id.Id resourceId, 
                                                                                          org.osid.id.Id referenceId, 
                                                                                          org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByGenusTypeForCommentorAndReference(resourceId, referenceId, commentGenusType));
    }


    /**
     *  Gets a list of all comments corresponding to a resource and
     *  reference {@code Id} and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  referenceId a reference {@code Id} 
     *  @param  commentGenusType the comment genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code CommentList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, referenceId, 
     *          commentGenusType, from,} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorAndReferenceOnDate(org.osid.id.Id resourceId, 
                                                                                                org.osid.id.Id referenceId, 
                                                                                                org.osid.type.Type commentGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCommentsByGenusTypeForCommentorAndReferenceOnDate(resourceId, referenceId, commentGenusType, from, to));
    }


    /**
     *  Gets all {@code Comments}. 
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Comments} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getComments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getComments());
    }
}
