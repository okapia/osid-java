//
// AbstractItem.java
//
//     Defines an Item.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Item</code>.
 */

public abstract class AbstractItem
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.inventory.Item {

    private org.osid.inventory.Stock stock;
    private String propertyTag;
    private String serialNumber;
    private org.osid.locale.DisplayText locationDescription;
    private org.osid.mapping.Location location;
    private org.osid.inventory.Item parent;

    private final java.util.Collection<org.osid.inventory.records.ItemRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the stock <code> Id </code> to which this item belongs. 
     *
     *  @return a stock <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        return (this.stock.getId());
    }


    /**
     *  Gets the stock to which this item belongs. 
     *
     *  @return a stock 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        return (this.stock);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @throws org.osid.NullArgumentException
     *          <code>stock</code> is <code>null</code>
     */

    protected void setStock(org.osid.inventory.Stock stock) {
        nullarg(stock, "stock");
        this.stock = stock;
        return;
    }


    /**
     *  Gets the property identification number for this item. 
     *
     *  @return a property tag 
     */

    @OSID @Override
    public String getPropertyTag() {
        return (this.propertyTag);
    }


    /**
     *  Sets the property tag.
     *
     *  @param tag a property tag
     *  @throws org.osid.NullArgumentException <code>tag</code> is
     *          <code>null</code>
     */

    protected void setPropertyTag(String tag) {
        nullarg(tag, "property tag");
        this.propertyTag = tag;
        return;
    }


    /**
     *  Gets the serial number for this item. 
     *
     *  @return a serial number 
     */

    @OSID @Override
    public String getSerialNumber() {
        return (this.serialNumber);
    }


    /**
     *  Sets the serial number.
     *
     *  @param number a serial number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setSerialNumber(String number) {
        nullarg(number, "serial number");
        this.serialNumber = number;
        return;
    }


    /**
     *  Gets a display string for the location. 
     *
     *  @return a location descrption 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.locationDescription);
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    protected void setLocationDescription(org.osid.locale.DisplayText description) {
        nullarg(description, "location description");
        this.locationDescription = description;
        return;
    }


    /**
     *  Tests if this item has a known location. 
     *
     *  @return <code> true </code> if a location is associated with this 
     *          item, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.location != null);
    }


    /**
     *  Gets the location <code> Id </code> to which this item belongs. 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location.getId());
    }


    /**
     *  Gets the location to which this item belongs. 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location);
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    protected void setLocation(org.osid.mapping.Location location) {
        nullarg(location, "location");
        this.location = location;
        return;
    }


    /**
     *  Tests if this item is a part of another item. 
     *
     *  @return <code> true </code> if this item is a part of another item, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isPart() {
        return (this.parent != null);
    }


    /**
     *  Gets the item <code> Id </code> to which this item belongs. 
     *
     *  @return a item <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isPart() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        if (!isPart()) {
            throw new org.osid.IllegalStateException("isPart() is false");
        }

        return (this.parent.getId());
    }


    /**
     *  Gets the item to which this item belongs. 
     *
     *  @return a item 
     *  @throws org.osid.IllegalStateException <code> isPart() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem()
        throws org.osid.OperationFailedException {

        if (!isPart()) {
            throw new org.osid.IllegalStateException("isPart() is false");
        }

        return (this.parent);
    }


    /**
     *  Sets the parent item.
     *
     *  @param item the parent item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    protected void setParentItem(org.osid.inventory.Item item) {
        nullarg(item, "parent item");
        this.parent = item;
        return;
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.inventory.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Item</code> record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemRecord the item record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecord</code> or
     *          <code>itemRecordTypeitem</code> is
     *          <code>null</code>
     */
            
    protected void addItemRecord(org.osid.inventory.records.ItemRecord itemRecord, 
                                 org.osid.type.Type itemRecordType) {
        
        nullarg(itemRecord, "item record");
        addRecordType(itemRecordType);
        this.records.add(itemRecord);
        
        return;
    }
}
