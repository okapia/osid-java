//
// AbstractAuthenticationKeysProxyManager.java
//
//     An adapter for a AuthenticationKeysProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AuthenticationKeysProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAuthenticationKeysProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.authentication.keys.AuthenticationKeysProxyManager>
    implements org.osid.authentication.keys.AuthenticationKeysProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationKeysProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAuthenticationKeysProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationKeysProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAuthenticationKeysProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a key lookup service is supported. A key lookup service 
     *  defines methods to access keys. 
     *
     *  @return <code> true </code> if key lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyLookup() {
        return (getAdapteeManager().supportsKeyLookup());
    }


    /**
     *  Tests if a key search service is supported. 
     *
     *  @return <code> true </code> if key search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeySearch() {
        return (getAdapteeManager().supportsKeySearch());
    }


    /**
     *  Tests if a key administrative service is supported. 
     *
     *  @return <code> true </code> if key admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyAdmin() {
        return (getAdapteeManager().supportsKeyAdmin());
    }


    /**
     *  Tests if key notification is supported. Messages may be sent when keys 
     *  are created, modified, or deleted. 
     *
     *  @return <code> true </code> if key notification is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyNotification() {
        return (getAdapteeManager().supportsKeyNotification());
    }


    /**
     *  Tests if retrieving mappings of keys and agencies is supported. 
     *
     *  @return <code> true </code> if key agency mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyAgency() {
        return (getAdapteeManager().supportsKeyAgency());
    }


    /**
     *  Tests if managing mappings of keys and agencies is supported. 
     *
     *  @return <code> true </code> if key agency assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyAgencyAssignment() {
        return (getAdapteeManager().supportsKeyAgencyAssignment());
    }


    /**
     *  Tests if key smart agency is available. 
     *
     *  @return <code> true </code> if key smart agency is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeySmartAgency() {
        return (getAdapteeManager().supportsKeySmartAgency());
    }


    /**
     *  Gets the supported <code> Key </code> record types. 
     *
     *  @return a list containing the supported <code> Key </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getKeyRecordTypes() {
        return (getAdapteeManager().getKeyRecordTypes());
    }


    /**
     *  Tests if the given <code> Key </code> record type is supported. 
     *
     *  @param  keyRecordType a <code> Type </code> indicating a <code> Key 
     *          </code> type 
     *  @return <code> true </code> if the given key record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> keyRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsKeyRecordType(org.osid.type.Type keyRecordType) {
        return (getAdapteeManager().supportsKeyRecordType(keyRecordType));
    }


    /**
     *  Gets the supported key search record types. 
     *
     *  @return a list containing the supported <code> Key </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getKeySearchRecordTypes() {
        return (getAdapteeManager().getKeySearchRecordTypes());
    }


    /**
     *  Tests if the given key search record type is supported. 
     *
     *  @param  keySearchRecordType a <code> Type </code> indicating a <code> 
     *          Key </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> keySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsKeySearchRecordType(org.osid.type.Type keySearchRecordType) {
        return (getAdapteeManager().supportsKeySearchRecordType(keySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> KeyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyLookupSession getKeyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> a KeyLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeyLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyLookupSession getKeyLookupSessionForAgency(org.osid.id.Id agencyId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyLookupSessionForAgency(agencyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> KeySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySearchSession getKeySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getKeySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key search 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> a KeySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySearchSession getKeySearchSessionForAgency(org.osid.id.Id agencyId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getKeySearchSessionForAgency(agencyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> KeyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAdminSession getKeyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key admin 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> a KeyAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAdminSession getKeyAdminSessionForAgency(org.osid.id.Id agencyId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyAdminSessionForAgency(agencyId, proxy));
    }


    /**
     *  Gets the messaging receiver session for notifications pertaining to 
     *  key changes. 
     *
     *  @param  keyReceiver the key receiver 
     *  @param  proxy a proxy 
     *  @return an <code> KeyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          keyReceiver </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyNotificationSession getKeyNotificationSession(org.osid.authentication.keys.KeyReceiver keyReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyNotificationSession(keyReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key 
     *  notification service for the given agency. 
     *
     *  @param  keyReceiver the key receiver 
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> an KeyNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> keyReceiver, agencyId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyNotificationSession getKeyNotificationSessionForAgency(org.osid.authentication.keys.KeyReceiver keyReceiver, 
                                                                                                  org.osid.id.Id agencyId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyNotificationSessionForAgency(keyReceiver, agencyId, proxy));
    }


    /**
     *  Gets the session for retrieving key to agency mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> KeyAgencySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsKeyAgency() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAgencySession getKeyAgencySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyAgencySession(proxy));
    }


    /**
     *  Gets the session for assigning key to agency mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> KeyAgencyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyAgencyAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyAgencyAssignmentSession getKeyAgencyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getKeyAgencyAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the key smart 
     *  agency service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> KeySmartAgencySession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeySmartAgency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeySmartAgencySession getKeySmartAgencySession(org.osid.id.Id agencyId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getKeySmartAgencySession(agencyId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
