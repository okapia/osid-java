//
// AbstractIndexedMapResourceRelationshipLookupSession.java
//
//    A simple framework for providing a ResourceRelationship lookup service
//    backed by a fixed collection of resource relationships with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ResourceRelationship lookup service backed by a
 *  fixed collection of resource relationships. The resource relationships are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some resource relationships may be compatible
 *  with more types than are indicated through these resource relationship
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ResourceRelationships</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapResourceRelationshipLookupSession
    extends AbstractMapResourceRelationshipLookupSession
    implements org.osid.resource.ResourceRelationshipLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resource.ResourceRelationship> resourceRelationshipsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.ResourceRelationship>());
    private final MultiMap<org.osid.type.Type, org.osid.resource.ResourceRelationship> resourceRelationshipsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.ResourceRelationship>());


    /**
     *  Makes a <code>ResourceRelationship</code> available in this session.
     *
     *  @param  resourceRelationship a resource relationship
     *  @throws org.osid.NullArgumentException <code>resourceRelationship<code> is
     *          <code>null</code>
     */

    @Override
    protected void putResourceRelationship(org.osid.resource.ResourceRelationship resourceRelationship) {
        super.putResourceRelationship(resourceRelationship);

        this.resourceRelationshipsByGenus.put(resourceRelationship.getGenusType(), resourceRelationship);
        
        try (org.osid.type.TypeList types = resourceRelationship.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.resourceRelationshipsByRecord.put(types.getNextType(), resourceRelationship);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a resource relationship from this session.
     *
     *  @param resourceRelationshipId the <code>Id</code> of the resource relationship
     *  @throws org.osid.NullArgumentException <code>resourceRelationshipId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeResourceRelationship(org.osid.id.Id resourceRelationshipId) {
        org.osid.resource.ResourceRelationship resourceRelationship;
        try {
            resourceRelationship = getResourceRelationship(resourceRelationshipId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.resourceRelationshipsByGenus.remove(resourceRelationship.getGenusType());

        try (org.osid.type.TypeList types = resourceRelationship.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.resourceRelationshipsByRecord.remove(types.getNextType(), resourceRelationship);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeResourceRelationship(resourceRelationshipId);
        return;
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> corresponding to the given
     *  resource relationship genus <code>Type</code> which does not include
     *  resource relationships of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known resource relationships or an error results. Otherwise,
     *  the returned list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  resourceRelationshipGenusType a resource relationship genus type 
     *  @return the returned <code>ResourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusType(org.osid.type.Type resourceRelationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.resourcerelationship.ArrayResourceRelationshipList(this.resourceRelationshipsByGenus.get(resourceRelationshipGenusType)));
    }


    /**
     *  Gets a <code>ResourceRelationshipList</code> containing the given
     *  resource relationship record <code>Type</code>. In plenary mode, the
     *  returned list contains all known resource relationships or an error
     *  results. Otherwise, the returned list may contain only those
     *  resource relationships that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  resourceRelationshipRecordType a resource relationship record type 
     *  @return the returned <code>resourceRelationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByRecordType(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.resourcerelationship.ArrayResourceRelationshipList(this.resourceRelationshipsByRecord.get(resourceRelationshipRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.resourceRelationshipsByGenus.clear();
        this.resourceRelationshipsByRecord.clear();

        super.close();

        return;
    }
}
