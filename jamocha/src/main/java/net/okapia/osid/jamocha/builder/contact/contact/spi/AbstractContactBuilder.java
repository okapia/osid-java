//
// AbstractContact.java
//
//     Defines a Contact builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.contact.contact.spi;


/**
 *  Defines a <code>Contact</code> builder.
 */

public abstract class AbstractContactBuilder<T extends AbstractContactBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.contact.contact.ContactMiter contact;


    /**
     *  Constructs a new <code>AbstractContactBuilder</code>.
     *
     *  @param contact the contact to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractContactBuilder(net.okapia.osid.jamocha.builder.contact.contact.ContactMiter contact) {
        super(contact);
        this.contact = contact;
        return;
    }


    /**
     *  Builds the contact.
     *
     *  @return the new contact
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.contact.Contact build() {
        (new net.okapia.osid.jamocha.builder.validator.contact.contact.ContactValidator(getValidations())).validate(this.contact);
        return (new net.okapia.osid.jamocha.builder.contact.contact.ImmutableContact(this.contact));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the contact miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.contact.contact.ContactMiter getMiter() {
        return (this.contact);
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    public T referenceId(org.osid.id.Id referenceId) {
        getMiter().setReferenceId(referenceId);
        return (self());
    }


    /**
     *  Sets the addressee.
     *
     *  @param addressee an addressee
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>addressee</code> is <code>null</code>
     */

    public T addressee(org.osid.resource.Resource addressee) {
        getMiter().setAddressee(addressee);
        return (self());
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    public T address(org.osid.contact.Address address) {
        getMiter().setAddress(address);
        return (self());
    }


    /**
     *  Adds a Contact record.
     *
     *  @param record a contact record
     *  @param recordType the type of contact record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.contact.records.ContactRecord record, org.osid.type.Type recordType) {
        getMiter().addContactRecord(record, recordType);
        return (self());
    }
}       


