//
// AbstractPriceScheduleQuery.java
//
//     A template for making a PriceSchedule Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.priceschedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for price schedules.
 */

public abstract class AbstractPriceScheduleQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.ordering.PriceScheduleQuery {

    private final java.util.Collection<org.osid.ordering.records.PriceScheduleQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the price <code> Id </code> for this query. 
     *
     *  @param  priceId a price <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriceId(org.osid.id.Id priceId, boolean match) {
        return;
    }


    /**
     *  Clears the price <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceIdTerms() {
        return;
    }


    /**
     *  Tests if a price query is available. 
     *
     *  @return <code> true </code> if a price query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price. 
     *
     *  @return the price query 
     *  @throws org.osid.UnimplementedException <code> supportsPriceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceQuery getPriceQuery() {
        throw new org.osid.UnimplementedException("supportsPriceQuery() is false");
    }


    /**
     *  Matches price schedules with any price. 
     *
     *  @param  match <code> true </code> to match price sschedules with any 
     *          price, <code> false </code> to match price schedules with no 
     *          prices 
     */

    @OSID @Override
    public void matchAnyPrice(boolean match) {
        return;
    }


    /**
     *  Clears the price terms. 
     */

    @OSID @Override
    public void clearPriceTerms() {
        return;
    }


    /**
     *  Sets the product <code> Id </code> for this query. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        return;
    }


    /**
     *  Clears the product <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        return;
    }


    /**
     *  Tests if a product query is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Matches price schedules used with any product. 
     *
     *  @param  match <code> true </code> to match price schedules with any 
     *          product, <code> false </code> to match price schedules with no 
     *          products 
     */

    @OSID @Override
    public void matchAnyProduct(boolean match) {
        return;
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        return;
    }


    /**
     *  Sets the price <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given price schedule query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a price schedule implementing the requested record.
     *
     *  @param priceScheduleRecordType a price schedule record type
     *  @return the price schedule query record
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceScheduleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleQueryRecord getPriceScheduleQueryRecord(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceScheduleQueryRecord record : this.records) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceScheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price schedule query. 
     *
     *  @param priceScheduleQueryRecord price schedule query record
     *  @param priceScheduleRecordType priceSchedule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceScheduleQueryRecord(org.osid.ordering.records.PriceScheduleQueryRecord priceScheduleQueryRecord, 
                                          org.osid.type.Type priceScheduleRecordType) {

        addRecordType(priceScheduleRecordType);
        nullarg(priceScheduleQueryRecord, "price schedule query record");
        this.records.add(priceScheduleQueryRecord);        
        return;
    }
}
