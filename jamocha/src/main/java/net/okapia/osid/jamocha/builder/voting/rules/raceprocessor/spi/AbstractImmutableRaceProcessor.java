//
// AbstractImmutableRaceProcessor.java
//
//     Wraps a mutable RaceProcessor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.rules.raceprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>RaceProcessor</code> to hide modifiers. This
 *  wrapper provides an immutized RaceProcessor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying raceProcessor whose state changes are visible.
 */

public abstract class AbstractImmutableRaceProcessor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidProcessor
    implements org.osid.voting.rules.RaceProcessor {

    private final org.osid.voting.rules.RaceProcessor raceProcessor;


    /**
     *  Constructs a new <code>AbstractImmutableRaceProcessor</code>.
     *
     *  @param raceProcessor the race processor to immutablize
     *  @throws org.osid.NullArgumentException <code>raceProcessor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRaceProcessor(org.osid.voting.rules.RaceProcessor raceProcessor) {
        super(raceProcessor);
        this.raceProcessor = raceProcessor;
        return;
    }


    /**
     *  Tests if there is a limit on the number of winners in a race. 
     *
     *  @return <code> true </code> if a limit on the number of winners, 
     *          <code> false </code> if no limit exists 
     */

    @OSID @Override
    public boolean hasMaximumWinners() {
        return (this.raceProcessor.hasMaximumWinners());
    }


    /**
     *  Gets the number of maximum winners in a race. 
     *
     *  @return the maximum number of winners permitted 
     *  @throws org.osid.IllegalStateException <code> hasMaximumWinners() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaximumWinners() {
        return (this.raceProcessor.getMaximumWinners());
    }


    /**
     *  Gets the minimum percentage (0-100) of total votes to be declared a 
     *  winner in a race. 
     *
     *  @return the minimum percentage 
     */

    @OSID @Override
    public long getMinimumPercentageToWin() {
        return (this.raceProcessor.getMinimumPercentageToWin());
    }


    /**
     *  Gets the minimum votesl votes to be declared a winner in a race. 
     *
     *  @return the minimum votes 
     */

    @OSID @Override
    public long getMinimumVotesToWin() {
        return (this.raceProcessor.getMinimumVotesToWin());
    }


    /**
     *  Gets the race processor record corresponding to the given <code> 
     *  RaceProcessor </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  raceProcessorRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(raceProcessorRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  raceProcessorRecordType the type of race processor record to 
     *          retrieve 
     *  @return the race processor record 
     *  @throws org.osid.NullArgumentException <code> raceProcessorRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(raceProcessorRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorRecord getRaceProcessorRecord(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException {

        return (this.raceProcessor.getRaceProcessorRecord(raceProcessorRecordType));
    }
}

