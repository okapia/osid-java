//
// AbstractAdapterControllerLookupSession.java
//
//    A Controller lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Controller lookup session adapter.
 */

public abstract class AbstractAdapterControllerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.ControllerLookupSession {

    private final org.osid.control.ControllerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterControllerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterControllerLookupSession(org.osid.control.ControllerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code Controller} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupControllers() {
        return (this.session.canLookupControllers());
    }


    /**
     *  A complete view of the {@code Controller} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeControllerView() {
        this.session.useComparativeControllerView();
        return;
    }


    /**
     *  A complete view of the {@code Controller} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryControllerView() {
        this.session.usePlenaryControllerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include controllers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    

    /**
     *  Only active controllers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveControllerView() {
        this.session.useActiveControllerView();
        return;
    }


    /**
     *  Active and inactive controllers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusControllerView() {
        this.session.useAnyStatusControllerView();
        return;
    }
    
     
    /**
     *  Gets the {@code Controller} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Controller} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Controller} and
     *  retained for compatibility.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param controllerId {@code Id} of the {@code Controller}
     *  @return the controller
     *  @throws org.osid.NotFoundException {@code controllerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code controllerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Controller getController(org.osid.id.Id controllerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getController(controllerId));
    }


    /**
     *  Gets a {@code ControllerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  controllers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Controllers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  controllerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Controller} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code controllerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByIds(org.osid.id.IdList controllerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getControllersByIds(controllerIds));
    }


    /**
     *  Gets a {@code ControllerList} corresponding to the given
     *  controller genus {@code Type} which does not include
     *  controllers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  controllerGenusType a controller genus type 
     *  @return the returned {@code Controller} list
     *  @throws org.osid.NullArgumentException
     *          {@code controllerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByGenusType(org.osid.type.Type controllerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getControllersByGenusType(controllerGenusType));
    }


    /**
     *  Gets a {@code ControllerList} corresponding to the given
     *  controller genus {@code Type} and include any additional
     *  controllers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  controllerGenusType a controller genus type 
     *  @return the returned {@code Controller} list
     *  @throws org.osid.NullArgumentException
     *          {@code controllerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByParentGenusType(org.osid.type.Type controllerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getControllersByParentGenusType(controllerGenusType));
    }


    /**
     *  Gets a {@code ControllerList} containing the given
     *  controller record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  controllerRecordType a controller record type 
     *  @return the returned {@code Controller} list
     *  @throws org.osid.NullArgumentException
     *          {@code controllerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByRecordType(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getControllersByRecordType(controllerRecordType));
    }


    /**
     *  Gets a {@code ControllerList} with the given address.
     *  
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session.
     *  
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  address a controller address 
     *  @return the returned {@code Controller} list 
     *  @throws org.osid.NullArgumentException {@code address} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByAddress(String address)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getControllersByAddress(address));
    }


    /**
     *  Gets all {@code Controllers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @return a list of {@code Controllers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getControllers());
    }
}
