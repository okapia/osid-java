//
// AbstractIndexedMapProductLookupSession.java
//
//    A simple framework for providing a Product lookup service
//    backed by a fixed collection of products with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Product lookup service backed by a
 *  fixed collection of products. The products are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some products may be compatible
 *  with more types than are indicated through these product
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Products</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProductLookupSession
    extends AbstractMapProductLookupSession
    implements org.osid.ordering.ProductLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ordering.Product> productsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.Product>());
    private final MultiMap<org.osid.type.Type, org.osid.ordering.Product> productsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.Product>());


    /**
     *  Makes a <code>Product</code> available in this session.
     *
     *  @param  product a product
     *  @throws org.osid.NullArgumentException <code>product<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProduct(org.osid.ordering.Product product) {
        super.putProduct(product);

        this.productsByGenus.put(product.getGenusType(), product);
        
        try (org.osid.type.TypeList types = product.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.productsByRecord.put(types.getNextType(), product);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a product from this session.
     *
     *  @param productId the <code>Id</code> of the product
     *  @throws org.osid.NullArgumentException <code>productId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProduct(org.osid.id.Id productId) {
        org.osid.ordering.Product product;
        try {
            product = getProduct(productId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.productsByGenus.remove(product.getGenusType());

        try (org.osid.type.TypeList types = product.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.productsByRecord.remove(types.getNextType(), product);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProduct(productId);
        return;
    }


    /**
     *  Gets a <code>ProductList</code> corresponding to the given
     *  product genus <code>Type</code> which does not include
     *  products of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known products or an error results. Otherwise,
     *  the returned list may contain only those products that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  productGenusType a product genus type 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByGenusType(org.osid.type.Type productGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.product.ArrayProductList(this.productsByGenus.get(productGenusType)));
    }


    /**
     *  Gets a <code>ProductList</code> containing the given
     *  product record <code>Type</code>. In plenary mode, the
     *  returned list contains all known products or an error
     *  results. Otherwise, the returned list may contain only those
     *  products that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  productRecordType a product record type 
     *  @return the returned <code>product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByRecordType(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.product.ArrayProductList(this.productsByRecord.get(productRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.productsByGenus.clear();
        this.productsByRecord.clear();

        super.close();

        return;
    }
}
