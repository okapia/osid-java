//
// OsidEnablerElements.java
//
//     Id definitions for OsidForm and OsidQuery fields.
//
//
// Tom Coppeto
// Okapia
// 15 May 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;


/**
 *  Ids for OsidForm and OsidQuery elements. These are used in
 *  Metadata and may also be used as a key for relational mapping.
 */

public class OsidEnablerElements
    extends OsidRuleElements {


    /**
     *  Gets the effective element Id.
     *
     *  @return the effective element Id
     */

    public static org.osid.id.Id getEffective() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getEffective());
    }


    /**
     *  Gets the start date element Id.
     *
     *  @return the start date element Id
     */

    public static org.osid.id.Id getStartDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getStartDate());
    }


    /**
     *  Gets the end date element Id.
     *
     *  @return the end date element Id
     */

    public static org.osid.id.Id getEndDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getEndDate());
    }


    /**
     *  Gets the date element Id.
     *
     *  @return the date element Id
     */

    public static org.osid.id.Id getDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getDate());
    }


    /**
     *  Gets the schedule Id element Id.
     *
     *  @return the schedule Id element Id
     */

    public static org.osid.id.Id getScheduleId() {
        return (makeElementId("osid.OsidSchedule.ScheduleId"));
    }


    /**
     *  Gets the schedule element Id.
     *
     *  @return the schedule element Id
     */

    public static org.osid.id.Id getSchedule() {
        return (makeElementId("osid.OsidSchedule.Schedule"));
    }


    /**
     *  Gets the event Id element Id.
     *
     *  @return the event Id element Id
     */

    public static org.osid.id.Id getEventId() {
        return (makeElementId("osid.OsidEvent.EventId"));
    }


    /**
     *  Gets the event element Id.
     *
     *  @return the event element Id
     */

    public static org.osid.id.Id getEvent() {
        return (makeElementId("osid.OsidEvent.Event"));
    }


    /**
     *  Gets the cyclic event Id element Id.
     *
     *  @return the cyclic event Id element Id
     */

    public static org.osid.id.Id getCyclicEventId() {
        return (makeElementId("osid.OsidCyclicEvent.CyclicEventId"));
    }


    /**
     *  Gets the cyclic event element Id.
     *
     *  @return the cyclic event element Id
     */

    public static org.osid.id.Id getCyclicEvent() {
        return (makeElementId("osid.OsidCyclicEvent.CyclicEvent"));
    }


    /**
     *  Gets the demographic Id element Id.
     *
     *  @return the demographic Id element Id
     */

    public static org.osid.id.Id getDemographicId() {
        return (makeElementId("osid.OsidDemographic.DemographicId"));
    }


    /**
     *  Gets the demographic element Id.
     *
     *  @return the demographic element Id
     */

    public static org.osid.id.Id getDemographic() {
        return (makeElementId("osid.OsidDemographic.Demographic"));
    }
}
