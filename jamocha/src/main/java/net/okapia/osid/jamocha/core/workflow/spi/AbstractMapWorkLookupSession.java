//
// AbstractMapWorkLookupSession
//
//    A simple framework for providing a Work lookup service
//    backed by a fixed collection of works.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Work lookup service backed by a
 *  fixed collection of works. The works are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Works</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapWorkLookupSession
    extends net.okapia.osid.jamocha.workflow.spi.AbstractWorkLookupSession
    implements org.osid.workflow.WorkLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.workflow.Work> works = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.workflow.Work>());


    /**
     *  Makes a <code>Work</code> available in this session.
     *
     *  @param  work a work
     *  @throws org.osid.NullArgumentException <code>work<code>
     *          is <code>null</code>
     */

    protected void putWork(org.osid.workflow.Work work) {
        this.works.put(work.getId(), work);
        return;
    }


    /**
     *  Makes an array of works available in this session.
     *
     *  @param  works an array of works
     *  @throws org.osid.NullArgumentException <code>works<code>
     *          is <code>null</code>
     */

    protected void putWorks(org.osid.workflow.Work[] works) {
        putWorks(java.util.Arrays.asList(works));
        return;
    }


    /**
     *  Makes a collection of works available in this session.
     *
     *  @param  works a collection of works
     *  @throws org.osid.NullArgumentException <code>works<code>
     *          is <code>null</code>
     */

    protected void putWorks(java.util.Collection<? extends org.osid.workflow.Work> works) {
        for (org.osid.workflow.Work work : works) {
            this.works.put(work.getId(), work);
        }

        return;
    }


    /**
     *  Removes a Work from this session.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @throws org.osid.NullArgumentException <code>workId<code> is
     *          <code>null</code>
     */

    protected void removeWork(org.osid.id.Id workId) {
        this.works.remove(workId);
        return;
    }


    /**
     *  Gets the <code>Work</code> specified by its <code>Id</code>.
     *
     *  @param  workId <code>Id</code> of the <code>Work</code>
     *  @return the work
     *  @throws org.osid.NotFoundException <code>workId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>workId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Work getWork(org.osid.id.Id workId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.workflow.Work work = this.works.get(workId);
        if (work == null) {
            throw new org.osid.NotFoundException("work not found: " + workId);
        }

        return (work);
    }


    /**
     *  Gets all <code>Works</code>. In plenary mode, the returned
     *  list contains all known works or an error
     *  results. Otherwise, the returned list may contain only those
     *  works that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Works</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.work.ArrayWorkList(this.works.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.works.clear();
        super.close();
        return;
    }
}
