//
// InvariantMapProxyRoomLookupSession
//
//    Implements a Room lookup service backed by a fixed
//    collection of rooms. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room;


/**
 *  Implements a Room lookup service backed by a fixed
 *  collection of rooms. The rooms are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyRoomLookupSession
    extends net.okapia.osid.jamocha.core.room.spi.AbstractMapRoomLookupSession
    implements org.osid.room.RoomLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRoomLookupSession} with no
     *  rooms.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                  org.osid.proxy.Proxy proxy) {
        setCampus(campus);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyRoomLookupSession} with a single
     *  room.
     *
     *  @param campus the campus
     *  @param room a single room
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code room} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.Room room, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putRoom(room);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRoomLookupSession} using
     *  an array of rooms.
     *
     *  @param campus the campus
     *  @param rooms an array of rooms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code rooms} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.Room[] rooms, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putRooms(rooms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRoomLookupSession} using a
     *  collection of rooms.
     *
     *  @param campus the campus
     *  @param rooms a collection of rooms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code rooms} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRoomLookupSession(org.osid.room.Campus campus,
                                                  java.util.Collection<? extends org.osid.room.Room> rooms,
                                                  org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putRooms(rooms);
        return;
    }
}
