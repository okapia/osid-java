//
// AbstractOsidCompendiumBuilder.java
//
//     Defines a builder for an OSID Compendium.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Defines the OsidCompendium builder.
 */

public abstract class AbstractOsidCompendiumBuilder<T extends AbstractOsidCompendiumBuilder<T>>
    extends AbstractOsidObjectBuilder<T> {

    private final OsidCompendiumMiter compendium;


    /**
     *  Creates a new <code>AbstractOsidCompendiumBuilder</code>.
     *
     *  @param compendium an compendium miter interface
     *  @throws org.osid.NullArgumentException <code>compendium</code> is
     *          <code>null</code>
     */

    protected AbstractOsidCompendiumBuilder(OsidCompendiumMiter compendium) {
        super(compendium);
        this.compendium = compendium;
        return;
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T startDate(org.osid.calendaring.DateTime date) {
        this.compendium.setStartDate(date);
        return (self());
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T endDate(org.osid.calendaring.DateTime date) {
        this.compendium.setEndDate(date);
        return (self());
    }


    /**
     *  Sets the interpolated flag.
     *
     *  @param interpolated {@code true} is interpolated, {@code
     *         false} otherwise
     */

    public T interpolated(boolean interpolated) {
        this.compendium.setInterpolated(interpolated);
        return (self());
    }


    /**
     *  Sets the extrapolated flag.
     *
     *  @param extrapolated {@code true} is extrapolated, {@code
     *         false} otherwise
     */

    public T extrapolated(boolean extrapolated) {
        this.compendium.setExtrapolated(extrapolated);
        return (self());
    }
}
