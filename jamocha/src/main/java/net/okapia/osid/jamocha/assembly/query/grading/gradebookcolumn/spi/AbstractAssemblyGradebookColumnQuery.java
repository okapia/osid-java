//
// AbstractAssemblyGradebookColumnQuery.java
//
//     A GradebookColumnQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradebookColumnQuery that stores terms.
 */

public abstract class AbstractAssemblyGradebookColumnQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.grading.GradebookColumnQuery,
               org.osid.grading.GradebookColumnQueryInspector,
               org.osid.grading.GradebookColumnSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookColumnQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookColumnSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyGradebookColumnQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyGradebookColumnQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getGradeSystemIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeSystemIdTerms() {
        getAssembler().clearTerms(getGradeSystemIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (getAssembler().getIdTerms(getGradeSystemIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the grade system. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradeSystem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeSystemColumn(), style);
        return;
    }


    /**
     *  Gets the GradeSystemId column name.
     *
     * @return the column name
     */

    protected String getGradeSystemIdColumn() {
        return ("grade_system_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available for querying 
     *  grade systems. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradeSystemQuery() {
        throw new org.osid.UnimplementedException("supportsGradeSystemQuery() is false");
    }


    /**
     *  Matches gradebook columns with any grade system assigned. 
     *
     *  @param  match <code> true </code> to match columns with any grade 
     *          system, <code> false </code> to match columns with no grade 
     *          system 
     */

    @OSID @Override
    public void matchAnyGradeSystem(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeSystemColumn(), match);
        return;
    }


    /**
     *  Clears the grade system terms. 
     */

    @OSID @Override
    public void clearGradeSystemTerms() {
        getAssembler().clearTerms(getGradeSystemColumn());
        return;
    }


    /**
     *  Gets the grade system terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a <code> GradeSystemSearchOrder </code> is available for 
     *  grade systems. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a gradebook column summary search order. 
     *
     *  @return the gradebook column summary search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSummarySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummarySearchOrder getGradeSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSystemSearchOrder() is false");
    }


    /**
     *  Gets the GradeSystem column name.
     *
     * @return the column name
     */

    protected String getGradeSystemColumn() {
        return ("grade_system");
    }


    /**
     *  Sets the grade entry <code> Id </code> for this query. 
     *
     *  @param  gradeEntryId a grade entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeEntryId(org.osid.id.Id gradeEntryId, boolean match) {
        getAssembler().addIdTerm(getGradeEntryIdColumn(), gradeEntryId, match);
        return;
    }


    /**
     *  Clears the grade entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeEntryIdTerms() {
        getAssembler().clearTerms(getGradeEntryIdColumn());
        return;
    }


    /**
     *  Gets the grade entry <code> Id </code> terms. 
     *
     *  @return the grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeEntryIdTerms() {
        return (getAssembler().getIdTerms(getGradeEntryIdColumn()));
    }


    /**
     *  Gets the GradeEntryId column name.
     *
     * @return the column name
     */

    protected String getGradeEntryIdColumn() {
        return ("grade_entry_id");
    }


    /**
     *  Tests if a <code> GradeEntryQuery </code> is available for querying 
     *  grade entries. 
     *
     *  @return <code> true </code> if a grade entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuery getGradeEntryQuery() {
        throw new org.osid.UnimplementedException("supportsGradeEntryQuery() is false");
    }


    /**
     *  Matches gradebook columns with any grade entry assigned. 
     *
     *  @param  match <code> true </code> to match columns with any grade 
     *          entry, <code> false </code> to match columns with no grade 
     *          entries 
     */

    @OSID @Override
    public void matchAnyGradeEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeEntryColumn(), match);
        return;
    }


    /**
     *  Clears the grade entry terms. 
     */

    @OSID @Override
    public void clearGradeEntryTerms() {
        getAssembler().clearTerms(getGradeEntryColumn());
        return;
    }


    /**
     *  Gets the grade entry terms. 
     *
     *  @return the grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the GradeEntry column name.
     *
     * @return the column name
     */

    protected String getGradeEntryColumn() {
        return ("grade_entry");
    }


    /**
     *  Tests if a <code> GradebookColumnSummaryQuery </code> is available for 
     *  querying grade systems. 
     *
     *  @return <code> true </code> if a gradebook column summary query 
     *          interface is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSummaryQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a gradebook column summary. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the gradebook column summary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSummaryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummaryQuery getGradebookColumnSummaryQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnSummaryQuery() is false");
    }


    /**
     *  Clears the gradebook column summary terms. 
     */

    @OSID @Override
    public void clearGradebookColumnSummaryTerms() {
        getAssembler().clearTerms(getGradebookColumnSummaryColumn());
        return;
    }


    /**
     *  Gets the gradebook column summary terms. 
     *
     *  @return the gradebook column summary terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummaryQueryInspector[] getGradebookColumnSummaryTerms() {
        return (new org.osid.grading.GradebookColumnSummaryQueryInspector[0]);
    }


    /**
     *  Gets the search order for a grade system. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGradebookColumnSummarySearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnSummarySearchOrder() is false");
    }


    /**
     *  Tests if a <code> GradebookColumnSummarySearchOrder </code> is 
     *  available for gradebook column summaries. 
     *
     *  @return <code> true </code> if a gradebook column summary search order 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSummarySearchOrder() {
        return (false);
    }


    /**
     *  Gets the GradebookColumnSummary column name.
     *
     * @return the column name
     */

    protected String getGradebookColumnSummaryColumn() {
        return ("gradebook_column_summary");
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        getAssembler().addIdTerm(getGradebookIdColumn(), gradebookId, match);
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        getAssembler().clearTerms(getGradebookIdColumn());
        return;
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (getAssembler().getIdTerms(getGradebookIdColumn()));
    }


    /**
     *  Gets the GradebookId column name.
     *
     * @return the column name
     */

    protected String getGradebookIdColumn() {
        return ("gradebook_id");
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available for querying 
     *  grade systems. 
     *
     *  @return <code> true </code> if a gradebook query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a gradebook. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        getAssembler().clearTerms(getGradebookColumn());
        return;
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the Gradebook column name.
     *
     * @return the column name
     */

    protected String getGradebookColumn() {
        return ("gradebook");
    }


    /**
     *  Tests if this gradebookColumn supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradebookColumnRecordType a gradebook column record type 
     *  @return <code>true</code> if the gradebookColumnRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookColumnRecordType) {
        for (org.osid.grading.records.GradebookColumnQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  gradebookColumnRecordType the gradebook column record type 
     *  @return the gradebook column query record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnQueryRecord getGradebookColumnQueryRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  gradebookColumnRecordType the gradebook column record type 
     *  @return the gradebook column query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnQueryInspectorRecord getGradebookColumnQueryInspectorRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param gradebookColumnRecordType the gradebook column record type
     *  @return the gradebook column search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSearchOrderRecord getGradebookColumnSearchOrderRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this gradebook column. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookColumnQueryRecord the gradebook column query record
     *  @param gradebookColumnQueryInspectorRecord the gradebook column query inspector
     *         record
     *  @param gradebookColumnSearchOrderRecord the gradebook column search order record
     *  @param gradebookColumnRecordType gradebook column record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnQueryRecord</code>,
     *          <code>gradebookColumnQueryInspectorRecord</code>,
     *          <code>gradebookColumnSearchOrderRecord</code> or
     *          <code>gradebookColumnRecordTypegradebookColumn</code> is
     *          <code>null</code>
     */
            
    protected void addGradebookColumnRecords(org.osid.grading.records.GradebookColumnQueryRecord gradebookColumnQueryRecord, 
                                      org.osid.grading.records.GradebookColumnQueryInspectorRecord gradebookColumnQueryInspectorRecord, 
                                      org.osid.grading.records.GradebookColumnSearchOrderRecord gradebookColumnSearchOrderRecord, 
                                      org.osid.type.Type gradebookColumnRecordType) {

        addRecordType(gradebookColumnRecordType);

        nullarg(gradebookColumnQueryRecord, "gradebook column query record");
        nullarg(gradebookColumnQueryInspectorRecord, "gradebook column query inspector record");
        nullarg(gradebookColumnSearchOrderRecord, "gradebook column search odrer record");

        this.queryRecords.add(gradebookColumnQueryRecord);
        this.queryInspectorRecords.add(gradebookColumnQueryInspectorRecord);
        this.searchOrderRecords.add(gradebookColumnSearchOrderRecord);
        
        return;
    }
}
