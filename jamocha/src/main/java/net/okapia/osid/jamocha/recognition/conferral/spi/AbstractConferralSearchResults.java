//
// AbstractConferralSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractConferralSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recognition.ConferralSearchResults {

    private org.osid.recognition.ConferralList conferrals;
    private final org.osid.recognition.ConferralQueryInspector inspector;
    private final java.util.Collection<org.osid.recognition.records.ConferralSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractConferralSearchResults.
     *
     *  @param conferrals the result set
     *  @param conferralQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>conferrals</code>
     *          or <code>conferralQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractConferralSearchResults(org.osid.recognition.ConferralList conferrals,
                                            org.osid.recognition.ConferralQueryInspector conferralQueryInspector) {
        nullarg(conferrals, "conferrals");
        nullarg(conferralQueryInspector, "conferral query inspectpr");

        this.conferrals = conferrals;
        this.inspector = conferralQueryInspector;

        return;
    }


    /**
     *  Gets the conferral list resulting from a search.
     *
     *  @return a conferral list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferrals() {
        if (this.conferrals == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recognition.ConferralList conferrals = this.conferrals;
        this.conferrals = null;
	return (conferrals);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recognition.ConferralQueryInspector getConferralQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  conferral search record <code> Type. </code> This method must
     *  be used to retrieve a conferral implementing the requested
     *  record.
     *
     *  @param conferralSearchRecordType a conferral search 
     *         record type 
     *  @return the conferral search
     *  @throws org.osid.NullArgumentException
     *          <code>conferralSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(conferralSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralSearchResultsRecord getConferralSearchResultsRecord(org.osid.type.Type conferralSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recognition.records.ConferralSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(conferralSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(conferralSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record conferral search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addConferralRecord(org.osid.recognition.records.ConferralSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "conferral record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
