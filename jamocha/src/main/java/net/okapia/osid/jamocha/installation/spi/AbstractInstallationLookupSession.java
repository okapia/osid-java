//
// AbstractInstallationLookupSession.java
//
//    A starter implementation framework for providing an Installation
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Installation
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getInstallations(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractInstallationLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.installation.InstallationLookupSession {

    private boolean pedantic  = false;
    private boolean normalizedVersion = false;
    private org.osid.installation.Site site = new net.okapia.osid.jamocha.nil.installation.site.UnknownSite();
    

    /**
     *  Gets the <code>Site/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Site Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSiteId() {
        return (this.site.getId());
    }


    /**
     *  Gets the <code>Site</code> associated with this 
     *  session.
     *
     *  @return the <code>Site</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Site getSite()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.site);
    }


    /**
     *  Sets the <code>Site</code>.
     *
     *  @param  site the site for this session
     *  @throws org.osid.NullArgumentException <code>site</code>
     *          is <code>null</code>
     */

    protected void setSite(org.osid.installation.Site site) {
        nullarg(site, "site");
        this.site = site;
        return;
    }


    /**
     *  Tests if this user can perform <code>Installation</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInstallations() {
        return (true);
    }


    /**
     *  A complete view of the <code>Installation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInstallationView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Installation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInstallationView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a plenary or comparative view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  The returns from the lookup methods may omit multiple versions
     *  of the same installation.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.normalizedVersion = true;
        return;
    }


    /**
     *  All versions of the same installation are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.normalizedVersion = false;
        return;
    }


    /**
     *  Tests if a normalized or denormalized version view is set.
     *
     *  @return <code>true</code> if normalized</code>,
     *          <code>false</code> if denormalized
     */

    protected boolean isNormalizedVersion() {
        return (this.normalizedVersion);
    }

     
    /**
     *  Gets the <code>Installation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Installation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Installation</code> and
     *  retained for compatibility.
     *
     *  @param  installationId <code>Id</code> of the
     *          <code>Installation</code>
     *  @return the installation
     *  @throws org.osid.NotFoundException <code>installationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>installationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Installation getInstallation(org.osid.id.Id installationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.installation.InstallationList installations = getInstallations()) {
            while (installations.hasNext()) {
                org.osid.installation.Installation installation = installations.getNextInstallation();
                if (installation.getId().equals(installationId)) {
                    return (installation);
                }
            }
        } 

        throw new org.osid.NotFoundException(installationId + " not found");
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  installations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Installations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInstallations()</code>.
     *
     *  @param  installationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>installationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByIds(org.osid.id.IdList installationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.installation.Installation> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = installationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getInstallation(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("installation " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.installation.installation.LinkedInstallationList(ret));
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the
     *  given installation genus <code>Type</code> which does not
     *  include installations of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned
     *  list may contain only those installations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInstallations()</code>.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.installation.installation.InstallationGenusFilterList(getInstallations(), installationGenusType));
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  installation genus <code>Type</code> and include any additional
     *  installations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned
     *  list may contain only those installations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInstallations()</code>.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByParentGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getInstallationsByGenusType(installationGenusType));
    }


    /**
     *  Gets an <code>InstallationList</code> containing the given
     *  installation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInstallations()</code>.
     *
     *  @param  installationRecordType an installation record type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByRecordType(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.installation.installation.InstallationRecordFilterList(getInstallations(), installationRecordType));
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the
     *  given <code>Package</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  installations for the specified package, in the order of the
     *  list, including duplicates, or an error results if an <code>
     *  Id </code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Installations
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  @param  packageId <code>Id</code> of a <code>Package</code> 
     *  @return the returned <code>Installation</code> list 
     *  @throws org.osid.NullArgumentException <code>packageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByPackage(org.osid.id.Id packageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.installation.installation.InstallationFilterList(new PackageFilter(packageId), getInstallations()));
    }        

    
    /**
     *  Gets all <code>Installations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Installations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.installation.InstallationList getInstallations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the installation list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of installations
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.installation.InstallationList filterInstallationsOnViews(org.osid.installation.InstallationList list)
        throws org.osid.OperationFailedException {

        return (list);
    }

    
    public static class PackageFilter
        implements net.okapia.osid.jamocha.inline.filter.installation.installation.InstallationFilter {
         
        private final org.osid.id.Id packageId;
         
         
        /**
         *  Constructs a new <code>PackageFilter</code>.
         *
         *  @param packageId the package to filter
         *  @throws org.osid.NullArgumentException
         *          <code>packageId</code> is <code>null</code>
         */
        
        public PackageFilter(org.osid.id.Id packageId) {
            nullarg(packageId, "package Id");
            this.packageId = packageId;
            return;
        }

         
        /**
         *  Used by the InstallationFilterList to filter the installation list
         *  based on installation.
         *
         *  @param installation the installation
         *  @return <code>true</code> to pass the installation,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.installation.Installation installation) {
            return (installation.getPackageId().equals(this.packageId));
        }
    }    
}
