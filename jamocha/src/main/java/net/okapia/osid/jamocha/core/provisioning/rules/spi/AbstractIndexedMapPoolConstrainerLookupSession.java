//
// AbstractIndexedMapPoolConstrainerLookupSession.java
//
//    A simple framework for providing a PoolConstrainer lookup service
//    backed by a fixed collection of pool constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a PoolConstrainer lookup service backed by a
 *  fixed collection of pool constrainers. The pool constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some pool constrainers may be compatible
 *  with more types than are indicated through these pool constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPoolConstrainerLookupSession
    extends AbstractMapPoolConstrainerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolConstrainer> poolConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolConstrainer> poolConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolConstrainer>());


    /**
     *  Makes a <code>PoolConstrainer</code> available in this session.
     *
     *  @param  poolConstrainer a pool constrainer
     *  @throws org.osid.NullArgumentException <code>poolConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPoolConstrainer(org.osid.provisioning.rules.PoolConstrainer poolConstrainer) {
        super.putPoolConstrainer(poolConstrainer);

        this.poolConstrainersByGenus.put(poolConstrainer.getGenusType(), poolConstrainer);
        
        try (org.osid.type.TypeList types = poolConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolConstrainersByRecord.put(types.getNextType(), poolConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a pool constrainer from this session.
     *
     *  @param poolConstrainerId the <code>Id</code> of the pool constrainer
     *  @throws org.osid.NullArgumentException <code>poolConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePoolConstrainer(org.osid.id.Id poolConstrainerId) {
        org.osid.provisioning.rules.PoolConstrainer poolConstrainer;
        try {
            poolConstrainer = getPoolConstrainer(poolConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.poolConstrainersByGenus.remove(poolConstrainer.getGenusType());

        try (org.osid.type.TypeList types = poolConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolConstrainersByRecord.remove(types.getNextType(), poolConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePoolConstrainer(poolConstrainerId);
        return;
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the given
     *  pool constrainer genus <code>Type</code> which does not include
     *  pool constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known pool constrainers or an error results. Otherwise,
     *  the returned list may contain only those pool constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  poolConstrainerGenusType a pool constrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.ArrayPoolConstrainerList(this.poolConstrainersByGenus.get(poolConstrainerGenusType)));
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> containing the given
     *  pool constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known pool constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  pool constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  poolConstrainerRecordType a pool constrainer record type 
     *  @return the returned <code>poolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByRecordType(org.osid.type.Type poolConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.ArrayPoolConstrainerList(this.poolConstrainersByRecord.get(poolConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolConstrainersByGenus.clear();
        this.poolConstrainersByRecord.clear();

        super.close();

        return;
    }
}
