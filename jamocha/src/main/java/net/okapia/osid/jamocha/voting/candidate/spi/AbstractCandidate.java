//
// AbstractCandidate.java
//
//     Defines a Candidate.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.candidate.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Candidate</code>.
 */

public abstract class AbstractCandidate
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.voting.Candidate {

    private org.osid.voting.Race race;
    private org.osid.resource.Resource resource;

    private final java.util.Collection<org.osid.voting.records.CandidateRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the race <code> Id </code> of the candidate. 
     *
     *  @return the candidate <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRaceId() {
        return (this.race.getId());
    }


    /**
     *  Gets the race of the candidate. 
     *
     *  @return the race 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Race getRace()
        throws org.osid.OperationFailedException {

        return (this.race);
    }


    /**
     *  Sets the race.
     *
     *  @param race a race
     *  @throws org.osid.NullArgumentException
     *          <code>race</code> is <code>null</code>
     */

    protected void setRace(org.osid.voting.Race race) {
        nullarg(race, "race");
        this.race = race;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> related to the candidate. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource related to the candidate. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this candidate supports the given record
     *  <code>Type</code>.
     *
     *  @param  candidateRecordType a candidate record type 
     *  @return <code>true</code> if the candidateRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type candidateRecordType) {
        for (org.osid.voting.records.CandidateRecord record : this.records) {
            if (record.implementsRecordType(candidateRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Candidate</code> record <code>Type</code>.
     *
     *  @param  candidateRecordType the candidate record type 
     *  @return the candidate record 
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(candidateRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.CandidateRecord getCandidateRecord(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.CandidateRecord record : this.records) {
            if (record.implementsRecordType(candidateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(candidateRecordType + " is not supported");
    }


    /**
     *  Adds a record to this candidate. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param candidateRecord the candidate record
     *  @param candidateRecordType candidate record type
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecord</code> or
     *          <code>candidateRecordTypecandidate</code> is
     *          <code>null</code>
     */
            
    protected void addCandidateRecord(org.osid.voting.records.CandidateRecord candidateRecord, 
                                      org.osid.type.Type candidateRecordType) {

        nullarg(candidateRecord, "candidate record");
        addRecordType(candidateRecordType);
        this.records.add(candidateRecord);
        
        return;
    }
}
