//
// AbstractAssemblyPathQuery.java
//
//     A PathQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.path.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PathQuery that stores terms.
 */

public abstract class AbstractAssemblyPathQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.mapping.path.PathQuery,
               org.osid.mapping.path.PathQueryInspector,
               org.osid.mapping.path.PathSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.records.PathQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.PathQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.PathSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPathQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPathQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches paths containing the specified <code> Coordinate. </code> 
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        getAssembler().addCoordinateTerm(getCoordinateColumn(), coordinate, match);
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        getAssembler().clearTerms(getCoordinateColumn());
        return;
    }


    /**
     *  Gets the coordinate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (getAssembler().getCoordinateTerms(getCoordinateColumn()));
    }


    /**
     *  Gets the Coordinate column name.
     *
     * @return the column name
     */

    protected String getCoordinateColumn() {
        return ("coordinate");
    }


    /**
     *  Matches paths overlapping with the specified <code> SpatialUnit. 
     *  </code> 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOverlappingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                            boolean match) {
        getAssembler().addSpatialUnitTerm(getOverlappingSpatialUnitColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Matches paths that have any spatial unit assignment. 
     *
     *  @param  match <code> true </code> to match paths with any spatial 
     *          dimension, <code> false </code> to match paths with no spatial 
     *          dimensions 
     */

    @OSID @Override
    public void matchAnyOverlappingSpatialUnit(boolean match) {
        getAssembler().addSpatialUnitWildcardTerm(getOverlappingSpatialUnitColumn(), match);
        return;
    }


    /**
     *  Clears the spatial unit query terms. 
     */

    @OSID @Override
    public void clearOverlappingSpatialUnitTerms() {
        getAssembler().clearTerms(getOverlappingSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getOverlappingSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getOverlappingSpatialUnitColumn()));
    }


    /**
     *  Gets the OverlappingSpatialUnit column name.
     *
     * @return the column name
     */

    protected String getOverlappingSpatialUnitColumn() {
        return ("overlapping_spatial_unit");
    }


    /**
     *  Sets the location <code> Ids </code> for this query to match paths 
     *  including all the given locations. 
     *
     *  @param  locationIds the location <code> Ids </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationIds </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAlongLocationIds(org.osid.id.Id[] locationIds, boolean match) {
        getAssembler().addIdSetTerm(getAlongLocationIdsColumn(), locationIds, match);
        return;
    }


    /**
     *  Clears the along location <code> Ids </code> query terms. 
     */

    @OSID @Override
    public void clearAlongLocationIdsTerms() {
        getAssembler().clearTerms(getAlongLocationIdsColumn());
        return;
    }


    /**
     *  Gets the along location <code> Ids </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdSetTerm[] getAlongLocationIdsTerms() {
        return (getAssembler().getIdSetTerms(getAlongLocationIdsColumn()));
    }


    /**
     *  Gets the Along Location Ids column name.
     *
     *  @return the column name
     */

    protected String getAlongLocationIdsColumn() {
        return ("along_location_ids");
    }


    /**
     *  Sets the path <code> Id </code> for this query to match paths 
     *  intersecting with another path, 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchIntersectingPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getIntersectingPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the intersecting path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathIdTerms() {
        getAssembler().clearTerms(getIntersectingPathIdColumn());
        return;
    }


    /**
     *  Gets the intersecting path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIntersectingPathIdTerms() {
        return (getAssembler().getIdTerms(getIntersectingPathIdColumn()));
    }


    /**
     *  Gets the IntersectingPathId column name.
     *
     * @return the column name
     */

    protected String getIntersectingPathIdColumn() {
        return ("intersecting_path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available for intersecting 
     *  paths, 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectingPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for an intersecting path, Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectingPathQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getIntersectingPathQuery() {
        throw new org.osid.UnimplementedException("supportsIntersectingPathQuery() is false");
    }


    /**
     *  Matches paths with any intersecting path, 
     *
     *  @param  match <code> true </code> to match paths with any intersecting 
     *          path, <code> false </code> to match paths with no intersecting 
     *          path 
     */

    @OSID @Override
    public void matchAnyIntersectingPath(boolean match) {
        getAssembler().addIdWildcardTerm(getIntersectingPathColumn(), match);
        return;
    }


    /**
     *  Clears the intersecting path query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathTerms() {
        getAssembler().clearTerms(getIntersectingPathColumn());
        return;
    }


    /**
     *  Gets the intersecting path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getIntersectingPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the IntersectingPath column name.
     *
     * @return the column name
     */

    protected String getIntersectingPathColumn() {
        return ("intersecting_path");
    }


    /**
     *  Sets the location <code> Id </code> for this query to match paths that 
     *  pass through locations. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches routes that go through any location. 
     *
     *  @param  match <code> true </code> to match routes with any location, 
     *          <code> false </code> to match routes with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location query terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Sets the route <code> Id </code> for this query to match paths used in 
     *  a route. 
     *
     *  @param  routeId a route <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> routeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id routeId, boolean match) {
        getAssembler().addIdTerm(getRouteIdColumn(), routeId, match);
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        getAssembler().clearTerms(getRouteIdColumn());
        return;
    }


    /**
     *  Gets the route <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRouteIdTerms() {
        return (getAssembler().getIdTerms(getRouteIdColumn()));
    }


    /**
     *  Gets the RouteId column name.
     *
     * @return the column name
     */

    protected String getRouteIdColumn() {
        return ("route_id");
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Matches paths that are used in any route. 
     *
     *  @param  match <code> true </code> to match paths in any route, <code> 
     *          false </code> to match paths used in no route 
     */

    @OSID @Override
    public void matchAnyRoute(boolean match) {
        getAssembler().addIdWildcardTerm(getRouteColumn(), match);
        return;
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        getAssembler().clearTerms(getRouteColumn());
        return;
    }


    /**
     *  Gets the route query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQueryInspector[] getRouteTerms() {
        return (new org.osid.mapping.route.RouteQueryInspector[0]);
    }


    /**
     *  Gets the Route column name.
     *
     * @return the column name
     */

    protected String getRouteColumn() {
        return ("route");
    }


    /**
     *  Sets the map <code> Id </code> for this query to match paths assigned 
     *  to maps. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this path supports the given record
     *  <code>Type</code>.
     *
     *  @param  pathRecordType a path record type 
     *  @return <code>true</code> if the pathRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pathRecordType) {
        for (org.osid.mapping.path.records.PathQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  pathRecordType the path record type 
     *  @return the path query record 
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.PathQueryRecord getPathQueryRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.PathQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  pathRecordType the path record type 
     *  @return the path query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.PathQueryInspectorRecord getPathQueryInspectorRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.PathQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param pathRecordType the path record type
     *  @return the path search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.PathSearchOrderRecord getPathSearchOrderRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.PathSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this path. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pathQueryRecord the path query record
     *  @param pathQueryInspectorRecord the path query inspector
     *         record
     *  @param pathSearchOrderRecord the path search order record
     *  @param pathRecordType path record type
     *  @throws org.osid.NullArgumentException
     *          <code>pathQueryRecord</code>,
     *          <code>pathQueryInspectorRecord</code>,
     *          <code>pathSearchOrderRecord</code> or
     *          <code>pathRecordTypepath</code> is
     *          <code>null</code>
     */
            
    protected void addPathRecords(org.osid.mapping.path.records.PathQueryRecord pathQueryRecord, 
                                      org.osid.mapping.path.records.PathQueryInspectorRecord pathQueryInspectorRecord, 
                                      org.osid.mapping.path.records.PathSearchOrderRecord pathSearchOrderRecord, 
                                      org.osid.type.Type pathRecordType) {

        addRecordType(pathRecordType);

        nullarg(pathQueryRecord, "path query record");
        nullarg(pathQueryInspectorRecord, "path query inspector record");
        nullarg(pathSearchOrderRecord, "path search odrer record");

        this.queryRecords.add(pathQueryRecord);
        this.queryInspectorRecords.add(pathQueryInspectorRecord);
        this.searchOrderRecords.add(pathSearchOrderRecord);
        
        return;
    }
}
