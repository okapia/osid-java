//
// AbstractIndexedMapGraphLookupSession.java
//
//    A simple framework for providing a Graph lookup service
//    backed by a fixed collection of graphs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Graph lookup service backed by a
 *  fixed collection of graphs. The graphs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some graphs may be compatible
 *  with more types than are indicated through these graph
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Graphs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapGraphLookupSession
    extends AbstractMapGraphLookupSession
    implements org.osid.topology.GraphLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.topology.Graph> graphsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.Graph>());
    private final MultiMap<org.osid.type.Type, org.osid.topology.Graph> graphsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.Graph>());


    /**
     *  Makes a <code>Graph</code> available in this session.
     *
     *  @param  graph a graph
     *  @throws org.osid.NullArgumentException <code>graph<code> is
     *          <code>null</code>
     */

    @Override
    protected void putGraph(org.osid.topology.Graph graph) {
        super.putGraph(graph);

        this.graphsByGenus.put(graph.getGenusType(), graph);
        
        try (org.osid.type.TypeList types = graph.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.graphsByRecord.put(types.getNextType(), graph);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a graph from this session.
     *
     *  @param graphId the <code>Id</code> of the graph
     *  @throws org.osid.NullArgumentException <code>graphId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeGraph(org.osid.id.Id graphId) {
        org.osid.topology.Graph graph;
        try {
            graph = getGraph(graphId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.graphsByGenus.remove(graph.getGenusType());

        try (org.osid.type.TypeList types = graph.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.graphsByRecord.remove(types.getNextType(), graph);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeGraph(graphId);
        return;
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  graph genus <code>Type</code> which does not include
     *  graphs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known graphs or an error results. Otherwise,
     *  the returned list may contain only those graphs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.graph.ArrayGraphList(this.graphsByGenus.get(graphGenusType)));
    }


    /**
     *  Gets a <code>GraphList</code> containing the given
     *  graph record <code>Type</code>. In plenary mode, the
     *  returned list contains all known graphs or an error
     *  results. Otherwise, the returned list may contain only those
     *  graphs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  graphRecordType a graph record type 
     *  @return the returned <code>graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByRecordType(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.graph.ArrayGraphList(this.graphsByRecord.get(graphRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.graphsByGenus.clear();
        this.graphsByRecord.clear();

        super.close();

        return;
    }
}
