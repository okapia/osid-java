//
// PayerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payer.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PayerElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the PayerElement Id.
     *
     *  @return the payer element Id
     */

    public static org.osid.id.Id getPayerEntityId() {
        return (makeEntityId("osid.billing.payment.Payer"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.billing.payment.payer.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.billing.payment.payer.Resource"));
    }


    /**
     *  Gets the CustomerId element Id.
     *
     *  @return the CustomerId element Id
     */

    public static org.osid.id.Id getCustomerId() {
        return (makeElementId("osid.billing.payment.payer.CustomerId"));
    }


    /**
     *  Gets the Customer element Id.
     *
     *  @return the Customer element Id
     */

    public static org.osid.id.Id getCustomer() {
        return (makeElementId("osid.billing.payment.payer.Customer"));
    }


    /**
     *  Gets the CreditCardNumber element Id.
     *
     *  @return the CreditCardNumber element Id
     */

    public static org.osid.id.Id getCreditCardNumber() {
        return (makeElementId("osid.billing.payment.payer.CreditCardNumber"));
    }


    /**
     *  Gets the CreditCardExpiration element Id.
     *
     *  @return the CreditCardExpiration element Id
     */

    public static org.osid.id.Id getCreditCardExpiration() {
        return (makeElementId("osid.billing.payment.payer.CreditCardExpiration"));
    }


    /**
     *  Gets the CreditCardCode element Id.
     *
     *  @return the CreditCardCode element Id
     */

    public static org.osid.id.Id getCreditCardCode() {
        return (makeElementId("osid.billing.payment.payer.CreditCardCode"));
    }


    /**
     *  Gets the BankRoutingNumber element Id.
     *
     *  @return the BankRoutingNumber element Id
     */

    public static org.osid.id.Id getBankRoutingNumber() {
        return (makeElementId("osid.billing.payment.payer.BankRoutingNumber"));
    }


    /**
     *  Gets the BankAccountNumber element Id.
     *
     *  @return the BankAccountNumber element Id
     */

    public static org.osid.id.Id getBankAccountNumber() {
        return (makeElementId("osid.billing.payment.payer.BankAccountNumber"));
    }


    /**
     *  Gets the UsesActivity element Id.
     *
     *  @return the UsesActivity element Id
     */

    public static org.osid.id.Id getUsesActivity() {
        return (makeElementId("osid.billing.payment.payer.UsesActivity"));
    }


    /**
     *  Gets the UsesCash element Id.
     *
     *  @return the UsesCash element Id
     */

    public static org.osid.id.Id getUsesCash() {
        return (makeElementId("osid.billing.payment.payer.UsesCash"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.billing.payment.payer.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.billing.payment.payer.Business"));
    }
}
