//
// MutableMapVoteLookupSession
//
//    Implements a Vote lookup service backed by a collection of
//    votes that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Vote lookup service backed by a collection of
 *  votes. The votes are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of votes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapVoteLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapVoteLookupSession
    implements org.osid.voting.VoteLookupSession {


    /**
     *  Constructs a new {@code MutableMapVoteLookupSession}
     *  with no votes.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls} is
     *          {@code null}
     */

      public MutableMapVoteLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapVoteLookupSession} with a
     *  single vote.
     *
     *  @param polls the polls  
     *  @param vote a vote
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code vote} is {@code null}
     */

    public MutableMapVoteLookupSession(org.osid.voting.Polls polls,
                                           org.osid.voting.Vote vote) {
        this(polls);
        putVote(vote);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapVoteLookupSession}
     *  using an array of votes.
     *
     *  @param polls the polls
     *  @param votes an array of votes
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code votes} is {@code null}
     */

    public MutableMapVoteLookupSession(org.osid.voting.Polls polls,
                                           org.osid.voting.Vote[] votes) {
        this(polls);
        putVotes(votes);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapVoteLookupSession}
     *  using a collection of votes.
     *
     *  @param polls the polls
     *  @param votes a collection of votes
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code votes} is {@code null}
     */

    public MutableMapVoteLookupSession(org.osid.voting.Polls polls,
                                           java.util.Collection<? extends org.osid.voting.Vote> votes) {

        this(polls);
        putVotes(votes);
        return;
    }

    
    /**
     *  Makes a {@code Vote} available in this session.
     *
     *  @param vote a vote
     *  @throws org.osid.NullArgumentException {@code vote{@code  is
     *          {@code null}
     */

    @Override
    public void putVote(org.osid.voting.Vote vote) {
        super.putVote(vote);
        return;
    }


    /**
     *  Makes an array of votes available in this session.
     *
     *  @param votes an array of votes
     *  @throws org.osid.NullArgumentException {@code votes{@code 
     *          is {@code null}
     */

    @Override
    public void putVotes(org.osid.voting.Vote[] votes) {
        super.putVotes(votes);
        return;
    }


    /**
     *  Makes collection of votes available in this session.
     *
     *  @param votes a collection of votes
     *  @throws org.osid.NullArgumentException {@code votes{@code  is
     *          {@code null}
     */

    @Override
    public void putVotes(java.util.Collection<? extends org.osid.voting.Vote> votes) {
        super.putVotes(votes);
        return;
    }


    /**
     *  Removes a Vote from this session.
     *
     *  @param voteId the {@code Id} of the vote
     *  @throws org.osid.NullArgumentException {@code voteId{@code 
     *          is {@code null}
     */

    @Override
    public void removeVote(org.osid.id.Id voteId) {
        super.removeVote(voteId);
        return;
    }    
}
