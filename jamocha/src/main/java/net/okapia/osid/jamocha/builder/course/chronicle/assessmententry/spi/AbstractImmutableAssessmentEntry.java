//
// AbstractImmutableAssessmentEntry.java
//
//     Wraps a mutable AssessmentEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.assessmententry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AssessmentEntry</code> to hide modifiers. This
 *  wrapper provides an immutized AssessmentEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assessmentEntry whose state changes are visible.
 */

public abstract class AbstractImmutableAssessmentEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.chronicle.AssessmentEntry {

    private final org.osid.course.chronicle.AssessmentEntry assessmentEntry;


    /**
     *  Constructs a new <code>AbstractImmutableAssessmentEntry</code>.
     *
     *  @param assessmentEntry the assessment entry to immutablize
     *  @throws org.osid.NullArgumentException <code>assessmentEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssessmentEntry(org.osid.course.chronicle.AssessmentEntry assessmentEntry) {
        super(assessmentEntry);
        this.assessmentEntry = assessmentEntry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.assessmentEntry.getStudentId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.assessmentEntry.getStudent());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Assessment. </code> 
     *
     *  @return the assessment <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessmentEntry.getAssessmentId());
    }


    /**
     *  Gets the <code> Assessment. </code> 
     *
     *  @return the assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessmentEntry.getAssessment());
    }


    /**
     *  Gets the completion date. 
     *
     *  @return the date completion 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateCompleted() {
        return (this.assessmentEntry.getDateCompleted());
    }


    /**
     *  Tests if this assessment applies to a program. If <code> hasCourse() 
     *  </code> is <code> true </code> then <code> hasProgram() </code> must 
     *  be <code> false. </code> 
     *
     *  @return <code> true </code> if a program is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgram() {
        return (this.assessmentEntry.hasProgram());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.assessmentEntry.getProgramId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.assessmentEntry.getProgram());
    }


    /**
     *  Tests if this assessment applies to a specific course. If <code> 
     *  hasCourse() </code> is <code> true </code> then <code> hasProgram() 
     *  </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if a course is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCourse() {
        return (this.assessmentEntry.hasCourse());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Course. </code> 
     *
     *  @return the course <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.assessmentEntry.getCourseId());
    }


    /**
     *  Gets the <code> Course. </code> 
     *
     *  @return the course 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.assessmentEntry.getCourse());
    }


    /**
     *  Tests if a grade is available. 
     *
     *  @return <code> true </code> if a grade is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasGrade() {
        return (this.assessmentEntry.hasGrade());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Grade. </code> 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasGrade() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        return (this.assessmentEntry.getGradeId());
    }


    /**
     *  Gets the <code> Grade. </code> 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasGrade() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        return (this.assessmentEntry.getGrade());
    }


    /**
     *  Tests if a score is available. 
     *
     *  @return <code> true </code> if a score is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasScore() {
        return (this.assessmentEntry.hasScore());
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreScaleId() {
        return (this.assessmentEntry.getScoreScaleId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreScale()
        throws org.osid.OperationFailedException {

        return (this.assessmentEntry.getScoreScale());
    }


    /**
     *  Gets the cumulative score. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        return (this.assessmentEntry.getScore());
    }


    /**
     *  Gets the assessment entry record corresponding to the given <code> 
     *  AssessmentEntry </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> assessmentEntryRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(assessmentEntryRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  assessmentEntryRecordType the type of assessment entry record 
     *          to retrieve 
     *  @return the assessment entry record 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentEntryRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assessmentEntryRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AssessmentEntryRecord getAssessmentEntryRecord(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.assessmentEntry.getAssessmentEntryRecord(assessmentEntryRecordType));
    }
}

