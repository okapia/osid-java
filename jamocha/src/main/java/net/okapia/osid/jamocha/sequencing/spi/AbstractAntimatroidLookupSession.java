//
// AbstractAntimatroidLookupSession.java
//
//    A starter implementation framework for providing an Antimatroid
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an Antimatroid
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAntimatroids(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAntimatroidLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.sequencing.AntimatroidLookupSession {

    private boolean pedantic      = false;
    private org.osid.sequencing.Antimatroid antimatroid = new net.okapia.osid.jamocha.nil.sequencing.antimatroid.UnknownAntimatroid();
    


    /**
     *  Tests if this user can perform <code>Antimatroid</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAntimatroids() {
        return (true);
    }


    /**
     *  A complete view of the <code>Antimatroid</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAntimatroidView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Antimatroid</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAntimatroidView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Antimatroid</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Antimatroid</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Antimatroid</code> and
     *  retained for compatibility.
     *
     *  @param  antimatroidId <code>Id</code> of the
     *          <code>Antimatroid</code>
     *  @return the antimatroid
     *  @throws org.osid.NotFoundException <code>antimatroidId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>antimatroidId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Antimatroid getAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.sequencing.AntimatroidList antimatroids = getAntimatroids()) {
            while (antimatroids.hasNext()) {
                org.osid.sequencing.Antimatroid antimatroid = antimatroids.getNextAntimatroid();
                if (antimatroid.getId().equals(antimatroidId)) {
                    return (antimatroid);
                }
            }
        } 

        throw new org.osid.NotFoundException(antimatroidId + " not found");
    }


    /**
     *  Gets an <code>AntimatroidList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  antimatroids specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Antimatroids</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAntimatroids()</code>.
     *
     *  @param  antimatroidIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Antimatroid</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByIds(org.osid.id.IdList antimatroidIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.sequencing.Antimatroid> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = antimatroidIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAntimatroid(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("antimatroid " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.sequencing.antimatroid.LinkedAntimatroidList(ret));
    }


    /**
     *  Gets an <code>AntimatroidList</code> corresponding to the given
     *  antimatroid genus <code>Type</code> which does not include
     *  antimatroids of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAntimatroids()</code>.
     *
     *  @param  antimatroidGenusType an antimatroid genus type 
     *  @return the returned <code>Antimatroid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByGenusType(org.osid.type.Type antimatroidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.sequencing.antimatroid.AntimatroidGenusFilterList(getAntimatroids(), antimatroidGenusType));
    }


    /**
     *  Gets an <code>AntimatroidList</code> corresponding to the given
     *  antimatroid genus <code>Type</code> and include any additional
     *  antimatroids with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAntimatroids()</code>.
     *
     *  @param  antimatroidGenusType an antimatroid genus type 
     *  @return the returned <code>Antimatroid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByParentGenusType(org.osid.type.Type antimatroidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAntimatroidsByGenusType(antimatroidGenusType));
    }


    /**
     *  Gets an <code>AntimatroidList</code> containing the given
     *  antimatroid record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAntimatroids()</code>.
     *
     *  @param  antimatroidRecordType an antimatroid record type 
     *  @return the returned <code>Antimatroid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByRecordType(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.sequencing.antimatroid.AntimatroidRecordFilterList(getAntimatroids(), antimatroidRecordType));
    }


    /**
     *  Gets an <code>AntimatroidList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known antimatroids or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  antimatroids that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Antimatroid</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.sequencing.antimatroid.AntimatroidProviderFilterList(getAntimatroids(), resourceId));
    }


    /**
     *  Gets all <code>Antimatroids</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  antimatroids or an error results. Otherwise, the returned list
     *  may contain only those antimatroids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Antimatroids</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.sequencing.AntimatroidList getAntimatroids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the antimatroid list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of antimatroids
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.sequencing.AntimatroidList filterAntimatroidsOnViews(org.osid.sequencing.AntimatroidList list)
        throws org.osid.OperationFailedException {

        org.osid.sequencing.AntimatroidList ret = list;

        return (ret);
    }
}
