//
// AbstractRouteSearch.java
//
//     A template for making a Route Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.route.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing route searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRouteSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.mapping.route.RouteSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.mapping.route.records.RouteSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.mapping.route.RouteSearchOrder routeSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of routes. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  routeIds list of routes
     *  @throws org.osid.NullArgumentException
     *          <code>routeIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRoutes(org.osid.id.IdList routeIds) {
        while (routeIds.hasNext()) {
            try {
                this.ids.add(routeIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRoutes</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of route Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRouteIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  routeSearchOrder route search order 
     *  @throws org.osid.NullArgumentException
     *          <code>routeSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>routeSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRouteResults(org.osid.mapping.route.RouteSearchOrder routeSearchOrder) {
	this.routeSearchOrder = routeSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.mapping.route.RouteSearchOrder getRouteSearchOrder() {
	return (this.routeSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given route search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a route implementing the requested record.
     *
     *  @param routeSearchRecordType a route search record
     *         type
     *  @return the route search record
     *  @throws org.osid.NullArgumentException
     *          <code>routeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSearchRecord getRouteSearchRecord(org.osid.type.Type routeSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.mapping.route.records.RouteSearchRecord record : this.records) {
            if (record.implementsRecordType(routeSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this route search. 
     *
     *  @param routeSearchRecord route search record
     *  @param routeSearchRecordType route search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRouteSearchRecord(org.osid.mapping.route.records.RouteSearchRecord routeSearchRecord, 
                                           org.osid.type.Type routeSearchRecordType) {

        addRecordType(routeSearchRecordType);
        this.records.add(routeSearchRecord);        
        return;
    }
}
