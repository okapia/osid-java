//
// AbstractReceiptLookupSession.java
//
//    A starter implementation framework for providing a Receipt
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Receipt
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getReceipts(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractReceiptLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.messaging.ReceiptLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.messaging.Mailbox mailbox = new net.okapia.osid.jamocha.nil.messaging.mailbox.UnknownMailbox();
    

    /**
     *  Gets the <code>Mailbox/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Mailbox Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.mailbox.getId());
    }


    /**
     *  Gets the <code>Mailbox</code> associated with this 
     *  session.
     *
     *  @return the <code>Mailbox</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.mailbox);
    }


    /**
     *  Sets the <code>Mailbox</code>.
     *
     *  @param  mailbox the mailbox for this session
     *  @throws org.osid.NullArgumentException <code>mailbox</code>
     *          is <code>null</code>
     */

    protected void setMailbox(org.osid.messaging.Mailbox mailbox) {
        nullarg(mailbox, "mailbox");
        this.mailbox = mailbox;
        return;
    }


    /**
     *  Tests if this user can perform <code>Receipt</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupReceipts() {
        return (true);
    }


    /**
     *  A complete view of the <code>Receipt</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeReceiptView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Receipt</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryReceiptView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include receipts in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Gets the <code>Receipt</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Receipt</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Receipt</code> and retained for
     *  compatibility.
     *
     *  @param  receiptId <code>Id</code> of the
     *          <code>Receipt</code>
     *  @return the receipt
     *  @throws org.osid.NotFoundException <code>receiptId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>receiptId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Receipt getReceipt(org.osid.id.Id receiptId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.messaging.ReceiptList receipts = getReceipts()) {
            while (receipts.hasNext()) {
                org.osid.messaging.Receipt receipt = receipts.getNextReceipt();
                if (receipt.getId().equals(receiptId)) {
                    return (receipt);
                }
            }
        } 

        throw new org.osid.NotFoundException(receiptId + " not found");
    }


    /**
     *  Gets a <code>ReceiptList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  receipts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Receipts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getReceipts()</code>.
     *
     *  @param  receiptIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>receiptIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByIds(org.osid.id.IdList receiptIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.messaging.Receipt> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = receiptIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getReceipt(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("receipt " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.messaging.receipt.LinkedReceiptList(ret));
    }


    /**
     *  Gets a <code>ReceiptList</code> corresponding to the given
     *  receipt genus <code>Type</code> which does not include
     *  receipts of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known receipts
     *  or an error results. Otherwise, the returned list may contain
     *  only those receipts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getReceipts()</code>.
     *
     *  @param  receiptGenusType a receipt genus type 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByGenusType(org.osid.type.Type receiptGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.receipt.ReceiptGenusFilterList(getReceipts(), receiptGenusType));
    }


    /**
     *  Gets a <code>ReceiptList</code> corresponding to the given
     *  receipt genus <code>Type</code> and include any additional
     *  receipts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known receipts
     *  or an error results. Otherwise, the returned list may contain
     *  only those receipts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getReceipts()</code>.
     *
     *  @param  receiptGenusType a receipt genus type 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByParentGenusType(org.osid.type.Type receiptGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getReceiptsByGenusType(receiptGenusType));
    }


    /**
     *  Gets a <code>ReceiptList</code> containing the given receipt
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known receipts
     *  or an error results. Otherwise, the returned list may contain
     *  only those receipts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getReceipts()</code>.
     *
     *  @param  receiptRecordType a receipt record type 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByRecordType(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.receipt.ReceiptRecordFilterList(getReceipts(), receiptRecordType));
    }


    /**
     *  Gets a <code>ReceiptList</code> for the given message.  In
     *  plenary mode, the returned list contains all known receipts or
     *  an error results. Otherwise, the returned list may contain
     *  only those receipts that are accessible through this session.
     *
     *  @param  messageId a message <code>Id</code> 
     *  @return the returned <code>Receipt</code> list 
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForMessage(org.osid.id.Id messageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.messaging.receipt.ReceiptFilterList(new MessageFilter(messageId), getReceipts()));
    }        


    /**
     *  Gets a <code>ReceiptList</code> received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known receipts or an error results. Otherwise, the returned
     *  list may contain only those receipts that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id 
     *  @return the returned <code>Receipt</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.receipt.ReceiptFilterList(new RecipientFilter(resourceId), getReceipts()));
    }


    /**
     *  Gets a list of <code>Receipts</code> for the given message and
     *  recipient. In plenary mode, the returned list contains all
     *  known receipts or an error results. Otherwise, the returned
     *  list may contain only those receipts that are accessible
     *  through this session.
     *
     *  @param  messageId a message <code>Id</code> 
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Receipt</code> 
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          or <code>recipientId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForMessageAndRecipient(org.osid.id.Id messageId, 
                                                                            org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.receipt.ReceiptFilterList(new RecipientFilter(resourceId), getReceiptsForMessage(messageId)));
    }

    
    /**
     *  Gets all <code>Receipts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Receipts</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.messaging.ReceiptList getReceipts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    public static class MessageFilter
        implements net.okapia.osid.jamocha.inline.filter.messaging.receipt.ReceiptFilter {

        private final org.osid.id.Id messageId;

        
        /**
         *  Constructs a new <code>MessageFilterList</code>.
         *
         *  @param messageId the message to filter
         *  @throws org.osid.NullArgumentException
         *          <code>messageId</code> is <code>null</code>
         */

        public MessageFilter(org.osid.id.Id messageId) {
            nullarg(messageId, "message Id");
            this.messageId = messageId;
            return;
        }


        /**
         *  Used by the ReceiptFilterList to filter the receipt list
         *  based on message.
         *
         *  @param receipt the receipt
         *  @return <code>true</code> to pass the receipt,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.messaging.Receipt receipt) {
            return (receipt.getMessageId().equals(this.messageId));
        }
    }


    public static class RecipientFilter
        implements net.okapia.osid.jamocha.inline.filter.messaging.receipt.ReceiptFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>RecipientFilterList</code>.
         *
         *  @param resourceId the recipient to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public RecipientFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "recipient Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the ReceiptFilterList to filter the receipt list
         *  based on recipient.
         *
         *  @param receipt the receipt
         *  @return <code>true</code> to pass the receipt,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.messaging.Receipt receipt) {
            return (receipt.getRecipientId().equals(this.resourceId));
        }
    }
}
