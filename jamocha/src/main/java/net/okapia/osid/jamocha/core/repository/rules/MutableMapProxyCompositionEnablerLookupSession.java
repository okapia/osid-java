//
// MutableMapProxyCompositionEnablerLookupSession
//
//    Implements a CompositionEnabler lookup service backed by a collection of
//    compositionEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.rules;


/**
 *  Implements a CompositionEnabler lookup service backed by a collection of
 *  compositionEnablers. The compositionEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of composition enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCompositionEnablerLookupSession
    extends net.okapia.osid.jamocha.core.repository.rules.spi.AbstractMapCompositionEnablerLookupSession
    implements org.osid.repository.rules.CompositionEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyCompositionEnablerLookupSession}
     *  with no composition enablers.
     *
     *  @param repository the repository
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.proxy.Proxy proxy) {
        setRepository(repository);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCompositionEnablerLookupSession} with a
     *  single composition enabler.
     *
     *  @param repository the repository
     *  @param compositionEnabler a composition enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code compositionEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                                org.osid.repository.rules.CompositionEnabler compositionEnabler, org.osid.proxy.Proxy proxy) {
        this(repository, proxy);
        putCompositionEnabler(compositionEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCompositionEnablerLookupSession} using an
     *  array of composition enablers.
     *
     *  @param repository the repository
     *  @param compositionEnablers an array of composition enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code compositionEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                                org.osid.repository.rules.CompositionEnabler[] compositionEnablers, org.osid.proxy.Proxy proxy) {
        this(repository, proxy);
        putCompositionEnablers(compositionEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCompositionEnablerLookupSession} using a
     *  collection of composition enablers.
     *
     *  @param repository the repository
     *  @param compositionEnablers a collection of composition enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code compositionEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                                java.util.Collection<? extends org.osid.repository.rules.CompositionEnabler> compositionEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(repository, proxy);
        setSessionProxy(proxy);
        putCompositionEnablers(compositionEnablers);
        return;
    }

    
    /**
     *  Makes a {@code CompositionEnabler} available in this session.
     *
     *  @param compositionEnabler an composition enabler
     *  @throws org.osid.NullArgumentException {@code compositionEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putCompositionEnabler(org.osid.repository.rules.CompositionEnabler compositionEnabler) {
        super.putCompositionEnabler(compositionEnabler);
        return;
    }


    /**
     *  Makes an array of compositionEnablers available in this session.
     *
     *  @param compositionEnablers an array of composition enablers
     *  @throws org.osid.NullArgumentException {@code compositionEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putCompositionEnablers(org.osid.repository.rules.CompositionEnabler[] compositionEnablers) {
        super.putCompositionEnablers(compositionEnablers);
        return;
    }


    /**
     *  Makes collection of composition enablers available in this session.
     *
     *  @param compositionEnablers
     *  @throws org.osid.NullArgumentException {@code compositionEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putCompositionEnablers(java.util.Collection<? extends org.osid.repository.rules.CompositionEnabler> compositionEnablers) {
        super.putCompositionEnablers(compositionEnablers);
        return;
    }


    /**
     *  Removes a CompositionEnabler from this session.
     *
     *  @param compositionEnablerId the {@code Id} of the composition enabler
     *  @throws org.osid.NullArgumentException {@code compositionEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCompositionEnabler(org.osid.id.Id compositionEnablerId) {
        super.removeCompositionEnabler(compositionEnablerId);
        return;
    }    
}
