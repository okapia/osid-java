//
// AbstractMapCommissionLookupSession
//
//    A simple framework for providing a Commission lookup service
//    backed by a fixed collection of commissions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Commission lookup service backed by a
 *  fixed collection of commissions. The commissions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Commissions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCommissionLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractCommissionLookupSession
    implements org.osid.resourcing.CommissionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.Commission> commissions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.Commission>());


    /**
     *  Makes a <code>Commission</code> available in this session.
     *
     *  @param  commission a commission
     *  @throws org.osid.NullArgumentException <code>commission<code>
     *          is <code>null</code>
     */

    protected void putCommission(org.osid.resourcing.Commission commission) {
        this.commissions.put(commission.getId(), commission);
        return;
    }


    /**
     *  Makes an array of commissions available in this session.
     *
     *  @param  commissions an array of commissions
     *  @throws org.osid.NullArgumentException <code>commissions<code>
     *          is <code>null</code>
     */

    protected void putCommissions(org.osid.resourcing.Commission[] commissions) {
        putCommissions(java.util.Arrays.asList(commissions));
        return;
    }


    /**
     *  Makes a collection of commissions available in this session.
     *
     *  @param  commissions a collection of commissions
     *  @throws org.osid.NullArgumentException <code>commissions<code>
     *          is <code>null</code>
     */

    protected void putCommissions(java.util.Collection<? extends org.osid.resourcing.Commission> commissions) {
        for (org.osid.resourcing.Commission commission : commissions) {
            this.commissions.put(commission.getId(), commission);
        }

        return;
    }


    /**
     *  Removes a Commission from this session.
     *
     *  @param  commissionId the <code>Id</code> of the commission
     *  @throws org.osid.NullArgumentException <code>commissionId<code> is
     *          <code>null</code>
     */

    protected void removeCommission(org.osid.id.Id commissionId) {
        this.commissions.remove(commissionId);
        return;
    }


    /**
     *  Gets the <code>Commission</code> specified by its <code>Id</code>.
     *
     *  @param  commissionId <code>Id</code> of the <code>Commission</code>
     *  @return the commission
     *  @throws org.osid.NotFoundException <code>commissionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>commissionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Commission getCommission(org.osid.id.Id commissionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.Commission commission = this.commissions.get(commissionId);
        if (commission == null) {
            throw new org.osid.NotFoundException("commission not found: " + commissionId);
        }

        return (commission);
    }


    /**
     *  Gets all <code>Commissions</code>. In plenary mode, the returned
     *  list contains all known commissions or an error
     *  results. Otherwise, the returned list may contain only those
     *  commissions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Commissions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.commission.ArrayCommissionList(this.commissions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commissions.clear();
        super.close();
        return;
    }
}
