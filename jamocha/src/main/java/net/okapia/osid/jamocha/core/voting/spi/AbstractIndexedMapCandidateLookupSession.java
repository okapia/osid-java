//
// AbstractIndexedMapCandidateLookupSession.java
//
//    A simple framework for providing a Candidate lookup service
//    backed by a fixed collection of candidates with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Candidate lookup service backed by a
 *  fixed collection of candidates. The candidates are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some candidates may be compatible
 *  with more types than are indicated through these candidate
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Candidates</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCandidateLookupSession
    extends AbstractMapCandidateLookupSession
    implements org.osid.voting.CandidateLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.Candidate> candidatesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Candidate>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.Candidate> candidatesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Candidate>());


    /**
     *  Makes a <code>Candidate</code> available in this session.
     *
     *  @param  candidate a candidate
     *  @throws org.osid.NullArgumentException <code>candidate<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCandidate(org.osid.voting.Candidate candidate) {
        super.putCandidate(candidate);

        this.candidatesByGenus.put(candidate.getGenusType(), candidate);
        
        try (org.osid.type.TypeList types = candidate.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.candidatesByRecord.put(types.getNextType(), candidate);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a candidate from this session.
     *
     *  @param candidateId the <code>Id</code> of the candidate
     *  @throws org.osid.NullArgumentException <code>candidateId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCandidate(org.osid.id.Id candidateId) {
        org.osid.voting.Candidate candidate;
        try {
            candidate = getCandidate(candidateId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.candidatesByGenus.remove(candidate.getGenusType());

        try (org.osid.type.TypeList types = candidate.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.candidatesByRecord.remove(types.getNextType(), candidate);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCandidate(candidateId);
        return;
    }


    /**
     *  Gets a <code>CandidateList</code> corresponding to the given
     *  candidate genus <code>Type</code> which does not include
     *  candidates of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known candidates or an error results. Otherwise,
     *  the returned list may contain only those candidates that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  candidateGenusType a candidate genus type 
     *  @return the returned <code>Candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByGenusType(org.osid.type.Type candidateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.candidate.ArrayCandidateList(this.candidatesByGenus.get(candidateGenusType)));
    }


    /**
     *  Gets a <code>CandidateList</code> containing the given
     *  candidate record <code>Type</code>. In plenary mode, the
     *  returned list contains all known candidates or an error
     *  results. Otherwise, the returned list may contain only those
     *  candidates that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  candidateRecordType a candidate record type 
     *  @return the returned <code>candidate</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>candidateRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByRecordType(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.candidate.ArrayCandidateList(this.candidatesByRecord.get(candidateRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.candidatesByGenus.clear();
        this.candidatesByRecord.clear();

        super.close();

        return;
    }
}
