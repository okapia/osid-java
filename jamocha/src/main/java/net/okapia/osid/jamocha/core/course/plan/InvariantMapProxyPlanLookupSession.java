//
// InvariantMapProxyPlanLookupSession
//
//    Implements a Plan lookup service backed by a fixed
//    collection of plans. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.plan;


/**
 *  Implements a Plan lookup service backed by a fixed
 *  collection of plans. The plans are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPlanLookupSession
    extends net.okapia.osid.jamocha.core.course.plan.spi.AbstractMapPlanLookupSession
    implements org.osid.course.plan.PlanLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPlanLookupSession} with no
     *  plans.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPlanLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPlanLookupSession} with a single
     *  plan.
     *
     *  @param courseCatalog the course catalog
     *  @param plan a single plan
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code plan} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPlanLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.plan.Plan plan, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putPlan(plan);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPlanLookupSession} using
     *  an array of plans.
     *
     *  @param courseCatalog the course catalog
     *  @param plans an array of plans
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code plans} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPlanLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.plan.Plan[] plans, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putPlans(plans);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPlanLookupSession} using a
     *  collection of plans.
     *
     *  @param courseCatalog the course catalog
     *  @param plans a collection of plans
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code plans} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPlanLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.plan.Plan> plans,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putPlans(plans);
        return;
    }
}
