//
// AuctionHouseElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auctionhouse.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AuctionHouseElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the AuctionHouseElement Id.
     *
     *  @return the auction house element Id
     */

    public static org.osid.id.Id getAuctionHouseEntityId() {
        return (makeEntityId("osid.bidding.AuctionHouse"));
    }


    /**
     *  Gets the AuctionId element Id.
     *
     *  @return the AuctionId element Id
     */

    public static org.osid.id.Id getAuctionId() {
        return (makeQueryElementId("osid.bidding.auctionhouse.AuctionId"));
    }


    /**
     *  Gets the Auction element Id.
     *
     *  @return the Auction element Id
     */

    public static org.osid.id.Id getAuction() {
        return (makeQueryElementId("osid.bidding.auctionhouse.Auction"));
    }


    /**
     *  Gets the BidId element Id.
     *
     *  @return the BidId element Id
     */

    public static org.osid.id.Id getBidId() {
        return (makeQueryElementId("osid.bidding.auctionhouse.BidId"));
    }


    /**
     *  Gets the Bid element Id.
     *
     *  @return the Bid element Id
     */

    public static org.osid.id.Id getBid() {
        return (makeQueryElementId("osid.bidding.auctionhouse.Bid"));
    }


    /**
     *  Gets the AncestorAuctionHouseId element Id.
     *
     *  @return the AncestorAuctionHouseId element Id
     */

    public static org.osid.id.Id getAncestorAuctionHouseId() {
        return (makeQueryElementId("osid.bidding.auctionhouse.AncestorAuctionHouseId"));
    }


    /**
     *  Gets the AncestorAuctionHouse element Id.
     *
     *  @return the AncestorAuctionHouse element Id
     */

    public static org.osid.id.Id getAncestorAuctionHouse() {
        return (makeQueryElementId("osid.bidding.auctionhouse.AncestorAuctionHouse"));
    }


    /**
     *  Gets the DescendantAuctionHouseId element Id.
     *
     *  @return the DescendantAuctionHouseId element Id
     */

    public static org.osid.id.Id getDescendantAuctionHouseId() {
        return (makeQueryElementId("osid.bidding.auctionhouse.DescendantAuctionHouseId"));
    }


    /**
     *  Gets the DescendantAuctionHouse element Id.
     *
     *  @return the DescendantAuctionHouse element Id
     */

    public static org.osid.id.Id getDescendantAuctionHouse() {
        return (makeQueryElementId("osid.bidding.auctionhouse.DescendantAuctionHouse"));
    }
}
