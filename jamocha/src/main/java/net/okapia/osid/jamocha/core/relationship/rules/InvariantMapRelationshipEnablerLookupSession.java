//
// InvariantMapRelationshipEnablerLookupSession
//
//    Implements a RelationshipEnabler lookup service backed by a fixed collection of
//    relationshipEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.rules;


/**
 *  Implements a RelationshipEnabler lookup service backed by a fixed
 *  collection of relationship enablers. The relationship enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRelationshipEnablerLookupSession
    extends net.okapia.osid.jamocha.core.relationship.rules.spi.AbstractMapRelationshipEnablerLookupSession
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRelationshipEnablerLookupSession</code> with no
     *  relationship enablers.
     *  
     *  @param family the family
     *  @throws org.osid.NullArgumnetException {@code family} is
     *          {@code null}
     */

    public InvariantMapRelationshipEnablerLookupSession(org.osid.relationship.Family family) {
        setFamily(family);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelationshipEnablerLookupSession</code> with a single
     *  relationship enabler.
     *  
     *  @param family the family
     *  @param relationshipEnabler a single relationship enabler
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationshipEnabler} is <code>null</code>
     */

      public InvariantMapRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                               org.osid.relationship.rules.RelationshipEnabler relationshipEnabler) {
        this(family);
        putRelationshipEnabler(relationshipEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelationshipEnablerLookupSession</code> using an array
     *  of relationship enablers.
     *  
     *  @param family the family
     *  @param relationshipEnablers an array of relationship enablers
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationshipEnablers} is <code>null</code>
     */

      public InvariantMapRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                               org.osid.relationship.rules.RelationshipEnabler[] relationshipEnablers) {
        this(family);
        putRelationshipEnablers(relationshipEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelationshipEnablerLookupSession</code> using a
     *  collection of relationship enablers.
     *
     *  @param family the family
     *  @param relationshipEnablers a collection of relationship enablers
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationshipEnablers} is <code>null</code>
     */

      public InvariantMapRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                               java.util.Collection<? extends org.osid.relationship.rules.RelationshipEnabler> relationshipEnablers) {
        this(family);
        putRelationshipEnablers(relationshipEnablers);
        return;
    }
}
