//
// AbstractMapFunctionLookupSession
//
//    A simple framework for providing a Function lookup service
//    backed by a fixed collection of functions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Function lookup service backed by a
 *  fixed collection of functions. The functions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Functions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapFunctionLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractFunctionLookupSession
    implements org.osid.authorization.FunctionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authorization.Function> functions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authorization.Function>());


    /**
     *  Makes a <code>Function</code> available in this session.
     *
     *  @param  function a function
     *  @throws org.osid.NullArgumentException <code>function<code>
     *          is <code>null</code>
     */

    protected void putFunction(org.osid.authorization.Function function) {
        this.functions.put(function.getId(), function);
        return;
    }


    /**
     *  Makes an array of functions available in this session.
     *
     *  @param  functions an array of functions
     *  @throws org.osid.NullArgumentException <code>functions<code>
     *          is <code>null</code>
     */

    protected void putFunctions(org.osid.authorization.Function[] functions) {
        putFunctions(java.util.Arrays.asList(functions));
        return;
    }


    /**
     *  Makes a collection of functions available in this session.
     *
     *  @param  functions a collection of functions
     *  @throws org.osid.NullArgumentException <code>functions<code>
     *          is <code>null</code>
     */

    protected void putFunctions(java.util.Collection<? extends org.osid.authorization.Function> functions) {
        for (org.osid.authorization.Function function : functions) {
            this.functions.put(function.getId(), function);
        }

        return;
    }


    /**
     *  Removes a Function from this session.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @throws org.osid.NullArgumentException <code>functionId<code> is
     *          <code>null</code>
     */

    protected void removeFunction(org.osid.id.Id functionId) {
        this.functions.remove(functionId);
        return;
    }


    /**
     *  Gets the <code>Function</code> specified by its <code>Id</code>.
     *
     *  @param  functionId <code>Id</code> of the <code>Function</code>
     *  @return the function
     *  @throws org.osid.NotFoundException <code>functionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>functionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Function getFunction(org.osid.id.Id functionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authorization.Function function = this.functions.get(functionId);
        if (function == null) {
            throw new org.osid.NotFoundException("function not found: " + functionId);
        }

        return (function);
    }


    /**
     *  Gets all <code>Functions</code>. In plenary mode, the returned
     *  list contains all known functions or an error
     *  results. Otherwise, the returned list may contain only those
     *  functions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Functions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.function.ArrayFunctionList(this.functions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.functions.clear();
        super.close();
        return;
    }
}
