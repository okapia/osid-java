//
// AbstractAdapterOfferingLookupSession.java
//
//    An Offering lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Offering lookup session adapter.
 */

public abstract class AbstractAdapterOfferingLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.OfferingLookupSession {

    private final org.osid.offering.OfferingLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOfferingLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOfferingLookupSession(org.osid.offering.OfferingLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code Offering} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOfferings() {
        return (this.session.canLookupOfferings());
    }


    /**
     *  A complete view of the {@code Offering} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOfferingView() {
        this.session.useComparativeOfferingView();
        return;
    }


    /**
     *  A complete view of the {@code Offering} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOfferingView() {
        this.session.usePlenaryOfferingView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offerings in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only offerings whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveOfferingView() {
        this.session.useEffectiveOfferingView();
        return;
    }
    

    /**
     *  All offerings of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveOfferingView() {
        this.session.useAnyEffectiveOfferingView();
        return;
    }

     
    /**
     *  Gets the {@code Offering} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Offering}
     *  may have a different {@code Id} than requested, such as the
     *  case where a duplicate {@code Id} was assigned to a {@code
     *  Offering} and retained for compatibility.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param offeringId {@code Id} of the {@code Offering}
     *  @return the offering
     *  @throws org.osid.NotFoundException {@code offeringId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code offeringId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Offering getOffering(org.osid.id.Id offeringId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffering(offeringId));
    }


    /**
     *  Gets an {@code OfferingList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offerings specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Offerings} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Offering} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code offeringIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByIds(org.osid.id.IdList offeringIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByIds(offeringIds));
    }


    /**
     *  Gets an {@code OfferingList} corresponding to the given
     *  offering genus {@code Type} which does not include
     *  offerings of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned {@code Offering} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusType(org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusType(offeringGenusType));
    }


    /**
     *  Gets an {@code OfferingList} corresponding to the given
     *  offering genus {@code Type} and include any additional
     *  offerings with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned {@code Offering} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByParentGenusType(org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByParentGenusType(offeringGenusType));
    }


    /**
     *  Gets an {@code OfferingList} containing the given
     *  offering record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringRecordType an offering record type 
     *  @return the returned {@code Offering} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByRecordType(org.osid.type.Type offeringRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByRecordType(offeringRecordType));
    }


    /**
     *  Gets an {@code OfferingList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In active mode, offerings are returned that are currently
     *  active. In any status mode, active and inactive offerings are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Offering} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsOnDate(from, to));
    }
        

    /**
     *  Gets an {@code OfferingList} by genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently
     *  effective. In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  offeringGenusType an offering genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code
     *          offeringGenusType, from}, or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusTypeOnDate(org.osid.type.Type offeringGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusTypeOnDate(offeringGenusType, from, to));
    }


    /**
     *  Gets a list of offerings corresponding to a canonical unit
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  canonicalUnitId the {@code Id} of the canonical unit
     *  @return the returned {@code OfferingList}
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForCanonicalUnit(org.osid.id.Id canonicalUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsForCanonicalUnit(canonicalUnitId));
    }


    /**
     *  Gets an {@code OfferingList} by genus type for the given
     *  canonical unit.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently 
     *  effective. In any effective mode, effective offerings and those 
     *  currently expired are returned. 
     *
     *  @param  canonicalUnitId a canonical unit {@code Id} 
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId} or 
     *          {@code offeringGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusTypeForCanonicalUnit(org.osid.id.Id canonicalUnitId, 
                                                                                  org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusTypeForCanonicalUnit(canonicalUnitId, offeringGenusType));
    }


    /**
     *  Gets a list of offerings corresponding to a canonical unit
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible
     *  through this session.
     *
     *  In effective mode, offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  offerings and those currently expired are returned.
     *
     *  @param  canonicalUnitId the {@code Id} of the canonical unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code OfferingList}
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForCanonicalUnitOnDate(org.osid.id.Id canonicalUnitId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsForCanonicalUnitOnDate(canonicalUnitId, from, to));
    }


    /**
     *  Gets an {@code OfferingList} by genus type for the given
     *  canonical unit and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently 
     *  effective. In any effective mode, effective offerings aqnd those 
     *  currently expired are returned. 
     *
     *  @param  canonicalUnitId a canonical {@code Id} 
     *  @param  offeringGenusType an offering genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId,
     *          offeringGenusType, from}, or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusTypeForCanonicalUnitOnDate(org.osid.id.Id canonicalUnitId, 
                                                                                        org.osid.type.Type offeringGenusType, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusTypeForCanonicalUnitOnDate(canonicalUnitId, offeringGenusType, from, to));
    }


    /**
     *  Gets a list of offerings corresponding to a time period {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  timePeriodId the {@code Id} of the time period
     *  @return the returned {@code OfferingList}
     *  @throws org.osid.NullArgumentException {@code timePeriodId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsForTimePeriod(timePeriodId));
    }


    /**
     *  Gets an {@code OfferingList} by genus type for the
     *  given time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently 
     *  effective. In any effective mode, effective offerings and those 
     *  currently expired are returned. 
     *
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.NullArgumentException {@code timePeriodId} or 
     *          {@code offeringGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusTypeForTimePeriod(org.osid.id.Id timePeriodId, 
                                                                               org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusTypeForTimePeriod(timePeriodId, offeringGenusType));
    }


    /**
     *  Gets a list of offerings corresponding to a time period {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  timePeriodId the {@code Id} of the time period
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code OfferingList}
     *  @throws org.osid.NullArgumentException {@code timePeriodId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForTimePeriodOnDate(org.osid.id.Id timePeriodId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsForTimePeriodOnDate(timePeriodId, from, to));
    }


    /**
     *  Gets an {@code OfferingList} by genus type for the given time
     *  period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently
     *  effective. In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  offeringGenusType an offering genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code timePeriodId,
     *          offeringGenusType, from}, or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusTypeForTimePeriodOnDate(org.osid.id.Id timePeriodId, 
                                                                                     org.osid.type.Type offeringGenusType, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusTypeForTimePeriodOnDate(timePeriodId, offeringGenusType, from, to));
    }


    /**
     *  Gets a list of offerings corresponding to canonical unit and
     *  time period {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  canonicalUnitId the {@code Id} of the canonical unit
     *  @param  timePeriodId the {@code Id} of the time period
     *  @return the returned {@code OfferingList}
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId},
     *          {@code timePeriodId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForCanonicalUnitAndTimePeriod(org.osid.id.Id canonicalUnitId,
                                                                                    org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsForCanonicalUnitAndTimePeriod(canonicalUnitId, timePeriodId));
    }


    /**
     *  Gets an {@code OfferingList} by genus type for the given
     *  canonical unit and time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently
     *  effective. In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  canonicalUnitId a canonical unit {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.NullArgumentException {@code
     *          canonicalUnitId}, {@code timePeriodId}, or {@code
     *          offeringGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusTypeForCanonicalUnitAndTimePeriod(org.osid.id.Id canonicalUnitId, 
                                                                                               org.osid.id.Id timePeriodId, 
                                                                                               org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusTypeForCanonicalUnitAndTimePeriod(canonicalUnitId, timePeriodId, offeringGenusType));
    }


    /**
     *  Gets a list of offerings corresponding to canonical unit and
     *  time period {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective. In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  timePeriodId the {@code Id} of the time period
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code OfferingList}
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId},
     *          {@code timePeriodId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsForCanonicalUnitAndTimePeriodOnDate(org.osid.id.Id canonicalUnitId,
                                                                                          org.osid.id.Id timePeriodId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsForCanonicalUnitAndTimePeriodOnDate(canonicalUnitId, timePeriodId, from, to));
    }


    /**
     *  Gets an {@code OfferingList} by genus type for the given
     *  canonical, time period, and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently
     *  effective. In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  canonicalUnitId a canonical unit {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  offeringGenusType an offering genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId,
     *          timePeriodId, offeringGenusType, from}, or {@code to}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusTypeForCanonicalUnitAndTimePeriodOnDate(org.osid.id.Id canonicalUnitId, 
                                                                                                     org.osid.id.Id timePeriodId, 
                                                                                                     org.osid.type.Type offeringGenusType, 
                                                                                                     org.osid.calendaring.DateTime from, 
                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByGenusTypeForCanonicalUnitAndTimePeriodOnDate(canonicalUnitId, timePeriodId, offeringGenusType, from, to));
    }


    /**
     *  Gets an {@code OfferingList} by code and time period. 
     *  
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session.
     *  
     *  In effective mode, offerings are returned that are currently
     *  effective. In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @param  code a canonical unit {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @return the returned {@code OfferingList} 
     *  @throws org.osid.NullArgumentException {@code code} or {@code
     *          timePeriodId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByCodeAndTimePeriod(String code, 
                                                                          org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingsByCodeAndTimePeriod(code, timePeriodId));
    }


    /**
     *  Gets all {@code Offerings}. 
     *
     *  In plenary mode, the returned list contains all known
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, offerings are returned that are currently
     *  effective.  In any effective mode, effective offerings and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Offerings} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferings());
    }
}
