//
// AbstractCampusNodeToCampusList.java
//
//     Implements an AbstractCampusNodeToCampusList adapter.
//
//
// Tom Coppeto
// OnTapSolutions
// 21 September 2010
//
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.room.campusnode.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements an AbstractCampusList adapter to convert campus
 *  nodes into campuses.
 */

public abstract class AbstractCampusNodeToCampusList
    extends net.okapia.osid.jamocha.adapter.room.campus.spi.AbstractAdapterCampusList
    implements org.osid.room.CampusList {

    private final org.osid.room.CampusNodeList list;


    /**
     *  Constructs a new {@code AbstractCampusNodeToCampusList}.
     *
     *  @param list the campus node list to convert
     *  @throws org.osid.NullArgumentException {@code list} is
     *          {@code null}
     */

    protected AbstractCampusNodeToCampusList(org.osid.room.CampusNodeList list) {
        super(list);
        this.list = list;
        return;
    }


    /**
     *  Gets the next {@code Campus} in this list. 
     *
     *  @return the next {@code Campus} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Campus} is available before calling this
     *          method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Campus getNextCampus()
        throws org.osid.OperationFailedException {

        return (this.list.getNextCampusNode().getCampus());
    }
}
