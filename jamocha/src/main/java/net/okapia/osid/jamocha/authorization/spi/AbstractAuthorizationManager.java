//
// AbstractAuthorizationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAuthorizationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.authorization.AuthorizationManager,
               org.osid.authorization.AuthorizationProxyManager {

    private final Types authorizationRecordTypes           = new TypeRefSet();
    private final Types authorizationSearchRecordTypes     = new TypeRefSet();

    private final Types functionRecordTypes                = new TypeRefSet();
    private final Types functionSearchRecordTypes          = new TypeRefSet();

    private final Types qualifierRecordTypes               = new TypeRefSet();
    private final Types qualifierSearchRecordTypes         = new TypeRefSet();

    private final Types vaultRecordTypes                   = new TypeRefSet();
    private final Types vaultSearchRecordTypes             = new TypeRefSet();

    private final Types authorizationConditionRecordTypes  = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractAuthorizationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAuthorizationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of an authorization service which is the 
     *  basic service for checking authorizations. 
     *
     *  @return <code> true </code> if authorization is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorization() {
        return (false);
    }


    /**
     *  Tests if an authorization lookup service is supported. An 
     *  authorization lookup service defines methods to access authorizations. 
     *
     *  @return true if authorization lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationLookup() {
        return (false);
    }


    /**
     *  Tests if an authorization query service is supported. 
     *
     *  @return <code> true </code> if authorization query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (false);
    }


    /**
     *  Tests if an authorization search service is supported. 
     *
     *  @return <code> true </code> if authorization search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationSearch() {
        return (false);
    }


    /**
     *  Tests if an authorization administrative service is supported. 
     *
     *  @return <code> true </code> if authorization admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationAdmin() {
        return (false);
    }


    /**
     *  Tests if authorization notification is supported. Messages may be sent 
     *  when authorizations are created, modified, or deleted. 
     *
     *  @return <code> true </code> if authorization notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationNotification() {
        return (false);
    }


    /**
     *  Tests if an authorization to vault lookup session is available. 
     *
     *  @return <code> true </code> if authorization vault lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationVault() {
        return (false);
    }


    /**
     *  Tests if an authorization to vault assignment session is available. 
     *
     *  @return <code> true </code> if authorization vault assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationVaultAssignment() {
        return (false);
    }


    /**
     *  Tests if an authorization smart vaulting session is available. 
     *
     *  @return <code> true </code> if authorization smart vaulting is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationSmartVault() {
        return (false);
    }


    /**
     *  Tests if a function lookup service is supported. A function lookup 
     *  service defines methods to access authorization functions. 
     *
     *  @return <code> true </code> if function lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionLookup() {
        return (false);
    }


    /**
     *  Tests if a function query service is supported. 
     *
     *  @return <code> true </code> if function query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionQuery() {
        return (false);
    }


    /**
     *  Tests if a function search service is supported. 
     *
     *  @return <code> true </code> if function search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionSearch() {
        return (false);
    }


    /**
     *  Tests if a function administrative service is supported. 
     *
     *  @return <code> true </code> if function admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionAdmin() {
        return (false);
    }


    /**
     *  Tests if function notification is supported. Messages may be sent when 
     *  functions are created, modified, or deleted. 
     *
     *  @return <code> true </code> if function notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionNotification() {
        return (false);
    }


    /**
     *  Tests if a function to vault lookup session is available. 
     *
     *  @return <code> true </code> if function vault lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionVault() {
        return (false);
    }


    /**
     *  Tests if a function to vault assignment session is available. 
     *
     *  @return <code> true </code> if function vault assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionVaultAssignment() {
        return (false);
    }


    /**
     *  Tests if a function smart vaulting session is available. 
     *
     *  @return <code> true </code> if function smart vaulting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionSmartVault() {
        return (false);
    }


    /**
     *  Tests if a qualifier lookup service is supported. A function lookup 
     *  service defines methods to access authorization qualifiers. 
     *
     *  @return <code> true </code> if qualifier lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierLookup() {
        return (false);
    }


    /**
     *  Tests if a qualifier query service is supported. 
     *
     *  @return <code> true </code> if qualifier query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierQuery() {
        return (false);
    }


    /**
     *  Tests if a qualifier search service is supported. 
     *
     *  @return <code> true </code> if qualifier search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierSearch() {
        return (false);
    }


    /**
     *  Tests if a qualifier administrative service is supported. 
     *
     *  @return <code> true </code> if qualifier admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierAdmin() {
        return (false);
    }


    /**
     *  Tests if qualifier notification is supported. Messages may be sent 
     *  when qualifiers are created, modified, or deleted. 
     *
     *  @return <code> true </code> if qualifier notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierNotification() {
        return (false);
    }


    /**
     *  Tests if a qualifier hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a qualifier hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchy() {
        return (false);
    }


    /**
     *  Tests if qualifier hierarchy design is supported. 
     *
     *  @return <code> true </code> if a qualifier hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a qualifier to vault lookup session is available. 
     *
     *  @return <code> true </code> if qualifier vault lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierVault() {
        return (false);
    }


    /**
     *  Tests if a qualifier to vault assignment session is available. 
     *
     *  @return <code> true </code> if qualifier vault assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierVaultAssignment() {
        return (false);
    }


    /**
     *  Tests if a qualifier smart vaulting session is available. 
     *
     *  @return <code> true </code> if qualifier smart vault session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierSmartVault() {
        return (false);
    }


    /**
     *  Tests if a vault lookup service is supported. A vault lookup service 
     *  defines methods to access authorization vaults. 
     *
     *  @return <code> true </code> if function lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultLookup() {
        return (false);
    }


    /**
     *  Tests if a vault query service is supported. 
     *
     *  @return <code> true </code> if vault query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Tests if a vault search service is supported. 
     *
     *  @return <code> true </code> if vault search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultSearch() {
        return (false);
    }


    /**
     *  Tests if a vault administrative service is supported. 
     *
     *  @return <code> true </code> if vault admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultAdmin() {
        return (false);
    }


    /**
     *  Tests if vault notification is supported. Messages may be sent when 
     *  vaults are created, modified, or deleted. 
     *
     *  @return <code> true </code> if vault notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultNotification() {
        return (false);
    }


    /**
     *  Tests if a vault hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a vault hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultHierarchy() {
        return (false);
    }


    /**
     *  Tests if vault hierarchy design is supported. 
     *
     *  @return <code> true </code> if a function hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an authorization batch service is supported. 
     *
     *  @return <code> true </code> if an authorization batch service design 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizatinBatch() {
        return (false);
    }


    /**
     *  Tests if an authorization rules service is supported. 
     *
     *  @return <code> true </code> if an authorization rules service design 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizatinRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Authorization </code> record types. 
     *
     *  @return a list containing the supported authorization record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.authorizationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given authorization record type is supported. 
     *
     *  @param  authorizationRecordType a <code> Type </code> indicating an 
     *          authorization record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> authorizationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationRecordType(org.osid.type.Type authorizationRecordType) {
        return (this.authorizationRecordTypes.contains(authorizationRecordType));
    }


    /**
     *  Adds support for an authorization record type.
     *
     *  @param authorizationRecordType an authorization record type
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationRecordType</code> is <code>null</code>
     */

    protected void addAuthorizationRecordType(org.osid.type.Type authorizationRecordType) {
        this.authorizationRecordTypes.add(authorizationRecordType);
        return;
    }


    /**
     *  Removes support for an authorization record type.
     *
     *  @param authorizationRecordType an authorization record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationRecordType</code> is <code>null</code>
     */

    protected void removeAuthorizationRecordType(org.osid.type.Type authorizationRecordType) {
        this.authorizationRecordTypes.remove(authorizationRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Authorization </code> search record types. 
     *
     *  @return a list containing the supported authorization search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.authorizationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given authorization search record type is supported. 
     *
     *  @param  authorizationSearchRecordType a <code> Type </code> indicating 
     *          an authorization search record type 
     *  @return <code> true </code> if the given search record Type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationSearchRecordType(org.osid.type.Type authorizationSearchRecordType) {
        return (this.authorizationSearchRecordTypes.contains(authorizationSearchRecordType));
    }


    /**
     *  Adds support for an authorization search record type.
     *
     *  @param authorizationSearchRecordType an authorization search record type
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationSearchRecordType</code> is <code>null</code>
     */

    protected void addAuthorizationSearchRecordType(org.osid.type.Type authorizationSearchRecordType) {
        this.authorizationSearchRecordTypes.add(authorizationSearchRecordType);
        return;
    }


    /**
     *  Removes support for an authorization search record type.
     *
     *  @param authorizationSearchRecordType an authorization search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuthorizationSearchRecordType(org.osid.type.Type authorizationSearchRecordType) {
        this.authorizationSearchRecordTypes.remove(authorizationSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Function </code> record types. 
     *
     *  @return a list containing the supported <code> Function </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFunctionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.functionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Function </code> record type is supported. 
     *
     *  @param  functionRecordType a <code> Type </code> indicating a <code> 
     *          Function </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> functionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFunctionRecordType(org.osid.type.Type functionRecordType) {
        return (this.functionRecordTypes.contains(functionRecordType));
    }


    /**
     *  Adds support for a function record type.
     *
     *  @param functionRecordType a function record type
     *  @throws org.osid.NullArgumentException
     *  <code>functionRecordType</code> is <code>null</code>
     */

    protected void addFunctionRecordType(org.osid.type.Type functionRecordType) {
        this.functionRecordTypes.add(functionRecordType);
        return;
    }


    /**
     *  Removes support for a function record type.
     *
     *  @param functionRecordType a function record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>functionRecordType</code> is <code>null</code>
     */

    protected void removeFunctionRecordType(org.osid.type.Type functionRecordType) {
        this.functionRecordTypes.remove(functionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Function </code> search record types. 
     *
     *  @return a list containing the supported <code> Function </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFunctionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.functionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Function </code> search record type is 
     *  supported. 
     *
     *  @param  functionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Function </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> functionSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFunctionSearchRecordType(org.osid.type.Type functionSearchRecordType) {
        return (this.functionSearchRecordTypes.contains(functionSearchRecordType));
    }


    /**
     *  Adds support for a function search record type.
     *
     *  @param functionSearchRecordType a function search record type
     *  @throws org.osid.NullArgumentException
     *  <code>functionSearchRecordType</code> is <code>null</code>
     */

    protected void addFunctionSearchRecordType(org.osid.type.Type functionSearchRecordType) {
        this.functionSearchRecordTypes.add(functionSearchRecordType);
        return;
    }


    /**
     *  Removes support for a function search record type.
     *
     *  @param functionSearchRecordType a function search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>functionSearchRecordType</code> is <code>null</code>
     */

    protected void removeFunctionSearchRecordType(org.osid.type.Type functionSearchRecordType) {
        this.functionSearchRecordTypes.remove(functionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Qualifier </code> record types. 
     *
     *  @return a list containing the supported <code> Qualifier </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQualifierRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.qualifierRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Qualifier </code> record type is supported. 
     *
     *  @param  qualifierRecordType a <code> Type </code> indicating a <code> 
     *          Qualifier </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> qualifierRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQualifierRecordType(org.osid.type.Type qualifierRecordType) {
        return (this.qualifierRecordTypes.contains(qualifierRecordType));
    }


    /**
     *  Adds support for a qualifier record type.
     *
     *  @param qualifierRecordType a qualifier record type
     *  @throws org.osid.NullArgumentException
     *  <code>qualifierRecordType</code> is <code>null</code>
     */

    protected void addQualifierRecordType(org.osid.type.Type qualifierRecordType) {
        this.qualifierRecordTypes.add(qualifierRecordType);
        return;
    }


    /**
     *  Removes support for a qualifier record type.
     *
     *  @param qualifierRecordType a qualifier record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>qualifierRecordType</code> is <code>null</code>
     */

    protected void removeQualifierRecordType(org.osid.type.Type qualifierRecordType) {
        this.qualifierRecordTypes.remove(qualifierRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Qualifier </code> search record types. 
     *
     *  @return a list containing the supported <code> Qualifier </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQualifierSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.qualifierSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Qualifier </code> search record type is 
     *  supported. 
     *
     *  @param  qualifierSearchRecordType a <code> Type </code> indicating a 
     *          <code> Qualifier </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          qualifierSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQualifierSearchRecordType(org.osid.type.Type qualifierSearchRecordType) {
        return (this.qualifierSearchRecordTypes.contains(qualifierSearchRecordType));
    }


    /**
     *  Adds support for a qualifier search record type.
     *
     *  @param qualifierSearchRecordType a qualifier search record type
     *  @throws org.osid.NullArgumentException
     *  <code>qualifierSearchRecordType</code> is <code>null</code>
     */

    protected void addQualifierSearchRecordType(org.osid.type.Type qualifierSearchRecordType) {
        this.qualifierSearchRecordTypes.add(qualifierSearchRecordType);
        return;
    }


    /**
     *  Removes support for a qualifier search record type.
     *
     *  @param qualifierSearchRecordType a qualifier search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>qualifierSearchRecordType</code> is <code>null</code>
     */

    protected void removeQualifierSearchRecordType(org.osid.type.Type qualifierSearchRecordType) {
        this.qualifierSearchRecordTypes.remove(qualifierSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Vault </code> record types. 
     *
     *  @return a list containing the supported <code> Vault </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVaultRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.vaultRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Vault </code> record type is supported. 
     *
     *  @param  vaultRecordType a <code> Type </code> indicating a <code> 
     *          Vault </code> type 
     *  @return <code> true </code> if the given vault record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> vaultRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVaultRecordType(org.osid.type.Type vaultRecordType) {
        return (this.vaultRecordTypes.contains(vaultRecordType));
    }


    /**
     *  Adds support for a vault record type.
     *
     *  @param vaultRecordType a vault record type
     *  @throws org.osid.NullArgumentException
     *  <code>vaultRecordType</code> is <code>null</code>
     */

    protected void addVaultRecordType(org.osid.type.Type vaultRecordType) {
        this.vaultRecordTypes.add(vaultRecordType);
        return;
    }


    /**
     *  Removes support for a vault record type.
     *
     *  @param vaultRecordType a vault record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>vaultRecordType</code> is <code>null</code>
     */

    protected void removeVaultRecordType(org.osid.type.Type vaultRecordType) {
        this.vaultRecordTypes.remove(vaultRecordType);
        return;
    }


    /**
     *  Gets the supported vault search record types. 
     *
     *  @return a list containing the supported <code> Vault </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getVaultSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.vaultSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given vault search record type is supported. 
     *
     *  @param  vaultSearchRecordType a <code> Type </code> indicating a 
     *          <code> Vault </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> vaultSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsVaultSearchRecordType(org.osid.type.Type vaultSearchRecordType) {
        return (this.vaultSearchRecordTypes.contains(vaultSearchRecordType));
    }


    /**
     *  Adds support for a vault search record type.
     *
     *  @param vaultSearchRecordType a vault search record type
     *  @throws org.osid.NullArgumentException
     *  <code>vaultSearchRecordType</code> is <code>null</code>
     */

    protected void addVaultSearchRecordType(org.osid.type.Type vaultSearchRecordType) {
        this.vaultSearchRecordTypes.add(vaultSearchRecordType);
        return;
    }


    /**
     *  Removes support for a vault search record type.
     *
     *  @param vaultSearchRecordType a vault search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>vaultSearchRecordType</code> is <code>null</code>
     */

    protected void removeVaultSearchRecordType(org.osid.type.Type vaultSearchRecordType) {
        this.vaultSearchRecordTypes.remove(vaultSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuthorizationCondition </code> record types. 
     *
     *  @return a list containing the supported <code> AuthorizationCondition 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationConditionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.authorizationConditionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuthorizationCondition </code> record type 
     *  is supported. 
     *
     *  @param  authorizationConditionRecordType a <code> Type </code> 
     *          indicating an <code> AuthorizationCondition </code> record 
     *          type 
     *  @return <code> true </code> if the given authorization condition 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationConditionRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationConditionRecordType(org.osid.type.Type authorizationConditionRecordType) {
        return (this.authorizationConditionRecordTypes.contains(authorizationConditionRecordType));
    }


    /**
     *  Adds support for an authorization condition record type.
     *
     *  @param authorizationConditionRecordType an authorization condition record type
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationConditionRecordType</code> is <code>null</code>
     */

    protected void addAuthorizationConditionRecordType(org.osid.type.Type authorizationConditionRecordType) {
        this.authorizationConditionRecordTypes.add(authorizationConditionRecordType);
        return;
    }


    /**
     *  Removes support for an authorization condition record type.
     *
     *  @param authorizationConditionRecordType an authorization condition record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationConditionRecordType</code> is <code>null</code>
     */

    protected void removeAuthorizationConditionRecordType(org.osid.type.Type authorizationConditionRecordType) {
        this.authorizationConditionRecordTypes.remove(authorizationConditionRecordType);
        return;
    }


    /**
     *  Gets an <code> AuthorizationSession </code> which is responsible for 
     *  performing authorization checks. 
     *
     *  @return an authorization session for this service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuthorization() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSession getAuthorizationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationSession not implemented");
    }


    /**
     *  Gets an <code> AuthorizationSession </code> which is responsible for 
     *  performing authorization checks. 
     *
     *  @param  proxy a proxy 
     *  @return an authorization session for this service 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuthorization() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSession getAuthorizationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationSession not implemented");
    }


    /**
     *  Gets an <code> AuthorizationSession </code> which is responsible for 
     *  performing authorization checks for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAuthorization() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSession getAuthorizationSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationSessionForVault not implemented");
    }


    /**
     *  Gets an <code> AuthorizationSession </code> which is responsible for 
     *  performing authorization checks for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> an AuthorizationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAuthorization() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSession getAuthorizationSessionForVault(org.osid.id.Id vaultId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  lookup service. 
     *
     *  @return an <code> AuthorizationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationLookupSession getAuthorizationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationLookupSession getAuthorizationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationLookupSession getAuthorizationLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> an AuthorizationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationLookupSession getAuthorizationLookupSessionForVault(org.osid.id.Id vaultId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  query service. 
     *
     *  @return an <code> AuthorizationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuerySession getAuthorizationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuerySession getAuthorizationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuerySession getAuthorizationQuerySessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> an AuthorizationQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuerySession getAuthorizationQuerySessionForVault(org.osid.id.Id vaultId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  search service. 
     *
     *  @return an <code> AuthorizationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSearchSession getAuthorizationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSearchSession getAuthorizationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSearchSession getAuthorizationSearchSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> an AuthorizationSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSearchSession getAuthorizationSearchSessionForVault(org.osid.id.Id vaultId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  administration service. 
     *
     *  @return an <code> AuthorizationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationAdminSession getAuthorizationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationAdminSession getAuthorizationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  admin service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationAdminSession getAuthorizationAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  admin service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> an AuthorizationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationAdminSession getAuthorizationAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationAdminSessionForVault not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  authorization changes. 
     *
     *  @param  authorizationReceiver the authorization receiver 
     *  @return an <code> AuthorizationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> authorizationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationNotificationSession getAuthorizationNotificationSession(org.osid.authorization.AuthorizationReceiver authorizationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  authorization changes. 
     *
     *  @param  authorizationReceiver the authorization receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> authorizationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationNotificationSession getAuthorizationNotificationSession(org.osid.authorization.AuthorizationReceiver authorizationReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  notification service for the given vault. 
     *
     *  @param  authorizationReceiver the authorization receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> an AuthorizationNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> authorizationReceiver 
     *          </code> or <code> vaultId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationNotificationSession getAuthorizationNotificationSessionForVault(org.osid.authorization.AuthorizationReceiver authorizationReceiver, 
                                                                                                               org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  notification service for the given vault. 
     *
     *  @param  authorizationReceiver the authorization receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> an AuthorizationNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> authorizationReceiver 
     *          </code> or <code> vaultId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationNotificationSession getAuthorizationNotificationSessionForVault(org.osid.authorization.AuthorizationReceiver authorizationReceiver, 
                                                                                                               org.osid.id.Id vaultId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the session for retrieving authorization to vault mappings. 
     *
     *  @return an <code> AuthorizationVaultSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationVaultSession getAuthorizationVaultSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationVaultSession not implemented");
    }


    /**
     *  Gets the session for retrieving authorization to vault mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationVaultSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationVaultSession getAuthorizationVaultSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationVaultSession not implemented");
    }


    /**
     *  Gets the session for assigning authorizations to vault mappings. 
     *
     *  @return a <code> AuthorizationVaultAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationVaultAssignmentSession getAuthorizationVaultAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning authorization to vault mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AuthorizationVaultAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationVaultAssignmentSession getAuthorizationVaultAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic authorization vaults. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> AuthorizationSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSmartVault() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSmartVaultSession getAuthorizationSmartVaultSession(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationSmartVaultSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic authorization vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> AuthorizationSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationSmartVault() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationSmartVaultSession getAuthorizationSmartVaultSession(org.osid.id.Id vaultId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationSmartVaultSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  lookup service. 
     *
     *  @return a <code> FunctionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionLookupSession getFunctionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionLookupSession getFunctionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> a FunctionLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionLookupSession getFunctionLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> a FunctionLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionLookupSession getFunctionLookupSessionForVault(org.osid.id.Id vaultId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function query 
     *  service. 
     *
     *  @return a <code> FunctionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuerySession getFunctionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuerySession getFunctionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function query 
     *  service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> FunctionQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuerySession getFunctionQuerySessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function query 
     *  service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> FunctionQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuerySession getFunctionQuerySessionForVault(org.osid.id.Id vaultId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  search service. 
     *
     *  @return a <code> FunctionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchSession getFunctionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchSession getFunctionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> FunctionSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchSession getFunctionSearchSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> FunctionSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchSession getFunctionSearchSessionForVault(org.osid.id.Id vaultId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  administration service. 
     *
     *  @return a <code> FunctionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionAdminSession getFunctionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionAdminSession getFunctionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function admin 
     *  service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> a FunctionAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionAdminSession getFunctionAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function admin 
     *  service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> a FunctionAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionAdminSession getFunctionAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionAdminSessionForVault not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to function 
     *  changes. 
     *
     *  @param  functionReceiver the function receiver 
     *  @return a <code> FunctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> functionReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionNotificationSession getFunctionNotificationSession(org.osid.authorization.FunctionReceiver functionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to function 
     *  changes. 
     *
     *  @param  functionReceiver the function receiver 
     *  @param  proxy a proxy 
     *  @return a <code> FunctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> functionReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionNotificationSession getFunctionNotificationSession(org.osid.authorization.FunctionReceiver functionReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  notification service for the given vault. 
     *
     *  @param  functionReceiver the function receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return <code> a FunctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> functionReceiver </code> 
     *          or <code> vaultId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionNotificationSession getFunctionNotificationSessionForVault(org.osid.authorization.FunctionReceiver functionReceiver, 
                                                                                                     org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the function 
     *  notification service for the given vault. 
     *
     *  @param  functionReceiver the function receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return <code> a FunctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> functionReceiver </code> 
     *          or <code> vaultId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionNotificationSession getFunctionNotificationSessionForVault(org.osid.authorization.FunctionReceiver functionReceiver, 
                                                                                                     org.osid.id.Id vaultId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the session for retrieving function to vault mappings. 
     *
     *  @return a <code> FunctionVaultSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionVault() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionVaultSession getFunctionVaultSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionVaultSession not implemented");
    }


    /**
     *  Gets the session for retrieving function to vault mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionVaultSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionVault() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionVaultSession getFunctionVaultSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionVaultSession not implemented");
    }


    /**
     *  Gets the session for assigning function to vault mappings. 
     *
     *  @return a <code> FunctionVaultAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionVaultAssignmentSession getFunctionVaultAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning function to vault mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionVaultAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionVaultAssignmentSession getFunctionVaultAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the function smart vault for the 
     *  given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> FunctionSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSmartVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSmartVaultSession getFunctionSmartVaultSession(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getFunctionSmartVaultSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic function vaults for the given 
     *  vault. 
     *
     *  @param  vaultId the <code> Id </code> of a vault 
     *  @param  proxy a proxy 
     *  @return <code> vaultId </code> not found 
     *  @throws org.osid.NotFoundException <code> vaultId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSmartVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSmartVaultSession getFunctionSmartVaultSession(org.osid.id.Id vaultId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getFunctionSmartVaultSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  lookup service. 
     *
     *  @return a <code> QualifierLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierLookupSession getQualifierLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierLookupSession getQualifierLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierLookupSession getQualifierLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierLookupSession getQualifierLookupSessionForVault(org.osid.id.Id vaultId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  query service. 
     *
     *  @return a <code> QualifierQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuerySession getQualifierQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuerySession getQualifierQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuerySession getQualifierQuerySessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuerySession getQualifierQuerySessionForVault(org.osid.id.Id vaultId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  search service. 
     *
     *  @return a <code> QualifierSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchSession getQualifierSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchSession getQualifierSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchSession getQualifierSearchSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  search service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchSession getQualifierSearchSessionForVault(org.osid.id.Id vaultId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  administration service. 
     *
     *  @return a <code> QualifierAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierAdminSession getQualifierAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierAdminSession getQualifierAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  admin service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierAdminSession getQualifierAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  admin service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierAdminSession getQualifierAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierAdminSessionForVault not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  qualifier changes. 
     *
     *  @param  qualifierReceiver the qualifier receiver 
     *  @return a <code> QualifierNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> qualifierReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierNotificationSession getQualifierNotificationSession(org.osid.authorization.QualifierReceiver qualifierReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  qualifier changes. 
     *
     *  @param  qualifierReceiver the qualifier receiver 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> qualifierReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierNotificationSession getQualifierNotificationSession(org.osid.authorization.QualifierReceiver qualifierReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  notification service for the given vault. 
     *
     *  @param  qualifierReceiver the qualifier receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> qualifierReceiver 
     *          </code> or <code> vaultId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierNotificationSession getQualifierNotificationSessionForVault(org.osid.authorization.QualifierReceiver qualifierReceiver, 
                                                                                                       org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  notification service for the given vault. 
     *
     *  @param  qualifierReceiver the qualifier receiver 
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> qualifierReceiver 
     *          </code> or <code> vaultId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierNotificationSession getQualifierNotificationSessionForVault(org.osid.authorization.QualifierReceiver qualifierReceiver, 
                                                                                                       org.osid.id.Id vaultId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  hierarchy traversal service. The authorization service uses distinct 
     *  hierarchies that can be managed through a Hierarchy OSID. 
     *
     *  @param  qualifierHierarchyId the <code> Id </code> of a qualifier 
     *          hierarchy 
     *  @return a <code> QualifierHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> qualifierHierarchyId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierHierarchySession getQualifierHierarchySession(org.osid.id.Id qualifierHierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  hierarchy traversal service. The authorization service uses distinct 
     *  hierarchies that can be managed through a Hierarchy OSID. 
     *
     *  @param  qualifierHierarchyId the <code> Id </code> of a qualifier 
     *          hierarchy 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> qualifierHierarchyId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierHierarchySession getQualifierHierarchySession(org.osid.id.Id qualifierHierarchyId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  hierarchy design service. 
     *
     *  @param  qualifierHierarchyId the <code> Id </code> of a qualifier 
     *          hierarchy 
     *  @return a <code> QualifierHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> qualifierHierarchyId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierHierarchyDesignSession getQualifierHierarchyDesignSession(org.osid.id.Id qualifierHierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the qualifier 
     *  hierarchy design service. 
     *
     *  @param  qualifierHierarchyId the <code> Id </code> of a qualifier 
     *          hierarchy 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> qualifierHierarchyId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierHierarchyDesignSession getQualifierHierarchyDesignSession(org.osid.id.Id qualifierHierarchyId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session for retrieving qualifier to vault mappings. 
     *
     *  @return a <code> QualifierVaultSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierVaultSession getQualifierVaultSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierVaultSession not implemented");
    }


    /**
     *  Gets the session for retrieving qualifier to vault mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierVaultSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierVaultSession getQualifierVaultSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierVaultSession not implemented");
    }


    /**
     *  Gets the session for assigning qualifier to vault mappings. 
     *
     *  @return a <code> QualifierVaultAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierVaultSession getQualifierVaultAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning qualifier to vault mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierVaultAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierVaultAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierVaultSession getQualifierVaultAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the qualifier smart vault for the 
     *  given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the vault 
     *  @return a <code> QualifierSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException <code> vaultId </code> not found 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSmartVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSmartVaultSession getQualifierSmartVaultSession(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getQualifierSmartVaultSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic qualifier vaults for the given 
     *  vault. 
     *
     *  @param  vaultId the <code> Id </code> of a vault 
     *  @param  proxy a proxy 
     *  @return <code> vaultId </code> not found 
     *  @throws org.osid.NotFoundException <code> vaultId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSmartVault() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSmartVaultSession getQualifierSmartVaultSession(org.osid.id.Id vaultId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getQualifierSmartVaultSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault lookup service. 
     *
     *  @return a <code> VaultLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultLookupSession getVaultLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getVaultLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultLookupSession getVaultLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getVaultLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault query service. 
     *
     *  @return a <code> VaultQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuerySession getVaultQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getVaultQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuerySession getVaultQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getVaultQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault search service. 
     *
     *  @return a <code> VaultSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultSearchSession getVaultSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getVaultSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultSearchSession getVaultSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getVaultSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault administration service. 
     *
     *  @return a <code> VaultAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultAdmin() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultAdminSession getVaultAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getVaultAdminSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the vault administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsVaultAdmin() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultAdminSession getVaultAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getVaultAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to vault 
     *  service changes. 
     *
     *  @param  vaultreceiver the vault receiver 
     *  @return a <code> VaultNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> vaultReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultNotificationSession getVaultNotificationSession(org.osid.authorization.VaultReceiver vaultreceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getVaultNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to vault 
     *  service changes. 
     *
     *  @param  vaultReceiver the vault receiver 
     *  @param  proxy a proxy 
     *  @return a <code> VaultNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> vaultReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultNotificationSession getVaultNotificationSession(org.osid.authorization.VaultReceiver vaultReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getVaultNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing vault hierarchies. 
     *
     *  @return a <code> VaultHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultHierarchySession getVaultHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getVaultHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing vault hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultHierarchySession getVaultHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getVaultHierarchySession not implemented");
    }


    /**
     *  Gets the session designing vault hierarchies. 
     *
     *  @return a <code> VaultHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultHierarchyDesignSession getVaultHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getVaultHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing vault hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultHierarchyDesignSession getVaultHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getVaultHierarchyDesignSession not implemented");
    }


    /**
     *  Gets an <code> AuthorizationBatchManager. </code> 
     *
     *  @return an <code> AuthorizationBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatch() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchManager getAuthorizationBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationBatchManager not implemented");
    }


    /**
     *  Gets an <code> AuthorizationBatchProxyManager. </code> 
     *
     *  @return an <code> AuthorizationBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatch() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchProxyManager getAuthorizationBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationBatchProxyManager not implemented");
    }


    /**
     *  Gets an <code> AuthorizationRulesManager. </code> 
     *
     *  @return an <code> AuthorizationRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationRules() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationRulesManager getAuthorizationRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationManager.getAuthorizationRulesManager not implemented");
    }


    /**
     *  Gets an <code> AuthorizationRulesProxyManager. </code> 
     *
     *  @return an <code> AuthorizationRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationRules() is false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationRulesProxyManager getAuthorizationRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.AuthorizationProxyManager.getAuthorizationRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.authorizationRecordTypes.clear();
        this.authorizationRecordTypes.clear();

        this.authorizationSearchRecordTypes.clear();
        this.authorizationSearchRecordTypes.clear();

        this.functionRecordTypes.clear();
        this.functionRecordTypes.clear();

        this.functionSearchRecordTypes.clear();
        this.functionSearchRecordTypes.clear();

        this.qualifierRecordTypes.clear();
        this.qualifierRecordTypes.clear();

        this.qualifierSearchRecordTypes.clear();
        this.qualifierSearchRecordTypes.clear();

        this.vaultRecordTypes.clear();
        this.vaultRecordTypes.clear();

        this.vaultSearchRecordTypes.clear();
        this.vaultSearchRecordTypes.clear();

        this.authorizationConditionRecordTypes.clear();
        this.authorizationConditionRecordTypes.clear();

        return;
    }
}
