//
// AbstractAssemblyAuthorizationEnablerQuery.java
//
//     An AuthorizationEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authorization.rules.authorizationenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuthorizationEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyAuthorizationEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.authorization.rules.AuthorizationEnablerQuery,
               org.osid.authorization.rules.AuthorizationEnablerQueryInspector,
               org.osid.authorization.rules.AuthorizationEnablerSearchOrder {

    private final java.util.Collection<org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.rules.records.AuthorizationEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAuthorizationEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuthorizationEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to an authorization. 
     *
     *  @param  authorizationId the authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuthorizationId(org.osid.id.Id authorizationId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRuledAuthorizationIdColumn(), authorizationId, match);
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuthorizationIdTerms() {
        getAssembler().clearTerms(getRuledAuthorizationIdColumn());
        return;
    }


    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuthorizationIdTerms() {
        return (getAssembler().getIdTerms(getRuledAuthorizationIdColumn()));
    }


    /**
     *  Gets the RuledAuthorizationId column name.
     *
     * @return the column name
     */

    protected String getRuledAuthorizationIdColumn() {
        return ("ruled_authorization_id");
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuthorizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getRuledAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuthorizationQuery() is false");
    }


    /**
     *  Matches enablers mapped to any authorization. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          authorization, <code> false </code> to match enablers mapped 
     *          to no authorizations 
     */

    @OSID @Override
    public void matchAnyRuledAuthorization(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledAuthorizationColumn(), match);
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearRuledAuthorizationTerms() {
        getAssembler().clearTerms(getRuledAuthorizationColumn());
        return;
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getRuledAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the RuledAuthorization column name.
     *
     * @return the column name
     */

    protected String getRuledAuthorizationColumn() {
        return ("ruled_authorization");
    }


    /**
     *  Matches enablers mapped to the vault. 
     *
     *  @param  vaultId the vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        getAssembler().addIdTerm(getVaultIdColumn(), vaultId, match);
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        getAssembler().clearTerms(getVaultIdColumn());
        return;
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (getAssembler().getIdTerms(getVaultIdColumn()));
    }


    /**
     *  Gets the VaultId column name.
     *
     * @return the column name
     */

    protected String getVaultIdColumn() {
        return ("vault_id");
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        getAssembler().clearTerms(getVaultColumn());
        return;
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }


    /**
     *  Gets the Vault column name.
     *
     * @return the column name
     */

    protected String getVaultColumn() {
        return ("vault");
    }


    /**
     *  Tests if this authorizationEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  authorizationEnablerRecordType an authorization enabler record type 
     *  @return <code>true</code> if the authorizationEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type authorizationEnablerRecordType) {
        for (org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(authorizationEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  authorizationEnablerRecordType the authorization enabler record type 
     *  @return the authorization enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord getAuthorizationEnablerQueryRecord(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(authorizationEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  authorizationEnablerRecordType the authorization enabler record type 
     *  @return the authorization enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord getAuthorizationEnablerQueryInspectorRecord(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(authorizationEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param authorizationEnablerRecordType the authorization enabler record type
     *  @return the authorization enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerSearchOrderRecord getAuthorizationEnablerSearchOrderRecord(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.rules.records.AuthorizationEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(authorizationEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this authorization enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param authorizationEnablerQueryRecord the authorization enabler query record
     *  @param authorizationEnablerQueryInspectorRecord the authorization enabler query inspector
     *         record
     *  @param authorizationEnablerSearchOrderRecord the authorization enabler search order record
     *  @param authorizationEnablerRecordType authorization enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerQueryRecord</code>,
     *          <code>authorizationEnablerQueryInspectorRecord</code>,
     *          <code>authorizationEnablerSearchOrderRecord</code> or
     *          <code>authorizationEnablerRecordTypeauthorizationEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addAuthorizationEnablerRecords(org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord authorizationEnablerQueryRecord, 
                                      org.osid.authorization.rules.records.AuthorizationEnablerQueryInspectorRecord authorizationEnablerQueryInspectorRecord, 
                                      org.osid.authorization.rules.records.AuthorizationEnablerSearchOrderRecord authorizationEnablerSearchOrderRecord, 
                                      org.osid.type.Type authorizationEnablerRecordType) {

        addRecordType(authorizationEnablerRecordType);

        nullarg(authorizationEnablerQueryRecord, "authorization enabler query record");
        nullarg(authorizationEnablerQueryInspectorRecord, "authorization enabler query inspector record");
        nullarg(authorizationEnablerSearchOrderRecord, "authorization enabler search odrer record");

        this.queryRecords.add(authorizationEnablerQueryRecord);
        this.queryInspectorRecords.add(authorizationEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(authorizationEnablerSearchOrderRecord);
        
        return;
    }
}
