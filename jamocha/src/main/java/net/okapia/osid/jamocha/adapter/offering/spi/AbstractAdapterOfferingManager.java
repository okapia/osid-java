//
// AbstractOfferingManager.java
//
//     An adapter for a OfferingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OfferingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOfferingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.offering.OfferingManager>
    implements org.osid.offering.OfferingManager {


    /**
     *  Constructs a new {@code AbstractAdapterOfferingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOfferingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOfferingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOfferingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any catalogue federation is exposed. Federation is exposed 
     *  when a specific catalogue may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogues appears as a single catalogue. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a canonical unit lookup service. 
     *
     *  @return <code> true </code> if canonical unit lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitLookup() {
        return (getAdapteeManager().supportsCanonicalUnitLookup());
    }


    /**
     *  Tests for the availability of a canonical unit query service. 
     *
     *  @return <code> true </code> if canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitQuery() {
        return (getAdapteeManager().supportsCanonicalUnitQuery());
    }


    /**
     *  Tests if searching for canonical units is available. 
     *
     *  @return <code> true </code> if canonical unit search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitSearch() {
        return (getAdapteeManager().supportsCanonicalUnitSearch());
    }


    /**
     *  Tests if managing for canonical units is available. 
     *
     *  @return <code> true </code> if a canonical unit adminstrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitAdmin() {
        return (getAdapteeManager().supportsCanonicalUnitAdmin());
    }


    /**
     *  Tests if canonical unit notification is available. 
     *
     *  @return <code> true </code> if canonical unit notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitNotification() {
        return (getAdapteeManager().supportsCanonicalUnitNotification());
    }


    /**
     *  Tests if a canonical unit to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if canonical unit catalogue lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitCatalogue());
    }


    /**
     *  Tests if a canonical unit to catalogue assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if canonical unit catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitCatalogueAssignment() {
        return (getAdapteeManager().supportsCanonicalUnitCatalogueAssignment());
    }


    /**
     *  Tests if a canonical unit smart catalogue session is available. 
     *
     *  @return <code> true </code> if canonical unit smart catalogue is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitSmartCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitSmartCatalogue());
    }


    /**
     *  Tests for the availability of an offering lookup service. 
     *
     *  @return <code> true </code> if offering lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingLookup() {
        return (getAdapteeManager().supportsOfferingLookup());
    }


    /**
     *  Tests for the availability of an offering query service. 
     *
     *  @return <code> true </code> if offering query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingQuery() {
        return (getAdapteeManager().supportsOfferingQuery());
    }


    /**
     *  Tests if searching for offerings is available. 
     *
     *  @return <code> true </code> if offering search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingSearch() {
        return (getAdapteeManager().supportsOfferingSearch());
    }


    /**
     *  Tests if managing for offerings is available. 
     *
     *  @return <code> true </code> if an offering adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingAdmin() {
        return (getAdapteeManager().supportsOfferingAdmin());
    }


    /**
     *  Tests if offering notification is available. 
     *
     *  @return <code> true </code> if offering notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingNotification() {
        return (getAdapteeManager().supportsOfferingNotification());
    }


    /**
     *  Tests if an offering to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if offering catalogue lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingCatalogue() {
        return (getAdapteeManager().supportsOfferingCatalogue());
    }


    /**
     *  Tests if an offering to catalogue assignment session is available. 
     *
     *  @return <code> true </code> if offering catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingCatalogueAssignment() {
        return (getAdapteeManager().supportsOfferingCatalogueAssignment());
    }


    /**
     *  Tests if an offering smart catalogue session is available. 
     *
     *  @return <code> true </code> if offering smart catalogue is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingSmartCatalogue() {
        return (getAdapteeManager().supportsOfferingSmartCatalogue());
    }


    /**
     *  Tests for the availability of a participant lookup service. 
     *
     *  @return <code> true </code> if participant lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantLookup() {
        return (getAdapteeManager().supportsParticipantLookup());
    }


    /**
     *  Tests for the availability of a participant query service. 
     *
     *  @return <code> true </code> if participant query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantQuery() {
        return (getAdapteeManager().supportsParticipantQuery());
    }


    /**
     *  Tests if searching for participants is available. 
     *
     *  @return <code> true </code> if participant search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantSearch() {
        return (getAdapteeManager().supportsParticipantSearch());
    }


    /**
     *  Tests if managing for participants is available. 
     *
     *  @return <code> true </code> if a participant adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantAdmin() {
        return (getAdapteeManager().supportsParticipantAdmin());
    }


    /**
     *  Tests if participant notification is available. 
     *
     *  @return <code> true </code> if participant notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantNotification() {
        return (getAdapteeManager().supportsParticipantNotification());
    }


    /**
     *  Tests if a participant to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if participant catalogue lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantCatalogue() {
        return (getAdapteeManager().supportsParticipantCatalogue());
    }


    /**
     *  Tests if a participant to catalogue assignment session is available. 
     *
     *  @return <code> true </code> if participant catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantCatalogueAssignment() {
        return (getAdapteeManager().supportsParticipantCatalogueAssignment());
    }


    /**
     *  Tests if a participant smart catalogue session is available. 
     *
     *  @return <code> true </code> if participant smart catalogue is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantSmartCatalogue() {
        return (getAdapteeManager().supportsParticipantSmartCatalogue());
    }


    /**
     *  Tests for the availability of an result lookup service. 
     *
     *  @return <code> true </code> if result lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultLookup() {
        return (getAdapteeManager().supportsResultLookup());
    }


    /**
     *  Tests for the availability of an result query service. 
     *
     *  @return <code> true </code> if result query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultQuery() {
        return (getAdapteeManager().supportsResultQuery());
    }


    /**
     *  Tests if searching for results is available. 
     *
     *  @return <code> true </code> if result search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultSearch() {
        return (getAdapteeManager().supportsResultSearch());
    }


    /**
     *  Tests if managing for results is available. 
     *
     *  @return <code> true </code> if an result adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultAdmin() {
        return (getAdapteeManager().supportsResultAdmin());
    }


    /**
     *  Tests if result notification is available. 
     *
     *  @return <code> true </code> if result notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultNotification() {
        return (getAdapteeManager().supportsResultNotification());
    }


    /**
     *  Tests if an result to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if result catalogue lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultCatalogue() {
        return (getAdapteeManager().supportsResultCatalogue());
    }


    /**
     *  Tests if an result to catalogue assignment session is available. 
     *
     *  @return <code> true </code> if result catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultCatalogueAssignment() {
        return (getAdapteeManager().supportsResultCatalogueAssignment());
    }


    /**
     *  Tests if an result smart catalogue session is available. 
     *
     *  @return <code> true </code> if result smart catalogue is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultSmartCatalogue() {
        return (getAdapteeManager().supportsResultSmartCatalogue());
    }


    /**
     *  Tests for the availability of an catalogue lookup service. 
     *
     *  @return <code> true </code> if catalogue lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueLookup() {
        return (getAdapteeManager().supportsCatalogueLookup());
    }


    /**
     *  Tests if querying catalogues is available. 
     *
     *  @return <code> true </code> if catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (getAdapteeManager().supportsCatalogueQuery());
    }


    /**
     *  Tests if searching for catalogues is available. 
     *
     *  @return <code> true </code> if catalogue search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueSearch() {
        return (getAdapteeManager().supportsCatalogueSearch());
    }


    /**
     *  Tests for the availability of a catalogue administrative service for 
     *  creating and deleting catalogues. 
     *
     *  @return <code> true </code> if catalogue administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueAdmin() {
        return (getAdapteeManager().supportsCatalogueAdmin());
    }


    /**
     *  Tests for the availability of a catalogue notification service. 
     *
     *  @return <code> true </code> if catalogue notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueNotification() {
        return (getAdapteeManager().supportsCatalogueNotification());
    }


    /**
     *  Tests for the availability of a catalogue hierarchy traversal service. 
     *
     *  @return <code> true </code> if catalogue hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueHierarchy() {
        return (getAdapteeManager().supportsCatalogueHierarchy());
    }


    /**
     *  Tests for the availability of a catalogue hierarchy design service. 
     *
     *  @return <code> true </code> if catalogue hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueHierarchyDesign() {
        return (getAdapteeManager().supportsCatalogueHierarchyDesign());
    }


    /**
     *  Tests for the availability of a offering batch service. 
     *
     *  @return <code> true </code> if a offering batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingBatch() {
        return (getAdapteeManager().supportsOfferingBatch());
    }


    /**
     *  Tests for the availability of a offering rules service. 
     *
     *  @return <code> true </code> if a offering rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingRules() {
        return (getAdapteeManager().supportsOfferingRules());
    }


    /**
     *  Gets the supported <code> CanonicalUnit </code> record types. 
     *
     *  @return a list containing the supported canonical unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitRecordTypes());
    }


    /**
     *  Tests if the given <code> CanonicalUnit </code> record type is 
     *  supported. 
     *
     *  @param  canonicalUnitRecordType a <code> Type </code> indicating a 
     *          <code> CanonicalUnit </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitRecordType(org.osid.type.Type canonicalUnitRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitRecordType(canonicalUnitRecordType));
    }


    /**
     *  Gets the supported canonical unit search record types. 
     *
     *  @return a list containing the supported canonical unit search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitSearchRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitSearchRecordTypes());
    }


    /**
     *  Tests if the given canonical unit search record type is supported. 
     *
     *  @param  canonicalUnitSearchRecordType a <code> Type </code> indicating 
     *          a canonical unit record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitSearchRecordType(org.osid.type.Type canonicalUnitSearchRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitSearchRecordType(canonicalUnitSearchRecordType));
    }


    /**
     *  Gets the supported <code> Offering </code> record types. 
     *
     *  @return a list containing the supported offering record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingRecordTypes() {
        return (getAdapteeManager().getOfferingRecordTypes());
    }


    /**
     *  Tests if the given <code> Offering </code> record type is supported. 
     *
     *  @param  offeringRecordType a <code> Type </code> indicating an <code> 
     *          Offering </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> offeringRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingRecordType(org.osid.type.Type offeringRecordType) {
        return (getAdapteeManager().supportsOfferingRecordType(offeringRecordType));
    }


    /**
     *  Gets the supported offering search record types. 
     *
     *  @return a list containing the supported offering search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingSearchRecordTypes() {
        return (getAdapteeManager().getOfferingSearchRecordTypes());
    }


    /**
     *  Tests if the given offering search record type is supported. 
     *
     *  @param  offeringSearchRecordType a <code> Type </code> indicating an 
     *          offering record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> offeringSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingSearchRecordType(org.osid.type.Type offeringSearchRecordType) {
        return (getAdapteeManager().supportsOfferingSearchRecordType(offeringSearchRecordType));
    }


    /**
     *  Gets the supported <code> Participant </code> record types. 
     *
     *  @return a list containing the supported participant record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParticipantRecordTypes() {
        return (getAdapteeManager().getParticipantRecordTypes());
    }


    /**
     *  Tests if the given <code> Participant </code> record type is 
     *  supported. 
     *
     *  @param  participantRecordType a <code> Type </code> indicating a 
     *          <code> Participant </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> participantRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParticipantRecordType(org.osid.type.Type participantRecordType) {
        return (getAdapteeManager().supportsParticipantRecordType(participantRecordType));
    }


    /**
     *  Gets the supported participant search record types. 
     *
     *  @return a list containing the supported participant search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParticipantSearchRecordTypes() {
        return (getAdapteeManager().getParticipantSearchRecordTypes());
    }


    /**
     *  Tests if the given participant search record type is supported. 
     *
     *  @param  participantSearchRecordType a <code> Type </code> indicating a 
     *          participant record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          participantSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParticipantSearchRecordType(org.osid.type.Type participantSearchRecordType) {
        return (getAdapteeManager().supportsParticipantSearchRecordType(participantSearchRecordType));
    }


    /**
     *  Gets the supported <code> Result </code> record types. 
     *
     *  @return a list containing the supported result record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResultRecordTypes() {
        return (getAdapteeManager().getResultRecordTypes());
    }


    /**
     *  Tests if the given <code> Result </code> record type is supported. 
     *
     *  @param  resultRecordType a <code> Type </code> indicating an <code> 
     *          Result </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resultRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResultRecordType(org.osid.type.Type resultRecordType) {
        return (getAdapteeManager().supportsResultRecordType(resultRecordType));
    }


    /**
     *  Gets the supported result search record types. 
     *
     *  @return a list containing the supported result search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResultSearchRecordTypes() {
        return (getAdapteeManager().getResultSearchRecordTypes());
    }


    /**
     *  Tests if the given result search record type is supported. 
     *
     *  @param  resultSearchRecordType a <code> Type </code> indicating an 
     *          result record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resultSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResultSearchRecordType(org.osid.type.Type resultSearchRecordType) {
        return (getAdapteeManager().supportsResultSearchRecordType(resultSearchRecordType));
    }


    /**
     *  Gets the supported <code> Catalogue </code> record types. 
     *
     *  @return a list containing the supported catalogue record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogueRecordTypes() {
        return (getAdapteeManager().getCatalogueRecordTypes());
    }


    /**
     *  Tests if the given <code> Catalogue </code> record type is supported. 
     *
     *  @param  catalogueRecordType a <code> Type </code> indicating a <code> 
     *          Catalogue </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogueRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogueRecordType(org.osid.type.Type catalogueRecordType) {
        return (getAdapteeManager().supportsCatalogueRecordType(catalogueRecordType));
    }


    /**
     *  Gets the supported catalogue search record types. 
     *
     *  @return a list containing the supported catalogue search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogueSearchRecordTypes() {
        return (getAdapteeManager().getCatalogueSearchRecordTypes());
    }


    /**
     *  Tests if the given catalogue search record type is supported. 
     *
     *  @param  catalogueSearchRecordType a <code> Type </code> indicating a 
     *          catalogue record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          catalogueSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogueSearchRecordType(org.osid.type.Type catalogueSearchRecordType) {
        return (getAdapteeManager().supportsCatalogueSearchRecordType(catalogueSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  lookup service. 
     *
     *  @return a <code> CanonicalUnitLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitLookupSession getCanonicalUnitLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitLookupSession getCanonicalUnitLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitLookupSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  query service. 
     *
     *  @return a <code> CanonicalUnitQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuerySession getCanonicalUnitQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuerySession getCanonicalUnitQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitQuerySessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  search service. 
     *
     *  @return a <code> CanonicalUnitSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSearchSession getCanonicalUnitSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSearchSession getCanonicalUnitSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitSearchSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  administration service. 
     *
     *  @return a <code> CanonicalUnitAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitAdminSession getCanonicalUnitAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitAdminSession getCanonicalUnitAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  notification service. 
     *
     *  @param  canonicalUnitReceiver the receiver 
     *  @return a <code> CanonicalUnitNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitNotificationSession getCanonicalUnitNotificationSession(org.osid.offering.CanonicalUnitReceiver canonicalUnitReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitNotificationSession(canonicalUnitReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  notification service for the given catalogue. 
     *
     *  @param  canonicalUnitReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitReceiver 
     *          </code> or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitNotificationSession getCanonicalUnitNotificationSessionForCatalogue(org.osid.offering.CanonicalUnitReceiver canonicalUnitReceiver, 
                                                                                                              org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitNotificationSessionForCatalogue(canonicalUnitReceiver, catalogueId));
    }


    /**
     *  Gets the session for retrieving canonical unit to catalogue mappings. 
     *
     *  @return a <code> CanonicalUnitCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitCatalogueSession getCanonicalUnitCatalogueSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitCatalogueSession());
    }


    /**
     *  Gets the session for assigning canonical unit to catalogue mappings. 
     *
     *  @return a <code> CanonicalUnitCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitCatalogueAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitCatalogueAssignmentSession getCanonicalUnitCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitCatalogueAssignmentSession());
    }


    /**
     *  Gets the session associated with the canonical unit smart catalogue 
     *  for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return a <code> CanonicalUnitSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSmartCatalogueSession getCanonicalUnitSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitSmartCatalogueSession(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  lookup service. 
     *
     *  @return an <code> OfferingLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingLookupSession getOfferingLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingLookupSession getOfferingLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingLookupSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering query 
     *  service. 
     *
     *  @return an <code> OfferingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuerySession getOfferingQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering query 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuerySession getOfferingQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingQuerySessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  search service. 
     *
     *  @return an <code> OfferingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchSession getOfferingSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchSession getOfferingSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingSearchSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  administration service. 
     *
     *  @return an <code> OfferingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingAdminSession getOfferingAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingAdminSession getOfferingAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  notification service. 
     *
     *  @param  offeringReceiver the receiver 
     *  @return an <code> OfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> offeringReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingNotificationSession getOfferingNotificationSession(org.osid.offering.OfferingReceiver offeringReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingNotificationSession(offeringReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  notification service for the given catalogue. 
     *
     *  @param  offeringReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> offeringReceiver </code> 
     *          or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingNotificationSession getOfferingNotificationSessionForCatalogue(org.osid.offering.OfferingReceiver offeringReceiver, 
                                                                                                    org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingNotificationSessionForCatalogue(offeringReceiver, catalogueId));
    }


    /**
     *  Gets the session for retrieving offering to catalogue mappings. 
     *
     *  @return an <code> OfferingCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingCatalogueSession getOfferingCatalogueSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingCatalogueSession());
    }


    /**
     *  Gets the session for assigning offering to catalogue mappings. 
     *
     *  @return an <code> OfferingCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingCatalogueAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingCatalogueAssignmentSession getOfferingCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingCatalogueAssignmentSession());
    }


    /**
     *  Gets the session associated with the offering smart catalogue for the 
     *  given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return an <code> OfferingSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSmartCatalogueSession getOfferingSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingSmartCatalogueSession(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  lookup service. 
     *
     *  @return a <code> ParticipantLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantLookupSession getParticipantLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantLookupSession getParticipantLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantLookupSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  query service. 
     *
     *  @return a <code> ParticipantQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuerySession getParticipantQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuerySession getParticipantQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantQuerySessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  search service. 
     *
     *  @return a <code> ParticipantSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchSession getParticipantSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchSession getParticipantSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantSearchSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  administration service. 
     *
     *  @return a <code> ParticipantAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantAdminSession getParticipantAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantAdminSession getParticipantAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  notification service. 
     *
     *  @param  participantReceiver the receiver 
     *  @return a <code> ParticipantNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> participantReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantNotificationSession getParticipantNotificationSession(org.osid.offering.ParticipantReceiver participantReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantNotificationSession(participantReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  notification service for the given catalogue. 
     *
     *  @param  participantReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> participantReceiver 
     *          </code> or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantNotificationSession getParticipantNotificationSessionForCatalogue(org.osid.offering.ParticipantReceiver participantReceiver, 
                                                                                                          org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantNotificationSessionForCatalogue(participantReceiver, catalogueId));
    }


    /**
     *  Gets the session for retrieving participant to catalogue mappings. 
     *
     *  @return a <code> ParticipantCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantCatalogueSession getParticipantCatalogueSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantCatalogueSession());
    }


    /**
     *  Gets the session for assigning participant to catalogue mappings. 
     *
     *  @return a <code> ParticipantCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantCatalogueAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantCatalogueAssignmentSession getParticipantCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantCatalogueAssignmentSession());
    }


    /**
     *  Gets the session associated with the participant smart catalogue for 
     *  the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return a <code> ParticipantSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSmartCatalogueSession getParticipantSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParticipantSmartCatalogueSession(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result lookup 
     *  service. 
     *
     *  @return an <code> ResultLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultLookupSession getResultLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result lookup 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultLookupSession getResultLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResultLookupSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result query 
     *  service. 
     *
     *  @return an <code> ResultQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuerySession getResultQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result query 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuerySession getResultQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResultQuerySessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result search 
     *  service. 
     *
     *  @return an <code> ResultSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSearchSession getResultSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result search 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSearchSession getResultSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResultSearchSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  administration service. 
     *
     *  @return an <code> ResultAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultAdminSession getResultAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultAdminSession getResultAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResultAdminSessionForCatalogue(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  notification service. 
     *
     *  @param  resultReceiver the receiver 
     *  @return an <code> ResultNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resultReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultNotificationSession getResultNotificationSession(org.osid.offering.ResultReceiver resultReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultNotificationSession(resultReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  notification service for the given catalogue. 
     *
     *  @param  resultReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> resultReceiver </code> 
     *          or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultNotificationSession getResultNotificationSessionForCatalogue(org.osid.offering.ResultReceiver resultReceiver, 
                                                                                                org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResultNotificationSessionForCatalogue(resultReceiver, catalogueId));
    }


    /**
     *  Gets the session for retrieving result to catalogue mappings. 
     *
     *  @return an <code> ResultCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultCatalogueSession getResultCatalogueSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultCatalogueSession());
    }


    /**
     *  Gets the session for assigning result to catalogue mappings. 
     *
     *  @return an <code> ResultCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultCatalogueAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultCatalogueAssignmentSession getResultCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResultCatalogueAssignmentSession());
    }


    /**
     *  Gets the session associated with the result smart catalogue for the 
     *  given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return an <code> ResultSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultSmartCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSmartCatalogueSession getResultSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResultSmartCatalogueSession(catalogueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  lookup service. 
     *
     *  @return a <code> CatalogueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueLookupSession getCatalogueLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  query service. 
     *
     *  @return a <code> CatalogueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuerySession getCatalogueQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  search service. 
     *
     *  @return a <code> CatalogueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueSearchSession getCatalogueSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  administrative service. 
     *
     *  @return a <code> CatalogueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueAdminSession getCatalogueAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  notification service. 
     *
     *  @param  catalogueReceiver the receiver 
     *  @return a <code> CatalogueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueNotificationSession getCatalogueNotificationSession(org.osid.offering.CatalogueReceiver catalogueReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueNotificationSession(catalogueReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  hierarchy service. 
     *
     *  @return a <code> CatalogueHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueHierarchySession getCatalogueHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  hierarchy design service. 
     *
     *  @return a <code> CatalogueHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueHierarchyDesignSession getCatalogueHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogueHierarchyDesignSession());
    }


    /**
     *  Gets a <code> OfferingBatchManager. </code> 
     *
     *  @return a <code> OfferingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.OfferingBatchManager getOfferingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingBatchManager());
    }


    /**
     *  Gets a <code> OfferingRulesManager. </code> 
     *
     *  @return a <code> OfferingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingRulesManager getOfferingRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
