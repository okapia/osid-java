//
// MutableMapRenovationLookupSession
//
//    Implements a Renovation lookup service backed by a collection of
//    renovations that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.construction;


/**
 *  Implements a Renovation lookup service backed by a collection of
 *  renovations. The renovations are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of renovations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapRenovationLookupSession
    extends net.okapia.osid.jamocha.core.room.construction.spi.AbstractMapRenovationLookupSession
    implements org.osid.room.construction.RenovationLookupSession {


    /**
     *  Constructs a new {@code MutableMapRenovationLookupSession}
     *  with no renovations.
     *
     *  @param campus the campus
     *  @throws org.osid.NullArgumentException {@code campus} is
     *          {@code null}
     */

      public MutableMapRenovationLookupSession(org.osid.room.Campus campus) {
        setCampus(campus);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRenovationLookupSession} with a
     *  single renovation.
     *
     *  @param campus the campus  
     *  @param renovation a renovation
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code renovation} is {@code null}
     */

    public MutableMapRenovationLookupSession(org.osid.room.Campus campus,
                                           org.osid.room.construction.Renovation renovation) {
        this(campus);
        putRenovation(renovation);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRenovationLookupSession}
     *  using an array of renovations.
     *
     *  @param campus the campus
     *  @param renovations an array of renovations
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code renovations} is {@code null}
     */

    public MutableMapRenovationLookupSession(org.osid.room.Campus campus,
                                           org.osid.room.construction.Renovation[] renovations) {
        this(campus);
        putRenovations(renovations);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRenovationLookupSession}
     *  using a collection of renovations.
     *
     *  @param campus the campus
     *  @param renovations a collection of renovations
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code renovations} is {@code null}
     */

    public MutableMapRenovationLookupSession(org.osid.room.Campus campus,
                                           java.util.Collection<? extends org.osid.room.construction.Renovation> renovations) {

        this(campus);
        putRenovations(renovations);
        return;
    }

    
    /**
     *  Makes a {@code Renovation} available in this session.
     *
     *  @param renovation a renovation
     *  @throws org.osid.NullArgumentException {@code renovation{@code  is
     *          {@code null}
     */

    @Override
    public void putRenovation(org.osid.room.construction.Renovation renovation) {
        super.putRenovation(renovation);
        return;
    }


    /**
     *  Makes an array of renovations available in this session.
     *
     *  @param renovations an array of renovations
     *  @throws org.osid.NullArgumentException {@code renovations{@code 
     *          is {@code null}
     */

    @Override
    public void putRenovations(org.osid.room.construction.Renovation[] renovations) {
        super.putRenovations(renovations);
        return;
    }


    /**
     *  Makes collection of renovations available in this session.
     *
     *  @param renovations a collection of renovations
     *  @throws org.osid.NullArgumentException {@code renovations{@code  is
     *          {@code null}
     */

    @Override
    public void putRenovations(java.util.Collection<? extends org.osid.room.construction.Renovation> renovations) {
        super.putRenovations(renovations);
        return;
    }


    /**
     *  Removes a Renovation from this session.
     *
     *  @param renovationId the {@code Id} of the renovation
     *  @throws org.osid.NullArgumentException {@code renovationId{@code 
     *          is {@code null}
     */

    @Override
    public void removeRenovation(org.osid.id.Id renovationId) {
        super.removeRenovation(renovationId);
        return;
    }    
}
