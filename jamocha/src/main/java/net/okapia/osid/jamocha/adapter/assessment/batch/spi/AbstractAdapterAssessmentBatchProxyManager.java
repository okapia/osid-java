//
// AbstractAssessmentBatchProxyManager.java
//
//     An adapter for a AssessmentBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AssessmentBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAssessmentBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.assessment.batch.AssessmentBatchProxyManager>
    implements org.osid.assessment.batch.AssessmentBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAssessmentBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of items is available. 
     *
     *  @return <code> true </code> if an item bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBatchAdmin() {
        return (getAdapteeManager().supportsItemBatchAdmin());
    }


    /**
     *  Tests if bulk administration of assessments is available. 
     *
     *  @return <code> true </code> if an assessment bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentBatchAdmin() {
        return (getAdapteeManager().supportsAssessmentBatchAdmin());
    }


    /**
     *  Tests if bulk administration of assessments offered is available. 
     *
     *  @return <code> true </code> if an assessment offered bulk 
     *          administrative service is available, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedBatchAdmin() {
        return (getAdapteeManager().supportsAssessmentOfferedBatchAdmin());
    }


    /**
     *  Tests if bulk administration of assessments taken is available. 
     *
     *  @return <code> true </code> if anassessment taken bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenBatchAdmin() {
        return (getAdapteeManager().supportsAssessmentTakenBatchAdmin());
    }


    /**
     *  Tests if bulk administration of banks is available. 
     *
     *  @return <code> true </code> if a bank bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankBatchAdmin() {
        return (getAdapteeManager().supportsBankBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.ItemBatchAdminSession getItemBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.ItemBatchAdminSession getItemBatchAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentBatchAdminSession getAssessmentBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentBatchAdminSession getAssessmentBatchAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentBatchAdminSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment offered administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentOfferedBatchAdminSession getAssessmentOfferedBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment offered administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentOfferedBatchAdminSession getAssessmentOfferedBatchAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedBatchAdminSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment taken administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentTakenBatchAdminSession getAssessmentTakenBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  assessment taken administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentTakenBatchAdminSession getAssessmentTakenBatchAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenBatchAdminSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk bank 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BankBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBankBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.BankBatchAdminSession getBankBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
