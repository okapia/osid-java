//
// AbstractMapStepProcessorLookupSession
//
//    A simple framework for providing a StepProcessor lookup service
//    backed by a fixed collection of step processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a StepProcessor lookup service backed by a
 *  fixed collection of step processors. The step processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>StepProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapStepProcessorLookupSession
    extends net.okapia.osid.jamocha.workflow.rules.spi.AbstractStepProcessorLookupSession
    implements org.osid.workflow.rules.StepProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.workflow.rules.StepProcessor> stepProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.workflow.rules.StepProcessor>());


    /**
     *  Makes a <code>StepProcessor</code> available in this session.
     *
     *  @param  stepProcessor a step processor
     *  @throws org.osid.NullArgumentException <code>stepProcessor<code>
     *          is <code>null</code>
     */

    protected void putStepProcessor(org.osid.workflow.rules.StepProcessor stepProcessor) {
        this.stepProcessors.put(stepProcessor.getId(), stepProcessor);
        return;
    }


    /**
     *  Makes an array of step processors available in this session.
     *
     *  @param  stepProcessors an array of step processors
     *  @throws org.osid.NullArgumentException <code>stepProcessors<code>
     *          is <code>null</code>
     */

    protected void putStepProcessors(org.osid.workflow.rules.StepProcessor[] stepProcessors) {
        putStepProcessors(java.util.Arrays.asList(stepProcessors));
        return;
    }


    /**
     *  Makes a collection of step processors available in this session.
     *
     *  @param  stepProcessors a collection of step processors
     *  @throws org.osid.NullArgumentException <code>stepProcessors<code>
     *          is <code>null</code>
     */

    protected void putStepProcessors(java.util.Collection<? extends org.osid.workflow.rules.StepProcessor> stepProcessors) {
        for (org.osid.workflow.rules.StepProcessor stepProcessor : stepProcessors) {
            this.stepProcessors.put(stepProcessor.getId(), stepProcessor);
        }

        return;
    }


    /**
     *  Removes a StepProcessor from this session.
     *
     *  @param  stepProcessorId the <code>Id</code> of the step processor
     *  @throws org.osid.NullArgumentException <code>stepProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeStepProcessor(org.osid.id.Id stepProcessorId) {
        this.stepProcessors.remove(stepProcessorId);
        return;
    }


    /**
     *  Gets the <code>StepProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  stepProcessorId <code>Id</code> of the <code>StepProcessor</code>
     *  @return the stepProcessor
     *  @throws org.osid.NotFoundException <code>stepProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>stepProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessor getStepProcessor(org.osid.id.Id stepProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.workflow.rules.StepProcessor stepProcessor = this.stepProcessors.get(stepProcessorId);
        if (stepProcessor == null) {
            throw new org.osid.NotFoundException("stepProcessor not found: " + stepProcessorId);
        }

        return (stepProcessor);
    }


    /**
     *  Gets all <code>StepProcessors</code>. In plenary mode, the returned
     *  list contains all known stepProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  stepProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>StepProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepprocessor.ArrayStepProcessorList(this.stepProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepProcessors.clear();
        super.close();
        return;
    }
}
