//
// SummaryMiter.java
//
//     Defines a Summary miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.summary;


/**
 *  Defines a <code>Summary</code> miter for use with the builders.
 */

public interface SummaryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidCompendiumMiter,
            org.osid.billing.payment.Summary {


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public void setCustomer(org.osid.billing.Customer customer);


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException <code>period</code>
     *          is <code>null</code>
     */

    public void setPeriod(org.osid.billing.Period period);


    /**
     *  Sets the credit balance flag.
     *
     *  @param credit <code>true</code> if a credit,
     *         <code>false</code> if a debit
     */

    public void setCreditBalance(boolean credit);


    /**
     *  Sets the balance.
     * 
     *  @param balance the balance amount
     *  @throws org.osid.NullArgumentException <code>balance</code> is
     *          <code>null</code>
     */

    public void setBalance(org.osid.financials.Currency balance);


    /**
     *  Sets the last payment date.
     *
     *  @param date the last payment date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setLastPaymentDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the last payment amount.
     *
     *  @param amount the last payment amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setLastPaymentAmount(org.osid.financials.Currency amount);


    /**
     *  Sets the due date.
     *
     *  @param date due date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    public void setPaymentDueDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a Summary record.
     *
     *  @param record a summary record
     *  @param recordType the type of summary record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSummaryRecord(org.osid.billing.payment.records.SummaryRecord record, org.osid.type.Type recordType);
}       


