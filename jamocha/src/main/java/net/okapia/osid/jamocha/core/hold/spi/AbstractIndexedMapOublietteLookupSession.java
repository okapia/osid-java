//
// AbstractIndexedMapOublietteLookupSession.java
//
//    A simple framework for providing an Oubliette lookup service
//    backed by a fixed collection of oubliettes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Oubliette lookup service backed by a
 *  fixed collection of oubliettes. The oubliettes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some oubliettes may be compatible
 *  with more types than are indicated through these oubliette
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Oubliettes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOublietteLookupSession
    extends AbstractMapOublietteLookupSession
    implements org.osid.hold.OublietteLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.hold.Oubliette> oubliettesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.Oubliette>());
    private final MultiMap<org.osid.type.Type, org.osid.hold.Oubliette> oubliettesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.Oubliette>());


    /**
     *  Makes an <code>Oubliette</code> available in this session.
     *
     *  @param  oubliette an oubliette
     *  @throws org.osid.NullArgumentException <code>oubliette<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOubliette(org.osid.hold.Oubliette oubliette) {
        super.putOubliette(oubliette);

        this.oubliettesByGenus.put(oubliette.getGenusType(), oubliette);
        
        try (org.osid.type.TypeList types = oubliette.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.oubliettesByRecord.put(types.getNextType(), oubliette);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an oubliette from this session.
     *
     *  @param oublietteId the <code>Id</code> of the oubliette
     *  @throws org.osid.NullArgumentException <code>oublietteId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOubliette(org.osid.id.Id oublietteId) {
        org.osid.hold.Oubliette oubliette;
        try {
            oubliette = getOubliette(oublietteId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.oubliettesByGenus.remove(oubliette.getGenusType());

        try (org.osid.type.TypeList types = oubliette.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.oubliettesByRecord.remove(types.getNextType(), oubliette);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOubliette(oublietteId);
        return;
    }


    /**
     *  Gets an <code>OublietteList</code> corresponding to the given
     *  oubliette genus <code>Type</code> which does not include
     *  oubliettes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known oubliettes or an error results. Otherwise,
     *  the returned list may contain only those oubliettes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  oublietteGenusType an oubliette genus type 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByGenusType(org.osid.type.Type oublietteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.oubliette.ArrayOublietteList(this.oubliettesByGenus.get(oublietteGenusType)));
    }


    /**
     *  Gets an <code>OublietteList</code> containing the given
     *  oubliette record <code>Type</code>. In plenary mode, the
     *  returned list contains all known oubliettes or an error
     *  results. Otherwise, the returned list may contain only those
     *  oubliettes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  oublietteRecordType an oubliette record type 
     *  @return the returned <code>oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByRecordType(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.oubliette.ArrayOublietteList(this.oubliettesByRecord.get(oublietteRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.oubliettesByGenus.clear();
        this.oubliettesByRecord.clear();

        super.close();

        return;
    }
}
