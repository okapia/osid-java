//
// AbstractJobProcessorEnablerLookupSession.java
//
//    A starter implementation framework for providing a JobProcessorEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a JobProcessorEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getJobProcessorEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractJobProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>JobProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJobProcessorEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>JobProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobProcessorEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>JobProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobProcessorEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job processor enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active job processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobProcessorEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive job processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobProcessorEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>JobProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JobProcessorEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>JobProcessorEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param  jobProcessorEnablerId <code>Id</code> of the
     *          <code>JobProcessorEnabler</code>
     *  @return the job processor enabler
     *  @throws org.osid.NotFoundException <code>jobProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnabler getJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.rules.JobProcessorEnablerList jobProcessorEnablers = getJobProcessorEnablers()) {
            while (jobProcessorEnablers.hasNext()) {
                org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler = jobProcessorEnablers.getNextJobProcessorEnabler();
                if (jobProcessorEnabler.getId().equals(jobProcessorEnablerId)) {
                    return (jobProcessorEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(jobProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobProcessorEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>JobProcessorEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getJobProcessorEnablers()</code>.
     *
     *  @param  jobProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByIds(org.osid.id.IdList jobProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.rules.JobProcessorEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = jobProcessorEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getJobProcessorEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("job processor enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.LinkedJobProcessorEnablerList(ret));
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> corresponding to the given
     *  job processor enabler genus <code>Type</code> which does not include
     *  job processor enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getJobProcessorEnablers()</code>.
     *
     *  @param  jobProcessorEnablerGenusType a jobProcessorEnabler genus type 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByGenusType(org.osid.type.Type jobProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessorenabler.JobProcessorEnablerGenusFilterList(getJobProcessorEnablers(), jobProcessorEnablerGenusType));
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> corresponding to the given
     *  job processor enabler genus <code>Type</code> and include any additional
     *  job processor enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJobProcessorEnablers()</code>.
     *
     *  @param  jobProcessorEnablerGenusType a jobProcessorEnabler genus type 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByParentGenusType(org.osid.type.Type jobProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getJobProcessorEnablersByGenusType(jobProcessorEnablerGenusType));
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> containing the given
     *  job processor enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJobProcessorEnablers()</code>.
     *
     *  @param  jobProcessorEnablerRecordType a jobProcessorEnabler record type 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByRecordType(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessorenabler.JobProcessorEnablerRecordFilterList(getJobProcessorEnablers(), jobProcessorEnablerRecordType));
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, job processor enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive job processor enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>JobProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessorenabler.TemporalJobProcessorEnablerFilterList(getJobProcessorEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>JobProcessorEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive job processor enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getJobProcessorEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>JobProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @return a list of <code>JobProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the job processor enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of job processor enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.rules.JobProcessorEnablerList filterJobProcessorEnablersOnViews(org.osid.resourcing.rules.JobProcessorEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.resourcing.rules.JobProcessorEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessorenabler.ActiveJobProcessorEnablerFilterList(ret);
        }

        return (ret);
    }
}
