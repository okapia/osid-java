//
// AbstractSourceable.java
//
//     Defines a Sourceable.
//
//
// Tom Coppeto
// Okapia
// 5 October 2011
//
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a Sourceable.  <code>setProvider()</code> should be used
 *  to set the provider information by subclasses.
 */

public abstract class AbstractSourceable
    implements org.osid.Sourceable {

    private org.osid.resource.Resource provider;
    private boolean brandingSet = false;
    private org.osid.locale.DisplayText license = new Plain("");
    private final java.util.Collection<org.osid.repository.Asset> branding = java.util.Collections.synchronizedList(new java.util.ArrayList<org.osid.repository.Asset>());
    

    /**
     *  Gets the <code> Id </code> of the <code> Provider </code> of this 
     *  <code> Catalog. </code> 
     *
     *  @return the <code> Provider Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProviderId() {
        return (this.provider.getId());
    }


    /**
     *  Gets the <code> Resource </code> representing the provider of this 
     *  catalog. 
     *
     *  @return the provider 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getProvider()
        throws org.osid.OperationFailedException {
        
        return (this.provider);
    }


    /**
     *  Sets the provider for this catalog.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected void setProvider(net.okapia.osid.provider.Provider provider) {
        nullarg(provider, "provider");
        setProvider(provider.getResource());
        setBranding(provider.getBranding());
        return;
    }


    /**
     *  Sets the provider.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected void setProvider(org.osid.resource.Resource provider) {
        nullarg(provider, "provider");
        this.provider = provider;
        return;
    }


    /**
     *  Gets the branding asset Ids, such as an image or logo,
     *  expressed using the <code> Asset </code> interface.
     *
     *  @return a list of asset Ids
     */

    @OSID @Override
    public org.osid.id.IdList getBrandingIds() {
        try {
            org.osid.repository.AssetList assets = getBranding();
            return (new net.okapia.osid.jamocha.adapter.converter.repository.asset.AssetToIdList(assets));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a branding, such as an image or logo, expressed using the <code> 
     *  Asset </code> interface. 
     *
     *  @return a list of assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getBranding()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.branding));
    }


    /**
     *  Adds an asset to the branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected void addAssetToBranding(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");

        if (!this.brandingSet) {
            this.branding.clear();
            this.brandingSet = true;
        }

        this.branding.add(asset);
        return;
    }


    /**
     *  Adds assets to the branding.
     *
     *  @param assets array of assets to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected void addAssetsToBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        nullarg(assets, "assets");

        if (!this.brandingSet) {
            this.branding.clear();
            this.brandingSet = true;
        }

        this.branding.addAll(assets);
        return;
    }


    /**
     *  Sets the branding to the given asset.
     *
     *  @param asset an asset to set
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected void setBranding(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");

        this.branding.clear();
        this.branding.add(asset);

        return;
    }


    /**
     *  Sets the branding to the given assets.
     *
     *  @param assets an array of assets to set
     *  @throws org.osid.NullArgumentException <code>assets</code>
     *          is <code>null</code>
     */

    protected void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        nullarg(assets, "assets");

        this.branding.clear();
        this.branding.addAll(assets);

        return;
    }


    /**
     *  Gets the terms of usage. An empty license means the terms are
     *  unknown.
     *
     *  @return the license 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLicense() {
        return (this.license);
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code> is
     *          <code>null</code>
     */

    protected void setLicense(org.osid.locale.DisplayText license) {
        nullarg(license, "license");
        this.license = license;
        return;
    }
}
