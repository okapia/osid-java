//
// AbstractAdapterStepProcessorLookupSession.java
//
//    A StepProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A StepProcessor lookup session adapter.
 */

public abstract class AbstractAdapterStepProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.rules.StepProcessorLookupSession {

    private final org.osid.workflow.rules.StepProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStepProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStepProcessorLookupSession(org.osid.workflow.rules.StepProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Office/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Office Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@code Office} associated with this session.
     *
     *  @return the {@code Office} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@code StepProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupStepProcessors() {
        return (this.session.canLookupStepProcessors());
    }


    /**
     *  A complete view of the {@code StepProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepProcessorView() {
        this.session.useComparativeStepProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code StepProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepProcessorView() {
        this.session.usePlenaryStepProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step processors in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active step processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepProcessorView() {
        this.session.useActiveStepProcessorView();
        return;
    }


    /**
     *  Active and inactive step processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepProcessorView() {
        this.session.useAnyStatusStepProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code StepProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code StepProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code StepProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param stepProcessorId {@code Id} of the {@code StepProcessor}
     *  @return the step processor
     *  @throws org.osid.NotFoundException {@code stepProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code stepProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessor getStepProcessor(org.osid.id.Id stepProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepProcessor(stepProcessorId));
    }


    /**
     *  Gets a {@code StepProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stepProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code StepProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param  stepProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code StepProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code stepProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByIds(org.osid.id.IdList stepProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepProcessorsByIds(stepProcessorIds));
    }


    /**
     *  Gets a {@code StepProcessorList} corresponding to the given
     *  step processor genus {@code Type} which does not include
     *  step processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned {@code StepProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepProcessorsByGenusType(stepProcessorGenusType));
    }


    /**
     *  Gets a {@code StepProcessorList} corresponding to the given
     *  step processor genus {@code Type} and include any additional
     *  step processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned {@code StepProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByParentGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepProcessorsByParentGenusType(stepProcessorGenusType));
    }


    /**
     *  Gets a {@code StepProcessorList} containing the given
     *  step processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param  stepProcessorRecordType a stepProcessor record type 
     *  @return the returned {@code StepProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByRecordType(org.osid.type.Type stepProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepProcessorsByRecordType(stepProcessorRecordType));
    }


    /**
     *  Gets all {@code StepProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @return a list of {@code StepProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepProcessors());
    }
}
