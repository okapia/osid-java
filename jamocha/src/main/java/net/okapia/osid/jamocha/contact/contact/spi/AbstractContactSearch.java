//
// AbstractContactSearch.java
//
//     A template for making a Contact Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.contact.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing contact searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractContactSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.contact.ContactSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.contact.records.ContactSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.contact.ContactSearchOrder contactSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of contacts. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  contactIds list of contacts
     *  @throws org.osid.NullArgumentException
     *          <code>contactIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongContacts(org.osid.id.IdList contactIds) {
        while (contactIds.hasNext()) {
            try {
                this.ids.add(contactIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongContacts</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of contact Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getContactIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  contactSearchOrder contact search order 
     *  @throws org.osid.NullArgumentException
     *          <code>contactSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>contactSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderContactResults(org.osid.contact.ContactSearchOrder contactSearchOrder) {
	this.contactSearchOrder = contactSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.contact.ContactSearchOrder getContactSearchOrder() {
	return (this.contactSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given contact search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a contact implementing the requested record.
     *
     *  @param contactSearchRecordType a contact search record
     *         type
     *  @return the contact search record
     *  @throws org.osid.NullArgumentException
     *          <code>contactSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.ContactSearchRecord getContactSearchRecord(org.osid.type.Type contactSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.contact.records.ContactSearchRecord record : this.records) {
            if (record.implementsRecordType(contactSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this contact search. 
     *
     *  @param contactSearchRecord contact search record
     *  @param contactSearchRecordType contact search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addContactSearchRecord(org.osid.contact.records.ContactSearchRecord contactSearchRecord, 
                                           org.osid.type.Type contactSearchRecordType) {

        addRecordType(contactSearchRecordType);
        this.records.add(contactSearchRecord);        
        return;
    }
}
