//
// AbstractTimePeriodLookupSession.java
//
//    A starter implementation framework for providing a TimePeriod
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a TimePeriod
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getTimePeriods(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractTimePeriodLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.TimePeriodLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>TimePeriod</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTimePeriods() {
        return (true);
    }


    /**
     *  A complete view of the <code>TimePeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTimePeriodView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>TimePeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTimePeriodView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include time periods in calendars which are
     *  children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>TimePeriod</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>TimePeriod</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>TimePeriod</code> and
     *  retained for compatibility.
     *
     *  @param  timePeriodId <code>Id</code> of the
     *          <code>TimePeriod</code>
     *  @return the time period
     *  @throws org.osid.NotFoundException <code>timePeriodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>timePeriodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.TimePeriodList timePeriods = getTimePeriods()) {
            while (timePeriods.hasNext()) {
                org.osid.calendaring.TimePeriod timePeriod = timePeriods.getNextTimePeriod();
                if (timePeriod.getId().equals(timePeriodId)) {
                    return (timePeriod);
                }
            }
        } 

        throw new org.osid.NotFoundException(timePeriodId + " not found");
    }


    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  timePeriods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>TimePeriods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getTimePeriods()</code>.
     *
     *  @param  timePeriodIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByIds(org.osid.id.IdList timePeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.TimePeriod> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = timePeriodIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getTimePeriod(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("time period " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.timeperiod.LinkedTimePeriodList(ret));
    }


    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  time period genus <code>Type</code> which does not include
     *  time periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getTimePeriods()</code>.
     *
     *  @param  timePeriodGenusType a timePeriod genus type 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByGenusType(org.osid.type.Type timePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.timeperiod.TimePeriodGenusFilterList(getTimePeriods(), timePeriodGenusType));
    }


    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  time period genus <code>Type</code> and include any additional
     *  time periods with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTimePeriods()</code>.
     *
     *  @param  timePeriodGenusType a timePeriod genus type 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByParentGenusType(org.osid.type.Type timePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTimePeriodsByGenusType(timePeriodGenusType));
    }


    /**
     *  Gets a <code>TimePeriodList</code> containing the given
     *  time period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTimePeriods()</code>.
     *
     *  @param  timePeriodRecordType a timePeriod record type 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByRecordType(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.timeperiod.TimePeriodRecordFilterList(getTimePeriods(), timePeriodRecordType));
    }


    /**
     *  Gets a <code> TimePeriodList </code> containing the given
     *  <code> DateTime. </code> Time periods containing the given
     *  date are matched.  In plenary mode, the returned list contains
     *  all of the time periods specified in the <code> Id </code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code> Id </code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible <code>
     *  TimePeriods </code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  @param  datetime a date
     *  @return the returned <code> TimePeriod </code> list
     *  @throws org.osid.NullArgumentException <code> datetime </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByDate(org.osid.calendaring.DateTime datetime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.timeperiod.TimePeriodFilterList(new DateFilter(datetime), getTimePeriods()));
    }
        

    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  <code>DateTime</code>. Time periods whose start end end times
     *  are included in the given date range are matched.In plenary
     *  mode, the returned list contains all of the time periods
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>TimePeriods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  start start of daterange 
     *  @param  end end of date range 
     *  @return the returned <code>TimePeriod</code> list 
     *  @throws org.osid.InvalidArgumentException <code>end</code> is less 
     *          than <code>start</code> 
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsInDateRange(org.osid.calendaring.DateTime start, 
                                                                         org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.timeperiod.TimePeriodFilterList(new DateRangeFilter(start, end), getTimePeriods()));
    }


    
    /**
     *  Gets all <code>TimePeriods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>TimePeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.TimePeriodList getTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the time period list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of time periods
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.TimePeriodList filterTimePeriodsOnViews(org.osid.calendaring.TimePeriodList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }


    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.timeperiod.TimePeriodFilter {

        private final org.osid.calendaring.DateTime datetime;


        /**
         *  Constructs a new <code>DateFilter</code>.
         *
         *  @param datetime a date
         *  @throws org.osid.NullArgumentException
         *          <code>datetime</code> is <code>null</code>
         */

        public DateFilter(org.osid.calendaring.DateTime datetime) {
            nullarg(datetime, "date");
            this.datetime = datetime;

            return;
        }


        /**
         *  Used by the TimePeriodFilterList to filter the
         *  time period list based on date.
         *
         *  @param timePeriod the time period
         *  @return <code>true</code> to pass the time period,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.calendaring.TimePeriod timePeriod) {
            if (timePeriod.getStart().isGreater(this.datetime)) {
                return (false);
            }

            if (timePeriod.getEnd().isLess(this.datetime)) {
                return (false);
            }

            return (true);
        }
    }        


    public static class DateRangeFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.timeperiod.TimePeriodFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;


        /**
         *  Constructs a new <code>DateRangeFilter</code>.
         *
         *  @param from start of range
         *  @param to end of range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public DateRangeFilter(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to) {
            nullarg(from, "from date");
            nullarg(to, "to date");

            this.from = from;
            this.to = to;

            return;
        }


        /**
         *  Used by the TimePeriodFilterList to filter the time period
         *  list based on date range.
         *
         *  @param timePeriod the time period
         *  @return <code>true</code> to pass the time period,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.calendaring.TimePeriod timePeriod) {
            if (timePeriod.getStart().isLess(this.from)) {
                return (false);
            }

            if (timePeriod.getEnd().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }        
}
