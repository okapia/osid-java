//
// AbstractAssemblyRecurringEventQuery.java
//
//     A RecurringEventQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.recurringevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RecurringEventQuery that stores terms.
 */

public abstract class AbstractAssemblyRecurringEventQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.calendaring.RecurringEventQuery,
               org.osid.calendaring.RecurringEventQueryInspector,
               org.osid.calendaring.RecurringEventSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.RecurringEventQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.RecurringEventQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.RecurringEventSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidContainableQuery query;


    /** 
     *  Constructs a new <code>AbstractAssemblyRecurringEventQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRecurringEventQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidContainableQuery(assembler);
        return;
    }
    

    /**
     *  Match containables that are sequestered. 
     *
     *  @param  match <code> true </code> to match any sequestered 
     *          containables, <code> false </code> to match non-sequestered 
     *          containables 
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.query.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms. 
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.query.clearSequesteredTerms();
        return;
    }

    
    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.query.getSequesteredTerms());
    }

    
    /**
     *  Specifies a preference for ordering the result set by the sequestered 
     *  flag. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.query.orderBySequestered(style);
        return;
    }


    /**
     *  Gets the column name for the sequestered field.
     *
     *  @return the column name
     */

    protected String getSequesteredColumn() {
        return (this.query.getSequesteredColumn());
    }


    /**
     *  Sets the schedule <code> Id </code> for this query for matching 
     *  schedules. 
     *
     *  @param  scheduleId a schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleId(org.osid.id.Id scheduleId, boolean match) {
        getAssembler().addIdTerm(getScheduleIdColumn(), scheduleId, match);
        return;
    }


    /**
     *  Clears the schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleIdTerms() {
        getAssembler().clearTerms(getScheduleIdColumn());
        return;
    }


    /**
     *  Gets the schedule <code> Id </code> terms. 
     *
     *  @return the schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleIdTerms() {
        return (getAssembler().getIdTerms(getScheduleIdColumn()));
    }


    /**
     *  Gets the ScheduleId column name.
     *
     * @return the column name
     */

    protected String getScheduleIdColumn() {
        return ("schedule_id");
    }


    /**
     *  Tests if a <code> ScheduleQuery </code> is available for querying 
     *  schedules. 
     *
     *  @return <code> true </code> if a schedule query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedule. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the schedule query 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuery getScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleQuery() is false");
    }


    /**
     *  Matches a recurring event that has any schedule assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          schedules, <code> false </code> to match recurring events with 
     *          no schedules 
     */

    @OSID @Override
    public void matchAnySchedule(boolean match) {
        getAssembler().addIdWildcardTerm(getScheduleColumn(), match);
        return;
    }


    /**
     *  Clears the schedule terms. 
     */

    @OSID @Override
    public void clearScheduleTerms() {
        getAssembler().clearTerms(getScheduleColumn());
        return;
    }


    /**
     *  Gets the schedule terms. 
     *
     *  @return the schedule terms 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQueryInspector[] getScheduleTerms() {
        return (new org.osid.calendaring.ScheduleQueryInspector[0]);
    }


    /**
     *  Gets the Schedule column name.
     *
     * @return the column name
     */

    protected String getScheduleColumn() {
        return ("schedule");
    }


    /**
     *  Sets the superseding event <code> Id </code> for this query. 
     *
     *  @param  supersedingEventId a superseding event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> supersedingEventId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSupersedingEventId(org.osid.id.Id supersedingEventId, 
                                        boolean match) {
        getAssembler().addIdTerm(getSupersedingEventIdColumn(), supersedingEventId, match);
        return;
    }


    /**
     *  Clears the superseding event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersedingEventIdTerms() {
        getAssembler().clearTerms(getSupersedingEventIdColumn());
        return;
    }


    /**
     *  Gets the superseding event <code> Id </code> terms. 
     *
     *  @return the superseding event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersedingEventIdTerms() {
        return (getAssembler().getIdTerms(getSupersedingEventIdColumn()));
    }


    /**
     *  Gets the SupersedingEventId column name.
     *
     * @return the column name
     */

    protected String getSupersedingEventIdColumn() {
        return ("superseding_event_id");
    }


    /**
     *  Tests if a <code> SupersedingEventQuery </code> is available for 
     *  querying superseding events. 
     *
     *  @return <code> true </code> if a superseding event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the superseding event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuery getSupersedingEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventQuery() is false");
    }


    /**
     *  Matches a recurring event that has any superseding event assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          superseding events, <code> false </code> to match events with 
     *          no superseding events 
     */

    @OSID @Override
    public void matchAnySupersedingEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getSupersedingEventColumn(), match);
        return;
    }


    /**
     *  Clears the superseding event terms. 
     */

    @OSID @Override
    public void clearSupersedingEventTerms() {
        getAssembler().clearTerms(getSupersedingEventColumn());
        return;
    }


    /**
     *  Gets the superseding event terms. 
     *
     *  @return the superseding event terms 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQueryInspector[] getSupersedingEventTerms() {
        return (new org.osid.calendaring.SupersedingEventQueryInspector[0]);
    }


    /**
     *  Gets the SupersedingEvent column name.
     *
     * @return the column name
     */

    protected String getSupersedingEventColumn() {
        return ("superseding_event");
    }


    /**
     *  Matches recurring events with specific dates between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchSpecificMeetingTime(org.osid.calendaring.DateTime start, 
                                         org.osid.calendaring.DateTime end, 
                                         boolean match) {
        getAssembler().addDateTimeRangeTerm(getSpecificMeetingTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches a recurring event that has any specific date assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          specific date, <code> false </code> to match recurring events 
     *          with no specific date 
     */

    @OSID @Override
    public void matchAnySpecificMeetingTime(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getSpecificMeetingTimeColumn(), match);
        return;
    }


    /**
     *  Clears the blackout terms. 
     */

    @OSID @Override
    public void clearSpecificMeetingTimeTerms() {
        getAssembler().clearTerms(getSpecificMeetingTimeColumn());
        return;
    }


    /**
     *  Gets the date terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getSpecificMeetingTimeTerms() {
        return (getAssembler().getDateTimeTerms(getSpecificMeetingTimeColumn()));
    }


    /**
     *  Gets the SpecificMeetingTime column name.
     *
     * @return the column name
     */

    protected String getSpecificMeetingTimeColumn() {
        return ("specific_meeting_time");
    }


    /**
     *  Sets the composed event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        getAssembler().clearTerms(getEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (getAssembler().getIdTerms(getEventIdColumn()));
    }


    /**
     *  Gets the EventId column name.
     *
     * @return the column name
     */

    protected String getEventIdColumn() {
        return ("event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  composed events. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches a recurring event that has any composed event assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          composed events, <code> false </code> to match events with no 
     *          composed events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getEventColumn(), match);
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        getAssembler().clearTerms(getEventColumn());
        return;
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the Event column name.
     *
     * @return the column name
     */

    protected String getEventColumn() {
        return ("event");
    }


    /**
     *  Matches a blackout that contains the given date time. 
     *
     *  @param  datetime a datetime 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> datetime </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlackout(org.osid.calendaring.DateTime datetime, 
                              boolean match) {
        getAssembler().addDateTimeTerm(getBlackoutColumn(), datetime, match);
        return;
    }


    /**
     *  Matches a recurring event that has any blackout assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          blackout, <code> false </code> to match recurring events with 
     *          no blackout 
     */

    @OSID @Override
    public void matchAnyBlackout(boolean match) {
        getAssembler().addDateTimeWildcardTerm(getBlackoutColumn(), match);
        return;
    }


    /**
     *  Clears the blackout terms. 
     */

    @OSID @Override
    public void clearBlackoutTerms() {
        getAssembler().clearTerms(getBlackoutColumn());
        return;
    }


    /**
     *  Gets the blackout terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getBlackoutTerms() {
        return (getAssembler().getDateTimeTerms(getBlackoutColumn()));
    }


    /**
     *  Gets the Blackout column name.
     *
     * @return the column name
     */

    protected String getBlackoutColumn() {
        return ("blackout");
    }


    /**
     *  Matches recurring events with blackouts between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchBlackoutInclusive(org.osid.calendaring.DateTime start, 
                                       org.osid.calendaring.DateTime end, 
                                       boolean match) {
        getAssembler().addDateTimeRangeTerm(getBlackoutInclusiveColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the blackout terms. 
     */

    @OSID @Override
    public void clearBlackoutInclusiveTerms() {
        getAssembler().clearTerms(getBlackoutInclusiveColumn());
        return;
    }


    /**
     *  Gets the inclusive blackout terms. 
     *
     *  @return the time range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBlackoutInclusiveTerms() {
        return (getAssembler().getDateTimeRangeTerms(getBlackoutInclusiveColumn()));
    }


    /**
     *  Gets the BlackoutInclusive column name.
     *
     * @return the column name
     */

    protected String getBlackoutInclusiveColumn() {
        return ("blackout_inclusive");
    }


    /**
     *  Sets the sponsor <code> Id </code> for this query. 
     *
     *  @param  sponsorId a sponsor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sponsorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id sponsorId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), sponsorId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> terms. 
     *
     *  @return the sponsor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  sponsors. 
     *
     *  @return <code> true </code> if a sponsor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the sponsor query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor terms. 
     *
     *  @return the sponsor terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this recurringEvent supports the given record
     *  <code>Type</code>.
     *
     *  @param  recurringEventRecordType a recurring event record type 
     *  @return <code>true</code> if the recurringEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recurringEventRecordType) {
        for (org.osid.calendaring.records.RecurringEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  recurringEventRecordType the recurring event record type 
     *  @return the recurring event query record 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventQueryRecord getRecurringEventQueryRecord(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.RecurringEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  recurringEventRecordType the recurring event record type 
     *  @return the recurring event query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventQueryInspectorRecord getRecurringEventQueryInspectorRecord(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.RecurringEventQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param recurringEventRecordType the recurring event record type
     *  @return the recurring event search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventSearchOrderRecord getRecurringEventSearchOrderRecord(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.RecurringEventSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this recurring event. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param recurringEventQueryRecord the recurring event query record
     *  @param recurringEventQueryInspectorRecord the recurring event query inspector
     *         record
     *  @param recurringEventSearchOrderRecord the recurring event search order record
     *  @param recurringEventRecordType recurring event record type
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventQueryRecord</code>,
     *          <code>recurringEventQueryInspectorRecord</code>,
     *          <code>recurringEventSearchOrderRecord</code> or
     *          <code>recurringEventRecordTyperecurringEvent</code> is
     *          <code>null</code>
     */
            
    protected void addRecurringEventRecords(org.osid.calendaring.records.RecurringEventQueryRecord recurringEventQueryRecord, 
                                      org.osid.calendaring.records.RecurringEventQueryInspectorRecord recurringEventQueryInspectorRecord, 
                                      org.osid.calendaring.records.RecurringEventSearchOrderRecord recurringEventSearchOrderRecord, 
                                      org.osid.type.Type recurringEventRecordType) {

        addRecordType(recurringEventRecordType);

        nullarg(recurringEventQueryRecord, "recurring event query record");
        nullarg(recurringEventQueryInspectorRecord, "recurring event query inspector record");
        nullarg(recurringEventSearchOrderRecord, "recurring event search odrer record");

        this.queryRecords.add(recurringEventQueryRecord);
        this.queryInspectorRecords.add(recurringEventQueryInspectorRecord);
        this.searchOrderRecords.add(recurringEventSearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidContainableQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidContainableQuery
        implements org.osid.OsidContainableQuery,
                   org.osid.OsidContainableQueryInspector,
                   org.osid.OsidContainableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidContainableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidContainableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the sequestered field.
         *
         *  @return the column name
         */
        
        protected String getSequesteredColumn() {
            return (super.getSequesteredColumn());
        }
    }    
}
