//
// AbstractRelationshipLookupSession.java
//
//    A starter implementation framework for providing a Relationship
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Relationship
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRelationships(), this other methods may need to be overridden
 *  for better performance.
 */

public abstract class AbstractRelationshipLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.relationship.RelationshipLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.relationship.Family family = new net.okapia.osid.jamocha.nil.relationship.family.UnknownFamily();
    

    /**
     *  Gets the <code>Family/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Family Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.family.getId());
    }


    /**
     *  Gets the <code>Family</code> associated with this session.
     *
     *  @return the <code>Family</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.family);
    }


    /**
     *  Sets the <code>Family</code>.
     *
     *  @param  family the family for this session
     *  @throws org.osid.NullArgumentException <code>family</code>
     *          is <code>null</code>
     */

    protected void setFamily(org.osid.relationship.Family family) {
        nullarg(family, "family");
        this.family = family;
        return;
    }


    /**
     *  Tests if this user can perform <code>Relationship</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelationships() {
        return (true);
    }


    /**
     *  A complete view of the <code>Relationship</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelationshipView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Relationship</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelationshipView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationships in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only relationships whose effective dates are current are
     *  returned by methods in this session.
     */

    public void useEffectiveRelationshipView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All relationships of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRelationshipView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Relationship</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Relationship</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Relationship</code>
     *  and retained for compatibility.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipId <code>Id</code> of the
     *          <code>Relationship</code>
     *  @return the relationship
     *  @throws org.osid.NotFoundException <code>relationshipId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relationshipId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Relationship getRelationship(org.osid.id.Id relationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.relationship.RelationshipList relationships = getRelationships()) {
            while (relationships.hasNext()) {
                org.osid.relationship.Relationship relationship = relationships.getNextRelationship();
                if (relationship.getId().equals(relationshipId)) {
                    return (relationship);
                }
            }
        } 

        throw new org.osid.NotFoundException(relationshipId + " not found");
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Relationships</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRelationships()</code>.
     *
     *  @param  relationshipIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByIds(org.osid.id.IdList relationshipIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.relationship.Relationship> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = relationshipIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRelationship(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("relationship " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.relationship.relationship.LinkedRelationshipList(ret));
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given relationship genus <code>Type</code> which does not
     *  include relationships of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRelationships()</code>.
     *
     *  @param relationshipGenusType a relationship genus type
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipGenusFilterList(getRelationships(), relationshipGenusType));
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given relationship genus <code>Type</code> and include any
     *  additional relationships with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRelationships()</code>.
     *
     *  @param relationshipGenusType a relationship genus type
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByParentGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRelationshipsByGenusType(relationshipGenusType));
    }


    /**
     *  Gets a <code>RelationshipList</code> containing the given
     *  relationship record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRelationships()</code>.
     *
     *  @param relationshipRecordType a relationship record type
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByRecordType(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipRecordFilterList(getRelationships(), relationshipRecordType));
    }


    /**
     *  Gets a <code>RelationshipList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In active mode, relationships are returned that are currently
     *  active. In any status mode, active and inactive relationships
     *  are returned.
     *
     *  @param from start of date range
     *  @param  to end of date range 
     *  @return the returned <code>Relationship</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsOnDate(org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.TemporalRelationshipFilterList(getRelationships(), from, to));
    }
        

    /**
     *  Gets a list of relationships corresponding to a source
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.relationship.RelationshipList getRelationshipsForSource(org.osid.id.Id sourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipFilterList(new SourceFilter(sourceId), getRelationships()));
    }


    /**
     *  Gets a list of relationships corresponding to a source
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param sourceId the <code>Id</code> of the source
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForSourceOnDate(org.osid.id.Id sourceId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.TemporalRelationshipFilterList(getRelationshipsForSource(sourceId), from, to));
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus <code>
     *  Type. Relationships </code> of any genus derived from the
     *  given genus are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible <code> Relationships
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          or <code>relationshipGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSource(org.osid.id.Id sourceId, 
                                                                                       org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipGenusFilterList(getRelationshipsForSource(sourceId), relationshipGenusType));
    }

    
    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus
     *  <code>Type</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from is greater than 
     *          to </code> 
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>relationshipGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSourceOnDate(org.osid.id.Id sourceId, 
                                                                                             org.osid.type.Type relationshipGenusType, 
                                                                                             org.osid.calendaring.DateTime from, 
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.TemporalRelationshipFilterList(getRelationshipsByGenusTypeForSource(sourceId, relationshipGenusType), from, to));
    }


    /**
     *  Gets a list of relationships corresponding to a destination
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the <code>Id</code> of the destination
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.relationship.RelationshipList getRelationshipsForDestination(org.osid.id.Id destinationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipFilterList(new DestinationFilter(destinationId), getRelationships()));
    }


    /**
     *  Gets a list of relationships corresponding to a destination
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param destinationId the <code>Id</code> of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForDestinationOnDate(org.osid.id.Id destinationId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.TemporalRelationshipFilterList(getRelationshipsForDestination(destinationId), from, to));
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus <code>
     *  Type. Relationships </code> of any genus derived from the
     *  given genus are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible <code> Relationships
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId a peer <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException
     *          <code>destinationId</code> or
     *          <code>relationshipGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestination(org.osid.id.Id destinationId, 
                                                                                            org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipGenusFilterList(getRelationshipsForDestination(destinationId), relationshipGenusType));
    }

    
    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus
     *  <code>Type</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  destinationId a peer <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from is greater than 
     *          to </code> 
     *  @throws org.osid.NullArgumentException <code>destinationId</code>,
     *          <code>relationshipGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestinationOnDate(org.osid.id.Id destinationId, 
                                                                                                  org.osid.type.Type relationshipGenusType, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.TemporalRelationshipFilterList(getRelationshipsByGenusTypeForSource(destinationId, relationshipGenusType), from, to));
    }


    /**
     *  Gets a list of relationships corresponding to source and
     *  destination <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  destinationId the <code>Id</code> of the destination
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeers(org.osid.id.Id sourceId,
                                                                           org.osid.id.Id destinationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipFilterList(new DestinationFilter(destinationId), getRelationshipsForSource(sourceId)));
    }


    /**
     *  Gets a list of relationships corresponding to source and destination
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param destinationId the <code>Id</code> of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeersOnDate(org.osid.id.Id sourceId,
                                                                                 org.osid.id.Id destinationId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.TemporalRelationshipFilterList(getRelationshipsForPeers(sourceId, destinationId), from, to));
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding between the
     *  given peer <code>Ids</code> and relationship genus
     *  <code>Type</code> <code>Relationships</code> of any genus
     *  derived from the given genus are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code> 
     *  @param  destinationId a related peer <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code>, or
     *          <code>relationshipGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeers(org.osid.id.Id sourceId, 
                                                                                      org.osid.id.Id destinationId, 
                                                                                      org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipGenusFilterList(getRelationshipsForPeers(sourceId, destinationId), relationshipGenusType));
    }        


    /**
     *  Gets a <code>RelationshipList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer set or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code> 
     *  @param  destinationId a related peer <code>Id</code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code> sourceId, destinationId, 
     *          relationshipGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeersOnDate(org.osid.id.Id sourceId, 
                                                                                            org.osid.id.Id destinationId, 
                                                                                            org.osid.type.Type relationshipGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.relationship.TemporalRelationshipFilterList(getRelationshipsByGenusTypeForPeers(sourceId, destinationId, relationshipGenusType), from, to)); 
    }        


    /**
     *  Gets all <code>Relationships</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @return a list of <code>Relationships</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.relationship.RelationshipList getRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the relationship list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of relationships
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.relationship.RelationshipList filterRelationshipsOnViews(org.osid.relationship.RelationshipList list)
        throws org.osid.OperationFailedException {

        org.osid.relationship.RelationshipList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.relationship.relationship.EffectiveRelationshipFilterList(ret);
        }

        return (ret);
    }


    public static class SourceFilter
        implements net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipFilter {
         
        private final org.osid.id.Id sourceId;
         
         
        /**
         *  Constructs a new <code>SourceFilter</code>.
         *
         *  @param sourceId the source to filter
         *  @throws org.osid.NullArgumentException
         *          <code>sourceId</code> is <code>null</code>
         */
        
        public SourceFilter(org.osid.id.Id sourceId) {
            nullarg(sourceId, "source Id");
            this.sourceId = sourceId;
            return;
        }

         
        /**
         *  Used by the RelationshipFilterList to filter the 
         *  relationship list based on source.
         *
         *  @param relationship the relationship
         *  @return <code>true</code> to pass the relationship,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.relationship.Relationship relationship) {
            return (relationship.getSourceId().equals(this.sourceId));
        }
    }


    public static class DestinationFilter
        implements net.okapia.osid.jamocha.inline.filter.relationship.relationship.RelationshipFilter {
         
        private final org.osid.id.Id destinationId;
         
         
        /**
         *  Constructs a new <code>DestinationFilter</code>.
         *
         *  @param destinationId the destination to filter
         *  @throws org.osid.NullArgumentException
         *          <code>destinationId</code> is <code>null</code>
         */
        
        public DestinationFilter(org.osid.id.Id destinationId) {
            nullarg(destinationId, "destination Id");
            this.destinationId = destinationId;
            return;
        }

         
        /**
         *  Used by the RelationshipFilterList to filter the 
         *  relationship list based on destination.
         *
         *  @param relationship the relationship
         *  @return <code>true</code> to pass the relationship,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.relationship.Relationship relationship) {
            return (relationship.getDestinationId().equals(this.destinationId));
        }
    }
}
