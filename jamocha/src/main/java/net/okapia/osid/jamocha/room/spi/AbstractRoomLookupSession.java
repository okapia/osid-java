//
// AbstractRoomLookupSession.java
//
//    A starter implementation framework for providing a Room
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Room lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRooms(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRoomLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.RoomLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();
    

    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this session.
     *
     *  @return the <code>Campus</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Room</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRooms() {
        return (true);
    }


    /**
     *  A complete view of the <code>Room</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRoomView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Room</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRoomView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rooms in campuses which are children of this
     *  campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only rooms whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRoomView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All rooms of any effective dates are returned by all methods
     *  in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRoomView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Room</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Room</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Room</code> and retained for
     *  compatibility.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @param  roomId <code>Id</code> of the
     *          <code>Room</code>
     *  @return the room
     *  @throws org.osid.NotFoundException <code>roomId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>roomId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Room getRoom(org.osid.id.Id roomId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.RoomList rooms = getRooms()) {
            while (rooms.hasNext()) {
                org.osid.room.Room room = rooms.getNextRoom();
                if (room.getId().equals(roomId)) {
                    return (room);
                }
            }
        } 

        throw new org.osid.NotFoundException(roomId + " not found");
    }


    /**
     *  Gets a <code>RoomList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the rooms
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Rooms</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRooms()</code>.
     *
     *  @param roomIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Room</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>roomIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByIds(org.osid.id.IdList roomIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.Room> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = roomIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRoom(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("room " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.room.LinkedRoomList(ret));
    }


    /**
     *  Gets a <code>RoomList</code> corresponding to the given room
     *  genus <code>Type</code> which does not include rooms of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRooms()</code>.
     *
     *  @param roomGenusType a room genus type
     *  @return the returned <code>Room</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>roomGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.RoomGenusFilterList(getRooms(), roomGenusType));
    }


    /**
     *  Gets a <code>RoomList</code> corresponding to the given room
     *  genus <code>Type</code> and include any additional rooms with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRooms()</code>.
     *
     *  @param roomGenusType a room genus type
     *  @return the returned <code>Room</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>roomGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByParentGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRoomsByGenusType(roomGenusType));
    }


    /**
     *  Gets a <code>RoomList</code> containing the given room record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRooms()</code>.
     *
     *  @param  roomRecordType a room record type 
     *  @return the returned <code>Room</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByRecordType(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.RoomRecordFilterList(getRooms(), roomRecordType));
    }


    /**
     *  Gets a <code>RoomList</code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In active mode, rooms are returned that are currently
     *  active. In any status mode, active and inactive rooms are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Room</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.RoomList getRoomsOnDate(org.osid.calendaring.DateTime from, 
                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.TemporalRoomFilterList(getRooms(), from, to));
    }
        

    /**
     *  Gets a list of all rooms of a genus type effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  roomGenusType a room genus <code>Type</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>roomGenusType</code>, <code>from</code>, </code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeOnDate(org.osid.type.Type roomGenusType, 
                                                            org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.TemporalRoomFilterList(getRoomsByGenusType(roomGenusType), from, to));
    }        


    /**
     *  Gets a list of all rooms corresponding to a building <code>
     *  Id</code>.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.NullArgumentException <code>buildingId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.RoomFilterList(new BuildingFilter(buildingId), getRooms()));
    }


    /**
     *  Gets a list of all rooms corresponding to a building
     *  <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code> 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is less 
     *          than <code>from</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                            org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.TemporalRoomFilterList(getRoomsForBuilding(buildingId), from, to));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a building <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building 
     *  @param  roomGenusType a room genus type 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.NullArgumentException <code>buildingId</code> or 
     *          <code>roomGenusType</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                 org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.RoomGenusFilterList(getRoomsForBuilding(buildingId), roomGenusType));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a building <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param buildingId a building <code>Id</code>
     *  @param  roomGenusType a room genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is less 
     *          than <code>from</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingId</code>, <code>roomGenusType</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                       org.osid.type.Type roomGenusType, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.TemporalRoomFilterList(getRoomsByGenusTypeForBuilding(buildingId, roomGenusType), from, to));
    }


    /**
     *  Gets a list of all rooms corresponding to a floor <code>
     *  Id</code>.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId the <code>Id /code> of the building 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.NullArgumentException <code>floorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForFloor(org.osid.id.Id floorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.RoomFilterList(new FloorFilter(floorId), getRooms()));
    }


    /**
     *  Gets a list of all rooms corresponding to a floor
     *  <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  Rooms are returned with start effective dates that fall
     *  between the requested dates inclusive. In plenary mode, the
     *  returned list contains all known rooms or an error
     *  results. Otherwise, the returned list may contain only those
     *  rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId a building <code>Id</code> 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is less 
     *          than <code>from</code> 
     *  @throws org.osid.NullArgumentException <code>floorId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForFloorOnDate(org.osid.id.Id floorId, 
                                                         org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.TemporalRoomFilterList(getRoomsForFloor(floorId), from, to));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a floor <code>Id</code>.
     *  
     *  <code></code> In plenary mode, the returned list contains all
     *  known rooms or an error results. Otherwise, the returned list
     *  may contain only those rooms that are accessible through this
     *  session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId the <code>Id</code> of the floor 
     *  @param  roomGenusType a room genus type 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.NullArgumentException <code>floorId</code> or 
     *          <code>roomGenusType</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForFloor(org.osid.id.Id floorId, 
                                                              org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.RoomGenusFilterList(getRoomsForFloor(floorId), roomGenusType));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a floor <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  Rooms are returned with start effective dates that fall
     *  between the requested dates inclusive. In plenary mode, the
     *  returned list contains all known rooms or an error
     *  results. Otherwise, the returned list may contain only those
     *  rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId a floor <code>Id</code> 
     *  @param  roomGenusType a room genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>RoomList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is less 
     *          than <code>from</code> 
     *  @throws org.osid.NullArgumentException <code>floorId</code>,
     *          <code>roomGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForFloorOnDate(org.osid.id.Id floorId, 
                                                                    org.osid.type.Type roomGenusType, 
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.TemporalRoomFilterList(getRoomsByGenusTypeForFloor(floorId, roomGenusType), from, to));
    }


    /**
     *  Gets a <code>RoomList</code> containing of the given
     *  room number.
     *  
     *  In plenary mode, the returned list contains all known
     *  rooms or an error results. Otherwise, the returned list
     *  may contain only those rooms that are accessible through
     *  this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @param  number a room number 
     *  @return the returned <code>Room</code> list 
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByRoomNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.room.RoomFilterList(new NumberFilter(number), getRooms()));
    }

    
    /**
     *  Gets all <code>Rooms</code>.
     *
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Rooms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.room.RoomList getRooms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the room list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of rooms
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.room.RoomList filterRoomsOnViews(org.osid.room.RoomList list)
        throws org.osid.OperationFailedException {

        org.osid.room.RoomList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.room.room.EffectiveRoomFilterList(ret);
        }

        return (ret);
    }


    public static class BuildingFilter
        implements net.okapia.osid.jamocha.inline.filter.room.room.RoomFilter {
         
        private final org.osid.id.Id buildingId;
         
         
        /**
         *  Constructs a new <code>BuildingFilter</code>.
         *
         *  @param buildingId the building to filter
         *  @throws org.osid.NullArgumentException
         *          <code>buildingId</code> is <code>null</code>
         */
        
        public BuildingFilter(org.osid.id.Id buildingId) {
            nullarg(buildingId, "building Id");
            this.buildingId = buildingId;
            return;
        }

         
        /**
         *  Used by the RoomFilterList to filter the 
         *  room list based on building.
         *
         *  @param room the room
         *  @return <code>true</code> to pass the room,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.Room room) {
            return (room.getBuildingId().equals(this.buildingId));
        }
    }


    public static class FloorFilter
        implements net.okapia.osid.jamocha.inline.filter.room.room.RoomFilter {
         
        private final org.osid.id.Id floorId;
         
         
        /**
         *  Constructs a new <code>FloorFilter</code>.
         *
         *  @param floorId the floor to filter
         *  @throws org.osid.NullArgumentException
         *          <code>floorId</code> is <code>null</code>
         */
        
        public FloorFilter(org.osid.id.Id floorId) {
            nullarg(floorId, "floor Id");
            this.floorId = floorId;
            return;
        }

         
        /**
         *  Used by the RoomFilterList to filter the 
         *  room list based on floor.
         *
         *  @param room the room
         *  @return <code>true</code> to pass the room,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.Room room) {
            return (room.getFloorId().equals(this.floorId));
        }
    }


    public static class NumberFilter
        implements net.okapia.osid.jamocha.inline.filter.room.room.RoomFilter {
         
        private final String number;
         
         
        /**
         *  Constructs a new <code>NumberFilter</code>.
         *
         *  @param number the number to filter
         *  @throws org.osid.NullArgumentException
         *          <code>number</code> is <code>null</code>
         */
        
        public NumberFilter(String number) {
            nullarg(number, "number");
            this.number = number;
            return;
        }

         
        /**
         *  Used by the RoomFilterList to filter the 
         *  room list based on number.
         *
         *  @param room the room
         *  @return <code>true</code> to pass the room,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.Room room) {
            return (room.getRoomNumber().equals(this.number));
        }
    }    
}
