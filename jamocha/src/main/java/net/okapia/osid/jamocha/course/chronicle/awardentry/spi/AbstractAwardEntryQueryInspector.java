//
// AbstractAwardEntryQueryInspector.java
//
//     A template for making an AwardEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.awardentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for award entries.
 */

public abstract class AbstractAwardEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.chronicle.AwardEntryQueryInspector {

    private final java.util.Collection<org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the award <code> Id </code> query terms. 
     *
     *  @return the award <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the award query terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the award date query terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateAwardedTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getAssessmentTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given award entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an award entry implementing the requested record.
     *
     *  @param awardEntryRecordType an award entry record type
     *  @return the award entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord getAwardEntryQueryInspectorRecord(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this award entry query. 
     *
     *  @param awardEntryQueryInspectorRecord award entry query inspector
     *         record
     *  @param awardEntryRecordType awardEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAwardEntryQueryInspectorRecord(org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord awardEntryQueryInspectorRecord, 
                                                   org.osid.type.Type awardEntryRecordType) {

        addRecordType(awardEntryRecordType);
        nullarg(awardEntryRecordType, "award entry record type");
        this.records.add(awardEntryQueryInspectorRecord);        
        return;
    }
}
