//
// AbstractTopologyRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTopologyRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.topology.rules.TopologyRulesManager,
               org.osid.topology.rules.TopologyRulesProxyManager {

    private final Types edgeEnablerRecordTypes             = new TypeRefSet();
    private final Types edgeEnablerSearchRecordTypes       = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractTopologyRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTopologyRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up edge enablers is supported. 
     *
     *  @return <code> true </code> if edge enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying edge enablers is supported. 
     *
     *  @return <code> true </code> if edge enabler query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching edge enablers is supported. 
     *
     *  @return <code> true </code> if edge enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an edge enabler administrative service is supported. 
     *
     *  @return <code> true </code> if edge enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an edge enabler notification service is supported. 
     *
     *  @return <code> true </code> if edge enabler notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an edge enabler graph lookup service is supported. 
     *
     *  @return <code> true </code> if an edge enabler graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerGraph() {
        return (false);
    }


    /**
     *  Tests if an edge enabler graph service is supported. 
     *
     *  @return <code> true </code> if edge enabler graph assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerGraphAssignment() {
        return (false);
    }


    /**
     *  Tests if an edge enabler graph lookup service is supported. 
     *
     *  @return <code> true </code> if an edge enabler graph service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerSmartGraph() {
        return (false);
    }


    /**
     *  Tests if an edge enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an edge enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an edge enabler rule application service is supported. 
     *
     *  @return <code> true </code> if edge enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> EdgeEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> EdgeEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.edgeEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> EdgeEnabler </code> record type is 
     *  supported. 
     *
     *  @param  edgeEnablerRecordType a <code> Type </code> indicating an 
     *          <code> EdgeEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerRecordType(org.osid.type.Type edgeEnablerRecordType) {
        return (this.edgeEnablerRecordTypes.contains(edgeEnablerRecordType));
    }


    /**
     *  Adds support for an edge enabler record type.
     *
     *  @param edgeEnablerRecordType an edge enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>edgeEnablerRecordType</code> is <code>null</code>
     */

    protected void addEdgeEnablerRecordType(org.osid.type.Type edgeEnablerRecordType) {
        this.edgeEnablerRecordTypes.add(edgeEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an edge enabler record type.
     *
     *  @param edgeEnablerRecordType an edge enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>edgeEnablerRecordType</code> is <code>null</code>
     */

    protected void removeEdgeEnablerRecordType(org.osid.type.Type edgeEnablerRecordType) {
        this.edgeEnablerRecordTypes.remove(edgeEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> EdgeEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> EdgeEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.edgeEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> EdgeEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  edgeEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> EdgeEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          edgeEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeEnablerSearchRecordType(org.osid.type.Type edgeEnablerSearchRecordType) {
        return (this.edgeEnablerSearchRecordTypes.contains(edgeEnablerSearchRecordType));
    }


    /**
     *  Adds support for an edge enabler search record type.
     *
     *  @param edgeEnablerSearchRecordType an edge enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>edgeEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addEdgeEnablerSearchRecordType(org.osid.type.Type edgeEnablerSearchRecordType) {
        this.edgeEnablerSearchRecordTypes.add(edgeEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an edge enabler search record type.
     *
     *  @param edgeEnablerSearchRecordType an edge enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>edgeEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeEdgeEnablerSearchRecordType(org.osid.type.Type edgeEnablerSearchRecordType) {
        this.edgeEnablerSearchRecordTypes.remove(edgeEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  lookup service. 
     *
     *  @return an <code> EdgeEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerLookupSession getEdgeEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerLookupSession getEdgeEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  lookup service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerLookupSession getEdgeEnablerLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  lookup service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerLookupSession getEdgeEnablerLookupSessionForGraph(org.osid.id.Id graphId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  query service. 
     *
     *  @return an <code> EdgeEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerQuerySession getEdgeEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerQuerySession getEdgeEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  query service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerQuerySession getEdgeEnablerQuerySessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  query service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerQuerySession getEdgeEnablerQuerySessionForGraph(org.osid.id.Id graphId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  search service. 
     *
     *  @return an <code> EdgeEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSearchSession getEdgeEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSearchSession getEdgeEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enablers 
     *  earch service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSearchSession getEdgeEnablerSearchSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enablers 
     *  earch service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSearchSession getEdgeEnablerSearchSessionForGraph(org.osid.id.Id graphId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  administration service. 
     *
     *  @return an <code> EdgeEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerAdminSession getEdgeEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerAdminSession getEdgeEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerAdminSession getEdgeEnablerAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerAdminSession getEdgeEnablerAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  notification service. 
     *
     *  @param  edgeEnablerReceiver the notification callback 
     *  @return an <code> EdgeEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerNotificationSession getEdgeEnablerNotificationSession(org.osid.topology.rules.EdgeEnablerReceiver edgeEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  notification service. 
     *
     *  @param  edgeEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerNotificationSession getEdgeEnablerNotificationSession(org.osid.topology.rules.EdgeEnablerReceiver edgeEnablerReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  notification service for the given graph. 
     *
     *  @param  edgeEnablerReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerReceiver 
     *          </code> or <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerNotificationSession getEdgeEnablerNotificationSessionForGraph(org.osid.topology.rules.EdgeEnablerReceiver edgeEnablerReceiver, 
                                                                                                            org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  notification service for the given graph. 
     *
     *  @param  edgeEnablerReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> edgeEnablerReceiver, 
     *          graphId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerNotificationSession getEdgeEnablerNotificationSessionForGraph(org.osid.topology.rules.EdgeEnablerReceiver edgeEnablerReceiver, 
                                                                                                            org.osid.id.Id graphId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup edge enabler/graph 
     *  mappings for edge enablers. 
     *
     *  @return an <code> EdgeEnablerGraphSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerGraphSession getEdgeEnablerGraphSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup edge enabler/graph 
     *  mappings for edge enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerGraphSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerGraphSession getEdgeEnablerGraphSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning edge 
     *  enablers to ontologies for edge. 
     *
     *  @return an <code> EdgeEnablerGraphAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerGraphAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerGraphAssignmentSession getEdgeEnablerGraphAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning edge 
     *  enablers to ontologies for edge. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerGraphAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerGraphAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerGraphAssignmentSession getEdgeEnablerGraphAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage edge enabler smart 
     *  ontologies. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSmartGraph() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSmartGraphSession getEdgeEnablerSmartGraphSession(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerSmartGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage edge enabler smart 
     *  ontologies. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerSmartGraph() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerSmartGraphSession getEdgeEnablerSmartGraphSession(org.osid.id.Id graphId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerSmartGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  mapping lookup service for looking up the rules applied to the graph. 
     *
     *  @return an <code> EdgeEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleLookupSession getEdgeEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  mapping lookup service for looking up the rules applied to the graph. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleLookupSession getEdgeEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  mapping lookup service for the given graph for looking up rules 
     *  applied to a graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleLookupSession getEdgeEnablerRuleLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerRuleLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  mapping lookup service for the given graph for looking up rules 
     *  applied to a graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleLookupSession getEdgeEnablerRuleLookupSessionForGraph(org.osid.id.Id graphId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerRuleLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  assignment service to apply enablers to ontologies. 
     *
     *  @return an <code> EdgeEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleApplicationSession getEdgeEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  assignment service to apply enablers to ontologies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleApplicationSession getEdgeEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  assignment service for the given graph to apply enablers to 
     *  ontologies. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleApplicationSession getEdgeEnablerRuleApplicationSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesManager.getEdgeEnablerRuleApplicationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge enabler 
     *  assignment service for the given graph to apply enablers to 
     *  ontologies. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerRuleApplicationSession getEdgeEnablerRuleApplicationSessionForGraph(org.osid.id.Id graphId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.rules.TopologyRulesProxyManager.getEdgeEnablerRuleApplicationSessionForGraph not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.edgeEnablerRecordTypes.clear();
        this.edgeEnablerRecordTypes.clear();

        this.edgeEnablerSearchRecordTypes.clear();
        this.edgeEnablerSearchRecordTypes.clear();

        return;
    }
}
