//
// AbstractOntologyBatchProxyManager.java
//
//     An adapter for a OntologyBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OntologyBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOntologyBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.ontology.batch.OntologyBatchProxyManager>
    implements org.osid.ontology.batch.OntologyBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterOntologyBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOntologyBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOntologyBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOntologyBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of subjects is available. 
     *
     *  @return <code> true </code> if a subject bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectBatchAdmin() {
        return (getAdapteeManager().supportsSubjectBatchAdmin());
    }


    /**
     *  Tests if bulk administration of relevancies is available. 
     *
     *  @return <code> true </code> if a relevancy bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyBatchAdmin() {
        return (getAdapteeManager().supportsRelevancyBatchAdmin());
    }


    /**
     *  Tests if bulk administration of ontologies is available. 
     *
     *  @return <code> true </code> if an ontology bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyBatchAdmin() {
        return (getAdapteeManager().supportsOntologyBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subject 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.SubjectBatchAdminSession getSubjectBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subject 
     *  administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.SubjectBatchAdminSession getSubjectBatchAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectBatchAdminSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk relevancy 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.RelevancyBatchAdminSession getRelevancyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk relevancy 
     *  administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.RelevancyBatchAdminSession getRelevancyBatchAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyBatchAdminSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ontology 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.OntologyBatchAdminSession getOntologyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
