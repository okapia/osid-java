//
// AbstractImmutableProcess.java
//
//     Wraps a mutable Process to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.process.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Process</code> to hide modifiers. This
 *  wrapper provides an immutized Process from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying process whose state changes are visible.
 */

public abstract class AbstractImmutableProcess
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.workflow.Process {

    private final org.osid.workflow.Process process;


    /**
     *  Constructs a new <code>AbstractImmutableProcess</code>.
     *
     *  @param process the process to immutablize
     *  @throws org.osid.NullArgumentException <code>process</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProcess(org.osid.workflow.Process process) {
        super(process);
        this.process = process;
        return;
    }


    /**
     *  Tests if this process is enabled. 
     *
     *  @return <code> true </code> if this process is enabled, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isEnabled() {
        return (this.process.isEnabled());
    }


    /**
     *  Gets the <code> Id </code> of the initial step of this process. All 
     *  work goes through an initial step. 
     *
     *  @return the step <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getInitialStepId() {
        return (this.process.getInitialStepId());
    }


    /**
     *  Gets the initial step of this process. 
     *
     *  @return the step 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Step getInitialStep()
        throws org.osid.OperationFailedException {

        return (this.process.getInitialStep());
    }


    /**
     *  Gets the <code> Id </code> of the initial state of the work upon 
     *  entering this process. 
     *
     *  @return the state <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getInitialStateId() {
        return (this.process.getInitialStateId());
    }


    /**
     *  Gets the initial state of the work upon entering this process. 
     *
     *  @return the state 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getInitialState()
        throws org.osid.OperationFailedException {

        return (this.process.getInitialState());
    }


    /**
     *  Gets the process record corresponding to the given <code> Process 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> processRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(processRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  processRecordType the type of process record to retrieve 
     *  @return the process record 
     *  @throws org.osid.NullArgumentException <code> processRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(processRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.records.ProcessRecord getProcessRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        return (this.process.getProcessRecord(processRecordType));
    }
}

