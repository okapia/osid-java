//
// AbstractWorkflowProxyManager.java
//
//     An adapter for a WorkflowProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a WorkflowProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterWorkflowProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.workflow.WorkflowProxyManager>
    implements org.osid.workflow.WorkflowProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterWorkflowProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterWorkflowProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterWorkflowProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterWorkflowProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any office federation is exposed. Federation is exposed when 
     *  a specific office may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  offices appears as a single office. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up process is supported. 
     *
     *  @return <code> true </code> if process lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessLookup() {
        return (getAdapteeManager().supportsProcessLookup());
    }


    /**
     *  Tests if querying process is supported. 
     *
     *  @return <code> true </code> if process query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (getAdapteeManager().supportsProcessQuery());
    }


    /**
     *  Tests if searching process is supported. 
     *
     *  @return <code> true </code> if process search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSearch() {
        return (getAdapteeManager().supportsProcessSearch());
    }


    /**
     *  Tests if process administrative service is supported. 
     *
     *  @return <code> true </code> if process administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessAdmin() {
        return (getAdapteeManager().supportsProcessAdmin());
    }


    /**
     *  Tests if a process notification service is supported. 
     *
     *  @return <code> true </code> if process notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessNotification() {
        return (getAdapteeManager().supportsProcessNotification());
    }


    /**
     *  Tests if a process office lookup service is supported. 
     *
     *  @return <code> true </code> if a process office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessOffice() {
        return (getAdapteeManager().supportsProcessOffice());
    }


    /**
     *  Tests if a process office service is supported. 
     *
     *  @return <code> true </code> if process to office assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessOfficeAssignment() {
        return (getAdapteeManager().supportsProcessOfficeAssignment());
    }


    /**
     *  Tests if a process smart office lookup service is supported. 
     *
     *  @return <code> true </code> if a process smart office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSmartOffice() {
        return (getAdapteeManager().supportsProcessSmartOffice());
    }


    /**
     *  Tests if looking up steps is supported. 
     *
     *  @return <code> true </code> if step lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepLookup() {
        return (getAdapteeManager().supportsStepLookup());
    }


    /**
     *  Tests if querying steps is supported. 
     *
     *  @return <code> true </code> if step query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (getAdapteeManager().supportsStepQuery());
    }


    /**
     *  Tests if searching steps is supported. 
     *
     *  @return <code> true </code> if step search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepSearch() {
        return (getAdapteeManager().supportsStepSearch());
    }


    /**
     *  Tests if a step administrative service is supported. 
     *
     *  @return <code> true </code> if step administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepAdmin() {
        return (getAdapteeManager().supportsStepAdmin());
    }


    /**
     *  Tests if a step <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if step notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepNotification() {
        return (getAdapteeManager().supportsStepNotification());
    }


    /**
     *  Tests if a step office lookup service is supported. 
     *
     *  @return <code> true </code> if a step office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepOffice() {
        return (getAdapteeManager().supportsStepOffice());
    }


    /**
     *  Tests if a step office assignment service is supported. 
     *
     *  @return <code> true </code> if a step to office assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepOfficeAssignment() {
        return (getAdapteeManager().supportsStepOfficeAssignment());
    }


    /**
     *  Tests if a step smart office service is supported. 
     *
     *  @return <code> true </code> if a step smart office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepSmartOffice() {
        return (getAdapteeManager().supportsStepSmartOffice());
    }


    /**
     *  Tests if looking up work is supported. 
     *
     *  @return <code> true </code> if work lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkLookup() {
        return (getAdapteeManager().supportsWorkLookup());
    }


    /**
     *  Tests if querying work is supported. 
     *
     *  @return <code> true </code> if work query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (getAdapteeManager().supportsWorkQuery());
    }


    /**
     *  Tests if searching work is supported. 
     *
     *  @return <code> true </code> if work search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSearch() {
        return (getAdapteeManager().supportsWorkSearch());
    }


    /**
     *  Tests if work administrative service is supported. 
     *
     *  @return <code> true </code> if work administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkAdmin() {
        return (getAdapteeManager().supportsWorkAdmin());
    }


    /**
     *  Tests if a work notification service is supported. 
     *
     *  @return <code> true </code> if work notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkNotification() {
        return (getAdapteeManager().supportsWorkNotification());
    }


    /**
     *  Tests if a work office lookup service is supported. 
     *
     *  @return <code> true </code> if a work office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkOffice() {
        return (getAdapteeManager().supportsWorkOffice());
    }


    /**
     *  Tests if a work office service is supported. 
     *
     *  @return <code> true </code> if work to office assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkOfficeAssignment() {
        return (getAdapteeManager().supportsWorkOfficeAssignment());
    }


    /**
     *  Tests if a work smart office lookup service is supported. 
     *
     *  @return <code> true </code> if a work smart office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSmartOffice() {
        return (getAdapteeManager().supportsWorkSmartOffice());
    }


    /**
     *  Tests if a workflow service is supported. 
     *
     *  @return <code> true </code> if workflow is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflow() {
        return (getAdapteeManager().supportsWorkflow());
    }


    /**
     *  Tests if a workflow initiation service is supported. 
     *
     *  @return <code> true </code> if workflow initiation is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowInitiation() {
        return (getAdapteeManager().supportsWorkflowInitiation());
    }


    /**
     *  Tests if a workflow management service is supported. 
     *
     *  @return <code> true </code> if workflow management is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowManagement() {
        return (getAdapteeManager().supportsWorkflowManagement());
    }


    /**
     *  Tests if a manual workflow service is supported. 
     *
     *  @return <code> true </code> if manual workflow is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsManualWorkflow() {
        return (getAdapteeManager().supportsManualWorkflow());
    }


    /**
     *  Tests if a workflow <code> </code> event lookup service is supported. 
     *
     *  @return <code> true </code> if workflow event lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowEventLookup() {
        return (getAdapteeManager().supportsWorkflowEventLookup());
    }


    /**
     *  Tests if a workflow event notification service is supported. 
     *
     *  @return <code> true </code> if a workflow event notification service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowEventNotification() {
        return (getAdapteeManager().supportsWorkflowEventNotification());
    }


    /**
     *  Tests if looking up offices is supported. 
     *
     *  @return <code> true </code> if office lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeLookup() {
        return (getAdapteeManager().supportsOfficeLookup());
    }


    /**
     *  Tests if querying offices is supported. 
     *
     *  @return <code> true </code> if a office query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (getAdapteeManager().supportsOfficeQuery());
    }


    /**
     *  Tests if searching offices is supported. 
     *
     *  @return <code> true </code> if office search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeSearch() {
        return (getAdapteeManager().supportsOfficeSearch());
    }


    /**
     *  Tests if office administrative service is supported. 
     *
     *  @return <code> true </code> if office administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeAdmin() {
        return (getAdapteeManager().supportsOfficeAdmin());
    }


    /**
     *  Tests if a office <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if office notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeNotification() {
        return (getAdapteeManager().supportsOfficeNotification());
    }


    /**
     *  Tests for the availability of a office hierarchy traversal service. 
     *
     *  @return <code> true </code> if office hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeHierarchy() {
        return (getAdapteeManager().supportsOfficeHierarchy());
    }


    /**
     *  Tests for the availability of a office hierarchy design service. 
     *
     *  @return <code> true </code> if office hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeHierarchyDesign() {
        return (getAdapteeManager().supportsOfficeHierarchyDesign());
    }


    /**
     *  Tests for the availability of a workflow batch service. 
     *
     *  @return <code> true </code> if a workflow batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowBatch() {
        return (getAdapteeManager().supportsWorkflowBatch());
    }


    /**
     *  Tests for the availability of a workflow rules service. 
     *
     *  @return <code> true </code> if a workflow rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowRules() {
        return (getAdapteeManager().supportsWorkflowRules());
    }


    /**
     *  Gets the supported <code> Process </code> record types. 
     *
     *  @return a list containing the supported <code> Process </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessRecordTypes() {
        return (getAdapteeManager().getProcessRecordTypes());
    }


    /**
     *  Tests if the given <code> Process </code> record type is supported. 
     *
     *  @param  processRecordType a <code> Type </code> indicating a <code> 
     *          Process </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessRecordType(org.osid.type.Type processRecordType) {
        return (getAdapteeManager().supportsProcessRecordType(processRecordType));
    }


    /**
     *  Gets the supported <code> Process </code> search record types. 
     *
     *  @return a list containing the supported <code> Process </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessSearchRecordTypes() {
        return (getAdapteeManager().getProcessSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Process </code> search record type is 
     *  supported. 
     *
     *  @param  processSearchRecordType a <code> Type </code> indicating a 
     *          <code> Process </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        return (getAdapteeManager().supportsProcessSearchRecordType(processSearchRecordType));
    }


    /**
     *  Gets the supported <code> Step </code> record types. 
     *
     *  @return a list containing the supported <code> Step </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepRecordTypes() {
        return (getAdapteeManager().getStepRecordTypes());
    }


    /**
     *  Tests if the given <code> Step </code> record type is supported. 
     *
     *  @param  stepRecordType a <code> Type </code> indicating a <code> Step 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stepRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepRecordType(org.osid.type.Type stepRecordType) {
        return (getAdapteeManager().supportsStepRecordType(stepRecordType));
    }


    /**
     *  Gets the supported <code> Step </code> search types. 
     *
     *  @return a list containing the supported <code> Step </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepSearchRecordTypes() {
        return (getAdapteeManager().getStepSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Step </code> search type is supported. 
     *
     *  @param  stepSearchRecordType a <code> Type </code> indicating a <code> 
     *          Step </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effiortSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepSearchRecordType(org.osid.type.Type stepSearchRecordType) {
        return (getAdapteeManager().supportsStepSearchRecordType(stepSearchRecordType));
    }


    /**
     *  Gets the supported <code> Work </code> record types. 
     *
     *  @return a list containing the supported <code> Work </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkRecordTypes() {
        return (getAdapteeManager().getWorkRecordTypes());
    }


    /**
     *  Tests if the given <code> Work </code> record type is supported. 
     *
     *  @param  workRecordType a <code> Type </code> indicating a <code> Work 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkRecordType(org.osid.type.Type workRecordType) {
        return (getAdapteeManager().supportsWorkRecordType(workRecordType));
    }


    /**
     *  Gets the supported <code> Work </code> search record types. 
     *
     *  @return a list containing the supported <code> Work </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkSearchRecordTypes() {
        return (getAdapteeManager().getWorkSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Work </code> search record type is 
     *  supported. 
     *
     *  @param  workSearchRecordType a <code> Type </code> indicating a <code> 
     *          Work </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        return (getAdapteeManager().supportsWorkSearchRecordType(workSearchRecordType));
    }


    /**
     *  Gets the supported <code> WorkflowEvent </code> record types. 
     *
     *  @return a list containing the supported <code> WorkflowEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkflowEventRecordTypes() {
        return (getAdapteeManager().getWorkflowEventRecordTypes());
    }


    /**
     *  Tests if the given <code> WorkflowEvent </code> record type is 
     *  supported. 
     *
     *  @param  workflowEventRecordType a <code> Type </code> indicating a 
     *          <code> WorkflowEvent </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workflowEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkflowEventRecordType(org.osid.type.Type workflowEventRecordType) {
        return (getAdapteeManager().supportsWorkflowEventRecordType(workflowEventRecordType));
    }


    /**
     *  Gets the supported <code> Office </code> record types. 
     *
     *  @return a list containing the supported <code> Office </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfficeRecordTypes() {
        return (getAdapteeManager().getOfficeRecordTypes());
    }


    /**
     *  Tests if the given <code> Office </code> record type is supported. 
     *
     *  @param  officeRecordType a <code> Type </code> indicating a <code> 
     *          Office </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> officeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfficeRecordType(org.osid.type.Type officeRecordType) {
        return (getAdapteeManager().supportsOfficeRecordType(officeRecordType));
    }


    /**
     *  Gets the supported <code> Office </code> search record types. 
     *
     *  @return a list containing the supported <code> Office </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfficeSearchRecordTypes() {
        return (getAdapteeManager().getOfficeSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Office </code> search record type is 
     *  supported. 
     *
     *  @param  officeSearchRecordType a <code> Type </code> indicating a 
     *          <code> Office </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> officeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfficeSearchRecordType(org.osid.type.Type officeSearchRecordType) {
        return (getAdapteeManager().supportsOfficeSearchRecordType(officeSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessLookupSession getProcessLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessLookupSession getProcessLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessLookupSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuerySession getProcessQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuerySession getProcessQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessQuerySessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchSession getProcessSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchSession getProcessSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessSearchSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessAdminSession getProcessAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessAdminSession getProcessAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessAdminSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  notification service. 
     *
     *  @param  processReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessNotificationSession getProcessNotificationSession(org.osid.workflow.ProcessReceiver processReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessNotificationSession(processReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  notification service for the given office. 
     *
     *  @param  processReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver, 
     *          officeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessNotificationSession getProcessNotificationSessionForOffice(org.osid.workflow.ProcessReceiver processReceiver, 
                                                                                               org.osid.id.Id officeId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessNotificationSessionForOffice(processReceiver, officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup process/office mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessOfficeSession getProcessOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessOfficeSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning process 
     *  to offices. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessOfficeAssignmentSession getProcessOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessOfficeAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage process smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSmartOfficeSession getProcessSmartOfficeSession(org.osid.id.Id officeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessSmartOfficeSession(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepLookupSession getStepLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepLookupSession getStepLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepLookupSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuerySession getStepQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuerySession getStepQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepQuerySessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchSession getStepSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchSession getStepSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepSearchSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepAdminSession getStepAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepAdminSession getStepAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepAdminSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  notification service. 
     *
     *  @param  stepReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> StepNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepNotificationSession getStepNotificationSession(org.osid.workflow.StepReceiver stepReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepNotificationSession(stepReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  notification service for the given office. 
     *
     *  @param  stepReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepReceiver, officeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepNotificationSession getStepNotificationSessionForOffice(org.osid.workflow.StepReceiver stepReceiver, 
                                                                                         org.osid.id.Id officeId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepNotificationSessionForOffice(stepReceiver, officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step/office mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStepOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeSession getStepOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepOfficeSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning steps to 
     *  offices. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> StepOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepOfficeAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeAssignmentSession getStepOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepOfficeAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return an <code> StepSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepOfficeSession getStepSmartOfficeSession(org.osid.id.Id officeId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepSmartOfficeSession(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkLookupSession getWorkLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkLookupSession getWorkLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkLookupSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuerySession getWorkQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuerySession getWorkQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkQuerySessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSearchSession getWorkSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSearchSession getWorkSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkSearchSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkAdminSession getWorkAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkAdminSession getWorkAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkAdminSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkNotificationSession getWorkNotificationSession(org.osid.workflow.WorkReceiver workReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkNotificationSession(workReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service for the given office. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver, officeId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkNotificationSession getWorkNotificationSessionForOffice(org.osid.workflow.WorkReceiver workReceiver, 
                                                                                         org.osid.id.Id officeId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkNotificationSessionForOffice(workReceiver, officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup work/office mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkOffice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkOfficeSession getWorkOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkOfficeSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning work to 
     *  offices. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkbOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkOfficeAssignmentSession getWorkOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkOfficeAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage work smart offices. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException no <code> Office </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkSmartOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkSmartOfficeSession getWorkSmartOfficeSession(org.osid.id.Id officeId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkSmartOfficeSession(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflow() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowSession getWorkflowSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow service for the 
     *  given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflow() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowSession getWorkflowSessionForOffice(org.osid.id.Id officeId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow initiation service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowInitiationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowInitiation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowInitiationSession getWorkflowInitiationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowInitiationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow initiation service 
     *  for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowInitiationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowInitiation() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowInitiationSession getWorkflowInitiationSessionForOffice(org.osid.id.Id officeId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowInitiationSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow management service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowManagementSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManagementSession getWorkflowManagementSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowManagementSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow management service 
     *  for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowManagementSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowManagement() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManagementSession getWorkflowManagementSessionForOffice(org.osid.id.Id officeId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowManagementSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a manual workflow service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ManualWorkflowSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManualWorkflow() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ManualWorkflowSession getManualWorkflowSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getManualWorkflowSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a manual workflow service for 
     *  the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ManualWorkflowSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManualWorkflow() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ManualWorkflowSession getManualWorkflowSessionForOffice(org.osid.id.Id officeId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getManualWorkflowSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow event lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventLookupSession getWorkflowEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowEventLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> for a workflow event lookup 
     *  service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventLookupSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventLookupSession getWorkflowEventLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowEventLookupSessionForOffice(officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the workflow event 
     *  notification service. 
     *
     *  @param  workflowEventReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workflowEventReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventNotificationSession getWorkflowEventNotificationSession(org.osid.workflow.WorkflowEventReceiver workflowEventReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowEventNotificationSession(workflowEventReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the workflow event 
     *  notification service for the given office. 
     *
     *  @param  workflowEventReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkflowEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> workflowEventReceiver, 
     *          officeId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventNotificationSession getWorkflowEventNotificationSessionForOffice(org.osid.workflow.WorkflowEventReceiver workflowEventReceiver, 
                                                                                                           org.osid.id.Id officeId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowEventNotificationSessionForOffice(workflowEventReceiver, officeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeLookupSession getOfficeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuerySession getOfficeQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeSearchSession getOfficeSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeAdminSession getOfficeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  notification service. 
     *
     *  @param  officeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> OfficeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> officeReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeNotificationSession getOfficeNotificationSession(org.osid.workflow.OfficeReceiver officeReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeNotificationSession(officeReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OfficeHierarchySession </code> for offices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeHierarchySession getOfficeHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the office 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for offices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeHierarchyDesignSession getOfficeHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> WorkflowBatchProxyManager. </code> 
     *
     *  @return a <code> WorkflowbatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflowBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.WorkflowBatchProxyManager getWorkflowBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowBatchProxyManager());
    }


    /**
     *  Gets a <code> WorkflowRulesProxyManager </code> . 
     *
     *  @return a <code> WorkflowRulesProxyManager </code> for offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkflowRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.WorkflowRulesProxyManager getWorkflowRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
