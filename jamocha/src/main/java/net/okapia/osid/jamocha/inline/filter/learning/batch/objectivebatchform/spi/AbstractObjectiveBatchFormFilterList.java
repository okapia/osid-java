//
// AbstractObjectiveBatchFormList
//
//     Implements a filter for an ObjectiveBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.learning.batch.objectivebatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an ObjectiveBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedObjectiveBatchFormList
 *  to improve performance.
 */

public abstract class AbstractObjectiveBatchFormFilterList
    extends net.okapia.osid.jamocha.learning.batch.objectivebatchform.spi.AbstractObjectiveBatchFormList
    implements org.osid.learning.batch.ObjectiveBatchFormList,
               net.okapia.osid.jamocha.inline.filter.learning.batch.objectivebatchform.ObjectiveBatchFormFilter {

    private org.osid.learning.batch.ObjectiveBatchForm objectiveBatchForm;
    private final org.osid.learning.batch.ObjectiveBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractObjectiveBatchFormFilterList</code>.
     *
     *  @param objectiveBatchFormList an <code>ObjectiveBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBatchFormList</code> is <code>null</code>
     */

    protected AbstractObjectiveBatchFormFilterList(org.osid.learning.batch.ObjectiveBatchFormList objectiveBatchFormList) {
        nullarg(objectiveBatchFormList, "objective batch form list");
        this.list = objectiveBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.objectiveBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ObjectiveBatchForm </code> in this list. 
     *
     *  @return the next <code> ObjectiveBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ObjectiveBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.batch.ObjectiveBatchForm getNextObjectiveBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.learning.batch.ObjectiveBatchForm objectiveBatchForm = this.objectiveBatchForm;
            this.objectiveBatchForm = null;
            return (objectiveBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in objective batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.objectiveBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ObjectiveBatchForms.
     *
     *  @param objectiveBatchForm the objective batch form to filter
     *  @return <code>true</code> if the objective batch form passes the filter,
     *          <code>false</code> if the objective batch form should be filtered
     */

    public abstract boolean pass(org.osid.learning.batch.ObjectiveBatchForm objectiveBatchForm);


    protected void prime() {
        if (this.objectiveBatchForm != null) {
            return;
        }

        org.osid.learning.batch.ObjectiveBatchForm objectiveBatchForm = null;

        while (this.list.hasNext()) {
            try {
                objectiveBatchForm = this.list.getNextObjectiveBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(objectiveBatchForm)) {
                this.objectiveBatchForm = objectiveBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
