//
// AbstractAdapterRouteLookupSession.java
//
//    A Route lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Route lookup session adapter.
 */

public abstract class AbstractAdapterRouteLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.route.RouteLookupSession {

    private final org.osid.mapping.route.RouteLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRouteLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRouteLookupSession(org.osid.mapping.route.RouteLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code Route} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRoutes() {
        return (this.session.canLookupRoutes());
    }


    /**
     *  A complete view of the {@code Route} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRouteView() {
        this.session.useComparativeRouteView();
        return;
    }


    /**
     *  A complete view of the {@code Route} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRouteView() {
        this.session.usePlenaryRouteView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include routes in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only routes whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRouteView() {
        this.session.useEffectiveRouteView();
        return;
    }
    

    /**
     *  All routes of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRouteView() {
        this.session.useAnyEffectiveRouteView();
        return;
    }

     
    /**
     *  Gets the {@code Route} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Route} may
     *  have a different {@code Id} than requested, such as the case
     *  where a duplicate {@code Id} was assigned to a {@code Route}
     *  and retained for compatibility.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param routeId {@code Id} of the {@code Route}
     *  @return the route
     *  @throws org.osid.NotFoundException {@code routeId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code routeId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.Route getRoute(org.osid.id.Id routeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoute(routeId));
    }


    /**
     *  Gets a {@code RouteList} corresponding to the given {@code
     *  IdList}.
     *
     *  In plenary mode, the returned list contains all of the routes
     *  specified in the {@code Id} list, in the order of the list,
     *  including duplicates, or an error results if an {@code Id} in
     *  the supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Routes} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effectiqve mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  routeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Route} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code routeIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByIds(org.osid.id.IdList routeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesByIds(routeIds));
    }


    /**
     *  Gets a {@code RouteList} corresponding to the given route
     *  genus {@code Type} which does not include routes of types
     *  derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  routeGenusType a route genus type 
     *  @return the returned {@code Route} list
     *  @throws org.osid.NullArgumentException
     *          {@code routeGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByGenusType(org.osid.type.Type routeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesByGenusType(routeGenusType));
    }


    /**
     *  Gets a {@code RouteList} corresponding to the given route
     *  genus {@code Type} and include any additional routes with
     *  genus types derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  routeGenusType a route genus type 
     *  @return the returned {@code Route} list
     *  @throws org.osid.NullArgumentException
     *          {@code routeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByParentGenusType(org.osid.type.Type routeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesByParentGenusType(routeGenusType));
    }


    /**
     *  Gets a {@code RouteList} containing the given route record
     *  {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  routeRecordType a route record type 
     *  @return the returned {@code Route} list
     *  @throws org.osid.NullArgumentException
     *          {@code routeRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesByRecordType(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesByRecordType(routeRecordType));
    }


    /**
     *  Gets a {@code RouteList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *  
     *  In active mode, routes are returned that are currently
     *  active. In any status mode, active and inactive routes are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Route} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesOnDate(from, to));
    }
        

    /**
     *  Gets a list of routes corresponding to a starting location
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  startingLocationId the {@code Id} of the starting location
     *  @return the returned {@code RouteList}
     *  @throws org.osid.NullArgumentException {@code startingLocationId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingLocation(org.osid.id.Id startingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesForStartingLocation(startingLocationId));
    }


    /**
     *  Gets a list of routes corresponding to a starting location
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  startingLocationId the {@code Id} of the starting location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RouteList}
     *  @throws org.osid.NullArgumentException {@code startingLocationId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingLocationOnDate(org.osid.id.Id startingLocationId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesForStartingLocationOnDate(startingLocationId, from, to));
    }


    /**
     *  Gets a list of routes corresponding to a ending location
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible
     *  through this session.
     *
     *  In effective mode, routes are returned that are
     *  currently effective.  In any effective mode, effective
     *  routes and those currently expired are returned.
     *
     *  @param  endingLocationId the {@code Id} of the ending location
     *  @return the returned {@code RouteList}
     *  @throws org.osid.NullArgumentException {@code endingLocationId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForEndingLocation(org.osid.id.Id endingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesForEndingLocation(endingLocationId));
    }


    /**
     *  Gets a list of routes corresponding to a ending location
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  endingLocationId the {@code Id} of the ending location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RouteList}
     *  @throws org.osid.NullArgumentException {@code endingLocationId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForEndingLocationOnDate(org.osid.id.Id endingLocationId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesForEndingLocationOnDate(endingLocationId, from, to));
    }


    /**
     *  Gets a list of routes corresponding to starting location and
     *  ending location {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  startingLocationId the {@code Id} of the starting location
     *  @param  endingLocationId the {@code Id} of the ending location
     *  @return the returned {@code RouteList}
     *  @throws org.osid.NullArgumentException {@code startingLocationId},
     *          {@code endingLocationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingAndEndingLocation(org.osid.id.Id startingLocationId,
                                                                                  org.osid.id.Id endingLocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesForStartingAndEndingLocation(startingLocationId, endingLocationId));
    }


    /**
     *  Gets a list of routes corresponding to starting location and
     *  ending location {@code Ids} and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known routes
     *  or an error results. Otherwise, the returned list may contain
     *  only those routes that are accessible through this session.
     *
     *  In effective mode, routes are returned that are currently
     *  effective. In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  endingLocationId the {@code Id} of the ending location
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RouteList}
     *  @throws org.osid.NullArgumentException {@code startingLocationId},
     *          {@code endingLocationId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesForStartingAndEndingLocationOnDate(org.osid.id.Id startingLocationId,
                                                                                        org.osid.id.Id endingLocationId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesForStartingAndEndingLocationOnDate(startingLocationId, endingLocationId, from, to));
    }


    /**
     *  Gets a {@code RouteList} connected to all the given {@code
     *  Locations}.
     *  
     *  In plenary mode, the returned list contains all of the routes
     *  along the locations, or an error results if a route connected
     *  to the location is not found or inaccessible. Otherwise,
     *  inaccessible {@code Routes} may be omitted from the list.
     *  
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and those
     *  currently expired are returned.
     *
     *  @param  locationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Route} list 
     *  @throws org.osid.NullArgumentException {@code locationIds} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutesAlongLocations(org.osid.id.IdList locationIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutesAlongLocations(locationIds));
    }


    /**
     *  Gets all {@code Routes}. 
     *
     *  In plenary mode, the returned list contains all known
     *  routes or an error results. Otherwise, the returned list
     *  may contain only those routes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, routes are returned that are currently
     *  effective.  In any effective mode, effective routes and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Routes} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoutes());
    }
}
