//
// AbstractAdapterPriceEnablerLookupSession.java
//
//    A PriceEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A PriceEnabler lookup session adapter.
 */

public abstract class AbstractAdapterPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {

    private final org.osid.ordering.rules.PriceEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPriceEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPriceEnablerLookupSession(org.osid.ordering.rules.PriceEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Store/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Store Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the {@code Store} associated with this session.
     *
     *  @return the {@code Store} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform {@code PriceEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPriceEnablers() {
        return (this.session.canLookupPriceEnablers());
    }


    /**
     *  A complete view of the {@code PriceEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePriceEnablerView() {
        this.session.useComparativePriceEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code PriceEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPriceEnablerView() {
        this.session.usePlenaryPriceEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price enablers in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    

    /**
     *  Only active price enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePriceEnablerView() {
        this.session.useActivePriceEnablerView();
        return;
    }


    /**
     *  Active and inactive price enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPriceEnablerView() {
        this.session.useAnyStatusPriceEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code PriceEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code PriceEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code PriceEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param priceEnablerId {@code Id} of the {@code PriceEnabler}
     *  @return the price enabler
     *  @throws org.osid.NotFoundException {@code priceEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code priceEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnabler getPriceEnabler(org.osid.id.Id priceEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceEnabler(priceEnablerId));
    }


    /**
     *  Gets a {@code PriceEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  priceEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code PriceEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code PriceEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code priceEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByIds(org.osid.id.IdList priceEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceEnablersByIds(priceEnablerIds));
    }


    /**
     *  Gets a {@code PriceEnablerList} corresponding to the given
     *  price enabler genus {@code Type} which does not include
     *  price enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned {@code PriceEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code priceEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceEnablersByGenusType(priceEnablerGenusType));
    }


    /**
     *  Gets a {@code PriceEnablerList} corresponding to the given
     *  price enabler genus {@code Type} and include any additional
     *  price enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned {@code PriceEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code priceEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByParentGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceEnablersByParentGenusType(priceEnablerGenusType));
    }


    /**
     *  Gets a {@code PriceEnablerList} containing the given
     *  price enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerRecordType a priceEnabler record type 
     *  @return the returned {@code PriceEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code priceEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByRecordType(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceEnablersByRecordType(priceEnablerRecordType));
    }


    /**
     *  Gets a {@code PriceEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible
     *  through this session.
     *  
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code PriceEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code PriceEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible
     *  through this session.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code PriceEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getPriceEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code PriceEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @return a list of {@code PriceEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPriceEnablers());
    }
}
