//
// PlanElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.plan.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PlanElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the PlanElement Id.
     *
     *  @return the plan element Id
     */

    public static org.osid.id.Id getPlanEntityId() {
        return (makeEntityId("osid.course.plan.Plan"));
    }


    /**
     *  Gets the SyllabusId element Id.
     *
     *  @return the SyllabusId element Id
     */

    public static org.osid.id.Id getSyllabusId() {
        return (makeElementId("osid.course.plan.plan.SyllabusId"));
    }


    /**
     *  Gets the Syllabus element Id.
     *
     *  @return the Syllabus element Id
     */

    public static org.osid.id.Id getSyllabus() {
        return (makeElementId("osid.course.plan.plan.Syllabus"));
    }


    /**
     *  Gets the CourseOfferingId element Id.
     *
     *  @return the CourseOfferingId element Id
     */

    public static org.osid.id.Id getCourseOfferingId() {
        return (makeElementId("osid.course.plan.plan.CourseOfferingId"));
    }


    /**
     *  Gets the CourseOffering element Id.
     *
     *  @return the CourseOffering element Id
     */

    public static org.osid.id.Id getCourseOffering() {
        return (makeElementId("osid.course.plan.plan.CourseOffering"));
    }


    /**
     *  Gets the ModuleIds element Id.
     *
     *  @return the ModuleIds element Id
     */

    public static org.osid.id.Id getModuleIds() {
        return (makeElementId("osid.course.plan.plan.ModuleIds"));
    }


    /**
     *  Gets the Modules element Id.
     *
     *  @return the Modules element Id
     */

    public static org.osid.id.Id getModules() {
        return (makeElementId("osid.course.plan.plan.Modules"));
    }


    /**
     *  Gets the LessonId element Id.
     *
     *  @return the LessonId element Id
     */

    public static org.osid.id.Id getLessonId() {
        return (makeQueryElementId("osid.course.plan.plan.LessonId"));
    }


    /**
     *  Gets the Lesson element Id.
     *
     *  @return the Lesson element Id
     */

    public static org.osid.id.Id getLesson() {
        return (makeQueryElementId("osid.course.plan.plan.Lesson"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.plan.plan.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.plan.plan.CourseCatalog"));
    }
}
