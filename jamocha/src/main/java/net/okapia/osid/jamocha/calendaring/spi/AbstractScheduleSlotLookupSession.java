//
// AbstractScheduleSlotLookupSession.java
//
//    A starter implementation framework for providing a ScheduleSlot
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ScheduleSlot
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getScheduleSlots(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractScheduleSlotLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.ScheduleSlotLookupSession {

    private boolean pedantic    = false;
    private boolean federated   = false;
    private boolean sequestered = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>ScheduleSlot</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupScheduleSlots() {
        return (true);
    }


    /**
     *  A complete view of the <code>ScheduleSlot</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeScheduleSlotView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ScheduleSlot</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryScheduleSlotView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedule slots in calendars which are
     *  children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  The returns from the lookup methods omit sequestered events.
     */

    @OSID @Override
    public void useSequesteredScheduleSlotView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All events are returned including sequestered events.
     */

    @OSID @Override
    public void useUnsequesteredScheduleSlotView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if undequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }

     
    /**
     *  Gets the <code>ScheduleSlot</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ScheduleSlot</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ScheduleSlot</code> and
     *  retained for compatibility.
     *
     *  @param  scheduleSlotId <code>Id</code> of the
     *          <code>ScheduleSlot</code>
     *  @return the schedule slot
     *  @throws org.osid.NotFoundException <code>scheduleSlotId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>scheduleSlotId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.ScheduleSlotList scheduleSlots = getScheduleSlots()) {
            while (scheduleSlots.hasNext()) {
                org.osid.calendaring.ScheduleSlot scheduleSlot = scheduleSlots.getNextScheduleSlot();
                if (scheduleSlot.getId().equals(scheduleSlotId)) {
                    return (scheduleSlot);
                }
            }
        } 

        throw new org.osid.NotFoundException(scheduleSlotId + " not found");
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  scheduleSlots specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ScheduleSlots</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getScheduleSlots()</code>.
     *
     *  @param  scheduleSlotIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByIds(org.osid.id.IdList scheduleSlotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.ScheduleSlot> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = scheduleSlotIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getScheduleSlot(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("schedule slot " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.scheduleslot.LinkedScheduleSlotList(ret));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  schedule slot genus <code>Type</code> which does not include
     *  schedule slots of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getScheduleSlots()</code>.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.ScheduleSlotGenusFilterList(getScheduleSlots(), scheduleSlotGenusType));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  schedule slot genus <code>Type</code> and include any additional
     *  schedule slots with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getScheduleSlots()</code>.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByParentGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getScheduleSlotsByGenusType(scheduleSlotGenusType));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> containing the given
     *  schedule slot record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getScheduleSlots()</code>.
     *
     *  @param  scheduleSlotRecordType a scheduleSlot record type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByRecordType(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.ScheduleSlotRecordFilterList(getScheduleSlots(), scheduleSlotRecordType));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> containing the given
     *  set of weekdays.
     *  
     *  In plenary mode, the returned list contains all known schedule
     *  slots or an error results. Otherwise, the returned list may
     *  contain only those schedule slots that are accessible through
     *  this session.
     *  
     *  In sequestered mode, no sequestered schedule slots are
     *  returned. In unsequestered mode, all schedule slots are
     *  returned.
     *
     *  @param  weekdays a set of weekdays 
     *  @return the returned <code> ScheduleSlot </code> list 
     *  @throws org.osid.InvalidArgumentException a <code> weekday </code> is 
     *          negative 
     *  @throws org.osid.NullArgumentException <code> weekdays </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByWeekdays(long[] weekdays)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.ScheduleSlotFilterList(new WeekdayFilter(weekdays), getScheduleSlots()));
    }        


    /**
     *  Gets a <code> ScheduleSlotList </code> matching the given
     *  time.
     *  
     *  In plenary mode, the returned list contains all known schedule
     *  slots or an error results. Otherwise, the returned list may
     *  contain only those schedule slots that are accessible through
     *  this session.
     *  
     *  In sequestered mode, no sequestered schedule slots are
     *  returned. In unsequestered mode, all schedule slots are
     *  returned.
     *
     *  @param  time a time 
     *  @return the returned <code> ScheduleSlot </code> list 
     *  @throws org.osid.NullArgumentException <code> time </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByTime(org.osid.calendaring.Time time)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.ScheduleSlotFilterList(new TimeFilter(time), getScheduleSlots()));
    }        

    
    /**
     *  Gets all <code>ScheduleSlots</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ScheduleSlots</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.ScheduleSlotList getScheduleSlots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the schedule slot list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of schedule slots
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.ScheduleSlotList filterScheduleSlotsOnViews(org.osid.calendaring.ScheduleSlotList list)
        throws org.osid.OperationFailedException {
            
        org.osid.calendaring.ScheduleSlotList ret = list;

        if (isSequestered()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.SequesteredScheduleSlotFilterList(ret);
        }

        return (ret);
    }


    public static class WeekdayFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.ScheduleSlotFilter {
         
        private final long[] weekdays;
         
         
        /**
         *  Constructs a new <code>WeekdayFilter</code>.
         *
         *  @param weekdays the weekdays to filter
         *  @throws org.osid.NullArgumentException
         *          <code>weekdays</code> is <code>null</code>
         */
        
        public WeekdayFilter(long[] weekdays) {
            nullarg(weekdays, "weekdays");

            this.weekdays = weekdays;
            java.util.Arrays.sort(this.weekdays);

            return;
        }

         
        /**
         *  Used by the ScheduleSlotFilterList to filter the 
         *  schedule slot list based on weekday.
         *
         *  @param scheduleSlot the schedule slot
         *  @return <code>true</code> to pass the schedule slot,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.ScheduleSlot scheduleSlot) {
            if (scheduleSlot.hasWeeklyInterval()) {
                long[] weekdays = scheduleSlot.getWeekdays();

                if (weekdays.length != this.weekdays.length) {
                    return (false);
                }

                java.util.Arrays.sort(weekdays);

                for (int i = 0; i <  weekdays.length; i++) {
                    if (weekdays[i] != this.weekdays[i]) {
                        return (false);
                    }
                }
                    
                return (true);
            } else {
                return (false);
            }
        }
    }


    public static class TimeFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.ScheduleSlotFilter {
         
        private final org.osid.calendaring.Time time;
         
         
        /**
         *  Constructs a new <code>TimeFilter</code>.
         *
         *  @param time the time to filter
         *  @throws org.osid.NullArgumentException <code>time</code>
         *          is <code>null</code>
         */
        
        public TimeFilter(org.osid.calendaring.Time time) {
            nullarg(time, "time");
            this.time = time;
            return;
        }

         
        /**
         *  Used by the ScheduleSlotFilterList to filter the schedule
         *  slot list based on time.
         *
         *  @param scheduleSlot the schedule slot
         *  @return <code>true</code> to pass the schedule slot,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.ScheduleSlot scheduleSlot) {
            if (scheduleSlot.hasWeeklyInterval()) {
                return(scheduleSlot.getWeekdayTime().equals(this.time));
            } else {
                return (false);
            }
        }
    }
}
