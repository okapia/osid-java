//
// AbstractQueryQualifierLookupSession.java
//
//    An inline adapter that maps a QualifierLookupSession to
//    a QualifierQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a QualifierLookupSession to
 *  a QualifierQuerySession.
 */

public abstract class AbstractQueryQualifierLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractQualifierLookupSession
    implements org.osid.authorization.QualifierLookupSession {

    private final org.osid.authorization.QualifierQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryQualifierLookupSession.
     *
     *  @param querySession the underlying qualifier query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryQualifierLookupSession(org.osid.authorization.QualifierQuerySession querySession) {
        nullarg(querySession, "qualifier query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Vault</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform <code>Qualifier</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQualifiers() {
        return (this.session.canSearchQualifiers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include qualifiers in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    
     
    /**
     *  Gets the <code>Qualifier</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Qualifier</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Qualifier</code> and
     *  retained for compatibility.
     *
     *  @param  qualifierId <code>Id</code> of the
     *          <code>Qualifier</code>
     *  @return the qualifier
     *  @throws org.osid.NotFoundException <code>qualifierId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>qualifierId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Qualifier getQualifier(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.QualifierQuery query = getQuery();
        query.matchId(qualifierId, true);
        org.osid.authorization.QualifierList qualifiers = this.session.getQualifiersByQuery(query);
        if (qualifiers.hasNext()) {
            return (qualifiers.getNextQualifier());
        } 
        
        throw new org.osid.NotFoundException(qualifierId + " not found");
    }


    /**
     *  Gets a <code>QualifierList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  qualifiers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Qualifiers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  qualifierIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByIds(org.osid.id.IdList qualifierIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.QualifierQuery query = getQuery();

        try (org.osid.id.IdList ids = qualifierIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getQualifiersByQuery(query));
    }


    /**
     *  Gets a <code>QualifierList</code> corresponding to the given
     *  qualifier genus <code>Type</code> which does not include
     *  qualifiers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierGenusType a qualifier genus type 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByGenusType(org.osid.type.Type qualifierGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.QualifierQuery query = getQuery();
        query.matchGenusType(qualifierGenusType, true);
        return (this.session.getQualifiersByQuery(query));
    }


    /**
     *  Gets a <code>QualifierList</code> corresponding to the given
     *  qualifier genus <code>Type</code> and include any additional
     *  qualifiers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierGenusType a qualifier genus type 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByParentGenusType(org.osid.type.Type qualifierGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.QualifierQuery query = getQuery();
        query.matchParentGenusType(qualifierGenusType, true);
        return (this.session.getQualifiersByQuery(query));
    }


    /**
     *  Gets a <code>QualifierList</code> containing the given
     *  qualifier record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierRecordType a qualifier record type 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByRecordType(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.QualifierQuery query = getQuery();
        query.matchRecordType(qualifierRecordType, true);
        return (this.session.getQualifiersByQuery(query));
    }

    
    /**
     *  Gets all <code>Qualifiers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Qualifiers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.QualifierQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getQualifiersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authorization.QualifierQuery getQuery() {
        org.osid.authorization.QualifierQuery query = this.session.getQualifierQuery();
        return (query);
    }
}
