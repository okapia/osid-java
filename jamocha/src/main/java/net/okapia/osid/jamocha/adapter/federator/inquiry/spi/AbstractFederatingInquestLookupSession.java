//
// AbstractFederatingInquestLookupSession.java
//
//     An abstract federating adapter for an InquestLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  InquestLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingInquestLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inquiry.InquestLookupSession>
    implements org.osid.inquiry.InquestLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingInquestLookupSession</code>.
     */

    protected AbstractFederatingInquestLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inquiry.InquestLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Inquest</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInquests() {
        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            if (session.canLookupInquests()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Inquest</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInquestView() {
        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            session.useComparativeInquestView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Inquest</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInquestView() {
        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            session.usePlenaryInquestView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Inquest</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Inquest</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Inquest</code> and
     *  retained for compatibility.
     *
     *  @param  inquestId <code>Id</code> of the
     *          <code>Inquest</code>
     *  @return the inquest
     *  @throws org.osid.NotFoundException <code>inquestId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inquestId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            try {
                return (session.getInquest(inquestId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(inquestId + " not found");
    }


    /**
     *  Gets an <code>InquestList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inquests specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Inquests</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inquestIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByIds(org.osid.id.IdList inquestIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inquiry.inquest.MutableInquestList ret = new net.okapia.osid.jamocha.inquiry.inquest.MutableInquestList();

        try (org.osid.id.IdList ids = inquestIds) {
            while (ids.hasNext()) {
                ret.addInquest(getInquest(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>InquestList</code> corresponding to the given
     *  inquest genus <code>Type</code> which does not include
     *  inquests of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestGenusType an inquest genus type 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByGenusType(org.osid.type.Type inquestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.FederatingInquestList ret = getInquestList();

        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            ret.addInquestList(session.getInquestsByGenusType(inquestGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquestList</code> corresponding to the given
     *  inquest genus <code>Type</code> and include any additional
     *  inquests with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestGenusType an inquest genus type 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByParentGenusType(org.osid.type.Type inquestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.FederatingInquestList ret = getInquestList();

        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            ret.addInquestList(session.getInquestsByParentGenusType(inquestGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquestList</code> containing the given
     *  inquest record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestRecordType an inquest record type 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByRecordType(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.FederatingInquestList ret = getInquestList();

        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            ret.addInquestList(session.getInquestsByRecordType(inquestRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquestList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known inquests or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  inquests that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Inquest</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.FederatingInquestList ret = getInquestList();

        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            ret.addInquestList(session.getInquestsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Inquests</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Inquests</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.FederatingInquestList ret = getInquestList();

        for (org.osid.inquiry.InquestLookupSession session : getSessions()) {
            ret.addInquestList(session.getInquests());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.FederatingInquestList getInquestList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.ParallelInquestList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.inquest.CompositeInquestList());
        }
    }
}
