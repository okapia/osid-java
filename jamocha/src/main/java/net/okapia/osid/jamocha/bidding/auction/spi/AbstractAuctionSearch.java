//
// AbstractAuctionSearch.java
//
//     A template for making an Auction Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing auction searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuctionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.bidding.AuctionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.bidding.records.AuctionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.bidding.AuctionSearchOrder auctionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of auctions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  auctionIds list of auctions
     *  @throws org.osid.NullArgumentException
     *          <code>auctionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuctions(org.osid.id.IdList auctionIds) {
        while (auctionIds.hasNext()) {
            try {
                this.ids.add(auctionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuctions</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of auction Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuctionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  auctionSearchOrder auction search order 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>auctionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuctionResults(org.osid.bidding.AuctionSearchOrder auctionSearchOrder) {
	this.auctionSearchOrder = auctionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.bidding.AuctionSearchOrder getAuctionSearchOrder() {
	return (this.auctionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given auction search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction implementing the requested record.
     *
     *  @param auctionSearchRecordType an auction search record
     *         type
     *  @return the auction search record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionSearchRecord getAuctionSearchRecord(org.osid.type.Type auctionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.bidding.records.AuctionSearchRecord record : this.records) {
            if (record.implementsRecordType(auctionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction search. 
     *
     *  @param auctionSearchRecord auction search record
     *  @param auctionSearchRecordType auction search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionSearchRecord(org.osid.bidding.records.AuctionSearchRecord auctionSearchRecord, 
                                           org.osid.type.Type auctionSearchRecordType) {

        addRecordType(auctionSearchRecordType);
        this.records.add(auctionSearchRecord);        
        return;
    }
}
