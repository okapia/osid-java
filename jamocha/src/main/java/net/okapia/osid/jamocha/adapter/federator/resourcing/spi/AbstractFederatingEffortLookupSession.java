//
// AbstractFederatingEffortLookupSession.java
//
//     An abstract federating adapter for an EffortLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EffortLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEffortLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resourcing.EffortLookupSession>
    implements org.osid.resourcing.EffortLookupSession {

    private boolean parallel = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();


    /**
     *  Constructs a new <code>AbstractFederatingEffortLookupSession</code>.
     */

    protected AbstractFederatingEffortLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resourcing.EffortLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Effort</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEfforts() {
        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            if (session.canLookupEfforts()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Effort</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEffortView() {
        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            session.useComparativeEffortView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Effort</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEffortView() {
        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            session.usePlenaryEffortView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include efforts in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            session.useFederatedFoundryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            session.useIsolatedFoundryView();
        }

        return;
    }


    /**
     *  Only efforts whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveEffortView() {
        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            session.useEffectiveEffortView();
        }

        return;
    }


    /**
     *  All efforts of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveEffortView() {
        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            session.useAnyEffectiveEffortView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Effort</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Effort</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Effort</code> and
     *  retained for compatibility.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @param  effortId <code>Id</code> of the
     *          <code>Effort</code>
     *  @return the effort
     *  @throws org.osid.NotFoundException <code>effortId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>effortId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Effort getEffort(org.osid.id.Id effortId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            try {
                return (session.getEffort(effortId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(effortId + " not found");
    }


    /**
     *  Gets an <code>EffortList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  efforts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Efforts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>effortIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByIds(org.osid.id.IdList effortIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resourcing.effort.MutableEffortList ret = new net.okapia.osid.jamocha.resourcing.effort.MutableEffortList();

        try (org.osid.id.IdList ids = effortIds) {
            while (ids.hasNext()) {
                ret.addEffort(getEffort(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EffortList</code> corresponding to the given
     *  effort genus <code>Type</code> which does not include
     *  efforts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortGenusType an effort genus type 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByGenusType(org.osid.type.Type effortGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsByGenusType(effortGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EffortList</code> corresponding to the given
     *  effort genus <code>Type</code> and include any additional
     *  efforts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortGenusType an effort genus type 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByParentGenusType(org.osid.type.Type effortGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsByParentGenusType(effortGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EffortList</code> containing the given
     *  effort record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortRecordType an effort record type 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByRecordType(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsByRecordType(effortRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EffortList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *  
     *  In active mode, efforts are returned that are currently
     *  active. In any status mode, active and inactive efforts
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Effort</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsOnDate(org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of efforts corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.EffortList getEffortsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to a commission
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  commissionId the <code>Id</code> of the commission
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>commissionId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.EffortList getEffortsForCommission(org.osid.id.Id commissionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForCommission(commissionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to a commission
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  commissionId the <code>Id</code> of the commission
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>commissionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForCommissionOnDate(org.osid.id.Id commissionId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForCommissionOnDate(commissionId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to resource and commission
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  commissionId the <code>Id</code> of the commission
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>commissionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndCommission(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id commissionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForResourceAndCommission(resourceId, commissionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to resource and commission
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  commissionId the <code>Id</code> of the commission
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>commissionId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndCommissionOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id commissionId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForResourceAndCommissionOnDate(resourceId, commissionId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to a work
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>workId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.EffortList getEffortsForWork(org.osid.id.Id workId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForWork(workId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to a work
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForWorkOnDate(org.osid.id.Id workId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForWorkOnDate(workId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to resource and work
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  workId the <code>Id</code> of the work
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>workId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndWork(org.osid.id.Id resourceId,
                                                                       org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForResourceAndWork(resourceId, workId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of efforts corresponding to resource and work
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>workId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndWorkOnDate(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id workId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEffortsForResourceAndWorkOnDate(resourceId, workId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Efforts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Efforts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEfforts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList ret = getEffortList();

        for (org.osid.resourcing.EffortLookupSession session : getSessions()) {
            ret.addEffortList(session.getEfforts());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resourcing.effort.FederatingEffortList getEffortList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.effort.ParallelEffortList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.effort.CompositeEffortList());
        }
    }
}
