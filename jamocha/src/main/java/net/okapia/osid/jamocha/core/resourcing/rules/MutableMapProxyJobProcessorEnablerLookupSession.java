//
// MutableMapProxyJobProcessorEnablerLookupSession
//
//    Implements a JobProcessorEnabler lookup service backed by a collection of
//    jobProcessorEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobProcessorEnabler lookup service backed by a collection of
 *  jobProcessorEnablers. The jobProcessorEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of job processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyJobProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobProcessorEnablerLookupSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyJobProcessorEnablerLookupSession}
     *  with no job processor enablers.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyJobProcessorEnablerLookupSession} with a
     *  single job processor enabler.
     *
     *  @param foundry the foundry
     *  @param jobProcessorEnabler a job processor enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobProcessorEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putJobProcessorEnabler(jobProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyJobProcessorEnablerLookupSession} using an
     *  array of job processor enablers.
     *
     *  @param foundry the foundry
     *  @param jobProcessorEnablers an array of job processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobProcessorEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.rules.JobProcessorEnabler[] jobProcessorEnablers, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyJobProcessorEnablerLookupSession} using a
     *  collection of job processor enablers.
     *
     *  @param foundry the foundry
     *  @param jobProcessorEnablers a collection of job processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobProcessorEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                java.util.Collection<? extends org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(foundry, proxy);
        setSessionProxy(proxy);
        putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }

    
    /**
     *  Makes a {@code JobProcessorEnabler} available in this session.
     *
     *  @param jobProcessorEnabler an job processor enabler
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putJobProcessorEnabler(org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler) {
        super.putJobProcessorEnabler(jobProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of jobProcessorEnablers available in this session.
     *
     *  @param jobProcessorEnablers an array of job processor enablers
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putJobProcessorEnablers(org.osid.resourcing.rules.JobProcessorEnabler[] jobProcessorEnablers) {
        super.putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of job processor enablers available in this session.
     *
     *  @param jobProcessorEnablers
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putJobProcessorEnablers(java.util.Collection<? extends org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablers) {
        super.putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }


    /**
     *  Removes a JobProcessorEnabler from this session.
     *
     *  @param jobProcessorEnablerId the {@code Id} of the job processor enabler
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId) {
        super.removeJobProcessorEnabler(jobProcessorEnablerId);
        return;
    }    
}
