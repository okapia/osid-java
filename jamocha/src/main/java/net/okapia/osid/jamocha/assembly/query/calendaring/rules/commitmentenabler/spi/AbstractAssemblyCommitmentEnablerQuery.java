//
// AbstractAssemblyCommitmentEnablerQuery.java
//
//     A CommitmentEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.rules.commitmentenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CommitmentEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyCommitmentEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.calendaring.rules.CommitmentEnablerQuery,
               org.osid.calendaring.rules.CommitmentEnablerQueryInspector,
               org.osid.calendaring.rules.CommitmentEnablerSearchOrder {

    private final java.util.Collection<org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.CommitmentEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCommitmentEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCommitmentEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the commitment. 
     *
     *  @param  commitmentId the commitment book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commitmentBookId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCommitmentId(org.osid.id.Id commitmentId, 
                                       boolean match) {
        getAssembler().addIdTerm(getRuledCommitmentIdColumn(), commitmentId, match);
        return;
    }


    /**
     *  Clears the commitment <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCommitmentIdTerms() {
        getAssembler().clearTerms(getRuledCommitmentIdColumn());
        return;
    }


    /**
     *  Gets the commitment <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCommitmentIdTerms() {
        return (getAssembler().getIdTerms(getRuledCommitmentIdColumn()));
    }


    /**
     *  Gets the RuledCommitmentId column name.
     *
     * @return the column name
     */

    protected String getRuledCommitmentIdColumn() {
        return ("ruled_commitment_id");
    }


    /**
     *  Tests if a <code> CommitmentBookQuery </code> is available. 
     *
     *  @return <code> true </code> if a commitment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCommitmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commitment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuery getRuledCommitmentQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCommitmentQuery() is false");
    }


    /**
     *  Matches enablers mapped to any commitment. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          commitment, <code> false </code> to match enablers mapped to 
     *          no commitment 
     */

    @OSID @Override
    public void matchAnyRuledCommitment(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledCommitmentColumn(), match);
        return;
    }


    /**
     *  Clears the commitment query terms. 
     */

    @OSID @Override
    public void clearRuledCommitmentTerms() {
        getAssembler().clearTerms(getRuledCommitmentColumn());
        return;
    }


    /**
     *  Gets the commitment query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQueryInspector[] getRuledCommitmentTerms() {
        return (new org.osid.calendaring.CommitmentQueryInspector[0]);
    }


    /**
     *  Gets the RuledCommitment column name.
     *
     * @return the column name
     */

    protected String getRuledCommitmentColumn() {
        return ("ruled_commitment");
    }


    /**
     *  Matches enablers mapped to the calendar. 
     *
     *  @param  calendarId the calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if an <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if an calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for an calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this commitmentEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  commitmentEnablerRecordType a commitment enabler record type 
     *  @return <code>true</code> if the commitmentEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commitmentEnablerRecordType) {
        for (org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commitmentEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  commitmentEnablerRecordType the commitment enabler record type 
     *  @return the commitment enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord getCommitmentEnablerQueryRecord(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commitmentEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  commitmentEnablerRecordType the commitment enabler record type 
     *  @return the commitment enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord getCommitmentEnablerQueryInspectorRecord(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(commitmentEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param commitmentEnablerRecordType the commitment enabler record type
     *  @return the commitment enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.CommitmentEnablerSearchOrderRecord getCommitmentEnablerSearchOrderRecord(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.CommitmentEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(commitmentEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this commitment enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commitmentEnablerQueryRecord the commitment enabler query record
     *  @param commitmentEnablerQueryInspectorRecord the commitment enabler query inspector
     *         record
     *  @param commitmentEnablerSearchOrderRecord the commitment enabler search order record
     *  @param commitmentEnablerRecordType commitment enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerQueryRecord</code>,
     *          <code>commitmentEnablerQueryInspectorRecord</code>,
     *          <code>commitmentEnablerSearchOrderRecord</code> or
     *          <code>commitmentEnablerRecordTypecommitmentEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addCommitmentEnablerRecords(org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord commitmentEnablerQueryRecord, 
                                      org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord commitmentEnablerQueryInspectorRecord, 
                                      org.osid.calendaring.rules.records.CommitmentEnablerSearchOrderRecord commitmentEnablerSearchOrderRecord, 
                                      org.osid.type.Type commitmentEnablerRecordType) {

        addRecordType(commitmentEnablerRecordType);

        nullarg(commitmentEnablerQueryRecord, "commitment enabler query record");
        nullarg(commitmentEnablerQueryInspectorRecord, "commitment enabler query inspector record");
        nullarg(commitmentEnablerSearchOrderRecord, "commitment enabler search odrer record");

        this.queryRecords.add(commitmentEnablerQueryRecord);
        this.queryInspectorRecords.add(commitmentEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(commitmentEnablerSearchOrderRecord);
        
        return;
    }
}
