//
// AbstractMapQueueProcessorLookupSession
//
//    A simple framework for providing a QueueProcessor lookup service
//    backed by a fixed collection of queue processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a QueueProcessor lookup service backed by a
 *  fixed collection of queue processors. The queue processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>QueueProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractQueueProcessorLookupSession
    implements org.osid.provisioning.rules.QueueProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.QueueProcessor> queueProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.QueueProcessor>());


    /**
     *  Makes a <code>QueueProcessor</code> available in this session.
     *
     *  @param  queueProcessor a queue processor
     *  @throws org.osid.NullArgumentException <code>queueProcessor<code>
     *          is <code>null</code>
     */

    protected void putQueueProcessor(org.osid.provisioning.rules.QueueProcessor queueProcessor) {
        this.queueProcessors.put(queueProcessor.getId(), queueProcessor);
        return;
    }


    /**
     *  Makes an array of queue processors available in this session.
     *
     *  @param  queueProcessors an array of queue processors
     *  @throws org.osid.NullArgumentException <code>queueProcessors<code>
     *          is <code>null</code>
     */

    protected void putQueueProcessors(org.osid.provisioning.rules.QueueProcessor[] queueProcessors) {
        putQueueProcessors(java.util.Arrays.asList(queueProcessors));
        return;
    }


    /**
     *  Makes a collection of queue processors available in this session.
     *
     *  @param  queueProcessors a collection of queue processors
     *  @throws org.osid.NullArgumentException <code>queueProcessors<code>
     *          is <code>null</code>
     */

    protected void putQueueProcessors(java.util.Collection<? extends org.osid.provisioning.rules.QueueProcessor> queueProcessors) {
        for (org.osid.provisioning.rules.QueueProcessor queueProcessor : queueProcessors) {
            this.queueProcessors.put(queueProcessor.getId(), queueProcessor);
        }

        return;
    }


    /**
     *  Removes a QueueProcessor from this session.
     *
     *  @param  queueProcessorId the <code>Id</code> of the queue processor
     *  @throws org.osid.NullArgumentException <code>queueProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeQueueProcessor(org.osid.id.Id queueProcessorId) {
        this.queueProcessors.remove(queueProcessorId);
        return;
    }


    /**
     *  Gets the <code>QueueProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  queueProcessorId <code>Id</code> of the <code>QueueProcessor</code>
     *  @return the queueProcessor
     *  @throws org.osid.NotFoundException <code>queueProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>queueProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessor getQueueProcessor(org.osid.id.Id queueProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.QueueProcessor queueProcessor = this.queueProcessors.get(queueProcessorId);
        if (queueProcessor == null) {
            throw new org.osid.NotFoundException("queueProcessor not found: " + queueProcessorId);
        }

        return (queueProcessor);
    }


    /**
     *  Gets all <code>QueueProcessors</code>. In plenary mode, the returned
     *  list contains all known queueProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  queueProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>QueueProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueprocessor.ArrayQueueProcessorList(this.queueProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queueProcessors.clear();
        super.close();
        return;
    }
}
