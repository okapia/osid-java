//
// MutableIndexedMapProxyModelLookupSession
//
//    Implements a Model lookup service backed by a collection of
//    models indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory;


/**
 *  Implements a Model lookup service backed by a collection of
 *  models. The models are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some models may be compatible
 *  with more types than are indicated through these model
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of models can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyModelLookupSession
    extends net.okapia.osid.jamocha.core.inventory.spi.AbstractIndexedMapModelLookupSession
    implements org.osid.inventory.ModelLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyModelLookupSession} with
     *  no model.
     *
     *  @param warehouse the warehouse
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.proxy.Proxy proxy) {
        setWarehouse(warehouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyModelLookupSession} with
     *  a single model.
     *
     *  @param warehouse the warehouse
     *  @param  model an model
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code model}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.inventory.Model model, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putModel(model);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyModelLookupSession} using
     *  an array of models.
     *
     *  @param warehouse the warehouse
     *  @param  models an array of models
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code models}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.inventory.Model[] models, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putModels(models);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyModelLookupSession} using
     *  a collection of models.
     *
     *  @param warehouse the warehouse
     *  @param  models a collection of models
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code models}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       java.util.Collection<? extends org.osid.inventory.Model> models,
                                                       org.osid.proxy.Proxy proxy) {
        this(warehouse, proxy);
        putModels(models);
        return;
    }

    
    /**
     *  Makes a {@code Model} available in this session.
     *
     *  @param  model a model
     *  @throws org.osid.NullArgumentException {@code model{@code 
     *          is {@code null}
     */

    @Override
    public void putModel(org.osid.inventory.Model model) {
        super.putModel(model);
        return;
    }


    /**
     *  Makes an array of models available in this session.
     *
     *  @param  models an array of models
     *  @throws org.osid.NullArgumentException {@code models{@code 
     *          is {@code null}
     */

    @Override
    public void putModels(org.osid.inventory.Model[] models) {
        super.putModels(models);
        return;
    }


    /**
     *  Makes collection of models available in this session.
     *
     *  @param  models a collection of models
     *  @throws org.osid.NullArgumentException {@code model{@code 
     *          is {@code null}
     */

    @Override
    public void putModels(java.util.Collection<? extends org.osid.inventory.Model> models) {
        super.putModels(models);
        return;
    }


    /**
     *  Removes a Model from this session.
     *
     *  @param modelId the {@code Id} of the model
     *  @throws org.osid.NullArgumentException {@code modelId{@code  is
     *          {@code null}
     */

    @Override
    public void removeModel(org.osid.id.Id modelId) {
        super.removeModel(modelId);
        return;
    }    
}
