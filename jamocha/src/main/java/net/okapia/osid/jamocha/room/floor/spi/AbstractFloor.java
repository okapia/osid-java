//
// AbstractFloor.java
//
//     Defines a Floor.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.floor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Floor</code>.
 */

public abstract class AbstractFloor
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.room.Floor {

    private org.osid.room.Building building;
    private String number;
    private java.math.BigDecimal area;

    private final java.util.Collection<org.osid.room.records.FloorRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.building.getId());
    }


    /**
     *  Gets the building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.building);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @throws org.osid.NullArgumentException
     *          <code>building</code> is <code>null</code>
     */

    protected void setBuilding(org.osid.room.Building building) {
        nullarg(building, "building");
        this.building = building;
        return;
    }


    /**
     *  Gets the floor number. 
     *
     *  @return the floor number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.number);
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    protected void setNumber(String number) {
        nullarg(number, "number");
        this.number = number;
        return;
    }


    /**
     *  Tests if an area is available. 
     *
     *  @return <code> true </code> if an area is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasArea() {
        return (this.area != null);
    }


    /**
     *  Gets the gross square footage of this floor. 
     *
     *  @return the gross area 
     *  @throws org.osid.IllegalStateException <code> hasArea() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getGrossArea() {
        if (!hasArea()) {
            throw new org.osid.IllegalStateException("hasArea() is false");
        }

        return (this.area);
    }


    /**
     *  Sets the gross area.
     *
     *  @param area a gross area
     *  @throws org.osid.InvalidArgumentException <code>area</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    protected void setGrossArea(java.math.BigDecimal area) {
        cardinalarg(area, "area");
        this.area = area;
        return;
    }


    /**
     *  Tests if this floor supports the given record
     *  <code>Type</code>.
     *
     *  @param  floorRecordType a floor record type 
     *  @return <code>true</code> if the floorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type floorRecordType) {
        for (org.osid.room.records.FloorRecord record : this.records) {
            if (record.implementsRecordType(floorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Floor</code>
     *  record <code>Type</code>.
     *
     *  @param  floorRecordType the floor record type 
     *  @return the floor record 
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(floorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorRecord getFloorRecord(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.FloorRecord record : this.records) {
            if (record.implementsRecordType(floorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(floorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this floor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param floorRecord the floor record
     *  @param floorRecordType floor record type
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecord</code> or
     *          <code>floorRecordTypefloor</code> is
     *          <code>null</code>
     */
            
    protected void addFloorRecord(org.osid.room.records.FloorRecord floorRecord, 
                                  org.osid.type.Type floorRecordType) {
        
        nullarg(floorRecord, "floor record");
        addRecordType(floorRecordType);
        this.records.add(floorRecord);
        
        return;
    }
}
