//
// MutableIndexedMapTodoLookupSession
//
//    Implements a Todo lookup service backed by a collection of
//    todos indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist;


/**
 *  Implements a Todo lookup service backed by a collection of
 *  todos. The todos are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some todos may be compatible
 *  with more types than are indicated through these todo
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of todos can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapTodoLookupSession
    extends net.okapia.osid.jamocha.core.checklist.spi.AbstractIndexedMapTodoLookupSession
    implements org.osid.checklist.TodoLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapTodoLookupSession} with no todos.
     *
     *  @param checklist the checklist
     *  @throws org.osid.NullArgumentException {@code checklist}
     *          is {@code null}
     */

      public MutableIndexedMapTodoLookupSession(org.osid.checklist.Checklist checklist) {
        setChecklist(checklist);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTodoLookupSession} with a
     *  single todo.
     *  
     *  @param checklist the checklist
     *  @param  todo a single todo
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todo} is {@code null}
     */

    public MutableIndexedMapTodoLookupSession(org.osid.checklist.Checklist checklist,
                                                  org.osid.checklist.Todo todo) {
        this(checklist);
        putTodo(todo);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTodoLookupSession} using an
     *  array of todos.
     *
     *  @param checklist the checklist
     *  @param  todos an array of todos
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todos} is {@code null}
     */

    public MutableIndexedMapTodoLookupSession(org.osid.checklist.Checklist checklist,
                                                  org.osid.checklist.Todo[] todos) {
        this(checklist);
        putTodos(todos);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTodoLookupSession} using a
     *  collection of todos.
     *
     *  @param checklist the checklist
     *  @param  todos a collection of todos
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todos} is {@code null}
     */

    public MutableIndexedMapTodoLookupSession(org.osid.checklist.Checklist checklist,
                                                  java.util.Collection<? extends org.osid.checklist.Todo> todos) {

        this(checklist);
        putTodos(todos);
        return;
    }
    

    /**
     *  Makes a {@code Todo} available in this session.
     *
     *  @param  todo a todo
     *  @throws org.osid.NullArgumentException {@code todo{@code  is
     *          {@code null}
     */

    @Override
    public void putTodo(org.osid.checklist.Todo todo) {
        super.putTodo(todo);
        return;
    }


    /**
     *  Makes an array of todos available in this session.
     *
     *  @param  todos an array of todos
     *  @throws org.osid.NullArgumentException {@code todos{@code 
     *          is {@code null}
     */

    @Override
    public void putTodos(org.osid.checklist.Todo[] todos) {
        super.putTodos(todos);
        return;
    }


    /**
     *  Makes collection of todos available in this session.
     *
     *  @param  todos a collection of todos
     *  @throws org.osid.NullArgumentException {@code todo{@code  is
     *          {@code null}
     */

    @Override
    public void putTodos(java.util.Collection<? extends org.osid.checklist.Todo> todos) {
        super.putTodos(todos);
        return;
    }


    /**
     *  Removes a Todo from this session.
     *
     *  @param todoId the {@code Id} of the todo
     *  @throws org.osid.NullArgumentException {@code todoId{@code  is
     *          {@code null}
     */

    @Override
    public void removeTodo(org.osid.id.Id todoId) {
        super.removeTodo(todoId);
        return;
    }    
}
