//
// AbstractSubscriptionEnablerNotificationSession.java
//
//     A template for making SubscriptionEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code SubscriptionEnabler} objects. This session
 *  is intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code SubscriptionEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for subscription enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractSubscriptionEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.subscription.rules.SubscriptionEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.subscription.Publisher publisher = new net.okapia.osid.jamocha.nil.subscription.publisher.UnknownPublisher();


    /**
     *  Gets the {@code Publisher} {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Publisher Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.publisher.getId());
    }

    
    /**
     *  Gets the {@code Publisher} associated with this session.
     *
     *  @return the {@code Publisher} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.publisher);
    }


    /**
     *  Sets the {@code Publisher}.
     *
     *  @param publisher the publisher for this session
     *  @throws org.osid.NullArgumentException {@code publisher}
     *          is {@code null}
     */

    protected void setPublisher(org.osid.subscription.Publisher publisher) {
        nullarg(publisher, "publisher");
        this.publisher = publisher;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  SubscriptionEnabler} notifications.  A return of true does not
     *  guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForSubscriptionEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode, notifications 
     *  are to be acknowledged using <code> acknowledgeSubscriptionEnablerNotification() 
     *  </code>. 
     */

    @OSID @Override
    public void reliableSubscriptionEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableSubscriptionEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge a subscription enabler notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeSubscriptionEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscription enablers in publishers which
     *  are children of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new subscription
     *  enablers. {@code
     *  SubscriptionEnablerReceiver.newSubscriptionEnabler()} is
     *  invoked when a new {@code SubscriptionEnabler} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewSubscriptionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated subscription
     *  enablers. {@code
     *  SubscriptionEnablerReceiver.changedSubscriptionEnabler()} is
     *  invoked when a subscription enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedSubscriptionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated subscription
     *  enabler. {@code
     *  SubscriptionEnablerReceiver.changedSubscriptionEnabler()} is
     *  invoked when the specified subscription enabler is changed.
     *
     *  @param subscriptionEnablerId the {@code Id} of the {@code SubscriptionEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code subscriptionEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedSubscriptionEnabler(org.osid.id.Id subscriptionEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted subscription
     *  enablers. {@code
     *  SubscriptionEnablerReceiver.deletedSubscriptionEnabler()} is
     *  invoked when a subscription enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedSubscriptionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted subscription
     *  enabler. {@code
     *  SubscriptionEnablerReceiver.deletedSubscriptionEnabler()} is
     *  invoked when the specified subscription enabler is deleted.
     *
     *  @param subscriptionEnablerId the {@code Id} of the
     *          {@code SubscriptionEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code subscriptionEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedSubscriptionEnabler(org.osid.id.Id subscriptionEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
