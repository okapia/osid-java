//
// AbstractQueryBrokerConstrainerEnablerLookupSession.java
//
//    A BrokerConstrainerEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BrokerConstrainerEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterBrokerConstrainerEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerQuerySession {

    private final org.osid.provisioning.rules.BrokerConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterBrokerConstrainerEnablerQuerySession.
     *
     *  @param session the underlying broker constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBrokerConstrainerEnablerQuerySession(org.osid.provisioning.rules.BrokerConstrainerEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeDistributor</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeDistributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@codeDistributor</code> associated with this 
     *  session.
     *
     *  @return the {@codeDistributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@codeBrokerConstrainerEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchBrokerConstrainerEnablers() {
        return (this.session.canSearchBrokerConstrainerEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker constrainer enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this distributor only.
     */
    
    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    
      
    /**
     *  Gets a broker constrainer enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the broker constrainer enabler query 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerQuery getBrokerConstrainerEnablerQuery() {
        return (this.session.getBrokerConstrainerEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  brokerConstrainerEnablerQuery the broker constrainer enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code brokerConstrainerEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByQuery(org.osid.provisioning.rules.BrokerConstrainerEnablerQuery brokerConstrainerEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getBrokerConstrainerEnablersByQuery(brokerConstrainerEnablerQuery));
    }
}
