//
// AbstractPayerSearch.java
//
//     A template for making a Payer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing payer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPayerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.billing.payment.PayerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.billing.payment.records.PayerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.billing.payment.PayerSearchOrder payerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of payers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  payerIds list of payers
     *  @throws org.osid.NullArgumentException
     *          <code>payerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPayers(org.osid.id.IdList payerIds) {
        while (payerIds.hasNext()) {
            try {
                this.ids.add(payerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPayers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of payer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPayerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  payerSearchOrder payer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>payerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>payerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPayerResults(org.osid.billing.payment.PayerSearchOrder payerSearchOrder) {
	this.payerSearchOrder = payerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.billing.payment.PayerSearchOrder getPayerSearchOrder() {
	return (this.payerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given payer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a payer implementing the requested record.
     *
     *  @param payerSearchRecordType a payer search record
     *         type
     *  @return the payer search record
     *  @throws org.osid.NullArgumentException
     *          <code>payerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(payerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerSearchRecord getPayerSearchRecord(org.osid.type.Type payerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.billing.payment.records.PayerSearchRecord record : this.records) {
            if (record.implementsRecordType(payerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(payerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payer search. 
     *
     *  @param payerSearchRecord payer search record
     *  @param payerSearchRecordType payer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPayerSearchRecord(org.osid.billing.payment.records.PayerSearchRecord payerSearchRecord, 
                                           org.osid.type.Type payerSearchRecordType) {

        addRecordType(payerSearchRecordType);
        this.records.add(payerSearchRecord);        
        return;
    }
}
