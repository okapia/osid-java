//
// AbstractCanonicalUnitProcessorList
//
//     Implements a filter for a CanonicalUnitProcessorList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.offering.rules.canonicalunitprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CanonicalUnitProcessorList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCanonicalUnitProcessorList
 *  to improve performance.
 */

public abstract class AbstractCanonicalUnitProcessorFilterList
    extends net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.spi.AbstractCanonicalUnitProcessorList
    implements org.osid.offering.rules.CanonicalUnitProcessorList,
               net.okapia.osid.jamocha.inline.filter.offering.rules.canonicalunitprocessor.CanonicalUnitProcessorFilter {

    private org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor;
    private final org.osid.offering.rules.CanonicalUnitProcessorList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCanonicalUnitProcessorFilterList</code>.
     *
     *  @param canonicalUnitProcessorList a <code>CanonicalUnitProcessorList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorList</code> is <code>null</code>
     */

    protected AbstractCanonicalUnitProcessorFilterList(org.osid.offering.rules.CanonicalUnitProcessorList canonicalUnitProcessorList) {
        nullarg(canonicalUnitProcessorList, "canonical unit processor list");
        this.list = canonicalUnitProcessorList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.canonicalUnitProcessor == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> CanonicalUnitProcessor </code> in this list. 
     *
     *  @return the next <code> CanonicalUnitProcessor </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CanonicalUnitProcessor </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessor getNextCanonicalUnitProcessor()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor = this.canonicalUnitProcessor;
            this.canonicalUnitProcessor = null;
            return (canonicalUnitProcessor);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in canonical unit processor list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitProcessor = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters CanonicalUnitProcessors.
     *
     *  @param canonicalUnitProcessor the canonical unit processor to filter
     *  @return <code>true</code> if the canonical unit processor passes the filter,
     *          <code>false</code> if the canonical unit processor should be filtered
     */

    public abstract boolean pass(org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor);


    protected void prime() {
        if (this.canonicalUnitProcessor != null) {
            return;
        }

        org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor = null;

        while (this.list.hasNext()) {
            try {
                canonicalUnitProcessor = this.list.getNextCanonicalUnitProcessor();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(canonicalUnitProcessor)) {
                this.canonicalUnitProcessor = canonicalUnitProcessor;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
