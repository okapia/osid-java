//
// AbstractEdgeEnablerQuery.java
//
//     A template for making an EdgeEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.edgeenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for edge enablers.
 */

public abstract class AbstractEdgeEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.topology.rules.EdgeEnablerQuery {

    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the edge. 
     *
     *  @param  edgeId the edge <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> edgeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledEdgeId(org.osid.id.Id edgeId, boolean match) {
        return;
    }


    /**
     *  Clears the edge <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledEdgeIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EdgeQuery </code> is available. 
     *
     *  @return <code> true </code> if an edge query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledEdgeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an edge. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the edge query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledEdgeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuery getRuledEdgeQuery() {
        throw new org.osid.UnimplementedException("supportsRuledEdgeQuery() is false");
    }


    /**
     *  Matches enablers mapped to any edge. 
     *
     *  @param  match <code> true </code> for enablers mapped to any edge, 
     *          <code> false </code> to match enablers mapped to no edge 
     */

    @OSID @Override
    public void matchAnyRuledEdge(boolean match) {
        return;
    }


    /**
     *  Clears the edge query terms. 
     */

    @OSID @Override
    public void clearRuledEdgeTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the graph. 
     *
     *  @param  graphId the graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraphId(org.osid.id.Id graphId, boolean match) {
        return;
    }


    /**
     *  Clears the graph <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearGraphIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getGraphQuery() {
        throw new org.osid.UnimplementedException("supportsGraphQuery() is false");
    }


    /**
     *  Clears the graph query terms. 
     */

    @OSID @Override
    public void clearGraphTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given edge enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an edge enabler implementing the requested record.
     *
     *  @param edgeEnablerRecordType an edge enabler record type
     *  @return the edge enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerQueryRecord getEdgeEnablerQueryRecord(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.rules.records.EdgeEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this edge enabler query. 
     *
     *  @param edgeEnablerQueryRecord edge enabler query record
     *  @param edgeEnablerRecordType edgeEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEdgeEnablerQueryRecord(org.osid.topology.rules.records.EdgeEnablerQueryRecord edgeEnablerQueryRecord, 
                                          org.osid.type.Type edgeEnablerRecordType) {

        addRecordType(edgeEnablerRecordType);
        nullarg(edgeEnablerQueryRecord, "edge enabler query record");
        this.records.add(edgeEnablerQueryRecord);        
        return;
    }
}
