//
// AbstractAddressSearch.java
//
//     A template for making an Address Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.address.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing address searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAddressSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.contact.AddressSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.contact.records.AddressSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.contact.AddressSearchOrder addressSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of addresses. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  addressIds list of addresses
     *  @throws org.osid.NullArgumentException
     *          <code>addressIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAddresses(org.osid.id.IdList addressIds) {
        while (addressIds.hasNext()) {
            try {
                this.ids.add(addressIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAddresses</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of address Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAddressIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  addressSearchOrder address search order 
     *  @throws org.osid.NullArgumentException
     *          <code>addressSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>addressSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAddressResults(org.osid.contact.AddressSearchOrder addressSearchOrder) {
	this.addressSearchOrder = addressSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.contact.AddressSearchOrder getAddressSearchOrder() {
	return (this.addressSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given address search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an address implementing the requested record.
     *
     *  @param addressSearchRecordType an address search record
     *         type
     *  @return the address search record
     *  @throws org.osid.NullArgumentException
     *          <code>addressSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressSearchRecord getAddressSearchRecord(org.osid.type.Type addressSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.contact.records.AddressSearchRecord record : this.records) {
            if (record.implementsRecordType(addressSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this address search. 
     *
     *  @param addressSearchRecord address search record
     *  @param addressSearchRecordType address search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAddressSearchRecord(org.osid.contact.records.AddressSearchRecord addressSearchRecord, 
                                           org.osid.type.Type addressSearchRecordType) {

        addRecordType(addressSearchRecordType);
        this.records.add(addressSearchRecord);        
        return;
    }
}
