//
// AbstractQueryOffsetEventLookupSession.java
//
//    An inline adapter that maps an OffsetEventLookupSession to
//    an OffsetEventQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an OffsetEventLookupSession to
 *  an OffsetEventQuerySession.
 */

public abstract class AbstractQueryOffsetEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractOffsetEventLookupSession
    implements org.osid.calendaring.OffsetEventLookupSession {

    private boolean activeonly = false;
    private final org.osid.calendaring.OffsetEventQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryOffsetEventLookupSession.
     *
     *  @param querySession the underlying offset event query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryOffsetEventLookupSession(org.osid.calendaring.OffsetEventQuerySession querySession) {
        nullarg(querySession, "offset event query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>OffsetEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOffsetEvents() {
        return (this.session.canSearchOffsetEvents());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offset events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active offset events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOffsetEventView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive offset events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOffsetEventView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>OffsetEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>OffsetEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>OffsetEvent</code> and
     *  retained for compatibility.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventId <code>Id</code> of the
     *          <code>OffsetEvent</code>
     *  @return the offset event
     *  @throws org.osid.NotFoundException <code>offsetEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>offsetEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEvent getOffsetEvent(org.osid.id.Id offsetEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.OffsetEventQuery query = getQuery();
        query.matchId(offsetEventId, true);
        org.osid.calendaring.OffsetEventList offsetEvents = this.session.getOffsetEventsByQuery(query);
        if (offsetEvents.hasNext()) {
            return (offsetEvents.getNextOffsetEvent());
        } 
        
        throw new org.osid.NotFoundException(offsetEventId + " not found");
    }


    /**
     *  Gets an <code>OffsetEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offsetEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>OffsetEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByIds(org.osid.id.IdList offsetEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.OffsetEventQuery query = getQuery();

        try (org.osid.id.IdList ids = offsetEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getOffsetEventsByQuery(query));
    }


    /**
     *  Gets an <code>OffsetEventList</code> corresponding to the given
     *  offset event genus <code>Type</code> which does not include
     *  offset events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventGenusType an offsetEvent genus type 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByGenusType(org.osid.type.Type offsetEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.OffsetEventQuery query = getQuery();
        query.matchGenusType(offsetEventGenusType, true);
        return (this.session.getOffsetEventsByQuery(query));
    }


    /**
     *  Gets an <code>OffsetEventList</code> corresponding to the given
     *  offset event genus <code>Type</code> and include any additional
     *  offset events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventGenusType an offsetEvent genus type 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByParentGenusType(org.osid.type.Type offsetEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.OffsetEventQuery query = getQuery();
        query.matchParentGenusType(offsetEventGenusType, true);
        return (this.session.getOffsetEventsByQuery(query));
    }


    /**
     *  Gets an <code>OffsetEventList</code> containing the given
     *  offset event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventRecordType an offsetEvent record type 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByRecordType(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.OffsetEventQuery query = getQuery();
        query.matchRecordType(offsetEventRecordType, true);
        return (this.session.getOffsetEventsByQuery(query));
    }


    /**
     *  Gets the <code> OffsetEvents </code> using the given event as
     *  a start or ending offset.
     *  
     *  In plenary mode, the returned list contains all known offset
     *  events or an error results. Otherwise, the returned list may
     *  contain only those offset events that are accessible through
     *  this session.
     *  
     *  In active mode, offset events are returned that are currently
     *  active.  In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  eventId <code> Id </code> of the related event 
     *  @return the offset events 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByEvent(org.osid.id.Id eventId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.offsetevent.CompositeOffsetEventList ret = new net.okapia.osid.jamocha.adapter.federator.calendaring.offsetevent.CompositeOffsetEventList();

        org.osid.calendaring.OffsetEventQuery query = getQuery();
        query.matchStartReferenceEventId(eventId, true);
        ret.addOffsetEventList(this.session.getOffsetEventsByQuery(query));

        query = getQuery();
        query.matchEndReferenceEventId(eventId, true);
        ret.addOffsetEventList(this.session.getOffsetEventsByQuery(query));

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>OffsetEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @return a list of <code>OffsetEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.OffsetEventQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getOffsetEventsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.OffsetEventQuery getQuery() {
        org.osid.calendaring.OffsetEventQuery query = this.session.getOffsetEventQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
