//
// AbstractQueryParameterLookupSession.java
//
//    An inline adapter that maps a ParameterLookupSession to
//    a ParameterQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ParameterLookupSession to
 *  a ParameterQuerySession.
 */

public abstract class AbstractQueryParameterLookupSession
    extends net.okapia.osid.jamocha.configuration.spi.AbstractParameterLookupSession
    implements org.osid.configuration.ParameterLookupSession {

    private boolean activeonly    = false;
    private final org.osid.configuration.ParameterQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryParameterLookupSession.
     *
     *  @param querySession the underlying parameter query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryParameterLookupSession(org.osid.configuration.ParameterQuerySession querySession) {
        nullarg(querySession, "parameter query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Configuration</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform <code>Parameter</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParameters() {
        return (this.session.canSearchParameters());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameters in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    

    /**
     *  Only active parameters are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive parameters are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Parameter</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Parameter</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Parameter</code> and
     *  retained for compatibility.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterId <code>Id</code> of the
     *          <code>Parameter</code>
     *  @return the parameter
     *  @throws org.osid.NotFoundException <code>parameterId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>parameterId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Parameter getParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ParameterQuery query = getQuery();
        query.matchId(parameterId, true);
        org.osid.configuration.ParameterList parameters = this.session.getParametersByQuery(query);
        if (parameters.hasNext()) {
            return (parameters.getNextParameter());
        } 
        
        throw new org.osid.NotFoundException(parameterId + " not found");
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameters specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Parameters</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>parameterIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByIds(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ParameterQuery query = getQuery();

        try (org.osid.id.IdList ids = parameterIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getParametersByQuery(query));
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  parameter genus <code>Type</code> which does not include
     *  parameters of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ParameterQuery query = getQuery();
        query.matchGenusType(parameterGenusType, true);
        return (this.session.getParametersByQuery(query));
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  parameter genus <code>Type</code> and include any additional
     *  parameters with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByParentGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ParameterQuery query = getQuery();
        query.matchParentGenusType(parameterGenusType, true);
        return (this.session.getParametersByQuery(query));
    }


    /**
     *  Gets a <code>ParameterList</code> containing the given
     *  parameter record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByRecordType(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ParameterQuery query = getQuery();
        query.matchRecordType(parameterRecordType, true);
        return (this.session.getParametersByQuery(query));
    }

    
    /**
     *  Gets all <code>Parameters</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameters or an error results. Otherwise, the returned list
     *  may contain only those parameters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameters are returned that are currently
     *  active. In any status mode, active and inactive parameters
     *  are returned.
     *
     *  @return a list of <code>Parameters</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParameters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ParameterQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getParametersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.configuration.ParameterQuery getQuery() {
        org.osid.configuration.ParameterQuery query = this.session.getParameterQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
