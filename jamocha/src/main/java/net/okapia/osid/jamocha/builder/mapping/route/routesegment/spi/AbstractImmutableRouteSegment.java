//
// AbstractImmutableRouteSegment.java
//
//     Wraps a mutable RouteSegment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.routesegment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>RouteSegment</code> to hide modifiers. This
 *  wrapper provides an immutized RouteSegment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying routeSegment whose state changes are visible.
 */

public abstract class AbstractImmutableRouteSegment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.mapping.route.RouteSegment {

    private final org.osid.mapping.route.RouteSegment routeSegment;


    /**
     *  Constructs a new <code>AbstractImmutableRouteSegment</code>.
     *
     *  @param routeSegment the route segment to immutablize
     *  @throws org.osid.NullArgumentException <code>routeSegment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRouteSegment(org.osid.mapping.route.RouteSegment routeSegment) {
        super(routeSegment);
        this.routeSegment = routeSegment;
        return;
    }


    /**
     *  Gets the starting instructions for this segment. 
     *
     *  @return the starting instructions 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getStartingInstructions() {
        return (this.routeSegment.getStartingInstructions());
    }


    /**
     *  Gets the ending instructions for this segment. 
     *
     *  @return the ending instructions 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getEndingInstructions() {
        return (this.routeSegment.getEndingInstructions());
    }


    /**
     *  Gets the length of the entire segment. 
     *
     *  @return the distance 
     */

    @OSID @Override
    public org.osid.mapping.Distance getDistance() {
        return (this.routeSegment.getDistance());
    }


    /**
     *  Gets the estimated travel time across the entire segment. 
     *
     *  @return the estimated travel time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getETA() {
        return (this.routeSegment.getETA());
    }


    /**
     *  Tests if this segment has a corresponding path. 
     *
     *  @return <code> true </code> if there is a path, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean hasPath() {
        return (this.routeSegment.hasPath());
    }


    /**
     *  Gets the corresponding path <code> Id </code> on which this segment 
     *  travels. 
     *
     *  @return the path 
     *  @throws org.osid.IllegalStateException <code> hasPath() </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.routeSegment.getPathId());
    }


    /**
     *  Gets the corresponding path on which this segment travels. 
     *
     *  @return the path 
     *  @throws org.osid.IllegalStateException <code> hasPath() </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.routeSegment.getPath());
    }


    /**
     *  Gets the route segment record corresponding to the given <code> 
     *  RouteSegment </code> record <code> Type. </code> 
     *
     *  @param  routeSegmentRecordType a route segment record type 
     *  @return the route segment record 
     *  @throws org.osid.NullArgumentException <code> routeRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasDomainType(routeSegmentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentRecord getRouteSegmentRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        return (this.routeSegment.getRouteSegmentRecord(routeSegmentRecordType));
    }
}

