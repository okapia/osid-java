//
// CourseCatalogElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.coursecatalog.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CourseCatalogElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the CourseCatalogElement Id.
     *
     *  @return the course catalog element Id
     */

    public static org.osid.id.Id getCourseCatalogEntityId() {
        return (makeEntityId("osid.course.CourseCatalog"));
    }


    /**
     *  Gets the CourseId element Id.
     *
     *  @return the CourseId element Id
     */

    public static org.osid.id.Id getCourseId() {
        return (makeQueryElementId("osid.course.coursecatalog.CourseId"));
    }


    /**
     *  Gets the Course element Id.
     *
     *  @return the Course element Id
     */

    public static org.osid.id.Id getCourse() {
        return (makeQueryElementId("osid.course.coursecatalog.Course"));
    }


    /**
     *  Gets the ActivityUnitId element Id.
     *
     *  @return the ActivityUnitId element Id
     */

    public static org.osid.id.Id getActivityUnitId() {
        return (makeQueryElementId("osid.course.coursecatalog.ActivityUnitId"));
    }


    /**
     *  Gets the ActivityUnit element Id.
     *
     *  @return the ActivityUnit element Id
     */

    public static org.osid.id.Id getActivityUnit() {
        return (makeQueryElementId("osid.course.coursecatalog.ActivityUnit"));
    }


    /**
     *  Gets the CourseOfferingId element Id.
     *
     *  @return the CourseOfferingId element Id
     */

    public static org.osid.id.Id getCourseOfferingId() {
        return (makeQueryElementId("osid.course.coursecatalog.CourseOfferingId"));
    }


    /**
     *  Gets the CourseOffering element Id.
     *
     *  @return the CourseOffering element Id
     */

    public static org.osid.id.Id getCourseOffering() {
        return (makeQueryElementId("osid.course.coursecatalog.CourseOffering"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeQueryElementId("osid.course.coursecatalog.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeQueryElementId("osid.course.coursecatalog.Activity"));
    }


    /**
     *  Gets the TermId element Id.
     *
     *  @return the TermId element Id
     */

    public static org.osid.id.Id getTermId() {
        return (makeQueryElementId("osid.course.coursecatalog.TermId"));
    }


    /**
     *  Gets the Term element Id.
     *
     *  @return the Term element Id
     */

    public static org.osid.id.Id getTerm() {
        return (makeQueryElementId("osid.course.coursecatalog.Term"));
    }


    /**
     *  Gets the AncestorCourseCatalogId element Id.
     *
     *  @return the AncestorCourseCatalogId element Id
     */

    public static org.osid.id.Id getAncestorCourseCatalogId() {
        return (makeQueryElementId("osid.course.coursecatalog.AncestorCourseCatalogId"));
    }


    /**
     *  Gets the AncestorCourseCatalog element Id.
     *
     *  @return the AncestorCourseCatalog element Id
     */

    public static org.osid.id.Id getAncestorCourseCatalog() {
        return (makeQueryElementId("osid.course.coursecatalog.AncestorCourseCatalog"));
    }


    /**
     *  Gets the DescendantCourseCatalogId element Id.
     *
     *  @return the DescendantCourseCatalogId element Id
     */

    public static org.osid.id.Id getDescendantCourseCatalogId() {
        return (makeQueryElementId("osid.course.coursecatalog.DescendantCourseCatalogId"));
    }


    /**
     *  Gets the DescendantCourseCatalog element Id.
     *
     *  @return the DescendantCourseCatalog element Id
     */

    public static org.osid.id.Id getDescendantCourseCatalog() {
        return (makeQueryElementId("osid.course.coursecatalog.DescendantCourseCatalog"));
    }
}
