//
// AbstractAssemblyPoolProcessorQuery.java
//
//     A PoolProcessorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PoolProcessorQuery that stores terms.
 */

public abstract class AbstractAssemblyPoolProcessorQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidProcessorQuery
    implements org.osid.provisioning.rules.PoolProcessorQuery,
               org.osid.provisioning.rules.PoolProcessorQueryInspector,
               org.osid.provisioning.rules.PoolProcessorSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPoolProcessorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPoolProcessorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches pools that allocate by least use. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by least use, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByLeastUse(boolean match) {
        getAssembler().addBooleanTerm(getAllocatesByLeastUseColumn(), match);
        return;
    }


    /**
     *  Clears the allocate by least use query terms. 
     */

    @OSID @Override
    public void clearAllocatesByLeastUseTerms() {
        getAssembler().clearTerms(getAllocatesByLeastUseColumn());
        return;
    }


    /**
     *  Gets the allocates by least use query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByLeastUseTerms() {
        return (getAssembler().getBooleanTerms(getAllocatesByLeastUseColumn()));
    }


    /**
     *  Orders the results by allocates by least use. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByLeastUse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAllocatesByLeastUseColumn(), style);
        return;
    }


    /**
     *  Gets the AllocatesByLeastUse column name.
     *
     * @return the column name
     */

    protected String getAllocatesByLeastUseColumn() {
        return ("allocates_by_least_use");
    }


    /**
     *  Matches pools that allocate by most use. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by most use, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByMostUse(boolean match) {
        getAssembler().addBooleanTerm(getAllocatesByMostUseColumn(), match);
        return;
    }


    /**
     *  Clears the allocate by most use query terms. 
     */

    @OSID @Override
    public void clearAllocatesByMostUseTerms() {
        getAssembler().clearTerms(getAllocatesByMostUseColumn());
        return;
    }


    /**
     *  Gets the allocates by most use query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByMostUseTerms() {
        return (getAssembler().getBooleanTerms(getAllocatesByMostUseColumn()));
    }


    /**
     *  Orders the results by allocates by most use. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByMostUse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAllocatesByMostUseColumn(), style);
        return;
    }


    /**
     *  Gets the AllocatesByMostUse column name.
     *
     * @return the column name
     */

    protected String getAllocatesByMostUseColumn() {
        return ("allocates_by_most_use");
    }


    /**
     *  Matches pools that allocate by least cost. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by least cost, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByLeastCost(boolean match) {
        getAssembler().addBooleanTerm(getAllocatesByLeastCostColumn(), match);
        return;
    }


    /**
     *  Clears the allocate by most cost query terms. 
     */

    @OSID @Override
    public void clearAllocatesByLeastCostTerms() {
        getAssembler().clearTerms(getAllocatesByLeastCostColumn());
        return;
    }


    /**
     *  Gets the allocates by least cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByLeastCostTerms() {
        return (getAssembler().getBooleanTerms(getAllocatesByLeastCostColumn()));
    }


    /**
     *  Orders the results by allocates by least cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByLeastCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAllocatesByLeastCostColumn(), style);
        return;
    }


    /**
     *  Gets the AllocatesByLeastCost column name.
     *
     * @return the column name
     */

    protected String getAllocatesByLeastCostColumn() {
        return ("allocates_by_least_cost");
    }


    /**
     *  Matches pools that allocate by most cost. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by most cost, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByMostCost(boolean match) {
        getAssembler().addBooleanTerm(getAllocatesByMostCostColumn(), match);
        return;
    }


    /**
     *  Clears the allocate by least cost query terms. 
     */

    @OSID @Override
    public void clearAllocatesByMostCostTerms() {
        getAssembler().clearTerms(getAllocatesByMostCostColumn());
        return;
    }


    /**
     *  Gets the allocates by most cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByMostCostTerms() {
        return (getAssembler().getBooleanTerms(getAllocatesByMostCostColumn()));
    }


    /**
     *  Orders the results by allocates by most cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByMostCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAllocatesByMostCostColumn(), style);
        return;
    }


    /**
     *  Gets the AllocatesByMostCost column name.
     *
     * @return the column name
     */

    protected String getAllocatesByMostCostColumn() {
        return ("allocates_by_most_cost");
    }


    /**
     *  Matches mapped to the pool. 
     *
     *  @param  distributorId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPoolId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getRuledPoolIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPoolIdTerms() {
        getAssembler().clearTerms(getRuledPoolIdColumn());
        return;
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledPoolIdTerms() {
        return (getAssembler().getIdTerms(getRuledPoolIdColumn()));
    }


    /**
     *  Gets the RuledPoolId column name.
     *
     * @return the column name
     */

    protected String getRuledPoolIdColumn() {
        return ("ruled_pool_id");
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPoolQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getRuledPoolQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPoolQuery() is false");
    }


    /**
     *  Matches mapped to any pool. 
     *
     *  @param  match <code> true </code> for mapped to any pool, <code> false 
     *          </code> to match mapped to no pool 
     */

    @OSID @Override
    public void matchAnyRuledPool(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledPoolColumn(), match);
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearRuledPoolTerms() {
        getAssembler().clearTerms(getRuledPoolColumn());
        return;
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getRuledPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Gets the RuledPool column name.
     *
     * @return the column name
     */

    protected String getRuledPoolColumn() {
        return ("ruled_pool");
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this poolProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  poolProcessorRecordType a pool processor record type 
     *  @return <code>true</code> if the poolProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolProcessorRecordType) {
        for (org.osid.provisioning.rules.records.PoolProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  poolProcessorRecordType the pool processor record type 
     *  @return the pool processor query record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorQueryRecord getPoolProcessorQueryRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  poolProcessorRecordType the pool processor record type 
     *  @return the pool processor query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord getPoolProcessorQueryInspectorRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param poolProcessorRecordType the pool processor record type
     *  @return the pool processor search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord getPoolProcessorSearchOrderRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this pool processor. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolProcessorQueryRecord the pool processor query record
     *  @param poolProcessorQueryInspectorRecord the pool processor query inspector
     *         record
     *  @param poolProcessorSearchOrderRecord the pool processor search order record
     *  @param poolProcessorRecordType pool processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorQueryRecord</code>,
     *          <code>poolProcessorQueryInspectorRecord</code>,
     *          <code>poolProcessorSearchOrderRecord</code> or
     *          <code>poolProcessorRecordTypepoolProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addPoolProcessorRecords(org.osid.provisioning.rules.records.PoolProcessorQueryRecord poolProcessorQueryRecord, 
                                      org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord poolProcessorQueryInspectorRecord, 
                                      org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord poolProcessorSearchOrderRecord, 
                                      org.osid.type.Type poolProcessorRecordType) {

        addRecordType(poolProcessorRecordType);

        nullarg(poolProcessorQueryRecord, "pool processor query record");
        nullarg(poolProcessorQueryInspectorRecord, "pool processor query inspector record");
        nullarg(poolProcessorSearchOrderRecord, "pool processor search odrer record");

        this.queryRecords.add(poolProcessorQueryRecord);
        this.queryInspectorRecords.add(poolProcessorQueryInspectorRecord);
        this.searchOrderRecords.add(poolProcessorSearchOrderRecord);
        
        return;
    }
}
