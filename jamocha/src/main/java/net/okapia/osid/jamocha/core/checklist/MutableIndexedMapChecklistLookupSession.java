//
// MutableIndexedMapChecklistLookupSession
//
//    Implements a Checklist lookup service backed by a collection of
//    checklists indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist;


/**
 *  Implements a Checklist lookup service backed by a collection of
 *  checklists. The checklists are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some checklists may be compatible
 *  with more types than are indicated through these checklist
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of checklists can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapChecklistLookupSession
    extends net.okapia.osid.jamocha.core.checklist.spi.AbstractIndexedMapChecklistLookupSession
    implements org.osid.checklist.ChecklistLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapChecklistLookupSession} with no
     *  checklists.
     */

    public MutableIndexedMapChecklistLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapChecklistLookupSession} with a
     *  single checklist.
     *  
     *  @param  checklist a single checklist
     *  @throws org.osid.NullArgumentException {@code checklist}
     *          is {@code null}
     */

    public MutableIndexedMapChecklistLookupSession(org.osid.checklist.Checklist checklist) {
        putChecklist(checklist);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapChecklistLookupSession} using an
     *  array of checklists.
     *
     *  @param  checklists an array of checklists
     *  @throws org.osid.NullArgumentException {@code checklists}
     *          is {@code null}
     */

    public MutableIndexedMapChecklistLookupSession(org.osid.checklist.Checklist[] checklists) {
        putChecklists(checklists);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapChecklistLookupSession} using a
     *  collection of checklists.
     *
     *  @param  checklists a collection of checklists
     *  @throws org.osid.NullArgumentException {@code checklists} is
     *          {@code null}
     */

    public MutableIndexedMapChecklistLookupSession(java.util.Collection<? extends org.osid.checklist.Checklist> checklists) {
        putChecklists(checklists);
        return;
    }
    

    /**
     *  Makes a {@code Checklist} available in this session.
     *
     *  @param  checklist a checklist
     *  @throws org.osid.NullArgumentException {@code checklist{@code  is
     *          {@code null}
     */

    @Override
    public void putChecklist(org.osid.checklist.Checklist checklist) {
        super.putChecklist(checklist);
        return;
    }


    /**
     *  Makes an array of checklists available in this session.
     *
     *  @param  checklists an array of checklists
     *  @throws org.osid.NullArgumentException {@code checklists{@code 
     *          is {@code null}
     */

    @Override
    public void putChecklists(org.osid.checklist.Checklist[] checklists) {
        super.putChecklists(checklists);
        return;
    }


    /**
     *  Makes collection of checklists available in this session.
     *
     *  @param  checklists a collection of checklists
     *  @throws org.osid.NullArgumentException {@code checklist{@code  is
     *          {@code null}
     */

    @Override
    public void putChecklists(java.util.Collection<? extends org.osid.checklist.Checklist> checklists) {
        super.putChecklists(checklists);
        return;
    }


    /**
     *  Removes a Checklist from this session.
     *
     *  @param checklistId the {@code Id} of the checklist
     *  @throws org.osid.NullArgumentException {@code checklistId{@code  is
     *          {@code null}
     */

    @Override
    public void removeChecklist(org.osid.id.Id checklistId) {
        super.removeChecklist(checklistId);
        return;
    }    
}
