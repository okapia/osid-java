//
// MutableIndexedMapEffortLookupSession
//
//    Implements an Effort lookup service backed by a collection of
//    efforts indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements an Effort lookup service backed by a collection of
 *  efforts. The efforts are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some efforts may be compatible
 *  with more types than are indicated through these effort
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of efforts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapEffortLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractIndexedMapEffortLookupSession
    implements org.osid.resourcing.EffortLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapEffortLookupSession} with no efforts.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry}
     *          is {@code null}
     */

      public MutableIndexedMapEffortLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEffortLookupSession} with a
     *  single effort.
     *  
     *  @param foundry the foundry
     *  @param  effort an single effort
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code effort} is {@code null}
     */

    public MutableIndexedMapEffortLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Effort effort) {
        this(foundry);
        putEffort(effort);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEffortLookupSession} using an
     *  array of efforts.
     *
     *  @param foundry the foundry
     *  @param  efforts an array of efforts
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code efforts} is {@code null}
     */

    public MutableIndexedMapEffortLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Effort[] efforts) {
        this(foundry);
        putEfforts(efforts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEffortLookupSession} using a
     *  collection of efforts.
     *
     *  @param foundry the foundry
     *  @param  efforts a collection of efforts
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code efforts} is {@code null}
     */

    public MutableIndexedMapEffortLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Effort> efforts) {

        this(foundry);
        putEfforts(efforts);
        return;
    }
    

    /**
     *  Makes an {@code Effort} available in this session.
     *
     *  @param  effort an effort
     *  @throws org.osid.NullArgumentException {@code effort{@code  is
     *          {@code null}
     */

    @Override
    public void putEffort(org.osid.resourcing.Effort effort) {
        super.putEffort(effort);
        return;
    }


    /**
     *  Makes an array of efforts available in this session.
     *
     *  @param  efforts an array of efforts
     *  @throws org.osid.NullArgumentException {@code efforts{@code 
     *          is {@code null}
     */

    @Override
    public void putEfforts(org.osid.resourcing.Effort[] efforts) {
        super.putEfforts(efforts);
        return;
    }


    /**
     *  Makes collection of efforts available in this session.
     *
     *  @param  efforts a collection of efforts
     *  @throws org.osid.NullArgumentException {@code effort{@code  is
     *          {@code null}
     */

    @Override
    public void putEfforts(java.util.Collection<? extends org.osid.resourcing.Effort> efforts) {
        super.putEfforts(efforts);
        return;
    }


    /**
     *  Removes an Effort from this session.
     *
     *  @param effortId the {@code Id} of the effort
     *  @throws org.osid.NullArgumentException {@code effortId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEffort(org.osid.id.Id effortId) {
        super.removeEffort(effortId);
        return;
    }    
}
