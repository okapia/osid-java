//
// AbstractValueQuery.java
//
//     A template for making a Value Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.value.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for values.
 */

public abstract class AbstractValueQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.configuration.ValueQuery {

    private final java.util.Collection<org.osid.configuration.records.ValueQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a priority match. Multiple ranges can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  low start priority value 
     *  @param  high end priority value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchPriority(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches values with any priority. 
     *
     *  @param  match <code> true </code> if to match values with any 
     *          priority, <code> false </code> to match values with no 
     *          priority 
     */

    @OSID @Override
    public void matchAnyPriority(boolean match) {
        return;
    }


    /**
     *  Clears the priority terms. 
     */

    @OSID @Override
    public void clearPriorityTerms() {
        return;
    }


    /**
     *  Adds a boolean match. 
     *
     *  @param  value a boolean value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchBooleanValue(boolean value, boolean match) {
        return;
    }


    /**
     *  Clears the boolean value terms. 
     */

    @OSID @Override
    public void clearBooleanValueTerms() {
        return;
    }


    /**
     *  Adds a byte string match. Multiple byte arrays can be added to perform 
     *  a boolean <code> OR </code> among them. 
     *
     *  @param  value a byte value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @param  partial <code> true </code> if for a partial match, <code> 
     *          false </code> for complete match 
     */

    @OSID @Override
    public void matchBytesValue(byte[] value, boolean match, boolean partial) {
        return;
    }


    /**
     *  Clears the bytes value terms. 
     */

    @OSID @Override
    public void clearBytesValueTerms() {
        return;
    }


    /**
     *  Adds a cardinal match within the given range inclusive. Multiple 
     *  ranges can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start cardinal value 
     *  @param  high end cardinal value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchCardinalValue(long low, long high, boolean match) {
        return;
    }


    /**
     *  Clears the cardinal value terms. 
     */

    @OSID @Override
    public void clearCardinalValueTerms() {
        return;
    }


    /**
     *  Adds a coordinate match for coordinates inside the specified 
     *  coordinate. Multiple ranges can be added to perform a boolean <code> 
     *  OR </code> among them. 
     *
     *  @param  coordinate a coordinate value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinateValue(org.osid.mapping.Coordinate coordinate, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the coordinate value terms. 
     */

    @OSID @Override
    public void clearCoordinateValueTerms() {
        return;
    }


    /**
     *  Adds a curency match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start currency value 
     *  @param  high a currency value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCurrencyValue(org.osid.financials.Currency low, 
                                   org.osid.financials.Currency high, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the currency value terms. 
     */

    @OSID @Override
    public void clearCurrencyValueTerms() {
        return;
    }


    /**
     *  Adds a <code> DateTime </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start datetime value 
     *  @param  high end datetime value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDateTimeValue(org.osid.calendaring.DateTime low, 
                                   org.osid.calendaring.DateTime high, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the date time value terms. 
     */

    @OSID @Override
    public void clearDateTimeValueTerms() {
        return;
    }


    /**
     *  Adds a decimal match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start decimal value 
     *  @param  high end decimal value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchDecimalValue(java.math.BigDecimal low, 
                                  java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Clears the decimal value terms. 
     */

    @OSID @Override
    public void clearDecimalValueTerms() {
        return;
    }


    /**
     *  Adds a <code> Distance </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start distance value 
     *  @param  high end distance value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDistanceValue(org.osid.mapping.Distance low, 
                                   org.osid.mapping.Distance high, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the distance value terms. 
     */

    @OSID @Override
    public void clearDistanceValueTerms() {
        return;
    }


    /**
     *  Adds a <code> Duration </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start duration value 
     *  @param  high end duration value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDurationValue(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the duration value terms. 
     */

    @OSID @Override
    public void clearDurationValueTerms() {
        return;
    }


    /**
     *  Adds a <code> Heading </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start heading value 
     *  @param  high end heading value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchHeadingValue(org.osid.mapping.Heading low, 
                                  org.osid.mapping.Heading high, boolean match) {
        return;
    }


    /**
     *  Clears the heading value terms. 
     */

    @OSID @Override
    public void clearHeadingValueTerms() {
        return;
    }


    /**
     *  Adds an <code> Id </code> to match. Multiple <code> Ids </code> can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  value an <code> Id </code> value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchIdValue(org.osid.id.Id value, boolean match) {
        return;
    }


    /**
     *  Clears the Id value terms. 
     */

    @OSID @Override
    public void clearIdValueTerms() {
        return;
    }


    /**
     *  Adds an integer match within the given range inclusive. Multiple 
     *  ranges can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start integer value 
     *  @param  high end integer value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchIntegerValue(long low, long high, boolean match) {
        return;
    }


    /**
     *  Clears the integer value terms. 
     */

    @OSID @Override
    public void clearIntegerValueTerms() {
        return;
    }


    /**
     *  Adds a spatial unit match within the given spatial unit inclusive. 
     *  Multiple ranges can be added to perform a boolean <code> OR </code> 
     *  among them. 
     *
     *  @param  value a spatial unit 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSpatialUnitValue(org.osid.mapping.SpatialUnit value, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit value terms. 
     */

    @OSID @Override
    public void clearSpatialUnitValueTerms() {
        return;
    }


    /**
     *  Adds a speed match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start speed value 
     *  @param  high end speed value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSpeedValue(org.osid.mapping.Speed low, 
                                org.osid.mapping.Speed high, boolean match) {
        return;
    }


    /**
     *  Clears the speed value terms. 
     */

    @OSID @Override
    public void clearSpeedValueTerms() {
        return;
    }


    /**
     *  Adds a string match. Multiple strings can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  value string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> value is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> value </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchStringValue(String value, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the string value terms. 
     */

    @OSID @Override
    public void clearStringValueTerms() {
        return;
    }


    /**
     *  Adds a time match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start time value 
     *  @param  high end time value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeValue(org.osid.calendaring.Time low, 
                               org.osid.calendaring.Time high, boolean match) {
        return;
    }


    /**
     *  Clears the time value terms. 
     */

    @OSID @Override
    public void clearTimeValueTerms() {
        return;
    }


    /**
     *  Adds a <code> Type </code> match. Multiple types can be added to 
     *  perform a boolean <code> OR </code> among them. 
     *
     *  @param  value type to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTypeValue(org.osid.type.Type value, boolean match) {
        return;
    }


    /**
     *  Clears the type value terms. 
     */

    @OSID @Override
    public void clearTypeValueTerms() {
        return;
    }


    /**
     *  Adds a <code> Version </code> match within the given range inclusive. 
     *  Multiple types can be added to perform a boolean <code> OR </code> 
     *  among them. 
     *
     *  @param  low start version value 
     *  @param  high end version value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchVersionValue(org.osid.installation.Version low, 
                                  org.osid.installation.Version high, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the version value terms. 
     */

    @OSID @Override
    public void clearVersionValueTerms() {
        return;
    }


    /**
     *  Adds a <code> Type </code> to match on the type of object. Multiple 
     *  types can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  objectType type to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchObjectValueType(org.osid.type.Type objectType, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the object value type value terms. 
     */

    @OSID @Override
    public void clearObjectValueTypeTerms() {
        return;
    }


    /**
     *  Adds an object match. Multiple objects can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  object object to match 
     *  @param  objectType type of object 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> object </code> or <code> 
     *          objectType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchObjectValue(java.lang.Object object, 
                                 org.osid.type.Type objectType, boolean match) {
        return;
    }


    /**
     *  Clears the object value terms. 
     */

    @OSID @Override
    public void clearObjectValueTerms() {
        return;
    }


    /**
     *  Adds a parameter <code> Id </code> for this query. 
     *
     *  @param  parameterId a parameter <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParameterId(org.osid.id.Id parameterId, boolean match) {
        return;
    }


    /**
     *  Clears the parameter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParameterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ParameterQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parameter. 
     *
     *  @return the parameter query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuery getParameterQuery() {
        throw new org.osid.UnimplementedException("supportsParameterQuery() is false");
    }


    /**
     *  Clears the parameter terms. 
     */

    @OSID @Override
    public void clearParameterTerms() {
        return;
    }


    /**
     *  Sets the configuration <code> Id </code> for this query. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        return;
    }


    /**
     *  Gets the record corresponding to the given value query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a value implementing the requested record.
     *
     *  @param valueRecordType a value record type
     *  @return the value query record
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ValueQueryRecord getValueQueryRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ValueQueryRecord record : this.records) {
            if (record.implementsRecordType(valueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this value query. 
     *
     *  @param valueQueryRecord value query record
     *  @param valueRecordType value record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addValueQueryRecord(org.osid.configuration.records.ValueQueryRecord valueQueryRecord, 
                                       org.osid.type.Type valueRecordType) {

        addRecordType(valueRecordType);
        nullarg(valueQueryRecord, "value query record");
        this.records.add(valueQueryRecord);        
        return;
    }
}
