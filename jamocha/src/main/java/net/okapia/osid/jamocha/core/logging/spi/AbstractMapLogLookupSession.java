//
// AbstractMapLogLookupSession
//
//    A simple framework for providing a Log lookup service
//    backed by a fixed collection of logs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.logging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Log lookup service backed by a
 *  fixed collection of logs. The logs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Logs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapLogLookupSession
    extends net.okapia.osid.jamocha.logging.spi.AbstractLogLookupSession
    implements org.osid.logging.LogLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.logging.Log> logs = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.logging.Log>());


    /**
     *  Makes a <code>Log</code> available in this session.
     *
     *  @param  log a log
     *  @throws org.osid.NullArgumentException <code>log<code>
     *          is <code>null</code>
     */

    protected void putLog(org.osid.logging.Log log) {
        this.logs.put(log.getId(), log);
        return;
    }


    /**
     *  Makes an array of logs available in this session.
     *
     *  @param  logs an array of logs
     *  @throws org.osid.NullArgumentException <code>logs<code>
     *          is <code>null</code>
     */

    protected void putLogs(org.osid.logging.Log[] logs) {
        putLogs(java.util.Arrays.asList(logs));
        return;
    }


    /**
     *  Makes a collection of logs available in this session.
     *
     *  @param  logs a collection of logs
     *  @throws org.osid.NullArgumentException <code>logs<code>
     *          is <code>null</code>
     */

    protected void putLogs(java.util.Collection<? extends org.osid.logging.Log> logs) {
        for (org.osid.logging.Log log : logs) {
            this.logs.put(log.getId(), log);
        }

        return;
    }


    /**
     *  Removes a Log from this session.
     *
     *  @param  logId the <code>Id</code> of the log
     *  @throws org.osid.NullArgumentException <code>logId<code> is
     *          <code>null</code>
     */

    protected void removeLog(org.osid.id.Id logId) {
        this.logs.remove(logId);
        return;
    }


    /**
     *  Gets the <code>Log</code> specified by its <code>Id</code>.
     *
     *  @param  logId <code>Id</code> of the <code>Log</code>
     *  @return the log
     *  @throws org.osid.NotFoundException <code>logId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>logId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.Log getLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.logging.Log log = this.logs.get(logId);
        if (log == null) {
            throw new org.osid.NotFoundException("log not found: " + logId);
        }

        return (log);
    }


    /**
     *  Gets all <code>Logs</code>. In plenary mode, the returned
     *  list contains all known logs or an error
     *  results. Otherwise, the returned list may contain only those
     *  logs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Logs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.logging.log.ArrayLogList(this.logs.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.logs.clear();
        super.close();
        return;
    }
}
