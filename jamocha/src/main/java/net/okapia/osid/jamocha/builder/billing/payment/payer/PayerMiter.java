//
// PayerMiter.java
//
//     Defines a Payer miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.payer;


/**
 *  Defines a <code>Payer</code> miter for use with the builders.
 */

public interface PayerMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            org.osid.billing.payment.Payer {


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public void setCustomer(org.osid.billing.Customer customer);


    /**
     *  Sets the credit card number.
     *
     *  @param number a credit card number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setCreditCardNumber(String number);


    /**
     *  Sets the credit card expiration.
     *
     *  @param expiration a credit card expiration
     *  @throws org.osid.NullArgumentException <code>expiration</code>
     *          is <code>null</code>
     */

    public void setCreditCardExpiration(org.osid.calendaring.DateTime expiration);


    /**
     *  Sets the credit card code.
     *
     *  @param code a credit card code
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    public void setCreditCardCode(String code);


    /**
     *  Sets the bank routing number.
     *
     *  @param number a bank routing number
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    public void setBankRoutingNumber(String number);


    /**
     *  Sets the bank account number.
     *
     *  @param number a bank account number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setBankAccountNumber(String number);


    /**
     *  Adds a Payer record.
     *
     *  @param record a payer record
     *  @param recordType the type of payer record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPayerRecord(org.osid.billing.payment.records.PayerRecord record, org.osid.type.Type recordType);
}       


