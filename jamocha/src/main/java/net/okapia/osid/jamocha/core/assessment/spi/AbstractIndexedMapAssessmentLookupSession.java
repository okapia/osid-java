//
// AbstractIndexedMapAssessmentLookupSession.java
//
//    A simple framework for providing an Assessment lookup service
//    backed by a fixed collection of assessments with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Assessment lookup service backed by a
 *  fixed collection of assessments. The assessments are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some assessments may be compatible
 *  with more types than are indicated through these assessment
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Assessments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAssessmentLookupSession
    extends AbstractMapAssessmentLookupSession
    implements org.osid.assessment.AssessmentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.Assessment> assessmentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.Assessment>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.Assessment> assessmentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.Assessment>());


    /**
     *  Makes an <code>Assessment</code> available in this session.
     *
     *  @param  assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAssessment(org.osid.assessment.Assessment assessment) {
        super.putAssessment(assessment);

        this.assessmentsByGenus.put(assessment.getGenusType(), assessment);
        
        try (org.osid.type.TypeList types = assessment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentsByRecord.put(types.getNextType(), assessment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an assessment from this session.
     *
     *  @param assessmentId the <code>Id</code> of the assessment
     *  @throws org.osid.NullArgumentException <code>assessmentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAssessment(org.osid.id.Id assessmentId) {
        org.osid.assessment.Assessment assessment;
        try {
            assessment = getAssessment(assessmentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.assessmentsByGenus.remove(assessment.getGenusType());

        try (org.osid.type.TypeList types = assessment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentsByRecord.remove(types.getNextType(), assessment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAssessment(assessmentId);
        return;
    }


    /**
     *  Gets an <code>AssessmentList</code> corresponding to the given
     *  assessment genus <code>Type</code> which does not include
     *  assessments of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known assessments or an error results. Otherwise,
     *  the returned list may contain only those assessments that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  assessmentGenusType an assessment genus type 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByGenusType(org.osid.type.Type assessmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessment.ArrayAssessmentList(this.assessmentsByGenus.get(assessmentGenusType)));
    }


    /**
     *  Gets an <code>AssessmentList</code> containing the given
     *  assessment record <code>Type</code>. In plenary mode, the
     *  returned list contains all known assessments or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  assessmentRecordType an assessment record type 
     *  @return the returned <code>assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByRecordType(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessment.ArrayAssessmentList(this.assessmentsByRecord.get(assessmentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentsByGenus.clear();
        this.assessmentsByRecord.clear();

        super.close();

        return;
    }
}
