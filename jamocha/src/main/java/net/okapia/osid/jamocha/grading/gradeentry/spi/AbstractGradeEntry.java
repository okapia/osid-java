//
// AbstractGradeEntry.java
//
//     Defines a GradeEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>GradeEntry</code>.
 */

public abstract class AbstractGradeEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.grading.GradeEntry {

    private org.osid.grading.GradebookColumn gradebookColumn;
    private org.osid.resource.Resource keyResource;
    private org.osid.grading.GradeEntry overriddenCalculatedEntry;
    private org.osid.grading.Grade grade;
    private java.math.BigDecimal score;
    private org.osid.calendaring.DateTime timeGraded;
    private org.osid.resource.Resource grader;
    private org.osid.authentication.Agent gradingAgent;

    private boolean derived = false;
    private boolean ignoredForCalculations = false;

    private final java.util.Collection<org.osid.grading.records.GradeEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> GradebookColumn. </code> 
     *
     *  @return the <code> Id </code> of the <code> GradebookColumn </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradebookColumnId() {
        return (this.gradebookColumn.getId());
    }


    /**
     *  Gets the <code> GradebookColumn. </code> 
     *
     *  @return the <code> GradebookColumn </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn()
        throws org.osid.OperationFailedException {

        return (this.gradebookColumn);
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    protected void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        nullarg(gradebookColumn, "gradebook column");
        this.gradebookColumn = gradebookColumn;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the key resource of this entry. The key 
     *  resource may be a student or other applicable key to identify a row of 
     *  grading entries. 
     *
     *  @return <code> Id </code> of the key resource 
     */

    @OSID @Override
    public org.osid.id.Id getKeyResourceId() {
        return (this.keyResource.getId());
    }


    /**
     *  Gets the key resource of this entry. The key resource may be a student 
     *  or other applicable key to identify a row of grading entries. 
     *
     *  @return the key resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getKeyResource()
        throws org.osid.OperationFailedException {

        return (this.keyResource);
    }


    /**
     *  Sets the key resource.
     *
     *  @param resource a key resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setKeyResource(org.osid.resource.Resource resource) {
        nullarg(resource, "key resource");
        this.keyResource = resource;
        return;
    }


    /**
     *  Tests if this is derived entry.
     *
     *  @return <code> true </code> if this is a derived entry, <code>
     *          false </code> if a manual entry. If <code>true</code>,
     *          then <code>overridesCalculatedEntry()</code> must be
     *          <code>false</code>
     */

    @OSID @Override
    public boolean isDerived() {
        return (this.derived);
    }


    /**
     *  Set the derived flag.
     *
     *  @param derived <code> true </code> if this is a derived entry,
     *         <code> false </code> if a manual entry
     */

    public void setDerived(boolean derived) {
        this.derived = derived;
        return;
    }


    /**
     *  Tests if this is a manual entry that overrides a calculated entry. 
     *
     *  @return <code> true </code> if this entry overrides a
     *          calculated entry, <code> false </code> otherwise. If
     *          <code>true</code>, then <code>isDerived()</code> must
     *          be <code>false</code>
     */

    @OSID @Override
    public boolean overridesCalculatedEntry() {
        return (this.overriddenCalculatedEntry != null);
    }


    /**
     *  Gets the calculated entry <code> Id </code> this entry
     *  overrides.
     *
     *  @return the calculated entry <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> overridesDerivedEntry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOverriddenCalculatedEntryId() {
        if (!overridesCalculatedEntry()) {
            throw new org.osid.IllegalStateException("overridesCalculatedEntry() is false");
        }

        return (this.overriddenCalculatedEntry.getId());
    }


    /**
     *  Gets the calculated entry this entry overrides. 
     *
     *  @return the calculated entry 
     *  @throws org.osid.IllegalStateException <code> 
     *          overridesCalculatedEntry() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeEntry getOverriddenCalculatedEntry()
        throws org.osid.OperationFailedException {

        if (!overridesCalculatedEntry()) {
            throw new org.osid.IllegalStateException("overridesCalculatedEntry() is false");
        }

        return (this.overriddenCalculatedEntry);
    }


    /**
     *  Sets the overridden calculated entry.
     *
     *  @param entry an overridden calculated entry
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    protected void setOverriddenCalculatedEntry(org.osid.grading.GradeEntry entry) {
        nullarg(entry, "overridden calculated entry");
        this.overriddenCalculatedEntry = entry;
        return;
    }


    /**
     *  Tests if this is entry should be ignored in any averaging, scaling or 
     *  curve calculation. 
     *
     *  @return <code> true </code> if this entry is ignored, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isIgnoredForCalculations() {
        return (this.ignoredForCalculations);
    }


    /**
     *  Sets the ignored for calculations.
     *
     *  @param ignore <code> true </code> if this entry is ignored,
     *          <code> false </code> otherwise
     */

    protected void setIgnoredForCalculations(boolean ignore) {
        this.ignoredForCalculations = ignore;
        return;
    }


    /**
     *  Tests if a grade or score has been assigned to this entry. Generally, 
     *  an entry is created with a grade or score. 
     *
     *  @return <code> true </code> if a grade has been assigned, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.grade != null);
    }


    /**
     *  Gets the grade <code> Id </code> in this entry if the grading system 
     *  is based on grades. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grade.getId());
    }


    /**
     *  Gets the grade in this entry if the grading system is based on grades. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grade);
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    protected void setGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "grade");
        this.grade = grade;
        return;
    }


    /**
     *  Gets the score in this entry if the grading system is not based on 
     *  grades. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.score);
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException
     *          <code>score</code> is <code>null</code>
     */

    protected void setScore(java.math.BigDecimal score) {
        nullarg(score, "score");
        this.score = score;
        return;
    }


    /**
     *  Gets the time the gradeable object was graded. 
     *
     *  @return the timestamp of the grading entry 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> isDerived() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeGraded() {
        if (!isGraded() || isDerived()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.timeGraded);
    }


    /**
     *  Sets the time graded.
     *
     *  @param time a time graded
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setTimeGraded(org.osid.calendaring.DateTime time) {
        nullarg(time, "time");
        this.timeGraded = time;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Resource </code> that created 
     *  this entry. 
     *
     *  @return the <code> Id </code> of the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code>  or <code> isDerived() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGraderId() {
        if (!isGraded() || isDerived()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grader.getId());
    }


    /**
     *  Gets the <code> Resource </code> that created this entry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() is false or 
     *          isDerived() is true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getGrader()
        throws org.osid.OperationFailedException {

        if (!isGraded() || isDerived()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grader);
    }


    /**
     *  Sets the grader.
     *
     *  @param grader a grader
     *  @throws org.osid.NullArgumentException
     *          <code>grader</code> is <code>null</code>
     */

    protected void setGrader(org.osid.resource.Resource grader) {
        nullarg(grader, "grader");
        this.grader = grader;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> that created 
     *  this entry. 
     *
     *  @return the <code> Id </code> of the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code>  or <code> isDerived() </code> is <code> 
     *          true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradingAgentId() {
        if (!isGraded() || isDerived()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.gradingAgent.getId());
    }


    /**
     *  Gets the <code> Agent </code> that created this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() is
     *          <code>false</code> or <code>isDerived()</code> is
     *          <code>true</code> </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getGradingAgent()
        throws org.osid.OperationFailedException {

        if (!isGraded() || isDerived()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.gradingAgent);
    }


    /**
     *  Sets the grading agent.
     *
     *  @param gradingAgent a grading agent
     *  @throws org.osid.NullArgumentException
     *          <code>gradingAgent</code> is <code>null</code>
     */

    protected void setGradingAgent(org.osid.authentication.Agent gradingAgent) {
        nullarg(gradingAgent, "grading agent");
        this.gradingAgent = gradingAgent;
        return;
    }


    /**
     *  Tests if this gradeEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradeEntryRecordType a grade entry record type 
     *  @return <code>true</code> if the gradeEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeEntryRecordType) {
        for (org.osid.grading.records.GradeEntryRecord record : this.records) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>GradeEntry</code> record <code>Type</code>.
     *
     *  @param  gradeEntryRecordType the grade entry record type 
     *  @return the grade entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntryRecord getGradeEntryRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeEntryRecord record : this.records) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeEntryRecord the grade entry record
     *  @param gradeEntryRecordType grade entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecord</code> or
     *          <code>gradeEntryRecordTypegradeEntry</code> is
     *          <code>null</code>
     */
            
    protected void addGradeEntryRecord(org.osid.grading.records.GradeEntryRecord gradeEntryRecord, 
                                       org.osid.type.Type gradeEntryRecordType) {
        
        nullarg(gradeEntryRecord, "grade entry record");
        addRecordType(gradeEntryRecordType);
        this.records.add(gradeEntryRecord);
        
        return;
    }
}
