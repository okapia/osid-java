//
// AbstractInquirySearchOdrer.java
//
//     Defines an InquirySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code InquirySearchOrder}.
 */

public abstract class AbstractInquirySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.inquiry.InquirySearchOrder {

    private final java.util.Collection<org.osid.inquiry.records.InquirySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by the audit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAudit(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an audit search order is available. 
     *
     *  @return <code> true </code> if an audit search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditSearchOrder() {
        return (false);
    }


    /**
     *  Gets the audit search order. 
     *
     *  @return the audit search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAuditSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchOrder getAuditSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAuditSearchOrder() is false");
    }


    /**
     *  Orders the results by the question. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuestion(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the required flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequired(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the affirmation required flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAffirmationRequired(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the needs one response flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNeedsOneResponse(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  inquiryRecordType an inquiry record type 
     *  @return {@code true} if the inquiryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inquiryRecordType) {
        for (org.osid.inquiry.records.InquirySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  inquiryRecordType the inquiry record type 
     *  @return the inquiry search order record
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(inquiryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.inquiry.records.InquirySearchOrderRecord getInquirySearchOrderRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquirySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this inquiry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inquiryRecord the inquiry search odrer record
     *  @param inquiryRecordType inquiry record type
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryRecord} or
     *          {@code inquiryRecordTypeinquiry} is
     *          {@code null}
     */
            
    protected void addInquiryRecord(org.osid.inquiry.records.InquirySearchOrderRecord inquirySearchOrderRecord, 
                                     org.osid.type.Type inquiryRecordType) {

        addRecordType(inquiryRecordType);
        this.records.add(inquirySearchOrderRecord);
        
        return;
    }
}
