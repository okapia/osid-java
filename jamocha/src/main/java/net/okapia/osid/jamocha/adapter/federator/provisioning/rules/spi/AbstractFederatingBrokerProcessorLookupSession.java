//
// AbstractFederatingBrokerProcessorLookupSession.java
//
//     An abstract federating adapter for a BrokerProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BrokerProcessorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBrokerProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.rules.BrokerProcessorLookupSession>
    implements org.osid.provisioning.rules.BrokerProcessorLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new <code>AbstractFederatingBrokerProcessorLookupSession</code>.
     */

    protected AbstractFederatingBrokerProcessorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.rules.BrokerProcessorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>BrokerProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokerProcessors() {
        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            if (session.canLookupBrokerProcessors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>BrokerProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerProcessorView() {
        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            session.useComparativeBrokerProcessorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>BrokerProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerProcessorView() {
        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            session.usePlenaryBrokerProcessorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker processors in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only active broker processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerProcessorView() {
        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            session.useActiveBrokerProcessorView();
        }

        return;
    }


    /**
     *  Active and inactive broker processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerProcessorView() {
        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            session.useAnyStatusBrokerProcessorView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>BrokerProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BrokerProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>BrokerProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @param  brokerProcessorId <code>Id</code> of the
     *          <code>BrokerProcessor</code>
     *  @return the broker processor
     *  @throws org.osid.NotFoundException <code>brokerProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessor getBrokerProcessor(org.osid.id.Id brokerProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            try {
                return (session.getBrokerProcessor(brokerProcessorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(brokerProcessorId + " not found");
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerProcessors specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>BrokerProcessors</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByIds(org.osid.id.IdList brokerProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.MutableBrokerProcessorList ret = new net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.MutableBrokerProcessorList();

        try (org.osid.id.IdList ids = brokerProcessorIds) {
            while (ids.hasNext()) {
                ret.addBrokerProcessor(getBrokerProcessor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> corresponding to the
     *  given broker processor genus <code>Type</code> which does not
     *  include broker processors of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorGenusType a brokerProcessor genus type 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByGenusType(org.osid.type.Type brokerProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessor.FederatingBrokerProcessorList ret = getBrokerProcessorList();

        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            ret.addBrokerProcessorList(session.getBrokerProcessorsByGenusType(brokerProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> corresponding to the
     *  given broker processor genus <code>Type</code> and include any
     *  additional broker processors with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorGenusType a brokerProcessor genus type 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByParentGenusType(org.osid.type.Type brokerProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessor.FederatingBrokerProcessorList ret = getBrokerProcessorList();

        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            ret.addBrokerProcessorList(session.getBrokerProcessorsByParentGenusType(brokerProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerProcessorList</code> containing the given
     *  broker processor record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known broker
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker processors are returned.
     *
     *  @param  brokerProcessorRecordType a brokerProcessor record type 
     *  @return the returned <code>BrokerProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByRecordType(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessor.FederatingBrokerProcessorList ret = getBrokerProcessorList();

        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            ret.addBrokerProcessorList(session.getBrokerProcessorsByRecordType(brokerProcessorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>BrokerProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  broker processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @return a list of <code>BrokerProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessor.FederatingBrokerProcessorList ret = getBrokerProcessorList();

        for (org.osid.provisioning.rules.BrokerProcessorLookupSession session : getSessions()) {
            ret.addBrokerProcessorList(session.getBrokerProcessors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessor.FederatingBrokerProcessorList getBrokerProcessorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessor.ParallelBrokerProcessorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessor.CompositeBrokerProcessorList());
        }
    }
}
