//
// AbstractAssemblyBrokerQuery.java
//
//     A BrokerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.broker.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BrokerQuery that stores terms.
 */

public abstract class AbstractAssemblyBrokerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.provisioning.BrokerQuery,
               org.osid.provisioning.BrokerQueryInspector,
               org.osid.provisioning.BrokerSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.BrokerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.BrokerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.BrokerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBrokerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBrokerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        getAssembler().addIdTerm(getQueueIdColumn(), queueId, match);
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        getAssembler().clearTerms(getQueueIdColumn());
        return;
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (getAssembler().getIdTerms(getQueueIdColumn()));
    }


    /**
     *  Gets the QueueId column name.
     *
     * @return the column name
     */

    protected String getQueueIdColumn() {
        return ("queue_id");
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Matches brokers with any queue. 
     *
     *  @param  match <code> true </code> to match brokers with any queue, 
     *          <code> false </code> to match brokers with no queue 
     */

    @OSID @Override
    public void matchAnyQueue(boolean match) {
        getAssembler().addIdWildcardTerm(getQueueColumn(), match);
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        getAssembler().clearTerms(getQueueColumn());
        return;
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.provisioning.QueueQueryInspector[0]);
    }


    /**
     *  Gets the Queue column name.
     *
     * @return the column name
     */

    protected String getQueueColumn() {
        return ("queue");
    }


    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        getAssembler().addIdTerm(getPoolIdColumn(), poolId, match);
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        getAssembler().clearTerms(getPoolIdColumn());
        return;
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPoolIdTerms() {
        return (getAssembler().getIdTerms(getPoolIdColumn()));
    }


    /**
     *  Gets the PoolId column name.
     *
     * @return the column name
     */

    protected String getPoolIdColumn() {
        return ("pool_id");
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Matches brokers with any pool. 
     *
     *  @param  match <code> true </code> to match brokers with any pool, 
     *          <code> false </code> to match brokers with no pool 
     */

    @OSID @Override
    public void matchAnyPool(boolean match) {
        getAssembler().addIdWildcardTerm(getPoolColumn(), match);
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        getAssembler().clearTerms(getPoolColumn());
        return;
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Gets the Pool column name.
     *
     * @return the column name
     */

    protected String getPoolColumn() {
        return ("pool");
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this broker supports the given record
     *  <code>Type</code>.
     *
     *  @param  brokerRecordType a broker record type 
     *  @return <code>true</code> if the brokerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type brokerRecordType) {
        for (org.osid.provisioning.records.BrokerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  brokerRecordType the broker record type 
     *  @return the broker query record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.BrokerQueryRecord getBrokerQueryRecord(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.BrokerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(brokerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  brokerRecordType the broker record type 
     *  @return the broker query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.BrokerQueryInspectorRecord getBrokerQueryInspectorRecord(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.BrokerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(brokerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param brokerRecordType the broker record type
     *  @return the broker search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.BrokerSearchOrderRecord getBrokerSearchOrderRecord(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.BrokerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(brokerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this broker. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param brokerQueryRecord the broker query record
     *  @param brokerQueryInspectorRecord the broker query inspector
     *         record
     *  @param brokerSearchOrderRecord the broker search order record
     *  @param brokerRecordType broker record type
     *  @throws org.osid.NullArgumentException
     *          <code>brokerQueryRecord</code>,
     *          <code>brokerQueryInspectorRecord</code>,
     *          <code>brokerSearchOrderRecord</code> or
     *          <code>brokerRecordTypebroker</code> is
     *          <code>null</code>
     */
            
    protected void addBrokerRecords(org.osid.provisioning.records.BrokerQueryRecord brokerQueryRecord, 
                                      org.osid.provisioning.records.BrokerQueryInspectorRecord brokerQueryInspectorRecord, 
                                      org.osid.provisioning.records.BrokerSearchOrderRecord brokerSearchOrderRecord, 
                                      org.osid.type.Type brokerRecordType) {

        addRecordType(brokerRecordType);

        nullarg(brokerQueryRecord, "broker query record");
        nullarg(brokerQueryInspectorRecord, "broker query inspector record");
        nullarg(brokerSearchOrderRecord, "broker search odrer record");

        this.queryRecords.add(brokerQueryRecord);
        this.queryInspectorRecords.add(brokerQueryInspectorRecord);
        this.searchOrderRecords.add(brokerSearchOrderRecord);
        
        return;
    }
}
