//
// AbstractAdapterWarehouseLookupSession.java
//
//    A Warehouse lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Warehouse lookup session adapter.
 */

public abstract class AbstractAdapterWarehouseLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inventory.WarehouseLookupSession {

    private final org.osid.inventory.WarehouseLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterWarehouseLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterWarehouseLookupSession(org.osid.inventory.WarehouseLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Warehouse} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupWarehouses() {
        return (this.session.canLookupWarehouses());
    }


    /**
     *  A complete view of the {@code Warehouse} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWarehouseView() {
        this.session.useComparativeWarehouseView();
        return;
    }


    /**
     *  A complete view of the {@code Warehouse} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWarehouseView() {
        this.session.usePlenaryWarehouseView();
        return;
    }

     
    /**
     *  Gets the {@code Warehouse} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Warehouse} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Warehouse} and
     *  retained for compatibility.
     *
     *  @param warehouseId {@code Id} of the {@code Warehouse}
     *  @return the warehouse
     *  @throws org.osid.NotFoundException {@code warehouseId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code warehouseId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWarehouse(warehouseId));
    }


    /**
     *  Gets a {@code WarehouseList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  warehouses specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Warehouses} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  warehouseIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Warehouse} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code warehouseIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByIds(org.osid.id.IdList warehouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWarehousesByIds(warehouseIds));
    }


    /**
     *  Gets a {@code WarehouseList} corresponding to the given
     *  warehouse genus {@code Type} which does not include
     *  warehouses of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned {@code Warehouse} list
     *  @throws org.osid.NullArgumentException
     *          {@code warehouseGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWarehousesByGenusType(warehouseGenusType));
    }


    /**
     *  Gets a {@code WarehouseList} corresponding to the given
     *  warehouse genus {@code Type} and include any additional
     *  warehouses with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned {@code Warehouse} list
     *  @throws org.osid.NullArgumentException
     *          {@code warehouseGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByParentGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWarehousesByParentGenusType(warehouseGenusType));
    }


    /**
     *  Gets a {@code WarehouseList} containing the given
     *  warehouse record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseRecordType a warehouse record type 
     *  @return the returned {@code Warehouse} list
     *  @throws org.osid.NullArgumentException
     *          {@code warehouseRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByRecordType(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWarehousesByRecordType(warehouseRecordType));
    }


    /**
     *  Gets a {@code WarehouseList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Warehouse} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWarehousesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Warehouses}. 
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Warehouses} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWarehouses());
    }
}
