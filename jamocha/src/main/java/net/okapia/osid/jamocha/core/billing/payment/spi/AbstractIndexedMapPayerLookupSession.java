//
// AbstractIndexedMapPayerLookupSession.java
//
//    A simple framework for providing a Payer lookup service
//    backed by a fixed collection of payers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Payer lookup service backed by a
 *  fixed collection of payers. The payers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some payers may be compatible
 *  with more types than are indicated through these payer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Payers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPayerLookupSession
    extends AbstractMapPayerLookupSession
    implements org.osid.billing.payment.PayerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.billing.payment.Payer> payersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.payment.Payer>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.payment.Payer> payersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.payment.Payer>());


    /**
     *  Makes a <code>Payer</code> available in this session.
     *
     *  @param  payer a payer
     *  @throws org.osid.NullArgumentException <code>payer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPayer(org.osid.billing.payment.Payer payer) {
        super.putPayer(payer);

        this.payersByGenus.put(payer.getGenusType(), payer);
        
        try (org.osid.type.TypeList types = payer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.payersByRecord.put(types.getNextType(), payer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a payer from this session.
     *
     *  @param payerId the <code>Id</code> of the payer
     *  @throws org.osid.NullArgumentException <code>payerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePayer(org.osid.id.Id payerId) {
        org.osid.billing.payment.Payer payer;
        try {
            payer = getPayer(payerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.payersByGenus.remove(payer.getGenusType());

        try (org.osid.type.TypeList types = payer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.payersByRecord.remove(types.getNextType(), payer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePayer(payerId);
        return;
    }


    /**
     *  Gets a <code>PayerList</code> corresponding to the given
     *  payer genus <code>Type</code> which does not include
     *  payers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known payers or an error results. Otherwise,
     *  the returned list may contain only those payers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  payerGenusType a payer genus type 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByGenusType(org.osid.type.Type payerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.payment.payer.ArrayPayerList(this.payersByGenus.get(payerGenusType)));
    }


    /**
     *  Gets a <code>PayerList</code> containing the given
     *  payer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known payers or an error
     *  results. Otherwise, the returned list may contain only those
     *  payers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  payerRecordType a payer record type 
     *  @return the returned <code>payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByRecordType(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.payment.payer.ArrayPayerList(this.payersByRecord.get(payerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.payersByGenus.clear();
        this.payersByRecord.clear();

        super.close();

        return;
    }
}
