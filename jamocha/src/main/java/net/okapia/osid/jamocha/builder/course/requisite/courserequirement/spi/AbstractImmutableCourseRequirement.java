//
// AbstractImmutableCourseRequirement.java
//
//     Wraps a mutable CourseRequirement to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.courserequirement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>CourseRequirement</code> to hide modifiers. This
 *  wrapper provides an immutized CourseRequirement from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying courseRequirement whose state changes are visible.
 */

public abstract class AbstractImmutableCourseRequirement
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.course.requisite.CourseRequirement {

    private final org.osid.course.requisite.CourseRequirement courseRequirement;


    /**
     *  Constructs a new <code>AbstractImmutableCourseRequirement</code>.
     *
     *  @param courseRequirement the course requirement to immutablize
     *  @throws org.osid.NullArgumentException <code>courseRequirement</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCourseRequirement(org.osid.course.requisite.CourseRequirement courseRequirement) {
        super(courseRequirement);
        this.courseRequirement = courseRequirement;
        return;
    }


    /**
     *  Gets any <code> Requisites </code> that may be substituted in place of 
     *  this <code> CourseRequirement. </code> All <code> Requisites </code> 
     *  must be satisifed to be a substitute for this course requirement. 
     *  Inactive <code> Requisites </code> are not evaluated but if no 
     *  applicable requisite exists, then the alternate requisite is not 
     *  satisifed. 
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.courseRequirement.getAltRequisites());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Course. </code> 
     *
     *  @return the course <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.courseRequirement.getCourseId());
    }


    /**
     *  Gets the <code> Course. </code> 
     *
     *  @return the course 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.courseRequirement.getCourse());
    }


    /**
     *  Tests if this requirement requires completion of the course with a 
     *  passing grade. 
     *
     *  @return <code> true </code> if a completion of the course is required, 
     *          <code> false </code> is the course is a co-requisite 
     */

    @OSID @Override
    public boolean requiresCompletion() {
        return (this.courseRequirement.requiresCompletion());
    }


    /**
     *  Tests if the course must be completed within the required duration. 
     *
     *  @return <code> true </code> if the course must be completed within a 
     *          required time, <code> false </code> if it could have been 
     *          completed at any time in the past 
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.courseRequirement.hasTimeframe());
    }


    /**
     *  Gets the timeframe in which the course has to be completed. A negative 
     *  duration indicates the course had to be completed within the specified 
     *  amount of time in the past. A posiitive duration indicates the course 
     *  must be completed within the specified amount of time in the future. A 
     *  zero duration indicates the course must be completed in the current 
     *  term. 
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        return (this.courseRequirement.getTimeframe());
    }


    /**
     *  Tests if a minimum grade above passing is required in the completion 
     *  of the course or maintained at this level during a co-requisite. 
     *
     *  @return <code> true </code> if a minimum grade is required, <code> 
     *          false </code> if the course just has to be passed 
     */

    @OSID @Override
    public boolean hasMinimumGrade() {
        return (this.courseRequirement.hasMinimumGrade());
    }


    /**
     *  Gets the minimum grade <code> Id. </code> 
     *
     *  @return the minimum grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGrade() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumGradeId() {
        return (this.courseRequirement.getMinimumGradeId());
    }


    /**
     *  Gets the minimum grade. 
     *
     *  @return the minimum grade 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGrade() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getMinimumGrade()
        throws org.osid.OperationFailedException {

        return (this.courseRequirement.getMinimumGrade());
    }


    /**
     *  Tests if a minimum score above passing is required in the completion 
     *  of the course or maintained at this level during a co-requisite. 
     *
     *  @return <code> true </code> if a minimum score is required, <code> 
     *          false </code> if the course just has to be passed 
     */

    @OSID @Override
    public boolean hasMinimumScore() {
        return (this.courseRequirement.hasMinimumScore());
    }


    /**
     *  Gets the scoring system <code> Id </code> for the minimum score. 
     *
     *  @return the scoring system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumScoreSystemId() {
        return (this.courseRequirement.getMinimumScoreSystemId());
    }


    /**
     *  Gets the scoring system for the minimum score. 
     *
     *  @return the scoring system 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getMinimumScoreSystem()
        throws org.osid.OperationFailedException {

        return (this.courseRequirement.getMinimumScoreSystem());
    }


    /**
     *  Gets the minimum score. 
     *
     *  @return the minimum score 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumScore() {
        return (this.courseRequirement.getMinimumScore());
    }


    /**
     *  Tests if a minimum credits earned from the completion of the course, 
     *  or registered for in a co-requisite is required. 
     *
     *  @return <code> true </code> if a minimum credits is required, <code> 
     *          false </code> otehrwise 
     */

    @OSID @Override
    public boolean hasMinimumEarnedCredits() {
        return (this.courseRequirement.hasMinimumEarnedCredits());
    }


    /**
     *  Gets the minimum earned credits. 
     *
     *  @return the minimum credits 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasMinimumEarnedCredits() </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumEarnedCredits() {
        return (this.courseRequirement.getMinimumEarnedCredits());
    }


    /**
     *  Gets the course requirement record corresponding to the given <code> 
     *  CourseRequirement </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> courseRequirementRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(courseRequirementRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  courseRequirementRecordType the type of course requirement 
     *          record to retrieve 
     *  @return the course requirement record 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseRequirementRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(courseRequirementRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.records.CourseRequirementRecord getCourseRequirementRecord(org.osid.type.Type courseRequirementRecordType)
        throws org.osid.OperationFailedException {

        return (this.courseRequirement.getCourseRequirementRecord(courseRequirementRecordType));
    }
}

