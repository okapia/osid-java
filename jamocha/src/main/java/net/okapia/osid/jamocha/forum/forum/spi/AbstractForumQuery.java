//
// AbstractForumQuery.java
//
//     A template for making a Forum Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for forums.
 */

public abstract class AbstractForumQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.forum.ForumQuery {

    private final java.util.Collection<org.osid.forum.records.ForumQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the reply <code> Id </code> for this query to match replies 
     *  assigned to forums. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReplyId(org.osid.id.Id replyId, boolean match) {
        return;
    }


    /**
     *  Clears the reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReplyIdTerms() {
        return;
    }


    /**
     *  Tests if a reply query is available. 
     *
     *  @return <code> true </code> if a reply query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum. 
     *
     *  @return the reply query 
     *  @throws org.osid.UnimplementedException <code> supportsReplyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getReplyQuery() {
        throw new org.osid.UnimplementedException("supportsReplyQuery() is false");
    }


    /**
     *  Matches forums with any reply. 
     *
     *  @param  match <code> true </code> to match forums with any reply, 
     *          <code> false </code> to match forums with no replies 
     */

    @OSID @Override
    public void matchAnyReply(boolean match) {
        return;
    }


    /**
     *  Clears the reply terms. 
     */

    @OSID @Override
    public void clearReplyTerms() {
        return;
    }


    /**
     *  Sets the post <code> Id </code> for this query to match replies 
     *  assigned to posts. 
     *
     *  @param  postId a post <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPostId(org.osid.id.Id postId, boolean match) {
        return;
    }


    /**
     *  Clears the post <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PostQuery </code> is available. 
     *
     *  @return <code> true </code> if a post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the post query 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuery getPostQuery() {
        throw new org.osid.UnimplementedException("supportsPostQuery() is false");
    }


    /**
     *  Matches forums with any post. 
     *
     *  @param  match <code> true </code> to match forums with any post, 
     *          <code> false </code> to match forums with no posts 
     */

    @OSID @Override
    public void matchAnyPost(boolean match) {
        return;
    }


    /**
     *  Clears the post terms. 
     */

    @OSID @Override
    public void clearPostTerms() {
        return;
    }


    /**
     *  Sets the forum <code> Id </code> for this query to match forums that 
     *  have the specified forum as an ancestor. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorForumId(org.osid.id.Id forumId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorForumIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorForumQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getAncestorForumQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorForumQuery() is false");
    }


    /**
     *  Matches forums with any ancestor. 
     *
     *  @param  match <code> true </code> to match forums with any ancestor, 
     *          <code> false </code> to match root forums 
     */

    @OSID @Override
    public void matchAnyAncestorForum(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor forum terms. 
     */

    @OSID @Override
    public void clearAncestorForumTerms() {
        return;
    }


    /**
     *  Sets the forum <code> Id </code> for this query to match forums that 
     *  have the specified forum as a descendant. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantForumId(org.osid.id.Id forumId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantForumIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantForumQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getDescendantForumQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantForumQuery() is false");
    }


    /**
     *  Matches forums with any descendant. 
     *
     *  @param  match <code> true </code> to match forums with any descendant, 
     *          <code> false </code> to match leaf forums 
     */

    @OSID @Override
    public void matchAnyDescendantForum(boolean match) {
        return;
    }


    /**
     *  Clears the descendant forum terms. 
     */

    @OSID @Override
    public void clearDescendantForumTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given forum query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a forum implementing the requested record.
     *
     *  @param forumRecordType a forum record type
     *  @return the forum query record
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(forumRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ForumQueryRecord getForumQueryRecord(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ForumQueryRecord record : this.records) {
            if (record.implementsRecordType(forumRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(forumRecordType + " is not supported");
    }


    /**
     *  Adds a record to this forum query. 
     *
     *  @param forumQueryRecord forum query record
     *  @param forumRecordType forum record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addForumQueryRecord(org.osid.forum.records.ForumQueryRecord forumQueryRecord, 
                                          org.osid.type.Type forumRecordType) {

        addRecordType(forumRecordType);
        nullarg(forumQueryRecord, "forum query record");
        this.records.add(forumQueryRecord);        
        return;
    }
}
