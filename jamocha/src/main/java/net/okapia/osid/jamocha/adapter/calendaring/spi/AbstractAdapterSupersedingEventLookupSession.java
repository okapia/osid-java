//
// AbstractAdapterSupersedingEventLookupSession.java
//
//    A SupersedingEvent lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SupersedingEvent lookup session adapter.
 */

public abstract class AbstractAdapterSupersedingEventLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.SupersedingEventLookupSession {

    private final org.osid.calendaring.SupersedingEventLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSupersedingEventLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSupersedingEventLookupSession(org.osid.calendaring.SupersedingEventLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code SupersedingEvent} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSupersedingEvents() {
        return (this.session.canLookupSupersedingEvents());
    }


    /**
     *  A complete view of the {@code SupersedingEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSupersedingEventView() {
        this.session.useComparativeSupersedingEventView();
        return;
    }


    /**
     *  A complete view of the {@code SupersedingEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySupersedingEventView() {
        this.session.usePlenarySupersedingEventView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include superseding events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active superseding events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSupersedingEventView() {
        this.session.useActiveSupersedingEventView();
        return;
    }


    /**
     *  Active and inactive superseding events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSupersedingEventView() {
        this.session.useAnyStatusSupersedingEventView();
        return;
    }
    
     
    /**
     *  Gets the {@code SupersedingEvent} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SupersedingEvent} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SupersedingEvent} and
     *  retained for compatibility.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param supersedingEventId {@code Id} of the {@code SupersedingEvent}
     *  @return the superseding event
     *  @throws org.osid.NotFoundException {@code supersedingEventId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code supersedingEventId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEvent getSupersedingEvent(org.osid.id.Id supersedingEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEvent(supersedingEventId));
    }


    /**
     *  Gets a {@code SupersedingEventList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  supersedingEvents specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code SupersedingEvents} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SupersedingEvent} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByIds(org.osid.id.IdList supersedingEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventsByIds(supersedingEventIds));
    }


    /**
     *  Gets a {@code SupersedingEventList} corresponding to the given
     *  superseding event genus {@code Type} which does not include
     *  superseding events of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned {@code SupersedingEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventsByGenusType(supersedingEventGenusType));
    }


    /**
     *  Gets a {@code SupersedingEventList} corresponding to the given
     *  superseding event genus {@code Type} and include any additional
     *  superseding events with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned {@code SupersedingEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByParentGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventsByParentGenusType(supersedingEventGenusType));
    }


    /**
     *  Gets a {@code SupersedingEventList} containing the given
     *  superseding event record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventRecordType a supersedingEvent record type 
     *  @return the returned {@code SupersedingEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByRecordType(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventsByRecordType(supersedingEventRecordType));
    }


    /**
     *  Gets the {@code SupersedingEvents} related to the relative
     *  event {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the
     *  returned list may contain only those superseding events that
     *  are accessible through this session.
     *  
     *  In active mode, supersdeing events are returned that are
     *  currently active. In any status mode, active and inactive
     *  superseding events are returned.
     *
     *  @param  supersededEventId {@code Id} of the related event 
     *  @return the superseding events 
     *  @throws org.osid.NotFoundException {@code supersededEventId} 
     *          not found 
     *  @throws org.osid.NullArgumentException {@code supersededEventId 
     *         } is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsBySupersededEvent(org.osid.id.Id supersededEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventsBySupersededEvent(supersededEventId));
    }


    /**
     *  Gets all {@code SupersedingEvents}. 
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @return a list of {@code SupersedingEvents} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEvents());
    }
}
