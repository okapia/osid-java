//
// AbstractAdapterQueueConstrainerEnablerLookupSession.java
//
//    A QueueConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A QueueConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterQueueConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.tracking.rules.QueueConstrainerEnablerLookupSession {

    private final org.osid.tracking.rules.QueueConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterQueueConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterQueueConstrainerEnablerLookupSession(org.osid.tracking.rules.QueueConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code FrontOffice/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code FrontOffice Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the {@code FrontOffice} associated with this session.
     *
     *  @return the {@code FrontOffice} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform {@code QueueConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupQueueConstrainerEnablers() {
        return (this.session.canLookupQueueConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code QueueConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueConstrainerEnablerView() {
        this.session.useComparativeQueueConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code QueueConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueConstrainerEnablerView() {
        this.session.usePlenaryQueueConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainer enablers in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    

    /**
     *  Only active queue constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueConstrainerEnablerView() {
        this.session.useActiveQueueConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive queue constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueConstrainerEnablerView() {
        this.session.useAnyStatusQueueConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code QueueConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code QueueConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code QueueConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param queueConstrainerEnablerId {@code Id} of the {@code QueueConstrainerEnabler}
     *  @return the queue constrainer enabler
     *  @throws org.osid.NotFoundException {@code queueConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code queueConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnabler getQueueConstrainerEnabler(org.osid.id.Id queueConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainerEnabler(queueConstrainerEnablerId));
    }


    /**
     *  Gets a {@code QueueConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queueConstrainerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code QueueConstrainerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  queueConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code QueueConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByIds(org.osid.id.IdList queueConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainerEnablersByIds(queueConstrainerEnablerIds));
    }


    /**
     *  Gets a {@code QueueConstrainerEnablerList} corresponding to the given
     *  queue constrainer enabler genus {@code Type} which does not include
     *  queue constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  queueConstrainerEnablerGenusType a queueConstrainerEnabler genus type 
     *  @return the returned {@code QueueConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByGenusType(org.osid.type.Type queueConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainerEnablersByGenusType(queueConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code QueueConstrainerEnablerList} corresponding to the given
     *  queue constrainer enabler genus {@code Type} and include any additional
     *  queue constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  queueConstrainerEnablerGenusType a queueConstrainerEnabler genus type 
     *  @return the returned {@code QueueConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByParentGenusType(org.osid.type.Type queueConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainerEnablersByParentGenusType(queueConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code QueueConstrainerEnablerList} containing the given
     *  queue constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  queueConstrainerEnablerRecordType a queueConstrainerEnabler record type 
     *  @return the returned {@code QueueConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByRecordType(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainerEnablersByRecordType(queueConstrainerEnablerRecordType));
    }


    /**
     *  Gets a {@code QueueConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code QueueConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code QueueConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code QueueConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getQueueConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code QueueConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainer enablers
     *  are returned.
     *
     *  @return a list of {@code QueueConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerList getQueueConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainerEnablers());
    }
}
