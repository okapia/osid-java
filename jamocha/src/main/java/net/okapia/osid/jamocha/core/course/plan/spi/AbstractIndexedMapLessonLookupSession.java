//
// AbstractIndexedMapLessonLookupSession.java
//
//    A simple framework for providing a Lesson lookup service
//    backed by a fixed collection of lessons with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Lesson lookup service backed by a
 *  fixed collection of lessons. The lessons are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some lessons may be compatible
 *  with more types than are indicated through these lesson
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Lessons</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapLessonLookupSession
    extends AbstractMapLessonLookupSession
    implements org.osid.course.plan.LessonLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.plan.Lesson> lessonsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.plan.Lesson>());
    private final MultiMap<org.osid.type.Type, org.osid.course.plan.Lesson> lessonsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.plan.Lesson>());


    /**
     *  Makes a <code>Lesson</code> available in this session.
     *
     *  @param  lesson a lesson
     *  @throws org.osid.NullArgumentException <code>lesson<code> is
     *          <code>null</code>
     */

    @Override
    protected void putLesson(org.osid.course.plan.Lesson lesson) {
        super.putLesson(lesson);

        this.lessonsByGenus.put(lesson.getGenusType(), lesson);
        
        try (org.osid.type.TypeList types = lesson.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.lessonsByRecord.put(types.getNextType(), lesson);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a lesson from this session.
     *
     *  @param lessonId the <code>Id</code> of the lesson
     *  @throws org.osid.NullArgumentException <code>lessonId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeLesson(org.osid.id.Id lessonId) {
        org.osid.course.plan.Lesson lesson;
        try {
            lesson = getLesson(lessonId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.lessonsByGenus.remove(lesson.getGenusType());

        try (org.osid.type.TypeList types = lesson.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.lessonsByRecord.remove(types.getNextType(), lesson);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeLesson(lessonId);
        return;
    }


    /**
     *  Gets a <code>LessonList</code> corresponding to the given
     *  lesson genus <code>Type</code> which does not include
     *  lessons of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known lessons or an error results. Otherwise,
     *  the returned list may contain only those lessons that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  lessonGenusType a lesson genus type 
     *  @return the returned <code>Lesson</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>lessonGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByGenusType(org.osid.type.Type lessonGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.plan.lesson.ArrayLessonList(this.lessonsByGenus.get(lessonGenusType)));
    }


    /**
     *  Gets a <code>LessonList</code> containing the given
     *  lesson record <code>Type</code>. In plenary mode, the
     *  returned list contains all known lessons or an error
     *  results. Otherwise, the returned list may contain only those
     *  lessons that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  lessonRecordType a lesson record type 
     *  @return the returned <code>lesson</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByRecordType(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.plan.lesson.ArrayLessonList(this.lessonsByRecord.get(lessonRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.lessonsByGenus.clear();
        this.lessonsByRecord.clear();

        super.close();

        return;
    }
}
