//
// AbstractVotingRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractVotingRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.voting.rules.VotingRulesManager,
               org.osid.voting.rules.VotingRulesProxyManager {

    private final Types raceConstrainerRecordTypes         = new TypeRefSet();
    private final Types raceConstrainerSearchRecordTypes   = new TypeRefSet();

    private final Types raceConstrainerEnablerRecordTypes  = new TypeRefSet();
    private final Types raceConstrainerEnablerSearchRecordTypes= new TypeRefSet();

    private final Types raceProcessorRecordTypes           = new TypeRefSet();
    private final Types raceProcessorSearchRecordTypes     = new TypeRefSet();

    private final Types raceProcessorEnablerRecordTypes    = new TypeRefSet();
    private final Types raceProcessorEnablerSearchRecordTypes= new TypeRefSet();

    private final Types ballotConstrainerRecordTypes       = new TypeRefSet();
    private final Types ballotConstrainerSearchRecordTypes = new TypeRefSet();

    private final Types ballotConstrainerEnablerRecordTypes= new TypeRefSet();
    private final Types ballotConstrainerEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractVotingRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractVotingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up race constrainer is supported. 
     *
     *  @return <code> true </code> if race constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerLookup() {
        return (false);
    }


    /**
     *  Tests if querying race constrainer is supported. 
     *
     *  @return <code> true </code> if race constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerQuery() {
        return (false);
    }


    /**
     *  Tests if searching race constrainer is supported. 
     *
     *  @return <code> true </code> if race constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerSearch() {
        return (false);
    }


    /**
     *  Tests if a race constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if race constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerAdmin() {
        return (false);
    }


    /**
     *  Tests if a race constrainer notification service is supported. 
     *
     *  @return <code> true </code> if race constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerNotification() {
        return (false);
    }


    /**
     *  Tests if a race constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer polls lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerPolls() {
        return (false);
    }


    /**
     *  Tests if a race constrainer polls service is supported. 
     *
     *  @return <code> true </code> if race constrainer polls assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerPollsAssignment() {
        return (false);
    }


    /**
     *  Tests if a race constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer polls service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerSmartPolls() {
        return (false);
    }


    /**
     *  Tests if a race constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a race constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a race constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up race constrainer enablers is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying race constrainer enablers is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching race constrainer enablers is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a race constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if race constrainer enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a race constrainer enabler notification service is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a race constrainer enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer enabler polls lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerPolls() {
        return (false);
    }


    /**
     *  Tests if a race constrainer enabler polls service is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler polls 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerPollsAssignment() {
        return (false);
    }


    /**
     *  Tests if a race constrainer enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer enabler polls 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerSmartPolls() {
        return (false);
    }


    /**
     *  Tests if a race constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a race constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if race constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up race processor is supported. 
     *
     *  @return <code> true </code> if race processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorLookup() {
        return (false);
    }


    /**
     *  Tests if querying race processor is supported. 
     *
     *  @return <code> true </code> if race processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorQuery() {
        return (false);
    }


    /**
     *  Tests if searching race processor is supported. 
     *
     *  @return <code> true </code> if race processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorSearch() {
        return (false);
    }


    /**
     *  Tests if a race processor administrative service is supported. 
     *
     *  @return <code> true </code> if race processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorAdmin() {
        return (false);
    }


    /**
     *  Tests if a race processor notification service is supported. 
     *
     *  @return <code> true </code> if race processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorNotification() {
        return (false);
    }


    /**
     *  Tests if a race processor polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor polls lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorPolls() {
        return (false);
    }


    /**
     *  Tests if a race processor polls service is supported. 
     *
     *  @return <code> true </code> if race processor polls assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorPollsAssignment() {
        return (false);
    }


    /**
     *  Tests if a race processor polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor polls service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorSmartPolls() {
        return (false);
    }


    /**
     *  Tests if a race processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a race processor rule application service is supported. 
     *
     *  @return <code> true </code> if race processor rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up race processor enablers is supported. 
     *
     *  @return <code> true </code> if race processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying race processor enablers is supported. 
     *
     *  @return <code> true </code> if race processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching race processor enablers is supported. 
     *
     *  @return <code> true </code> if race processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a race processor enabler administrative service is supported. 
     *
     *  @return <code> true </code> if race processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a race processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if race processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a race processor enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor enabler polls lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerPolls() {
        return (false);
    }


    /**
     *  Tests if a race processor enabler polls service is supported. 
     *
     *  @return <code> true </code> if race processor enabler polls assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerPollsAssignment() {
        return (false);
    }


    /**
     *  Tests if a race processor enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor enabler polls service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerSmartPolls() {
        return (false);
    }


    /**
     *  Tests if a race processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a race processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if race processor enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up ballot constrainer is supported. 
     *
     *  @return <code> true </code> if ballot constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerLookup() {
        return (false);
    }


    /**
     *  Tests if querying ballot constrainer is supported. 
     *
     *  @return <code> true </code> if ballot constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerQuery() {
        return (false);
    }


    /**
     *  Tests if searching ballot constrainer is supported. 
     *
     *  @return <code> true </code> if ballot constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerSearch() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerAdmin() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer notification service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerNotification() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer polls lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerPolls() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer polls service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer polls assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerPollsAssignment() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer polls service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerSmartPolls() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up ballot constrainer enablers is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying ballot constrainer enablers is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching ballot constrainer enablers is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer enabler polls lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a ballot constrainer enabler polls 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerPolls() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer enabler polls service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler polls 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerPollsAssignment() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer enabler polls lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a ballot constrainer enabler polls 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerSmartPolls() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a ballot constrainer enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a ballot constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> RaceConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> RaceConstrainer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceConstrainerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  raceConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> RaceConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerRecordType(org.osid.type.Type raceConstrainerRecordType) {
        return (this.raceConstrainerRecordTypes.contains(raceConstrainerRecordType));
    }


    /**
     *  Adds support for a race constrainer record type.
     *
     *  @param raceConstrainerRecordType a race constrainer record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerRecordType</code> is <code>null</code>
     */

    protected void addRaceConstrainerRecordType(org.osid.type.Type raceConstrainerRecordType) {
        this.raceConstrainerRecordTypes.add(raceConstrainerRecordType);
        return;
    }


    /**
     *  Removes support for a race constrainer record type.
     *
     *  @param raceConstrainerRecordType a race constrainer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerRecordType</code> is <code>null</code>
     */

    protected void removeRaceConstrainerRecordType(org.osid.type.Type raceConstrainerRecordType) {
        this.raceConstrainerRecordTypes.remove(raceConstrainerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RaceConstrainer </code> search record types. 
     *
     *  @return a list containing the supported <code> RaceConstrainer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceConstrainerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  raceConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RaceConstrainer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerSearchRecordType(org.osid.type.Type raceConstrainerSearchRecordType) {
        return (this.raceConstrainerSearchRecordTypes.contains(raceConstrainerSearchRecordType));
    }


    /**
     *  Adds support for a race constrainer search record type.
     *
     *  @param raceConstrainerSearchRecordType a race constrainer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void addRaceConstrainerSearchRecordType(org.osid.type.Type raceConstrainerSearchRecordType) {
        this.raceConstrainerSearchRecordTypes.add(raceConstrainerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a race constrainer search record type.
     *
     *  @param raceConstrainerSearchRecordType a race constrainer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void removeRaceConstrainerSearchRecordType(org.osid.type.Type raceConstrainerSearchRecordType) {
        this.raceConstrainerSearchRecordTypes.remove(raceConstrainerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RaceConstrainerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> RaceConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceConstrainerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  raceConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> RaceConstrainerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerRecordType(org.osid.type.Type raceConstrainerEnablerRecordType) {
        return (this.raceConstrainerEnablerRecordTypes.contains(raceConstrainerEnablerRecordType));
    }


    /**
     *  Adds support for a race constrainer enabler record type.
     *
     *  @param raceConstrainerEnablerRecordType a race constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void addRaceConstrainerEnablerRecordType(org.osid.type.Type raceConstrainerEnablerRecordType) {
        this.raceConstrainerEnablerRecordTypes.add(raceConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a race constrainer enabler record type.
     *
     *  @param raceConstrainerEnablerRecordType a race constrainer enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeRaceConstrainerEnablerRecordType(org.osid.type.Type raceConstrainerEnablerRecordType) {
        this.raceConstrainerEnablerRecordTypes.remove(raceConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RaceConstrainerEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> RaceConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceConstrainerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceConstrainerEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  raceConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RaceConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerSearchRecordType(org.osid.type.Type raceConstrainerEnablerSearchRecordType) {
        return (this.raceConstrainerEnablerSearchRecordTypes.contains(raceConstrainerEnablerSearchRecordType));
    }


    /**
     *  Adds support for a race constrainer enabler search record type.
     *
     *  @param raceConstrainerEnablerSearchRecordType a race constrainer enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addRaceConstrainerEnablerSearchRecordType(org.osid.type.Type raceConstrainerEnablerSearchRecordType) {
        this.raceConstrainerEnablerSearchRecordTypes.add(raceConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a race constrainer enabler search record type.
     *
     *  @param raceConstrainerEnablerSearchRecordType a race constrainer enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeRaceConstrainerEnablerSearchRecordType(org.osid.type.Type raceConstrainerEnablerSearchRecordType) {
        this.raceConstrainerEnablerSearchRecordTypes.remove(raceConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RaceProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> RaceProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceProcessorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceProcessor </code> record type is 
     *  supported. 
     *
     *  @param  raceProcessorRecordType a <code> Type </code> indicating a 
     *          <code> RaceProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> raceProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorRecordType(org.osid.type.Type raceProcessorRecordType) {
        return (this.raceProcessorRecordTypes.contains(raceProcessorRecordType));
    }


    /**
     *  Adds support for a race processor record type.
     *
     *  @param raceProcessorRecordType a race processor record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorRecordType</code> is <code>null</code>
     */

    protected void addRaceProcessorRecordType(org.osid.type.Type raceProcessorRecordType) {
        this.raceProcessorRecordTypes.add(raceProcessorRecordType);
        return;
    }


    /**
     *  Removes support for a race processor record type.
     *
     *  @param raceProcessorRecordType a race processor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorRecordType</code> is <code>null</code>
     */

    protected void removeRaceProcessorRecordType(org.osid.type.Type raceProcessorRecordType) {
        this.raceProcessorRecordTypes.remove(raceProcessorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RaceProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> RaceProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceProcessorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  raceProcessorSearchRecordType a <code> Type </code> indicating 
     *          a <code> RaceProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorSearchRecordType(org.osid.type.Type raceProcessorSearchRecordType) {
        return (this.raceProcessorSearchRecordTypes.contains(raceProcessorSearchRecordType));
    }


    /**
     *  Adds support for a race processor search record type.
     *
     *  @param raceProcessorSearchRecordType a race processor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void addRaceProcessorSearchRecordType(org.osid.type.Type raceProcessorSearchRecordType) {
        this.raceProcessorSearchRecordTypes.add(raceProcessorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a race processor search record type.
     *
     *  @param raceProcessorSearchRecordType a race processor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void removeRaceProcessorSearchRecordType(org.osid.type.Type raceProcessorSearchRecordType) {
        this.raceProcessorSearchRecordTypes.remove(raceProcessorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RaceProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> RaceProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceProcessorEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  raceProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> RaceProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerRecordType(org.osid.type.Type raceProcessorEnablerRecordType) {
        return (this.raceProcessorEnablerRecordTypes.contains(raceProcessorEnablerRecordType));
    }


    /**
     *  Adds support for a race processor enabler record type.
     *
     *  @param raceProcessorEnablerRecordType a race processor enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void addRaceProcessorEnablerRecordType(org.osid.type.Type raceProcessorEnablerRecordType) {
        this.raceProcessorEnablerRecordTypes.add(raceProcessorEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a race processor enabler record type.
     *
     *  @param raceProcessorEnablerRecordType a race processor enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void removeRaceProcessorEnablerRecordType(org.osid.type.Type raceProcessorEnablerRecordType) {
        this.raceProcessorEnablerRecordTypes.remove(raceProcessorEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RaceProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> RaceProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.raceProcessorEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RaceProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  raceProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RaceProcessorEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerSearchRecordType(org.osid.type.Type raceProcessorEnablerSearchRecordType) {
        return (this.raceProcessorEnablerSearchRecordTypes.contains(raceProcessorEnablerSearchRecordType));
    }


    /**
     *  Adds support for a race processor enabler search record type.
     *
     *  @param raceProcessorEnablerSearchRecordType a race processor enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addRaceProcessorEnablerSearchRecordType(org.osid.type.Type raceProcessorEnablerSearchRecordType) {
        this.raceProcessorEnablerSearchRecordTypes.add(raceProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a race processor enabler search record type.
     *
     *  @param raceProcessorEnablerSearchRecordType a race processor enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>raceProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeRaceProcessorEnablerSearchRecordType(org.osid.type.Type raceProcessorEnablerSearchRecordType) {
        this.raceProcessorEnablerSearchRecordTypes.remove(raceProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> BallotConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> BallotConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ballotConstrainerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> BallotConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  ballotConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> BallotConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerRecordType(org.osid.type.Type ballotConstrainerRecordType) {
        return (this.ballotConstrainerRecordTypes.contains(ballotConstrainerRecordType));
    }


    /**
     *  Adds support for a ballot constrainer record type.
     *
     *  @param ballotConstrainerRecordType a ballot constrainer record type
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerRecordType</code> is <code>null</code>
     */

    protected void addBallotConstrainerRecordType(org.osid.type.Type ballotConstrainerRecordType) {
        this.ballotConstrainerRecordTypes.add(ballotConstrainerRecordType);
        return;
    }


    /**
     *  Removes support for a ballot constrainer record type.
     *
     *  @param ballotConstrainerRecordType a ballot constrainer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerRecordType</code> is <code>null</code>
     */

    protected void removeBallotConstrainerRecordType(org.osid.type.Type ballotConstrainerRecordType) {
        this.ballotConstrainerRecordTypes.remove(ballotConstrainerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> BallotConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> BallotConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ballotConstrainerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> BallotConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  ballotConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> BallotConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerSearchRecordType(org.osid.type.Type ballotConstrainerSearchRecordType) {
        return (this.ballotConstrainerSearchRecordTypes.contains(ballotConstrainerSearchRecordType));
    }


    /**
     *  Adds support for a ballot constrainer search record type.
     *
     *  @param ballotConstrainerSearchRecordType a ballot constrainer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void addBallotConstrainerSearchRecordType(org.osid.type.Type ballotConstrainerSearchRecordType) {
        this.ballotConstrainerSearchRecordTypes.add(ballotConstrainerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a ballot constrainer search record type.
     *
     *  @param ballotConstrainerSearchRecordType a ballot constrainer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void removeBallotConstrainerSearchRecordType(org.osid.type.Type ballotConstrainerSearchRecordType) {
        this.ballotConstrainerSearchRecordTypes.remove(ballotConstrainerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> BallotConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          BallotConstrainerEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ballotConstrainerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> BallotConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  ballotConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> BallotConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType) {
        return (this.ballotConstrainerEnablerRecordTypes.contains(ballotConstrainerEnablerRecordType));
    }


    /**
     *  Adds support for a ballot constrainer enabler record type.
     *
     *  @param ballotConstrainerEnablerRecordType a ballot constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void addBallotConstrainerEnablerRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType) {
        this.ballotConstrainerEnablerRecordTypes.add(ballotConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a ballot constrainer enabler record type.
     *
     *  @param ballotConstrainerEnablerRecordType a ballot constrainer enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeBallotConstrainerEnablerRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType) {
        this.ballotConstrainerEnablerRecordTypes.remove(ballotConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> BallotConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          BallotConstrainerEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ballotConstrainerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> BallotConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  ballotConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> BallotConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerSearchRecordType(org.osid.type.Type ballotConstrainerEnablerSearchRecordType) {
        return (this.ballotConstrainerEnablerSearchRecordTypes.contains(ballotConstrainerEnablerSearchRecordType));
    }


    /**
     *  Adds support for a ballot constrainer enabler search record type.
     *
     *  @param ballotConstrainerEnablerSearchRecordType a ballot constrainer enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addBallotConstrainerEnablerSearchRecordType(org.osid.type.Type ballotConstrainerEnablerSearchRecordType) {
        this.ballotConstrainerEnablerSearchRecordTypes.add(ballotConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a ballot constrainer enabler search record type.
     *
     *  @param ballotConstrainerEnablerSearchRecordType a ballot constrainer enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ballotConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeBallotConstrainerEnablerSearchRecordType(org.osid.type.Type ballotConstrainerEnablerSearchRecordType) {
        this.ballotConstrainerEnablerSearchRecordTypes.remove(ballotConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer lookup service. 
     *
     *  @return a <code> RaceConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerLookupSession getRaceConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerLookupSession getRaceConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerLookupSession getRaceConstrainerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerLookupSession getRaceConstrainerLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer query service. 
     *
     *  @return a <code> RaceConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuerySession getRaceConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuerySession getRaceConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuerySession getRaceConstrainerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuerySession getRaceConstrainerQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer search service. 
     *
     *  @return a <code> RaceConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSearchSession getRaceConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSearchSession getRaceConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSearchSession getRaceConstrainerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSearchSession getRaceConstrainerSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer administration service. 
     *
     *  @return a <code> RaceConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerAdminSession getRaceConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerAdminSession getRaceConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerAdminSession getRaceConstrainerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerAdminSession getRaceConstrainerAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer notification service. 
     *
     *  @param  raceConstrainerReceiver the notification callback 
     *  @return a <code> RaceConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerNotificationSession getRaceConstrainerNotificationSession(org.osid.voting.rules.RaceConstrainerReceiver raceConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer notification service. 
     *
     *  @param  raceConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerNotificationSession getRaceConstrainerNotificationSession(org.osid.voting.rules.RaceConstrainerReceiver raceConstrainerReceiver, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer notification service for the given polls. 
     *
     *  @param  raceConstrainerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerReceiver 
     *          </code> or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerNotificationSession getRaceConstrainerNotificationSessionForPolls(org.osid.voting.rules.RaceConstrainerReceiver raceConstrainerReceiver, 
                                                                                                                  org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer notification service for the given polls. 
     *
     *  @param  raceConstrainerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerReceiver, 
     *          pollsId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerNotificationSession getRaceConstrainerNotificationSessionForPolls(org.osid.voting.rules.RaceConstrainerReceiver raceConstrainerReceiver, 
                                                                                                                  org.osid.id.Id pollsId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race constrainer/polls 
     *  mappings for race constrainers. 
     *
     *  @return a <code> RaceConstrainerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerPollsSession getRaceConstrainerPollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race constrainer/polls 
     *  mappings for race constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerPollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerPollsSession getRaceConstrainerPollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  constrainer to polls. 
     *
     *  @return a <code> RaceConstrainerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerPollsAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerPollsAssignmentSession getRaceConstrainerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  constrainer to polls. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerPollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerPollsAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerPollsAssignmentSession getRaceConstrainerPollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race constrainer smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSmartPollsSession getRaceConstrainerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race constrainer smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSmartPollsSession getRaceConstrainerSmartPollsSession(org.osid.id.Id pollsId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a race. 
     *
     *  @return a <code> RaceConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleLookupSession getRaceConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  ta race. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleLookupSession getRaceConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer mapping lookup service for the given polls for looking up 
     *  rules applied to a race. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleLookupSession getRaceConstrainerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer mapping lookup service for the given polls for looking up 
     *  rules applied to a qeue. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleLookupSession getRaceConstrainerRuleLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer assignment service to apply to races. 
     *
     *  @return a <code> RaceConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleApplicationSession getRaceConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer assignment service to apply to races. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleApplicationSession getRaceConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer assignment service for the given polls to apply to races. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleApplicationSession getRaceConstrainerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer assignment service for the given polls to apply to races. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleApplicationSession getRaceConstrainerRuleApplicationSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> RaceConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerLookupSession getRaceConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerLookupSession getRaceConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerLookupSession getRaceConstrainerEnablerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerLookupSession getRaceConstrainerEnablerLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler query service. 
     *
     *  @return a <code> RaceConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerQuerySession getRaceConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerQuerySession getRaceConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerQuerySession getRaceConstrainerEnablerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerQuerySession getRaceConstrainerEnablerQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler search service. 
     *
     *  @return a <code> RaceConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSearchSession getRaceConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSearchSession getRaceConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSearchSession getRaceConstrainerEnablerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSearchSession getRaceConstrainerEnablerSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> RaceConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerAdminSession getRaceConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerAdminSession getRaceConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerAdminSession getRaceConstrainerEnablerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerAdminSession getRaceConstrainerEnablerAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler notification service. 
     *
     *  @param  raceConstrainerEnablerReceiver the notification callback 
     *  @return a <code> RaceConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerNotificationSession getRaceConstrainerEnablerNotificationSession(org.osid.voting.rules.RaceConstrainerEnablerReceiver raceConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler notification service. 
     *
     *  @param  raceConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerNotificationSession getRaceConstrainerEnablerNotificationSession(org.osid.voting.rules.RaceConstrainerEnablerReceiver raceConstrainerEnablerReceiver, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler notification service for the given polls. 
     *
     *  @param  raceConstrainerEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerReceiver </code> or <code> pollsId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerNotificationSession getRaceConstrainerEnablerNotificationSessionForPolls(org.osid.voting.rules.RaceConstrainerEnablerReceiver raceConstrainerEnablerReceiver, 
                                                                                                                                org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler notification service for the given polls. 
     *
     *  @param  raceConstrainerEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerReceiver, pollsId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerNotificationSession getRaceConstrainerEnablerNotificationSessionForPolls(org.osid.voting.rules.RaceConstrainerEnablerReceiver raceConstrainerEnablerReceiver, 
                                                                                                                                org.osid.id.Id pollsId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race constrainer 
     *  enabler/polls mappings for race constrainer enablers. 
     *
     *  @return a <code> RaceConstrainerEnablerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerPollsSession getRaceConstrainerEnablerPollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race constrainer 
     *  enabler/polls mappings for race constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerPollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerPollsSession getRaceConstrainerEnablerPollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  constrainer enablers to polls. 
     *
     *  @return a <code> RaceConstrainerEnablerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerPollsAssignmentSession getRaceConstrainerEnablerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  constrainer enablers to polls. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerPollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerPollsAssignmentSession getRaceConstrainerEnablerPollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race constrainer enabler 
     *  smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSmartPollsSession getRaceConstrainerEnablerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race constrainer enabler 
     *  smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSmartPollsSession getRaceConstrainerEnablerSmartPollsSession(org.osid.id.Id pollsId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> RaceConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleLookupSession getRaceConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleLookupSession getRaceConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler mapping lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleLookupSession getRaceConstrainerEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler mapping lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleLookupSession getRaceConstrainerEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> RaceConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleApplicationSession getRaceConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleApplicationSession getRaceConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleApplicationSession getRaceConstrainerEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceConstrainerEnablerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleApplicationSession getRaceConstrainerEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceConstrainerEnablerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  lookup service. 
     *
     *  @return a <code> RaceProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorLookupSession getRaceProcessorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorLookupSession getRaceProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorLookupSession getRaceProcessorLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorLookupSession getRaceProcessorLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  query service. 
     *
     *  @return a <code> RaceProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuerySession getRaceProcessorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuerySession getRaceProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuerySession getRaceProcessorQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuerySession getRaceProcessorQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  search service. 
     *
     *  @return a <code> RaceProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSearchSession getRaceProcessorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSearchSession getRaceProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSearchSession getRaceProcessorSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSearchSession getRaceProcessorSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  administration service. 
     *
     *  @return a <code> RaceProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorAdminSession getRaceProcessorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorAdminSession getRaceProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorAdminSession getRaceProcessorAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorAdminSession getRaceProcessorAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  notification service. 
     *
     *  @param  raceProcessorReceiver the notification callback 
     *  @return a <code> RaceProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorNotificationSession getRaceProcessorNotificationSession(org.osid.voting.rules.RaceProcessorReceiver raceProcessorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  notification service. 
     *
     *  @param  raceProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceProcessorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorNotificationSession getRaceProcessorNotificationSession(org.osid.voting.rules.RaceProcessorReceiver raceProcessorReceiver, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  notification service for the given polls. 
     *
     *  @param  raceProcessorReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceProcessorReceiver 
     *          </code> or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorNotificationSession getRaceProcessorNotificationSessionForPolls(org.osid.voting.rules.RaceProcessorReceiver raceProcessorReceiver, 
                                                                                                              org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  notification service for the given polls. 
     *
     *  @param  raceProcessorReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceProcessorReceiver, 
     *          pollsId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorNotificationSession getRaceProcessorNotificationSessionForPolls(org.osid.voting.rules.RaceProcessorReceiver raceProcessorReceiver, 
                                                                                                              org.osid.id.Id pollsId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race processor/polls 
     *  mappings for race processors. 
     *
     *  @return a <code> RaceProcessorPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorPollsSession getRaceProcessorPollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race processor/polls 
     *  mappings for race processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorPollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorPollsSession getRaceProcessorPollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  processor to polls. 
     *
     *  @return a <code> RaceProcessorPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorPollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorPollsAssignmentSession getRaceProcessorPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  processor to polls. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorPollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorPollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorPollsAssignmentSession getRaceProcessorPollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race processor smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSmartPollsSession getRaceProcessorSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race processor smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSmartPollsSession getRaceProcessorSmartPollsSession(org.osid.id.Id pollsId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  mapping lookup service for looking up the rules applied to a race. 
     *
     *  @return a <code> RaceProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleLookupSession getRaceProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  mapping lookup service for looking up the rules applied to a race 
     *  processor. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleLookupSession getRaceProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  mapping lookup service for the given polls for looking up rules 
     *  applied to a race. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleLookupSession getRaceProcessorRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  mapping lookup service for the given polls for looking up rules 
     *  applied to a race. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleLookupSession getRaceProcessorRuleLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  assignment service. 
     *
     *  @return a <code> RaceProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleApplicationSession getRaceProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  assignment service to apply to races. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleApplicationSession getRaceProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleApplicationSession getRaceProcessorRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  assignment service for the given polls to apply to races. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleApplicationSession getRaceProcessorRuleApplicationSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler lookup service. 
     *
     *  @return a <code> RaceProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerLookupSession getRaceProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerLookupSession getRaceProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerLookupSession getRaceProcessorEnablerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerLookupSession getRaceProcessorEnablerLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler query service. 
     *
     *  @return a <code> RaceProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerQuerySession getRaceProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerQuerySession getRaceProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerQuerySession getRaceProcessorEnablerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerQuerySession getRaceProcessorEnablerQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler search service. 
     *
     *  @return a <code> RaceProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSearchSession getRaceProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSearchSession getRaceProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSearchSession getRaceProcessorEnablerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSearchSession getRaceProcessorEnablerSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler administration service. 
     *
     *  @return a <code> RaceProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerAdminSession getRaceProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerAdminSession getRaceProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerAdminSession getRaceProcessorEnablerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerAdminSession getRaceProcessorEnablerAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler notification service. 
     *
     *  @param  raceProcessorEnablerReceiver the notification callback 
     *  @return a <code> RaceProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerNotificationSession getRaceProcessorEnablerNotificationSession(org.osid.voting.rules.RaceProcessorEnablerReceiver raceProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler notification service. 
     *
     *  @param  raceProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerNotificationSession getRaceProcessorEnablerNotificationSession(org.osid.voting.rules.RaceProcessorEnablerReceiver raceProcessorEnablerReceiver, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler notification service for the given polls. 
     *
     *  @param  raceProcessorEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerReceiver </code> or <code> pollsId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerNotificationSession getRaceProcessorEnablerNotificationSessionForPolls(org.osid.voting.rules.RaceProcessorEnablerReceiver raceProcessorEnablerReceiver, 
                                                                                                                            org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler notification service for the given polls. 
     *
     *  @param  raceProcessorEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerReceiver, pollsId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerNotificationSession getRaceProcessorEnablerNotificationSessionForPolls(org.osid.voting.rules.RaceProcessorEnablerReceiver raceProcessorEnablerReceiver, 
                                                                                                                            org.osid.id.Id pollsId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race processor 
     *  enabler/polls mappings for race processor enablers. 
     *
     *  @return a <code> RaceProcessorEnablerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerPollsSession getRaceProcessorEnablerPollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race processor 
     *  enabler/polls mappings for race processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerPollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerPollsSession getRaceProcessorEnablerPollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  processor enablers to polls. 
     *
     *  @return a <code> RaceProcessorEnablerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerPollsAssignmentSession getRaceProcessorEnablerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  processor enablers to race processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerPollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerPollsAssignmentSession getRaceProcessorEnablerPollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race processor enabler 
     *  smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSmartPollsSession getRaceProcessorEnablerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race processor enabler 
     *  smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSmartPollsSession getRaceProcessorEnablerSmartPollsSession(org.osid.id.Id pollsId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> RaceProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleLookupSession getRaceProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleLookupSession getRaceProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler mapping lookup service. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleLookupSession getRaceProcessorEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler mapping lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleLookupSession getRaceProcessorEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler assignment service. 
     *
     *  @return a <code> RaceProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleApplicationSession getRaceProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleApplicationSession getRaceProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleApplicationSession getRaceProcessorEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getRaceProcessorEnablerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleApplicationSession getRaceProcessorEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getRaceProcessorEnablerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer lookup service. 
     *
     *  @return a <code> BallotConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerLookupSession getBallotConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerLookupSession getBallotConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerLookupSession getBallotConstrainerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerLookupSession getBallotConstrainerLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer query service. 
     *
     *  @return a <code> BallotConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuerySession getBallotConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuerySession getBallotConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuerySession getBallotConstrainerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuerySession getBallotConstrainerQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer search service. 
     *
     *  @return a <code> BallotConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSearchSession getBallotConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException a <code> 
     *          BallotConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSearchSession getBallotConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSearchSession getBallotConstrainerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSearchSession getBallotConstrainerSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer administration service. 
     *
     *  @return a <code> BallotConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerAdminSession getBallotConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerAdminSession getBallotConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerAdminSession getBallotConstrainerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerAdminSession getBallotConstrainerAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer notification service. 
     *
     *  @param  ballotConstrainerReceiver the notification callback 
     *  @return a <code> BallotConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerNotificationSession getBallotConstrainerNotificationSession(org.osid.voting.rules.BallotConstrainerReceiver ballotConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer notification service. 
     *
     *  @param  ballotConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerNotificationSession getBallotConstrainerNotificationSession(org.osid.voting.rules.BallotConstrainerReceiver ballotConstrainerReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer notification service for the given polls. 
     *
     *  @param  ballotConstrainerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerReceiver </code> or <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerNotificationSession getBallotConstrainerNotificationSessionForPolls(org.osid.voting.rules.BallotConstrainerReceiver ballotConstrainerReceiver, 
                                                                                                                      org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer notification service for the given polls. 
     *
     *  @param  ballotConstrainerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerReceiver, pollsId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerNotificationSession getBallotConstrainerNotificationSessionForPolls(org.osid.voting.rules.BallotConstrainerReceiver ballotConstrainerReceiver, 
                                                                                                                      org.osid.id.Id pollsId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup ballot constrainer/polls 
     *  mappings for ballot constrainers. 
     *
     *  @return a <code> BallotConstrainerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerPollsSession getBallotConstrainerPollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup ballot constrainer/polls 
     *  mappings for ballot constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerPollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerPollsSession getBallotConstrainerPollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning ballot 
     *  constrainer to polls. 
     *
     *  @return a <code> BallotConstrainerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerPollsAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerPollsAssignmentSession getBallotConstrainerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning ballot 
     *  constrainer to polls. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerPollsAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerPollsAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerPollsAssignmentSession getBallotConstrainerPollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage ballot constrainer smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSmartPollsSession getBallotConstrainerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage ballot constrainer smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSmartPollsSession getBallotConstrainerSmartPollsSession(org.osid.id.Id pollsId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  the ballot. 
     *
     *  @return a <code> BallotConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleLookupSession getBallotConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a ballot. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleLookupSession getBallotConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer mapping lookup service for the given polls for looking up 
     *  rules applied to a ballot. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleLookupSession getBallotConstrainerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer mapping lookup service for the given polls for looking up 
     *  rules applied to a ballot. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleLookupSession getBallotConstrainerRuleLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer assignment service to apply to ballots. 
     *
     *  @return a <code> BallotConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleApplicationSession getBallotConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer assignment service to apply to ballots. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleApplicationSession getBallotConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer assignment service for the given polls to apply to 
     *  ballots. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleApplicationSession getBallotConstrainerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer assignment service for the given polls to apply to 
     *  ballots. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleApplicationSession getBallotConstrainerRuleApplicationSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> BallotConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerLookupSession getBallotConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerLookupSession getBallotConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerLookupSession getBallotConstrainerEnablerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerLookupSession getBallotConstrainerEnablerLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler query service. 
     *
     *  @return a <code> BallotConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerQuerySession getBallotConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerQuerySession getBallotConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerQuerySession getBallotConstrainerEnablerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerQuerySession getBallotConstrainerEnablerQuerySessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerQuerySessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler search service. 
     *
     *  @return a <code> BallotConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSearchSession getBallotConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSearchSession getBallotConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSearchSession getBallotConstrainerEnablerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSearchSession getBallotConstrainerEnablerSearchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerSearchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> BallotConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerAdminSession getBallotConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerAdminSession getBallotConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerAdminSession getBallotConstrainerEnablerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerAdminSession getBallotConstrainerEnablerAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler notification service. 
     *
     *  @param  ballotConstrainerEnablerReceiver the notification callback 
     *  @return a <code> BallotConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerReceiver </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerNotificationSession getBallotConstrainerEnablerNotificationSession(org.osid.voting.rules.BallotConstrainerEnablerReceiver ballotConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler notification service. 
     *
     *  @param  ballotConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerNotificationSession getBallotConstrainerEnablerNotificationSession(org.osid.voting.rules.BallotConstrainerEnablerReceiver ballotConstrainerEnablerReceiver, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler notification service for the given polls. 
     *
     *  @param  ballotConstrainerEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerReceiver </code> or <code> pollsId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerNotificationSession getBallotConstrainerEnablerNotificationSessionForPolls(org.osid.voting.rules.BallotConstrainerEnablerReceiver ballotConstrainerEnablerReceiver, 
                                                                                                                                    org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler notification service for the given polls. 
     *
     *  @param  ballotConstrainerEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerReceiver, pollsId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerNotificationSession getBallotConstrainerEnablerNotificationSessionForPolls(org.osid.voting.rules.BallotConstrainerEnablerReceiver ballotConstrainerEnablerReceiver, 
                                                                                                                                    org.osid.id.Id pollsId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerNotificationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup ballot constrainer 
     *  enabler/polls mappings for ballot constrainer enablers. 
     *
     *  @return a <code> BallotConstrainerEnablerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerPollsSession getBallotConstrainerEnablerPollsSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup ballot constrainer 
     *  enabler/polls mappings for ballot constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerPollsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerPollsSession getBallotConstrainerEnablerPollsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning ballot 
     *  constrainer enablers to polls. 
     *
     *  @return a <code> BallotConstrainerEnablerPollsAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerPollsAssignmentSession getBallotConstrainerEnablerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning ballot 
     *  constrainer enablers to polls. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerPollsAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerPollsAssignmentSession getBallotConstrainerEnablerPollsAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerPollsAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage ballot constrainer 
     *  enabler smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSmartPollsSession getBallotConstrainerEnablerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage ballot constrainer 
     *  enabler smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSmartPollsSession getBallotConstrainerEnablerSmartPollsSession(org.osid.id.Id pollsId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerSmartPollsSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> BallotConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleLookupSession getBallotConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleLookupSession getBallotConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler mapping lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleLookupSession getBallotConstrainerEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler mapping lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleLookupSession getBallotConstrainerEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerRuleLookupSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> BallotConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleApplicationSession getBallotConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleApplicationSession getBallotConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleApplicationSession getBallotConstrainerEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesManager.getBallotConstrainerEnablerRuleApplicationSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleApplicationSession getBallotConstrainerEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.rules.VotingRulesProxyManager.getBallotConstrainerEnablerRuleApplicationSessionForPolls not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.raceConstrainerRecordTypes.clear();
        this.raceConstrainerRecordTypes.clear();

        this.raceConstrainerSearchRecordTypes.clear();
        this.raceConstrainerSearchRecordTypes.clear();

        this.raceConstrainerEnablerRecordTypes.clear();
        this.raceConstrainerEnablerRecordTypes.clear();

        this.raceConstrainerEnablerSearchRecordTypes.clear();
        this.raceConstrainerEnablerSearchRecordTypes.clear();

        this.raceProcessorRecordTypes.clear();
        this.raceProcessorRecordTypes.clear();

        this.raceProcessorSearchRecordTypes.clear();
        this.raceProcessorSearchRecordTypes.clear();

        this.raceProcessorEnablerRecordTypes.clear();
        this.raceProcessorEnablerRecordTypes.clear();

        this.raceProcessorEnablerSearchRecordTypes.clear();
        this.raceProcessorEnablerSearchRecordTypes.clear();

        this.ballotConstrainerRecordTypes.clear();
        this.ballotConstrainerRecordTypes.clear();

        this.ballotConstrainerSearchRecordTypes.clear();
        this.ballotConstrainerSearchRecordTypes.clear();

        this.ballotConstrainerEnablerRecordTypes.clear();
        this.ballotConstrainerEnablerRecordTypes.clear();

        this.ballotConstrainerEnablerSearchRecordTypes.clear();
        this.ballotConstrainerEnablerSearchRecordTypes.clear();

        return;
    }
}
