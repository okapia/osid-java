//
// CompositionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.composition.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CompositionElements
    extends net.okapia.osid.jamocha.spi.SourceableOsidObjectElements {


    /**
     *  Gets the active element Id.
     *
     *  @return the active element Id
     */

    public static org.osid.id.Id getActive() {
        return (net.okapia.osid.jamocha.spi.OperableElements.getActive());
    }


    /**
     *  Gets the enabled element Id.
     *
     *  @return the enabled element Id
     */

    public static org.osid.id.Id getEnabled() {
        return (net.okapia.osid.jamocha.spi.OperableElements.getEnabled());
    }


    /**
     *  Gets the disabled element Id.
     *
     *  @return the disabled element Id
     */

    public static org.osid.id.Id getDisabled() {
        return (net.okapia.osid.jamocha.spi.OperableElements.getDisabled());
    }


    /**
     *  Gets the operational element Id.
     *
     *  @return the operational element Id
     */

    public static org.osid.id.Id getOperational() {
        return (net.okapia.osid.jamocha.spi.OperableElements.getOperational());
    }


    /**
     *  Gets the sequestered element Id.
     *
     *  @return the sequestered element Id
     */

    public static org.osid.id.Id getSequestered() {
        return (net.okapia.osid.jamocha.spi.ContainableElements.getSequestered());
    }


    /**
     *  Gets the CompositionElement Id.
     *
     *  @return the composition element Id
     */

    public static org.osid.id.Id getCompositionEntityId() {
        return (makeEntityId("osid.repository.Composition"));
    }


    /**
     *  Gets the ChildrenIds element Id.
     *
     *  @return the ChildrenIds element Id
     */

    public static org.osid.id.Id getChildrenIds() {
        return (makeElementId("osid.repository.composition.ChildrenIds"));
    }


    /**
     *  Gets the Children element Id.
     *
     *  @return the Children element Id
     */

    public static org.osid.id.Id getChildren() {
        return (makeElementId("osid.repository.composition.Children"));
    }


    /**
     *  Gets the AssetId element Id.
     *
     *  @return the AssetId element Id
     */

    public static org.osid.id.Id getAssetId() {
        return (makeQueryElementId("osid.repository.composition.AssetId"));
    }


    /**
     *  Gets the Asset element Id.
     *
     *  @return the Asset element Id
     */

    public static org.osid.id.Id getAsset() {
        return (makeQueryElementId("osid.repository.composition.Asset"));
    }


    /**
     *  Gets the ContainingCompositionId element Id.
     *
     *  @return the ContainingCompositionId element Id
     */

    public static org.osid.id.Id getContainingCompositionId() {
        return (makeQueryElementId("osid.repository.composition.ContainingCompositionId"));
    }


    /**
     *  Gets the ContainingComposition element Id.
     *
     *  @return the ContainingComposition element Id
     */

    public static org.osid.id.Id getContainingComposition() {
        return (makeQueryElementId("osid.repository.composition.ContainingComposition"));
    }


    /**
     *  Gets the ContainedCompositionId element Id.
     *
     *  @return the ContainedCompositionId element Id
     */

    public static org.osid.id.Id getContainedCompositionId() {
        return (makeQueryElementId("osid.repository.composition.ContainedCompositionId"));
    }


    /**
     *  Gets the ContainedComposition element Id.
     *
     *  @return the ContainedComposition element Id
     */

    public static org.osid.id.Id getContainedComposition() {
        return (makeQueryElementId("osid.repository.composition.ContainedComposition"));
    }


    /**
     *  Gets the RepositoryId element Id.
     *
     *  @return the RepositoryId element Id
     */

    public static org.osid.id.Id getRepositoryId() {
        return (makeQueryElementId("osid.repository.composition.RepositoryId"));
    }


    /**
     *  Gets the Repository element Id.
     *
     *  @return the Repository element Id
     */

    public static org.osid.id.Id getRepository() {
        return (makeQueryElementId("osid.repository.composition.Repository"));
    }
}
