//
// AbstractQueryAwardLookupSession.java
//
//    An inline adapter that maps an AwardLookupSession to
//    an AwardQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AwardLookupSession to
 *  an AwardQuerySession.
 */

public abstract class AbstractQueryAwardLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractAwardLookupSession
    implements org.osid.recognition.AwardLookupSession {

    private final org.osid.recognition.AwardQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAwardLookupSession.
     *
     *  @param querySession the underlying award query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAwardLookupSession(org.osid.recognition.AwardQuerySession querySession) {
        nullarg(querySession, "award query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Academy</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.session.getAcademyId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this 
     *  session.
     *
     *  @return the <code>Academy</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAcademy());
    }


    /**
     *  Tests if this user can perform <code>Award</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAwards() {
        return (this.session.canSearchAwards());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include awards in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.session.useFederatedAcademyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.session.useIsolatedAcademyView();
        return;
    }
    
     
    /**
     *  Gets the <code>Award</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Award</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Award</code> and
     *  retained for compatibility.
     *
     *  @param  awardId <code>Id</code> of the
     *          <code>Award</code>
     *  @return the award
     *  @throws org.osid.NotFoundException <code>awardId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward(org.osid.id.Id awardId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AwardQuery query = getQuery();
        query.matchId(awardId, true);
        org.osid.recognition.AwardList awards = this.session.getAwardsByQuery(query);
        if (awards.hasNext()) {
            return (awards.getNextAward());
        } 
        
        throw new org.osid.NotFoundException(awardId + " not found");
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  awards specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Awards</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  awardIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>awardIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByIds(org.osid.id.IdList awardIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AwardQuery query = getQuery();

        try (org.osid.id.IdList ids = awardIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAwardsByQuery(query));
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  award genus <code>Type</code> which does not include
     *  awards of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AwardQuery query = getQuery();
        query.matchGenusType(awardGenusType, true);
        return (this.session.getAwardsByQuery(query));
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  award genus <code>Type</code> and include any additional
     *  awards with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByParentGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AwardQuery query = getQuery();
        query.matchParentGenusType(awardGenusType, true);
        return (this.session.getAwardsByQuery(query));
    }


    /**
     *  Gets an <code>AwardList</code> containing the given
     *  award record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardRecordType an award record type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByRecordType(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AwardQuery query = getQuery();
        query.matchRecordType(awardRecordType, true);
        return (this.session.getAwardsByQuery(query));
    }

    
    /**
     *  Gets all <code>Awards</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Awards</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwards()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AwardQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAwardsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.recognition.AwardQuery getQuery() {
        org.osid.recognition.AwardQuery query = this.session.getAwardQuery();
        return (query);
    }
}
