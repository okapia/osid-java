//
// AbstractAssemblyScheduleQuery.java
//
//     A ScheduleQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ScheduleQuery that stores terms.
 */

public abstract class AbstractAssemblyScheduleQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.calendaring.ScheduleQuery,
               org.osid.calendaring.ScheduleQueryInspector,
               org.osid.calendaring.ScheduleSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.ScheduleQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.ScheduleQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.ScheduleSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyScheduleQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyScheduleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the schedule <code> Id </code> for this query for matching nested 
     *  schedule slots. 
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleSlotId(org.osid.id.Id scheduleSlotId, 
                                    boolean match) {
        getAssembler().addIdTerm(getScheduleSlotIdColumn(), scheduleSlotId, match);
        return;
    }


    /**
     *  Clears the schedule slot <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleSlotIdTerms() {
        getAssembler().clearTerms(getScheduleSlotIdColumn());
        return;
    }


    /**
     *  Gets the schedule slot <code> Id </code> terms. 
     *
     *  @return the schedule slot <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleSlotIdTerms() {
        return (getAssembler().getIdTerms(getScheduleSlotIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the schedule slot. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleSlot(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScheduleSlotColumn(), style);
        return;
    }


    /**
     *  Gets the ScheduleSlotId column name.
     *
     * @return the column name
     */

    protected String getScheduleSlotIdColumn() {
        return ("schedule_slot_id");
    }


    /**
     *  Tests if a <code> ScheduleSlotQuery </code> is available for querying 
     *  sechedule slots. 
     *
     *  @return <code> true </code> if a schedule slot query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedul slot. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the schedule slot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuery getScheduleSlotQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleSlotQuery() is false");
    }


    /**
     *  Matches a schedule that has any schedule slot assigned. 
     *
     *  @param  match <code> true </code> to match schedule with any schedule 
     *          slots, <code> false </code> to match schedules with no 
     *          schedule slots 
     */

    @OSID @Override
    public void matchAnyScheduleSlot(boolean match) {
        getAssembler().addIdWildcardTerm(getScheduleSlotColumn(), match);
        return;
    }


    /**
     *  Clears the schedule slot terms. 
     */

    @OSID @Override
    public void clearScheduleSlotTerms() {
        getAssembler().clearTerms(getScheduleSlotColumn());
        return;
    }


    /**
     *  Gets the schedule slot terms. 
     *
     *  @return the schedule slot terms 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQueryInspector[] getScheduleSlotTerms() {
        return (new org.osid.calendaring.ScheduleSlotQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ScheduleSlotSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a schedule slot search order is
     *          available, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsScheduleSlotSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the schedule slot.
     *
     *  @return the schedule slot search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchOrder getScheduleSlotSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScheduleSlotSearchOrder() is false");
    }


    /**
     *  Gets the ScheduleSlot column name.
     *
     * @return the column name
     */

    protected String getScheduleSlotColumn() {
        return ("schedule_slot");
    }


    /**
     *  Sets the time period <code> Id </code> for this query. 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        getAssembler().addIdTerm(getTimePeriodIdColumn(), timePeriodId, match);
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        getAssembler().clearTerms(getTimePeriodIdColumn());
        return;
    }


    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (getAssembler().getIdTerms(getTimePeriodIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the time period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimePeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimePeriodColumn(), style);
        return;
    }


    /**
     *  Gets the TimePeriodId column name.
     *
     * @return the column name
     */

    protected String getTimePeriodIdColumn() {
        return ("time_period_id");
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches a schedule that has any time period assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any time 
     *          periods, <code> false </code> to match schedules with no time 
     *          periods 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getTimePeriodColumn(), match);
        return;
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        getAssembler().clearTerms(getTimePeriodColumn());
        return;
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Tests if a <code> TimePeriodSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a time period search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the time period. 
     *
     *  @return the time period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchOrder getTimePeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimePeriodSearchOrder() is false");
    }


    /**
     *  Gets the TimePeriod column name.
     *
     * @return the column name
     */

    protected String getTimePeriodColumn() {
        return ("time_period");
    }


    /**
     *  Matches the schedule start time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleStart(org.osid.calendaring.DateTime low, 
                                   org.osid.calendaring.DateTime high, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getScheduleStartColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a schedule that has any start time assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any start 
     *          time, <code> false </code> to match schedules with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyScheduleStart(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getScheduleStartColumn(), match);
        return;
    }


    /**
     *  Clears the schedule start terms. 
     */

    @OSID @Override
    public void clearScheduleStartTerms() {
        getAssembler().clearTerms(getScheduleStartColumn());
        return;
    }


    /**
     *  Gets the schedule start terms. 
     *
     *  @return the schedule start terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getScheduleStartTerms() {
        return (getAssembler().getDateTimeRangeTerms(getScheduleStartColumn()));
    }


    /**
     *  Specified a preference for ordering results by the schedule start. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleStart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScheduleStartColumn(), style);
        return;
    }


    /**
     *  Gets the ScheduleStart column name.
     *
     * @return the column name
     */

    protected String getScheduleStartColumn() {
        return ("schedule_start");
    }


    /**
     *  Matches the schedule end time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleEnd(org.osid.calendaring.DateTime low, 
                                 org.osid.calendaring.DateTime high, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getScheduleEndColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a schedule that has any end time assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any end 
     *          time, <code> false </code> to match schedules with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyScheduleEnd(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getScheduleEndColumn(), match);
        return;
    }


    /**
     *  Clears the schedule end terms. 
     */

    @OSID @Override
    public void clearScheduleEndTerms() {
        getAssembler().clearTerms(getScheduleEndColumn());
        return;
    }


    /**
     *  Gets the schedule end terms. 
     *
     *  @return the schedule end terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getScheduleEndTerms() {
        return (getAssembler().getDateTimeRangeTerms(getScheduleEndColumn()));
    }


    /**
     *  Specified a preference for ordering results by the schedule end. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleEnd(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScheduleEndColumn(), style);
        return;
    }


    /**
     *  Gets the ScheduleEnd column name.
     *
     * @return the column name
     */

    protected String getScheduleEndColumn() {
        return ("schedule_end");
    }


    /**
     *  Matches schedules with start and end times between the given range 
     *  inclusive. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchScheduleTime(org.osid.calendaring.DateTime date, 
                                  boolean match) {
        getAssembler().addDateTimeTerm(getScheduleTimeColumn(), date, match);
        return;
    }


    /**
     *  Matches schedules that has any time assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any time, 
     *          <code> false </code> to match schedules with no time 
     */

    @OSID @Override
    public void matchAnyScheduleTime(boolean match) {
        getAssembler().addDateTimeWildcardTerm(getScheduleTimeColumn(), match);
        return;
    }


    /**
     *  Clears the schedule time terms. 
     */

    @OSID @Override
    public void clearScheduleTimeTerms() {
        getAssembler().clearTerms(getScheduleTimeColumn());
        return;
    }


    /**
     *  Gets the schedule time terms. 
     *
     *  @return the schedule time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getScheduleTimeTerms() {
        return (getAssembler().getDateTimeTerms(getScheduleTimeColumn()));
    }


    /**
     *  Gets the ScheduleTime column name.
     *
     * @return the column name
     */

    protected String getScheduleTimeColumn() {
        return ("schedule_time");
    }


    /**
     *  Matches schedules with start and end times between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> end </code> or <code> 
     *          start </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleTimeInclusive(org.osid.calendaring.DateTime start, 
                                           org.osid.calendaring.DateTime end, 
                                           boolean match) {
        getAssembler().addDateTimeRangeTerm(getScheduleTimeInclusiveColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the schedule time inclusive terms. 
     */

    @OSID @Override
    public void clearScheduleTimeInclusiveTerms() {
        getAssembler().clearTerms(getScheduleTimeInclusiveColumn());
        return;
    }


    /**
     *  Gets the schedule inclusive time terms. 
     *
     *  @return the schedule time range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getScheduleTimeInclusiveTerms() {
        return (getAssembler().getDateTimeRangeTerms(getScheduleTimeInclusiveColumn()));
    }


    /**
     *  Gets the ScheduleTimeInclusive column name.
     *
     * @return the column name
     */

    protected String getScheduleTimeInclusiveColumn() {
        return ("schedule_time_inclusive");
    }


    /**
     *  Matches schedules that have the given limit in the given range 
     *  inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchLimit(long from, long to, boolean match) {
        getAssembler().addIntegerRangeTerm(getLimitColumn(), from, to, match);
        return;
    }


    /**
     *  Matches schedules with any occurrence limit. 
     *
     *  @param  match <code> true </code> to match schedules with any limit, 
     *          to match schedules with no limit 
     */

    @OSID @Override
    public void matchAnyLimit(boolean match) {
        getAssembler().addIntegerRangeWildcardTerm(getLimitColumn(), match);
        return;
    }


    /**
     *  Clears the limit terms. 
     */

    @OSID @Override
    public void clearLimitTerms() {
        getAssembler().clearTerms(getLimitColumn());
        return;
    }


    /**
     *  Gets the limit terms. 
     *
     *  @return the limit terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getLimitTerms() {
        return (getAssembler().getCardinalTerms(getLimitColumn()));
    }


    /**
     *  Specified a preference for ordering results by the occurrence limit. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLimit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLimitColumn(), style);
        return;
    }


    /**
     *  Gets the Limit column name.
     *
     * @return the column name
     */

    protected String getLimitColumn() {
        return ("limit");
    }


    /**
     *  Matches the location description string. 
     *
     *  @param  location location string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> location </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        getAssembler().addStringTerm(getLocationDescriptionColumn(), location, stringMatchType, match);
        return;
    }


    /**
     *  Matches a schedule that has any location description assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any location 
     *          description, <code> false </code> to match schedules with no 
     *          location description 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        getAssembler().addStringWildcardTerm(getLocationDescriptionColumn(), match);
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        getAssembler().clearTerms(getLocationDescriptionColumn());
        return;
    }


    /**
     *  Gets the location description terms. 
     *
     *  @return the location description terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (getAssembler().getStringTerms(getLocationDescriptionColumn()));
    }


    /**
     *  Specified a preference for ordering results by the location 
     *  description. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocationDescription(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationDescriptionColumn(), style);
        return;
    }


    /**
     *  Gets the LocationDescription column name.
     *
     * @return the column name
     */

    protected String getLocationDescriptionColumn() {
        return ("location_description");
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> terms. 
     *
     *  @return the location <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the location. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLocationColumn(), style);
        return;
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  locations. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches a schedule that has any location assigned. 
     *
     *  @param  match <code> true </code> to match schedules with any 
     *          location, <code> false </code> to match schedules with no 
     *          location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location terms. 
     *
     *  @return the location terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Tests if a <code> LocationSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a location search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a location. 
     *
     *  @return the location search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchOrder getLocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLocationSearchOrder() is false");
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Matches the total duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalDuration(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        getAssembler().addDurationRangeTerm(getTotalDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the total duration terms. 
     */

    @OSID @Override
    public void clearTotalDurationTerms() {
        getAssembler().clearTerms(getTotalDurationColumn());
        return;
    }


    /**
     *  Gets the total duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalDurationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the schedule duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalDurationColumn(), style);
        return;
    }


    /**
     *  Gets the TotalDuration column name.
     *
     * @return the column name
     */

    protected String getTotalDurationColumn() {
        return ("total_duration");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this schedule supports the given record
     *  <code>Type</code>.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return <code>true</code> if the scheduleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type scheduleRecordType) {
        for (org.osid.calendaring.records.ScheduleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  scheduleRecordType the schedule record type 
     *  @return the schedule query record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleQueryRecord getScheduleQueryRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  scheduleRecordType the schedule record type 
     *  @return the schedule query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleQueryInspectorRecord getScheduleQueryInspectorRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param scheduleRecordType the schedule record type
     *  @return the schedule search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSearchOrderRecord getScheduleSearchOrderRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this schedule. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param scheduleQueryRecord the schedule query record
     *  @param scheduleQueryInspectorRecord the schedule query inspector
     *         record
     *  @param scheduleSearchOrderRecord the schedule search order record
     *  @param scheduleRecordType schedule record type
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleQueryRecord</code>,
     *          <code>scheduleQueryInspectorRecord</code>,
     *          <code>scheduleSearchOrderRecord</code> or
     *          <code>scheduleRecordTypeschedule</code> is
     *          <code>null</code>
     */
            
    protected void addScheduleRecords(org.osid.calendaring.records.ScheduleQueryRecord scheduleQueryRecord, 
                                      org.osid.calendaring.records.ScheduleQueryInspectorRecord scheduleQueryInspectorRecord, 
                                      org.osid.calendaring.records.ScheduleSearchOrderRecord scheduleSearchOrderRecord, 
                                      org.osid.type.Type scheduleRecordType) {

        addRecordType(scheduleRecordType);

        nullarg(scheduleQueryRecord, "schedule query record");
        nullarg(scheduleQueryInspectorRecord, "schedule query inspector record");
        nullarg(scheduleSearchOrderRecord, "schedule search odrer record");

        this.queryRecords.add(scheduleQueryRecord);
        this.queryInspectorRecords.add(scheduleQueryInspectorRecord);
        this.searchOrderRecords.add(scheduleSearchOrderRecord);
        
        return;
    }
}
