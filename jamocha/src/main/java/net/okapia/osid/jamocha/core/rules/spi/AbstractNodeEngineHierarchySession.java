//
// AbstractNodeEngineHierarchySession.java
//
//     Defines an Engine hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an engine hierarchy session for delivering a hierarchy
 *  of engines using the EngineNode interface.
 */

public abstract class AbstractNodeEngineHierarchySession
    extends net.okapia.osid.jamocha.rules.spi.AbstractEngineHierarchySession
    implements org.osid.rules.EngineHierarchySession {

    private java.util.Collection<org.osid.rules.EngineNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root engine <code> Ids </code> in this hierarchy.
     *
     *  @return the root engine <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootEngineIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.rules.enginenode.EngineNodeToIdList(this.roots));
    }


    /**
     *  Gets the root engines in the engine hierarchy. A node
     *  with no parents is an orphan. While all engine <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root engines 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getRootEngines()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.rules.enginenode.EngineNodeToEngineList(new net.okapia.osid.jamocha.rules.enginenode.ArrayEngineNodeList(this.roots)));
    }


    /**
     *  Adds a root engine node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootEngine(org.osid.rules.EngineNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root engine nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootEngines(java.util.Collection<org.osid.rules.EngineNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root engine node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootEngine(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.rules.EngineNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Engine </code> has any parents. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @return <code> true </code> if the engine has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentEngines(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getEngineNode(engineId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  engine.
     *
     *  @param  id an <code> Id </code> 
     *  @param  engineId the <code> Id </code> of an engine 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> engineId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> engineId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfEngine(org.osid.id.Id id, org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.rules.EngineNodeList parents = getEngineNode(engineId).getParentEngineNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextEngineNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given engine. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @return the parent <code> Ids </code> of the engine 
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentEngineIds(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.rules.engine.EngineToIdList(getParentEngines(engineId)));
    }


    /**
     *  Gets the parents of the given engine. 
     *
     *  @param  engineId the <code> Id </code> to query 
     *  @return the parents of the engine 
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getParentEngines(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.rules.enginenode.EngineNodeToEngineList(getEngineNode(engineId).getParentEngineNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  engine.
     *
     *  @param  id an <code> Id </code> 
     *  @param  engineId the Id of an engine 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> engineId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfEngine(org.osid.id.Id id, org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfEngine(id, engineId)) {
            return (true);
        }

        try (org.osid.rules.EngineList parents = getParentEngines(engineId)) {
            while (parents.hasNext()) {
                if (isAncestorOfEngine(id, parents.getNextEngine().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an engine has any children. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @return <code> true </code> if the <code> engineId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildEngines(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEngineNode(engineId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  engine.
     *
     *  @param  id an <code> Id </code> 
     *  @param engineId the <code> Id </code> of an 
     *         engine
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> engineId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> engineId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfEngine(org.osid.id.Id id, org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfEngine(engineId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  engine.
     *
     *  @param  engineId the <code> Id </code> to query 
     *  @return the children of the engine 
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildEngineIds(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.rules.engine.EngineToIdList(getChildEngines(engineId)));
    }


    /**
     *  Gets the children of the given engine. 
     *
     *  @param  engineId the <code> Id </code> to query 
     *  @return the children of the engine 
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getChildEngines(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.rules.enginenode.EngineNodeToEngineList(getEngineNode(engineId).getChildEngineNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  engine.
     *
     *  @param  id an <code> Id </code> 
     *  @param engineId the <code> Id </code> of an 
     *         engine
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> engineId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfEngine(org.osid.id.Id id, org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfEngine(engineId, id)) {
            return (true);
        }

        try (org.osid.rules.EngineList children = getChildEngines(engineId)) {
            while (children.hasNext()) {
                if (isDescendantOfEngine(id, children.getNextEngine().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  engine.
     *
     *  @param  engineId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified engine node 
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getEngineNodeIds(org.osid.id.Id engineId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.rules.enginenode.EngineNodeToNode(getEngineNode(engineId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given engine.
     *
     *  @param  engineId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified engine node 
     *  @throws org.osid.NotFoundException <code> engineId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> engineId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineNode getEngineNodes(org.osid.id.Id engineId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEngineNode(engineId));
    }


    /**
     *  Closes this <code>EngineHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an engine node.
     *
     *  @param engineId the id of the engine node
     *  @throws org.osid.NotFoundException <code>engineId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>engineId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.rules.EngineNode getEngineNode(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(engineId, "engine Id");
        for (org.osid.rules.EngineNode engine : this.roots) {
            if (engine.getId().equals(engineId)) {
                return (engine);
            }

            org.osid.rules.EngineNode r = findEngine(engine, engineId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(engineId + " is not found");
    }


    protected org.osid.rules.EngineNode findEngine(org.osid.rules.EngineNode node, 
                                                   org.osid.id.Id engineId) 
	throws org.osid.OperationFailedException {

        try (org.osid.rules.EngineNodeList children = node.getChildEngineNodes()) {
            while (children.hasNext()) {
                org.osid.rules.EngineNode engine = children.getNextEngineNode();
                if (engine.getId().equals(engineId)) {
                    return (engine);
                }
                
                engine = findEngine(engine, engineId);
                if (engine != null) {
                    return (engine);
                }
            }
        }

        return (null);
    }
}
