//
// AbstractJobConstrainerEnablerSearch.java
//
//     A template for making a JobConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing job constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractJobConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resourcing.rules.JobConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resourcing.rules.JobConstrainerEnablerSearchOrder jobConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of job constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  jobConstrainerEnablerIds list of job constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongJobConstrainerEnablers(org.osid.id.IdList jobConstrainerEnablerIds) {
        while (jobConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(jobConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongJobConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of job constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getJobConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  jobConstrainerEnablerSearchOrder job constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>jobConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderJobConstrainerEnablerResults(org.osid.resourcing.rules.JobConstrainerEnablerSearchOrder jobConstrainerEnablerSearchOrder) {
	this.jobConstrainerEnablerSearchOrder = jobConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resourcing.rules.JobConstrainerEnablerSearchOrder getJobConstrainerEnablerSearchOrder() {
	return (this.jobConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given job constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a job constrainer enabler implementing the requested record.
     *
     *  @param jobConstrainerEnablerSearchRecordType a job constrainer enabler search record
     *         type
     *  @return the job constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerEnablerSearchRecord getJobConstrainerEnablerSearchRecord(org.osid.type.Type jobConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resourcing.rules.records.JobConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(jobConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job constrainer enabler search. 
     *
     *  @param jobConstrainerEnablerSearchRecord job constrainer enabler search record
     *  @param jobConstrainerEnablerSearchRecordType jobConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobConstrainerEnablerSearchRecord(org.osid.resourcing.rules.records.JobConstrainerEnablerSearchRecord jobConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type jobConstrainerEnablerSearchRecordType) {

        addRecordType(jobConstrainerEnablerSearchRecordType);
        this.records.add(jobConstrainerEnablerSearchRecord);        
        return;
    }
}
