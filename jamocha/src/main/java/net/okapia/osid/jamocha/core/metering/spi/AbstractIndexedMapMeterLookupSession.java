//
// AbstractIndexedMapMeterLookupSession.java
//
//    A simple framework for providing a Meter lookup service
//    backed by a fixed collection of meters with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Meter lookup service backed by a
 *  fixed collection of meters. The meters are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some meters may be compatible
 *  with more types than are indicated through these meter
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Meters</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapMeterLookupSession
    extends AbstractMapMeterLookupSession
    implements org.osid.metering.MeterLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.metering.Meter> metersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.metering.Meter>());
    private final MultiMap<org.osid.type.Type, org.osid.metering.Meter> metersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.metering.Meter>());


    /**
     *  Makes a <code>Meter</code> available in this session.
     *
     *  @param  meter a meter
     *  @throws org.osid.NullArgumentException <code>meter<code> is
     *          <code>null</code>
     */

    @Override
    protected void putMeter(org.osid.metering.Meter meter) {
        super.putMeter(meter);

        this.metersByGenus.put(meter.getGenusType(), meter);
        
        try (org.osid.type.TypeList types = meter.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.metersByRecord.put(types.getNextType(), meter);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a meter from this session.
     *
     *  @param meterId the <code>Id</code> of the meter
     *  @throws org.osid.NullArgumentException <code>meterId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeMeter(org.osid.id.Id meterId) {
        org.osid.metering.Meter meter;
        try {
            meter = getMeter(meterId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.metersByGenus.remove(meter.getGenusType());

        try (org.osid.type.TypeList types = meter.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.metersByRecord.remove(types.getNextType(), meter);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeMeter(meterId);
        return;
    }


    /**
     *  Gets a <code>MeterList</code> corresponding to the given
     *  meter genus <code>Type</code> which does not include
     *  meters of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known meters or an error results. Otherwise,
     *  the returned list may contain only those meters that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  meterGenusType a meter genus type 
     *  @return the returned <code>Meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByGenusType(org.osid.type.Type meterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.metering.meter.ArrayMeterList(this.metersByGenus.get(meterGenusType)));
    }


    /**
     *  Gets a <code>MeterList</code> containing the given
     *  meter record <code>Type</code>. In plenary mode, the
     *  returned list contains all known meters or an error
     *  results. Otherwise, the returned list may contain only those
     *  meters that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  meterRecordType a meter record type 
     *  @return the returned <code>meter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByRecordType(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.metering.meter.ArrayMeterList(this.metersByRecord.get(meterRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.metersByGenus.clear();
        this.metersByRecord.clear();

        super.close();

        return;
    }
}
