//
// AbstractImmutableAction.java
//
//     Wraps a mutable Action to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.action.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Action</code> to hide modifiers. This
 *  wrapper provides an immutized Action from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying action whose state changes are visible.
 */

public abstract class AbstractImmutableAction
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.control.Action {

    private final org.osid.control.Action action;


    /**
     *  Constructs a new <code>AbstractImmutableAction</code>.
     *
     *  @param action the action to immutablize
     *  @throws org.osid.NullArgumentException <code>action</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAction(org.osid.control.Action action) {
        super(action);
        this.action = action;
        return;
    }


    /**
     *  Gets the action group <code> Id </code> for this action. 
     *
     *  @return the action group <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActionGroupId() {
        return (this.action.getActionGroupId());
    }


    /**
     *  Gets the action group for this action. 
     *
     *  @return the action group 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getActionGroup()
        throws org.osid.OperationFailedException {

        return (this.action.getActionGroup());
    }


    /**
     *  Gets the delay before proceeding with execution. 
     *
     *  @return the delay 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDelay() {
        return (this.action.getDelay());
    }


    /**
     *  Tests if this action blocks further actions until complete. 
     *
     *  @return <code> true </code> if blocking, <code> false </code> if 
     *          subsequent actions can begin before this action completes 
     */

    @OSID @Override
    public boolean isBlocking() {
        return (this.action.isBlocking());
    }


    /**
     *  Tests if this rule executes a scene. If <code> executesActionGroup() 
     *  </code> is <code> true, </code> <code> hasRule(), executesScene(), 
     *  executesSetting(), </code> and <code> executesMatchingSetting() 
     *  </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if this is a scene execution, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean executesActionGroup() {
        return (this.action.executesActionGroup());
    }


    /**
     *  Gets the action group <code> Id </code> to execute. 
     *
     *  @return the action group <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> executesActionGroup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getNextActionGroupId() {
        return (this.action.getNextActionGroupId());
    }


    /**
     *  Gets the action group to execute, 
     *
     *  @return the action group 
     *  @throws org.osid.IllegalStateException <code> executesActionGroup() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getNextActionGroup()
        throws org.osid.OperationFailedException {

        return (this.action.getNextActionGroup());
    }


    /**
     *  Tests if this rule executes a scene. If <code> executesScene() </code> 
     *  is <code> true, </code> <code> hasRule(), executesActionGroup(), 
     *  executesSetting(), </code> and <code> executesMatchingSetting() 
     *  </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if this is a scene execution, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean executesScene() {
        return (this.action.executesScene());
    }


    /**
     *  Gets the scene <code> Id. </code> 
     *
     *  @return the scene <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> executesScene() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSceneId() {
        return (this.action.getSceneId());
    }


    /**
     *  Gets the scene. 
     *
     *  @return the scene 
     *  @throws org.osid.IllegalStateException <code> executesScene() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Scene getScene()
        throws org.osid.OperationFailedException {

        return (this.action.getScene());
    }


    /**
     *  Tests if this rule executes a setting. If <code> executesSetting() 
     *  </code> is <code> true, </code> <code> hasRule(), 
     *  executesActionGroup(), executesScene(), </code> and <code> 
     *  executesMatchingSetting() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if this is a setting execution, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean executesSetting() {
        return (this.action.executesSetting());
    }


    /**
     *  Gets the setting <code> Id. </code> 
     *
     *  @return the setting <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> executesSetting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSettingId() {
        return (this.action.getSettingId());
    }


    /**
     *  Gets the setting. 
     *
     *  @return the setting 
     *  @throws org.osid.IllegalStateException <code> executesSetting() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Setting getSetting()
        throws org.osid.OperationFailedException {

        return (this.action.getSetting());
    }


    /**
     *  Tests if this rule executes a matching setting. If <code> 
     *  executesMatchingSetting() </code> is <code> true, </code> <code> 
     *  hasRule(), </code> <code> executesScene(), executesActionGroup(), 
     *  </code> and <code> executesSetting() </code> must be <code> false. 
     *  </code> 
     *
     *  @return <code> true </code> if this is a setting execution, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean executesMatchingSetting() {
        return (this.action.executesMatchingSetting());
    }


    /**
     *  Gets the matching controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMatchingControllerId() {
        return (this.action.getMatchingControllerId());
    }


    /**
     *  Gets the matching controller. 
     *
     *  @return the controller 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getMatchingController()
        throws org.osid.OperationFailedException {

        return (this.action.getMatchingController());
    }


    /**
     *  Gets the factor by which the matching amount differs. A factor of 1 
     *  matches the amount of the matching controller. 
     *
     *  @return the factor 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMatchingAmountFactor() {
        return (this.action.getMatchingAmountFactor());
    }


    /**
     *  Gets the factor by which the matching transition rate differs. A 
     *  factor of 1 matches the ramp rate of the matching controller. 
     *
     *  @return the factor 
     *  @throws org.osid.IllegalStateException <code> 
     *          executesMatchingSetting() </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMatchingRateFactor() {
        return (this.action.getMatchingRateFactor());
    }


    /**
     *  Gets the action record corresponding to the given <code>
     *  Action </code> record <code> Type. </code> This method is used
     *  to retrieve an object implementing the requested record. The
     *  <code> actionRecordType </code> may be the <code> Type </code>
     *  returned in <code> getRecordTypes() </code> or any of its
     *  parents in a <code> Type </code> hierarchy where <code>
     *  hasRecordType(actionRecordType) </code> is <code> true </code>
     *  .
     *
     *  @param  actionRecordType the type of action record to retrieve 
     *  @return the action record 
     *  @throws org.osid.NullArgumentException <code> actionRecordType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(actionRecordType) </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public org.osid.control.records.ActionRecord getActionRecord(org.osid.type.Type actionRecordType)
        throws org.osid.OperationFailedException {

        return (this.action.getActionRecord(actionRecordType));
    }
}

