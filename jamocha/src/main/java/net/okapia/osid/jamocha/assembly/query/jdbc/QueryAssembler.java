//
// QueryAssembler.java
//
//     An interface for assembling a search queries.
//
//
// Tom Coppeto
// Okapia
// 23 April 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc;

import net.okapia.osid.primordium.types.search.StringMatchTypes;
import net.okapia.osid.torrefacto.collect.TypeEnumSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A utility interface for assembling SQL search queries.
 */

public class QueryAssembler 
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractQueryAssembler
    implements net.okapia.osid.jamocha.assembly.query.QueryAssembler {

    private static final TypeEnumSet<StringMatchTypes> stringMatchTypes = new TypeEnumSet<>(StringMatchTypes.class);


    /**
     *  Constructs a new <code>QueryAssembler</code>.
     */

    public QueryAssembler() {
        super.addStringMatchTypes(stringMatchTypes.toCollection());
        return;
    }

    
    /**
     *  Gets the string matching types supported. A string match type
     *  specifies the syntax of the string query, such as matching a word or
     *  including a wildcard or regular expression.
     *
     *  @return a list containing the supported string match types
     *  @throws org.osid.NullArgumentException <code>type</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException <code>type</code> not
     *          supported
     */

    static StringMatchTypes getStringMatchType(org.osid.type.Type type) {
        return (stringMatchTypes.getEnum(type));
    }


    /**
     *  Adds an SQL Boolean search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addBooleanTerm(String column, boolean match) {
        addTerm(new BooleanQueryTerm(column, match, true));
        return;
    }
        

    /**
     *  Adds a Boolean wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *  is <code>null</code>
     */

    @Override
    public void addBooleanWildcardTerm(String column, boolean match) {
        addTerm(new BooleanWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL Byte search query term.
     *
     *  @param column query column
     *  @param bytes an array of bytes
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>bytes</code> is <code>null</code>
     */
    
    @Override
    public void addBytesTerm(String column, byte[] bytes, boolean match, boolean partial) {
        addTerm(new BytesQueryTerm(column, bytes, match, partial));
        return;
    }
        

    /**
     *  Adds a Byte wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *  is <code>null</code>
     */

    @Override
    public void addBytesWildcardTerm(String column, boolean match) {
        addTerm(new BytesWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL Cardinal search query term.
     *
     *  @param column query column
     *  @param value a cardinal value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addCardinalTerm(String column, long value, boolean match) {
        addTerm(new CardinalQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Cardinal wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addCardinalWildcardTerm(String column, boolean match) {
        addTerm(new CardinalWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds an SQL cardinal range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addCardinalRangeTerm(String column, long start, long end, boolean match) {
        addTerm(new CardinalRangeQueryTerm(column, start, end, match));
        return;
    }
    

    /**
     *  Adds an SQL Cardinal wildcard range search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addCardinalRangeWildcardTerm(String column, boolean match) {
        addTerm(new CardinalRangeWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds an SQL Coordinate search query term.
     *
     *  @param column query column
     *  @param value a coordinate value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addCoordinateTerm(String column, org.osid.mapping.Coordinate value, 
                                  boolean match) {
        addTerm(new CoordinateQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Coordinate wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addCoordinateWildcardTerm(String column, boolean match) {
        addTerm(new CoordinateWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL coordinate range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addCoordinateRangeTerm(String column, org.osid.mapping.Coordinate start, 
                                       org.osid.mapping.Coordinate end, boolean match) {
        addTerm(new CoordinateRangeQueryTerm(column, start, end, match));
        return;
    }


    /**
     *  Adds an SQL Coordinate range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addCoordinateRangeWildcardTerm(String column, boolean match) {
        addTerm(new CoordinateRangeWildcardQueryTerm(column, match));
        return;
    }

    
    /**
     *  Adds an SQL Currency search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addCurrencyTerm(String column, org.osid.financials.Currency value, 
                                boolean match) {
        addTerm(new CurrencyQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Currency wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addCurrencyWildcardTerm(String column, boolean match) {
        addTerm(new CurrencyWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds an SQL currency range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addCurrencyRangeTerm(String column, org.osid.financials.Currency start, 
                                     org.osid.financials.Currency end, boolean match) {
        addTerm(new CurrencyRangeQueryTerm(column, start, end, match));
        return;
    }


    /**
     *  Adds an SQL Currency wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addCurrencyRangeWildcardTerm(String column, boolean match) {
        addTerm(new CurrencyWildcardQueryTerm(column, match));
        return;
    }
    
    
    /**
     *  Adds an SQL <code>DateTime</code> search query term.
     *
     *  @param column query column
     *  @param value datetime value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addDateTimeTerm(String column, org.osid.calendaring.DateTime value, 
                                boolean match) {
        addTerm(new DateTimeQueryTerm(column, value, match));
        return;
    }
    

    /**
     *  Adds an SQL <code>DateTime</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addDateTimeWildcardTerm(String column, boolean match) {
        addTerm(new DateTimeWildcardQueryTerm(column, match));
        return;
    }
    
    
    /**
     *  Adds an SQL <code>DateTime</code> range search query term.
     *
     *  @param column query column
     *  @param start low end time
     *  @param end high end time
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addDateTimeRangeTerm(String column, org.osid.calendaring.DateTime start, 
                                     org.osid.calendaring.DateTime end, boolean match) {
        addTerm(new DateTimeRangeQueryTerm(column, start, end, match));
        return;
    }
    

    /**
     *  Adds an SQL <code>DateTime</code> range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addDateTimeRangeWildcardTerm(String column, boolean match) {
        addTerm(new DateTimeRangeWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds an SQL Decimal search query term.
     *
     *  @param column query column
     *  @param value a decimal value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addDecimalTerm(String column, java.math.BigDecimal value, boolean match) {
        addTerm(new DecimalQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Decimal wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addDecimalWildcardTerm(String column, boolean match) {
        addTerm(new DecimalWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds an SQL decimal range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addDecimalRangeTerm(String column, java.math.BigDecimal start, 
                                    java.math.BigDecimal end, boolean match) {
        addTerm(new DecimalRangeQueryTerm(column, start, end, match));
        return;
    }
    

    /**
     *  Adds an SQL Decimal wildcard range search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addDecimalRangeWildcardTerm(String column, boolean match) {
        addTerm(new DecimalRangeWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds an SQL Distance search query term.
     *
     *  @param column query column
     *  @param value a distance value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addDistanceTerm(String column, org.osid.mapping.Distance value, 
                                boolean match) {
        addTerm(new DistanceQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Distance wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addDistanceWildcardTerm(String column, boolean match) {
        addTerm(new DistanceWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL distance range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addDistanceRangeTerm(String column, org.osid.mapping.Distance start, 
                                     org.osid.mapping.Distance end, boolean match) {
        addTerm(new DistanceRangeQueryTerm(column, start, end, match));
        return;
    }


    /**
     *  Adds an SQL Distance range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addDistanceRangeWildcardTerm(String column, boolean match) {
        addTerm(new DistanceRangeWildcardQueryTerm(column, match));
        return;
    }

    
    /**
     *  Adds an SQL Duration search query term.
     *
     *  @param column query column
     *  @param value a duration value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addDurationTerm(String column, org.osid.calendaring.Duration value, 
                                boolean match) {
        addTerm(new DurationQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Duration wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addDurationWildcardTerm(String column, boolean match) {
        addTerm(new DurationWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL duration range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addDurationRangeTerm(String column, org.osid.calendaring.Duration start, 
                                     org.osid.calendaring.Duration end, boolean match) {
        addTerm(new DurationRangeQueryTerm(column, start, end, match));
        return;
    }


    /**
     *  Adds an SQL Duration range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addDurationRangeWildcardTerm(String column, boolean match) {
        addTerm(new DurationRangeWildcardQueryTerm(column, match));
        return;
    }

    
    /**
     *  Adds an SQL Heading search query term.
     *
     *  @param column query column
     *  @param value a heading value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addHeadingTerm(String column, org.osid.mapping.Heading value, 
                                       boolean match) {
        addTerm(new HeadingQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Heading wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addHeadingWildcardTerm(String column, boolean match) {
        addTerm(new HeadingWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL heading range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addHeadingRangeTerm(String column, org.osid.mapping.Heading start, 
                                    org.osid.mapping.Heading end,  boolean match) {
        addTerm(new HeadingRangeQueryTerm(column, start, end, match));
        return;
    }


    /**
     *  Adds an SQL Heading range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addHeadingRangeWildcardTerm(String column, boolean match) {
        addTerm(new HeadingRangeWildcardQueryTerm(column, match));
        return;
    }

    
    /**
     *  Adds an SQL <code>Id</code> search query term.
     *
     *  @param column query column
     *  @param value Id value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addIdTerm(String column, org.osid.id.Id value, boolean match) {
        addTerm(new IdQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL <code>Id</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addIdWildcardTerm(String column, boolean match) {
        addTerm(new IdWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds an SQL <code>Id</code> set search query term.
     *
     *  @param column query column
     *  @param values Id values to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>values</code> is <code>null</code>
     */

    @Override
    public void addIdSetTerm(String column, org.osid.id.Id[] values, boolean match) {
        addTerm(new IdSetQueryTerm(column, values, match));
        return;
    }


    /**
     *  Adds an SQL integer search query term.
     *
     *  @param column query column
     *  @param value integer value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addIntegerTerm(String column, long value, boolean match) {
        addTerm(new IntegerQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL integer wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addIntegerWildcardTerm(String column, boolean match) {
        addTerm(new IntegerWildcardQueryTerm(column, match));
        return;
    }
    
    
    /**
     *  Adds an SQL integer range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addIntegerRangeTerm(String column, long start, long end, boolean match) {
        addTerm(new IntegerRangeQueryTerm(column, start, end, match));
        return;
    }
    

    /**
     *  Adds an SQL integer range search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addIntegerRangeWildcardTerm(String column, boolean match) {
        addTerm(new IntegerRangeWildcardQueryTerm(column, match));
        return;
    }
    

    /**
     *  Adds a Object search query term.
     *
     *  @param column query column
     *  @param value a object value
     *  @param objectType the objectType
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>value</code>, or <code>objectType</code> is
     *          <code>null</code>
     */

    @Override    
    public void addObjectTerm(String column, Object value, 
                              org.osid.type.Type objectType, boolean match) {
        addTerm(new ObjectQueryTerm(column, value, objectType, match));
        return;
    }


    /**
     *  Adds an SQL Object wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addObjectWildcardTerm(String column, boolean match) {
        addTerm(new ObjectWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL SpatialUnit search query term.
     *
     *  @param column query column
     *  @param value a spatial unit value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addSpatialUnitTerm(String column, org.osid.mapping.SpatialUnit value, 
                                   boolean match) {
        addTerm(new SpatialUnitQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL SpatialUnit wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addSpatialUnitWildcardTerm(String column, boolean match) {
        addTerm(new SpatialUnitWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL Speed search query term.
     *
     *  @param column query column
     *  @param value a speed value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addSpeedTerm(String column, org.osid.mapping.Speed value, 
                                       boolean match) {
        addTerm(new SpeedQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Speed wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addSpeedWildcardTerm(String column, boolean match) {
        addTerm(new SpeedWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL speed range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    @Override
    public void addSpeedRangeTerm(String column, org.osid.mapping.Speed start, 
                                  org.osid.mapping.Speed end, boolean match) {
        addTerm(new SpeedRangeQueryTerm(column, start, end, match));
        return;
    }


    /**
     *  Adds an SQL Speed range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addSpeedRangeWildcardTerm(String column, boolean match) {
        addTerm(new SpeedRangeWildcardQueryTerm(column, match));
        return;
    }

    
    /**
     *  Adds an SQL Syntax search query term.
     *
     *  @param column query column
     *  @param value a syntax value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addSyntaxTerm(String column, org.osid.Syntax value, boolean match) {
        addTerm(new SyntaxQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL Syntax wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    @Override
    public void addSyntaxWildcardTerm(String column, boolean match) {
        addTerm(new SyntaxWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds a SQL string search query term.
     *
     *  @param column query column
     *  @param value string value to match
     *  @param stringMatchType type of string matching
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>value</code>, <code>stringMatchType</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addStringTerm(String column, String value, 
                              org.osid.type.Type stringMatchType, boolean match) {
        addTerm(new StringQueryTerm(column, value, stringMatchType, match));
        return;
    }

    
    /**
     *  Adds a SQL string wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addStringWildcardTerm(String column, boolean match) {
        addTerm(new StringWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL <code>Time</code> search query term.
     *
     *  @param column query column
     *  @param value time value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addTimeTerm(String column, org.osid.calendaring.Time value, 
                            boolean match) {
        addTerm(new TimeQueryTerm(column, value, match));
        return;
    }
    

    /**
     *  Adds an SQL <code>Time</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addTimeWildcardTerm(String column, boolean match) {
        addTerm(new TimeWildcardQueryTerm(column, match));
        return;
    }
    
    
    /**
     *  Adds an SQL <code>Time</code> range search query term.
     *
     *  @param column query column
     *  @param start low end time
     *  @param end high end time
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addTimeRangeTerm(String column, org.osid.calendaring.Time start, 
                                 org.osid.calendaring.Time end, boolean match) {
        addTerm(new TimeRangeQueryTerm(column, start, end, match));
        return;
    }
    

    /**
     *  Adds an SQL <code>Time</code> range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addTimeRangeWildcardTerm(String column, boolean match) {
        addTerm(new TimeRangeWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL <code>Type</code> search query term.
     *
     *  @param column query column
     *  @param value Type value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addTypeTerm(String column, org.osid.type.Type value, boolean match) {
        addTerm(new TypeQueryTerm(column, value, match));
        return;
    }


    /**
     *  Adds an SQL <code>Type</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addTypeWildcardTerm(String column, boolean match) {
        addTerm(new TypeWildcardQueryTerm(column, match));
        return;
    }

    
    /**
     *  Adds an SQL <code>Version</code> search query term.
     *
     *  @param column query column
     *  @param value version value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    @Override
    public void addVersionTerm(String column, org.osid.installation.Version value, 
                               boolean match) {
        addTerm(new VersionQueryTerm(column, value, match));
        return;
    }
    

    /**
     *  Adds an SQL <code>Version</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addVersionWildcardTerm(String column, boolean match) {
        addTerm(new VersionWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Adds an SQL <code>Version</code> range search query term.
     *
     *  @param column query column
     *  @param start low end version
     *  @param end high end version
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addVersionRangeTerm(String column, org.osid.installation.Version start, 
                                    org.osid.installation.Version end, boolean match) {
        addTerm(new VersionRangeQueryTerm(column, start, end, match));
        return;
    }
    

    /**
     *  Adds an SQL <code>Version</code> range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    @Override
    public void addVersionRangeWildcardTerm(String column, boolean match) {
        addTerm(new VersionRangeWildcardQueryTerm(column, match));
        return;
    }


    /**
     *  Gets the resulting query string for matching.
     *
     *  @return the query string
     */

    public String getQueryString() {
        StringBuffer sb = new StringBuffer();
        boolean firstQuerySet;
        boolean firstQueryTerm;

        for (String column : getColumns()) {
            firstQuerySet = true;
            if (firstQuerySet) {
                firstQuerySet = false;
            } else {
                sb.append(" AND ");
            }

            sb.append('(');
            
            firstQueryTerm = true;
            for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
                if (firstQueryTerm) {
                    firstQueryTerm = false;
                } else {
                    sb.append(" OR ");
                }
                
                sb.append('(');
                sb.append(term);
                sb.append(')');
            }
            sb.append(')');
        }
        
        return (sb.toString());
    }
}
