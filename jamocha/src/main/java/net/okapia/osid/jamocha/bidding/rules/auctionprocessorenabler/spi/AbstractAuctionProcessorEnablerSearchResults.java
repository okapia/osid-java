//
// AbstractAuctionProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuctionProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.bidding.rules.AuctionProcessorEnablerSearchResults {

    private org.osid.bidding.rules.AuctionProcessorEnablerList auctionProcessorEnablers;
    private final org.osid.bidding.rules.AuctionProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuctionProcessorEnablerSearchResults.
     *
     *  @param auctionProcessorEnablers the result set
     *  @param auctionProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablers</code>
     *          or <code>auctionProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuctionProcessorEnablerSearchResults(org.osid.bidding.rules.AuctionProcessorEnablerList auctionProcessorEnablers,
                                            org.osid.bidding.rules.AuctionProcessorEnablerQueryInspector auctionProcessorEnablerQueryInspector) {
        nullarg(auctionProcessorEnablers, "auction processor enablers");
        nullarg(auctionProcessorEnablerQueryInspector, "auction processor enabler query inspectpr");

        this.auctionProcessorEnablers = auctionProcessorEnablers;
        this.inspector = auctionProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the auction processor enabler list resulting from a search.
     *
     *  @return an auction processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablers() {
        if (this.auctionProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.bidding.rules.AuctionProcessorEnablerList auctionProcessorEnablers = this.auctionProcessorEnablers;
        this.auctionProcessorEnablers = null;
	return (auctionProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.bidding.rules.AuctionProcessorEnablerQueryInspector getAuctionProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  auction processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve an auctionProcessorEnabler implementing the requested
     *  record.
     *
     *  @param auctionProcessorEnablerSearchRecordType an auctionProcessorEnabler search 
     *         record type 
     *  @return the auction processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(auctionProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorEnablerSearchResultsRecord getAuctionProcessorEnablerSearchResultsRecord(org.osid.type.Type auctionProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.bidding.rules.records.AuctionProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(auctionProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record auction processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuctionProcessorEnablerRecord(org.osid.bidding.rules.records.AuctionProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "auction processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
