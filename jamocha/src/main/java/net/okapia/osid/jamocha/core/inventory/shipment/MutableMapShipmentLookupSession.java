//
// MutableMapShipmentLookupSession
//
//    Implements a Shipment lookup service backed by a collection of
//    shipments that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.shipment;


/**
 *  Implements a Shipment lookup service backed by a collection of
 *  shipments. The shipments are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of shipments can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapShipmentLookupSession
    extends net.okapia.osid.jamocha.core.inventory.shipment.spi.AbstractMapShipmentLookupSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {


    /**
     *  Constructs a new {@code MutableMapShipmentLookupSession}
     *  with no shipments.
     *
     *  @param warehouse the warehouse
     *  @throws org.osid.NullArgumentException {@code warehouse} is
     *          {@code null}
     */

      public MutableMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse) {
        setWarehouse(warehouse);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapShipmentLookupSession} with a
     *  single shipment.
     *
     *  @param warehouse the warehouse  
     *  @param shipment a shipment
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code shipment} is {@code null}
     */

    public MutableMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                           org.osid.inventory.shipment.Shipment shipment) {
        this(warehouse);
        putShipment(shipment);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapShipmentLookupSession}
     *  using an array of shipments.
     *
     *  @param warehouse the warehouse
     *  @param shipments an array of shipments
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code shipments} is {@code null}
     */

    public MutableMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                           org.osid.inventory.shipment.Shipment[] shipments) {
        this(warehouse);
        putShipments(shipments);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapShipmentLookupSession}
     *  using a collection of shipments.
     *
     *  @param warehouse the warehouse
     *  @param shipments a collection of shipments
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code shipments} is {@code null}
     */

    public MutableMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                           java.util.Collection<? extends org.osid.inventory.shipment.Shipment> shipments) {

        this(warehouse);
        putShipments(shipments);
        return;
    }

    
    /**
     *  Makes a {@code Shipment} available in this session.
     *
     *  @param shipment a shipment
     *  @throws org.osid.NullArgumentException {@code shipment{@code  is
     *          {@code null}
     */

    @Override
    public void putShipment(org.osid.inventory.shipment.Shipment shipment) {
        super.putShipment(shipment);
        return;
    }


    /**
     *  Makes an array of shipments available in this session.
     *
     *  @param shipments an array of shipments
     *  @throws org.osid.NullArgumentException {@code shipments{@code 
     *          is {@code null}
     */

    @Override
    public void putShipments(org.osid.inventory.shipment.Shipment[] shipments) {
        super.putShipments(shipments);
        return;
    }


    /**
     *  Makes collection of shipments available in this session.
     *
     *  @param shipments a collection of shipments
     *  @throws org.osid.NullArgumentException {@code shipments{@code  is
     *          {@code null}
     */

    @Override
    public void putShipments(java.util.Collection<? extends org.osid.inventory.shipment.Shipment> shipments) {
        super.putShipments(shipments);
        return;
    }


    /**
     *  Removes a Shipment from this session.
     *
     *  @param shipmentId the {@code Id} of the shipment
     *  @throws org.osid.NullArgumentException {@code shipmentId{@code 
     *          is {@code null}
     */

    @Override
    public void removeShipment(org.osid.id.Id shipmentId) {
        super.removeShipment(shipmentId);
        return;
    }    
}
