//
// AbstractProxyCondition.java
//
//     Defines an AbstractProxyCondition.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.proxy.proxycondition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>ProxyCondition</code>.
 */

public abstract class AbstractProxyCondition
    extends net.okapia.osid.jamocha.spi.AbstractOsidCondition
    implements org.osid.proxy.ProxyCondition {

    private org.osid.id.Id agentId;
    private java.util.Date date;
    private java.math.BigDecimal rate;

    private org.osid.type.Type language;
    private org.osid.type.Type script;
    private org.osid.type.Type calendar;
    private org.osid.type.Type time;
    private org.osid.type.Type currency;
    private org.osid.type.Type units;
    private org.osid.type.Type format;

    private final java.util.Collection<org.osid.proxy.records.ProxyConditionRecord> records = new java.util.LinkedHashSet<>();
    
        
    /**
     *  Sets the effective agent <code> Id </code> to indicate acting on 
     *  behalf of. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setEffectiveAgentId(org.osid.id.Id agentId) {
        nullarg(agentId, "agent Id");
        this.agentId = agentId;
        return;
    }


    /**
     *  Gets the specified effective agent Id.
     *
     *  @return the effective agent Id or <code>null</code> if none
     *          specified
     */

    protected org.osid.id.Id getEffectiveAgentId() {
        return (this.agentId);
    }


    /**
     *  Sets the effective date. 
     *
     *  @param  date a date 
     *  @param  rate the rate at which the clock should tick from the given 
     *          effective date. 0 is a clock that is fixed 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void setEffectiveDate(java.util.Date date, java.math.BigDecimal rate) {
        nullarg(date, "date");
        nullarg(rate, "rate");

        this.date = date;
        this.rate = rate;
        return;
    }


    /**
     *  Gets the specified effective date.
     *
     *  @return the effective date or <code>null</code> if none
     *          specified
     */

    protected java.util.Date getEffectiveDate() {
        return (this.date);
    }


    /**
     *  Gets the specified effective clock rate.
     *
     *  @return the effective clock rate or <code>null</code> if none
     *          specified
     */

    protected java.math.BigDecimal getEffectiveClockRate() {
        return (this.rate);
    }


    /**
     *  Sets the language type. 
     *
     *  @param  languageType the language type 
     *  @throws org.osid.NullArgumentException <code> languageType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setLanguageType(org.osid.type.Type languageType) {
        nullarg(languageType, "language type");
        this.language = languageType;
        return;
    }


    /**
     *  Gets the specified language type.
     *
     *  @return the language type or <code>null</code> if none
     *          specified
     */

    protected org.osid.type.Type getLanguageType() {
        return (this.language);
    }


    /**
     *  Sets the script type. 
     *
     *  @param  scriptType the script type 
     *  @throws org.osid.NullArgumentException <code> scriptType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setScriptType(org.osid.type.Type scriptType) {
        nullarg(scriptType, "script type");
        this.script = scriptType;
        return;
    }


    /**
     *  Gets the specified script type.
     *
     *  @return the script type or <code>null</code> if none specified
     */

    protected org.osid.type.Type getScriptType() {
        return (this.script);
    }


    /**
     *  Sets the calendar type. 
     *
     *  @param  calendarType the calendar type 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setCalendarType(org.osid.type.Type calendarType) {
        nullarg(calendarType, "calendar type");
        this.calendar = calendarType;
        return;
    }


    /**
     *  Gets the specified calendar type.
     *
     *  @return the calendar type or <code>null</code> if none
     *          specified
     */

    protected org.osid.type.Type getCalendarType() {
        return (this.calendar);
    }


    /**
     *  Sets the time type. 
     *
     *  @param  timeType the time type 
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setTimeType(org.osid.type.Type timeType) {
        nullarg(timeType, "time type");
        this.time = timeType;
        return;
    }


    /**
     *  Gets the specified time type.
     *
     *  @return the time type or <code>null</code> if none specified
     */

    protected org.osid.type.Type getTimeType() {
        return (this.time);
    }


    /**
     *  Sets the currency type. 
     *
     *  @param  currencyType the currency type 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setCurrencyType(org.osid.type.Type currencyType) {
        nullarg(currencyType, "currency type");
        this.currency = currencyType;
        return;
    }


    /**
     *  Gets the specified currency type.
     *
     *  @return the currency type or <code>null</code> if none
     *          specified
     */

    protected org.osid.type.Type getCurrencyType() {
        return (this.currency);
    }


    /**
     *  Sets the unit system type. 
     *
     *  @param  unitSystemType the unit system type 
     *  @throws org.osid.NullArgumentException <code> unitSystemType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void setUnitSystemType(org.osid.type.Type unitSystemType) {
        nullarg(unitSystemType, "unit system type");
        this.units = unitSystemType;
        return;
    }


    /**
     *  Gets the specified unit system type.
     *
     *  @return the unit system type or <code>null</code>
     *          if none specified
     */

    protected org.osid.type.Type getUnitSystemType() {
        return (this.units);
    }


    /**
     *  Sets the <code> DisplayText </code> format type. 
     *
     *  @param  formatType the format type 
     *  @throws org.osid.NullArgumentException <code> formatType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setFormatType(org.osid.type.Type formatType) {
        nullarg(formatType, "format type");
        this.format = formatType;
        return;
    }


    /**
     *  Gets the <code> DisplayText </code> format type.
     *
     *  @return the format type
     */

    protected org.osid.type.Type getFormatType() {
        return (this.format);
    }


    /**
     *  Tests if this proxy condition supports the given
     *  record <code>Type</code>.
     *
     *  @param proxyConditionRecordType a proxy condition record type
     *  @return <code>true</code> if the proxyConditionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>proxyConditionRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type proxyConditionRecordType) {
        for (org.osid.proxy.records.ProxyConditionRecord record : this.records) {
            if (record.implementsRecordType(proxyConditionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ProxyCondition</code> record <code>Type</code>.
     *
     *  @param proxyConditionRecordType the proxy
     *         condition record type
     *  @return the proxy condition record 
     *  @throws org.osid.NullArgumentException
     *          <code>proxyConditionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proxyConditionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.proxy.records.ProxyConditionRecord getProxyConditionRecord(org.osid.type.Type proxyConditionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.proxy.records.ProxyConditionRecord record : this.records) {
            if (record.implementsRecordType(proxyConditionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proxyConditionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this proxy condition. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param proxyConditionRecord the proxy
     *         condition record
     *  @param proxyConditionRecordType proxy
     *         condition record type
     *  @throws org.osid.NullArgumentException
     *          <code>proxyConditionRecord</code> or
     *          <code>proxyConditionRecordTyp</code> is
     *          <code>null</code>
     */
            
    protected void addProxyConditionRecord(org.osid.proxy.records.ProxyConditionRecord proxyConditionRecord, 
                                                   org.osid.type.Type proxyConditionRecordType) {

        nullarg(proxyConditionRecord, "proxy condition record");
        addRecordType(proxyConditionRecordType);
        this.records.add(proxyConditionRecord);
        
        return;
    }
}
