//
// AbstractQueryCyclicTimePeriodLookupSession.java
//
//    An inline adapter that maps a CyclicTimePeriodLookupSession to
//    a CyclicTimePeriodQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CyclicTimePeriodLookupSession to
 *  a CyclicTimePeriodQuerySession.
 */

public abstract class AbstractQueryCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.calendaring.cycle.spi.AbstractCyclicTimePeriodLookupSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {

    private final org.osid.calendaring.cycle.CyclicTimePeriodQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCyclicTimePeriodLookupSession.
     *
     *  @param querySession the underlying cyclic time period query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCyclicTimePeriodLookupSession(org.osid.calendaring.cycle.CyclicTimePeriodQuerySession querySession) {
        nullarg(querySession, "cyclic time period query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>CyclicTimePeriod</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCyclicTimePeriods() {
        return (this.session.canSearchCyclicTimePeriods());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic time periods in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the <code>CyclicTimePeriod</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CyclicTimePeriod</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CyclicTimePeriod</code> and
     *  retained for compatibility.
     *
     *  @param  cyclicTimePeriodId <code>Id</code> of the
     *          <code>CyclicTimePeriod</code>
     *  @return the cyclic time period
     *  @throws org.osid.NotFoundException <code>cyclicTimePeriodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriod getCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicTimePeriodQuery query = getQuery();
        query.matchId(cyclicTimePeriodId, true);
        org.osid.calendaring.cycle.CyclicTimePeriodList cyclicTimePeriods = this.session.getCyclicTimePeriodsByQuery(query);
        if (cyclicTimePeriods.hasNext()) {
            return (cyclicTimePeriods.getNextCyclicTimePeriod());
        } 
        
        throw new org.osid.NotFoundException(cyclicTimePeriodId + " not found");
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicTimePeriods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CyclicTimePeriods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  cyclicTimePeriodIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByIds(org.osid.id.IdList cyclicTimePeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicTimePeriodQuery query = getQuery();

        try (org.osid.id.IdList ids = cyclicTimePeriodIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCyclicTimePeriodsByQuery(query));
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  cyclic time period genus <code>Type</code> which does not include
     *  cyclic time periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicTimePeriodQuery query = getQuery();
        query.matchGenusType(cyclicTimePeriodGenusType, true);
        return (this.session.getCyclicTimePeriodsByQuery(query));
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  cyclic time period genus <code>Type</code> and include any additional
     *  cyclic time periods with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByParentGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicTimePeriodQuery query = getQuery();
        query.matchParentGenusType(cyclicTimePeriodGenusType, true);
        return (this.session.getCyclicTimePeriodsByQuery(query));
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> containing the given
     *  cyclic time period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodRecordType a cyclicTimePeriod record type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByRecordType(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicTimePeriodQuery query = getQuery();
        query.matchRecordType(cyclicTimePeriodRecordType, true);
        return (this.session.getCyclicTimePeriodsByQuery(query));
    }

    
    /**
     *  Gets all <code>CyclicTimePeriods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>CyclicTimePeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.cycle.CyclicTimePeriodQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCyclicTimePeriodsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.cycle.CyclicTimePeriodQuery getQuery() {
        org.osid.calendaring.cycle.CyclicTimePeriodQuery query = this.session.getCyclicTimePeriodQuery();
        return (query);
    }
}
