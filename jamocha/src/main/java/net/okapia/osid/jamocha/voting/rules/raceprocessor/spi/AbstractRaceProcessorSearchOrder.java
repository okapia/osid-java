//
// AbstractRaceProcessorSearchOdrer.java
//
//     Defines a RaceProcessorSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code RaceProcessorSearchOrder}.
 */

public abstract class AbstractRaceProcessorSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorSearchOrder
    implements org.osid.voting.rules.RaceProcessorSearchOrder {

    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering the maximum winners value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumWinners(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering the minimum percentage to win 
     *  value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumPercentageToWin(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering the minimum votes to win value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumVotesToWin(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  raceProcessorRecordType a race processor record type 
     *  @return {@code true} if the raceProcessorRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceProcessorRecordType) {
        for (org.osid.voting.rules.records.RaceProcessorSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  raceProcessorRecordType the race processor record type 
     *  @return the race processor search order record
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(raceProcessorRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorSearchOrderRecord getRaceProcessorSearchOrderRecord(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceProcessorSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this race processor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceProcessorRecord the race processor search odrer record
     *  @param raceProcessorRecordType race processor record type
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorRecord} or
     *          {@code raceProcessorRecordTyperaceProcessor} is
     *          {@code null}
     */
            
    protected void addRaceProcessorRecord(org.osid.voting.rules.records.RaceProcessorSearchOrderRecord raceProcessorSearchOrderRecord, 
                                     org.osid.type.Type raceProcessorRecordType) {

        addRecordType(raceProcessorRecordType);
        this.records.add(raceProcessorSearchOrderRecord);
        
        return;
    }
}
