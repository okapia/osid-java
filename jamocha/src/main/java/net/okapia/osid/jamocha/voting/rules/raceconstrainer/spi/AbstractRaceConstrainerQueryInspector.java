//
// AbstractRaceConstrainerQueryInspector.java
//
//     A template for making a RaceConstrainerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for race constrainers.
 */

public abstract class AbstractRaceConstrainerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQueryInspector
    implements org.osid.voting.rules.RaceConstrainerQueryInspector {

    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the race <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRaceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the race query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.RaceQueryInspector[] getRuledRaceTerms() {
        return (new org.osid.voting.RaceQueryInspector[0]);
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given race constrainer query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a race constrainer implementing the requested record.
     *
     *  @param raceConstrainerRecordType a race constrainer record type
     *  @return the race constrainer query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord getRaceConstrainerQueryInspectorRecord(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(raceConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race constrainer query. 
     *
     *  @param raceConstrainerQueryInspectorRecord race constrainer query inspector
     *         record
     *  @param raceConstrainerRecordType raceConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceConstrainerQueryInspectorRecord(org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord raceConstrainerQueryInspectorRecord, 
                                                   org.osid.type.Type raceConstrainerRecordType) {

        addRecordType(raceConstrainerRecordType);
        nullarg(raceConstrainerRecordType, "race constrainer record type");
        this.records.add(raceConstrainerQueryInspectorRecord);        
        return;
    }
}
