//
// AbstractPersonnelBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractPersonnelBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.personnel.batch.PersonnelBatchManager,
               org.osid.personnel.batch.PersonnelBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractPersonnelBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractPersonnelBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of persons is available. 
     *
     *  @return <code> true </code> if a person bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of organizations is available. 
     *
     *  @return <code> true </code> if an organization bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of positions is available. 
     *
     *  @return <code> true </code> if a position bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of appointments is available. 
     *
     *  @return <code> true </code> if an appointment bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of realms is available. 
     *
     *  @return <code> true </code> if a realm bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk person 
     *  administration service. 
     *
     *  @return a <code> PersonBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonBatchAdminSession getPersonBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getPersonBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk person 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonBatchAdminSession getPersonBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getPersonBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk person 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PersonBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonBatchAdminSession getPersonBatchAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getPersonBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk person 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonBatchAdminSession getPersonBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getPersonBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  organization administration service. 
     *
     *  @return an <code> OrganizationBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.OrganizationBatchAdminSession getOrganizationBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getOrganizationBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  organization administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.OrganizationBatchAdminSession getOrganizationBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getOrganizationBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  organization administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> OrganizationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.OrganizationBatchAdminSession getOrganizationBatchAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getOrganizationBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  organization administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.OrganizationBatchAdminSession getOrganizationBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getOrganizationBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk position 
     *  administration service. 
     *
     *  @return a <code> PositionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PositionBatchAdminSession getPositionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getPositionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk position 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PositionBatchAdminSession getPositionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getPositionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk position 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return a <code> PositionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PositionBatchAdminSession getPositionBatchAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getPositionBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk position 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PositionBatchAdminSession getPositionBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getPositionBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  appointment administration service. 
     *
     *  @return an <code> AppointmentBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.AppointmentBatchAdminSession getAppointmentBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getAppointmentBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  appointment administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.AppointmentBatchAdminSession getAppointmentBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getAppointmentBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  appointment administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @return an <code> AppointmentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.AppointmentBatchAdminSession getAppointmentBatchAdminSessionForRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getAppointmentBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  appointment administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.AppointmentBatchAdminSession getAppointmentBatchAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getAppointmentBatchAdminSessionForRealm not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk realm 
     *  administration service. 
     *
     *  @return a <code> RealmBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.RealmBatchAdminSession getRealmBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchManager.getRealmBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk realm 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.RealmBatchAdminSession getRealmBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.personnel.batch.PersonnelBatchProxyManager.getRealmBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
