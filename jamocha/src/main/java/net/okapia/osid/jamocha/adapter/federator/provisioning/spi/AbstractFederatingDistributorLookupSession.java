//
// AbstractFederatingDistributorLookupSession.java
//
//     An abstract federating adapter for a DistributorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  DistributorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingDistributorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.DistributorLookupSession>
    implements org.osid.provisioning.DistributorLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingDistributorLookupSession</code>.
     */

    protected AbstractFederatingDistributorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.DistributorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Distributor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDistributors() {
        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            if (session.canLookupDistributors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Distributor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDistributorView() {
        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            session.useComparativeDistributorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Distributor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDistributorView() {
        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            session.usePlenaryDistributorView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Distributor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Distributor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Distributor</code> and
     *  retained for compatibility.
     *
     *  @param  distributorId <code>Id</code> of the
     *          <code>Distributor</code>
     *  @return the distributor
     *  @throws org.osid.NotFoundException <code>distributorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>distributorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            try {
                return (session.getDistributor(distributorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(distributorId + " not found");
    }


    /**
     *  Gets a <code>DistributorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  distributors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Distributors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  distributorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>distributorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByIds(org.osid.id.IdList distributorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.distributor.MutableDistributorList ret = new net.okapia.osid.jamocha.provisioning.distributor.MutableDistributorList();

        try (org.osid.id.IdList ids = distributorIds) {
            while (ids.hasNext()) {
                ret.addDistributor(getDistributor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>DistributorList</code> corresponding to the given
     *  distributor genus <code>Type</code> which does not include
     *  distributors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  distributorGenusType a distributor genus type 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByGenusType(org.osid.type.Type distributorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.FederatingDistributorList ret = getDistributorList();

        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            ret.addDistributorList(session.getDistributorsByGenusType(distributorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DistributorList</code> corresponding to the given
     *  distributor genus <code>Type</code> and include any additional
     *  distributors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  distributorGenusType a distributor genus type 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByParentGenusType(org.osid.type.Type distributorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.FederatingDistributorList ret = getDistributorList();

        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            ret.addDistributorList(session.getDistributorsByParentGenusType(distributorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DistributorList</code> containing the given
     *  distributor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  distributorRecordType a distributor record type 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByRecordType(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.FederatingDistributorList ret = getDistributorList();

        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            ret.addDistributorList(session.getDistributorsByRecordType(distributorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DistributorList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known distributors or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  distributors that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Distributor</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.FederatingDistributorList ret = getDistributorList();

        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            ret.addDistributorList(session.getDistributorsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Distributors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Distributors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.FederatingDistributorList ret = getDistributorList();

        for (org.osid.provisioning.DistributorLookupSession session : getSessions()) {
            ret.addDistributorList(session.getDistributors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.FederatingDistributorList getDistributorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.ParallelDistributorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.distributor.CompositeDistributorList());
        }
    }
}
