//
// PagingCreditBatchFormList
//
//     Implements a paging CreditBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 June 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.acknowledgement.batch.creditbatchform;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a paging CreditBatchFormList. This list can be used to honor
 *  parameters set in search interfaces where the provider doesn't
 *  support poging. 
 *
 *  The <code>skip()</code> method is used to move to the desired
 *  starting position.
 */

public final class PagingCreditBatchFormList
    extends net.okapia.osid.jamocha.acknowledgement.batch.creditbatchform.spi.AbstractCreditBatchFormList
    implements org.osid.acknowledgement.batch.CreditBatchFormList {

    private long index = 0;
    private long end   = 0;
    private org.osid.acknowledgement.batch.CreditBatchFormList list;
    

    /**
     *  Creates a new <code>PagingCreditBatchFormList</code>.
     *
     *  @param creditBatchFormList actviity <code>CreditBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>creditBatchFormList</code> is <code>null</code>
     */

    public PagingCreditBatchFormList(org.osid.acknowledgement.batch.CreditBatchFormList creditBatchFormList, long start, long end) {
        nullarg(creditBatchFormList, "creditBatchForm list");
        cardinalarg(start, "start");
        cardinalarg(end, "end");

        this.list = creditBatchFormList;
        this.end  = end;
        this.list.skip(start);
        
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.index >= this.end) {
            return (false);
        }

        return (this.list.hasNext());
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        long n = this.list.available();
        if (n > (this.index - this.end)) {
            return (this.index - this.end);
        }

        return (n);
    }


    /**
     *  Skip the specified number of elements in the list. If the
     *  number skipped is greater than the number of elements in the
     *  list, hasNext() becomes false and available() returns zero as
     *  there are no more elements to retrieve.
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        this.list.skip(n);
        this.index += n;
        return;
    }


    /**
     *  Gets the next <code> CreditBatchForm </code> in this list. 
     *
     *  @return the next <code> CreditBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CreditBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchForm getNextCreditBatchForm()
        throws org.osid.OperationFailedException {

        if (!hasNext()) {
            throw new org.osid.IllegalStateException("no more elements available in creditBatchForm list");
        }

        org.osid.acknowledgement.batch.CreditBatchForm creditBatchForm = this.list.getNextCreditBatchForm();
        ++this.index;
        return (creditBatchForm);
    }

        
    /**
     *  Gets the next set of <code> CreditBatchForm </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param  n the number of <code> CreditBatchForm </code> elements requested which 
     *          must be less than or equal to <code> available() </code> 
     *  @return an array of <code> CreditBatchForm </code> elements. <code> </code> The 
     *          length of the array is less than or equal to the number 
     *          specified. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.acknowledgement.batch.CreditBatchForm[] getNextCreditBatchForms(long n)
        throws org.osid.OperationFailedException {

        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        org.osid.acknowledgement.batch.CreditBatchForm[] ret = this.list.getNextCreditBatchForms(n);
        this.index += n;
        return (ret);
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.list.close();
        return;
    }
}
