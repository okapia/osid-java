//
// AbstractQueryVaultLookupSession.java
//
//    An inline adapter that maps a VaultLookupSession to
//    a VaultQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a VaultLookupSession to
 *  a VaultQuerySession.
 */

public abstract class AbstractQueryVaultLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractVaultLookupSession
    implements org.osid.authorization.VaultLookupSession {

    private final org.osid.authorization.VaultQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryVaultLookupSession.
     *
     *  @param querySession the underlying vault query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryVaultLookupSession(org.osid.authorization.VaultQuerySession querySession) {
        nullarg(querySession, "vault query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Vault</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupVaults() {
        return (this.session.canSearchVaults());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Vault</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Vault</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Vault</code> and
     *  retained for compatibility.
     *
     *  @param  vaultId <code>Id</code> of the
     *          <code>Vault</code>
     *  @return the vault
     *  @throws org.osid.NotFoundException <code>vaultId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>vaultId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.VaultQuery query = getQuery();
        query.matchId(vaultId, true);
        org.osid.authorization.VaultList vaults = this.session.getVaultsByQuery(query);
        if (vaults.hasNext()) {
            return (vaults.getNextVault());
        } 
        
        throw new org.osid.NotFoundException(vaultId + " not found");
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  vaults specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Vaults</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  vaultIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>vaultIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByIds(org.osid.id.IdList vaultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.VaultQuery query = getQuery();

        try (org.osid.id.IdList ids = vaultIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getVaultsByQuery(query));
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  vault genus <code>Type</code> which does not include
     *  vaults of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.VaultQuery query = getQuery();
        query.matchGenusType(vaultGenusType, true);
        return (this.session.getVaultsByQuery(query));
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  vault genus <code>Type</code> and include any additional
     *  vaults with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByParentGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.VaultQuery query = getQuery();
        query.matchParentGenusType(vaultGenusType, true);
        return (this.session.getVaultsByQuery(query));
    }


    /**
     *  Gets a <code>VaultList</code> containing the given
     *  vault record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  vaultRecordType a vault record type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByRecordType(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.VaultQuery query = getQuery();
        query.matchRecordType(vaultRecordType, true);
        return (this.session.getVaultsByQuery(query));
    }


    /**
     *  Gets a <code>VaultList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known vaults or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  vaults that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Vault</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.VaultQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getVaultsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Vaults</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Vaults</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.VaultQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getVaultsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authorization.VaultQuery getQuery() {
        org.osid.authorization.VaultQuery query = this.session.getVaultQuery();
        return (query);
    }
}
