//
// InvariantMapAssessmentOfferedLookupSession
//
//    Implements an AssessmentOffered lookup service backed by a fixed collection of
//    assessmentsOffered.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an AssessmentOffered lookup service backed by a fixed
 *  collection of assessments offered. The assessments offered are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAssessmentOfferedLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractMapAssessmentOfferedLookupSession
    implements org.osid.assessment.AssessmentOfferedLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentOfferedLookupSession</code> with no
     *  assessments offered.
     *  
     *  @param bank the bank
     *  @throws org.osid.NullArgumnetException {@code bank} is
     *          {@code null}
     */

    public InvariantMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentOfferedLookupSession</code> with a single
     *  assessment offered.
     *  
     *  @param bank the bank
     *  @param assessmentOffered an single assessment offered
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentOffered} is <code>null</code>
     */

      public InvariantMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.AssessmentOffered assessmentOffered) {
        this(bank);
        putAssessmentOffered(assessmentOffered);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentOfferedLookupSession</code> using an array
     *  of assessments offered.
     *  
     *  @param bank the bank
     *  @param assessmentsOffered an array of assessments offered
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsOffered} is <code>null</code>
     */

      public InvariantMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.AssessmentOffered[] assessmentsOffered) {
        this(bank);
        putAssessmentsOffered(assessmentsOffered);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentOfferedLookupSession</code> using a
     *  collection of assessments offered.
     *
     *  @param bank the bank
     *  @param assessmentsOffered a collection of assessments offered
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsOffered} is <code>null</code>
     */

      public InvariantMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank,
                                               java.util.Collection<? extends org.osid.assessment.AssessmentOffered> assessmentsOffered) {
        this(bank);
        putAssessmentsOffered(assessmentsOffered);
        return;
    }
}
