//
// AbstractJournalEntryLookupSession.java
//
//    A starter implementation framework for providing a JournalEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a JournalEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getJournalEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractJournalEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.journaling.JournalEntryLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.journaling.Journal journal = new net.okapia.osid.jamocha.nil.journaling.journal.UnknownJournal();
    

    /**
     *  Gets the <code>Journal/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Journal Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.journal.getId());
    }


    /**
     *  Gets the <code>Journal</code> associated with this 
     *  session.
     *
     *  @return the <code>Journal</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.journal);
    }


    /**
     *  Sets the <code>Journal</code>.
     *
     *  @param  journal the journal for this session
     *  @throws org.osid.NullArgumentException <code>journal</code>
     *          is <code>null</code>
     */

    protected void setJournal(org.osid.journaling.Journal journal) {
        nullarg(journal, "journal");
        this.journal = journal;
        return;
    }


    /**
     *  Tests if this user can perform <code>JournalEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canReadJournal() {
        return (true);
    }


    /**
     *  A complete view of the <code>JournalEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJournalEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>JournalEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJournalEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include journal entries in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>JournalEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JournalEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>JournalEntry</code> and
     *  retained for compatibility.
     *
     *  @param  journalEntryId <code>Id</code> of the
     *          <code>JournalEntry</code>
     *  @return the journal entry
     *  @throws org.osid.NotFoundException <code>journalEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>journalEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry getJournalEntry(org.osid.id.Id journalEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.journaling.JournalEntryList journalEntries = getJournalEntries()) {
            while (journalEntries.hasNext()) {
                org.osid.journaling.JournalEntry journalEntry = journalEntries.getNextJournalEntry();
                if (journalEntry.getId().equals(journalEntryId)) {
                    return (journalEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(journalEntryId + " not found");
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journalEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>JournalEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getJournalEntries()</code>.
     *
     *  @param  journalEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByIds(org.osid.id.IdList journalEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.journaling.JournalEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = journalEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getJournalEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("journal entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.journaling.journalentry.LinkedJournalEntryList(ret));
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  journal entry genus <code>Type</code> which does not include
     *  journal entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getJournalEntries()</code>.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryGenusFilterList(getJournalEntries(), journalEntryGenusType));
    }


    /**
     *  Gets a <code>JournalEntryList</code> corresponding to the given
     *  journal entry genus <code>Type</code> and include any additional
     *  journal entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJournalEntries()</code>.
     *
     *  @param  journalEntryGenusType a journalEntry genus type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByParentGenusType(org.osid.type.Type journalEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getJournalEntriesByGenusType(journalEntryGenusType));
    }


    /**
     *  Gets a <code>JournalEntryList</code> containing the given
     *  journal entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJournalEntries()</code>.
     *
     *  @param  journalEntryRecordType a journalEntry record type 
     *  @return the returned <code>JournalEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByRecordType(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryRecordFilterList(getJournalEntries(), journalEntryRecordType));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch
     *  <code> Id</code>. In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  branchId the <code>Id</code> of the branch 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.NullArgumentException <code>branchId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranch(org.osid.id.Id branchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new BranchFilter(branchId), getJournalEntries()));
    }        


    /**
     *  Gets the journal entry corresponding to a resource
     *  <code>Id</code> and date. The entries returned have a
     *  date equal to or more recent than the requested date. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId the <code>Id</code> of the branch 
     *  @param  date from date 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.NullArgumentException <code>branchId</code>
     *          or <code>date</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranch(org.osid.id.Id branchId, 
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new StartDateFilter(date), getJournalEntriesForBranch(branchId)));        
    }
    

    /**
     *  Gets a list of journal entries corresponding to a branch
     *  <code>Id</code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results.  Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId the <code> Id </code> of the branch 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> branchId, from </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranch(org.osid.id.Id branchId, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new EndDateFilter(to), getJournalEntriesByDateForBranch(branchId, from)));
    }        


    /**
     *  Gets a list of journal entries corresponding to a source
     *  <code> Id</code>. In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  sourceId the <code>Id</code> of the source 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new SourceFilter(sourceId), getJournalEntries()));
    }        


    /**
     *  Gets the journal entry corresponding to a resource <code> Id
     *  </code> and date. The entry returned has a date equal to or
     *  more recent than the requested date. In plenary mode, the
     *  returned list contains all known journal entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journal entries that are accessible through this session.
     *
     *  @param  sourceId the <code> Id </code> of the source 
     *  @param  date from date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> or 
     *          <code> date </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForSource(org.osid.id.Id sourceId, 
                                                                                 org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new StartDateFilter(date), getJournalEntriesForSource(sourceId)));
    }
    

    /**
     *  Gets a list of journal entries corresponding to a source
     *  <code>Id</code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results.  Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  sourceId the <code> Id </code> of the source 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> sourceId, from </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForSource(org.osid.id.Id sourceId, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new EndDateFilter(to), getJournalEntriesByDateForSource(sourceId, from)));        
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source <code>Id</code>. A source <code>Id</code> of any
     *  version may be requested. 
     * 
     *  In plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId the <code> Id </code> of the branch 
     *  @param  sourceId the <code> Id </code> of the source 
     *  @return the returned <code> JournalEntryList </code> 
     *  @throws org.osid.NullArgumentException <code> branchId </code> or 
     *          <code> sourceId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForBranchAndSource(org.osid.id.Id branchId, 
                                                                                    org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new BranchFilter(branchId), getJournalEntriesForSource(sourceId)));        
    }


    /**
     *  Gets the journal entry corresponding to a branch and source
     *  <code>Id</code> and date. The entry returned has a date equal
     *  to or more recent than the requested date. The
     *  <code>sourceId</code> may correspond to any version. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  branchId a branch <code>Id</code> 
     *  @param  sourceId the <code>Id</code> of the source 
     *  @param  date from date 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.NullArgumentException <code>branchId</code>,
     *          <code>sourceId</code> or <code>date</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranchAndSource(org.osid.id.Id branchId, 
                                                                                                   org.osid.id.Id sourceId, 
                                                                                                   org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new StartDateFilter(date), getJournalEntriesForBranchAndSource(branchId, sourceId)));
    }


    /**
     *  Gets a list of journal entries corresponding to a branch and
     *  source <code>Id</code> and date range. Entries are returned
     *  with dates that fall between the requested dates
     *  inclusive. The <code>sourceId</code> may correspond to any
     *  version In plenary mode, the returned list contains all known
     *  journal entries or an error results.  Otherwise, the returned
     *  list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  branchId a branch <code>Id</code> 
     *  @param  sourceId the <code>Id</code> of the source 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is less 
     *          than <code>from</code> 
     *  @throws org.osid.NullArgumentException <code>branchId<code>,
     *          <code>sourceId</code>, <code>from </code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForBranchAndSource(org.osid.id.Id branchId, 
                                                                                               org.osid.id.Id sourceId, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new EndDateFilter(from), getJournalEntriesByDateForBranchAndSource(branchId, sourceId, to)));
    }


    /**
     *  Gets a list of journal entries corresponding to a resource
     *  <code>Id</code>. In plenary mode, the returned list contains
     *  all known journal entries or an error results. Otherwise, the
     *  returned list may contain only those journal entries that are
     *  accessible through this session.
     *
     *  @param  resourceId the <code>Id</code> of the resource 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new ResourceFilter(resourceId), getJournalEntries()));
    }        


    /**
     *  Gets the journal entry corresponding to a resource <code>Id</code> 
     *  and date. The entry returned has a date equal to or more recent than 
     *  the requested date. In plenary mode, the returned list contains all 
     *  known journal entries or an error results. Otherwise, the returned 
     *  list may contain only those journal entries that are accessible 
     *  through this session. 
     *
     *  @param  resourceId the <code>Id</code> of the resource 
     *  @param  date from date 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>date</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateForResource(org.osid.id.Id resourceId, 
                                                                                   org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new StartDateFilter(date), getJournalEntriesForResource(resourceId)));
    }


    /**
     *  Gets a list of journal entries corresponding to a resource
     *  <code>Id</code> and date range. Entries are returned with
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known journal
     *  entries or an error results.  Otherwise, the returned list may
     *  contain only those journal entries that are accessible through
     *  this session.
     *
     *  @param  resourceId the <code>Id</code> of the resource 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code>JournalEntryList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByDateRangeForResource(org.osid.id.Id resourceId, 
                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilterList(new EndDateFilter(from), getJournalEntriesByDateForResource(resourceId, to)));
    }


    /**
     *  Gets all <code>JournalEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  journal entries or an error results. Otherwise, the returned list
     *  may contain only those journal entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>JournalEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.journaling.JournalEntryList getJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the journal entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of journal entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.journaling.JournalEntryList filterJournalEntriesOnViews(org.osid.journaling.JournalEntryList list)
        throws org.osid.OperationFailedException {

        return (list);
    }

    
    public static class BranchFilter
        implements net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilter {
         
        private final org.osid.id.Id branchId;
         
         
        /**
         *  Constructs a new <code>BranchFilter</code>.
         *
         *  @param branchId the branch to filter
         *  @throws org.osid.NullArgumentException
         *          <code>branchId</code> is <code>null</code>
         */
        
        public BranchFilter(org.osid.id.Id branchId) {
            nullarg(branchId, "branch Id");
            this.branchId = branchId;
            return;
        }

         
        /**
         *  Used by the JournalEntryFilterList to filter the 
         *  journal entry list based on branch.
         *
         *  @param journalEntry the journal entry
         *  @return <code>true</code> to pass the journal entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.journaling.JournalEntry journalEntry) {
            return (journalEntry.getBranchId().equals(this.branchId));
        }
    }


    public static class SourceFilter
        implements net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilter {
         
        private final org.osid.id.Id sourceId;
         
         
        /**
         *  Constructs a new <code>SourceFilter</code>.
         *
         *  @param sourceId the source to filter
         *  @throws org.osid.NullArgumentException
         *          <code>sourceId</code> is <code>null</code>
         */
        
        public SourceFilter(org.osid.id.Id sourceId) {
            nullarg(sourceId, "source Id");
            this.sourceId = sourceId;
            return;
        }

         
        /**
         *  Used by the JournalEntryFilterList to filter the 
         *  journal entry list based on source.
         *
         *  @param journalEntry the journal entry
         *  @return <code>true</code> to pass the journal entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.journaling.JournalEntry journalEntry) {
            return (journalEntry.getSourceId().equals(this.sourceId));
        }
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the JournalEntryFilterList to filter the 
         *  journal entry list based on resource.
         *
         *  @param journalEntry the journal entry
         *  @return <code>true</code> to pass the journal entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.journaling.JournalEntry journalEntry) {
            return (journalEntry.getResourceId().equals(this.resourceId));
        }
    }


    public static class StartDateFilter
        implements net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilter {
         
        private final org.osid.calendaring.DateTime date;
         
         
        /**
         *  Constructs a new <code>StartDateFilter</code>.
         *
         *  @param date the date
         *  @throws org.osid.NullArgumentException <code>date</code>
         *          is <code>null</code>
         */
        
        public StartDateFilter(org.osid.calendaring.DateTime date) {
            nullarg(date, "date");
            this.date = date;
            return;
        }

         
        /**
         *  Used by the JournalEntryFilterList to filter the 
         *  journal entry list based on start date.
         *
         *  @param journalEntry the journal entry
         *  @return <code>true</code> to pass the journal entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.journaling.JournalEntry journalEntry) {
            return (!journalEntry.getTimestamp().isLess(this.date));
        }
    }


    public static class EndDateFilter
        implements net.okapia.osid.jamocha.inline.filter.journaling.journalentry.JournalEntryFilter {
         
        private final org.osid.calendaring.DateTime date;
         
         
        /**
         *  Constructs a new <code>EndDateFilter</code>.
         *
         *  @param date the date
         *  @throws org.osid.NullArgumentException <code>date</code>
         *          is <code>null</code>
         */
        
        public EndDateFilter(org.osid.calendaring.DateTime date) {
            nullarg(date, "date");
            this.date = date;
            return;
        }

         
        /**
         *  Used by the JournalEntryFilterList to filter the 
         *  journal entry list based on end date.
         *
         *  @param journalEntry the journal entry
         *  @return <code>true</code> to pass the journal entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.journaling.JournalEntry journalEntry) {
            return (!journalEntry.getTimestamp().isGreater(this.date));
        }
    }
}
