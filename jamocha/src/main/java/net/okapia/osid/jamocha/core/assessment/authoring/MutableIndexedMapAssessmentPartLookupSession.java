//
// MutableIndexedMapAssessmentPartLookupSession
//
//    Implements an AssessmentPart lookup service backed by a collection of
//    assessmentParts indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements an AssessmentPart lookup service backed by a collection of
 *  assessment parts. The assessment parts are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some assessment parts may be compatible
 *  with more types than are indicated through these assessment part
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of assessment parts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractIndexedMapAssessmentPartLookupSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAssessmentPartLookupSession} with no assessment parts.
     *
     *  @param bank the bank
     *  @throws org.osid.NullArgumentException {@code bank}
     *          is {@code null}
     */

      public MutableIndexedMapAssessmentPartLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentPartLookupSession} with a
     *  single assessment part.
     *  
     *  @param bank the bank
     *  @param  assessmentPart an single assessmentPart
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentPart} is {@code null}
     */

    public MutableIndexedMapAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        this(bank);
        putAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentPartLookupSession} using an
     *  array of assessment parts.
     *
     *  @param bank the bank
     *  @param  assessmentParts an array of assessment parts
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentParts} is {@code null}
     */

    public MutableIndexedMapAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.authoring.AssessmentPart[] assessmentParts) {
        this(bank);
        putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentPartLookupSession} using a
     *  collection of assessment parts.
     *
     *  @param bank the bank
     *  @param  assessmentParts a collection of assessment parts
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentParts} is {@code null}
     */

    public MutableIndexedMapAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                  java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts) {

        this(bank);
        putAssessmentParts(assessmentParts);
        return;
    }
    

    /**
     *  Makes an {@code AssessmentPart} available in this session.
     *
     *  @param  assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException {@code assessmentPart}  is
     *          {@code null}
     */

    @Override
    public void putAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        super.putAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Makes an array of assessment parts available in this session.
     *
     *  @param  assessmentParts an array of assessment parts
     *  @throws org.osid.NullArgumentException {@code assessmentParts}
     *          is {@code null}
     */

    @Override
    public void putAssessmentParts(org.osid.assessment.authoring.AssessmentPart[] assessmentParts) {
        super.putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Makes collection of assessment parts available in this session.
     *
     *  @param  assessmentParts a collection of assessment parts
     *  @throws org.osid.NullArgumentException {@code assessmentPart}  is
     *          {@code null}
     */

    @Override
    public void putAssessmentParts(java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts) {
        super.putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Removes an AssessmentPart from this session.
     *
     *  @param assessmentPartId the {@code Id} of the assessment part
     *  @throws org.osid.NullArgumentException {@code assessmentPartId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAssessmentPart(org.osid.id.Id assessmentPartId) {
        super.removeAssessmentPart(assessmentPartId);
        return;
    }    
}
