//
// AbstractRecurringEventEnablerLookupSession.java
//
//    A starter implementation framework for providing a RecurringEventEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a RecurringEventEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRecurringEventEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRecurringEventEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.rules.RecurringEventEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>RecurringEventEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRecurringEventEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>RecurringEventEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRecurringEventEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>RecurringEventEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRecurringEventEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recurring event enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active recurring event enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRecurringEventEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive recurring event enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRecurringEventEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RecurringEventEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RecurringEventEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RecurringEventEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerId <code>Id</code> of the
     *          <code>RecurringEventEnabler</code>
     *  @return the recurring event enabler
     *  @throws org.osid.NotFoundException <code>recurringEventEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnabler getRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.rules.RecurringEventEnablerList recurringEventEnablers = getRecurringEventEnablers()) {
            while (recurringEventEnablers.hasNext()) {
                org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler = recurringEventEnablers.getNextRecurringEventEnabler();
                if (recurringEventEnabler.getId().equals(recurringEventEnablerId)) {
                    return (recurringEventEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(recurringEventEnablerId + " not found");
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recurringEventEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RecurringEventEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRecurringEventEnablers()</code>.
     *
     *  @param  recurringEventEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByIds(org.osid.id.IdList recurringEventEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.rules.RecurringEventEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = recurringEventEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRecurringEventEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("recurring event enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.LinkedRecurringEventEnablerList(ret));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> corresponding to the given
     *  recurring event enabler genus <code>Type</code> which does not include
     *  recurring event enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRecurringEventEnablers()</code>.
     *
     *  @param  recurringEventEnablerGenusType a recurringEventEnabler genus type 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByGenusType(org.osid.type.Type recurringEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.rules.recurringeventenabler.RecurringEventEnablerGenusFilterList(getRecurringEventEnablers(), recurringEventEnablerGenusType));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> corresponding to the given
     *  recurring event enabler genus <code>Type</code> and include any additional
     *  recurring event enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRecurringEventEnablers()</code>.
     *
     *  @param  recurringEventEnablerGenusType a recurringEventEnabler genus type 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByParentGenusType(org.osid.type.Type recurringEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRecurringEventEnablersByGenusType(recurringEventEnablerGenusType));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> containing the given
     *  recurring event enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRecurringEventEnablers()</code>.
     *
     *  @param  recurringEventEnablerRecordType a recurringEventEnabler record type 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByRecordType(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.rules.recurringeventenabler.RecurringEventEnablerRecordFilterList(getRecurringEventEnablers(), recurringEventEnablerRecordType));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible
     *  through this session.
     *  
     *  In active mode, recurring event enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive recurring event enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RecurringEventEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.rules.recurringeventenabler.TemporalRecurringEventEnablerFilterList(getRecurringEventEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>RecurringEventEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible
     *  through this session.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive recurring event enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getRecurringEventEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>RecurringEventEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @return a list of <code>RecurringEventEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the recurring event enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of recurring event enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.rules.RecurringEventEnablerList filterRecurringEventEnablersOnViews(org.osid.calendaring.rules.RecurringEventEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.calendaring.rules.RecurringEventEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.rules.recurringeventenabler.ActiveRecurringEventEnablerFilterList(ret);
        }

        return (ret);
    }
}
