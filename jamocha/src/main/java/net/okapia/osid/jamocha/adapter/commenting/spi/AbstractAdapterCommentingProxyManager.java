//
// AbstractCommentingProxyManager.java
//
//     An adapter for a CommentingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CommentingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCommentingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.commenting.CommentingProxyManager>
    implements org.osid.commenting.CommentingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCommentingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCommentingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCommentingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCommentingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any book federation is exposed. Federation is exposed when a 
     *  specific book may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of books 
     *  appears as a single book. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a comment lookup service. 
     *
     *  @return <code> true </code> if comment lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentLookup() {
        return (getAdapteeManager().supportsCommentLookup());
    }


    /**
     *  Tests for the availability of a rating lookup service. 
     *
     *  @return <code> true </code> if rating lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRatingLookup() {
        return (getAdapteeManager().supportsRatingLookup());
    }


    /**
     *  Tests if querying comments is available. 
     *
     *  @return <code> true </code> if comment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentQuery() {
        return (getAdapteeManager().supportsCommentQuery());
    }


    /**
     *  Tests if searching for comments is available. 
     *
     *  @return <code> true </code> if comment search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentSearch() {
        return (getAdapteeManager().supportsCommentSearch());
    }


    /**
     *  Tests if managing comments is available. 
     *
     *  @return <code> true </code> if comment admin is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentAdmin() {
        return (getAdapteeManager().supportsCommentAdmin());
    }


    /**
     *  Tests if comment notification is available. 
     *
     *  @return <code> true </code> if comment notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentNotification() {
        return (getAdapteeManager().supportsCommentNotification());
    }


    /**
     *  Tests if a comment to book lookup session is available. 
     *
     *  @return <code> true </code> if comment book lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentBook() {
        return (getAdapteeManager().supportsCommentBook());
    }


    /**
     *  Tests if a comment to book assignment session is available. 
     *
     *  @return <code> true </code> if comment book assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentBookAssignment() {
        return (getAdapteeManager().supportsCommentBookAssignment());
    }


    /**
     *  Tests if a comment smart booking session is available. 
     *
     *  @return <code> true </code> if comment smart booking is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentSmartBook() {
        return (getAdapteeManager().supportsCommentSmartBook());
    }


    /**
     *  Tests for the availability of an book lookup service. 
     *
     *  @return <code> true </code> if book lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookLookup() {
        return (getAdapteeManager().supportsBookLookup());
    }


    /**
     *  Tests if querying books is available. 
     *
     *  @return <code> true </code> if book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookQuery() {
        return (getAdapteeManager().supportsBookQuery());
    }


    /**
     *  Tests if searching for books is available. 
     *
     *  @return <code> true </code> if book search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookSearch() {
        return (getAdapteeManager().supportsBookSearch());
    }


    /**
     *  Tests for the availability of a book administrative service for 
     *  creating and deleting books. 
     *
     *  @return <code> true </code> if book administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookAdmin() {
        return (getAdapteeManager().supportsBookAdmin());
    }


    /**
     *  Tests for the availability of a book notification service. 
     *
     *  @return <code> true </code> if book notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookNotification() {
        return (getAdapteeManager().supportsBookNotification());
    }


    /**
     *  Tests for the availability of a book hierarchy traversal service. 
     *
     *  @return <code> true </code> if book hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookHierarchy() {
        return (getAdapteeManager().supportsBookHierarchy());
    }


    /**
     *  Tests for the availability of a book hierarchy design service. 
     *
     *  @return <code> true </code> if book hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookHierarchyDesign() {
        return (getAdapteeManager().supportsBookHierarchyDesign());
    }


    /**
     *  Tests for the availability of a commenting batch service. 
     *
     *  @return <code> true </code> if commenting batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingBatch() {
        return (getAdapteeManager().supportsCommentingBatch());
    }


    /**
     *  Gets the supported <code> Comment </code> record types. 
     *
     *  @return a list containing the supported comment record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommentRecordTypes() {
        return (getAdapteeManager().getCommentRecordTypes());
    }


    /**
     *  Tests if the given <code> Comment </code> record type is supported. 
     *
     *  @param  commentRecordType a <code> Type </code> indicating a <code> 
     *          Comment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommentRecordType(org.osid.type.Type commentRecordType) {
        return (getAdapteeManager().supportsCommentRecordType(commentRecordType));
    }


    /**
     *  Gets the supported comment search record types. 
     *
     *  @return a list containing the supported comment search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommentSearchRecordTypes() {
        return (getAdapteeManager().getCommentSearchRecordTypes());
    }


    /**
     *  Tests if the given comment search record type is supported. 
     *
     *  @param  commentSearchRecordType a <code> Type </code> indicating a 
     *          comment record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commentSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommentSearchRecordType(org.osid.type.Type commentSearchRecordType) {
        return (getAdapteeManager().supportsCommentSearchRecordType(commentSearchRecordType));
    }


    /**
     *  Gets the supported <code> Book </code> record types. 
     *
     *  @return a list containing the supported book record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBookRecordTypes() {
        return (getAdapteeManager().getBookRecordTypes());
    }


    /**
     *  Tests if the given <code> Book </code> record type is supported. 
     *
     *  @param  bookRecordType a <code> Type </code> indicating a <code> Book 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bookRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBookRecordType(org.osid.type.Type bookRecordType) {
        return (getAdapteeManager().supportsBookRecordType(bookRecordType));
    }


    /**
     *  Gets the supported book search record types. 
     *
     *  @return a list containing the supported book search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBookSearchRecordTypes() {
        return (getAdapteeManager().getBookSearchRecordTypes());
    }


    /**
     *  Tests if the given book search record type is supported. 
     *
     *  @param  bookSearchRecordType a <code> Type </code> indicating a book 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bookSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBookSearchRecordType(org.osid.type.Type bookSearchRecordType) {
        return (getAdapteeManager().supportsBookSearchRecordType(bookSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentLookupSession getCommentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment lookup 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentLookupSession getCommentLookupSessionForBook(org.osid.id.Id bookId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentLookupSessionForBook(bookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rating lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RatingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRatingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.RatingLookupSession getRatingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRatingLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rating lookup 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RatingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRatingLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.RatingLookupSession getRatingLookupSessionForBook(org.osid.id.Id bookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRatingLookupSessionForBook(bookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuerySession getCommentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment query 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuerySession getCommentQuerySessionForBook(org.osid.id.Id bookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentQuerySessionForBook(bookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSearchSession getCommentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment search 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSearchSession getCommentSearchSessionForBook(org.osid.id.Id bookId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentSearchSessionForBook(bookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentAdminSession getCommentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  administration service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentAdminSession getCommentAdminSessionForBook(org.osid.id.Id bookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentAdminSessionForBook(bookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  notification service. 
     *
     *  @param  commentReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CommentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commentReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentNotificationSession getCommentNotificationSession(org.osid.commenting.CommentReceiver commentReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentNotificationSession(commentReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  notification service for the given book. 
     *
     *  @param  commentReceiver the receiver 
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> commentReceiver, bookId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentNotificationSession getCommentNotificationSessionForBook(org.osid.commenting.CommentReceiver commentReceiver, 
                                                                                               org.osid.id.Id bookId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentNotificationSessionForBook(commentReceiver, bookId, proxy));
    }


    /**
     *  Gets the session for retrieving comment to book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentBookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentBook() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentBookSession getCommentBookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentBookSession(proxy));
    }


    /**
     *  Gets the session for assigning comment to book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentBookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentBookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentBookAssignmentSession getCommentBookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentBookAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic comment books for the given 
     *  book. 
     *
     *  @param  bookId the <code> Id </code> of a book 
     *  @param  proxy a proxy 
     *  @return <code> bookId </code> not found 
     *  @throws org.osid.NotFoundException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentSmartBook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSmartBookSession getCommentSmartBookSession(org.osid.id.Id bookId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentSmartBookSession(bookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookLookupSession getBookLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookQueryh() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuerySession getBookQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookSearchSession getBookSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookAdminSession getBookAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book 
     *  notification service. 
     *
     *  @param  bookReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> BookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> bookReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBookNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookNotificationSession getBookNotificationSession(org.osid.commenting.BookReceiver bookReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookNotificationSession(bookReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book hierarchy 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookHierarchySession getBookHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book hierarchy 
     *  design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBookHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookHierarchyDesignSession getBookHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBookHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> CommentingBatchProxyManager. </code> 
     *
     *  @return a <code> CommentingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.batch.CommentingBatchProxyManager getCommentingBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentingBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
