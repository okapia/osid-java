//
// AbstractAdapterAvailabilityLookupSession.java
//
//    An Availability lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Availability lookup session adapter.
 */

public abstract class AbstractAdapterAvailabilityLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.AvailabilityLookupSession {

    private final org.osid.resourcing.AvailabilityLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAvailabilityLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAvailabilityLookupSession(org.osid.resourcing.AvailabilityLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code Availability} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAvailabilities() {
        return (this.session.canLookupAvailabilities());
    }


    /**
     *  A complete view of the {@code Availability} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAvailabilityView() {
        this.session.useComparativeAvailabilityView();
        return;
    }


    /**
     *  A complete view of the {@code Availability} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAvailabilityView() {
        this.session.usePlenaryAvailabilityView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include availabilities in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only availabilities whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAvailabilityView() {
        this.session.useEffectiveAvailabilityView();
        return;
    }
    

    /**
     *  All availabilities of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAvailabilityView() {
        this.session.useAnyEffectiveAvailabilityView();
        return;
    }

     
    /**
     *  Gets the {@code Availability} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Availability} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Availability} and
     *  retained for compatibility.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param availabilityId {@code Id} of the {@code Availability}
     *  @return the availability
     *  @throws org.osid.NotFoundException {@code availabilityId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code availabilityId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Availability getAvailability(org.osid.id.Id availabilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailability(availabilityId));
    }


    /**
     *  Gets an {@code AvailabilityList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  availabilities specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Availabilities} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  availabilityIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Availability} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByIds(org.osid.id.IdList availabilityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesByIds(availabilityIds));
    }


    /**
     *  Gets an {@code AvailabilityList} corresponding to the given
     *  availability genus {@code Type} which does not include
     *  availabilities of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  availabilityGenusType an availability genus type 
     *  @return the returned {@code Availability} list
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesByGenusType(availabilityGenusType));
    }


    /**
     *  Gets an {@code AvailabilityList} corresponding to the given
     *  availability genus {@code Type} and include any additional
     *  availabilities with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  availabilityGenusType an availability genus type 
     *  @return the returned {@code Availability} list
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByParentGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesByParentGenusType(availabilityGenusType));
    }


    /**
     *  Gets an {@code AvailabilityList} containing the given
     *  availability record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  availabilityRecordType an availability record type 
     *  @return the returned {@code Availability} list
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByRecordType(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesByRecordType(availabilityRecordType));
    }


    /**
     *  Gets an {@code AvailabilityList} effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *  
     *  In active mode, availabilities are returned that are currently
     *  active. In any status mode, active and inactive availabilities
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Availability} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesOnDate(org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesOnDate(from, to));
    }
        

    /**
     *  Gets a list of availabilities corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code AvailabilityList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesForResource(resourceId));
    }


    /**
     *  Gets a list of availabilities corresponding to a resource
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AvailabilityList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of availabilities corresponding to a job {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the {@code Id} of the job
     *  @return the returned {@code AvailabilityList}
     *  @throws org.osid.NullArgumentException {@code jobId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForJob(org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesForJob(jobId));
    }


    /**
     *  Gets a list of availabilities corresponding to a job {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the {@code Id} of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AvailabilityList}
     *  @throws org.osid.NullArgumentException {@code jobId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForJobOnDate(org.osid.id.Id jobId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesForJobOnDate(jobId, from, to));
    }


    /**
     *  Gets a list of availabilities corresponding to resource and
     *  job {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  jobId the {@code Id} of the job
     *  @return the returned {@code AvailabilityList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code jobId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJob(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesForResourceAndJob(resourceId, jobId));
    }


    /**
     *  Gets a list of availabilities corresponding to resource and
     *  job {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective. In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the {@code Id} of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AvailabilityList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code jobId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJobOnDate(org.osid.id.Id resourceId,
                                                                                         org.osid.id.Id jobId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilitiesForResourceAndJobOnDate(resourceId, jobId, from, to));
    }


    /**
     *  Gets all {@code Availabilities}. 
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @return a list of {@code Availabilities} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilities());
    }
}
