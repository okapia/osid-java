//
// AbstractProcessEnablerLookupSession.java
//
//    A starter implementation framework for providing a ProcessEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ProcessEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProcessEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProcessEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.rules.ProcessEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    

    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProcessEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProcessEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>ProcessEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProcessEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ProcessEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProcessEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include process enablers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active process enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveProcessEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive process enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusProcessEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ProcessEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProcessEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProcessEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param  processEnablerId <code>Id</code> of the
     *          <code>ProcessEnabler</code>
     *  @return the process enabler
     *  @throws org.osid.NotFoundException <code>processEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>processEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnabler getProcessEnabler(org.osid.id.Id processEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.rules.ProcessEnablerList processEnablers = getProcessEnablers()) {
            while (processEnablers.hasNext()) {
                org.osid.workflow.rules.ProcessEnabler processEnabler = processEnablers.getNextProcessEnabler();
                if (processEnabler.getId().equals(processEnablerId)) {
                    return (processEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(processEnablerId + " not found");
    }


    /**
     *  Gets a <code>ProcessEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  processEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProcessEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProcessEnablers()</code>.
     *
     *  @param  processEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProcessEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByIds(org.osid.id.IdList processEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.rules.ProcessEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = processEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProcessEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("process enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.rules.processenabler.LinkedProcessEnablerList(ret));
    }


    /**
     *  Gets a <code>ProcessEnablerList</code> corresponding to the given
     *  process enabler genus <code>Type</code> which does not include
     *  process enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProcessEnablers()</code>.
     *
     *  @param  processEnablerGenusType a processEnabler genus type 
     *  @return the returned <code>ProcessEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByGenusType(org.osid.type.Type processEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.processenabler.ProcessEnablerGenusFilterList(getProcessEnablers(), processEnablerGenusType));
    }


    /**
     *  Gets a <code>ProcessEnablerList</code> corresponding to the given
     *  process enabler genus <code>Type</code> and include any additional
     *  process enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProcessEnablers()</code>.
     *
     *  @param  processEnablerGenusType a processEnabler genus type 
     *  @return the returned <code>ProcessEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByParentGenusType(org.osid.type.Type processEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProcessEnablersByGenusType(processEnablerGenusType));
    }


    /**
     *  Gets a <code>ProcessEnablerList</code> containing the given
     *  process enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProcessEnablers()</code>.
     *
     *  @param  processEnablerRecordType a processEnabler record type 
     *  @return the returned <code>ProcessEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByRecordType(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.processenabler.ProcessEnablerRecordFilterList(getProcessEnablers(), processEnablerRecordType));
    }


    /**
     *  Gets a <code>ProcessEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible
     *  through this session.
     *  
     *  In active mode, process enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive process enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProcessEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.processenabler.TemporalProcessEnablerFilterList(getProcessEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>ProcessEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible
     *  through this session.
     *
     *  In active mode, process enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive process enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ProcessEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getProcessEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>ProcessEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @return a list of <code>ProcessEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.rules.ProcessEnablerList getProcessEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the process enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of process enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.rules.ProcessEnablerList filterProcessEnablersOnViews(org.osid.workflow.rules.ProcessEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.workflow.rules.ProcessEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.workflow.rules.processenabler.ActiveProcessEnablerFilterList(ret);
        }

        return (ret);
    }
}
