//
// AbstractIndexedMapGradeSystemTransformLookupSession.java
//
//    A simple framework for providing a GradeSystemTransform lookup service
//    backed by a fixed collection of grade system transforms with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.transform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a GradeSystemTransform lookup service backed by a
 *  fixed collection of grade system transforms. The grade system transforms are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some grade system transforms may be compatible
 *  with more types than are indicated through these grade system transform
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradeSystemTransforms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapGradeSystemTransformLookupSession
    extends AbstractMapGradeSystemTransformLookupSession
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.grading.transform.GradeSystemTransform> gradeSystemTransformsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.transform.GradeSystemTransform>());
    private final MultiMap<org.osid.type.Type, org.osid.grading.transform.GradeSystemTransform> gradeSystemTransformsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.transform.GradeSystemTransform>());


    /**
     *  Makes a <code>GradeSystemTransform</code> available in this session.
     *
     *  @param  gradeSystemTransform a grade system transform
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransform<code> is
     *          <code>null</code>
     */

    @Override
    protected void putGradeSystemTransform(org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        super.putGradeSystemTransform(gradeSystemTransform);

        this.gradeSystemTransformsByGenus.put(gradeSystemTransform.getGenusType(), gradeSystemTransform);
        
        try (org.osid.type.TypeList types = gradeSystemTransform.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradeSystemTransformsByRecord.put(types.getNextType(), gradeSystemTransform);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a grade system transform from this session.
     *
     *  @param gradeSystemTransformId the <code>Id</code> of the grade system transform
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransformId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeGradeSystemTransform(org.osid.id.Id gradeSystemTransformId) {
        org.osid.grading.transform.GradeSystemTransform gradeSystemTransform;
        try {
            gradeSystemTransform = getGradeSystemTransform(gradeSystemTransformId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.gradeSystemTransformsByGenus.remove(gradeSystemTransform.getGenusType());

        try (org.osid.type.TypeList types = gradeSystemTransform.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradeSystemTransformsByRecord.remove(types.getNextType(), gradeSystemTransform);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeGradeSystemTransform(gradeSystemTransformId);
        return;
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> corresponding to the given
     *  grade system transform genus <code>Type</code> which does not include
     *  grade system transforms of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known grade system transforms or an error results. Otherwise,
     *  the returned list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  gradeSystemTransformGenusType a grade system transform genus type 
     *  @return the returned <code>GradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByGenusType(org.osid.type.Type gradeSystemTransformGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.transform.gradesystemtransform.ArrayGradeSystemTransformList(this.gradeSystemTransformsByGenus.get(gradeSystemTransformGenusType)));
    }


    /**
     *  Gets a <code>GradeSystemTransformList</code> containing the given
     *  grade system transform record <code>Type</code>. In plenary mode, the
     *  returned list contains all known grade system transforms or an error
     *  results. Otherwise, the returned list may contain only those
     *  grade system transforms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  gradeSystemTransformRecordType a grade system transform record type 
     *  @return the returned <code>gradeSystemTransform</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByRecordType(org.osid.type.Type gradeSystemTransformRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.transform.gradesystemtransform.ArrayGradeSystemTransformList(this.gradeSystemTransformsByRecord.get(gradeSystemTransformRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradeSystemTransformsByGenus.clear();
        this.gradeSystemTransformsByRecord.clear();

        super.close();

        return;
    }
}
