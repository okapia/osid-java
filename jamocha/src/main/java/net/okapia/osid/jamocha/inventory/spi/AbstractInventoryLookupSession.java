//
// AbstractInventoryLookupSession.java
//
//    A starter implementation framework for providing an Inventory
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Inventory
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getInventories(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractInventoryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.InventoryLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();
    
    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Inventory</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInventories() {
        return (true);
    }


    /**
     *  A complete view of the <code>Inventory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInventoryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Inventory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInventoryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inventories in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Inventory</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Inventory</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Inventory</code> and
     *  retained for compatibility.
     *
     *  @param  inventoryId <code>Id</code> of the
     *          <code>Inventory</code>
     *  @return the inventory
     *  @throws org.osid.NotFoundException <code>inventoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inventoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Inventory getInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.InventoryList inventories = getInventories()) {
            while (inventories.hasNext()) {
                org.osid.inventory.Inventory inventory = inventories.getNextInventory();
                if (inventory.getId().equals(inventoryId)) {
                    return (inventory);
                }
            }
        } 

        throw new org.osid.NotFoundException(inventoryId + " not found");
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inventories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Inventories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInventories()</code>.
     *
     *  @param  inventoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByIds(org.osid.id.IdList inventoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.Inventory> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = inventoryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getInventory(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("inventory " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.inventory.LinkedInventoryList(ret));
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  inventory genus <code>Type</code> which does not include
     *  inventories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInventories()</code>.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.inventory.InventoryGenusFilterList(getInventories(), inventoryGenusType));
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  inventory genus <code>Type</code> and include any additional
     *  inventories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInventories()</code>.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByParentGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getInventoriesByGenusType(inventoryGenusType));
    }


    /**
     *  Gets an <code>InventoryList</code> containing the given
     *  inventory record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInventories()</code>.
     *
     *  @param  inventoryRecordType an inventory record type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByRecordType(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.inventory.InventoryRecordFilterList(getInventories(), inventoryRecordType));
    }


    /**
     *  Gets the most recent <code>Inventories</code>. In plenary
     *  mode, the returned list contains all known inventories or an
     *  error results.  Otherwise, the returned list may contain only
     *  those inventories that are accessible through this session.
     *
     *  @return the returned <code>Inventory</code> list 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getRecentInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Map<org.osid.id.Id, org.osid.inventory.Inventory> recent = new java.util.HashMap<>();

        try (org.osid.inventory.InventoryList inventories = getInventories()) {
            while (inventories.hasNext()) {
                org.osid.inventory.Inventory inventory = inventories.getNextInventory();
                org.osid.inventory.Inventory recentInventory = recent.get(inventory.getStockId());
                if (recentInventory == null) {
                    recent.put(inventory.getStockId(), inventory);
                } else {
                    if (recentInventory.getDate().isLess(inventory.getDate())) {
                        recent.put(inventory.getStockId(), inventory);
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.inventory.LinkedInventoryList(recent.values()));
    }
        

    /**
     *  Gets the most recent <code>Inventories</code> for with the
     *  given <code>Stock</code>, In plenary mode, the returned list
     *  contains all known inventories or an error results. Otherwise,
     *  the returned list may contain only those inventories that are
     *  accessible through this session.
     *
     *  @param  stockId an inventory <code>Id</code> 
     *  @return the returned <code>Inventory</code> list 
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getRecentInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.Inventory> ret = new java.util.ArrayList<>();
        org.osid.calendaring.DateTime recent = null;

        try (org.osid.inventory.InventoryList inventories = getInventoriesForStock(stockId)) {
            while (inventories.hasNext()) {
                org.osid.inventory.Inventory inventory = inventories.getNextInventory();
                if (recent == null) {
                    ret.add(inventory);
                    recent = inventory.getDate();
                } else if (inventory.getDate().isGreater(recent)) {
                    ret.clear();
                    ret.add(inventory);
                    recent = inventory.getDate();
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.inventory.LinkedInventoryList(ret));
    }


    /**
     *  Gets <code>Inventories</code> between the given date range
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Inventory</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to< /code> 
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.inventory.InventoryFilterList(new DateFilter(from, to), getInventories()));
    }        


    /**
     *  Gets all <code>Inventories</code> for the given
     *  <code>Stock</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session.
     *
     *  @param  stockId a stock <code>Id</code> 
     *  @return the returned <code>Inventory</code> list 
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.inventory.InventoryFilterList(new StockFilter(stockId), getInventories()));
    }


    /**
     *  Gets <code>Inventories</code> between the given date range
     *  inclusive for the given stock.  In plenary mode, the returned
     *  list contains all known inventories or an error
     *  results. Otherwise, the returned list may contain only those
     *  inventories that are accessible through this session.
     *
     *  @param  stockId an inventory <code>Id</code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Inventory</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>stockid</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDateForStock(org.osid.id.Id stockId, 
                                                                         org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.inventory.InventoryFilterList(new DateFilter(from, to), getInventoriesForStock(stockId)));
    }


    
    /**
     *  Gets all <code>Inventories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Inventories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inventory.InventoryList getInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the inventory list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of inventories
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inventory.InventoryList filterInventoriesOnViews(org.osid.inventory.InventoryList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.inventory.InventoryFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;

        
        /**
         *  Constructs a new <date>DateFilter</date>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <date>from</date>
         *          or <code>to</code> is <date>null</date>
         */

        public DateFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to   = to;

            return;
        }


        /**
         *  Used by the InventoryFilterList to filter the inventory list based
         *  on date.
         *
         *  @param inventory the inventory
         *  @return <date>true</date> to pass the inventory,
         *          <date>false</date> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Inventory inventory) {
            if (inventory.getDate().isLess(this.from)) {
                return (false);
            }

            if (inventory.getDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }    


    public static class StockFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.inventory.InventoryFilter {
         
        private final org.osid.id.Id stockId;
         
         
        /**
         *  Constructs a new <code>StockFilter</code>.
         *
         *  @param stockId the stock to filter
         *  @throws org.osid.NullArgumentException
         *          <code>stockId</code> is <code>null</code>
         */
        
        public StockFilter(org.osid.id.Id stockId) {
            nullarg(stockId, "stock Id");
            this.stockId = stockId;
            return;
        }

         
        /**
         *  Used by the InventoryFilterList to filter the 
         *  inventory list based on stock.
         *
         *  @param inventory the inventory
         *  @return <code>true</code> to pass the inventory,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Inventory inventory) {
            return (inventory.getStockId().equals(this.stockId));
        }
    }
}
