//
// AbstractRepositoryRulesManager.java
//
//     An adapter for a RepositoryRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.repository.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RepositoryRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRepositoryRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.repository.rules.RepositoryRulesManager>
    implements org.osid.repository.rules.RepositoryRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterRepositoryRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRepositoryRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRepositoryRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRepositoryRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up composition enablers is supported. 
     *
     *  @return <code> true </code> if composition enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerLookup() {
        return (getAdapteeManager().supportsCompositionEnablerLookup());
    }


    /**
     *  Tests if querying composition enablers is supported. 
     *
     *  @return <code> true </code> if composition enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerQuery() {
        return (getAdapteeManager().supportsCompositionEnablerQuery());
    }


    /**
     *  Tests if searching composition enablers is supported. 
     *
     *  @return <code> true </code> if composition enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerSearch() {
        return (getAdapteeManager().supportsCompositionEnablerSearch());
    }


    /**
     *  Tests if a composition enabler administrative service is supported. 
     *
     *  @return <code> true </code> if composition enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerAdmin() {
        return (getAdapteeManager().supportsCompositionEnablerAdmin());
    }


    /**
     *  Tests if a composition enabler notification service is supported. 
     *
     *  @return <code> true </code> if composition enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerNotification() {
        return (getAdapteeManager().supportsCompositionEnablerNotification());
    }


    /**
     *  Tests if a composition enabler repository lookup service is supported. 
     *
     *  @return <code> true </code> if a repository enabler composition lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRepository() {
        return (getAdapteeManager().supportsCompositionEnablerRepository());
    }


    /**
     *  Tests if a composition enabler repository service is supported. 
     *
     *  @return <code> true </code> if composition enabler repository 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRepositoryAssignment() {
        return (getAdapteeManager().supportsCompositionEnablerRepositoryAssignment());
    }


    /**
     *  Tests if a composition enabler repository lookup service is supported. 
     *
     *  @return <code> true </code> if a composition enabler repository 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerSmartRepository() {
        return (getAdapteeManager().supportsCompositionEnablerSmartRepository());
    }


    /**
     *  Tests if a composition enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a composition enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRuleLookup() {
        return (getAdapteeManager().supportsCompositionEnablerRuleLookup());
    }


    /**
     *  Tests if a composition enabler rule application service is supported. 
     *
     *  @return <code> true </code> if composition enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRuleApplication() {
        return (getAdapteeManager().supportsCompositionEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> CompositionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CompositionEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionEnablerRecordTypes() {
        return (getAdapteeManager().getCompositionEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> CompositionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  compositionEnablerRecordType a <code> Type </code> indicating 
     *          a <code> CompositionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRecordType(org.osid.type.Type compositionEnablerRecordType) {
        return (getAdapteeManager().supportsCompositionEnablerRecordType(compositionEnablerRecordType));
    }


    /**
     *  Gets the supported <code> CompositionEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CompositionEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionEnablerSearchRecordTypes() {
        return (getAdapteeManager().getCompositionEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CompositionEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  compositionEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CompositionEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerSearchRecordType(org.osid.type.Type compositionEnablerSearchRecordType) {
        return (getAdapteeManager().supportsCompositionEnablerSearchRecordType(compositionEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler lookup service. 
     *
     *  @return a <code> CompositionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerLookupSession getCompositionEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler lookup service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerLookupSession getCompositionEnablerLookupSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerLookupSessionForRepository(repositoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler query service. 
     *
     *  @return a <code> CompositionEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerQuerySession getCompositionEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler query service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerQuerySession getCompositionEnablerQuerySessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerQuerySessionForRepository(repositoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler search service. 
     *
     *  @return a <code> CompositionEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSearchSession getCompositionEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enablers earch service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSearchSession getCompositionEnablerSearchSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerSearchSessionForRepository(repositoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler administration service. 
     *
     *  @return a <code> CompositionEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerAdminSession getCompositionEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler administration service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerAdminSession getCompositionEnablerAdminSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerAdminSessionForRepository(repositoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler notification service. 
     *
     *  @param  compositionEnablerReceiver the notification callback 
     *  @return a <code> CompositionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerNotificationSession getCompositionEnablerNotificationSession(org.osid.repository.rules.CompositionEnablerReceiver compositionEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerNotificationSession(compositionEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler notification service for the given repository. 
     *
     *  @param  compositionEnablerReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no repository found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerReceiver </code> or <code> repositoryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerNotificationSession getCompositionEnablerNotificationSessionForRepository(org.osid.repository.rules.CompositionEnablerReceiver compositionEnablerReceiver, 
                                                                                                                                 org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerNotificationSessionForRepository(compositionEnablerReceiver, repositoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup composition enabler 
     *  repository mappings for composition enablers. 
     *
     *  @return a <code> CompositionEnablerCompositionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRepository() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRepositorySession getCompositionEnablerRepositorySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerRepositorySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  composition enablers to repositories. 
     *
     *  @return a <code> CompositionEnablerRepositoryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerCompositionAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRepositoryAssignmentSession getCompositionEnablerRepositoryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerRepositoryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage composition enabler 
     *  smart repositories. 
     *
     *  @param  repositoryId the Id of the <code> Repository </code> 
     *  @return a <code> CompositionEnablerSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSmartRepository() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSmartRepositorySession getCompositionEnablerSmartRepositorySession(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerSmartRepositorySession(repositoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler mapping lookup service for looking up the rules applied to a 
     *  composition. 
     *
     *  @return a <code> CompositionEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleLookupSession getCompositionEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler mapping lookup service for the given composition for looking 
     *  up rules applied to a composition. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleLookupSession getCompositionEnablerRuleLookupSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerRuleLookupSessionForRepository(repositoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler assignment service to apply enablers to compositions. 
     *
     *  @return a <code> CompositionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleApplicationSession getCompositionEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler assignment service for the given composition to apply enablers 
     *  to compositions. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleApplicationSession getCompositionEnablerRuleApplicationSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompositionEnablerRuleApplicationSessionForRepository(repositoryId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
