//
// AbstractWarehouseSearch.java
//
//     A template for making a Warehouse Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.warehouse.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing warehouse searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractWarehouseSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inventory.WarehouseSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inventory.records.WarehouseSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inventory.WarehouseSearchOrder warehouseSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of warehouses. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  warehouseIds list of warehouses
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongWarehouses(org.osid.id.IdList warehouseIds) {
        while (warehouseIds.hasNext()) {
            try {
                this.ids.add(warehouseIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongWarehouses</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of warehouse Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getWarehouseIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  warehouseSearchOrder warehouse search order 
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>warehouseSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderWarehouseResults(org.osid.inventory.WarehouseSearchOrder warehouseSearchOrder) {
	this.warehouseSearchOrder = warehouseSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inventory.WarehouseSearchOrder getWarehouseSearchOrder() {
	return (this.warehouseSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given warehouse search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a warehouse implementing the requested record.
     *
     *  @param warehouseSearchRecordType a warehouse search record
     *         type
     *  @return the warehouse search record
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(warehouseSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.WarehouseSearchRecord getWarehouseSearchRecord(org.osid.type.Type warehouseSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inventory.records.WarehouseSearchRecord record : this.records) {
            if (record.implementsRecordType(warehouseSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(warehouseSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this warehouse search. 
     *
     *  @param warehouseSearchRecord warehouse search record
     *  @param warehouseSearchRecordType warehouse search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addWarehouseSearchRecord(org.osid.inventory.records.WarehouseSearchRecord warehouseSearchRecord, 
                                           org.osid.type.Type warehouseSearchRecordType) {

        addRecordType(warehouseSearchRecordType);
        this.records.add(warehouseSearchRecord);        
        return;
    }
}
