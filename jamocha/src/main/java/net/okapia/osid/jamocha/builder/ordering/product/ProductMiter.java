//
// ProductMiter.java
//
//     Defines a Product miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.product;


/**
 *  Defines a <code>Product</code> miter for use with the builders.
 */

public interface ProductMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.ordering.Product {


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    public void setCode(String code);


    /**
     *  Adds a price schedule.
     *
     *  @param schedule a price schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public void addPriceSchedule(org.osid.ordering.PriceSchedule schedule);


    /**
     *  Sets all the price schedules.
     *
     *  @param schedules a collection of price schedules
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    public void setPriceSchedules(java.util.Collection<org.osid.ordering.PriceSchedule> schedules);


    /**
     *  Sets the availability.
     *
     *  @param availability an availability quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>availability</code> is negative
     */

    public void setAvailability(long availability);


    /**
     *  Adds a Product record.
     *
     *  @param record a product record
     *  @param recordType the type of product record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addProductRecord(org.osid.ordering.records.ProductRecord record, org.osid.type.Type recordType);
}       


