//
// AbstractMutableAwardRequirement.java
//
//     Defines a mutable AwardRequirement.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.awardrequirement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>AwardRequirement</code>.
 */

public abstract class AbstractMutableAwardRequirement
    extends net.okapia.osid.jamocha.course.requisite.awardrequirement.spi.AbstractAwardRequirement
    implements org.osid.course.requisite.AwardRequirement,
               net.okapia.osid.jamocha.builder.course.requisite.awardrequirement.AwardRequirementMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this award requirement. 
     *
     *  @param record award requirement record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAwardRequirementRecord(org.osid.course.requisite.records.AwardRequirementRecord record, org.osid.type.Type recordType) {
        super.addAwardRequirementRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this award requirement. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if enabled
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this award requirement. Disabling an operable
     *  overrides any enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this award requirement.
     *
     *  @param displayName the name for this award requirement
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this award requirement.
     *
     *  @param description the description of this award requirement
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Adds an alternative requisite.
     *
     *  @param requisite an alternative requisite
     *  @throws org.osid.NullArgumentException <code>requisite</code>
     *          is <code>null</code>
     */

    @Override
    public void addAltRequisite(org.osid.course.requisite.Requisite requisite) {
        super.addAltRequisite(requisite);
        return;
    }


    /**
     *  Sets all the alternative requisite.
     *
     *  @param requisites a collection of alternative requisite
     *  @throws org.osid.NullArgumentException <code>requisites</code>
     *          is <code>null</code>
     */

    @Override
    public void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> requisites) {
        super.setAltRequisites(requisites);
        return;
    }


    /**
     *  Sets the award.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException
     *          <code>award</code> is <code>null</code>
     */

    @Override
    public void setAward(org.osid.recognition.Award award) {
        super.setAward(award);
        return;
    }


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @throws org.osid.NullArgumentException
     *          <code>timeframe</code> is <code>null</code>
     */

    @Override
    public void setTimeframe(org.osid.calendaring.Duration timeframe) {
        super.setTimeframe(timeframe);
        return;
    }
}

