//
// AbstractQueueQuery.java
//
//     A template for making a Queue Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.queue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for queues.
 */

public abstract class AbstractQueueQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.tracking.QueueQuery {

    private final java.util.Collection<org.osid.tracking.records.QueueQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the issue <code> Id </code> for this query to match queues that 
     *  pass through issues. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Matches log entries that go through any issue. 
     *
     *  @param  match <code> true </code> to match log entries with any issue, 
     *          <code> false </code> to match log entries with no issue 
     */

    @OSID @Override
    public void matchAnyIssue(boolean match) {
        return;
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        return;
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match queues 
     *  assigned to frontOffices. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given queue query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue implementing the requested record.
     *
     *  @param queueRecordType a queue record type
     *  @return the queue query record
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.QueueQueryRecord getQueueQueryRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.QueueQueryRecord record : this.records) {
            if (record.implementsRecordType(queueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue query. 
     *
     *  @param queueQueryRecord queue query record
     *  @param queueRecordType queue record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueQueryRecord(org.osid.tracking.records.QueueQueryRecord queueQueryRecord, 
                                          org.osid.type.Type queueRecordType) {

        addRecordType(queueRecordType);
        nullarg(queueQueryRecord, "queue query record");
        this.records.add(queueQueryRecord);        
        return;
    }
}
