//
// AbstractFoundry.java
//
//     Defines a Foundry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.foundry.spi;


/**
 *  Defines a <code>Foundry</code> builder.
 */

public abstract class AbstractFoundryBuilder<T extends AbstractFoundryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resourcing.foundry.FoundryMiter foundry;


    /**
     *  Constructs a new <code>AbstractFoundryBuilder</code>.
     *
     *  @param foundry the foundry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractFoundryBuilder(net.okapia.osid.jamocha.builder.resourcing.foundry.FoundryMiter foundry) {
        super(foundry);
        this.foundry = foundry;
        return;
    }


    /**
     *  Builds the foundry.
     *
     *  @return the new foundry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resourcing.Foundry build() {
        (new net.okapia.osid.jamocha.builder.validator.resourcing.foundry.FoundryValidator(getValidations())).validate(this.foundry);
        return (new net.okapia.osid.jamocha.builder.resourcing.foundry.ImmutableFoundry(this.foundry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the foundry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resourcing.foundry.FoundryMiter getMiter() {
        return (this.foundry);
    }


    /**
     *  Adds a Foundry record.
     *
     *  @param record a foundry record
     *  @param recordType the type of foundry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resourcing.records.FoundryRecord record, org.osid.type.Type recordType) {
        getMiter().addFoundryRecord(record, recordType);
        return (self());
    }
}       


