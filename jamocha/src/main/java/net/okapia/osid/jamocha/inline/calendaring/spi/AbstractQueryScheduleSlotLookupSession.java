//
// AbstractQueryScheduleSlotLookupSession.java
//
//    An inline adapter that maps a ScheduleSlotLookupSession to
//    a ScheduleSlotQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ScheduleSlotLookupSession to
 *  a ScheduleSlotQuerySession.
 */

public abstract class AbstractQueryScheduleSlotLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractScheduleSlotLookupSession
    implements org.osid.calendaring.ScheduleSlotLookupSession {

    private boolean sequestered   = false;
    private final org.osid.calendaring.ScheduleSlotQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryScheduleSlotLookupSession.
     *
     *  @param querySession the underlying schedule slot query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryScheduleSlotLookupSession(org.osid.calendaring.ScheduleSlotQuerySession querySession) {
        nullarg(querySession, "schedule slot query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>ScheduleSlot</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupScheduleSlots() {
        return (this.session.canSearchScheduleSlots());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedule slots in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  schedule slots.
     */

    @OSID @Override
    public void useSequesteredScheduleSlotView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All schedule slots are returned including sequestered schedule slots.
     */

    @OSID @Override
    public void useUnsequesteredScheduleSlotView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if unsequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }

     
    /**
     *  Gets the <code>ScheduleSlot</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ScheduleSlot</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ScheduleSlot</code> and
     *  retained for compatibility.
     *
     *  @param  scheduleSlotId <code>Id</code> of the
     *          <code>ScheduleSlot</code>
     *  @return the schedule slot
     *  @throws org.osid.NotFoundException <code>scheduleSlotId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>scheduleSlotId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleSlotQuery query = getQuery();
        query.matchId(scheduleSlotId, true);
        org.osid.calendaring.ScheduleSlotList scheduleSlots = this.session.getScheduleSlotsByQuery(query);
        if (scheduleSlots.hasNext()) {
            return (scheduleSlots.getNextScheduleSlot());
        } 
        
        throw new org.osid.NotFoundException(scheduleSlotId + " not found");
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  scheduleSlots specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ScheduleSlots</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  scheduleSlotIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByIds(org.osid.id.IdList scheduleSlotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleSlotQuery query = getQuery();

        try (org.osid.id.IdList ids = scheduleSlotIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getScheduleSlotsByQuery(query));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  schedule slot genus <code>Type</code> which does not include
     *  schedule slots of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleSlotQuery query = getQuery();
        query.matchGenusType(scheduleSlotGenusType, true);
        return (this.session.getScheduleSlotsByQuery(query));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  schedule slot genus <code>Type</code> and include any additional
     *  schedule slots with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByParentGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleSlotQuery query = getQuery();
        query.matchParentGenusType(scheduleSlotGenusType, true);
        return (this.session.getScheduleSlotsByQuery(query));
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> containing the given
     *  schedule slot record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotRecordType a scheduleSlot record type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByRecordType(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleSlotQuery query = getQuery();
        query.matchRecordType(scheduleSlotRecordType, true);
        return (this.session.getScheduleSlotsByQuery(query));
    }


    /**
     *  Gets a <code> ScheduleSlotList </code> containing the given
     *  set of weekdays.
     *  
     *  In plenary mode, the returned list contains all known schedule
     *  slots or an error results. Otherwise, the returned list may
     *  contain only those schedule slots that are accessible through
     *  this session.
     *  
     *  In sequestered mode, no sequestered schedule slots are
     *  returned. In unsequestered mode, all schedule slots are
     *  returned.
     *
     *  @param  weekdays a set of weekdays 
     *  @return the returned <code> ScheduleSlot </code> list 
     *  @throws org.osid.InvalidArgumentException a <code> weekday
     *          </code> is negative
     *  @throws org.osid.NullArgumentException <code> weekdays </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByWeekdays(long[] weekdays)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleSlotQuery query = getQuery();
        for (long w : weekdays) {
            query.matchWeekday(w, true);
        }

        return (this.session.getScheduleSlotsByQuery(query));
    }


    /**
     *  Gets all <code>ScheduleSlots</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ScheduleSlots</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleSlotQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getScheduleSlotsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.ScheduleSlotQuery getQuery() {
        org.osid.calendaring.ScheduleSlotQuery query = this.session.getScheduleSlotQuery();
        
        if (isSequestered()) {
            query.matchSequestered(true);
        }

        return (query);
    }
}
