//
// AbstractSubscriptionRulesProxyManager.java
//
//     An adapter for a SubscriptionRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a SubscriptionRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterSubscriptionRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.subscription.rules.SubscriptionRulesProxyManager>
    implements org.osid.subscription.rules.SubscriptionRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterSubscriptionRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterSubscriptionRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up subscription enablers is supported. 
     *
     *  @return <code> true </code> if subscription enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerLookup() {
        return (getAdapteeManager().supportsSubscriptionEnablerLookup());
    }


    /**
     *  Tests if querying subscription enablers is supported. 
     *
     *  @return <code> true </code> if subscription enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerQuery() {
        return (getAdapteeManager().supportsSubscriptionEnablerQuery());
    }


    /**
     *  Tests if searching subscription enablers is supported. 
     *
     *  @return <code> true </code> if subscription enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerSearch() {
        return (getAdapteeManager().supportsSubscriptionEnablerSearch());
    }


    /**
     *  Tests if a subscription enabler administrative service is supported. 
     *
     *  @return <code> true </code> if subscription enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerAdmin() {
        return (getAdapteeManager().supportsSubscriptionEnablerAdmin());
    }


    /**
     *  Tests if a subscription enabler notification service is supported. 
     *
     *  @return <code> true </code> if subscription enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerNotification() {
        return (getAdapteeManager().supportsSubscriptionEnablerNotification());
    }


    /**
     *  Tests if a subscription enabler publisher lookup service is supported. 
     *
     *  @return <code> true </code> if a publisher enabler subscription lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerPublisher() {
        return (getAdapteeManager().supportsSubscriptionEnablerPublisher());
    }


    /**
     *  Tests if a subscription enabler publisher service is supported. 
     *
     *  @return <code> true </code> if subscription enabler publisher 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerPublisherAssignment() {
        return (getAdapteeManager().supportsSubscriptionEnablerPublisherAssignment());
    }


    /**
     *  Tests if a subscription enabler subscription lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a subscription enabler subscription 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerSmartSubscription() {
        return (getAdapteeManager().supportsSubscriptionEnablerSmartSubscription());
    }


    /**
     *  Tests if a subscription enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a subscription enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerRuleLookup() {
        return (getAdapteeManager().supportsSubscriptionEnablerRuleLookup());
    }


    /**
     *  Tests if a subscription enabler rule application service is supported. 
     *
     *  @return <code> true </code> if subscription enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerRuleApplication() {
        return (getAdapteeManager().supportsSubscriptionEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> SubscriptionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> SubscriptionEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubscriptionEnablerRecordTypes() {
        return (getAdapteeManager().getSubscriptionEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> SubscriptionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  subscriptionEnablerRecordType a <code> Type </code> indicating 
     *          a <code> SubscriptionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          subscriptionEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerRecordType(org.osid.type.Type subscriptionEnablerRecordType) {
        return (getAdapteeManager().supportsSubscriptionEnablerRecordType(subscriptionEnablerRecordType));
    }


    /**
     *  Gets the supported <code> SubscriptionEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> SubscriptionEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubscriptionEnablerSearchRecordTypes() {
        return (getAdapteeManager().getSubscriptionEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SubscriptionEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  subscriptionEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> SubscriptionEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          subscriptionEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSubscriptionEnablerSearchRecordType(org.osid.type.Type subscriptionEnablerSearchRecordType) {
        return (getAdapteeManager().supportsSubscriptionEnablerSearchRecordType(subscriptionEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerLookupSession getSubscriptionEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler lookup service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerLookupSession getSubscriptionEnablerLookupSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerLookupSessionForPublisher(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerQuerySession getSubscriptionEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler query service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerQuerySession getSubscriptionEnablerQuerySessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerQuerySessionForPublisher(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerSearchSession getSubscriptionEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enablers earch service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerSearchSession getSubscriptionEnablerSearchSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerSearchSessionForPublisher(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerAdminSession getSubscriptionEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler administration service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerAdminSession getSubscriptionEnablerAdminSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerAdminSessionForPublisher(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler notification service. 
     *
     *  @param  subscriptionEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          subscriptionEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerNotificationSession getSubscriptionEnablerNotificationSession(org.osid.subscription.rules.SubscriptionEnablerReceiver subscriptionEnablerReceiver, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerNotificationSession(subscriptionEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler notification service for the given publisher. 
     *
     *  @param  subscriptionEnablerReceiver the notification callback 
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no publisher found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          subscriptionEnablerReceiver, publisherId, </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerNotificationSession getSubscriptionEnablerNotificationSessionForPublisher(org.osid.subscription.rules.SubscriptionEnablerReceiver subscriptionEnablerReceiver, 
                                                                                                                                    org.osid.id.Id publisherId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerNotificationSessionForPublisher(subscriptionEnablerReceiver, publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup subscription 
     *  enabler/publisher mappings for subscription enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerPublisherSession getSubscriptionEnablerPublisherSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerPublisherSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  subscription enablers to publishers 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerPublisherAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerPublisherAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerPublisherAssignmentSession getSubscriptionEnablerPublisherAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerPublisherAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage subscription enabler 
     *  smart publishers. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerSmartPublisherSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerSmartPublisher() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerSmartPublisherSession getSubscriptionEnablerSmartPublisherSession(org.osid.id.Id publisherId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerSmartPublisherSession(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  subscription. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerRuleLookupSession getSubscriptionEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler mapping lookup service for the given subscription for looking 
     *  up rules applied to a subscription. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerRuleLookupSession getSubscriptionEnablerRuleLookupSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerRuleLookupSessionForPublisher(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler assignment service to apply enablers to subscriptions. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerRuleApplicationSession getSubscriptionEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  enabler assignment service for the given subscription to apply 
     *  enablers to subscriptions. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerRuleApplicationSession getSubscriptionEnablerRuleApplicationSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionEnablerRuleApplicationSessionForPublisher(publisherId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
