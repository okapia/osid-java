//
// MutableIndexedMapAuctionProcessorLookupSession
//
//    Implements an AuctionProcessor lookup service backed by a collection of
//    auctionProcessors indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules;


/**
 *  Implements an AuctionProcessor lookup service backed by a collection of
 *  auction processors. The auction processors are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some auction processors may be compatible
 *  with more types than are indicated through these auction processor
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of auction processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAuctionProcessorLookupSession
    extends net.okapia.osid.jamocha.core.bidding.rules.spi.AbstractIndexedMapAuctionProcessorLookupSession
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAuctionProcessorLookupSession} with no auction processors.
     *
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumentException {@code auctionHouse}
     *          is {@code null}
     */

      public MutableIndexedMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuctionProcessorLookupSession} with a
     *  single auction processor.
     *  
     *  @param auctionHouse the auction house
     *  @param  auctionProcessor an single auctionProcessor
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessor} is {@code null}
     */

    public MutableIndexedMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.rules.AuctionProcessor auctionProcessor) {
        this(auctionHouse);
        putAuctionProcessor(auctionProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuctionProcessorLookupSession} using an
     *  array of auction processors.
     *
     *  @param auctionHouse the auction house
     *  @param  auctionProcessors an array of auction processors
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessors} is {@code null}
     */

    public MutableIndexedMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.bidding.rules.AuctionProcessor[] auctionProcessors) {
        this(auctionHouse);
        putAuctionProcessors(auctionProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAuctionProcessorLookupSession} using a
     *  collection of auction processors.
     *
     *  @param auctionHouse the auction house
     *  @param  auctionProcessors a collection of auction processors
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessors} is {@code null}
     */

    public MutableIndexedMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessor> auctionProcessors) {

        this(auctionHouse);
        putAuctionProcessors(auctionProcessors);
        return;
    }
    

    /**
     *  Makes an {@code AuctionProcessor} available in this session.
     *
     *  @param  auctionProcessor an auction processor
     *  @throws org.osid.NullArgumentException {@code auctionProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionProcessor(org.osid.bidding.rules.AuctionProcessor auctionProcessor) {
        super.putAuctionProcessor(auctionProcessor);
        return;
    }


    /**
     *  Makes an array of auction processors available in this session.
     *
     *  @param  auctionProcessors an array of auction processors
     *  @throws org.osid.NullArgumentException {@code auctionProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putAuctionProcessors(org.osid.bidding.rules.AuctionProcessor[] auctionProcessors) {
        super.putAuctionProcessors(auctionProcessors);
        return;
    }


    /**
     *  Makes collection of auction processors available in this session.
     *
     *  @param  auctionProcessors a collection of auction processors
     *  @throws org.osid.NullArgumentException {@code auctionProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionProcessors(java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessor> auctionProcessors) {
        super.putAuctionProcessors(auctionProcessors);
        return;
    }


    /**
     *  Removes an AuctionProcessor from this session.
     *
     *  @param auctionProcessorId the {@code Id} of the auction processor
     *  @throws org.osid.NullArgumentException {@code auctionProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAuctionProcessor(org.osid.id.Id auctionProcessorId) {
        super.removeAuctionProcessor(auctionProcessorId);
        return;
    }    
}
