//
// MutableMapPostLookupSession
//
//    Implements a Post lookup service backed by a collection of
//    posts that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting;


/**
 *  Implements a Post lookup service backed by a collection of
 *  posts. The posts are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of posts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapPostLookupSession
    extends net.okapia.osid.jamocha.core.financials.posting.spi.AbstractMapPostLookupSession
    implements org.osid.financials.posting.PostLookupSession {


    /**
     *  Constructs a new {@code MutableMapPostLookupSession}
     *  with no posts.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business} is
     *          {@code null}
     */

      public MutableMapPostLookupSession(org.osid.financials.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPostLookupSession} with a
     *  single post.
     *
     *  @param business the business  
     *  @param post a post
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code post} is {@code null}
     */

    public MutableMapPostLookupSession(org.osid.financials.Business business,
                                           org.osid.financials.posting.Post post) {
        this(business);
        putPost(post);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPostLookupSession}
     *  using an array of posts.
     *
     *  @param business the business
     *  @param posts an array of posts
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code posts} is {@code null}
     */

    public MutableMapPostLookupSession(org.osid.financials.Business business,
                                           org.osid.financials.posting.Post[] posts) {
        this(business);
        putPosts(posts);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPostLookupSession}
     *  using a collection of posts.
     *
     *  @param business the business
     *  @param posts a collection of posts
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code posts} is {@code null}
     */

    public MutableMapPostLookupSession(org.osid.financials.Business business,
                                           java.util.Collection<? extends org.osid.financials.posting.Post> posts) {

        this(business);
        putPosts(posts);
        return;
    }

    
    /**
     *  Makes a {@code Post} available in this session.
     *
     *  @param post a post
     *  @throws org.osid.NullArgumentException {@code post{@code  is
     *          {@code null}
     */

    @Override
    public void putPost(org.osid.financials.posting.Post post) {
        super.putPost(post);
        return;
    }


    /**
     *  Makes an array of posts available in this session.
     *
     *  @param posts an array of posts
     *  @throws org.osid.NullArgumentException {@code posts{@code 
     *          is {@code null}
     */

    @Override
    public void putPosts(org.osid.financials.posting.Post[] posts) {
        super.putPosts(posts);
        return;
    }


    /**
     *  Makes collection of posts available in this session.
     *
     *  @param posts a collection of posts
     *  @throws org.osid.NullArgumentException {@code posts{@code  is
     *          {@code null}
     */

    @Override
    public void putPosts(java.util.Collection<? extends org.osid.financials.posting.Post> posts) {
        super.putPosts(posts);
        return;
    }


    /**
     *  Removes a Post from this session.
     *
     *  @param postId the {@code Id} of the post
     *  @throws org.osid.NullArgumentException {@code postId{@code 
     *          is {@code null}
     */

    @Override
    public void removePost(org.osid.id.Id postId) {
        super.removePost(postId);
        return;
    }    
}
