//
// AbstractNodeRealmHierarchySession.java
//
//     Defines a Realm hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a realm hierarchy session for delivering a hierarchy
 *  of realms using the RealmNode interface.
 */

public abstract class AbstractNodeRealmHierarchySession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractRealmHierarchySession
    implements org.osid.personnel.RealmHierarchySession {

    private java.util.Collection<org.osid.personnel.RealmNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root realm <code> Ids </code> in this hierarchy.
     *
     *  @return the root realm <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootRealmIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.personnel.realmnode.RealmNodeToIdList(this.roots));
    }


    /**
     *  Gets the root realms in the realm hierarchy. A node
     *  with no parents is an orphan. While all realm <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root realms 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRootRealms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.realmnode.RealmNodeToRealmList(new net.okapia.osid.jamocha.personnel.realmnode.ArrayRealmNodeList(this.roots)));
    }


    /**
     *  Adds a root realm node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootRealm(org.osid.personnel.RealmNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root realm nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootRealms(java.util.Collection<org.osid.personnel.RealmNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root realm node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootRealm(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.personnel.RealmNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Realm </code> has any parents. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @return <code> true </code> if the realm has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentRealms(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getRealmNode(realmId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  realm.
     *
     *  @param  id an <code> Id </code> 
     *  @param  realmId the <code> Id </code> of a realm 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> realmId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> realmId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfRealm(org.osid.id.Id id, org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.personnel.RealmNodeList parents = getRealmNode(realmId).getParentRealmNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextRealmNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given realm. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @return the parent <code> Ids </code> of the realm 
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentRealmIds(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.realm.RealmToIdList(getParentRealms(realmId)));
    }


    /**
     *  Gets the parents of the given realm. 
     *
     *  @param  realmId the <code> Id </code> to query 
     *  @return the parents of the realm 
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getParentRealms(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.realmnode.RealmNodeToRealmList(getRealmNode(realmId).getParentRealmNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  realm.
     *
     *  @param  id an <code> Id </code> 
     *  @param  realmId the Id of a realm 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> realmId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfRealm(org.osid.id.Id id, org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfRealm(id, realmId)) {
            return (true);
        }

        try (org.osid.personnel.RealmList parents = getParentRealms(realmId)) {
            while (parents.hasNext()) {
                if (isAncestorOfRealm(id, parents.getNextRealm().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a realm has any children. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @return <code> true </code> if the <code> realmId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildRealms(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRealmNode(realmId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  realm.
     *
     *  @param  id an <code> Id </code> 
     *  @param realmId the <code> Id </code> of a 
     *         realm
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> realmId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> realmId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfRealm(org.osid.id.Id id, org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfRealm(realmId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  realm.
     *
     *  @param  realmId the <code> Id </code> to query 
     *  @return the children of the realm 
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildRealmIds(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.realm.RealmToIdList(getChildRealms(realmId)));
    }


    /**
     *  Gets the children of the given realm. 
     *
     *  @param  realmId the <code> Id </code> to query 
     *  @return the children of the realm 
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getChildRealms(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.personnel.realmnode.RealmNodeToRealmList(getRealmNode(realmId).getChildRealmNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  realm.
     *
     *  @param  id an <code> Id </code> 
     *  @param realmId the <code> Id </code> of a 
     *         realm
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> realmId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfRealm(org.osid.id.Id id, org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfRealm(realmId, id)) {
            return (true);
        }

        try (org.osid.personnel.RealmList children = getChildRealms(realmId)) {
            while (children.hasNext()) {
                if (isDescendantOfRealm(id, children.getNextRealm().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  realm.
     *
     *  @param  realmId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified realm node 
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getRealmNodeIds(org.osid.id.Id realmId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.personnel.realmnode.RealmNodeToNode(getRealmNode(realmId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given realm.
     *
     *  @param  realmId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified realm node 
     *  @throws org.osid.NotFoundException <code> realmId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> realmId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmNode getRealmNodes(org.osid.id.Id realmId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRealmNode(realmId));
    }


    /**
     *  Closes this <code>RealmHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a realm node.
     *
     *  @param realmId the id of the realm node
     *  @throws org.osid.NotFoundException <code>realmId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>realmId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.personnel.RealmNode getRealmNode(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(realmId, "realm Id");
        for (org.osid.personnel.RealmNode realm : this.roots) {
            if (realm.getId().equals(realmId)) {
                return (realm);
            }

            org.osid.personnel.RealmNode r = findRealm(realm, realmId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(realmId + " is not found");
    }


    protected org.osid.personnel.RealmNode findRealm(org.osid.personnel.RealmNode node, 
                                                     org.osid.id.Id realmId) 
	throws org.osid.OperationFailedException {

        try (org.osid.personnel.RealmNodeList children = node.getChildRealmNodes()) {
            while (children.hasNext()) {
                org.osid.personnel.RealmNode realm = children.getNextRealmNode();
                if (realm.getId().equals(realmId)) {
                    return (realm);
                }
                
                realm = findRealm(realm, realmId);
                if (realm != null) {
                    return (realm);
                }
            }
        }

        return (null);
    }
}
