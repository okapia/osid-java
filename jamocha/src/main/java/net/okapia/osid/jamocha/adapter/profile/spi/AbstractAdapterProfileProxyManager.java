//
// AbstractProfileProxyManager.java
//
//     An adapter for a ProfileProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ProfileProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterProfileProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.profile.ProfileProxyManager>
    implements org.osid.profile.ProfileProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterProfileProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterProfileProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterProfileProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterProfileProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a profile service which is the basic 
     *  service for checking profiles. 
     *
     *  @return <code> true </code> if profiling is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfiling() {
        return (getAdapteeManager().supportsProfiling());
    }


    /**
     *  Tests if a profile entry lookup service is supported. A profile entry 
     *  lookup service defines methods to access profile entries. 
     *
     *  @return true if profile entry lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryLookup() {
        return (getAdapteeManager().supportsProfileEntryLookup());
    }


    /**
     *  Tests if profile entry query is supported. 
     *
     *  @return <code> true </code> if profile entry query is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryQuery() {
        return (getAdapteeManager().supportsProfileEntryQuery());
    }


    /**
     *  Tests if a profile entry search service is supported. 
     *
     *  @return <code> true </code> if profile entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntrySearch() {
        return (getAdapteeManager().supportsProfileEntrySearch());
    }


    /**
     *  Tests if a profile entry administrative service is supported. 
     *
     *  @return <code> true </code> if profile entry admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryAdmin() {
        return (getAdapteeManager().supportsProfileEntryAdmin());
    }


    /**
     *  Tests if profile entry notification is supported. Messages may be sent 
     *  when peofile entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if profile entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryNotification() {
        return (getAdapteeManager().supportsProfileEntryNotification());
    }


    /**
     *  Tests if retrieving mappings of profile entries and profile is 
     *  supported. 
     *
     *  @return <code> true </code> if profile entry profile mapping retrieval 
     *          is supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryProfile() {
        return (getAdapteeManager().supportsProfileEntryProfile());
    }


    /**
     *  Tests if managing mappings of profile entries and profile is 
     *  supported. 
     *
     *  @return <code> true </code> if profile entry profile assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryProfileAssignment() {
        return (getAdapteeManager().supportsProfileEntryProfileAssignment());
    }


    /**
     *  Tests if profile entry smart profiles are available. 
     *
     *  @return <code> true </code> if profile entry smart profiles are 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryRelationshpSmartProfile() {
        return (getAdapteeManager().supportsProfileEntryRelationshpSmartProfile());
    }


    /**
     *  Tests if a profile item lookup service is supported. A profile item 
     *  lookup service defines methods to access profile items. 
     *
     *  @return <code> true </code> if profile item lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemLookup() {
        return (getAdapteeManager().supportsProfileItemLookup());
    }


    /**
     *  Tests if profile item query is supported. 
     *
     *  @return <code> true </code> if profile item query is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemQuery() {
        return (getAdapteeManager().supportsProfileItemQuery());
    }


    /**
     *  Tests if a profile item search service is supported. 
     *
     *  @return <code> true </code> if profile item search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemSearch() {
        return (getAdapteeManager().supportsProfileItemSearch());
    }


    /**
     *  Tests if a profile item administrative service is supported. 
     *
     *  @return <code> true </code> if profile item admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemAdmin() {
        return (getAdapteeManager().supportsProfileItemAdmin());
    }


    /**
     *  Tests if profile item notification is supported. Messages may be sent 
     *  when profile items are created, modified, or deleted. 
     *
     *  @return <code> true </code> if profile item notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemNotification() {
        return (getAdapteeManager().supportsProfileItemNotification());
    }


    /**
     *  Tests if a profile item to profile lookup session is available. 
     *
     *  @return <code> true </code> if profile item profile lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemProfile() {
        return (getAdapteeManager().supportsProfileItemProfile());
    }


    /**
     *  Tests if a profile item to profile assignment session is available. 
     *
     *  @return <code> true </code> if profile item profile assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemProfileAssignment() {
        return (getAdapteeManager().supportsProfileItemProfileAssignment());
    }


    /**
     *  Tests if profile item smart profiles are available. 
     *
     *  @return <code> true </code> if profile item smart profiles are 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemSmartProfile() {
        return (getAdapteeManager().supportsProfileItemSmartProfile());
    }


    /**
     *  Tests if a profile lookup service is supported. A profile lookup 
     *  service defines methods to access profiles. 
     *
     *  @return <code> true </code> if profile lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileLookup() {
        return (getAdapteeManager().supportsProfileLookup());
    }


    /**
     *  Tests if profile query is supported. 
     *
     *  @return <code> true </code> if profile query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (getAdapteeManager().supportsProfileQuery());
    }


    /**
     *  Tests if a profile search service is supported. 
     *
     *  @return <code> true </code> if profile search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileSearch() {
        return (getAdapteeManager().supportsProfileSearch());
    }


    /**
     *  Tests if a profile administrative service is supported. 
     *
     *  @return <code> true </code> if profile admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileAdmin() {
        return (getAdapteeManager().supportsProfileAdmin());
    }


    /**
     *  Tests if profile notification is supported. Messages may be sent when 
     *  profiles are created, modified, or deleted. 
     *
     *  @return <code> true </code> if profile notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileNotification() {
        return (getAdapteeManager().supportsProfileNotification());
    }


    /**
     *  Tests if a profile hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a profile hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileHierarchy() {
        return (getAdapteeManager().supportsProfileHierarchy());
    }


    /**
     *  Tests if profile hierarchy design is supported. 
     *
     *  @return <code> true </code> if a profile hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileHierarchyDesign() {
        return (getAdapteeManager().supportsProfileHierarchyDesign());
    }


    /**
     *  Tests if a profile batch service is supported. 
     *
     *  @return <code> true </code> if a profile batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileBatch() {
        return (getAdapteeManager().supportsProfileBatch());
    }


    /**
     *  Tests if a profile rules service is supported. 
     *
     *  @return <code> true </code> if a profile rules service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileRules() {
        return (getAdapteeManager().supportsProfileRules());
    }


    /**
     *  Gets the supported <code> ProfileEntry </code> record types. 
     *
     *  @return a list containing the supported profile entry record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntryRecordTypes() {
        return (getAdapteeManager().getProfileEntryRecordTypes());
    }


    /**
     *  Tests if the given profile entry record type is supported. 
     *
     *  @param  profileEntryRecordType a <code> Type </code> indicating a 
     *          profile entry record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntryRecordType(org.osid.type.Type profileEntryRecordType) {
        return (getAdapteeManager().supportsProfileEntryRecordType(profileEntryRecordType));
    }


    /**
     *  Gets the supported <code> ProfileEntry </code> search record types. 
     *
     *  @return a list containing the supported profile entry search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntrySearchRecordTypes() {
        return (getAdapteeManager().getProfileEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given profile entry search record type is supported. 
     *
     *  @param  profileEntrySearchRecordType a <code> Type </code> indicating 
     *          a profile entry search record type 
     *  @return <code> true </code> if the given search record Type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntrySearchRecordType(org.osid.type.Type profileEntrySearchRecordType) {
        return (getAdapteeManager().supportsProfileEntrySearchRecordType(profileEntrySearchRecordType));
    }


    /**
     *  Gets the supported <code> ProfileItem </code> record types. 
     *
     *  @return a list containing the supported <code> ProfileItem </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileItemRecordTypes() {
        return (getAdapteeManager().getProfileItemRecordTypes());
    }


    /**
     *  Tests if the given <code> ProfileItem </code> record type is 
     *  supported. 
     *
     *  @param  profileItemRecordType a <code> Type </code> indicating a 
     *          <code> ProfileItem </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileItemRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileItemRecordType(org.osid.type.Type profileItemRecordType) {
        return (getAdapteeManager().supportsProfileItemRecordType(profileItemRecordType));
    }


    /**
     *  Gets the supported <code> ProfileItem </code> search record types. 
     *
     *  @return a list containing the supported <code> ProfileItem </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileItemSearchRecordTypes() {
        return (getAdapteeManager().getProfileItemSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ProfileItem </code> search record type is 
     *  supported. 
     *
     *  @param  profileItemSearchRecordType a <code> Type </code> indicating a 
     *          <code> ProfileItem </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileItemSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileItemSearchRecordType(org.osid.type.Type profileItemSearchRecordType) {
        return (getAdapteeManager().supportsProfileItemSearchRecordType(profileItemSearchRecordType));
    }


    /**
     *  Gets the supported <code> Profile </code> record types. 
     *
     *  @return a list containing the supported <code> Profile </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileRecordTypes() {
        return (getAdapteeManager().getProfileRecordTypes());
    }


    /**
     *  Tests if the given <code> Profile </code> record type is supported. 
     *
     *  @param  profileRecordType a <code> Type </code> indicating a <code> 
     *          Profile </code> type 
     *  @return <code> true </code> if the given profile record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileRecordType(org.osid.type.Type profileRecordType) {
        return (getAdapteeManager().supportsProfileRecordType(profileRecordType));
    }


    /**
     *  Gets the supported profile search record types. 
     *
     *  @return a list containing the supported <code> Profile </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileSearchRecordTypes() {
        return (getAdapteeManager().getProfileSearchRecordTypes());
    }


    /**
     *  Tests if the given profile search record type is supported. 
     *
     *  @param  profileSearchRecordType a <code> Type </code> indicating a 
     *          <code> Profile </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileSearchRecordType(org.osid.type.Type profileSearchRecordType) {
        return (getAdapteeManager().supportsProfileSearchRecordType(profileSearchRecordType));
    }


    /**
     *  Gets the supported <code> ProfileEntryCondition </code> record types. 
     *
     *  @return a list containing the supported <code> ProfileEntryCondition 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntryConditionRecordTypes() {
        return (getAdapteeManager().getProfileEntryConditionRecordTypes());
    }


    /**
     *  Tests if the given <code> ProfileEntryCondition </code> record type is 
     *  supported. 
     *
     *  @param  profileEntryConditionRecordType a <code> Type </code> 
     *          indicating an <code> ProfileEntryCondition </code> record type 
     *  @return <code> true </code> if the given profile entry condition 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryConditionRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntryConditionRecordType(org.osid.type.Type profileEntryConditionRecordType) {
        return (getAdapteeManager().supportsProfileEntryConditionRecordType(profileEntryConditionRecordType));
    }


    /**
     *  Gets a <code> ProfilingSession </code> which is responsible for 
     *  performing profile checks. 
     *
     *  @param  proxy a proxy 
     *  @return a profiling session for this service 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfiling() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfilingSession getProfilingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfilingSession(proxy));
    }


    /**
     *  Gets a <code> ProfilingSession </code> which is responsible for 
     *  performing profile checks for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfilingSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsProfiling() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfilingSession getProfilingSessionForProfile(org.osid.id.Id profileId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfilingSessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryLookupSession getProfileEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryLookupSession getProfileEntryLookupSessionForProfile(org.osid.id.Id profileId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryLookupSessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuerySession getProfileEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  query service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuerySession getProfileEntryQuerySessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryQuerySessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySearchSession getProfileEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  search service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySearchSession getProfileEntrySearchSessionForProfile(org.osid.id.Id profileId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntrySearchSessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryAdminSession getProfileEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  admin service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryAdminSession getProfileEntryAdminSessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryAdminSessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  entry changes. 
     *
     *  @param  profileEntryReceiver the profile entry receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryNotificationSession getProfileEntryNotificationSession(org.osid.profile.ProfileEntryReceiver profileEntryReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryNotificationSession(profileEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  notification service for the given profile. 
     *
     *  @param  profileEntryReceiver the profile entry receiver interface 
     *  @param  profileId the profile entry receiver 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileEntryReceiver, 
     *          profileId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryNotificationSession getProfileEntryNotificationSessionForProfile(org.osid.profile.ProfileEntryReceiver profileEntryReceiver, 
                                                                                                         org.osid.id.Id profileId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryNotificationSessionForProfile(profileEntryReceiver, profileId, proxy));
    }


    /**
     *  Gets the session for retrieving profile entry to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryProfileSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryProfile() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryProfileSession getProfileEntryProfileSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryProfileSession(proxy));
    }


    /**
     *  Gets the session for assigning profile entry to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryProfileAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryProfileAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryProfileAssignmentSession getProfileEntryProfileAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntryProfileAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic profile entry profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntrySmartProfileSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySmartProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySmartProfileSession getProfileEntrySmartProfileSession(org.osid.id.Id profileId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileEntrySmartProfileSession(profileId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemLookupSession getProfileItemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemLookupSession getProfileItemLookupSessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemLookupSessionForProfile(profileId, proxy));
    }


    /**
     *  Gets a profile item query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuerySession getProfileItemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemQuerySession(proxy));
    }


    /**
     *  Gets a profile item query session for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuerySession getProfileItemQuerySessionForProfile(org.osid.id.Id profileId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemQuerySessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchSession getProfileItemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  search service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchSession getProfileItemSearchSessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemSearchSessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemAdminSession getProfileItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  admin service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemAdminSession getProfileItemAdminSessionForProfile(org.osid.id.Id profileId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemAdminSessionForProfile(profileId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  item changes. 
     *
     *  @param  profileItemReceiver the profile item receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileItemReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemNotificationSession getProfileItemNotificationSession(org.osid.profile.ProfileItemReceiver profileItemReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemNotificationSession(profileItemReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  notification service for the given profile. 
     *
     *  @param  profileItemReceiver the profile item receiver 
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileItemReceiver, 
     *          profileId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemNotificationSession getProfileItemNotificationSessionForProfile(org.osid.profile.ProfileItemReceiver profileItemReceiver, 
                                                                                                       org.osid.id.Id profileId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemNotificationSessionForProfile(profileItemReceiver, profileId, proxy));
    }


    /**
     *  Gets the session for retrieving profile item to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemProfileSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemProfile() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemProfileSession getProfileItemProfileSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemProfileSession(proxy));
    }


    /**
     *  Gets the session for assigning profile item to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemProfileAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemProfileAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemProfileSession getProfileItemProfileAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemProfileAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic profile item profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemSmartProfileSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSmartProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSmartProfileSession getProfileItemSmartProfileSession(org.osid.id.Id profileId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileItemSmartProfileSession(profileId, proxy));
    }


    /**
     *  Gets the OsidSession associated with the profile lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileLookupSession getProfileLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileLookupSession(proxy));
    }


    /**
     *  Gets the profile query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuerySession getProfileQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileQuerySession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the profile search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileSearchSession getProfileSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileSearchSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the profile administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileAdminSession getProfileAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileAdminSession(proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  service changes. 
     *
     *  @param  profileReceiver the profile receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileNotificationSession getProfileNotificationSession(org.osid.profile.ProfileReceiver profileReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileNotificationSession(profileReceiver, proxy));
    }


    /**
     *  Gets the session traversing profile hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileHierarchySession getProfileHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileHierarchySession(proxy));
    }


    /**
     *  Gets the session designing profile hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileHierarchyDesignSession getProfileHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> ProfileBatchProxyManager. </code> 
     *
     *  @return a <code> ProfileBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileBatchProxyManager getProfileBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileBatchProxyManager());
    }


    /**
     *  Gets a <code> ProfileRulesProxyManager. </code> 
     *
     *  @return a <code> ProfileRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileRules() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileRulesProxyManager getProfileRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
