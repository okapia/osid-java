//
// AbstractMapRelationshipEnablerLookupSession
//
//    A simple framework for providing a RelationshipEnabler lookup service
//    backed by a fixed collection of relationship enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RelationshipEnabler lookup service backed by a
 *  fixed collection of relationship enablers. The relationship enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RelationshipEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRelationshipEnablerLookupSession
    extends net.okapia.osid.jamocha.relationship.rules.spi.AbstractRelationshipEnablerLookupSession
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.relationship.rules.RelationshipEnabler> relationshipEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.relationship.rules.RelationshipEnabler>());


    /**
     *  Makes a <code>RelationshipEnabler</code> available in this session.
     *
     *  @param  relationshipEnabler a relationship enabler
     *  @throws org.osid.NullArgumentException <code>relationshipEnabler<code>
     *          is <code>null</code>
     */

    protected void putRelationshipEnabler(org.osid.relationship.rules.RelationshipEnabler relationshipEnabler) {
        this.relationshipEnablers.put(relationshipEnabler.getId(), relationshipEnabler);
        return;
    }


    /**
     *  Makes an array of relationship enablers available in this session.
     *
     *  @param  relationshipEnablers an array of relationship enablers
     *  @throws org.osid.NullArgumentException <code>relationshipEnablers<code>
     *          is <code>null</code>
     */

    protected void putRelationshipEnablers(org.osid.relationship.rules.RelationshipEnabler[] relationshipEnablers) {
        putRelationshipEnablers(java.util.Arrays.asList(relationshipEnablers));
        return;
    }


    /**
     *  Makes a collection of relationship enablers available in this session.
     *
     *  @param  relationshipEnablers a collection of relationship enablers
     *  @throws org.osid.NullArgumentException <code>relationshipEnablers<code>
     *          is <code>null</code>
     */

    protected void putRelationshipEnablers(java.util.Collection<? extends org.osid.relationship.rules.RelationshipEnabler> relationshipEnablers) {
        for (org.osid.relationship.rules.RelationshipEnabler relationshipEnabler : relationshipEnablers) {
            this.relationshipEnablers.put(relationshipEnabler.getId(), relationshipEnabler);
        }

        return;
    }


    /**
     *  Removes a RelationshipEnabler from this session.
     *
     *  @param  relationshipEnablerId the <code>Id</code> of the relationship enabler
     *  @throws org.osid.NullArgumentException <code>relationshipEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeRelationshipEnabler(org.osid.id.Id relationshipEnablerId) {
        this.relationshipEnablers.remove(relationshipEnablerId);
        return;
    }


    /**
     *  Gets the <code>RelationshipEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  relationshipEnablerId <code>Id</code> of the <code>RelationshipEnabler</code>
     *  @return the relationshipEnabler
     *  @throws org.osid.NotFoundException <code>relationshipEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>relationshipEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnabler getRelationshipEnabler(org.osid.id.Id relationshipEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.relationship.rules.RelationshipEnabler relationshipEnabler = this.relationshipEnablers.get(relationshipEnablerId);
        if (relationshipEnabler == null) {
            throw new org.osid.NotFoundException("relationshipEnabler not found: " + relationshipEnablerId);
        }

        return (relationshipEnabler);
    }


    /**
     *  Gets all <code>RelationshipEnablers</code>. In plenary mode, the returned
     *  list contains all known relationshipEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  relationshipEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RelationshipEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.rules.relationshipenabler.ArrayRelationshipEnablerList(this.relationshipEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relationshipEnablers.clear();
        super.close();
        return;
    }
}
