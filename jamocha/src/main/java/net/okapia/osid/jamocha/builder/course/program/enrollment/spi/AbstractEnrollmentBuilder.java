//
// AbstractEnrollment.java
//
//     Defines an Enrollment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.enrollment.spi;


/**
 *  Defines an <code>Enrollment</code> builder.
 */

public abstract class AbstractEnrollmentBuilder<T extends AbstractEnrollmentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.program.enrollment.EnrollmentMiter enrollment;


    /**
     *  Constructs a new <code>AbstractEnrollmentBuilder</code>.
     *
     *  @param enrollment the enrollment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEnrollmentBuilder(net.okapia.osid.jamocha.builder.course.program.enrollment.EnrollmentMiter enrollment) {
        super(enrollment);
        this.enrollment = enrollment;
        return;
    }


    /**
     *  Builds the enrollment.
     *
     *  @return the new enrollment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.program.Enrollment build() {
        (new net.okapia.osid.jamocha.builder.validator.course.program.enrollment.EnrollmentValidator(getValidations())).validate(this.enrollment);
        return (new net.okapia.osid.jamocha.builder.course.program.enrollment.ImmutableEnrollment(this.enrollment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the enrollment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.program.enrollment.EnrollmentMiter getMiter() {
        return (this.enrollment);
    }


    /**
     *  Sets the program offering.
     *
     *  @param programOffering a program offering
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>programOffering</code> is <code>null</code>
     */

    public T programOffering(org.osid.course.program.ProgramOffering programOffering) {
        getMiter().setProgramOffering(programOffering);
        return (self());
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public T student(org.osid.resource.Resource student) {
        getMiter().setStudent(student);
        return (self());
    }


    /**
     *  Adds an Enrollment record.
     *
     *  @param record an enrollment record
     *  @param recordType the type of enrollment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.program.records.EnrollmentRecord record, org.osid.type.Type recordType) {
        getMiter().addEnrollmentRecord(record, recordType);
        return (self());
    }
}       


