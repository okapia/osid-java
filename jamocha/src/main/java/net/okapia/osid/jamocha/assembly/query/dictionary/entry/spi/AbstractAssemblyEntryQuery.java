//
// AbstractAssemblyEntryQuery.java
//
//     An EntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.dictionary.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EntryQuery that stores terms.
 */

public abstract class AbstractAssemblyEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.dictionary.EntryQuery,
               org.osid.dictionary.EntryQueryInspector,
               org.osid.dictionary.EntrySearchOrder {

    private final java.util.Collection<org.osid.dictionary.records.EntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.dictionary.records.EntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.dictionary.records.EntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the <code> Type </code> for querying keys of a given type. 
     *
     *  @param  keyType the key <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> keyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchKeyType(org.osid.type.Type keyType, boolean match) {
        getAssembler().addTypeTerm(getKeyTypeColumn(), keyType, match);
        return;
    }


    /**
     *  Clears the key type terms. 
     */

    @OSID @Override
    public void clearKeyTypeTerms() {
        getAssembler().clearTerms(getKeyTypeColumn());
        return;
    }


    /**
     *  Gets the key type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getKeyTypeTerms() {
        return (getAssembler().getTypeTerms(getKeyTypeColumn()));
    }


    /**
     *  Specifies a preference for ordering the results by key type. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByKeyType(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getKeyTypeColumn(), style);
        return;
    }


    /**
     *  Gets the KeyType column name.
     *
     * @return the column name
     */

    protected String getKeyTypeColumn() {
        return ("key_type");
    }


    /**
     *  Matches entries of this key. 
     *
     *  @param  key the key 
     *  @param  keyType the key <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> key </code> or <code> 
     *          keyType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchKey(java.lang.Object key, org.osid.type.Type keyType, 
                         boolean match) {
        getAssembler().addObjectTerm(getKeyColumn(), key, keyType, match);
        return;
    }


    /**
     *  Clears the key terms. 
     */

    @OSID @Override
    public void clearKeyTerms() {
        getAssembler().clearTerms(getKeyColumn());
        return;
    }


    /**
     *  Gets the key query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.ObjectTerm[] getKeyTerms() {
        return (getAssembler().getObjectTerms(getKeyColumn()));
    }


    /**
     *  Specifies a preference for ordering the results by key. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByKey(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getKeyColumn(), style);
        return;
    }


    /**
     *  Gets the Key column name.
     *
     * @return the column name
     */

    protected String getKeyColumn() {
        return ("key");
    }


    /**
     *  Sets the <code> Type </code> of this entry value. 
     *
     *  @param  valueType the value <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> valueType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueType(org.osid.type.Type valueType, boolean match) {
        getAssembler().addTypeTerm(getValueTypeColumn(), valueType, match);
        return;
    }


    /**
     *  Clears the value type terms. 
     */

    @OSID @Override
    public void clearValueTypeTerms() {
        getAssembler().clearTerms(getValueTypeColumn());
        return;
    }


    /**
     *  Gets the value type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueTypeTerms() {
        return (getAssembler().getTypeTerms(getValueTypeColumn()));
    }


    /**
     *  Specifies a preference for ordering the results by value type. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValueType(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getValueTypeColumn(), style);
        return;
    }


    /**
     *  Gets the ValueType column name.
     *
     * @return the column name
     */

    protected String getValueTypeColumn() {
        return ("value_type");
    }


    /**
     *  Sets the value in this entry. 
     *
     *  @param  value the value 
     *  @param  valueType the value <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> or <code> 
     *          valueType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchValue(java.lang.Object value, 
                           org.osid.type.Type valueType, boolean match) {
        getAssembler().addObjectTerm(getValueColumn(), value, valueType, match);
        return;
    }


    /**
     *  Clears the value terms. 
     */

    @OSID @Override
    public void clearValueTerms() {
        getAssembler().clearTerms(getValueColumn());
        return;
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.ObjectTerm[] getValueTerms() {
        return (getAssembler().getObjectTerms(getValueColumn()));
    }


    /**
     *  Specifies a preference for ordering the results by value. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getValueColumn(), style);
        return;
    }


    /**
     *  Gets the Value column name.
     *
     * @return the column name
     */

    protected String getValueColumn() {
        return ("value");
    }


    /**
     *  Sets the dictionary <code> Id </code> for this query. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDictionaryId(org.osid.id.Id dictionaryId, boolean match) {
        getAssembler().addIdTerm(getDictionaryIdColumn(), dictionaryId, match);
        return;
    }


    /**
     *  Clears the dictionary <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDictionaryIdTerms() {
        getAssembler().clearTerms(getDictionaryIdColumn());
        return;
    }


    /**
     *  Gets the dictionary <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDictionaryIdTerms() {
        return (getAssembler().getIdTerms(getDictionaryIdColumn()));
    }


    /**
     *  Gets the DictionaryId column name.
     *
     * @return the column name
     */

    protected String getDictionaryIdColumn() {
        return ("dictionary_id");
    }


    /**
     *  Tests if a <code> DictionaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a dictionary query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dictionary. Multiple retrievals produce a nested 
     *  boolean <code> OR </code> term. 
     *
     *  @return the dictionary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuery getDictionaryQuery() {
        throw new org.osid.UnimplementedException("supportsDictionaryQuery() is false");
    }


    /**
     *  Clears the dictionary terms. 
     */

    @OSID @Override
    public void clearDictionaryTerms() {
        getAssembler().clearTerms(getDictionaryColumn());
        return;
    }


    /**
     *  Gets the dictionary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQueryInspector[] getDictionaryTerms() {
        return (new org.osid.dictionary.DictionaryQueryInspector[0]);
    }


    /**
     *  Gets the Dictionary column name.
     *
     * @return the column name
     */

    protected String getDictionaryColumn() {
        return ("dictionary");
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.dictionary.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.EntryQueryRecord getEntryQueryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.EntryQueryInspectorRecord getEntryQueryInspectorRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.EntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param entryRecordType the entry record type
     *  @return the entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.EntrySearchOrderRecord getEntrySearchOrderRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.EntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryQueryRecord the entry query record
     *  @param entryQueryInspectorRecord the entry query inspector
     *         record
     *  @param entrySearchOrderRecord the entry search order record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryQueryRecord</code>,
     *          <code>entryQueryInspectorRecord</code>,
     *          <code>entrySearchOrderRecord</code> or
     *          <code>entryRecordTypeentry</code> is
     *          <code>null</code>
     */
            
    protected void addEntryRecords(org.osid.dictionary.records.EntryQueryRecord entryQueryRecord, 
                                      org.osid.dictionary.records.EntryQueryInspectorRecord entryQueryInspectorRecord, 
                                      org.osid.dictionary.records.EntrySearchOrderRecord entrySearchOrderRecord, 
                                      org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);

        nullarg(entryQueryRecord, "entry query record");
        nullarg(entryQueryInspectorRecord, "entry query inspector record");
        nullarg(entrySearchOrderRecord, "entry search odrer record");

        this.queryRecords.add(entryQueryRecord);
        this.queryInspectorRecords.add(entryQueryInspectorRecord);
        this.searchOrderRecords.add(entrySearchOrderRecord);
        
        return;
    }
}
