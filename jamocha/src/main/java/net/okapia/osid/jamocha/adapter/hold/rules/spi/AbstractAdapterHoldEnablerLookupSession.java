//
// AbstractAdapterHoldEnablerLookupSession.java
//
//    A HoldEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A HoldEnabler lookup session adapter.
 */

public abstract class AbstractAdapterHoldEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hold.rules.HoldEnablerLookupSession {

    private final org.osid.hold.rules.HoldEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterHoldEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterHoldEnablerLookupSession(org.osid.hold.rules.HoldEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Oubliette/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Oubliette Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.session.getOublietteId());
    }


    /**
     *  Gets the {@code Oubliette} associated with this session.
     *
     *  @return the {@code Oubliette} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOubliette());
    }


    /**
     *  Tests if this user can perform {@code HoldEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupHoldEnablers() {
        return (this.session.canLookupHoldEnablers());
    }


    /**
     *  A complete view of the {@code HoldEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeHoldEnablerView() {
        this.session.useComparativeHoldEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code HoldEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryHoldEnablerView() {
        this.session.usePlenaryHoldEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include hold enablers in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.session.useFederatedOublietteView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.session.useIsolatedOublietteView();
        return;
    }
    

    /**
     *  Only active hold enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveHoldEnablerView() {
        this.session.useActiveHoldEnablerView();
        return;
    }


    /**
     *  Active and inactive hold enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusHoldEnablerView() {
        this.session.useAnyStatusHoldEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code HoldEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code HoldEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code HoldEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param holdEnablerId {@code Id} of the {@code HoldEnabler}
     *  @return the hold enabler
     *  @throws org.osid.NotFoundException {@code holdEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code holdEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnabler getHoldEnabler(org.osid.id.Id holdEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldEnabler(holdEnablerId));
    }


    /**
     *  Gets a {@code HoldEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  holdEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code HoldEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code HoldEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code holdEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByIds(org.osid.id.IdList holdEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldEnablersByIds(holdEnablerIds));
    }


    /**
     *  Gets a {@code HoldEnablerList} corresponding to the given
     *  hold enabler genus {@code Type} which does not include
     *  hold enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerGenusType a holdEnabler genus type 
     *  @return the returned {@code HoldEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code holdEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByGenusType(org.osid.type.Type holdEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldEnablersByGenusType(holdEnablerGenusType));
    }


    /**
     *  Gets a {@code HoldEnablerList} corresponding to the given
     *  hold enabler genus {@code Type} and include any additional
     *  hold enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerGenusType a holdEnabler genus type 
     *  @return the returned {@code HoldEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code holdEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByParentGenusType(org.osid.type.Type holdEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldEnablersByParentGenusType(holdEnablerGenusType));
    }


    /**
     *  Gets a {@code HoldEnablerList} containing the given
     *  hold enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerRecordType a holdEnabler record type 
     *  @return the returned {@code HoldEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code holdEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByRecordType(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldEnablersByRecordType(holdEnablerRecordType));
    }


    /**
     *  Gets a {@code HoldEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible
     *  through this session.
     *  
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code HoldEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code HoldEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible
     *  through this session.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code HoldEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getHoldEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code HoldEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @return a list of {@code HoldEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHoldEnablers());
    }
}
