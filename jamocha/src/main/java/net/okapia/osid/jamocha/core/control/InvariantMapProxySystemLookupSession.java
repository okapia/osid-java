//
// InvariantMapProxySystemLookupSession
//
//    Implements a System lookup service backed by a fixed
//    collection of systems. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a System lookup service backed by a fixed
 *  collection of systems. The systems are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySystemLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapSystemLookupSession
    implements org.osid.control.SystemLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySystemLookupSession} with no
     *  systems.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxySystemLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySystemLookupSession} with a
     *  single system.
     *
     *  @param system a single system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySystemLookupSession(org.osid.control.System system, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSystem(system);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySystemLookupSession} using
     *  an array of systems.
     *
     *  @param systems an array of systems
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code systems} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySystemLookupSession(org.osid.control.System[] systems, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSystems(systems);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySystemLookupSession} using a
     *  collection of systems.
     *
     *  @param systems a collection of systems
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code systems} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySystemLookupSession(java.util.Collection<? extends org.osid.control.System> systems,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSystems(systems);
        return;
    }
}
