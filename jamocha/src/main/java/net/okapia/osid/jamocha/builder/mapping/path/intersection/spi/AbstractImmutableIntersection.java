//
// AbstractImmutableIntersection.java
//
//     Wraps a mutable Intersection to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.intersection.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Intersection</code> to hide modifiers. This
 *  wrapper provides an immutized Intersection from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying intersection whose state changes are visible.
 */

public abstract class AbstractImmutableIntersection
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.mapping.path.Intersection {

    private final org.osid.mapping.path.Intersection intersection;


    /**
     *  Constructs a new <code>AbstractImmutableIntersection</code>.
     *
     *  @param intersection the intersection to immutablize
     *  @throws org.osid.NullArgumentException <code>intersection</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableIntersection(org.osid.mapping.path.Intersection intersection) {
        super(intersection);
        this.intersection = intersection;
        return;
    }


    /**
     *  Gets a single corrdinate to represent the intersection. 
     *
     *  @return the coordinate 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinate() {
        return (this.intersection.getCoordinate());
    }


    /**
     *  Gets the intersecting path <code> Ids. </code> 
     *
     *  @return the path <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPathIds() {
        return (this.intersection.getPathIds());
    }


    /**
     *  Gets the intersecting paths. 
     *
     *  @return the paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPaths()
        throws org.osid.OperationFailedException {

        return (this.intersection.getPaths());
    }


    /**
     *  Tests if this intersection is a rotary. 
     *
     *  @return <code> true </code> if this intersection is a rotary, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isRotary() {
        return (this.intersection.isRotary());
    }


    /**
     *  Tests if this intersection is a fork or exit. 
     *
     *  @return <code> true </code> if this intersection is a fork, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isFork() {
        return (this.intersection.isFork());
    }


    /**
     *  Gets the intersection record corresponding to the given <code> 
     *  Intersection </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record.. The <code> 
     *  intersectionRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(intersectionRecordType) </code> is <code> true </code> . 
     *
     *  @param  intersectionRecordType the type of intersection record to 
     *          retrieve 
     *  @return the intersection record 
     *  @throws org.osid.NullArgumentException <code> intersectionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(intersectionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.records.IntersectionRecord getIntersectionRecord(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException {

        return (this.intersection.getIntersectionRecord(intersectionRecordType));
    }
}

