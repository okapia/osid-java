//
// MutableMapProxyCanonicalUnitProcessorLookupSession
//
//    Implements a CanonicalUnitProcessor lookup service backed by a collection of
//    canonicalUnitProcessors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements a CanonicalUnitProcessor lookup service backed by a collection of
 *  canonicalUnitProcessors. The canonicalUnitProcessors are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of canonical unit processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCanonicalUnitProcessorLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractMapCanonicalUnitProcessorLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyCanonicalUnitProcessorLookupSession}
     *  with no canonical unit processors.
     *
     *  @param catalogue the catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.proxy.Proxy proxy) {
        setCatalogue(catalogue);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCanonicalUnitProcessorLookupSession} with a
     *  single canonical unit processor.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessor a canonical unit processor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnitProcessor}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putCanonicalUnitProcessor(canonicalUnitProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCanonicalUnitProcessorLookupSession} using an
     *  array of canonical unit processors.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessors an array of canonical unit processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnitProcessors}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.rules.CanonicalUnitProcessor[] canonicalUnitProcessors, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCanonicalUnitProcessorLookupSession} using a
     *  collection of canonical unit processors.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessors a collection of canonical unit processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnitProcessors}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                                java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors,
                                                org.osid.proxy.Proxy proxy) {
   
        this(catalogue, proxy);
        setSessionProxy(proxy);
        putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }

    
    /**
     *  Makes a {@code CanonicalUnitProcessor} available in this session.
     *
     *  @param canonicalUnitProcessor an canonical unit processor
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnitProcessor(org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor) {
        super.putCanonicalUnitProcessor(canonicalUnitProcessor);
        return;
    }


    /**
     *  Makes an array of canonicalUnitProcessors available in this session.
     *
     *  @param canonicalUnitProcessors an array of canonical unit processors
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnitProcessors(org.osid.offering.rules.CanonicalUnitProcessor[] canonicalUnitProcessors) {
        super.putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }


    /**
     *  Makes collection of canonical unit processors available in this session.
     *
     *  @param canonicalUnitProcessors
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnitProcessors(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors) {
        super.putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }


    /**
     *  Removes a CanonicalUnitProcessor from this session.
     *
     *  @param canonicalUnitProcessorId the {@code Id} of the canonical unit processor
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId) {
        super.removeCanonicalUnitProcessor(canonicalUnitProcessorId);
        return;
    }    
}
