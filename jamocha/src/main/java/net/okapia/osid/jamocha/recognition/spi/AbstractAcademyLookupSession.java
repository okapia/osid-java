//
// AbstractAcademyLookupSession.java
//
//    A starter implementation framework for providing an Academy
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an Academy
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAcademies(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAcademyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.recognition.AcademyLookupSession {

    private boolean pedantic  = false;
    private org.osid.recognition.Academy academy = new net.okapia.osid.jamocha.nil.recognition.academy.UnknownAcademy();
    


    /**
     *  Tests if this user can perform <code>Academy</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAcademies() {
        return (true);
    }


    /**
     *  A complete view of the <code>Academy</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAcademyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Academy</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAcademyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Academy</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Academy</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Academy</code> and retained for
     *  compatibility.
     *
     *  @param  academyId <code>Id</code> of the
     *          <code>Academy</code>
     *  @return the academy
     *  @throws org.osid.NotFoundException <code>academyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>academyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.recognition.AcademyList academies = getAcademies()) {
            while (academies.hasNext()) {
                org.osid.recognition.Academy academy = academies.getNextAcademy();
                if (academy.getId().equals(academyId)) {
                    return (academy);
                }
            }
        } 

        throw new org.osid.NotFoundException(academyId + " not found");
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  academies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Academies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAcademies()</code>.
     *
     *  @param  academyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>academyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByIds(org.osid.id.IdList academyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.recognition.Academy> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = academyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAcademy(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("academy " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.recognition.academy.LinkedAcademyList(ret));
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  academy genus <code>Type</code> which does not include
     *  academies of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAcademies()</code>.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.academy.AcademyGenusFilterList(getAcademies(), academyGenusType));
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  academy genus <code>Type</code> and include any additional
     *  academies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAcademies()</code>.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByParentGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAcademiesByGenusType(academyGenusType));
    }


    /**
     *  Gets an <code>AcademyList</code> containing the given academy
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAcademies()</code>.
     *
     *  @param  academyRecordType an academy record type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByRecordType(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.academy.AcademyRecordFilterList(getAcademies(), academyRecordType));
    }


    /**
     *  Gets an <code>AcademyList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session.
     *
     *  @param resourceId a resource <code>Id</code>
     *  @return the returned <code>Academy</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.recognition.academy.AcademyProviderFilterList(getAcademies(), resourceId));
    }


    /**
     *  Gets all <code>Academies</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Academies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.recognition.AcademyList getAcademies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the academy list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of academies
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.recognition.AcademyList filterAcademiesOnViews(org.osid.recognition.AcademyList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
