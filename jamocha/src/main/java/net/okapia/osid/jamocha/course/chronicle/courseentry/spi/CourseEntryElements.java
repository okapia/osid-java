//
// CourseEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.courseentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CourseEntryElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the CourseEntryElement Id.
     *
     *  @return the course entry element Id
     */

    public static org.osid.id.Id getCourseEntryEntityId() {
        return (makeEntityId("osid.course.chronicle.CourseEntry"));
    }


    /**
     *  Gets the StudentId element Id.
     *
     *  @return the StudentId element Id
     */

    public static org.osid.id.Id getStudentId() {
        return (makeElementId("osid.course.chronicle.courseentry.StudentId"));
    }


    /**
     *  Gets the Student element Id.
     *
     *  @return the Student element Id
     */

    public static org.osid.id.Id getStudent() {
        return (makeElementId("osid.course.chronicle.courseentry.Student"));
    }


    /**
     *  Gets the CourseId element Id.
     *
     *  @return the CourseId element Id
     */

    public static org.osid.id.Id getCourseId() {
        return (makeElementId("osid.course.chronicle.courseentry.CourseId"));
    }


    /**
     *  Gets the Course element Id.
     *
     *  @return the Course element Id
     */

    public static org.osid.id.Id getCourse() {
        return (makeElementId("osid.course.chronicle.courseentry.Course"));
    }


    /**
     *  Gets the TermId element Id.
     *
     *  @return the TermId element Id
     */

    public static org.osid.id.Id getTermId() {
        return (makeElementId("osid.course.chronicle.courseentry.TermId"));
    }


    /**
     *  Gets the Term element Id.
     *
     *  @return the Term element Id
     */

    public static org.osid.id.Id getTerm() {
        return (makeElementId("osid.course.chronicle.courseentry.Term"));
    }


    /**
     *  Gets the CreditScaleId element Id.
     *
     *  @return the CreditScaleId element Id
     */

    public static org.osid.id.Id getCreditScaleId() {
        return (makeElementId("osid.course.chronicle.courseentry.CreditScaleId"));
    }


    /**
     *  Gets the CreditScale element Id.
     *
     *  @return the CreditScale element Id
     */

    public static org.osid.id.Id getCreditScale() {
        return (makeElementId("osid.course.chronicle.courseentry.CreditScale"));
    }


    /**
     *  Gets the CreditsEarned element Id.
     *
     *  @return the CreditsEarned element Id
     */

    public static org.osid.id.Id getCreditsEarned() {
        return (makeElementId("osid.course.chronicle.courseentry.CreditsEarned"));
    }


    /**
     *  Gets the GradeId element Id.
     *
     *  @return the GradeId element Id
     */

    public static org.osid.id.Id getGradeId() {
        return (makeElementId("osid.course.chronicle.courseentry.GradeId"));
    }


    /**
     *  Gets the Grade element Id.
     *
     *  @return the Grade element Id
     */

    public static org.osid.id.Id getGrade() {
        return (makeElementId("osid.course.chronicle.courseentry.Grade"));
    }


    /**
     *  Gets the ScoreScaleId element Id.
     *
     *  @return the ScoreScaleId element Id
     */

    public static org.osid.id.Id getScoreScaleId() {
        return (makeElementId("osid.course.chronicle.courseentry.ScoreScaleId"));
    }


    /**
     *  Gets the ScoreScale element Id.
     *
     *  @return the ScoreScale element Id
     */

    public static org.osid.id.Id getScoreScale() {
        return (makeElementId("osid.course.chronicle.courseentry.ScoreScale"));
    }


    /**
     *  Gets the Score element Id.
     *
     *  @return the Score element Id
     */

    public static org.osid.id.Id getScore() {
        return (makeElementId("osid.course.chronicle.courseentry.Score"));
    }


    /**
     *  Gets the RegistrationIds element Id.
     *
     *  @return the RegistrationIds element Id
     */

    public static org.osid.id.Id getRegistrationIds() {
        return (makeElementId("osid.course.chronicle.courseentry.RegistrationIds"));
    }


    /**
     *  Gets the Registrations element Id.
     *
     *  @return the Registrations element Id
     */

    public static org.osid.id.Id getRegistrations() {
        return (makeElementId("osid.course.chronicle.courseentry.Registrations"));
    }


    /**
     *  Gets the Complete element Id.
     *
     *  @return the Complete element Id
     */

    public static org.osid.id.Id getComplete() {
        return (makeElementId("osid.course.chronicle.courseentry.Complete"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.chronicle.courseentry.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.chronicle.courseentry.CourseCatalog"));
    }
}
