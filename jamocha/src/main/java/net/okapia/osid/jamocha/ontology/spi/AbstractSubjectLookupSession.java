//
// AbstractSubjectLookupSession.java
//
//    A starter implementation framework for providing a Subject
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Subject
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSubjects(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSubjectLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ontology.SubjectLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.ontology.Ontology ontology = new net.okapia.osid.jamocha.nil.ontology.ontology.UnknownOntology();
    

    /**
     *  Gets the <code>Ontology/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Ontology Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.ontology.getId());
    }


    /**
     *  Gets the <code>Ontology</code> associated with this 
     *  session.
     *
     *  @return the <code>Ontology</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.ontology);
    }


    /**
     *  Sets the <code>Ontology</code>.
     *
     *  @param  ontology the ontology for this session
     *  @throws org.osid.NullArgumentException <code>ontology</code>
     *          is <code>null</code>
     */

    protected void setOntology(org.osid.ontology.Ontology ontology) {
        nullarg(ontology, "ontology");
        this.ontology = ontology;
        return;
    }


    /**
     *  Tests if this user can perform <code>Subject</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSubjects() {
        return (true);
    }


    /**
     *  A complete view of the <code>Subject</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSubjectView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Subject</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySubjectView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subjects in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Subject</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Subject</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Subject</code> and
     *  retained for compatibility.
     *
     *  @param  subjectId <code>Id</code> of the
     *          <code>Subject</code>
     *  @return the subject
     *  @throws org.osid.NotFoundException <code>subjectId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>subjectId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Subject getSubject(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ontology.SubjectList subjects = getSubjects()) {
            while (subjects.hasNext()) {
                org.osid.ontology.Subject subject = subjects.getNextSubject();
                if (subject.getId().equals(subjectId)) {
                    return (subject);
                }
            }
        } 

        throw new org.osid.NotFoundException(subjectId + " not found");
    }


    /**
     *  Gets a <code>SubjectList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  subjects specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Subjects</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSubjects()</code>.
     *
     *  @param  subjectIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>subjectIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByIds(org.osid.id.IdList subjectIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ontology.Subject> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = subjectIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSubject(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("subject " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ontology.subject.LinkedSubjectList(ret));
    }


    /**
     *  Gets a <code>SubjectList</code> corresponding to the given
     *  subject genus <code>Type</code> which does not include
     *  subjects of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSubjects()</code>.
     *
     *  @param  subjectGenusType a subject genus type 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByGenusType(org.osid.type.Type subjectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.subject.SubjectGenusFilterList(getSubjects(), subjectGenusType));
    }


    /**
     *  Gets a <code>SubjectList</code> corresponding to the given
     *  subject genus <code>Type</code> and include any additional
     *  subjects with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSubjects()</code>.
     *
     *  @param  subjectGenusType a subject genus type 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByParentGenusType(org.osid.type.Type subjectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSubjectsByGenusType(subjectGenusType));
    }


    /**
     *  Gets a <code>SubjectList</code> containing the given
     *  subject record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSubjects()</code>.
     *
     *  @param  subjectRecordType a subject record type 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByRecordType(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.subject.SubjectRecordFilterList(getSubjects(), subjectRecordType));
    }


    /**
     *  Gets all <code>Subjects</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Subjects</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.ontology.SubjectList getSubjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the subject list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of subjects
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.ontology.SubjectList filterSubjectsOnViews(org.osid.ontology.SubjectList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
