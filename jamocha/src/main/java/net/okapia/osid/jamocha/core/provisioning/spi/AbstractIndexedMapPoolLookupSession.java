//
// AbstractIndexedMapPoolLookupSession.java
//
//    A simple framework for providing a Pool lookup service
//    backed by a fixed collection of pools with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Pool lookup service backed by a
 *  fixed collection of pools. The pools are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some pools may be compatible
 *  with more types than are indicated through these pool
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Pools</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPoolLookupSession
    extends AbstractMapPoolLookupSession
    implements org.osid.provisioning.PoolLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Pool> poolsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Pool>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Pool> poolsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Pool>());


    /**
     *  Makes a <code>Pool</code> available in this session.
     *
     *  @param  pool a pool
     *  @throws org.osid.NullArgumentException <code>pool<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPool(org.osid.provisioning.Pool pool) {
        super.putPool(pool);

        this.poolsByGenus.put(pool.getGenusType(), pool);
        
        try (org.osid.type.TypeList types = pool.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolsByRecord.put(types.getNextType(), pool);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a pool from this session.
     *
     *  @param poolId the <code>Id</code> of the pool
     *  @throws org.osid.NullArgumentException <code>poolId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePool(org.osid.id.Id poolId) {
        org.osid.provisioning.Pool pool;
        try {
            pool = getPool(poolId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.poolsByGenus.remove(pool.getGenusType());

        try (org.osid.type.TypeList types = pool.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolsByRecord.remove(types.getNextType(), pool);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePool(poolId);
        return;
    }


    /**
     *  Gets a <code>PoolList</code> corresponding to the given
     *  pool genus <code>Type</code> which does not include
     *  pools of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known pools or an error results. Otherwise,
     *  the returned list may contain only those pools that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  poolGenusType a pool genus type 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByGenusType(org.osid.type.Type poolGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.pool.ArrayPoolList(this.poolsByGenus.get(poolGenusType)));
    }


    /**
     *  Gets a <code>PoolList</code> containing the given
     *  pool record <code>Type</code>. In plenary mode, the
     *  returned list contains all known pools or an error
     *  results. Otherwise, the returned list may contain only those
     *  pools that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  poolRecordType a pool record type 
     *  @return the returned <code>pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByRecordType(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.pool.ArrayPoolList(this.poolsByRecord.get(poolRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolsByGenus.clear();
        this.poolsByRecord.clear();

        super.close();

        return;
    }
}
