//
// AbstractOffsetEventSearchOdrer.java
//
//     Defines an OffsetEventSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code OffsetEventSearchOrder}.
 */

public abstract class AbstractOffsetEventSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.calendaring.OffsetEventSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.OffsetEventSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the fixed start time. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedStartTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the starting reference 
     *  event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartReferenceEvent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> EventSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartReferenceEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartReferenceEventSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getStartReferenceEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStartReferenceEventSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the fixed offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedStartOffset(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the relative weekday 
     *  offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeWeekdayStartOffset(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the relative weekday. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeStartWeekday(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the fixed duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedDuration(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the ending reference 
     *  event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndReferenceEvent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> EventSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReferenceEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndReferenceEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getEndReferenceEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEndReferenceEventSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the fixed offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedEndOffset(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the relative weekday 
     *  offset. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeWeekdayEndOffset(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the relative weekday. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRelativeEndWeekday(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the location 
     *  description. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocationDescription(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the location. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocation(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> LocationSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a location search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a location. 
     *
     *  @return the location search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchOrder getLocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLocationSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  offsetEventRecordType an offset event record type 
     *  @return {@code true} if the offsetEventRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offsetEventRecordType) {
        for (org.osid.calendaring.records.OffsetEventSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  offsetEventRecordType the offset event record type 
     *  @return the offset event search order record
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(offsetEventRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventSearchOrderRecord getOffsetEventSearchOrderRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.OffsetEventSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this offset event. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offsetEventRecord the offset event search odrer record
     *  @param offsetEventRecordType offset event record type
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventRecord} or
     *          {@code offsetEventRecordTypeoffsetEvent} is
     *          {@code null}
     */
            
    protected void addOffsetEventRecord(org.osid.calendaring.records.OffsetEventSearchOrderRecord offsetEventSearchOrderRecord, 
                                     org.osid.type.Type offsetEventRecordType) {

        addRecordType(offsetEventRecordType);
        this.records.add(offsetEventSearchOrderRecord);
        
        return;
    }
}
