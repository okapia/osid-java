//
// AbstractMapRelevancyEnablerLookupSession
//
//    A simple framework for providing a RelevancyEnabler lookup service
//    backed by a fixed collection of relevancy enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RelevancyEnabler lookup service backed by a
 *  fixed collection of relevancy enablers. The relevancy enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RelevancyEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRelevancyEnablerLookupSession
    extends net.okapia.osid.jamocha.ontology.rules.spi.AbstractRelevancyEnablerLookupSession
    implements org.osid.ontology.rules.RelevancyEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ontology.rules.RelevancyEnabler> relevancyEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ontology.rules.RelevancyEnabler>());


    /**
     *  Makes a <code>RelevancyEnabler</code> available in this session.
     *
     *  @param  relevancyEnabler a relevancy enabler
     *  @throws org.osid.NullArgumentException <code>relevancyEnabler<code>
     *          is <code>null</code>
     */

    protected void putRelevancyEnabler(org.osid.ontology.rules.RelevancyEnabler relevancyEnabler) {
        this.relevancyEnablers.put(relevancyEnabler.getId(), relevancyEnabler);
        return;
    }


    /**
     *  Makes an array of relevancy enablers available in this session.
     *
     *  @param  relevancyEnablers an array of relevancy enablers
     *  @throws org.osid.NullArgumentException <code>relevancyEnablers<code>
     *          is <code>null</code>
     */

    protected void putRelevancyEnablers(org.osid.ontology.rules.RelevancyEnabler[] relevancyEnablers) {
        putRelevancyEnablers(java.util.Arrays.asList(relevancyEnablers));
        return;
    }


    /**
     *  Makes a collection of relevancy enablers available in this session.
     *
     *  @param  relevancyEnablers a collection of relevancy enablers
     *  @throws org.osid.NullArgumentException <code>relevancyEnablers<code>
     *          is <code>null</code>
     */

    protected void putRelevancyEnablers(java.util.Collection<? extends org.osid.ontology.rules.RelevancyEnabler> relevancyEnablers) {
        for (org.osid.ontology.rules.RelevancyEnabler relevancyEnabler : relevancyEnablers) {
            this.relevancyEnablers.put(relevancyEnabler.getId(), relevancyEnabler);
        }

        return;
    }


    /**
     *  Removes a RelevancyEnabler from this session.
     *
     *  @param  relevancyEnablerId the <code>Id</code> of the relevancy enabler
     *  @throws org.osid.NullArgumentException <code>relevancyEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeRelevancyEnabler(org.osid.id.Id relevancyEnablerId) {
        this.relevancyEnablers.remove(relevancyEnablerId);
        return;
    }


    /**
     *  Gets the <code>RelevancyEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  relevancyEnablerId <code>Id</code> of the <code>RelevancyEnabler</code>
     *  @return the relevancyEnabler
     *  @throws org.osid.NotFoundException <code>relevancyEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>relevancyEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnabler getRelevancyEnabler(org.osid.id.Id relevancyEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ontology.rules.RelevancyEnabler relevancyEnabler = this.relevancyEnablers.get(relevancyEnablerId);
        if (relevancyEnabler == null) {
            throw new org.osid.NotFoundException("relevancyEnabler not found: " + relevancyEnablerId);
        }

        return (relevancyEnabler);
    }


    /**
     *  Gets all <code>RelevancyEnablers</code>. In plenary mode, the returned
     *  list contains all known relevancyEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  relevancyEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RelevancyEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.rules.relevancyenabler.ArrayRelevancyEnablerList(this.relevancyEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relevancyEnablers.clear();
        super.close();
        return;
    }
}
