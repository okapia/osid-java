//
// AbstractOffsetEventEnablerQuery.java
//
//     A template for making an OffsetEventEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for offset event enablers.
 */

public abstract class AbstractOffsetEventEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.calendaring.rules.OffsetEventEnablerQuery {

    private final java.util.Collection<org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the offset event. 
     *
     *  @param  offsetEventId the offset event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offsetEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledOffsetEventId(org.osid.id.Id offsetEventId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the offset event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledOffsetEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OffsetEventQuery </code> is available. 
     *
     *  @return <code> true </code> if an offset event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledOffsetEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offset event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the offset event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledOffsetEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuery getRuledOffsetEventQuery() {
        throw new org.osid.UnimplementedException("supportsRuledOffsetEventQuery() is false");
    }


    /**
     *  Matches enablers mapped to any offset event. 
     *
     *  @param  match <code> true </code> for enablers mapped to any offset 
     *          event, <code> false </code> to match enablers mapped to no 
     *          offset events 
     */

    @OSID @Override
    public void matchAnyRuledOffsetEvent(boolean match) {
        return;
    }


    /**
     *  Clears the offset event query terms. 
     */

    @OSID @Override
    public void clearRuledOffsetEventTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the calendar. 
     *
     *  @param  calendarHouseId the calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarHouseId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given offset event enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offset event enabler implementing the requested record.
     *
     *  @param offsetEventEnablerRecordType an offset event enabler record type
     *  @return the offset event enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord getOffsetEventEnablerQueryRecord(org.osid.type.Type offsetEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(offsetEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offset event enabler query. 
     *
     *  @param offsetEventEnablerQueryRecord offset event enabler query record
     *  @param offsetEventEnablerRecordType offsetEventEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOffsetEventEnablerQueryRecord(org.osid.calendaring.rules.records.OffsetEventEnablerQueryRecord offsetEventEnablerQueryRecord, 
                                          org.osid.type.Type offsetEventEnablerRecordType) {

        addRecordType(offsetEventEnablerRecordType);
        nullarg(offsetEventEnablerQueryRecord, "offset event enabler query record");
        this.records.add(offsetEventEnablerQueryRecord);        
        return;
    }
}
