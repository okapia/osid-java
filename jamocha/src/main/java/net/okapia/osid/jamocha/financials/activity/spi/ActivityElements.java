//
// ActivityElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.activity.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActivityElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the ActivityElement Id.
     *
     *  @return the activity element Id
     */

    public static org.osid.id.Id getActivityEntityId() {
        return (makeEntityId("osid.financials.Activity"));
    }


    /**
     *  Gets the OrganizationId element Id.
     *
     *  @return the OrganizationId element Id
     */

    public static org.osid.id.Id getOrganizationId() {
        return (makeElementId("osid.financials.activity.OrganizationId"));
    }


    /**
     *  Gets the Organization element Id.
     *
     *  @return the Organization element Id
     */

    public static org.osid.id.Id getOrganization() {
        return (makeElementId("osid.financials.activity.Organization"));
    }


    /**
     *  Gets the SupervisorId element Id.
     *
     *  @return the SupervisorId element Id
     */

    public static org.osid.id.Id getSupervisorId() {
        return (makeElementId("osid.financials.activity.SupervisorId"));
    }


    /**
     *  Gets the Supervisor element Id.
     *
     *  @return the Supervisor element Id
     */

    public static org.osid.id.Id getSupervisor() {
        return (makeElementId("osid.financials.activity.Supervisor"));
    }


    /**
     *  Gets the Code element Id.
     *
     *  @return the Code element Id
     */

    public static org.osid.id.Id getCode() {
        return (makeElementId("osid.financials.activity.Code"));
    }


    /**
     *  Gets the Summary element Id.
     *
     *  @return the Summary element Id
     */

    public static org.osid.id.Id getSummary() {
        return (makeQueryElementId("osid.financials.activity.Summary"));
    }


    /**
     *  Gets the AncestorActivityId element Id.
     *
     *  @return the AncestorActivityId element Id
     */

    public static org.osid.id.Id getAncestorActivityId() {
        return (makeQueryElementId("osid.financials.activity.AncestorActivityId"));
    }


    /**
     *  Gets the AncestorActivity element Id.
     *
     *  @return the AncestorActivity element Id
     */

    public static org.osid.id.Id getAncestorActivity() {
        return (makeQueryElementId("osid.financials.activity.AncestorActivity"));
    }


    /**
     *  Gets the DescendantActivityId element Id.
     *
     *  @return the DescendantActivityId element Id
     */

    public static org.osid.id.Id getDescendantActivityId() {
        return (makeQueryElementId("osid.financials.activity.DescendantActivityId"));
    }


    /**
     *  Gets the DescendantActivity element Id.
     *
     *  @return the DescendantActivity element Id
     */

    public static org.osid.id.Id getDescendantActivity() {
        return (makeQueryElementId("osid.financials.activity.DescendantActivity"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.financials.activity.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.financials.activity.Business"));
    }
}
