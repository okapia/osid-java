//
// AbstractMessageValidator.java
//
//     Validates a Message.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.messaging.message.spi;


/**
 *  Validates a Message.
 */

public abstract class AbstractMessageValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractMessageValidator</code>.
     */

    protected AbstractMessageValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractMessageValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractMessageValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Message.
     *
     *  @param message a message to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>message</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.messaging.Message message) {
        super.validate(message);

        test(message.getSubjectLine(), "getSubjectLine()");
        test(message.getText(), "getText()");

        testConditionalMethod(message, "getSentTime", message.isSent(), "isSent()");
        testConditionalMethod(message, "getSenderId", message.isSent(), "isSent()");
        testConditionalMethod(message, "getSender", message.isSent(), "isSent()");
        testConditionalMethod(message, "getSendingAgentId", message.isSent(), "isSent()");
        testConditionalMethod(message, "getSendingAgent", message.isSent(), "isSent()");
        testConditionalMethod(message, "getReceivedTime", message.isSent(), "isSent()");
        testConditionalMethod(message, "getRecipientIds", message.isSent(), "isSent()");
        testConditionalMethod(message, "getRecipients", message.isSent(), "isSent()");
        testConditionalMethod(message, "isRecipient", message.isSent(), "isSent()");

        if (message.isSent()) {
            testNestedObject(message, "getSender");
            testNestedObject(message, "getSendingAgent");
            testNestedObjects(message, "getRecipientIds", "getRecipients");
            
            testConditionalMethod(message, "getReceiptIds", message.isRecipient(), "isRecipient()");
            testConditionalMethod(message, "getReceipts", message.isRecipient(), "isRecipient()");

            if (message.isRecipient()) {
                testNestedObject(message, "getReceipt");
            }
        }            
        
        return;
    }
}
