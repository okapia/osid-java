//
// AbstractAdapterProcedureLookupSession.java
//
//    A Procedure lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Procedure lookup session adapter.
 */

public abstract class AbstractAdapterProcedureLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recipe.ProcedureLookupSession {

    private final org.osid.recipe.ProcedureLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProcedureLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProcedureLookupSession(org.osid.recipe.ProcedureLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Cookbook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Cookbook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.session.getCookbookId());
    }


    /**
     *  Gets the {@code Cookbook} associated with this session.
     *
     *  @return the {@code Cookbook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCookbook());
    }


    /**
     *  Tests if this user can perform {@code Procedure} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProcedures() {
        return (this.session.canLookupProcedures());
    }


    /**
     *  A complete view of the {@code Procedure} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProcedureView() {
        this.session.useComparativeProcedureView();
        return;
    }


    /**
     *  A complete view of the {@code Procedure} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProcedureView() {
        this.session.usePlenaryProcedureView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include procedures in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.session.useFederatedCookbookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this cookbook only.
     */

    @OSID @Override
    public void useIsolatedCookbookView() {
        this.session.useIsolatedCookbookView();
        return;
    }
    
     
    /**
     *  Gets the {@code Procedure} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Procedure} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Procedure} and
     *  retained for compatibility.
     *
     *  @param procedureId {@code Id} of the {@code Procedure}
     *  @return the procedure
     *  @throws org.osid.NotFoundException {@code procedureId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code procedureId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Procedure getProcedure(org.osid.id.Id procedureId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcedure(procedureId));
    }


    /**
     *  Gets a {@code ProcedureList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  procedures specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Procedures} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  procedureIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Procedure} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code procedureIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByIds(org.osid.id.IdList procedureIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProceduresByIds(procedureIds));
    }


    /**
     *  Gets a {@code ProcedureList} corresponding to the given
     *  procedure genus {@code Type} which does not include
     *  procedures of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  procedureGenusType a procedure genus type 
     *  @return the returned {@code Procedure} list
     *  @throws org.osid.NullArgumentException
     *          {@code procedureGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByGenusType(org.osid.type.Type procedureGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProceduresByGenusType(procedureGenusType));
    }


    /**
     *  Gets a {@code ProcedureList} corresponding to the given
     *  procedure genus {@code Type} and include any additional
     *  procedures with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  procedureGenusType a procedure genus type 
     *  @return the returned {@code Procedure} list
     *  @throws org.osid.NullArgumentException
     *          {@code procedureGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByParentGenusType(org.osid.type.Type procedureGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProceduresByParentGenusType(procedureGenusType));
    }


    /**
     *  Gets a {@code ProcedureList} containing the given
     *  procedure record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  procedureRecordType a procedure record type 
     *  @return the returned {@code Procedure} list
     *  @throws org.osid.NullArgumentException
     *          {@code procedureRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByRecordType(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProceduresByRecordType(procedureRecordType));
    }


    /**
     *  Gets all {@code Procedures}. 
     *
     *  In plenary mode, the returned list contains all known
     *  procedures or an error results. Otherwise, the returned list
     *  may contain only those procedures that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Procedures} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProcedures()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcedures());
    }
}
