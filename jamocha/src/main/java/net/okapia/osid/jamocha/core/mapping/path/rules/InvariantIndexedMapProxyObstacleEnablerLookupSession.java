//
// InvariantIndexedMapProxyObstacleEnablerLookupSession
//
//    Implements an ObstacleEnabler lookup service backed by a fixed
//    collection of obstacleEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules;


/**
 *  Implements a ObstacleEnabler lookup service backed by a fixed
 *  collection of obstacleEnablers. The obstacleEnablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some obstacleEnablers may be compatible
 *  with more types than are indicated through these obstacleEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyObstacleEnablerLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.rules.spi.AbstractIndexedMapObstacleEnablerLookupSession
    implements org.osid.mapping.path.rules.ObstacleEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyObstacleEnablerLookupSession}
     *  using an array of obstacle enablers.
     *
     *  @param map the map
     *  @param obstacleEnablers an array of obstacle enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code obstacleEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                                         org.osid.mapping.path.rules.ObstacleEnabler[] obstacleEnablers, 
                                                         org.osid.proxy.Proxy proxy) {

        setMap(map);
        setSessionProxy(proxy);
        putObstacleEnablers(obstacleEnablers);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyObstacleEnablerLookupSession}
     *  using a collection of obstacle enablers.
     *
     *  @param map the map
     *  @param obstacleEnablers a collection of obstacle enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code obstacleEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                                         java.util.Collection<? extends org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablers,
                                                         org.osid.proxy.Proxy proxy) {

        setMap(map);
        setSessionProxy(proxy);
        putObstacleEnablers(obstacleEnablers);

        return;
    }
}
