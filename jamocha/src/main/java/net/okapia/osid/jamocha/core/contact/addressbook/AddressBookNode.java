//
// AddressBookNode.java
//
//     Defines an AddressBook node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact;


/**
 *  A class for managing a hierarchy of address book nodes in core.
 */

public final class AddressBookNode
    extends net.okapia.osid.jamocha.core.contact.spi.AbstractAddressBookNode
    implements org.osid.contact.AddressBookNode {


    /**
     *  Constructs a new <code>AddressBookNode</code> from a single
     *  address book.
     *
     *  @param addressBook the address book
     *  @throws org.osid.NullArgumentException <code>addressBook</code> is 
     *          <code>null</code>.
     */

    public AddressBookNode(org.osid.contact.AddressBook addressBook) {
        super(addressBook);
        return;
    }


    /**
     *  Constructs a new <code>AddressBookNode</code>.
     *
     *  @param addressBook the address book
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>.
     */

    public AddressBookNode(org.osid.contact.AddressBook addressBook, boolean root, boolean leaf) {
        super(addressBook, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this address book.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.contact.AddressBookNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this address book.
     *
     *  @param addressBook the address book to add as a parent
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.contact.AddressBook addressBook) {
        addParent(new AddressBookNode(addressBook));
        return;
    }


    /**
     *  Adds a child to this address book.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.contact.AddressBookNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this address book.
     *
     *  @param addressBook the address book to add as a child
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.contact.AddressBook addressBook) {
        addChild(new AddressBookNode(addressBook));
        return;
    }
}
