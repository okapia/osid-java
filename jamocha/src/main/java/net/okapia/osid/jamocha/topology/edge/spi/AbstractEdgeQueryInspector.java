//
// AbstractEdgeQueryInspector.java
//
//     A template for making an EdgeQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.edge.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for edges.
 */

public abstract class AbstractEdgeQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.topology.EdgeQueryInspector {

    private final java.util.Collection<org.osid.topology.records.EdgeQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the node <code> Id </code> terms. 
     *
     *  @return the node <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceNodeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the node terms. 
     *
     *  @return the node terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getSourceNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the related node <code> Id </code> terms. 
     *
     *  @return the node <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDestinationNodeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the related node terms. 
     *
     *  @return the node terms 
     */

    @OSID @Override
    public org.osid.topology.NodeQueryInspector[] getDestinationNodeTerms() {
        return (new org.osid.topology.NodeQueryInspector[0]);
    }


    /**
     *  Gets the same node terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSameNodeTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the directional terms. 
     *
     *  @return the directional terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDirectionalTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the bi-directional terms. 
     *
     *  @return the directional terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getBiDirectionalTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCostTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the distance terms. 
     *
     *  @return the distance terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDistanceTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the graph <code> Id </code> terms. 
     *
     *  @return the graph <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraphIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the graph terms. 
     *
     *  @return the graph terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given edge query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an edge implementing the requested record.
     *
     *  @param edgeRecordType an edge record type
     *  @return the edge query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.EdgeQueryInspectorRecord getEdgeQueryInspectorRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.EdgeQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this edge query. 
     *
     *  @param edgeQueryInspectorRecord edge query inspector
     *         record
     *  @param edgeRecordType edge record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEdgeQueryInspectorRecord(org.osid.topology.records.EdgeQueryInspectorRecord edgeQueryInspectorRecord, 
                                                   org.osid.type.Type edgeRecordType) {

        addRecordType(edgeRecordType);
        nullarg(edgeRecordType, "edge record type");
        this.records.add(edgeQueryInspectorRecord);        
        return;
    }
}
