//
// AbstractFederatingBlockLookupSession.java
//
//     An abstract federating adapter for a BlockLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BlockLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBlockLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.hold.BlockLookupSession>
    implements org.osid.hold.BlockLookupSession {

    private boolean parallel = false;
    private org.osid.hold.Oubliette oubliette = new net.okapia.osid.jamocha.nil.hold.oubliette.UnknownOubliette();


    /**
     *  Constructs a new <code>AbstractFederatingBlockLookupSession</code>.
     */

    protected AbstractFederatingBlockLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.hold.BlockLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Oubliette/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Oubliette Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.oubliette.getId());
    }


    /**
     *  Gets the <code>Oubliette</code> associated with this 
     *  session.
     *
     *  @return the <code>Oubliette</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.oubliette);
    }


    /**
     *  Sets the <code>Oubliette</code>.
     *
     *  @param  oubliette the oubliette for this session
     *  @throws org.osid.NullArgumentException <code>oubliette</code>
     *          is <code>null</code>
     */

    protected void setOubliette(org.osid.hold.Oubliette oubliette) {
        nullarg(oubliette, "oubliette");
        this.oubliette = oubliette;
        return;
    }


    /**
     *  Tests if this user can perform <code>Block</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBlocks() {
        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            if (session.canLookupBlocks()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Block</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBlockView() {
        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            session.useComparativeBlockView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Block</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBlockView() {
        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            session.usePlenaryBlockView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include blocks in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            session.useFederatedOublietteView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            session.useIsolatedOublietteView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Block</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Block</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Block</code> and
     *  retained for compatibility.
     *
     *  @param  blockId <code>Id</code> of the
     *          <code>Block</code>
     *  @return the block
     *  @throws org.osid.NotFoundException <code>blockId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>blockId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Block getBlock(org.osid.id.Id blockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            try {
                return (session.getBlock(blockId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(blockId + " not found");
    }


    /**
     *  Gets a <code>BlockList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  blocks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Blocks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  blockIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>blockIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByIds(org.osid.id.IdList blockIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.hold.block.MutableBlockList ret = new net.okapia.osid.jamocha.hold.block.MutableBlockList();

        try (org.osid.id.IdList ids = blockIds) {
            while (ids.hasNext()) {
                ret.addBlock(getBlock(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BlockList</code> corresponding to the given
     *  block genus <code>Type</code> which does not include
     *  blocks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockGenusType a block genus type 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByGenusType(org.osid.type.Type blockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.block.FederatingBlockList ret = getBlockList();

        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            ret.addBlockList(session.getBlocksByGenusType(blockGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BlockList</code> corresponding to the given
     *  block genus <code>Type</code> and include any additional
     *  blocks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockGenusType a block genus type 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByParentGenusType(org.osid.type.Type blockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.block.FederatingBlockList ret = getBlockList();

        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            ret.addBlockList(session.getBlocksByParentGenusType(blockGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BlockList</code> containing the given
     *  block record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockRecordType a block record type 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByRecordType(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.block.FederatingBlockList ret = getBlockList();

        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            ret.addBlockList(session.getBlocksByRecordType(blockRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BlockList</code> mapped to the given
     *  <code>Issue</code> In plenary mode, the returned list contains
     *  all known blocks or an error results. Otherwise, the returned
     *  list may contain only those blocks that are accessible through
     *  this session.
     *
     *  @param  issueId an issue <code>Id</code>
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException <code>issueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.block.FederatingBlockList ret = getBlockList();

        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            ret.addBlockList(session.getBlocksForIssue(issueId));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets all <code>Blocks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Blocks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.block.FederatingBlockList ret = getBlockList();

        for (org.osid.hold.BlockLookupSession session : getSessions()) {
            ret.addBlockList(session.getBlocks());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.hold.block.FederatingBlockList getBlockList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.hold.block.ParallelBlockList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.hold.block.CompositeBlockList());
        }
    }
}
