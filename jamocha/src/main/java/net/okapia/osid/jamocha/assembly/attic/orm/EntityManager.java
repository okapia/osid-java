//
// EntityManager.java
//
//     An interface for assembling relational statements.
//
//
// Tom Coppeto
// Okapia
// 12 May 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.orm;


/**
 *  Assembles relational statements.
 */

public interface EntityManager {

    /**
     *  Adds an element. 
     *
     *  @param element the form element
     *  @throws org.osid.NullArgumentException <code>element</code> is
     *          <code>null</code>
     */

    public void addElement(Element element);


    /**
     *  Gets the elements.
     *
     *  @return an iteratable collection of elements
     */

    public Iterable<Elements> getElements();


    /**
     *  
     */



    /**
     *  Adds a Boolean search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addBooleanTerm(String column, boolean match);
    

    /**
     *  Adds a Boolean wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addBooleanWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the boolean query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of boolean terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.BooleanTerm[] getBooleanTerms(String column);


    /**
     *  Adds a Bytes search query term.
     *
     *  @param column query column
     *  @param bytes the bytes to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @param partial <code>true for a partial match,
     *         <code>false</code> for a complete match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>bytes</code> is <code>null</code>
     */
    
    public void addBytesTerm(String column, byte[] bytes, boolean match, boolean partial);


    /**
     *  Adds a Bytes wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addBytesWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the boolean query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of boolean terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.BytesTerm[] getBytesTerms(String column);


    /**
     *  Adds a Cardinal search query term.
     *
     *  @param column query column
     *  @param value a cardinal value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addCardinalTerm(String column, long value, boolean match);


    /**
     *  Adds a Cardinal wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addCardinalWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the cardinal query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of cardinal terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.CardinalTerm[] getCardinalTerms(String column);        


    /**
     *  Adds aa cardinal range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addCardinalRangeTerm(String column, long start, long end, boolean match);
    

    /**
     *  Adds a Cardinal wildcard range search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addCardinalRangeWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the cardinal range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of cardinal range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.CardinalRangeTerm[] getCardinalRangeTerms(String column);        


    /**
     *  Adds a Coordinate search query term.
     *
     *  @param column query column
     *  @param value a coordinate value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addCoordinateTerm(String column, org.osid.mapping.Coordinate value, 
                                  boolean match);


    /**
     *  Adds a Coordinate wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addCoordinateWildcardTerm(String column, boolean match);


    /**
     *  Gets the coordinate query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of coordinate terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms(String column);


    /**
     *  Adds a coordinate range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addCoordinateRangeTerm(String column, org.osid.mapping.Coordinate start, 
                                       org.osid.mapping.Coordinate end, boolean match);


    /**
     *  Adds a Coordinate range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addCoordinateRangeWildcardTerm(String column, boolean match);


    /**
     *  Gets the coordinate range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of coordinate range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.CoordinateRangeTerm[] getCoordinateRangeTerms(String column);

    
    /**
     *  Adds a Currency search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addCurrencyTerm(String column, org.osid.financials.Currency value,
                                     boolean match);


    /**
     *  Adds a Currency wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addCurrencyWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the currency query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of currency terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.CurrencyTerm[] getCurrencyTerms(String column);


    /**
     *  Adds a currency range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    public void addCurrencyRangeTerm(String column, org.osid.financials.Currency start, 
                                     org.osid.financials.Currency end, boolean match);


    /**
     *  Adds a Currency wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addCurrencyRangeWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the currency range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of currency range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.CurrencyRangeTerm[] getCurrencyRangeTerms(String column);

    
    /**
     *  Adds a <code>DateTime</code> search query term.
     *
     *  @param column query column
     *  @param value datetime value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addDateTimeTerm(String column, org.osid.calendaring.DateTime value, 
                                boolean match);
    

    /**
     *  Adds a <code>DateTime</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addDateTimeWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the datetime query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of datetime terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DateTimeTerm[] getDateTimeTerms(String column);

    
    /**
     *  Adds a <code>DateTime</code> range search query term.
     *
     *  @param column query column
     *  @param start low end time
     *  @param end high end time
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    public void addDateTimeRangeTerm(String column, org.osid.calendaring.DateTime start, 
                                     org.osid.calendaring.DateTime end, boolean match);
    

    /**
     *  Adds a <code>DateTime</code> range wildcard search query
     *  term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addDateTimeRangeWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the datetime range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of datetime range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DateTimeRangeTerm[] getDateTimeRangeTerms(String column);


    /**
     *  Adds a Decimal search query term.
     *
     *  @param column query column
     *  @param value a decimal value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addDecimalTerm(String column, java.math.BigDecimal value, boolean match);


    /**
     *  Adds a Decimal wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addDecimalWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the decimal query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of decimal terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DecimalTerm[] getDecimalTerms(String column);


    /**
     *  Adds a decimal range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    public void addDecimalRangeTerm(String column, java.math.BigDecimal start, 
                                    java.math.BigDecimal end, boolean match);
    

    /**
     *  Adds a Decimal wildcard range search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addDecimalRangeWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the decimal range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of decimal range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DecimalRangeTerm[] getDecimalRangeTerms(String column);


    /**
     *  Adds a Distance search query term.
     *
     *  @param column query column
     *  @param value a distance value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addDistanceTerm(String column, org.osid.mapping.Distance value, 
                                     boolean match);


    /**
     *  Gets the distance query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of distance terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DistanceTerm[] getDistanceTerms(String column);


    /**
     *  Adds a Distance wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addDistanceWildcardTerm(String column, boolean match);


    /**
     *  Adds a distance range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addDistanceRangeTerm(String column, org.osid.mapping.Distance start, 
                                     org.osid.mapping.Distance end, boolean match);


    /**
     *  Adds a Distance range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addDistanceRangeWildcardTerm(String column, boolean match);


    /**
     *  Gets the distance range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of distance range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DistanceRangeTerm[] getDistanceRangeTerms(String column);

    
    /**
     *  Adds a Duration search query term.
     *
     *  @param column query column
     *  @param value a duration value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addDurationTerm(String column, org.osid.calendaring.Duration value, 
                                     boolean match);
    

    /**
     *  Adds a Duration wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addDurationWildcardTerm(String column, boolean match);


    /**
     *  Gets the duration query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of duration terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DurationTerm[] getDurationTerms(String column);


    /**
     *  Adds a duration range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addDurationRangeTerm(String column, org.osid.calendaring.Duration start, 
                                     org.osid.calendaring.Duration end, boolean match);


    /**
     *  Adds a Duration range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addDurationRangeWildcardTerm(String column, boolean match);


    /**
     *  Gets the duration range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of duration range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.DurationRangeTerm[] getDurationRangeTerms(String column);

    
    /**
     *  Adds a Heading search query term.
     *
     *  @param column query column
     *  @param value a heading value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addHeadingTerm(String column, org.osid.mapping.Heading value, 
                               boolean match);


    /**
     *  Adds a Heading wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addHeadingWildcardTerm(String column, boolean match);


    /**
     *  Gets the heading query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of heading terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.HeadingTerm[] getHeadingTerms(String column);


    /**
     *  Adds a heading range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addHeadingRangeTerm(String column, org.osid.mapping.Heading start, 
                                    org.osid.mapping.Heading end,  boolean match);


    /**
     *  Adds a Heading range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addHeadingRangeWildcardTerm(String column, boolean match);


    /**
     *  Gets the heading range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of heading range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.HeadingRangeTerm[] getHeadingRangeTerms(String column);

    
    /**
     *  Adds a <code>Id</code> search query term.
     *
     *  @param column query column
     *  @param value Id value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addIdTerm(String column, org.osid.id.Id value, boolean match);


    /**
     *  Adds an <code>Id</code> search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addIdWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the Id query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of Id terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.IdTerm[] getIdTerms(String column);


    /**
     *  Adds an <code>Id</code> set search query term.
     *
     *  @param column query column
     *  @param values Id values to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>values</code> is <code>null</code>
     */
    
    public void addIdSetTerm(String column, org.osid.id.Id[] values, boolean match);


    /**
     *  Gets the Id set query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of Id set terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.IdSetTerm[] getIdSetTerms(String column);


    /**
     *  Adds a integer search query term.
     *
     *  @param column query column
     *  @param value integer value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addIntegerTerm(String column, long value, boolean match);


    /**
     *  Adds a integer wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addIntegerWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the integer query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of integer terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.IntegerTerm[] getIntegerTerms(String column);

    
    /**
     *  Adds a integer range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addIntegerRangeTerm(String column, long start, long end, boolean match);
    

    /**
     *  Adds a integer range search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addIntegerRangeWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the intgeer range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of intgeer range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.IntegerRangeTerm[] getIntegerRangeTerms(String column);


    /**
     *  Adds a Object search query term.
     *
     *  @param column query column
     *  @param value a object value
     *  @param objectType the objectType
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>value</code>, or <code>objectType</code> is
     *          <code>null</code>
     */
    
    public void addObjectTerm(String column, Object value, 
                              org.osid.type.Type objectType, boolean match);


    /**
     *  Adds a Object wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addObjectWildcardTerm(String column, boolean match);


    /**
     *  Gets the object query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of object terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.ObjectTerm[] getObjectTerms(String column);


    /**
     *  Adds a SpatialUnit search query term.
     *
     *  @param column query column
     *  @param value a spatial unit value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addSpatialUnitTerm(String column, org.osid.mapping.SpatialUnit value, 
                                   boolean match);


    /**
     *  Adds a SpatialUnit wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addSpatialUnitWildcardTerm(String column, boolean match);


    /**
     *  Gets the spatial unit query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of spatial unit terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitTerms(String column);


    /**
     *  Adds a Speed search query term.
     *
     *  @param column query column
     *  @param value a speed value
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addSpeedTerm(String column, org.osid.mapping.Speed value, 
                             boolean match);


    /**
     *  Adds a Speed wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addSpeedWildcardTerm(String column, boolean match);


    /**
     *  Gets the speed query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of speed terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.SpeedTerm[] getSpeedTerms(String column);


    /**
     *  Adds a speed range search query term.
     *
     *  @param column query column
     *  @param start low end number
     *  @param end high end number
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code> 
     */
    
    public void addSpeedRangeTerm(String column, org.osid.mapping.Speed start, 
                                  org.osid.mapping.Speed end, boolean match);


    /**
     *  Adds a Speed range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addSpeedRangeWildcardTerm(String column, boolean match);


    /**
     *  Gets the speed range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of speed range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.SpeedRangeTerm[] getSpeedRangeTerms(String column);


    /**
     *  Adds a string search query term.
     *
     *  @param column query column
     *  @param value string value to match
     *  @param stringMatchType type of string matching
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>value</code>, <code>stringMatchType</code> is
     *          <code>null</code>
     */
    
    public void addStringTerm(String column, String value, 
                              org.osid.type.Type stringMatchType, boolean match);

    
    /**
     *  Adds a string wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addStringWildcardTerm(String column, boolean match);


    /**
     *  Gets the string query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of string terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.StringTerm[] getStringTerms(String column);


    /**
     *  Adds a Syntax search query term.
     *
     *  @param column query column
     *  @param value a syntax
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addSyntaxTerm(String column, org.osid.Syntax value, boolean match);


    /**
     *  Adds a Syntax wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code>
     *          is <code>null</code>
     */
    
    public void addSyntaxWildcardTerm(String column, boolean match);


    /**
     *  Gets the syntax query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of syntax terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.SyntaxTerm[] getSyntaxTerms(String column);

    
    /**
     *  Adds a <code>Time</code> search query term.
     *
     *  @param column query column
     *  @param value time value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addTimeTerm(String column, org.osid.calendaring.Time value, 
                            boolean match);
    

    /**
     *  Adds a <code>Time</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addTimeWildcardTerm(String column, boolean match);
    

    /**
     *  Gets the time query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of time terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.TimeTerm[] getTimeTerms(String column);

    
    /**
     *  Adds a <code>Time</code> range search query term.
     *
     *  @param column query column
     *  @param start low end time
     *  @param end high end time
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    public void addTimeRangeTerm(String column, org.osid.calendaring.Time start, 
                                 org.osid.calendaring.Time end, boolean match);
    

    /**
     *  Adds a <code>Time</code> range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addTimeRangeWildcardTerm(String column, boolean match);


    /**
     *  Gets the time range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of time range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.TimeRangeTerm[] getTimeRangeTerms(String column);


    /**
     *  Adds a <code>Type</code> search query term.
     *
     *  @param column query column
     *  @param value Type value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addTypeTerm(String column, org.osid.type.Type value, boolean match);


    /**
     *  Adds a <code>Type</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addTypeWildcardTerm(String column, boolean match);

    
    /**
     *  Gets the type query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of type terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.TypeTerm[] getTypeTerms(String column);


    /**
     *  Adds a <code>Version</code> search query term.
     *
     *  @param column query column
     *  @param value version value to match
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>value</code> is <code>null</code>
     */
    
    public void addVersionTerm(String column, org.osid.installation.Version value, 
                               boolean match);
    

    /**
     *  Adds a <code>Version</code> wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addVersionWildcardTerm(String column, boolean match);


    /**
     *  Gets the version query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of version terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.VersionTerm[] getVersionTerms(String column);


    /**
     *  Adds a <code>Version</code> range search query term.
     *
     *  @param column query column
     *  @param start low end version
     *  @param end high end version
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>start</code>
     *          is greater than <code>end</code>
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>start</code>, or <code>end</code> is
     *          <code>null</code>
     */
    
    public void addVersionRangeTerm(String column, org.osid.installation.Version start, 
                                    org.osid.installation.Version end, boolean match);
    

    /**
     *  Adds a <code>Version</code> range wildcard search query term.
     *
     *  @param column query column
     *  @param match <code>true for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */
    
    public void addVersionRangeWildcardTerm(String column, boolean match);


    /**
     *  Gets the version range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of version range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public org.osid.search.terms.VersionRangeTerm[] getVersionRangeTerms(String column);


    /**
     *  Adds an order term.
     *
     *  @param column a column
     *  @param style search order style
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>style</code> is <code>null</code>
     */

    public void addOrder(String column, org.osid.SearchOrderStyle style);


    /**
     *  Adds an order term with a qualifying Id.
     *
     *  @param column a column
     *  @param qualifierId a qualifiying Id
     *  @param style search order style
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>qualifierId</code> or <code>style</code> is
     *          <code>null</code>
     */

    public void addOrder(String column, org.osid.id.Id qualifierId, 
                         org.osid.SearchOrderStyle style);
}
