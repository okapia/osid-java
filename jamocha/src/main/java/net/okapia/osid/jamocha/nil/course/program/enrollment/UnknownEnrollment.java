//
// UnknownEnrollment.java
//
//     Defines an unknown Enrollment.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.program.enrollment;


/**
 *  Defines an unknown <code>Enrollment</code>.
 */

public final class UnknownEnrollment
    extends net.okapia.osid.jamocha.nil.course.program.enrollment.spi.AbstractUnknownEnrollment
    implements org.osid.course.program.Enrollment {


    /**
     *  Constructs a new <code>UnknownEnrollment</code>.
     */

    public UnknownEnrollment() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownEnrollment</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownEnrollment(boolean optional) {
        super(optional);
        addEnrollmentRecord(new EnrollmentRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Enrollment.
     *
     *  @return an unknown Enrollment
     */

    public static org.osid.course.program.Enrollment create() {
        return (net.okapia.osid.jamocha.builder.validator.course.program.enrollment.EnrollmentValidator.validateEnrollment(new UnknownEnrollment()));
    }


    public class EnrollmentRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.course.program.records.EnrollmentRecord {

        
        protected EnrollmentRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
