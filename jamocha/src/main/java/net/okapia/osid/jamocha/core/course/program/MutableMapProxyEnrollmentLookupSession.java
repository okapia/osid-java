//
// MutableMapProxyEnrollmentLookupSession
//
//    Implements an Enrollment lookup service backed by a collection of
//    enrollments that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements an Enrollment lookup service backed by a collection of
 *  enrollments. The enrollments are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of enrollments can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyEnrollmentLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractMapEnrollmentLookupSession
    implements org.osid.course.program.EnrollmentLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyEnrollmentLookupSession}
     *  with no enrollments.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyEnrollmentLookupSession} with a
     *  single enrollment.
     *
     *  @param courseCatalog the course catalog
     *  @param enrollment an enrollment
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code enrollment}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.program.Enrollment enrollment, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putEnrollment(enrollment);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEnrollmentLookupSession} using an
     *  array of enrollments.
     *
     *  @param courseCatalog the course catalog
     *  @param enrollments an array of enrollments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code enrollments}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.program.Enrollment[] enrollments, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putEnrollments(enrollments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEnrollmentLookupSession} using a
     *  collection of enrollments.
     *
     *  @param courseCatalog the course catalog
     *  @param enrollments a collection of enrollments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code enrollments}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                java.util.Collection<? extends org.osid.course.program.Enrollment> enrollments,
                                                org.osid.proxy.Proxy proxy) {
   
        this(courseCatalog, proxy);
        setSessionProxy(proxy);
        putEnrollments(enrollments);
        return;
    }

    
    /**
     *  Makes a {@code Enrollment} available in this session.
     *
     *  @param enrollment an enrollment
     *  @throws org.osid.NullArgumentException {@code enrollment{@code 
     *          is {@code null}
     */

    @Override
    public void putEnrollment(org.osid.course.program.Enrollment enrollment) {
        super.putEnrollment(enrollment);
        return;
    }


    /**
     *  Makes an array of enrollments available in this session.
     *
     *  @param enrollments an array of enrollments
     *  @throws org.osid.NullArgumentException {@code enrollments{@code 
     *          is {@code null}
     */

    @Override
    public void putEnrollments(org.osid.course.program.Enrollment[] enrollments) {
        super.putEnrollments(enrollments);
        return;
    }


    /**
     *  Makes collection of enrollments available in this session.
     *
     *  @param enrollments
     *  @throws org.osid.NullArgumentException {@code enrollment{@code 
     *          is {@code null}
     */

    @Override
    public void putEnrollments(java.util.Collection<? extends org.osid.course.program.Enrollment> enrollments) {
        super.putEnrollments(enrollments);
        return;
    }


    /**
     *  Removes a Enrollment from this session.
     *
     *  @param enrollmentId the {@code Id} of the enrollment
     *  @throws org.osid.NullArgumentException {@code enrollmentId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEnrollment(org.osid.id.Id enrollmentId) {
        super.removeEnrollment(enrollmentId);
        return;
    }    
}
