//
// MutableMapProxyEngineLookupSession
//
//    Implements an Engine lookup service backed by a collection of
//    engines that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules;


/**
 *  Implements an Engine lookup service backed by a collection of
 *  engines. The engines are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of engines can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyEngineLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractMapEngineLookupSession
    implements org.osid.rules.EngineLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEngineLookupSession} with no
     *  engines.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyEngineLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyEngineLookupSession} with a
     *  single engine.
     *
     *  @param engine an engine
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyEngineLookupSession(org.osid.rules.Engine engine, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEngineLookupSession} using an
     *  array of engines.
     *
     *  @param engines an array of engines
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engines} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyEngineLookupSession(org.osid.rules.Engine[] engines, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putEngines(engines);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyEngineLookupSession} using
     *  a collection of engines.
     *
     *  @param engines a collection of engines
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engines} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyEngineLookupSession(java.util.Collection<? extends org.osid.rules.Engine> engines,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putEngines(engines);
        return;
    }

    
    /**
     *  Makes a {@code Engine} available in this session.
     *
     *  @param engine an engine
     *  @throws org.osid.NullArgumentException {@code engine{@code 
     *          is {@code null}
     */

    @Override
    public void putEngine(org.osid.rules.Engine engine) {
        super.putEngine(engine);
        return;
    }


    /**
     *  Makes an array of engines available in this session.
     *
     *  @param engines an array of engines
     *  @throws org.osid.NullArgumentException {@code engines{@code 
     *          is {@code null}
     */

    @Override
    public void putEngines(org.osid.rules.Engine[] engines) {
        super.putEngines(engines);
        return;
    }


    /**
     *  Makes collection of engines available in this session.
     *
     *  @param engines
     *  @throws org.osid.NullArgumentException {@code engine{@code 
     *          is {@code null}
     */

    @Override
    public void putEngines(java.util.Collection<? extends org.osid.rules.Engine> engines) {
        super.putEngines(engines);
        return;
    }


    /**
     *  Removes a Engine from this session.
     *
     *  @param engineId the {@code Id} of the engine
     *  @throws org.osid.NullArgumentException {@code engineId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEngine(org.osid.id.Id engineId) {
        super.removeEngine(engineId);
        return;
    }    
}
