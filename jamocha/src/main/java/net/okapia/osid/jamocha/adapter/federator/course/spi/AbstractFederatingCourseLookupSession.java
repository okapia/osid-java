//
// AbstractFederatingCourseLookupSession.java
//
//     An abstract federating adapter for a CourseLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CourseLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCourseLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.CourseLookupSession>
    implements org.osid.course.CourseLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingCourseLookupSession</code>.
     */

    protected AbstractFederatingCourseLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.CourseLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Course</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourses() {
        for (org.osid.course.CourseLookupSession session : getSessions()) {
            if (session.canLookupCourses()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Course</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseView() {
        for (org.osid.course.CourseLookupSession session : getSessions()) {
            session.useComparativeCourseView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Course</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseView() {
        for (org.osid.course.CourseLookupSession session : getSessions()) {
            session.usePlenaryCourseView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include courses in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.CourseLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.CourseLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only active courses are returned by methods in this session.
     */

    @OSID @Override
    public void useActiveCourseView() {
        for (org.osid.course.CourseLookupSession session : getSessions()) {
            session.useActiveCourseView();
        }

        return;
    }


    /**
     *  All courses of any active or inactive status are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyStatusCourseView() {
        for (org.osid.course.CourseLookupSession session : getSessions()) {
            session.useAnyStatusCourseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Course</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Course</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Course</code> and
     *  retained for compatibility.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseId <code>Id</code> of the
     *          <code>Course</code>
     *  @return the course
     *  @throws org.osid.NotFoundException <code>courseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Course getCourse(org.osid.id.Id courseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.CourseLookupSession session : getSessions()) {
            try {
                return (session.getCourse(courseId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(courseId + " not found");
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Courses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>courseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByIds(org.osid.id.IdList courseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.course.MutableCourseList ret = new net.okapia.osid.jamocha.course.course.MutableCourseList();

        try (org.osid.id.IdList ids = courseIds) {
            while (ids.hasNext()) {
                ret.addCourse(getCourse(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  course genus <code>Type</code> which does not include
     *  courses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, courses are returned that are currently effective.
     *  In any effective mode, effective courses and those currently expired
     *  are returned.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.course.FederatingCourseList ret = getCourseList();

        for (org.osid.course.CourseLookupSession session : getSessions()) {
            ret.addCourseList(session.getCoursesByGenusType(courseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  course genus <code>Type</code> and include any additional
     *  courses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByParentGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.course.FederatingCourseList ret = getCourseList();

        for (org.osid.course.CourseLookupSession session : getSessions()) {
            ret.addCourseList(session.getCoursesByParentGenusType(courseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseList</code> containing the given
     *  course record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  courseRecordType a course record type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByRecordType(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.course.FederatingCourseList ret = getCourseList();

        for (org.osid.course.CourseLookupSession session : getSessions()) {
            ret.addCourseList(session.getCoursesByRecordType(courseRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> CourseList </code> by the given number. 
     *  
     *  In plenary mode, the returned list contains all known courses
     *  or an error results. Otherwise, the returned list may contain
     *  only those courses that are accessible through this session.
     *  
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  number a course number 
     *  @return the returned <code> CourseList </code> list 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.course.FederatingCourseList ret = getCourseList();

        for (org.osid.course.CourseLookupSession session : getSessions()) {
            ret.addCourseList(session.getCoursesByNumber(number));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Courses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @return a list of <code>Courses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCourses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.course.FederatingCourseList ret = getCourseList();

        for (org.osid.course.CourseLookupSession session : getSessions()) {
            ret.addCourseList(session.getCourses());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.course.FederatingCourseList getCourseList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.course.ParallelCourseList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.course.CompositeCourseList());
        }
    }
}
