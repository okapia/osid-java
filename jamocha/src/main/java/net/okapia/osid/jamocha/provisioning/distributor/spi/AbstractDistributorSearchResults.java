//
// AbstractDistributorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.distributor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDistributorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.DistributorSearchResults {

    private org.osid.provisioning.DistributorList distributors;
    private final org.osid.provisioning.DistributorQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.records.DistributorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDistributorSearchResults.
     *
     *  @param distributors the result set
     *  @param distributorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>distributors</code>
     *          or <code>distributorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDistributorSearchResults(org.osid.provisioning.DistributorList distributors,
                                            org.osid.provisioning.DistributorQueryInspector distributorQueryInspector) {
        nullarg(distributors, "distributors");
        nullarg(distributorQueryInspector, "distributor query inspectpr");

        this.distributors = distributors;
        this.inspector = distributorQueryInspector;

        return;
    }


    /**
     *  Gets the distributor list resulting from a search.
     *
     *  @return a distributor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributors() {
        if (this.distributors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.DistributorList distributors = this.distributors;
        this.distributors = null;
	return (distributors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.DistributorQueryInspector getDistributorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  distributor search record <code> Type. </code> This method must
     *  be used to retrieve a distributor implementing the requested
     *  record.
     *
     *  @param distributorSearchRecordType a distributor search 
     *         record type 
     *  @return the distributor search
     *  @throws org.osid.NullArgumentException
     *          <code>distributorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(distributorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorSearchResultsRecord getDistributorSearchResultsRecord(org.osid.type.Type distributorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.records.DistributorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(distributorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(distributorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record distributor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDistributorRecord(org.osid.provisioning.records.DistributorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "distributor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
