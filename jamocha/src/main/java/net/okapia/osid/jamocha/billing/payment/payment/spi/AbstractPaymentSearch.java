//
// AbstractPaymentSearch.java
//
//     A template for making a Payment Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing payment searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPaymentSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.billing.payment.PaymentSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.billing.payment.records.PaymentSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.billing.payment.PaymentSearchOrder paymentSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of payments. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  paymentIds list of payments
     *  @throws org.osid.NullArgumentException
     *          <code>paymentIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPayments(org.osid.id.IdList paymentIds) {
        while (paymentIds.hasNext()) {
            try {
                this.ids.add(paymentIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPayments</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of payment Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPaymentIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  paymentSearchOrder payment search order 
     *  @throws org.osid.NullArgumentException
     *          <code>paymentSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>paymentSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPaymentResults(org.osid.billing.payment.PaymentSearchOrder paymentSearchOrder) {
	this.paymentSearchOrder = paymentSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.billing.payment.PaymentSearchOrder getPaymentSearchOrder() {
	return (this.paymentSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given payment search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a payment implementing the requested record.
     *
     *  @param paymentSearchRecordType a payment search record
     *         type
     *  @return the payment search record
     *  @throws org.osid.NullArgumentException
     *          <code>paymentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(paymentSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentSearchRecord getPaymentSearchRecord(org.osid.type.Type paymentSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.billing.payment.records.PaymentSearchRecord record : this.records) {
            if (record.implementsRecordType(paymentSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payment search. 
     *
     *  @param paymentSearchRecord payment search record
     *  @param paymentSearchRecordType payment search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPaymentSearchRecord(org.osid.billing.payment.records.PaymentSearchRecord paymentSearchRecord, 
                                           org.osid.type.Type paymentSearchRecordType) {

        addRecordType(paymentSearchRecordType);
        this.records.add(paymentSearchRecord);        
        return;
    }
}
