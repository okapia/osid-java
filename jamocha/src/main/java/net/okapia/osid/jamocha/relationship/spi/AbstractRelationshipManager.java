//
// AbstractRelationshipManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRelationshipManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.relationship.RelationshipManager,
               org.osid.relationship.RelationshipProxyManager {

    private final Types relationshipRecordTypes            = new TypeRefSet();
    private final Types relationshipSearchRecordTypes      = new TypeRefSet();

    private final Types familyRecordTypes                  = new TypeRefSet();
    private final Types familySearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRelationshipManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRelationshipManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any family federation is exposed. Federation is exposed when 
     *  a specific family may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  families appears as a single family. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up relationships is supported. 
     *
     *  @return <code> true </code> if relationship lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipLookup() {
        return (false);
    }


    /**
     *  Tests if querying relationships is supported. 
     *
     *  @return <code> true </code> if relationship query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipQuery() {
        return (false);
    }


    /**
     *  Tests if searching relationships is supported. 
     *
     *  @return <code> true </code> if relationship search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipSearch() {
        return (false);
    }


    /**
     *  Tests if relationship administrative service is supported. 
     *
     *  @return <code> true </code> if relationship administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipAdmin() {
        return (false);
    }


    /**
     *  Tests if a relationship notification service is supported. 
     *
     *  @return <code> true </code> if relationship notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipNotification() {
        return (false);
    }


    /**
     *  Tests if a relationship family cataloging service is supported. 
     *
     *  @return <code> true </code> if relationship families are supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipFamily() {
        return (false);
    }


    /**
     *  Tests if a relationship cataloging service is supported. A 
     *  relationship cataloging service maps relationships to families. 
     *
     *  @return <code> true </code> if relationship families are supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipFamilyAssignment() {
        return (false);
    }


    /**
     *  Tests if a relationship smart family cataloging service is supported. 
     *
     *  @return <code> true </code> if relationship smart families are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipSmartFamily() {
        return (false);
    }


    /**
     *  Tests if looking up families is supported. 
     *
     *  @return <code> true </code> if family lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyLookup() {
        return (false);
    }


    /**
     *  Tests if querying families is supported. 
     *
     *  @return <code> true </code> if family query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyQuery() {
        return (false);
    }


    /**
     *  Tests if searching families is supported. 
     *
     *  @return <code> true </code> if family search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilySearch() {
        return (false);
    }


    /**
     *  Tests if family <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if family administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyAdmin() {
        return (false);
    }


    /**
     *  Tests if a family notification service is supported. 
     *
     *  @return <code> true </code> if family notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a family hierarchy traversal service. 
     *
     *  @return <code> true </code> if family hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a family hierarchy design service. 
     *
     *  @return <code> true </code> if family hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a relationship batch service. 
     *
     *  @return <code> true </code> if a relationship batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipBatch() {
        return (false);
    }


    /**
     *  Tests if a relationship rules service is supported. 
     *
     *  @return <code> true </code> if relationship rules service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Relationship </code> record types. 
     *
     *  @return a list containing the supported <code> Relationship </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelationshipRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relationshipRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Relationship </code> record type is 
     *  supported. 
     *
     *  @param  relationshipRecordType a <code> Type </code> indicating a 
     *          <code> Relationship </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> relationshipRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelationshipRecordType(org.osid.type.Type relationshipRecordType) {
        return (this.relationshipRecordTypes.contains(relationshipRecordType));
    }


    /**
     *  Adds support for a relationship record type.
     *
     *  @param relationshipRecordType a relationship record type
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipRecordType</code> is <code>null</code>
     */

    protected void addRelationshipRecordType(org.osid.type.Type relationshipRecordType) {
        this.relationshipRecordTypes.add(relationshipRecordType);
        return;
    }


    /**
     *  Removes support for a relationship record type.
     *
     *  @param relationshipRecordType a relationship record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipRecordType</code> is <code>null</code>
     */

    protected void removeRelationshipRecordType(org.osid.type.Type relationshipRecordType) {
        this.relationshipRecordTypes.remove(relationshipRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Relationship </code> search record types. 
     *
     *  @return a list containing the supported <code> Relationship </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelationshipSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relationshipSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Relationship </code> search record type is 
     *  supported. 
     *
     *  @param  relationshipSearchRecordType a <code> Type </code> indicating 
     *          a <code> Relationship </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelationshipSearchRecordType(org.osid.type.Type relationshipSearchRecordType) {
        return (this.relationshipSearchRecordTypes.contains(relationshipSearchRecordType));
    }


    /**
     *  Adds support for a relationship search record type.
     *
     *  @param relationshipSearchRecordType a relationship search record type
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipSearchRecordType</code> is <code>null</code>
     */

    protected void addRelationshipSearchRecordType(org.osid.type.Type relationshipSearchRecordType) {
        this.relationshipSearchRecordTypes.add(relationshipSearchRecordType);
        return;
    }


    /**
     *  Removes support for a relationship search record type.
     *
     *  @param relationshipSearchRecordType a relationship search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relationshipSearchRecordType</code> is <code>null</code>
     */

    protected void removeRelationshipSearchRecordType(org.osid.type.Type relationshipSearchRecordType) {
        this.relationshipSearchRecordTypes.remove(relationshipSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Family </code> record types. 
     *
     *  @return a list containing the supported <code> Family </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFamilyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.familyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Family </code> record type is supported. 
     *
     *  @param  familyRecordType a <code> Type </code> indicating a <code> 
     *          Family </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> familyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFamilyRecordType(org.osid.type.Type familyRecordType) {
        return (this.familyRecordTypes.contains(familyRecordType));
    }


    /**
     *  Adds support for a family record type.
     *
     *  @param familyRecordType a family record type
     *  @throws org.osid.NullArgumentException
     *  <code>familyRecordType</code> is <code>null</code>
     */

    protected void addFamilyRecordType(org.osid.type.Type familyRecordType) {
        this.familyRecordTypes.add(familyRecordType);
        return;
    }


    /**
     *  Removes support for a family record type.
     *
     *  @param familyRecordType a family record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>familyRecordType</code> is <code>null</code>
     */

    protected void removeFamilyRecordType(org.osid.type.Type familyRecordType) {
        this.familyRecordTypes.remove(familyRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Family </code> search record types. 
     *
     *  @return a list containing the supported <code> Family </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFamilySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.familySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Family </code> search record type is 
     *  supported. 
     *
     *  @param  familySearchRecordType a <code> Type </code> indicating a 
     *          <code> Family </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> familySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFamilySearchRecordType(org.osid.type.Type familySearchRecordType) {
        return (this.familySearchRecordTypes.contains(familySearchRecordType));
    }


    /**
     *  Adds support for a family search record type.
     *
     *  @param familySearchRecordType a family search record type
     *  @throws org.osid.NullArgumentException
     *  <code>familySearchRecordType</code> is <code>null</code>
     */

    protected void addFamilySearchRecordType(org.osid.type.Type familySearchRecordType) {
        this.familySearchRecordTypes.add(familySearchRecordType);
        return;
    }


    /**
     *  Removes support for a family search record type.
     *
     *  @param familySearchRecordType a family search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>familySearchRecordType</code> is <code>null</code>
     */

    protected void removeFamilySearchRecordType(org.osid.type.Type familySearchRecordType) {
        this.familySearchRecordTypes.remove(familySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  lookup service. 
     *
     *  @return a <code> RelationshipLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipLookupSession getRelationshipLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipLookupSession getRelationshipLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  lookup service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @return a <code> RelationshipLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipLookupSession getRelationshipLookupSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipLookupSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  lookup service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipLookupSession getRelationshipLookupSessionForFamily(org.osid.id.Id familyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipLookupSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  query service. 
     *
     *  @return a <code> RelationshipQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuerySession getRelationshipQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuerySession getRelationshipQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  query service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @return a <code> RelationshipQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuerySession getRelationshipQuerySessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipQuerySessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  query service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuerySession getRelationshipQuerySessionForFamily(org.osid.id.Id familyId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipQuerySessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  search service. 
     *
     *  @return a <code> RelationshipSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSearchSession getRelationshipSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSearchSession getRelationshipSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  search service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipSearchSession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSearchSession getRelationshipSearchSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipSearchSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  search service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSearchSession getRelationshipSearchSessionForFamily(org.osid.id.Id familyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipSearchSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  administration service. 
     *
     *  @return a <code> RelationshipAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipAdminSession getRelationshipAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipAdminSession getRelationshipAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipAdminSession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipAdminSession getRelationshipAdminSessionForFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipAdminSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipAdminSession getRelationshipAdminSessionForFamily(org.osid.id.Id familyId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipAdminSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  notification service. 
     *
     *  @param  relationshipReceiver the receiver 
     *  @return a <code> RelationshipNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relationshipReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipNotificationSession getRelationshipNotificationSession(org.osid.relationship.RelationshipReceiver relationshipReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  notification service. 
     *
     *  @param  relationshipReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relationshipReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipNotificationSession getRelationshipNotificationSession(org.osid.relationship.RelationshipReceiver relationshipReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  notification service for the given family. 
     *
     *  @param  relationshipReceiver the receiver 
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipNotificationSession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> relationshipReceiver 
     *          </code> or <code> familyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipNotificationSession getRelationshipNotificationSessionForFamily(org.osid.relationship.RelationshipReceiver relationshipReceiver, 
                                                                                                             org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipNotificationSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  notification service for the given family. 
     *
     *  @param  relationshipReceiver the receiver 
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> relationshipReceiver, 
     *          familyId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipNotificationSession getRelationshipNotificationSessionForFamily(org.osid.relationship.RelationshipReceiver relationshipReceiver, 
                                                                                                             org.osid.id.Id familyId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipNotificationSessionForFamily not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relationship/family 
     *  mappings. 
     *
     *  @return a <code> RelationshipFamilySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipFamily() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipFamilySession getRelationshipFamilySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relationship/family 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipFamilySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipFamily() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipFamilySession getRelationshipFamilySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relationships to families. 
     *
     *  @return a <code> RelationshipFamilyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipFamilyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipFamilyAssignmentSession getRelationshipFamilyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipFamilyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relationships to families. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipFamilyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipFamilyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipFamilyAssignmentSession getRelationshipFamilyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipFamilyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic families of 
     *  retlationships. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @return a <code> RelationshipSmartFamilySession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSmartFamily() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSmartFamilySession getRelationshipSmartFamilySession(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipSmartFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic families of 
     *  retlationships. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipSmartFamilySession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSmartFamily() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSmartFamilySession getRelationshipSmartFamilySession(org.osid.id.Id familyId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipSmartFamilySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family lookup 
     *  service. 
     *
     *  @return a <code> FamilyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyLookupSession getFamilyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getFamilyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyLookupSession getFamilyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getFamilyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family query 
     *  service. 
     *
     *  @return a <code> FamilyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuerySession getFamilyQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getFamilyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuerySession getFamilyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getFamilyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family search 
     *  service. 
     *
     *  @return a <code> FamilySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilySearchSession getFamilySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getFamilySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilySearchSession getFamilySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getFamilySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  administrative service. 
     *
     *  @return a <code> FamilyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyAdminSession getFamilyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getFamilyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyAdminSession getFamilyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getFamilyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  notification service. 
     *
     *  @param  familyReceiver the receiver 
     *  @return a <code> FamilyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> familyReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyNotificationSession getFamilyNotificationSession(org.osid.relationship.FamilyReceiver familyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getFamilyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  notification service. 
     *
     *  @param  familyReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> FamilyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> familyReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyNotificationSession getFamilyNotificationSession(org.osid.relationship.FamilyReceiver familyReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getFamilyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  hierarchy service. 
     *
     *  @return a <code> FamilyHierarchySession </code> for families 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyHierarchySession getFamilyHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getFamilyHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyHierarchySession </code> for families 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyHierarchySession getFamilyHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getFamilyHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for families 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyHierarchyDesignSession getFamilyHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getFamilyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for families 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyHierarchyDesignSession getFamilyHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getFamilyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the relationship batch manager. 
     *
     *  @return a <code> RelationshipBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchManager getRelationshipBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipBatchManager not implemented");
    }


    /**
     *  Gets the relationship batch proxy manager. 
     *
     *  @return a <code> RelationshipBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchProxyManager getRelationshipBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipBatchProxyManager not implemented");
    }


    /**
     *  Gets the relationship rules manager. 
     *
     *  @return a <code> RelationshipRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipRulesManager getRelationshipRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipManager.getRelationshipRulesManager not implemented");
    }


    /**
     *  Gets the relationship rules proxy manager. 
     *
     *  @return a <code> RelationshipRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipRulesProxyManager getRelationshipRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.relationship.RelationshipProxyManager.getRelationshipRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.relationshipRecordTypes.clear();
        this.relationshipRecordTypes.clear();

        this.relationshipSearchRecordTypes.clear();
        this.relationshipSearchRecordTypes.clear();

        this.familyRecordTypes.clear();
        this.familyRecordTypes.clear();

        this.familySearchRecordTypes.clear();
        this.familySearchRecordTypes.clear();

        return;
    }
}
