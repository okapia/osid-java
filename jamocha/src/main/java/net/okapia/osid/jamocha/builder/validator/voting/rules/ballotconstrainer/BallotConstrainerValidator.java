//
// BallotConstrainerValidator.java
//
//     Validates a BallotConstrainer.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.voting.rules.ballotconstrainer;


/**
 *  Validates a BallotConstrainer.
 */

public final class BallotConstrainerValidator
    extends net.okapia.osid.jamocha.builder.validator.voting.rules.ballotconstrainer.spi.AbstractBallotConstrainerValidator {


    /**
     *  Constructs a new <code>BallotConstrainerValidator</code>.
     */

    public BallotConstrainerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>BallotConstrainerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public BallotConstrainerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a BallotConstrainer with a default validation.
     *
     *  @param ballotConstrainer a ballot constrainer to validate
     *  @return the ballot constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>ballotConstrainer</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.voting.rules.BallotConstrainer validateBallotConstrainer(org.osid.voting.rules.BallotConstrainer ballotConstrainer) {
        BallotConstrainerValidator validator = new BallotConstrainerValidator();
        validator.validate(ballotConstrainer);
        return (ballotConstrainer);
    }


    /**
     *  Validates a BallotConstrainer for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param ballotConstrainer a ballot constrainer to validate
     *  @return the ballot constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>ballotConstrainer</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.voting.rules.BallotConstrainer validateBallotConstrainer(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.voting.rules.BallotConstrainer ballotConstrainer) {

        BallotConstrainerValidator validator = new BallotConstrainerValidator(validation);
        validator.validate(ballotConstrainer);
        return (ballotConstrainer);
    }
}
