//
// AbstractContactEnablerLookupSession.java
//
//    A starter implementation framework for providing a ContactEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ContactEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getContactEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractContactEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.contact.rules.ContactEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.contact.AddressBook addressBook = new net.okapia.osid.jamocha.nil.contact.addressbook.UnknownAddressBook();
    

    /**
     *  Gets the <code>AddressBook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AddressBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.addressBook.getId());
    }


    /**
     *  Gets the <code>AddressBook</code> associated with this 
     *  session.
     *
     *  @return the <code>AddressBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.addressBook);
    }


    /**
     *  Sets the <code>AddressBook</code>.
     *
     *  @param  addressBook the address book for this session
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>
     */

    protected void setAddressBook(org.osid.contact.AddressBook addressBook) {
        nullarg(addressBook, "address book");
        this.addressBook = addressBook;
        return;
    }


    /**
     *  Tests if this user can perform <code>ContactEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupContactEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>ContactEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeContactEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ContactEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryContactEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include contact enablers in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active contact enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveContactEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive contact enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusContactEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ContactEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ContactEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ContactEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, contact enablers are returned that are currently
     *  active. In any status mode, active and inactive contact enablers
     *  are returned.
     *
     *  @param  contactEnablerId <code>Id</code> of the
     *          <code>ContactEnabler</code>
     *  @return the contact enabler
     *  @throws org.osid.NotFoundException <code>contactEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>contactEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnabler getContactEnabler(org.osid.id.Id contactEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.contact.rules.ContactEnablerList contactEnablers = getContactEnablers()) {
            while (contactEnablers.hasNext()) {
                org.osid.contact.rules.ContactEnabler contactEnabler = contactEnablers.getNextContactEnabler();
                if (contactEnabler.getId().equals(contactEnablerId)) {
                    return (contactEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(contactEnablerId + " not found");
    }


    /**
     *  Gets a <code>ContactEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  contactEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ContactEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, contact enablers are returned that are currently
     *  active. In any status mode, active and inactive contact enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getContactEnablers()</code>.
     *
     *  @param  contactEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByIds(org.osid.id.IdList contactEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.contact.rules.ContactEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = contactEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getContactEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("contact enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.contact.rules.contactenabler.LinkedContactEnablerList(ret));
    }


    /**
     *  Gets a <code>ContactEnablerList</code> corresponding to the given
     *  contact enabler genus <code>Type</code> which does not include
     *  contact enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  contact enablers or an error results. Otherwise, the returned list
     *  may contain only those contact enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, contact enablers are returned that are currently
     *  active. In any status mode, active and inactive contact enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getContactEnablers()</code>.
     *
     *  @param  contactEnablerGenusType a contactEnabler genus type 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByGenusType(org.osid.type.Type contactEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.contact.rules.contactenabler.ContactEnablerGenusFilterList(getContactEnablers(), contactEnablerGenusType));
    }


    /**
     *  Gets a <code>ContactEnablerList</code> corresponding to the given
     *  contact enabler genus <code>Type</code> and include any additional
     *  contact enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  contact enablers or an error results. Otherwise, the returned list
     *  may contain only those contact enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, contact enablers are returned that are currently
     *  active. In any status mode, active and inactive contact enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getContactEnablers()</code>.
     *
     *  @param  contactEnablerGenusType a contactEnabler genus type 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByParentGenusType(org.osid.type.Type contactEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getContactEnablersByGenusType(contactEnablerGenusType));
    }


    /**
     *  Gets a <code>ContactEnablerList</code> containing the given
     *  contact enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  contact enablers or an error results. Otherwise, the returned list
     *  may contain only those contact enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, contact enablers are returned that are currently
     *  active. In any status mode, active and inactive contact enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getContactEnablers()</code>.
     *
     *  @param  contactEnablerRecordType a contactEnabler record type 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByRecordType(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.contact.rules.contactenabler.ContactEnablerRecordFilterList(getContactEnablers(), contactEnablerRecordType));
    }


    /**
     *  Gets a <code>ContactEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  contact enablers or an error results. Otherwise, the returned list
     *  may contain only those contact enablers that are accessible
     *  through this session.
     *  
     *  In active mode, contact enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive contact enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ContactEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.contact.rules.contactenabler.TemporalContactEnablerFilterList(getContactEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>ContactEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  contact enablers or an error results. Otherwise, the returned list
     *  may contain only those contact enablers that are accessible
     *  through this session.
     *
     *  In active mode, contact enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive contact enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getContactEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>ContactEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  contact enablers or an error results. Otherwise, the returned list
     *  may contain only those contact enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, contact enablers are returned that are currently
     *  active. In any status mode, active and inactive contact enablers
     *  are returned.
     *
     *  @return a list of <code>ContactEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.contact.rules.ContactEnablerList getContactEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the contact enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of contact enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.contact.rules.ContactEnablerList filterContactEnablersOnViews(org.osid.contact.rules.ContactEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.contact.rules.ContactEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.contact.rules.contactenabler.ActiveContactEnablerFilterList(ret);
        }

        return (ret);
    }
}
