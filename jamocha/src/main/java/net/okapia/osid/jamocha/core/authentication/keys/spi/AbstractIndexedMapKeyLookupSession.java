//
// AbstractIndexedMapKeyLookupSession.java
//
//    A simple framework for providing a Key lookup service
//    backed by a fixed collection of keys with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Key lookup service backed by a
 *  fixed collection of keys. The keys are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some keys may be compatible
 *  with more types than are indicated through these key
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Keys</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapKeyLookupSession
    extends AbstractMapKeyLookupSession
    implements org.osid.authentication.keys.KeyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authentication.keys.Key> keysByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.keys.Key>());
    private final MultiMap<org.osid.type.Type, org.osid.authentication.keys.Key> keysByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.keys.Key>());


    /**
     *  Makes a <code>Key</code> available in this session.
     *
     *  @param  key a key
     *  @throws org.osid.NullArgumentException <code>key<code> is
     *          <code>null</code>
     */

    @Override
    protected void putKey(org.osid.authentication.keys.Key key) {
        super.putKey(key);

        this.keysByGenus.put(key.getGenusType(), key);
        
        try (org.osid.type.TypeList types = key.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.keysByRecord.put(types.getNextType(), key);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a key from this session.
     *
     *  @param keyId the <code>Id</code> of the key
     *  @throws org.osid.NullArgumentException <code>keyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeKey(org.osid.id.Id keyId) {
        org.osid.authentication.keys.Key key;
        try {
            key = getKey(keyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.keysByGenus.remove(key.getGenusType());

        try (org.osid.type.TypeList types = key.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.keysByRecord.remove(types.getNextType(), key);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeKey(keyId);
        return;
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  key genus <code>Type</code> which does not include
     *  keys of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known keys or an error results. Otherwise,
     *  the returned list may contain only those keys that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  keyGenusType a key genus type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.keys.key.ArrayKeyList(this.keysByGenus.get(keyGenusType)));
    }


    /**
     *  Gets a <code>KeyList</code> containing the given
     *  key record <code>Type</code>. In plenary mode, the
     *  returned list contains all known keys or an error
     *  results. Otherwise, the returned list may contain only those
     *  keys that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  keyRecordType a key record type 
     *  @return the returned <code>key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByRecordType(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.keys.key.ArrayKeyList(this.keysByRecord.get(keyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.keysByGenus.clear();
        this.keysByRecord.clear();

        super.close();

        return;
    }
}
