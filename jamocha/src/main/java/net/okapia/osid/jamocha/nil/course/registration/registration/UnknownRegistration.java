//
// UnknownRegistration.java
//
//     Defines an unknown Registration.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.registration.registration;


/**
 *  Defines an unknown <code>Registration</code>.
 */

public final class UnknownRegistration
    extends net.okapia.osid.jamocha.nil.course.registration.registration.spi.AbstractUnknownRegistration
    implements org.osid.course.registration.Registration {


    /**
     *  Constructs a new <code>UnknownRegistration</code>.
     */

    public UnknownRegistration() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownRegistration</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownRegistration(boolean optional) {
        super(optional);
        addRegistrationRecord(new RegistrationRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Registration.
     *
     *  @return an unknown Registration
     */

    public static org.osid.course.registration.Registration create() {
        return (net.okapia.osid.jamocha.builder.validator.course.registration.registration.RegistrationValidator.validateRegistration(new UnknownRegistration()));
    }


    public class RegistrationRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.course.registration.records.RegistrationRecord {

        
        protected RegistrationRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
