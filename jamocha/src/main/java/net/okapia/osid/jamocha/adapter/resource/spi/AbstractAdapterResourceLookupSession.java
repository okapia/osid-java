//
// AbstractAdapterResourceLookupSession.java
//
//    A Resource lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Resource lookup session adapter.
 */

public abstract class AbstractAdapterResourceLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.ResourceLookupSession {

    private final org.osid.resource.ResourceLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterResourceLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterResourceLookupSession(org.osid.resource.ResourceLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bin/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bin Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the {@code Bin} associated with this session.
     *
     *  @return the {@code Bin} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform {@code Resource} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupResources() {
        return (this.session.canLookupResources());
    }


    /**
     *  A complete view of the {@code Resource} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResourceView() {
        this.session.useComparativeResourceView();
        return;
    }


    /**
     *  A complete view of the {@code Resource} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResourceView() {
        this.session.usePlenaryResourceView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resources in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    
     
    /**
     *  Gets the {@code Resource} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Resource} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Resource} and
     *  retained for compatibility.
     *
     *  @param resourceId {@code Id} of the {@code Resource}
     *  @return the resource
     *  @throws org.osid.NotFoundException {@code resourceId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResource(resourceId));
    }


    /**
     *  Gets a {@code ResourceList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  resources specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Resources} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  resourceIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Resource} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code resourceIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByIds(org.osid.id.IdList resourceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourcesByIds(resourceIds));
    }


    /**
     *  Gets a {@code ResourceList} corresponding to the given
     *  resource genus {@code Type} which does not include
     *  resources of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned {@code Resource} list
     *  @throws org.osid.NullArgumentException
     *          {@code resourceGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourcesByGenusType(resourceGenusType));
    }


    /**
     *  Gets a {@code ResourceList} corresponding to the given
     *  resource genus {@code Type} and include any additional
     *  resources with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned {@code Resource} list
     *  @throws org.osid.NullArgumentException
     *          {@code resourceGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByParentGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourcesByParentGenusType(resourceGenusType));
    }


    /**
     *  Gets a {@code ResourceList} containing the given
     *  resource record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return the returned {@code Resource} list
     *  @throws org.osid.NullArgumentException
     *          {@code resourceRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByRecordType(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourcesByRecordType(resourceRecordType));
    }


    /**
     *  Gets all {@code Resources}. 
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Resources} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResources());
    }
}
