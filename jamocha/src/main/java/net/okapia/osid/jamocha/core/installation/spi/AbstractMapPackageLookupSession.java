//
// AbstractMapPackageLookupSession
//
//    A simple framework for providing a Package lookup service
//    backed by a fixed collection of packages.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Simple implementation of a Package lookup service backed by a
 *  fixed collection of packages. The packages are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Packages</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPackageLookupSession
    extends net.okapia.osid.jamocha.installation.spi.AbstractPackageLookupSession
    implements org.osid.installation.PackageLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.installation.Package> packages = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.installation.Package>());
    private final java.util.Collection<java.util.Collection<org.osid.id.Id>> chains = java.util.Collections.synchronizedList(new java.util.ArrayList<java.util.Collection<org.osid.id.Id>>());


    /**
     *  Makes a <code>Package</code> available in this session.
     *
     *  @param  pkg a package
     *  @throws org.osid.NullArgumentException <code>pkg<code>
     *          is <code>null</code>
     */

    protected void putPackage(org.osid.installation.Package pkg) {
        this.packages.put(pkg.getId(), pkg);
        return;
    }


    /**
     *  Makes an array of packages available in this session.
     *
     *  @param  packages an array of packages
     *  @throws org.osid.NullArgumentException <code>packages<code>
     *          is <code>null</code>
     */

    protected void putPackages(org.osid.installation.Package[] packages) {
        putPackages(java.util.Arrays.asList(packages));
        return;
    }


    /**
     *  Makes a collection of packages available in this session.
     *
     *  @param  packages a collection of packages
     *  @throws org.osid.NullArgumentException <code>packages<code>
     *          is <code>null</code>
     */

    protected void putPackages(java.util.Collection<? extends org.osid.installation.Package> packages) {
        for (org.osid.installation.Package pkg : packages) {
            this.packages.put(pkg.getId(), pkg);
        }

        return;
    }


    /**
     *  Removes a Package from this session.
     *
     *  @param  pkgId the <code>Id</code> of the package
     *  @throws org.osid.NullArgumentException <code>pkgId<code> is
     *          <code>null</code>
     */

    protected void removePackage(org.osid.id.Id pkgId) {
        this.packages.remove(pkgId);
        return;
    }


    /**
     *  Starts a new version chain.  A package can only belong to one
     *  chain.
     *
     *  @param packageId a package Id 
     *  @throws org.osid.NullArgumentException <code>packageId<code>
     *          is <code>null</code>
     */

    protected synchronized void createPackageVersionChain(org.osid.id.Id packageId) {
        nullarg(packageId, "package Id");

        for (java.util.Collection<org.osid.id.Id> chain : this.chains) {
            if (chain.contains(packageId)) {
                return;
            }
        }

        java.util.Collection<org.osid.id.Id> chain = java.util.Collections.synchronizedSet(new java.util.HashSet<org.osid.id.Id>());

        chain.add(packageId);
        this.chains.add(chain);

        return;
    }


    /**
     *  Adds a <code>Package</code> to the version chain containing
     *  another package. A package can only belong to one chain.
     *
     *  This method does not check for the existence of the package
     *  Ids.
     *
     *  @param packageId a package Id to add
     *  @param existingPackageId the existing package Id in a chain
     *  @throws org.osid.NullArgumentException <code>packageId<code>
     *          or <code>existingPackageId</code> is <code>null</code>
     */

    protected synchronized void addPackageVersion(org.osid.id.Id packageId, 
                                                  org.osid.id.Id existingPackageId) {

        nullarg(packageId, "package Id");
        nullarg(existingPackageId, "existing package Id");

        for (java.util.Collection<org.osid.id.Id> chain : this.chains) {
            if (chain.contains(existingPackageId)) {
                chain.add(packageId);
                return;
            }
        }

        return;
    }


    /**
     *  Removes a <code>Package</code> from its version chain.
     *
     *  @param packageId a package to remove
     *  @throws org.osid.NullArgumentException <code>packageId<code>
     *          is <code>null</code>
     */

    protected synchronized void removePackageVersion(org.osid.id.Id packageId) {
        nullarg(packageId, "package Id");

        for (java.util.Collection<org.osid.id.Id> chain : this.chains) {
            if (chain.contains(packageId)) {
                chain.remove(packageId);
                if (chain.size() == 0) {
                    this.chains.remove(chain);
                }

                return;
            }
        }

        return;
    }


    /**
     *  Gets the <code>Package</code> specified by its <code>Id</code>.
     *
     *  @param  pkgId <code>Id</code> of the <code>Package</code>
     *  @return the pkg
     *  @throws org.osid.NotFoundException <code>pkgId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>pkgId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage(org.osid.id.Id pkgId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.installation.Package pkg = this.packages.get(pkgId);
        if (pkg == null) {
            throw new org.osid.NotFoundException("pkg not found: " + pkgId);
        }

        return (pkg);
    }


    /**
     *  Gets a list of packages in the specified package version chain. 
     *
     *  @param  packageId an <code>Id</code> of a <code>Package</code> 
     *  @return list of dependencies 
     *  @throws org.osid.NotFoundException <code>packageId</code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code>packageId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackageVersions(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.id.Id> ret = new java.util.ArrayList<>();

        for (java.util.Collection<org.osid.id.Id> chain : this.chains) {
            if (chain.contains(packageId)) {
                ret.addAll(chain);
                break;
            }
        }
        
        // could sort by version
        return (getPackagesByIds(new net.okapia.osid.jamocha.id.id.ArrayIdList(ret)));
    }
        

    /**
     *  Gets all <code>Packages</code>. In plenary mode, the returned
     *  list contains all known packages or an error
     *  results. Otherwise, the returned list may contain only those
     *  packages that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Packages</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.pkg.ArrayPackageList(this.packages.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.packages.clear();
        this.chains.clear();

        super.close();
        return;
    }
}
