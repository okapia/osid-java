//
// AbstractImmutableAgenda.java
//
//     Wraps a mutable Agenda to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.agenda.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Agenda</code> to hide modifiers. This
 *  wrapper provides an immutized Agenda from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying agenda whose state changes are visible.
 */

public abstract class AbstractImmutableAgenda
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.rules.check.Agenda {

    private final org.osid.rules.check.Agenda agenda;


    /**
     *  Constructs a new <code>AbstractImmutableAgenda</code>.
     *
     *  @param agenda the agenda to immutablize
     *  @throws org.osid.NullArgumentException <code>agenda</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAgenda(org.osid.rules.check.Agenda agenda) {
        super(agenda);
        this.agenda = agenda;
        return;
    }


    /**
     *  Gets the agenda record corresponding to the given <code> Agenda 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> agendaRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(agendaRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  agendaRecordType the type of agenda record to retrieve 
     *  @return the agenda record 
     *  @throws org.osid.NullArgumentException <code> agendaRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(agendaRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaRecord getAgendaRecord(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.agenda.getAgendaRecord(agendaRecordType));
    }
}

