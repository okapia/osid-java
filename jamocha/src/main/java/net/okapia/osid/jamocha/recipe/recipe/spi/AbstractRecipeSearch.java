//
// AbstractRecipeSearch.java
//
//     A template for making a Recipe Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.recipe.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing recipe searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRecipeSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.recipe.RecipeSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.recipe.records.RecipeSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.recipe.RecipeSearchOrder recipeSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of recipes. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  recipeIds list of recipes
     *  @throws org.osid.NullArgumentException
     *          <code>recipeIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRecipes(org.osid.id.IdList recipeIds) {
        while (recipeIds.hasNext()) {
            try {
                this.ids.add(recipeIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRecipes</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of recipe Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRecipeIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  recipeSearchOrder recipe search order 
     *  @throws org.osid.NullArgumentException
     *          <code>recipeSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>recipeSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRecipeResults(org.osid.recipe.RecipeSearchOrder recipeSearchOrder) {
	this.recipeSearchOrder = recipeSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.recipe.RecipeSearchOrder getRecipeSearchOrder() {
	return (this.recipeSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given recipe search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a recipe implementing the requested record.
     *
     *  @param recipeSearchRecordType a recipe search record
     *         type
     *  @return the recipe search record
     *  @throws org.osid.NullArgumentException
     *          <code>recipeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recipeSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeSearchRecord getRecipeSearchRecord(org.osid.type.Type recipeSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.recipe.records.RecipeSearchRecord record : this.records) {
            if (record.implementsRecordType(recipeSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recipeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recipe search. 
     *
     *  @param recipeSearchRecord recipe search record
     *  @param recipeSearchRecordType recipe search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecipeSearchRecord(org.osid.recipe.records.RecipeSearchRecord recipeSearchRecord, 
                                           org.osid.type.Type recipeSearchRecordType) {

        addRecordType(recipeSearchRecordType);
        this.records.add(recipeSearchRecord);        
        return;
    }
}
