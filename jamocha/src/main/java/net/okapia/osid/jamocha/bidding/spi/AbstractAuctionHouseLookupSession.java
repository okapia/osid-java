//
// AbstractAuctionHouseLookupSession.java
//
//    A starter implementation framework for providing an AuctionHouse
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an AuctionHouse
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAuctionHouses(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.AuctionHouseLookupSession {

    private boolean pedantic      = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();
    


    /**
     *  Tests if this user can perform <code>AuctionHouse</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionHouses() {
        return (true);
    }


    /**
     *  A complete view of the <code>AuctionHouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionHouseView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>AuctionHouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionHouseView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>AuctionHouse</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionHouse</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionHouse</code> and
     *  retained for compatibility.
     *
     *  @param  auctionHouseId <code>Id</code> of the
     *          <code>AuctionHouse</code>
     *  @return the auction house
     *  @throws org.osid.NotFoundException <code>auctionHouseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionHouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.bidding.AuctionHouseList auctionHouses = getAuctionHouses()) {
            while (auctionHouses.hasNext()) {
                org.osid.bidding.AuctionHouse auctionHouse = auctionHouses.getNextAuctionHouse();
                if (auctionHouse.getId().equals(auctionHouseId)) {
                    return (auctionHouse);
                }
            }
        } 

        throw new org.osid.NotFoundException(auctionHouseId + " not found");
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionHouses specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>AuctionHouses</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAuctionHouses()</code>.
     *
     *  @param  auctionHouseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByIds(org.osid.id.IdList auctionHouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.bidding.AuctionHouse> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = auctionHouseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAuctionHouse(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("auction house " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.bidding.auctionhouse.LinkedAuctionHouseList(ret));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  auction house genus <code>Type</code> which does not include
     *  auction houses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known auction
     *  houses or an error results. Otherwise, the returned list may
     *  contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAuctionHouses()</code>.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.auctionhouse.AuctionHouseGenusFilterList(getAuctionHouses(), auctionHouseGenusType));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the
     *  given auction house genus <code>Type</code> and include any
     *  additional auction houses with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  houses or an error results. Otherwise, the returned list may
     *  contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionHouses()</code>.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByParentGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuctionHousesByGenusType(auctionHouseGenusType));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> containing the given
     *  auction house record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known auction
     *  houses or an error results. Otherwise, the returned list may
     *  contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionHouses()</code>.
     *
     *  @param  auctionHouseRecordType an auctionHouse record type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByRecordType(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.auctionhouse.AuctionHouseRecordFilterList(getAuctionHouses(), auctionHouseRecordType));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known auction
     *  houses or an error results. Otherwise, the returned list may
     *  contain only those auction houses that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>AuctionHouse</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.bidding.auctionhouse.AuctionHouseProviderFilterList(getAuctionHouses(), resourceId));
    }


    /**
     *  Gets all <code>AuctionHouses</code>. 
     *
     *  In plenary mode, the returned list contains all known auction
     *  houses or an error results. Otherwise, the returned list may
     *  contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AuctionHouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.bidding.AuctionHouseList getAuctionHouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the auction house list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of auction houses
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.bidding.AuctionHouseList filterAuctionHousesOnViews(org.osid.bidding.AuctionHouseList list)
        throws org.osid.OperationFailedException {

        org.osid.bidding.AuctionHouseList ret = list;
        return (ret);
    }
}
