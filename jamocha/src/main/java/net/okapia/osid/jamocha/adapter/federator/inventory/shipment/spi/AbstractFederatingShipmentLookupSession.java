//
// AbstractFederatingShipmentLookupSession.java
//
//     An abstract federating adapter for a ShipmentLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ShipmentLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingShipmentLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inventory.shipment.ShipmentLookupSession>
    implements org.osid.inventory.shipment.ShipmentLookupSession {

    private boolean parallel = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();


    /**
     *  Constructs a new <code>AbstractFederatingShipmentLookupSession</code>.
     */

    protected AbstractFederatingShipmentLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inventory.shipment.ShipmentLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Shipment</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupShipments() {
        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            if (session.canLookupShipments()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Shipment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeShipmentView() {
        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            session.useComparativeShipmentView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Shipment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryShipmentView() {
        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            session.usePlenaryShipmentView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include shipments in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            session.useFederatedWarehouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            session.useIsolatedWarehouseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Shipment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Shipment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Shipment</code> and
     *  retained for compatibility.
     *
     *  @param  shipmentId <code>Id</code> of the
     *          <code>Shipment</code>
     *  @return the shipment
     *  @throws org.osid.NotFoundException <code>shipmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>shipmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.Shipment getShipment(org.osid.id.Id shipmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            try {
                return (session.getShipment(shipmentId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(shipmentId + " not found");
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  shipments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Shipments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  shipmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByIds(org.osid.id.IdList shipmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inventory.shipment.shipment.MutableShipmentList ret = new net.okapia.osid.jamocha.inventory.shipment.shipment.MutableShipmentList();

        try (org.osid.id.IdList ids = shipmentIds) {
            while (ids.hasNext()) {
                ret.addShipment(getShipment(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  shipment genus <code>Type</code> which does not include
     *  shipments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsByGenusType(shipmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  shipment genus <code>Type</code> and include any additional
     *  shipments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByParentGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsByParentGenusType(shipmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> containing the given
     *  shipment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentRecordType a shipment record type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByRecordType(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsByRecordType(shipmentRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> received between the given
     *  date range inclusive.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>ShipmentList</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsOnDate(org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> for to the given stock.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  stockId a stock <code>Id</code>
     *  @return the returned <code>ShipmentList</code> list
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsForStock(stockId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> for the given stock and
     *  received between the given date range inclusive.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  stockId a stock <code>Id</code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>ShipmentList</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>stockId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStockOnDate(org.osid.id.Id stockId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsForStockOnDate(stockId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> from to the given source.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @return the returned <code>ShipmentList</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsBySource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ShipmentList</code> from the given source and
     *  received between the given date range inclusive.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>ShipmentList</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code>, </code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySourceOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
  
        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipmentsBySourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Shipments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Shipments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList ret = getShipmentList();

        for (org.osid.inventory.shipment.ShipmentLookupSession session : getSessions()) {
            ret.addShipmentList(session.getShipments());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.FederatingShipmentList getShipmentList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.ParallelShipmentList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.shipment.shipment.CompositeShipmentList());
        }
    }
}
