//
// AbstractInputEnablerQuery.java
//
//     A template for making an InputEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.inputenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for input enablers.
 */

public abstract class AbstractInputEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.control.rules.InputEnablerQuery {

    private final java.util.Collection<org.osid.control.rules.records.InputEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the input. 
     *
     *  @param  inputId the device <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inputId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledInputId(org.osid.id.Id inputId, boolean match) {
        return;
    }


    /**
     *  Clears the input <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledInputIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InputQuery </code> is available. 
     *
     *  @return <code> true </code> if an input query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledInputQuery() {
        return (false);
    }


    /**
     *  Gets the query for an input. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the input query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledInputQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuery getRuledInputQuery() {
        throw new org.osid.UnimplementedException("supportsRuledInputQuery() is false");
    }


    /**
     *  Matches enablers mapped to any input. 
     *
     *  @param  match <code> true </code> for enablers mapped to any input, 
     *          <code> false </code> to match enablers mapped to no inputs 
     */

    @OSID @Override
    public void matchAnyRuledInput(boolean match) {
        return;
    }


    /**
     *  Clears the input query terms. 
     */

    @OSID @Override
    public void clearRuledInputTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the system. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given input enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an input enabler implementing the requested record.
     *
     *  @param inputEnablerRecordType an input enabler record type
     *  @return the input enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.InputEnablerQueryRecord getInputEnablerQueryRecord(org.osid.type.Type inputEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.InputEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(inputEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this input enabler query. 
     *
     *  @param inputEnablerQueryRecord input enabler query record
     *  @param inputEnablerRecordType inputEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInputEnablerQueryRecord(org.osid.control.rules.records.InputEnablerQueryRecord inputEnablerQueryRecord, 
                                          org.osid.type.Type inputEnablerRecordType) {

        addRecordType(inputEnablerRecordType);
        nullarg(inputEnablerQueryRecord, "input enabler query record");
        this.records.add(inputEnablerQueryRecord);        
        return;
    }
}
