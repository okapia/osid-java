//
// AbstractIndexedMapAuctionProcessorLookupSession.java
//
//    A simple framework for providing an AuctionProcessor lookup service
//    backed by a fixed collection of auction processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AuctionProcessor lookup service backed by a
 *  fixed collection of auction processors. The auction processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some auction processors may be compatible
 *  with more types than are indicated through these auction processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuctionProcessorLookupSession
    extends AbstractMapAuctionProcessorLookupSession
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionProcessor> auctionProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionProcessor> auctionProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionProcessor>());


    /**
     *  Makes an <code>AuctionProcessor</code> available in this session.
     *
     *  @param  auctionProcessor an auction processor
     *  @throws org.osid.NullArgumentException <code>auctionProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuctionProcessor(org.osid.bidding.rules.AuctionProcessor auctionProcessor) {
        super.putAuctionProcessor(auctionProcessor);

        this.auctionProcessorsByGenus.put(auctionProcessor.getGenusType(), auctionProcessor);
        
        try (org.osid.type.TypeList types = auctionProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionProcessorsByRecord.put(types.getNextType(), auctionProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an auction processor from this session.
     *
     *  @param auctionProcessorId the <code>Id</code> of the auction processor
     *  @throws org.osid.NullArgumentException <code>auctionProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuctionProcessor(org.osid.id.Id auctionProcessorId) {
        org.osid.bidding.rules.AuctionProcessor auctionProcessor;
        try {
            auctionProcessor = getAuctionProcessor(auctionProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auctionProcessorsByGenus.remove(auctionProcessor.getGenusType());

        try (org.osid.type.TypeList types = auctionProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionProcessorsByRecord.remove(types.getNextType(), auctionProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuctionProcessor(auctionProcessorId);
        return;
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the given
     *  auction processor genus <code>Type</code> which does not include
     *  auction processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known auction processors or an error results. Otherwise,
     *  the returned list may contain only those auction processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auctionProcessorGenusType an auction processor genus type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessor.ArrayAuctionProcessorList(this.auctionProcessorsByGenus.get(auctionProcessorGenusType)));
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> containing the given
     *  auction processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known auction processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  auction processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auctionProcessorRecordType an auction processor record type 
     *  @return the returned <code>auctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByRecordType(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessor.ArrayAuctionProcessorList(this.auctionProcessorsByRecord.get(auctionProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionProcessorsByGenus.clear();
        this.auctionProcessorsByRecord.clear();

        super.close();

        return;
    }
}
