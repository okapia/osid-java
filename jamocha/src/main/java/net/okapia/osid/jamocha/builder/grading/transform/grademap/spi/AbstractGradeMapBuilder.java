//
// AbstractGradeMap.java
//
//     Defines a GradeMap builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.transform.grademap.spi;


/**
 *  Defines a <code>GradeMap</code> builder.
 */

public abstract class AbstractGradeMapBuilder<T extends AbstractGradeMapBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.transform.grademap.GradeMapMiter gradeMap;


    /**
     *  Constructs a new <code>AbstractGradeMapBuilder</code>.
     *
     *  @param gradeMap the grade map to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradeMapBuilder(net.okapia.osid.jamocha.builder.grading.transform.grademap.GradeMapMiter gradeMap) {
        this.gradeMap = gradeMap;
        return;
    }


    /**
     *  Builds the grade map.
     *
     *  @return the new grade map
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.transform.GradeMap build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.transform.grademap.GradeMapValidator(getValidations())).validate(this.gradeMap);
        return (new net.okapia.osid.jamocha.builder.grading.transform.grademap.ImmutableGradeMap(this.gradeMap));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the grade map miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.transform.grademap.GradeMapMiter getMiter() {
        return (this.gradeMap);
    }


    /**
     *  Sets the source grade.
     * 
     *  @param grade the source grade
     *  @throws org.osid.NUllArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public T sourceGrade(org.osid.grading.Grade grade) {
        getMiter().setSourceGrade(grade);
        return(self());
    }


    /**
     *  Sets the target grade.
     * 
     *  @param grade the target grade
     *  @throws org.osid.NUllArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public T targetGrade(org.osid.grading.Grade grade) {
        getMiter().setTargetGrade(grade);
        return(self());
    }
}       


