//
// AbstractQueryMessageLookupSession.java
//
//    An inline adapter that maps a MessageLookupSession to
//    a MessageQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a MessageLookupSession to
 *  a MessageQuerySession.
 */

public abstract class AbstractQueryMessageLookupSession
    extends net.okapia.osid.jamocha.messaging.spi.AbstractMessageLookupSession
    implements org.osid.messaging.MessageLookupSession {

    private final org.osid.messaging.MessageQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryMessageLookupSession.
     *
     *  @param querySession the underlying message query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryMessageLookupSession(org.osid.messaging.MessageQuerySession querySession) {
        nullarg(querySession, "message query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Mailbox</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Mailbox Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.session.getMailboxId());
    }


    /**
     *  Gets the <code>Mailbox</code> associated with this 
     *  session.
     *
     *  @return the <code>Mailbox</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMailbox());
    }


    /**
     *  Tests if this user can perform <code>Message</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMessages() {
        return (this.session.canSearchMessages());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include messages in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        this.session.useFederatedMailboxView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        this.session.useIsolatedMailboxView();
        return;
    }
    
     
    /**
     *  Gets the <code>Message</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Message</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Message</code> and
     *  retained for compatibility.
     *
     *  @param  messageId <code>Id</code> of the
     *          <code>Message</code>
     *  @return the message
     *  @throws org.osid.NotFoundException <code>messageId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Message getMessage(org.osid.id.Id messageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchId(messageId, true);
        org.osid.messaging.MessageList messages = this.session.getMessagesByQuery(query);
        if (messages.hasNext()) {
            return (messages.getNextMessage());
        } 
        
        throw new org.osid.NotFoundException(messageId + " not found");
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  messages specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Messages</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  messageIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>messageIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByIds(org.osid.id.IdList messageIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();

        try (org.osid.id.IdList ids = messageIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  message genus <code>Type</code> which does not include
     *  messages of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  message genus <code>Type</code> and include any additional
     *  messages with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByParentGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchParentGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code>MessageList</code> containing the given
     *  message record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  messageRecordType a message record type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByRecordType(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchRecordType(messageRecordType, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code> MessageList </code> sent within the specified
     *  range inclusive. In plenary mode, the returned list contains
     *  all known messages or an error results. Otherwise, the
     *  returned list may contain only those messages that are
     *  accessible through this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTime(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSentTime(from, to, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code> MessageList </code> of the given genus type and
     *  sent within the specified range inclusive. In plenary mode,
     *  the returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code>
     *          messageGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusType(org.osid.type.Type messageGenusType, 
                                                                            org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSentTime(from, to, true);
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }

    
    /**
     *  Gets a <code> MessageList </code> sent by the specified
     *  sender. In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list may
     *  contain only those messages that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource Id 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSender(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(resourceId, true);
        return (this.session.getMessagesByQuery(query));
    }

    
    /**
     *  Gets a <code> MessageList </code> of the given genus type and
     *  sender..  In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> or <code> messageGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSender(org.osid.id.Id resourceId, 
                                                                           org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(resourceId, true);
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code> MessageList </code> sent by the specified sender
     *  and sent time. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session
     *
     *  @param  resourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeFromSender(org.osid.id.Id resourceId, 
                                                                          org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(resourceId, true);
        query.matchSentTime(from, to, true);
        return (this.session.getMessagesByQuery(query));
    }
    
    
    /**
     *  Gets a <code> MessageList </code> of the given genus type and
     *  sent within the specified range inclusive. In plenary mode,
     *  the returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          messageGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesBySentTimeAndGenusTypeFromSender(org.osid.id.Id resourceId, 
                                                                                      org.osid.type.Type messageGenusType, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(resourceId, true);
        query.matchSentTime(from, to, true);
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code> MessageList </code> received by the specified recipient. 
     *  In plenary mode, the returned list contains all known messages or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  messages that are accessible through this session. 
     *
     *  @param  resourceId a resource Id 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code> MessageList </code> received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> or <code> messageGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeForRecipient(org.osid.id.Id resourceId, 
                                                                             org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }


    
    /**
     *  Gets a <code> MessageList </code> received by the specified
     *  resource and received time. In plenary mode, the returned list
     *  contains all known messages or an error results. Otherwise,
     *  the returned list may contain only those messages that are
     *  accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeForRecipient(org.osid.id.Id resourceId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        query.matchReceivedTime(from, to, true);
        return (this.session.getMessagesByQuery(query));
    }

    
    /**
     *  Gets a <code> MessageList </code> of the given genus type
     *  received by the specified resource and received time. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results.  Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  resourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          messageGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeForRecipient(org.osid.id.Id resourceId, 
                                                                                            org.osid.type.Type messageGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        query.matchReceivedTime(from, to, true);
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }

    
    /**
     *  Gets a <code> MessageList </code> sent by the specified sender
     *  and received by the specified recipient. In plenary mode, the
     *  returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.NullArgumentException <code> senderResourceId
     *          </code> or <code> recipientResourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                            org.osid.id.Id recipientResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(senderResourceId, true);
        query.matchRecipientId(recipientResourceId, true);
        return (this.session.getMessagesByQuery(query));
    }

    
    /**
     *  Gets a <code> MessageList </code> of the given genus type sent
     *  by the specified sender and received by the specified
     *  recipient. In plenary mode, the returned list contains all
     *  known messages or an error results. Otherwise, the returned
     *  list may contain only those messages that are accessible
     *  through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.NullArgumentException <code>
     *          senderResourceId, recipientResourceId </code> or
     *          <code> messageGenusType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                       org.osid.id.Id recipientResourceId, 
                                                                                       org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(senderResourceId, true);
        query.matchRecipientId(recipientResourceId, true);
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }

    
    /**
     *  Gets a <code> MessageList </code> by the specified sender,
     *  recipient, and received time. In plenary mode, the returned
     *  list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code>
     *          senderResourceId, recipientResourceId, from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                          org.osid.id.Id recipientResourceId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(senderResourceId, true);
        query.matchRecipientId(recipientResourceId, true);
        query.matchReceivedTime(from, to, true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets a <code> MessageList </code> of the given genus type and
     *  received by the specified resource and received time. In
     *  plenary mode, the returned list contains all known messages or
     *  an error results.  Otherwise, the returned list may contain
     *  only those messages that are accessible through this session.
     *
     *  @param  senderResourceId a resource Id 
     *  @param  recipientResourceId a resource Id 
     *  @param  messageGenusType a message genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Message </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code>
     *          senderResourceId, recipientResourceId,
     *          messageGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByReceivedTimeAndGenusTypeFromSenderForRecipient(org.osid.id.Id senderResourceId, 
                                                                                                      org.osid.id.Id recipientResourceId, 
                                                                                                      org.osid.type.Type messageGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchSenderId(senderResourceId, true);
        query.matchRecipientId(recipientResourceId, true);
        query.matchReceivedTime(from, to, true);
        query.matchGenusType(messageGenusType, true);
        return (this.session.getMessagesByQuery(query));
    }
    
    
    /**
     *  Gets all <code>Messages</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  messages or an error results. Otherwise, the returned list
     *  may contain only those messages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Messages</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.messaging.MessageQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getMessagesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.messaging.MessageQuery getQuery() {
        org.osid.messaging.MessageQuery query = this.session.getMessageQuery();
        return (query);
    }
}
