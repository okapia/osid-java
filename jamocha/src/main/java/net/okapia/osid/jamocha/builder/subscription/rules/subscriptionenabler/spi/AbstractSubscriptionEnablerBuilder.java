//
// AbstractSubscriptionEnabler.java
//
//     Defines a SubscriptionEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.subscription.rules.subscriptionenabler.spi;


/**
 *  Defines a <code>SubscriptionEnabler</code> builder.
 */

public abstract class AbstractSubscriptionEnablerBuilder<T extends AbstractSubscriptionEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.subscription.rules.subscriptionenabler.SubscriptionEnablerMiter subscriptionEnabler;


    /**
     *  Constructs a new <code>AbstractSubscriptionEnablerBuilder</code>.
     *
     *  @param subscriptionEnabler the subscription enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSubscriptionEnablerBuilder(net.okapia.osid.jamocha.builder.subscription.rules.subscriptionenabler.SubscriptionEnablerMiter subscriptionEnabler) {
        super(subscriptionEnabler);
        this.subscriptionEnabler = subscriptionEnabler;
        return;
    }


    /**
     *  Builds the subscription enabler.
     *
     *  @return the new subscription enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.subscription.rules.SubscriptionEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.subscription.rules.subscriptionenabler.SubscriptionEnablerValidator(getValidations())).validate(this.subscriptionEnabler);
        return (new net.okapia.osid.jamocha.builder.subscription.rules.subscriptionenabler.ImmutableSubscriptionEnabler(this.subscriptionEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the subscription enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.subscription.rules.subscriptionenabler.SubscriptionEnablerMiter getMiter() {
        return (this.subscriptionEnabler);
    }


    /**
     *  Adds a SubscriptionEnabler record.
     *
     *  @param record a subscription enabler record
     *  @param recordType the type of subscription enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.subscription.rules.records.SubscriptionEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addSubscriptionEnablerRecord(record, recordType);
        return (self());
    }
}       


