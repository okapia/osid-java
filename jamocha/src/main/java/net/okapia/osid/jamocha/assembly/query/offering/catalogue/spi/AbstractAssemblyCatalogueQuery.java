//
// AbstractAssemblyCatalogueQuery.java
//
//     A CatalogueQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.catalogue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CatalogueQuery that stores terms.
 */

public abstract class AbstractAssemblyCatalogueQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.offering.CatalogueQuery,
               org.osid.offering.CatalogueQueryInspector,
               org.osid.offering.CatalogueSearchOrder {

    private final java.util.Collection<org.osid.offering.records.CatalogueQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.CatalogueQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.CatalogueSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCatalogueQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCatalogueQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the canonical unit <code> Id </code> for this query to match 
     *  canonical units assigned to catalogues. 
     *
     *  @param  canonicalUnitId a canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCanonicalUnitId(org.osid.id.Id canonicalUnitId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCanonicalUnitIdColumn(), canonicalUnitId, match);
        return;
    }


    /**
     *  Clears all canonical unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCanonicalUnitIdTerms() {
        getAssembler().clearTerms(getCanonicalUnitIdColumn());
        return;
    }


    /**
     *  Gets the canonical unit <code> Id </code> query terms. 
     *
     *  @return the canonical unit <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCanonicalUnitIdTerms() {
        return (getAssembler().getIdTerms(getCanonicalUnitIdColumn()));
    }


    /**
     *  Gets the CanonicalUnitId column name.
     *
     * @return the column name
     */

    protected String getCanonicalUnitIdColumn() {
        return ("canonical_unit_id");
    }


    /**
     *  Tests if a canonical unit query is available. 
     *
     *  @return <code> true </code> if a canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. 
     *
     *  @return the canonical unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuery getCanonicalUnitQuery() {
        throw new org.osid.UnimplementedException("supportsCanonicalUnitQuery() is false");
    }


    /**
     *  Matches catalogues with any canonical unit. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          canonical unit, <code> false </code> to match catalogues with 
     *          no canonical units 
     */

    @OSID @Override
    public void matchAnyCanonicalUnit(boolean match) {
        getAssembler().addIdWildcardTerm(getCanonicalUnitColumn(), match);
        return;
    }


    /**
     *  Clears all canonical unit terms. 
     */

    @OSID @Override
    public void clearCanonicalUnitTerms() {
        getAssembler().clearTerms(getCanonicalUnitColumn());
        return;
    }


    /**
     *  Gets the canonical unit query terms. 
     *
     *  @return the canonical unit terms 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQueryInspector[] getCanonicalUnitTerms() {
        return (new org.osid.offering.CanonicalUnitQueryInspector[0]);
    }


    /**
     *  Gets the CanonicalUnit column name.
     *
     * @return the column name
     */

    protected String getCanonicalUnitColumn() {
        return ("canonical_unit");
    }


    /**
     *  Sets the offering <code> Id </code> for this query to match offerings 
     *  assigned to catalogues. 
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offeringId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfferingId(org.osid.id.Id offeringId, boolean match) {
        getAssembler().addIdTerm(getOfferingIdColumn(), offeringId, match);
        return;
    }


    /**
     *  Clears all offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOfferingIdTerms() {
        getAssembler().clearTerms(getOfferingIdColumn());
        return;
    }


    /**
     *  Gets the offering <code> Id </code> query terms. 
     *
     *  @return the offering <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfferingIdTerms() {
        return (getAssembler().getIdTerms(getOfferingIdColumn()));
    }


    /**
     *  Gets the OfferingId column name.
     *
     * @return the column name
     */

    protected String getOfferingIdColumn() {
        return ("offering_id");
    }


    /**
     *  Tests if an offering query is available. 
     *
     *  @return <code> true </code> if an offering query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offering. 
     *
     *  @return the offering query 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuery getOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsOfferingQuery() is false");
    }


    /**
     *  Matches catalogues with any offering. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          offering, <code> false </code> to match catalogues with no 
     *          offerings 
     */

    @OSID @Override
    public void matchAnyOffering(boolean match) {
        getAssembler().addIdWildcardTerm(getOfferingColumn(), match);
        return;
    }


    /**
     *  Clears all offering terms. 
     */

    @OSID @Override
    public void clearOfferingTerms() {
        getAssembler().clearTerms(getOfferingColumn());
        return;
    }


    /**
     *  Gets the offering query terms. 
     *
     *  @return the offering terms 
     */

    @OSID @Override
    public org.osid.offering.OfferingQueryInspector[] getOfferingTerms() {
        return (new org.osid.offering.OfferingQueryInspector[0]);
    }


    /**
     *  Gets the Offering column name.
     *
     * @return the column name
     */

    protected String getOfferingColumn() {
        return ("offering");
    }


    /**
     *  Sets the participant <code> Id </code> for this query to match 
     *  participants assigned to catalogues. 
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> participantId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParticipantId(org.osid.id.Id participantId, boolean match) {
        getAssembler().addIdTerm(getParticipantIdColumn(), participantId, match);
        return;
    }


    /**
     *  Clears all participant <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParticipantIdTerms() {
        getAssembler().clearTerms(getParticipantIdColumn());
        return;
    }


    /**
     *  Gets the participant <code> Id </code> query terms. 
     *
     *  @return the participant <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParticipantIdTerms() {
        return (getAssembler().getIdTerms(getParticipantIdColumn()));
    }


    /**
     *  Gets the ParticipantId column name.
     *
     * @return the column name
     */

    protected String getParticipantIdColumn() {
        return ("participant_id");
    }


    /**
     *  Tests if a participant query is available. 
     *
     *  @return <code> true </code> if a participant query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantQuery() {
        return (false);
    }


    /**
     *  Gets the query for a participant. 
     *
     *  @return the participant query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuery getParticipantQuery() {
        throw new org.osid.UnimplementedException("supportsParticipantQuery() is false");
    }


    /**
     *  Matches catalogues with any participant. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          participant, <code> false </code> to match catalogues with no 
     *          participants 
     */

    @OSID @Override
    public void matchAnyParticipant(boolean match) {
        getAssembler().addIdWildcardTerm(getParticipantColumn(), match);
        return;
    }


    /**
     *  Clears all participant terms. 
     */

    @OSID @Override
    public void clearParticipantTerms() {
        getAssembler().clearTerms(getParticipantColumn());
        return;
    }


    /**
     *  Gets the participant query terms. 
     *
     *  @return the participant terms 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQueryInspector[] getParticipantTerms() {
        return (new org.osid.offering.ParticipantQueryInspector[0]);
    }


    /**
     *  Gets the Participant column name.
     *
     * @return the column name
     */

    protected String getParticipantColumn() {
        return ("participant");
    }


    /**
     *  Sets the result <code> Id </code> for this query to match results. 
     *
     *  @param  resultId an result <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultId(org.osid.id.Id resultId, boolean match) {
        getAssembler().addIdTerm(getResultIdColumn(), resultId, match);
        return;
    }


    /**
     *  Clears all result <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResultIdTerms() {
        getAssembler().clearTerms(getResultIdColumn());
        return;
    }


    /**
     *  Gets the result <code> Id </code> query terms. 
     *
     *  @return the result <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultIdTerms() {
        return (getAssembler().getIdTerms(getResultIdColumn()));
    }


    /**
     *  Gets the ResultId column name.
     *
     * @return the column name
     */

    protected String getResultIdColumn() {
        return ("result_id");
    }


    /**
     *  Tests if an result query is available. 
     *
     *  @return <code> true </code> if an result query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultQuery() {
        return (false);
    }


    /**
     *  Gets the query for an result. 
     *
     *  @return the result query 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuery getResultQuery() {
        throw new org.osid.UnimplementedException("supportsResultQuery() is false");
    }


    /**
     *  Matches catalogues with any result. 
     *
     *  @param  match <code> true </code> to match catalogues with any result, 
     *          <code> false </code> to match catalogues with no results 
     */

    @OSID @Override
    public void matchAnyResult(boolean match) {
        getAssembler().addIdWildcardTerm(getResultColumn(), match);
        return;
    }


    /**
     *  Clears all result terms. 
     */

    @OSID @Override
    public void clearResultTerms() {
        getAssembler().clearTerms(getResultColumn());
        return;
    }


    /**
     *  Gets the result query terms. 
     *
     *  @return the result terms 
     */

    @OSID @Override
    public org.osid.offering.ResultQueryInspector[] getResultTerms() {
        return (new org.osid.offering.ResultQueryInspector[0]);
    }


    /**
     *  Gets the Result column name.
     *
     * @return the column name
     */

    protected String getResultColumn() {
        return ("result");
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match 
     *  catalogues that have the specified catalogue as an ancestor. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCatalogueId(org.osid.id.Id catalogueId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears all ancestor catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogueIdTerms() {
        getAssembler().clearTerms(getAncestorCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the ancestor catalogue <code> Id </code> query terms. 
     *
     *  @return the ancestor catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getAncestorCatalogueIdColumn()));
    }


    /**
     *  Gets the AncestorCatalogueId column name.
     *
     * @return the column name
     */

    protected String getAncestorCatalogueIdColumn() {
        return ("ancestor_catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCatalogueQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getAncestorCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCatalogueQuery() is false");
    }


    /**
     *  Matches catalogues with any ancestor. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          ancestor, <code> false </code> to match root catalogues 
     */

    @OSID @Override
    public void matchAnyAncestorCatalogue(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorCatalogueColumn(), match);
        return;
    }


    /**
     *  Clears all ancestor catalogue terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogueTerms() {
        getAssembler().clearTerms(getAncestorCatalogueColumn());
        return;
    }


    /**
     *  Gets the ancestor catalogue query terms. 
     *
     *  @return the ancestor catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getAncestorCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the AncestorCatalogue column name.
     *
     * @return the column name
     */

    protected String getAncestorCatalogueColumn() {
        return ("ancestor_catalogue");
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match 
     *  catalogues that have the specified catalogue as a descendant. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCatalogueId(org.osid.id.Id catalogueId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears all descendant catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogueIdTerms() {
        getAssembler().clearTerms(getDescendantCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the descendant catalogue <code> Id </code> query terms. 
     *
     *  @return the descendant catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getDescendantCatalogueIdColumn()));
    }


    /**
     *  Gets the DescendantCatalogueId column name.
     *
     * @return the column name
     */

    protected String getDescendantCatalogueIdColumn() {
        return ("descendant_catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCatalogueQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getDescendantCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCatalogueQuery() is false");
    }


    /**
     *  Matches catalogues with any descendant. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          descendant, <code> false </code> to match leaf catalogues 
     */

    @OSID @Override
    public void matchAnyDescendantCatalogue(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantCatalogueColumn(), match);
        return;
    }


    /**
     *  Clears all descendant catalogue terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogueTerms() {
        getAssembler().clearTerms(getDescendantCatalogueColumn());
        return;
    }


    /**
     *  Gets the descendant catalogue query terms. 
     *
     *  @return the descendant catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getDescendantCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the DescendantCatalogue column name.
     *
     * @return the column name
     */

    protected String getDescendantCatalogueColumn() {
        return ("descendant_catalogue");
    }


    /**
     *  Tests if this catalogue supports the given record
     *  <code>Type</code>.
     *
     *  @param  catalogueRecordType a catalogue record type 
     *  @return <code>true</code> if the catalogueRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type catalogueRecordType) {
        for (org.osid.offering.records.CatalogueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(catalogueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  catalogueRecordType the catalogue record type 
     *  @return the catalogue query record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CatalogueQueryRecord getCatalogueQueryRecord(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CatalogueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(catalogueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogueRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  catalogueRecordType the catalogue record type 
     *  @return the catalogue query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CatalogueQueryInspectorRecord getCatalogueQueryInspectorRecord(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CatalogueQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(catalogueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogueRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param catalogueRecordType the catalogue record type
     *  @return the catalogue search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CatalogueSearchOrderRecord getCatalogueSearchOrderRecord(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CatalogueSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(catalogueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogueRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this catalogue. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param catalogueQueryRecord the catalogue query record
     *  @param catalogueQueryInspectorRecord the catalogue query inspector
     *         record
     *  @param catalogueSearchOrderRecord the catalogue search order record
     *  @param catalogueRecordType catalogue record type
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueQueryRecord</code>,
     *          <code>catalogueQueryInspectorRecord</code>,
     *          <code>catalogueSearchOrderRecord</code> or
     *          <code>catalogueRecordTypecatalogue</code> is
     *          <code>null</code>
     */
            
    protected void addCatalogueRecords(org.osid.offering.records.CatalogueQueryRecord catalogueQueryRecord, 
                                      org.osid.offering.records.CatalogueQueryInspectorRecord catalogueQueryInspectorRecord, 
                                      org.osid.offering.records.CatalogueSearchOrderRecord catalogueSearchOrderRecord, 
                                      org.osid.type.Type catalogueRecordType) {

        addRecordType(catalogueRecordType);

        nullarg(catalogueQueryRecord, "catalogue query record");
        nullarg(catalogueQueryInspectorRecord, "catalogue query inspector record");
        nullarg(catalogueSearchOrderRecord, "catalogue search odrer record");

        this.queryRecords.add(catalogueQueryRecord);
        this.queryInspectorRecords.add(catalogueQueryInspectorRecord);
        this.searchOrderRecords.add(catalogueSearchOrderRecord);
        
        return;
    }
}
