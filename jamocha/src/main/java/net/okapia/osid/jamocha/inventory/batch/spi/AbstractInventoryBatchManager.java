//
// AbstractInventoryBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInventoryBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.inventory.batch.InventoryBatchManager,
               org.osid.inventory.batch.InventoryBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractInventoryBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInventoryBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of items is available. 
     *
     *  @return <code> true </code> if an item bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of stocks is available. 
     *
     *  @return <code> true </code> if a stock bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of models is available. 
     *
     *  @return <code> true </code> if a model bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of inventories is available. 
     *
     *  @return <code> true </code> if an inventory bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service. 
     *
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ItemBatchAdminSession getItemBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchManager.getItemBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ItemBatchAdminSession getItemBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchProxyManager.getItemBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ItemBatchAdminSession getItemBatchAdminSessionForInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchManager.getItemBatchAdminSessionForInventory not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ItemBatchAdminSession getItemBatchAdminSessionForInventory(org.osid.id.Id inventoryId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchProxyManager.getItemBatchAdminSessionForInventory not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk stock 
     *  administration service. 
     *
     *  @return a <code> StockBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.StockBatchAdminSession getStockBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchManager.getStockBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk stock 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StockBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.StockBatchAdminSession getStockBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchProxyManager.getStockBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk stock 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @return a <code> StockBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.StockBatchAdminSession getStockBatchAdminSessionForInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchManager.getStockBatchAdminSessionForInventory not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk stock 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StockBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.StockBatchAdminSession getStockBatchAdminSessionForInventory(org.osid.id.Id inventoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchProxyManager.getStockBatchAdminSessionForInventory not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk model 
     *  administration service. 
     *
     *  @return an <code> ModelBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ModelBatchAdminSession getModelBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchManager.getModelBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk model 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ModelBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ModelBatchAdminSession getModelBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchProxyManager.getModelBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk model 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @return an <code> ModelBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ModelBatchAdminSession getModelBatchAdminSessionForInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchManager.getModelBatchAdminSessionForInventory not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk model 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ModelBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ModelBatchAdminSession getModelBatchAdminSessionForInventory(org.osid.id.Id inventoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchProxyManager.getModelBatchAdminSessionForInventory not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inventory 
     *  administration service. 
     *
     *  @return an <code> InventoryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.InventoryBatchAdminSession getInventoryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchManager.getInventoryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inventory 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InventoryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.InventoryBatchAdminSession getInventoryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.inventory.batch.InventoryBatchProxyManager.getInventoryBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
