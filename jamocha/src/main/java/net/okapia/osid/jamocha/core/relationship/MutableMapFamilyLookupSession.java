//
// MutableMapFamilyLookupSession
//
//    Implements a Family lookup service backed by a collection of
//    families that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship;


/**
 *  Implements a Family lookup service backed by a collection of
 *  families. The families are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of families can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapFamilyLookupSession
    extends net.okapia.osid.jamocha.core.relationship.spi.AbstractMapFamilyLookupSession
    implements org.osid.relationship.FamilyLookupSession {


    /**
     *  Constructs a new {@code MutableMapFamilyLookupSession}
     *  with no families.
     */

    public MutableMapFamilyLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFamilyLookupSession} with a
     *  single family.
     *  
     *  @param family a family
     *  @throws org.osid.NullArgumentException {@code family}
     *          is {@code null}
     */

    public MutableMapFamilyLookupSession(org.osid.relationship.Family family) {
        putFamily(family);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFamilyLookupSession}
     *  using an array of families.
     *
     *  @param families an array of families
     *  @throws org.osid.NullArgumentException {@code families}
     *          is {@code null}
     */

    public MutableMapFamilyLookupSession(org.osid.relationship.Family[] families) {
        putFamilies(families);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFamilyLookupSession}
     *  using a collection of families.
     *
     *  @param families a collection of families
     *  @throws org.osid.NullArgumentException {@code families}
     *          is {@code null}
     */

    public MutableMapFamilyLookupSession(java.util.Collection<? extends org.osid.relationship.Family> families) {
        putFamilies(families);
        return;
    }

    
    /**
     *  Makes a {@code Family} available in this session.
     *
     *  @param family a family
     *  @throws org.osid.NullArgumentException {@code family{@code  is
     *          {@code null}
     */

    @Override
    public void putFamily(org.osid.relationship.Family family) {
        super.putFamily(family);
        return;
    }


    /**
     *  Makes an array of families available in this session.
     *
     *  @param families an array of families
     *  @throws org.osid.NullArgumentException {@code families{@code 
     *          is {@code null}
     */

    @Override
    public void putFamilies(org.osid.relationship.Family[] families) {
        super.putFamilies(families);
        return;
    }


    /**
     *  Makes collection of families available in this session.
     *
     *  @param families a collection of families
     *  @throws org.osid.NullArgumentException {@code families{@code  is
     *          {@code null}
     */

    @Override
    public void putFamilies(java.util.Collection<? extends org.osid.relationship.Family> families) {
        super.putFamilies(families);
        return;
    }


    /**
     *  Removes a Family from this session.
     *
     *  @param familyId the {@code Id} of the family
     *  @throws org.osid.NullArgumentException {@code familyId{@code 
     *          is {@code null}
     */

    @Override
    public void removeFamily(org.osid.id.Id familyId) {
        super.removeFamily(familyId);
        return;
    }    
}
