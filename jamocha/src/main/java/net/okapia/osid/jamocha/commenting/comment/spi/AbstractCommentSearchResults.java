//
// AbstractCommentSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCommentSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.commenting.CommentSearchResults {

    private org.osid.commenting.CommentList comments;
    private final org.osid.commenting.CommentQueryInspector inspector;
    private final java.util.Collection<org.osid.commenting.records.CommentSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCommentSearchResults.
     *
     *  @param comments the result set
     *  @param commentQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>comments</code>
     *          or <code>commentQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCommentSearchResults(org.osid.commenting.CommentList comments,
                                            org.osid.commenting.CommentQueryInspector commentQueryInspector) {
        nullarg(comments, "comments");
        nullarg(commentQueryInspector, "comment query inspectpr");

        this.comments = comments;
        this.inspector = commentQueryInspector;

        return;
    }


    /**
     *  Gets the comment list resulting from a search.
     *
     *  @return a comment list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.commenting.CommentList getComments() {
        if (this.comments == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.commenting.CommentList comments = this.comments;
        this.comments = null;
	return (comments);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.commenting.CommentQueryInspector getCommentQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  comment search record <code> Type. </code> This method must
     *  be used to retrieve a comment implementing the requested
     *  record.
     *
     *  @param commentSearchRecordType a comment search 
     *         record type 
     *  @return the comment search
     *  @throws org.osid.NullArgumentException
     *          <code>commentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(commentSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentSearchResultsRecord getCommentSearchResultsRecord(org.osid.type.Type commentSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.commenting.records.CommentSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(commentSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(commentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record comment search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCommentRecord(org.osid.commenting.records.CommentSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "comment record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
