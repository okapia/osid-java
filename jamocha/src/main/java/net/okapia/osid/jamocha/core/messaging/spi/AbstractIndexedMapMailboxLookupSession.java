//
// AbstractIndexedMapMailboxLookupSession.java
//
//    A simple framework for providing a Mailbox lookup service
//    backed by a fixed collection of mailboxes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Mailbox lookup service backed by a
 *  fixed collection of mailboxes. The mailboxes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some mailboxes may be compatible
 *  with more types than are indicated through these mailbox
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Mailboxes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapMailboxLookupSession
    extends AbstractMapMailboxLookupSession
    implements org.osid.messaging.MailboxLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.messaging.Mailbox> mailboxesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.messaging.Mailbox>());
    private final MultiMap<org.osid.type.Type, org.osid.messaging.Mailbox> mailboxesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.messaging.Mailbox>());


    /**
     *  Makes a <code>Mailbox</code> available in this session.
     *
     *  @param  mailbox a mailbox
     *  @throws org.osid.NullArgumentException <code>mailbox<code> is
     *          <code>null</code>
     */

    @Override
    protected void putMailbox(org.osid.messaging.Mailbox mailbox) {
        super.putMailbox(mailbox);

        this.mailboxesByGenus.put(mailbox.getGenusType(), mailbox);
        
        try (org.osid.type.TypeList types = mailbox.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.mailboxesByRecord.put(types.getNextType(), mailbox);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a mailbox from this session.
     *
     *  @param mailboxId the <code>Id</code> of the mailbox
     *  @throws org.osid.NullArgumentException <code>mailboxId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeMailbox(org.osid.id.Id mailboxId) {
        org.osid.messaging.Mailbox mailbox;
        try {
            mailbox = getMailbox(mailboxId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.mailboxesByGenus.remove(mailbox.getGenusType());

        try (org.osid.type.TypeList types = mailbox.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.mailboxesByRecord.remove(types.getNextType(), mailbox);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeMailbox(mailboxId);
        return;
    }


    /**
     *  Gets a <code>MailboxList</code> corresponding to the given
     *  mailbox genus <code>Type</code> which does not include
     *  mailboxes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known mailboxes or an error results. Otherwise,
     *  the returned list may contain only those mailboxes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  mailboxGenusType a mailbox genus type 
     *  @return the returned <code>Mailbox</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByGenusType(org.osid.type.Type mailboxGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.mailbox.ArrayMailboxList(this.mailboxesByGenus.get(mailboxGenusType)));
    }


    /**
     *  Gets a <code>MailboxList</code> containing the given
     *  mailbox record <code>Type</code>. In plenary mode, the
     *  returned list contains all known mailboxes or an error
     *  results. Otherwise, the returned list may contain only those
     *  mailboxes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  mailboxRecordType a mailbox record type 
     *  @return the returned <code>mailbox</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByRecordType(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.mailbox.ArrayMailboxList(this.mailboxesByRecord.get(mailboxRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.mailboxesByGenus.clear();
        this.mailboxesByRecord.clear();

        super.close();

        return;
    }
}
