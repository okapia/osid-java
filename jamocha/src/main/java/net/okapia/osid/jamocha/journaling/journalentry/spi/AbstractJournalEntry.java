//
// AbstractJournalEntry.java
//
//     Defines a JournalEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journalentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>JournalEntry</code>.
 */

public abstract class AbstractJournalEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.journaling.JournalEntry {

    private org.osid.journaling.Branch branch;
    private org.osid.id.Id sourceId;
    private org.osid.id.Id versionId;
    private org.osid.calendaring.DateTime timestamp;
    private org.osid.resource.Resource resource;
    private org.osid.authentication.Agent agent;

    private final java.util.Collection<org.osid.journaling.records.JournalEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the branch <code> Id </code> for this entry. 
     *
     *  @return the branch <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBranchId() {
        return (this.branch.getId());
    }


    /**
     *  Gets the branch for this entry. 
     *
     *  @return the branch 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.journaling.Branch getBranch()
        throws org.osid.OperationFailedException {

        return (this.branch);
    }


    /**
     *  Sets the branch.
     *
     *  @param branch a branch
     *  @throws org.osid.NullArgumentException
     *          <code>branch</code> is <code>null</code>
     */

    protected void setBranch(org.osid.journaling.Branch branch) {
        nullarg(branch, "branch");
        this.branch = branch;
        return;
    }


    /**
     *  Gets the principal <code> Id </code> of the journaled object. 
     *
     *  @return the source <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.sourceId);
    }


    /**
     *  Sets the source id.
     *
     *  @param sourceId a source id
     *  @throws org.osid.NullArgumentException
     *          <code>sourceId</code> is <code>null</code>
     */

    protected void setSourceId(org.osid.id.Id sourceId) {
        nullarg(sourceId, "source Id");
        this.sourceId = sourceId;
        return;
    }


    /**
     *  Gets the version <code> Id </code> of the journaled object. 
     *
     *  @return the version <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVersionId() {
        return (this.versionId);
    }


    /**
     *  Sets the version id.
     *
     *  @param versionId a version id
     *  @throws org.osid.NullArgumentException
     *          <code>versionId</code> is <code>null</code>
     */

    protected void setVersionId(org.osid.id.Id versionId) {
        nullarg(versionId, "version Id");
        this.versionId = versionId;
        return;
    }


    /**
     *  Gets the <code> timestamp </code> of this journal entry. 
     *
     *  @return the time of this entry 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.timestamp);
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException
     *          <code>timestamp</code> is <code>null</code>
     */

    protected void setTimestamp(org.osid.calendaring.DateTime timestamp) {
        nullarg(timestamp, "timestamp");
        this.timestamp = timestamp;
        return;
    }


    /**
     *  Gets the Id of the resource who created this entry. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource who created this entry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the Id of the agent who created this entry. 
     *
     *  @return the <code> Agent </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.agent.getId());
    }


    /**
     *  Gets the agent who created this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Tests if this journalEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  journalEntryRecordType a journal entry record type 
     *  @return <code>true</code> if the journalEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type journalEntryRecordType) {
        for (org.osid.journaling.records.JournalEntryRecord record : this.records) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>JournalEntry</code> record <code>Type</code>.
     *
     *  @param  journalEntryRecordType the journal entry record type 
     *  @return the journal entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntryRecord getJournalEntryRecord(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalEntryRecord record : this.records) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this journal entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param journalEntryRecord the journal entry record
     *  @param journalEntryRecordType journal entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecord</code> or
     *          <code>journalEntryRecordTypejournalEntry</code> is
     *          <code>null</code>
     */
            
    protected void addJournalEntryRecord(org.osid.journaling.records.JournalEntryRecord journalEntryRecord, 
                                         org.osid.type.Type journalEntryRecordType) {

        nullarg(journalEntryRecord, "journal entry record");
        addRecordType(journalEntryRecordType);
        this.records.add(journalEntryRecord);
        
        return;
    }
}
