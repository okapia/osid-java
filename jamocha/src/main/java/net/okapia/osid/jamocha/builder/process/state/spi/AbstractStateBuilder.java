//
// AbstractState.java
//
//     Defines a State builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.process.state.spi;


/**
 *  Defines a <code>State</code> builder.
 */

public abstract class AbstractStateBuilder<T extends AbstractStateBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.process.state.StateMiter state;


    /**
     *  Constructs a new <code>AbstractStateBuilder</code>.
     *
     *  @param state the state to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractStateBuilder(net.okapia.osid.jamocha.builder.process.state.StateMiter state) {
        super(state);
        this.state = state;
        return;
    }


    /**
     *  Builds the state.
     *
     *  @return the new state
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.process.State build() {
        (new net.okapia.osid.jamocha.builder.validator.process.state.StateValidator(getValidations())).validate(this.state);
        return (new net.okapia.osid.jamocha.builder.process.state.ImmutableState(this.state));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the state miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.process.state.StateMiter getMiter() {
        return (this.state);
    }


    /**
     *  Adds a State record.
     *
     *  @param record a state record
     *  @param recordType the type of state record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.process.records.StateRecord record, org.osid.type.Type recordType) {
        getMiter().addStateRecord(record, recordType);
        return (self());
    }
}       


