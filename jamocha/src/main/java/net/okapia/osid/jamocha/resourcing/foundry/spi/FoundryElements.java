//
// FoundryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.foundry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class FoundryElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the FoundryElement Id.
     *
     *  @return the foundry element Id
     */

    public static org.osid.id.Id getFoundryEntityId() {
        return (makeEntityId("osid.resourcing.Foundry"));
    }


    /**
     *  Gets the JobId element Id.
     *
     *  @return the JobId element Id
     */

    public static org.osid.id.Id getJobId() {
        return (makeQueryElementId("osid.resourcing.foundry.JobId"));
    }


    /**
     *  Gets the Job element Id.
     *
     *  @return the Job element Id
     */

    public static org.osid.id.Id getJob() {
        return (makeQueryElementId("osid.resourcing.foundry.Job"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeQueryElementId("osid.resourcing.foundry.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeQueryElementId("osid.resourcing.foundry.Work"));
    }


    /**
     *  Gets the AvailabilityId element Id.
     *
     *  @return the AvailabilityId element Id
     */

    public static org.osid.id.Id getAvailabilityId() {
        return (makeQueryElementId("osid.resourcing.foundry.AvailabilityId"));
    }


    /**
     *  Gets the Availability element Id.
     *
     *  @return the Availability element Id
     */

    public static org.osid.id.Id getAvailability() {
        return (makeQueryElementId("osid.resourcing.foundry.Availability"));
    }


    /**
     *  Gets the CommissionId element Id.
     *
     *  @return the CommissionId element Id
     */

    public static org.osid.id.Id getCommissionId() {
        return (makeQueryElementId("osid.resourcing.foundry.CommissionId"));
    }


    /**
     *  Gets the Commission element Id.
     *
     *  @return the Commission element Id
     */

    public static org.osid.id.Id getCommission() {
        return (makeQueryElementId("osid.resourcing.foundry.Commission"));
    }


    /**
     *  Gets the EffortId element Id.
     *
     *  @return the EffortId element Id
     */

    public static org.osid.id.Id getEffortId() {
        return (makeQueryElementId("osid.resourcing.foundry.EffortId"));
    }


    /**
     *  Gets the Effort element Id.
     *
     *  @return the Effort element Id
     */

    public static org.osid.id.Id getEffort() {
        return (makeQueryElementId("osid.resourcing.foundry.Effort"));
    }


    /**
     *  Gets the AncestorFoundryId element Id.
     *
     *  @return the AncestorFoundryId element Id
     */

    public static org.osid.id.Id getAncestorFoundryId() {
        return (makeQueryElementId("osid.resourcing.foundry.AncestorFoundryId"));
    }


    /**
     *  Gets the AncestorFoundry element Id.
     *
     *  @return the AncestorFoundry element Id
     */

    public static org.osid.id.Id getAncestorFoundry() {
        return (makeQueryElementId("osid.resourcing.foundry.AncestorFoundry"));
    }


    /**
     *  Gets the DescendantFoundryId element Id.
     *
     *  @return the DescendantFoundryId element Id
     */

    public static org.osid.id.Id getDescendantFoundryId() {
        return (makeQueryElementId("osid.resourcing.foundry.DescendantFoundryId"));
    }


    /**
     *  Gets the DescendantFoundry element Id.
     *
     *  @return the DescendantFoundry element Id
     */

    public static org.osid.id.Id getDescendantFoundry() {
        return (makeQueryElementId("osid.resourcing.foundry.DescendantFoundry"));
    }
}
