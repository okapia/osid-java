//
// AbstractInstallationQueryInspector.java
//
//     A template for making an InstallationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for installations.
 */

public abstract class AbstractInstallationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.installation.InstallationQueryInspector {

    private final java.util.Collection<org.osid.installation.records.InstallationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the site <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSiteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the site query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.SiteQueryInspector[] getSiteTerms() {
        return (new org.osid.installation.SiteQueryInspector[0]);
    }


    /**
     *  Gets the package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPackageIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getPackageTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the install date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getInstallDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the check date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getLastCheckDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }



    /**
     *  Gets the record corresponding to the given installation query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an installation implementing the requested record.
     *
     *  @param installationRecordType an installation record type
     *  @return the installation query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationQueryInspectorRecord getInstallationQueryInspectorRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(installationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this installation query. 
     *
     *  @param installationQueryInspectorRecord installation query inspector
     *         record
     *  @param installationRecordType installation record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstallationQueryInspectorRecord(org.osid.installation.records.InstallationQueryInspectorRecord installationQueryInspectorRecord, 
                                                   org.osid.type.Type installationRecordType) {

        addRecordType(installationRecordType);
        nullarg(installationRecordType, "installation record type");
        this.records.add(installationQueryInspectorRecord);        
        return;
    }
}
