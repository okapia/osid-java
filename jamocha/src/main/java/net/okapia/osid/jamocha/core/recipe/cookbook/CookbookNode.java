//
// CookbookNode.java
//
//     Defines a Cookbook node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  A class for managing a hierarchy of cookbook nodes in core.
 */

public final class CookbookNode
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractCookbookNode
    implements org.osid.recipe.CookbookNode {


    /**
     *  Constructs a new <code>CookbookNode</code> from a single
     *  cookbook.
     *
     *  @param cookbook the cookbook
     *  @throws org.osid.NullArgumentException <code>cookbook</code> is 
     *          <code>null</code>.
     */

    public CookbookNode(org.osid.recipe.Cookbook cookbook) {
        super(cookbook);
        return;
    }


    /**
     *  Constructs a new <code>CookbookNode</code>.
     *
     *  @param cookbook the cookbook
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>cookbook</code>
     *          is <code>null</code>.
     */

    public CookbookNode(org.osid.recipe.Cookbook cookbook, boolean root, boolean leaf) {
        super(cookbook, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this cookbook.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.recipe.CookbookNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this cookbook.
     *
     *  @param cookbook the cookbook to add as a parent
     *  @throws org.osid.NullArgumentException <code>cookbook</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.recipe.Cookbook cookbook) {
        addParent(new CookbookNode(cookbook));
        return;
    }


    /**
     *  Adds a child to this cookbook.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.recipe.CookbookNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this cookbook.
     *
     *  @param cookbook the cookbook to add as a child
     *  @throws org.osid.NullArgumentException <code>cookbook</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.recipe.Cookbook cookbook) {
        addChild(new CookbookNode(cookbook));
        return;
    }
}
