//
// AbstractAssemblyMessageQuery.java
//
//     A MessageQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.messaging.message.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A MessageQuery that stores terms.
 */

public abstract class AbstractAssemblyMessageQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.messaging.MessageQuery,
               org.osid.messaging.MessageQueryInspector,
               org.osid.messaging.MessageSearchOrder {

    private final java.util.Collection<org.osid.messaging.records.MessageQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.messaging.records.MessageQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.messaging.records.MessageSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyMessageQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyMessageQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a subject line to match. Multiple subject line matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subject subject to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> subject is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> subject </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSubjectLine(String subject, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getSubjectLineColumn(), subject, stringMatchType, match);
        return;
    }


    /**
     *  Matches messages with any subject line. 
     *
     *  @param  match <code> true </code> to match messages with any subject 
     *          line, <code> false </code> to match messages with no subject 
     *          line 
     */

    @OSID @Override
    public void matchAnySubjectLine(boolean match) {
        getAssembler().addStringWildcardTerm(getSubjectLineColumn(), match);
        return;
    }


    /**
     *  Clears the subject line terms. 
     */

    @OSID @Override
    public void clearSubjectLineTerms() {
        getAssembler().clearTerms(getSubjectLineColumn());
        return;
    }


    /**
     *  Gets the subject line query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSubjectLineTerms() {
        return (getAssembler().getStringTerms(getSubjectLineColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by subject line. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubjectLine(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubjectLineColumn(), style);
        return;
    }


    /**
     *  Gets the SubjectLine column name.
     *
     * @return the column name
     */

    protected String getSubjectLineColumn() {
        return ("subject_line");
    }


    /**
     *  Adds text to match. Multiple subject line matches can be added to 
     *  perform a boolean <code> OR </code> among them. 
     *
     *  @param  text dtext to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getTextColumn(), text, stringMatchType, match);
        return;
    }


    /**
     *  Matches messages with any text. 
     *
     *  @param  match <code> true </code> to match messages with any text, 
     *          <code> false </code> to match messages with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        getAssembler().addStringWildcardTerm(getTextColumn(), match);
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        getAssembler().clearTerms(getTextColumn());
        return;
    }


    /**
     *  Gets the text query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (getAssembler().getStringTerms(getTextColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTextColumn(), style);
        return;
    }


    /**
     *  Gets the Text column name.
     *
     * @return the column name
     */

    protected String getTextColumn() {
        return ("text");
    }


    /**
     *  Matches messages that have been sent. 
     *
     *  @param  match <code> true </code> to match sent messages, <code> false 
     *          </code> to match unsent messages 
     */

    @OSID @Override
    public void matchSent(boolean match) {
        getAssembler().addBooleanTerm(getSentColumn(), match);
        return;
    }


    /**
     *  Clears the sent terms. 
     */

    @OSID @Override
    public void clearSentTerms() {
        getAssembler().clearTerms(getSentColumn());
        return;
    }


    /**
     *  Gets the sent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSentTerms() {
        return (getAssembler().getBooleanTerms(getSentColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by sent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSentColumn(), style);
        return;
    }


    /**
     *  Gets the Sent column name.
     *
     * @return the column name
     */

    protected String getSentColumn() {
        return ("sent");
    }


    /**
     *  Matches messages whose sent time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSentTime(org.osid.calendaring.DateTime startTime, 
                              org.osid.calendaring.DateTime endTime, 
                              boolean match) {
        getAssembler().addDateTimeRangeTerm(getSentTimeColumn(), startTime, endTime, match);
        return;
    }


    /**
     *  Clears the sent time terms. 
     */

    @OSID @Override
    public void clearSentTimeTerms() {
        getAssembler().clearTerms(getSentTimeColumn());
        return;
    }


    /**
     *  Gets the sent time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getSentTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getSentTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by the sent time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySentTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSentTimeColumn(), style);
        return;
    }


    /**
     *  Gets the SentTime column name.
     *
     * @return the column name
     */

    protected String getSentTimeColumn() {
        return ("sent_time");
    }


    /**
     *  Matches the sender of the message. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSenderId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSenderIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the sender <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSenderIdTerms() {
        getAssembler().clearTerms(getSenderIdColumn());
        return;
    }


    /**
     *  Gets the sender <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSenderIdTerms() {
        return (getAssembler().getIdTerms(getSenderIdColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by sender. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySender(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSenderColumn(), style);
        return;
    }


    /**
     *  Gets the SenderId column name.
     *
     * @return the column name
     */

    protected String getSenderIdColumn() {
        return ("sender_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  senders. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSenderQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSenderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSenderQuery() {
        throw new org.osid.UnimplementedException("supportsSenderQuery() is false");
    }


    /**
     *  Clears the sender terms. 
     */

    @OSID @Override
    public void clearSenderTerms() {
        getAssembler().clearTerms(getSenderColumn());
        return;
    }


    /**
     *  Gets the sender query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSenderTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSenderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSenderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSenderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSenderSearchOrder() is false");
    }


    /**
     *  Gets the Sender column name.
     *
     * @return the column name
     */

    protected String getSenderColumn() {
        return ("sender");
    }


    /**
     *  Matches the sending agent of the message. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSendingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getSendingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the sending agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSendingAgentIdTerms() {
        getAssembler().clearTerms(getSendingAgentIdColumn());
        return;
    }


    /**
     *  Gets the sending agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSendingAgentIdTerms() {
        return (getAssembler().getIdTerms(getSendingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by sending agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySendingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSendingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the SendingAgentId column name.
     *
     * @return the column name
     */

    protected String getSendingAgentIdColumn() {
        return ("sending_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  senders. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSendingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSendingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getSendingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsSendingAgentQuery() is false");
    }


    /**
     *  Clears the sending agent terms. 
     */

    @OSID @Override
    public void clearSendingAgentTerms() {
        getAssembler().clearTerms(getSendingAgentColumn());
        return;
    }


    /**
     *  Gets the sending agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getSendingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSendingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the sending agent order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSendingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getSendingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSendingAgentSearchOrder() is false");
    }


    /**
     *  Gets the SendingAgent column name.
     *
     * @return the column name
     */

    protected String getSendingAgentColumn() {
        return ("sending_agent");
    }


    /**
     *  Matches messages whose received time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReceivedTime(org.osid.calendaring.DateTime startTime, 
                                  org.osid.calendaring.DateTime endTime, 
                                  boolean match) {
        getAssembler().addDateTimeRangeTerm(getReceivedTimeColumn(), startTime, endTime, match);
        return;
    }


    /**
     *  Clears the received time terms. 
     */

    @OSID @Override
    public void clearReceivedTimeTerms() {
        getAssembler().clearTerms(getReceivedTimeColumn());
        return;
    }


    /**
     *  Gets the received time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReceivedTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getReceivedTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by the sent time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReceivedTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReceivedTimeColumn(), style);
        return;
    }


    /**
     *  Gets the ReceivedTime column name.
     *
     * @return the column name
     */

    protected String getReceivedTimeColumn() {
        return ("received_time");
    }


    /**
     *  Matches messages whose delivery duration is between the supplied range 
     *  inclusive. 
     *
     *  @param  start start time 
     *  @param  end end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDeliveryTime(org.osid.calendaring.Duration start, 
                                  org.osid.calendaring.Duration end, 
                                  boolean match) {
        getAssembler().addDurationRangeTerm(getDeliveryTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the delivery time terms. 
     */

    @OSID @Override
    public void clearDeliveryTimeTerms() {
        getAssembler().clearTerms(getDeliveryTimeColumn());
        return;
    }


    /**
     *  Gets the delivery time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getDeliveryTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getDeliveryTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by the delivery time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDeliveryTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDeliveryTimeColumn(), style);
        return;
    }


    /**
     *  Gets the DeliveryTime column name.
     *
     * @return the column name
     */

    protected String getDeliveryTimeColumn() {
        return ("delivery_time");
    }


    /**
     *  Matches any recipient of the message. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getRecipientIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the recipient <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        getAssembler().clearTerms(getRecipientIdColumn());
        return;
    }


    /**
     *  Gets the recipient <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipientIdTerms() {
        return (getAssembler().getIdTerms(getRecipientIdColumn()));
    }


    /**
     *  Gets the RecipientId column name.
     *
     * @return the column name
     */

    protected String getRecipientIdColumn() {
        return ("recipient_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  recipients. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipient resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the recipient terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        getAssembler().clearTerms(getRecipientColumn());
        return;
    }


    /**
     *  Gets the recipient query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRecipientTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Recipient column name.
     *
     * @return the column name
     */

    protected String getRecipientColumn() {
        return ("recipient");
    }


    /**
     *  Matches the receipt of the message. 
     *
     *  @param  receiptId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> receiptId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReceiptId(org.osid.id.Id receiptId, boolean match) {
        getAssembler().addIdTerm(getReceiptIdColumn(), receiptId, match);
        return;
    }


    /**
     *  Clears the receipt <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReceiptIdTerms() {
        getAssembler().clearTerms(getReceiptIdColumn());
        return;
    }


    /**
     *  Gets the receipt <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReceiptIdTerms() {
        return (getAssembler().getIdTerms(getReceiptIdColumn()));
    }


    /**
     *  Specifies a preference for ordering messages by receipt. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReceipt(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReceiptColumn(), style);
        return;
    }


    /**
     *  Gets the ReceiptId column name.
     *
     * @return the column name
     */

    protected String getReceiptIdColumn() {
        return ("receipt_id");
    }


    /**
     *  Tests if a <code> ReceiptQuery </code> is available for querying 
     *  receipts. 
     *
     *  @return <code> true </code> if a receipt query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptQuery() {
        return (false);
    }


    /**
     *  Gets the query for the receipt. 
     *
     *  @return the receipt query 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptQuery getReceiptQuery() {
        throw new org.osid.UnimplementedException("supportsReceiptQuery() is false");
    }


    /**
     *  Matches any received messages. 
     *
     *  @param  match <code> true </code> to match any received messages, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyReceipt(boolean match) {
        getAssembler().addIdWildcardTerm(getReceiptColumn(), match);
        return;
    }


    /**
     *  Clears the receipt terms. 
     */

    @OSID @Override
    public void clearReceiptTerms() {
        getAssembler().clearTerms(getReceiptColumn());
        return;
    }


    /**
     *  Gets the receipt query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptQueryInspector[] getReceiptTerms() {
        return (new org.osid.messaging.ReceiptQueryInspector[0]);
    }


    /**
     *  Tests if a receipt order is available. 
     *
     *  @return <code> true </code> if a receipt order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptSearchOrder() {
        return (false);
    }


    /**
     *  Gets the receipt order. 
     *
     *  @return the receipt search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptSearchOrder getReceiptSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReceiptSearchOrder() is false");
    }


    /**
     *  Gets the Receipt column name.
     *
     * @return the column name
     */

    protected String getReceiptColumn() {
        return ("receipt");
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query. 
     *
     *  @param  mailboxId the mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMailboxId(org.osid.id.Id mailboxId, boolean match) {
        getAssembler().addIdTerm(getMailboxIdColumn(), mailboxId, match);
        return;
    }


    /**
     *  Clears the mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMailboxIdTerms() {
        getAssembler().clearTerms(getMailboxIdColumn());
        return;
    }


    /**
     *  Gets the mailbox <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMailboxIdTerms() {
        return (getAssembler().getIdTerms(getMailboxIdColumn()));
    }


    /**
     *  Gets the MailboxId column name.
     *
     * @return the column name
     */

    protected String getMailboxIdColumn() {
        return ("mailbox_id");
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsMailboxQuery() is false");
    }


    /**
     *  Clears the mailbox terms. 
     */

    @OSID @Override
    public void clearMailboxTerms() {
        getAssembler().clearTerms(getMailboxColumn());
        return;
    }


    /**
     *  Gets the mailbox query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQueryInspector[] getMailboxTerms() {
        return (new org.osid.messaging.MailboxQueryInspector[0]);
    }


    /**
     *  Gets the Mailbox column name.
     *
     * @return the column name
     */

    protected String getMailboxColumn() {
        return ("mailbox");
    }


    /**
     *  Tests if this message supports the given record
     *  <code>Type</code>.
     *
     *  @param  messageRecordType a message record type 
     *  @return <code>true</code> if the messageRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type messageRecordType) {
        for (org.osid.messaging.records.MessageQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(messageRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  messageRecordType the message record type 
     *  @return the message query record 
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(messageRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MessageQueryRecord getMessageQueryRecord(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MessageQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(messageRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(messageRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  messageRecordType the message record type 
     *  @return the message query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(messageRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MessageQueryInspectorRecord getMessageQueryInspectorRecord(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MessageQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(messageRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(messageRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param messageRecordType the message record type
     *  @return the message search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(messageRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MessageSearchOrderRecord getMessageSearchOrderRecord(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MessageSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(messageRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(messageRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this message. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param messageQueryRecord the message query record
     *  @param messageQueryInspectorRecord the message query inspector
     *         record
     *  @param messageSearchOrderRecord the message search order record
     *  @param messageRecordType message record type
     *  @throws org.osid.NullArgumentException
     *          <code>messageQueryRecord</code>,
     *          <code>messageQueryInspectorRecord</code>,
     *          <code>messageSearchOrderRecord</code> or
     *          <code>messageRecordTypemessage</code> is
     *          <code>null</code>
     */
            
    protected void addMessageRecords(org.osid.messaging.records.MessageQueryRecord messageQueryRecord, 
                                      org.osid.messaging.records.MessageQueryInspectorRecord messageQueryInspectorRecord, 
                                      org.osid.messaging.records.MessageSearchOrderRecord messageSearchOrderRecord, 
                                      org.osid.type.Type messageRecordType) {

        addRecordType(messageRecordType);

        nullarg(messageQueryRecord, "message query record");
        nullarg(messageQueryInspectorRecord, "message query inspector record");
        nullarg(messageSearchOrderRecord, "message search odrer record");

        this.queryRecords.add(messageQueryRecord);
        this.queryInspectorRecords.add(messageQueryInspectorRecord);
        this.searchOrderRecords.add(messageSearchOrderRecord);
        
        return;
    }
}
