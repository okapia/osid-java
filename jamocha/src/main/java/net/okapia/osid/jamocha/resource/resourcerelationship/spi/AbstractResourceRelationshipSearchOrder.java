//
// AbstractResourceRelationshipSearchOdrer.java
//
//     Defines a ResourceRelationshipSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resourcerelationship.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ResourceRelationshipSearchOrder}.
 */

public abstract class AbstractResourceRelationshipSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.resource.ResourceRelationshipSearchOrder {

    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySourceResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceResourceSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSourceResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSourceResourceSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDestinationResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a peer resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationResourceSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getDestinationResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDestinationResourceSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  resourceRelationshipRecordType a resource relationship record type 
     *  @return {@code true} if the resourceRelationshipRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code resourceRelationshipRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceRelationshipRecordType) {
        for (org.osid.resource.records.ResourceRelationshipSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  resourceRelationshipRecordType the resource relationship record type 
     *  @return the resource relationship search order record
     *  @throws org.osid.NullArgumentException
     *          {@code resourceRelationshipRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(resourceRelationshipRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipSearchOrderRecord getResourceRelationshipSearchOrderRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRelationshipSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRelationshipRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this resource relationship. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceRelationshipRecord the resource relationship search odrer record
     *  @param resourceRelationshipRecordType resource relationship record type
     *  @throws org.osid.NullArgumentException
     *          {@code resourceRelationshipRecord} or
     *          {@code resourceRelationshipRecordTyperesourceRelationship} is
     *          {@code null}
     */
            
    protected void addResourceRelationshipRecord(org.osid.resource.records.ResourceRelationshipSearchOrderRecord resourceRelationshipSearchOrderRecord, 
                                     org.osid.type.Type resourceRelationshipRecordType) {

        addRecordType(resourceRelationshipRecordType);
        this.records.add(resourceRelationshipSearchOrderRecord);
        
        return;
    }
}
