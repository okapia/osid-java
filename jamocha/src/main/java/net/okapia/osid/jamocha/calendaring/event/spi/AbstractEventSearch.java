//
// AbstractEventSearch.java
//
//     A template for making an Event Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.event.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing event searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractEventSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.EventSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.EventSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.EventSearchOrder eventSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of events. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  eventIds list of events
     *  @throws org.osid.NullArgumentException
     *          <code>eventIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongEvents(org.osid.id.IdList eventIds) {
        while (eventIds.hasNext()) {
            try {
                this.ids.add(eventIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongEvents</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of event Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getEventIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  eventSearchOrder event search order 
     *  @throws org.osid.NullArgumentException
     *          <code>eventSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>eventSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderEventResults(org.osid.calendaring.EventSearchOrder eventSearchOrder) {
	this.eventSearchOrder = eventSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.EventSearchOrder getEventSearchOrder() {
	return (this.eventSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given event search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an event implementing the requested record.
     *
     *  @param eventSearchRecordType an event search record
     *         type
     *  @return the event search record
     *  @throws org.osid.NullArgumentException
     *          <code>eventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(eventSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.EventSearchRecord getEventSearchRecord(org.osid.type.Type eventSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.EventSearchRecord record : this.records) {
            if (record.implementsRecordType(eventSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(eventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this event search. 
     *
     *  @param eventSearchRecord event search record
     *  @param eventSearchRecordType event search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEventSearchRecord(org.osid.calendaring.records.EventSearchRecord eventSearchRecord, 
                                           org.osid.type.Type eventSearchRecordType) {

        addRecordType(eventSearchRecordType);
        this.records.add(eventSearchRecord);        
        return;
    }
}
