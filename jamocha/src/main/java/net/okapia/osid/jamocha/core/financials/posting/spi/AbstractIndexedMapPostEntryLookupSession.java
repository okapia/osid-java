//
// AbstractIndexedMapPostEntryLookupSession.java
//
//    A simple framework for providing a PostEntry lookup service
//    backed by a fixed collection of post entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a PostEntry lookup service backed by a
 *  fixed collection of post entries. The post entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some post entries may be compatible
 *  with more types than are indicated through these post entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PostEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPostEntryLookupSession
    extends AbstractMapPostEntryLookupSession
    implements org.osid.financials.posting.PostEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.financials.posting.PostEntry> postEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.posting.PostEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.financials.posting.PostEntry> postEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.posting.PostEntry>());


    /**
     *  Makes a <code>PostEntry</code> available in this session.
     *
     *  @param  postEntry a post entry
     *  @throws org.osid.NullArgumentException <code>postEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPostEntry(org.osid.financials.posting.PostEntry postEntry) {
        super.putPostEntry(postEntry);

        this.postEntriesByGenus.put(postEntry.getGenusType(), postEntry);
        
        try (org.osid.type.TypeList types = postEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.postEntriesByRecord.put(types.getNextType(), postEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a post entry from this session.
     *
     *  @param postEntryId the <code>Id</code> of the post entry
     *  @throws org.osid.NullArgumentException <code>postEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePostEntry(org.osid.id.Id postEntryId) {
        org.osid.financials.posting.PostEntry postEntry;
        try {
            postEntry = getPostEntry(postEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.postEntriesByGenus.remove(postEntry.getGenusType());

        try (org.osid.type.TypeList types = postEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.postEntriesByRecord.remove(types.getNextType(), postEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePostEntry(postEntryId);
        return;
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  post entry genus <code>Type</code> which does not include
     *  post entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known post entries or an error results. Otherwise,
     *  the returned list may contain only those post entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  postEntryGenusType a post entry genus type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.posting.postentry.ArrayPostEntryList(this.postEntriesByGenus.get(postEntryGenusType)));
    }


    /**
     *  Gets a <code>PostEntryList</code> containing the given
     *  post entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known post entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  post entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  postEntryRecordType a post entry record type 
     *  @return the returned <code>postEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByRecordType(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.posting.postentry.ArrayPostEntryList(this.postEntriesByRecord.get(postEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.postEntriesByGenus.clear();
        this.postEntriesByRecord.clear();

        super.close();

        return;
    }
}
