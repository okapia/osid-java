//
// AbstractImmutableAssessment.java
//
//     Wraps a mutable Assessment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Assessment</code> to hide modifiers. This
 *  wrapper provides an immutized Assessment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assessment whose state changes are visible.
 */

public abstract class AbstractImmutableAssessment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.assessment.Assessment {

    private final org.osid.assessment.Assessment assessment;


    /**
     *  Constructs a new <code>AbstractImmutableAssessment</code>.
     *
     *  @param assessment the assessment to immutablize
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssessment(org.osid.assessment.Assessment assessment) {
        super(assessment);
        this.assessment = assessment;
        return;
    }


    /**
     *  Gets the <code> Id </code> of a <code> Grade </code> corresponding to 
     *  the assessment difficulty. 
     *
     *  @return a grade <code> id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        return (this.assessment.getLevelId());
    }


    /**
     *  Gets the <code> Grade </code> corresponding to the assessment 
     *  difficulty. 
     *
     *  @return the level 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        return (this.assessment.getLevel());
    }


    /**
     *  Tests if a rubric assessment is associated with this assessment. 
     *
     *  @return <code> true </code> if a rubric is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasRubric() {
        return (this.assessment.hasRubric());
    }


    /**
     *  Gets the <code> Id </code> of the rubric. 
     *
     *  @return an assessment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRubricId() {
        return (this.assessment.getRubricId());
    }


    /**
     *  Gets the rubric. 
     *
     *  @return the assessment 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getRubric()
        throws org.osid.OperationFailedException {

        return (this.assessment.getRubric());
    }


    /**
     *  Gets the assessment record corresponding to the given <code> 
     *  Assessment </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  assessmentRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(assessmentRecordType) </code> is <code> true </code> . 
     *
     *  @param  assessmentRecordType the type of the record to retrieve 
     *  @return the assessment record 
     *  @throws org.osid.NullArgumentException <code> assessmentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assessmentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentRecord getAssessmentRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        return (this.assessment.getAssessmentRecord(assessmentRecordType));
    }
}

