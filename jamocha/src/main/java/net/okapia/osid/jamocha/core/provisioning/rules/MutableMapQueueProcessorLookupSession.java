//
// MutableMapQueueProcessorLookupSession
//
//    Implements a QueueProcessor lookup service backed by a collection of
//    queueProcessors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a QueueProcessor lookup service backed by a collection of
 *  queue processors. The queue processors are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of queue processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapQueueProcessorLookupSession
    implements org.osid.provisioning.rules.QueueProcessorLookupSession {


    /**
     *  Constructs a new {@code MutableMapQueueProcessorLookupSession}
     *  with no queue processors.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueProcessorLookupSession} with a
     *  single queueProcessor.
     *
     *  @param distributor the distributor  
     *  @param queueProcessor a queue processor
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessor} is {@code null}
     */

    public MutableMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.QueueProcessor queueProcessor) {
        this(distributor);
        putQueueProcessor(queueProcessor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueProcessorLookupSession}
     *  using an array of queue processors.
     *
     *  @param distributor the distributor
     *  @param queueProcessors an array of queue processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessors} is {@code null}
     */

    public MutableMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.QueueProcessor[] queueProcessors) {
        this(distributor);
        putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueProcessorLookupSession}
     *  using a collection of queue processors.
     *
     *  @param distributor the distributor
     *  @param queueProcessors a collection of queue processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessors} is {@code null}
     */

    public MutableMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.rules.QueueProcessor> queueProcessors) {

        this(distributor);
        putQueueProcessors(queueProcessors);
        return;
    }

    
    /**
     *  Makes a {@code QueueProcessor} available in this session.
     *
     *  @param queueProcessor a queue processor
     *  @throws org.osid.NullArgumentException {@code queueProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putQueueProcessor(org.osid.provisioning.rules.QueueProcessor queueProcessor) {
        super.putQueueProcessor(queueProcessor);
        return;
    }


    /**
     *  Makes an array of queue processors available in this session.
     *
     *  @param queueProcessors an array of queue processors
     *  @throws org.osid.NullArgumentException {@code queueProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessors(org.osid.provisioning.rules.QueueProcessor[] queueProcessors) {
        super.putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Makes collection of queue processors available in this session.
     *
     *  @param queueProcessors a collection of queue processors
     *  @throws org.osid.NullArgumentException {@code queueProcessors{@code  is
     *          {@code null}
     */

    @Override
    public void putQueueProcessors(java.util.Collection<? extends org.osid.provisioning.rules.QueueProcessor> queueProcessors) {
        super.putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Removes a QueueProcessor from this session.
     *
     *  @param queueProcessorId the {@code Id} of the queue processor
     *  @throws org.osid.NullArgumentException {@code queueProcessorId{@code 
     *          is {@code null}
     */

    @Override
    public void removeQueueProcessor(org.osid.id.Id queueProcessorId) {
        super.removeQueueProcessor(queueProcessorId);
        return;
    }    
}
