//
// MutableIndexedMapProxyCanonicalUnitLookupSession
//
//    Implements a CanonicalUnit lookup service backed by a collection of
//    canonicalUnits indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a CanonicalUnit lookup service backed by a collection of
 *  canonicalUnits. The canonical units are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some canonicalUnits may be compatible
 *  with more types than are indicated through these canonicalUnit
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of canonical units can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractIndexedMapCanonicalUnitLookupSession
    implements org.osid.offering.CanonicalUnitLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCanonicalUnitLookupSession} with
     *  no canonical unit.
     *
     *  @param catalogue the catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                       org.osid.proxy.Proxy proxy) {
        setCatalogue(catalogue);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCanonicalUnitLookupSession} with
     *  a single canonical unit.
     *
     *  @param catalogue the catalogue
     *  @param  canonicalUnit an canonical unit
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnit}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                       org.osid.offering.CanonicalUnit canonicalUnit, org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnit(canonicalUnit);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCanonicalUnitLookupSession} using
     *  an array of canonical units.
     *
     *  @param catalogue the catalogue
     *  @param  canonicalUnits an array of canonical units
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnits}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                       org.osid.offering.CanonicalUnit[] canonicalUnits, org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnits(canonicalUnits);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCanonicalUnitLookupSession} using
     *  a collection of canonical units.
     *
     *  @param catalogue the catalogue
     *  @param  canonicalUnits a collection of canonical units
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnits}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                       java.util.Collection<? extends org.osid.offering.CanonicalUnit> canonicalUnits,
                                                       org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putCanonicalUnits(canonicalUnits);
        return;
    }

    
    /**
     *  Makes a {@code CanonicalUnit} available in this session.
     *
     *  @param  canonicalUnit a canonical unit
     *  @throws org.osid.NullArgumentException {@code canonicalUnit{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit) {
        super.putCanonicalUnit(canonicalUnit);
        return;
    }


    /**
     *  Makes an array of canonical units available in this session.
     *
     *  @param  canonicalUnits an array of canonical units
     *  @throws org.osid.NullArgumentException {@code canonicalUnits{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnits(org.osid.offering.CanonicalUnit[] canonicalUnits) {
        super.putCanonicalUnits(canonicalUnits);
        return;
    }


    /**
     *  Makes collection of canonical units available in this session.
     *
     *  @param  canonicalUnits a collection of canonical units
     *  @throws org.osid.NullArgumentException {@code canonicalUnit{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnits(java.util.Collection<? extends org.osid.offering.CanonicalUnit> canonicalUnits) {
        super.putCanonicalUnits(canonicalUnits);
        return;
    }


    /**
     *  Removes a CanonicalUnit from this session.
     *
     *  @param canonicalUnitId the {@code Id} of the canonical unit
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCanonicalUnit(org.osid.id.Id canonicalUnitId) {
        super.removeCanonicalUnit(canonicalUnitId);
        return;
    }    
}
