//
// AbstractAssemblySignalEnablerQuery.java
//
//     A SignalEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.path.rules.signalenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SignalEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblySignalEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.mapping.path.rules.SignalEnablerQuery,
               org.osid.mapping.path.rules.SignalEnablerQueryInspector,
               org.osid.mapping.path.rules.SignalEnablerSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.rules.records.SignalEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.rules.records.SignalEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySignalEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySignalEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to a signal. 
     *
     *  @param  signalId the signal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> signalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSignalId(org.osid.id.Id signalId, boolean match) {
        getAssembler().addIdTerm(getRuledSignalIdColumn(), signalId, match);
        return;
    }


    /**
     *  Clears the signal <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSignalIdTerms() {
        getAssembler().clearTerms(getRuledSignalIdColumn());
        return;
    }


    /**
     *  Gets the signal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSignalIdTerms() {
        return (getAssembler().getIdTerms(getRuledSignalIdColumn()));
    }


    /**
     *  Gets the RuledSignalId column name.
     *
     * @return the column name
     */

    protected String getRuledSignalIdColumn() {
        return ("ruled_signal_id");
    }


    /**
     *  Tests if a <code> SignalQuery </code> is available. 
     *
     *  @return <code> true </code> if a signal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSignalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a signal. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the signal query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSignalQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuery getRuledSignalQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSignalQuery() is false");
    }


    /**
     *  Matches rules mapped to any signal. 
     *
     *  @param  match <code> true </code> for rules mapped to any signal, 
     *          <code> false </code> to match rules mapped to no signals 
     */

    @OSID @Override
    public void matchAnyRuledSignal(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledSignalColumn(), match);
        return;
    }


    /**
     *  Clears the signal query terms. 
     */

    @OSID @Override
    public void clearRuledSignalTerms() {
        getAssembler().clearTerms(getRuledSignalColumn());
        return;
    }


    /**
     *  Gets the signal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQueryInspector[] getRuledSignalTerms() {
        return (new org.osid.mapping.path.SignalQueryInspector[0]);
    }


    /**
     *  Gets the RuledSignal column name.
     *
     * @return the column name
     */

    protected String getRuledSignalColumn() {
        return ("ruled_signal");
    }


    /**
     *  Matches enablers mapped to an map. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if an <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if an map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for an map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this signalEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  signalEnablerRecordType a signal enabler record type 
     *  @return <code>true</code> if the signalEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type signalEnablerRecordType) {
        for (org.osid.mapping.path.rules.records.SignalEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(signalEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  signalEnablerRecordType the signal enabler record type 
     *  @return the signal enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SignalEnablerQueryRecord getSignalEnablerQueryRecord(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SignalEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(signalEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  signalEnablerRecordType the signal enabler record type 
     *  @return the signal enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord getSignalEnablerQueryInspectorRecord(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(signalEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param signalEnablerRecordType the signal enabler record type
     *  @return the signal enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SignalEnablerSearchOrderRecord getSignalEnablerSearchOrderRecord(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SignalEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(signalEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this signal enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param signalEnablerQueryRecord the signal enabler query record
     *  @param signalEnablerQueryInspectorRecord the signal enabler query inspector
     *         record
     *  @param signalEnablerSearchOrderRecord the signal enabler search order record
     *  @param signalEnablerRecordType signal enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerQueryRecord</code>,
     *          <code>signalEnablerQueryInspectorRecord</code>,
     *          <code>signalEnablerSearchOrderRecord</code> or
     *          <code>signalEnablerRecordTypesignalEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addSignalEnablerRecords(org.osid.mapping.path.rules.records.SignalEnablerQueryRecord signalEnablerQueryRecord, 
                                      org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord signalEnablerQueryInspectorRecord, 
                                      org.osid.mapping.path.rules.records.SignalEnablerSearchOrderRecord signalEnablerSearchOrderRecord, 
                                      org.osid.type.Type signalEnablerRecordType) {

        addRecordType(signalEnablerRecordType);

        nullarg(signalEnablerQueryRecord, "signal enabler query record");
        nullarg(signalEnablerQueryInspectorRecord, "signal enabler query inspector record");
        nullarg(signalEnablerSearchOrderRecord, "signal enabler search odrer record");

        this.queryRecords.add(signalEnablerQueryRecord);
        this.queryInspectorRecords.add(signalEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(signalEnablerSearchOrderRecord);
        
        return;
    }
}
