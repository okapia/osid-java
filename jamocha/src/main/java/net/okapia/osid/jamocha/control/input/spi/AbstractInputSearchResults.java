//
// AbstractInputSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.input.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractInputSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.InputSearchResults {

    private org.osid.control.InputList inputs;
    private final org.osid.control.InputQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.InputSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractInputSearchResults.
     *
     *  @param inputs the result set
     *  @param inputQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>inputs</code>
     *          or <code>inputQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractInputSearchResults(org.osid.control.InputList inputs,
                                            org.osid.control.InputQueryInspector inputQueryInspector) {
        nullarg(inputs, "inputs");
        nullarg(inputQueryInspector, "input query inspectpr");

        this.inputs = inputs;
        this.inspector = inputQueryInspector;

        return;
    }


    /**
     *  Gets the input list resulting from a search.
     *
     *  @return an input list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.InputList getInputs() {
        if (this.inputs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.InputList inputs = this.inputs;
        this.inputs = null;
	return (inputs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.InputQueryInspector getInputQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  input search record <code> Type. </code> This method must
     *  be used to retrieve an input implementing the requested
     *  record.
     *
     *  @param inputSearchRecordType an input search 
     *         record type 
     *  @return the input search
     *  @throws org.osid.NullArgumentException
     *          <code>inputSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(inputSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.InputSearchResultsRecord getInputSearchResultsRecord(org.osid.type.Type inputSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.InputSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(inputSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(inputSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record input search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addInputRecord(org.osid.control.records.InputSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "input record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
