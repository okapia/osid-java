//
// AbstractProcessQuery.java
//
//     A template for making a Process Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for processes.
 */

public abstract class AbstractProcessQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.process.ProcessQuery {

    private final java.util.Collection<org.osid.process.records.ProcessQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears the state <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getStateQuery() {
        throw new org.osid.UnimplementedException("supportsStateQuery() is false");
    }


    /**
     *  Matches processes with any states. 
     *
     *  @param  match <code> true </code> to match processes with any states, 
     *          <code> false </code> to match processes with no states 
     */

    @OSID @Override
    public void matchAnyState(boolean match) {
        return;
    }


    /**
     *  Clears the state terms. 
     */

    @OSID @Override
    public void clearStateTerms() {
        return;
    }


    /**
     *  Sets the process <code> Id </code> for this query to match processes 
     *  that have the specified process as an ancestor. 
     *
     *  @param  processId a process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorProcessId(org.osid.id.Id processId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorProcessQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuery getAncestorProcessQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorProcessQuery() is false");
    }


    /**
     *  Matches processes with any ancestor. 
     *
     *  @param  match <code> true </code> to match process with any ancestor, 
     *          <code> false </code> to match root processes 
     */

    @OSID @Override
    public void matchAnyAncestorProcess(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor process terms. 
     */

    @OSID @Override
    public void clearAncestorProcessTerms() {
        return;
    }


    /**
     *  Sets the process <code> Id </code> for this query to match that have 
     *  the specified process as a descendant. 
     *
     *  @param  processId a process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantProcessId(org.osid.id.Id processId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantProcessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuery getDescendantProcessQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantProcessQuery() is false");
    }


    /**
     *  Matches processes with any descendant. 
     *
     *  @param  match <code> true </code> to match process with any 
     *          descendant, <code> false </code> to match leaf processes 
     */

    @OSID @Override
    public void matchAnyDescendantProcess(boolean match) {
        return;
    }


    /**
     *  Clears the descendant process terms. 
     */

    @OSID @Override
    public void clearDescendantProcessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given process query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a process implementing the requested record.
     *
     *  @param processRecordType a process record type
     *  @return the process query record
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.ProcessQueryRecord getProcessQueryRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.ProcessQueryRecord record : this.records) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Adds a record to this process query. 
     *
     *  @param processQueryRecord process query record
     *  @param processRecordType process record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcessQueryRecord(org.osid.process.records.ProcessQueryRecord processQueryRecord, 
                                          org.osid.type.Type processRecordType) {

        addRecordType(processRecordType);
        nullarg(processQueryRecord, "process query record");
        this.records.add(processQueryRecord);        
        return;
    }
}
