//
// AbstractMessagingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractMessagingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.messaging.MessagingManager,
               org.osid.messaging.MessagingProxyManager {

    private final Types messageRecordTypes                 = new TypeRefSet();
    private final Types messageSearchRecordTypes           = new TypeRefSet();

    private final Types receiptRecordTypes                 = new TypeRefSet();
    private final Types mailboxRecordTypes                 = new TypeRefSet();
    private final Types mailboxSearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractMessagingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractMessagingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if sending messages is supported. 
     *
     *  @return <code> true </code> if message sending is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessaging() {
        return (false);
    }


    /**
     *  Tests if message lookup is supported. 
     *
     *  @return <code> true </code> if message lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageLookup() {
        return (false);
    }


    /**
     *  Tests if querying messages is supported. 
     *
     *  @return <code> true </code> if message query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageQuery() {
        return (false);
    }


    /**
     *  Tests if message search is supported. 
     *
     *  @return <code> true </code> if message search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageSearch() {
        return (false);
    }


    /**
     *  Tests if creating, updating and deleting messages is supported. 
     *
     *  @return <code> true </code> if message administration is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageAdmin() {
        return (false);
    }


    /**
     *  Tests if message notification is supported. Messages may be sent when 
     *  messages are created, modified, or deleted. 
     *
     *  @return <code> true </code> if message notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of message and mailboxes is supported. 
     *
     *  @return <code> true </code> if message mailbox mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageMailbox() {
        return (false);
    }


    /**
     *  Tests if managing mappings of messages and mailboxes is supported. 
     *
     *  @return <code> true </code> if message mailbox assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageMailboxAssignment() {
        return (false);
    }


    /**
     *  Tests if a messaging smart mailbox service is supported. 
     *
     *  @return <code> true </code> if a message smart mailbox service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageSmartMailbox() {
        return (false);
    }


    /**
     *  Tests if receipt lookup is supported. 
     *
     *  @return <code> true </code> if receipt lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptLookup() {
        return (false);
    }


    /**
     *  Tests if updating receipts is supported. 
     *
     *  @return <code> true </code> if receipt administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptAdmin() {
        return (false);
    }


    /**
     *  Tests if receipts notification is supported. Messages may be sent when 
     *  receipts are created, modified, or deleted. 
     *
     *  @return <code> true </code> if receipt notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptNotification() {
        return (false);
    }


    /**
     *  Tests if mailbox lookup is supported. 
     *
     *  @return <code> true </code> if mailbox lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxLookup() {
        return (false);
    }


    /**
     *  Tests if mailbox search is supported. 
     *
     *  @return <code> true </code> if mailbox search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxSearch() {
        return (false);
    }


    /**
     *  Tests if mailbox administration is supported. 
     *
     *  @return <code> true </code> if mailbox administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxAdmin() {
        return (false);
    }


    /**
     *  Tests if mailbox notification is supported. Messages may be sent when 
     *  <code> Mailbox </code> objects are created, deleted or updated. 
     *  Notifications for messages within mailboxes are sent via the message 
     *  notification session. 
     *
     *  @return <code> true </code> if mailbox notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxNotification() {
        return (false);
    }


    /**
     *  Tests if a mailbox hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a mailbox hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxHierarchy() {
        return (false);
    }


    /**
     *  Tests if a mailbox hierarchy design is supported. 
     *
     *  @return <code> true </code> if a mailbox hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a messaging batch service is supported. 
     *
     *  @return <code> true </code> if a messaging batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessagingBatch() {
        return (false);
    }


    /**
     *  Gets all the message record types supported. 
     *
     *  @return the list of supported message record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMessageRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.messageRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given message record type is supported. 
     *
     *  @param  messageRecordType the message type 
     *  @return <code> true </code> if the message record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> messageRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMessageRecordType(org.osid.type.Type messageRecordType) {
        return (this.messageRecordTypes.contains(messageRecordType));
    }


    /**
     *  Adds support for a message record type.
     *
     *  @param messageRecordType a message record type
     *  @throws org.osid.NullArgumentException
     *  <code>messageRecordType</code> is <code>null</code>
     */

    protected void addMessageRecordType(org.osid.type.Type messageRecordType) {
        this.messageRecordTypes.add(messageRecordType);
        return;
    }


    /**
     *  Removes support for a message record type.
     *
     *  @param messageRecordType a message record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>messageRecordType</code> is <code>null</code>
     */

    protected void removeMessageRecordType(org.osid.type.Type messageRecordType) {
        this.messageRecordTypes.remove(messageRecordType);
        return;
    }


    /**
     *  Gets all the message search record types supported. 
     *
     *  @return the list of supported message search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMessageSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.messageSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given message search type is supported. 
     *
     *  @param  messageSearchRecordType the message search type 
     *  @return <code> true </code> if the message search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> messageSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMessageSearchRecordType(org.osid.type.Type messageSearchRecordType) {
        return (this.messageSearchRecordTypes.contains(messageSearchRecordType));
    }


    /**
     *  Adds support for a message search record type.
     *
     *  @param messageSearchRecordType a message search record type
     *  @throws org.osid.NullArgumentException
     *  <code>messageSearchRecordType</code> is <code>null</code>
     */

    protected void addMessageSearchRecordType(org.osid.type.Type messageSearchRecordType) {
        this.messageSearchRecordTypes.add(messageSearchRecordType);
        return;
    }


    /**
     *  Removes support for a message search record type.
     *
     *  @param messageSearchRecordType a message search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>messageSearchRecordType</code> is <code>null</code>
     */

    protected void removeMessageSearchRecordType(org.osid.type.Type messageSearchRecordType) {
        this.messageSearchRecordTypes.remove(messageSearchRecordType);
        return;
    }


    /**
     *  Gets all the receipt record types supported. 
     *
     *  @return the list of supported receipt record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getReceiptRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.receiptRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given receipt record type is supported. 
     *
     *  @param  receiptRecordType the mesreceiptsage type 
     *  @return <code> true </code> if the receipt record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> receiptRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsReceiptRecordType(org.osid.type.Type receiptRecordType) {
        return (this.receiptRecordTypes.contains(receiptRecordType));
    }


    /**
     *  Adds support for a receipt record type.
     *
     *  @param receiptRecordType a receipt record type
     *  @throws org.osid.NullArgumentException
     *  <code>receiptRecordType</code> is <code>null</code>
     */

    protected void addReceiptRecordType(org.osid.type.Type receiptRecordType) {
        this.receiptRecordTypes.add(receiptRecordType);
        return;
    }


    /**
     *  Removes support for a receipt record type.
     *
     *  @param receiptRecordType a receipt record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>receiptRecordType</code> is <code>null</code>
     */

    protected void removeReceiptRecordType(org.osid.type.Type receiptRecordType) {
        this.receiptRecordTypes.remove(receiptRecordType);
        return;
    }


    /**
     *  Gets all the mailbox record types supported. 
     *
     *  @return the list of supported mailbox record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMailboxRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.mailboxRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given mailbox record type is supported. 
     *
     *  @param  mailboxRecordType the mailbox record type 
     *  @return <code> true </code> if the mailbox record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mailboxRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMailboxRecordType(org.osid.type.Type mailboxRecordType) {
        return (this.mailboxRecordTypes.contains(mailboxRecordType));
    }


    /**
     *  Adds support for a mailbox record type.
     *
     *  @param mailboxRecordType a mailbox record type
     *  @throws org.osid.NullArgumentException
     *  <code>mailboxRecordType</code> is <code>null</code>
     */

    protected void addMailboxRecordType(org.osid.type.Type mailboxRecordType) {
        this.mailboxRecordTypes.add(mailboxRecordType);
        return;
    }


    /**
     *  Removes support for a mailbox record type.
     *
     *  @param mailboxRecordType a mailbox record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>mailboxRecordType</code> is <code>null</code>
     */

    protected void removeMailboxRecordType(org.osid.type.Type mailboxRecordType) {
        this.mailboxRecordTypes.remove(mailboxRecordType);
        return;
    }


    /**
     *  Gets all the mailbox search record types supported. 
     *
     *  @return the list of supported mailbox search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMailboxSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.mailboxSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given mailbox search record type is supported. 
     *
     *  @param  mailboxSearchRecordType the mailbox search record type 
     *  @return <code> true </code> if the mailbox search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mailboxSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMailboxSearchRecordType(org.osid.type.Type mailboxSearchRecordType) {
        return (this.mailboxSearchRecordTypes.contains(mailboxSearchRecordType));
    }


    /**
     *  Adds support for a mailbox search record type.
     *
     *  @param mailboxSearchRecordType a mailbox search record type
     *  @throws org.osid.NullArgumentException
     *  <code>mailboxSearchRecordType</code> is <code>null</code>
     */

    protected void addMailboxSearchRecordType(org.osid.type.Type mailboxSearchRecordType) {
        this.mailboxSearchRecordTypes.add(mailboxSearchRecordType);
        return;
    }


    /**
     *  Removes support for a mailbox search record type.
     *
     *  @param mailboxSearchRecordType a mailbox search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>mailboxSearchRecordType</code> is <code>null</code>
     */

    protected void removeMailboxSearchRecordType(org.osid.type.Type mailboxSearchRecordType) {
        this.mailboxSearchRecordTypes.remove(mailboxSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message 
     *  sending. 
     *
     *  @return <code> a MessagingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessaging() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingSession getMessagingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessagingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message 
     *  sending service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessagingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessaging() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingSession getMessagingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessagingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service. 
     *
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageLookupSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageLookupSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service. 
     *
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.NotFoundException no mailbox found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageQuerySessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageQuerySessionForMailbox not implemented");
    }


    /**
     *  Gets a message search session. 
     *
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageSearchSession not implemented");
    }


    /**
     *  Gets a message search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageSearchSession not implemented");
    }


    /**
     *  Gets a message search session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageSearchSessionForMailbox not implemented");
    }


    /**
     *  Gets a message search session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageSearchSessionForMailbox not implemented");
    }


    /**
     *  Gets a message administration session for creating, updating and 
     *  deleting messages. 
     *
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageAdminSession not implemented");
    }


    /**
     *  Gets a message administration session for creating, updating and 
     *  deleting messages. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageAdminSession not implemented");
    }


    /**
     *  Gets a message administration session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets a message administration session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to message 
     *  changes. 
     *
     *  @param  messageReceiver the notification callback 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSession(org.osid.messaging.MessageReceiver messageReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageNotificationSession not implemented");
    }


    /**
     *  Gets the message notification session for the given mailbox. 
     *
     *  @param  messageReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSession(org.osid.messaging.MessageReceiver messageReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageNotificationSession not implemented");
    }


    /**
     *  Gets the message notification session for the given mailbox. 
     *
     *  @param  messageReceiver the notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          or <code> mailboxId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSessionForMailbox(org.osid.messaging.MessageReceiver messageReceiver, 
                                                                                                 org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageNotificationSessionForMailbox not implemented");
    }


    /**
     *  Gets the message notification session for the given mailbox. 
     *
     *  @param  messageReceiver notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> messageReceiver, 
     *          mailboxId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSessionForMailbox(org.osid.messaging.MessageReceiver messageReceiver, 
                                                                                                 org.osid.id.Id mailboxId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageNotificationSessionForMailbox not implemented");
    }


    /**
     *  Gets the session for retrieving message to mailbox mappings. 
     *
     *  @return a <code> MessageMailboxSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxSession getMessageMailboxSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageMailboxSession not implemented");
    }


    /**
     *  Gets the session for retrieving message to mailbox mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageMailboxSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxSession getMessageMailboxSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageMailboxSession not implemented");
    }


    /**
     *  Gets the session for assigning message to mailbox mappings. 
     *
     *  @return a <code> MessageMailboxAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailboxAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxAssignmentSession getMessageMailboxAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageMailboxAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning message to mailbox mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageMailboxAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailboxAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxAssignmentSession getMessageMailboxAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageMailboxAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart mailboxes. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return a <code> MessageSmartMailboxSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageSmartMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSmartMailboxSession getMessageSmartMailboxSession(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessageSmartMailboxSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage message smart mailboxes. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return a <code> MessageSmartMailboxesession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageSmartMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSmartMailboxSession getMessageSmartMailboxSession(org.osid.id.Id mailboxId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessageSmartMailboxSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service. 
     *
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getReceiptLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getReceiptLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getReceiptLookupSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getReceiptLookupSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service. 
     *
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getReceiptAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getReceiptAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getReceiptAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getReceiptAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to receipt 
     *  changes. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSession(org.osid.messaging.ReceiptReceiver receiptReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getReceiptNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to receipt 
     *  changes. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSession(org.osid.messaging.ReceiptReceiver receiptReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getReceiptNotificationSession not implemented");
    }


    /**
     *  Gets the receipt notification session for the given mailbox. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver </code> 
     *          or <code> mailboxId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSessionForMailbox(org.osid.messaging.ReceiptReceiver receiptReceiver, 
                                                                                                 org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getReceiptNotificationSessionForMailbox not implemented");
    }


    /**
     *  Gets the receipt notification session for the given mailbox. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @param  proxy a proxy 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver, 
     *          mailboxId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSessionForMailbox(org.osid.messaging.ReceiptReceiver receiptReceiver, 
                                                                                                 org.osid.id.Id mailboxId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getReceiptNotificationSessionForMailbox not implemented");
    }


    /**
     *  Gets the mailbox lookup session. 
     *
     *  @return a <code> MailboxLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxLookupSession getMailboxLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMailboxLookupSession not implemented");
    }


    /**
     *  Gets the mailbox lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxLookupSession getMailboxLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMailboxLookupSession not implemented");
    }


    /**
     *  Gets the mailbox query session. 
     *
     *  @return a <code> MailboxQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMailboxQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMailboxQuerySession not implemented");
    }


    /**
     *  Gets the mailbox query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuerySession getMailboxQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMailboxQuerySession not implemented");
    }


    /**
     *  Gets the mailbox search session. 
     *
     *  @return a <code> MailboxSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxSearchSession getMailboxSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMailboxSearchSession not implemented");
    }


    /**
     *  Gets the mailbox search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxSearchSession getMailboxSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMailboxSearchSession not implemented");
    }


    /**
     *  Gets the mailbox administrative session for creating, updating and 
     *  deleteing mailboxes. 
     *
     *  @return a <code> MailboxAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxAdminSession getMailboxAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMailboxAdminSession not implemented");
    }


    /**
     *  Gets the mailbox administrative session for creating, updating and 
     *  deleteing mailboxes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxAdminSession getMailboxAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMailboxAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to a mailbox. 
     *
     *  @param  mailboxReceiver the notification callback 
     *  @return a <code> MailboxNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxNotificationSession getMailboxNotificationSession(org.osid.messaging.MailboxReceiver mailboxReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMailboxNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to a mailbox. 
     *
     *  @param  mailboxReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> MailboxNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxNotificationSession getMailboxNotificationSession(org.osid.messaging.MailboxReceiver mailboxReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMailboxNotificationSession not implemented");
    }


    /**
     *  Gets the mailbox hierarchy traversal session. 
     *
     *  @return <code> a MailboxHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchySession getMailboxHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMailboxHierarchySession not implemented");
    }


    /**
     *  Gets the mailbox hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a MailboxHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchySession getMailboxHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMailboxHierarchySession not implemented");
    }


    /**
     *  Gets the mailbox hierarchy design session. 
     *
     *  @return a <code> MailboxHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchyDesignSession getMailboxHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMailboxHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the mailbox hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchyDesignSession getMailboxHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMailboxHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> MessagingBatchManager. </code> 
     *
     *  @return a <code> MessagingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessagingBatchManager getMessagingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingManager.getMessagingBatchManager not implemented");
    }


    /**
     *  Gets a <code> MessagingBatchProxyManager. </code> 
     *
     *  @return a <code> MessagingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessagingBatchProxyManager getMessagingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.MessagingProxyManager.getMessagingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.messageRecordTypes.clear();
        this.messageRecordTypes.clear();

        this.messageSearchRecordTypes.clear();
        this.messageSearchRecordTypes.clear();

        this.receiptRecordTypes.clear();
        this.receiptRecordTypes.clear();

        this.mailboxRecordTypes.clear();
        this.mailboxRecordTypes.clear();

        this.mailboxSearchRecordTypes.clear();
        this.mailboxSearchRecordTypes.clear();

        return;
    }
}
