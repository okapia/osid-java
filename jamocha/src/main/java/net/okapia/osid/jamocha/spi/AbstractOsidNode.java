//
// AbstractOsisNode.java
//
//     A template for an OsidNode.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An abstract template for an OsidNode.
 */

public abstract class AbstractOsidNode
    extends AbstractIdentifiable
    implements org.osid.OsidNode {

    private boolean root = false;
    private boolean leaf = false;


    /**
     *  Tests if this <code> Containable </code> is sequestered in that it 
     *  should not appear outside of its aggregated composition. 
     *
     *  @return <code> true </code> if this containable is sequestered, <code> 
     *          false </code> if this containable may appear outside its 
     *          aggregate 
     */
     
    @OSID @Override
    public boolean isSequestered() {
        return (true);
    }


    /**
     *  Tests if this node is a root in the hierarchy (has no
     *  parents). A node may have no more parents available in this
     *  node structure but is not a root in the hierarchy. If both
     *  <code> isRoot() </code> and <code> hasParents() </code> is
     *  false, the parents of this node may be accessed thorugh
     *  another node structure retrieval.
     *
     *  @return <code> true </code> if this node is a root in the
     *          hierarchy, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isRoot() {
        return (this.root);
    }


    /**
     *  Designates this node as a root.
     */

    protected void root() {
        this.root = true;
        return;
    }


    /**
     *  Designates this node as a non-root.
     */

    protected void unroot() {
        this.root = false;
        return;
    }


    /**
     *  Tests if this node is a leaf in the hierarchy (has no
     *  children). A node may have no more children available in this
     *  node structure but is not a leaf in the hierarchy. If both
     *  <code> isLeaf() </code> and <code> hasChildren() </code> is
     *  false, the children of this node may be accessed thorugh
     *  another node structure retrieval.
     *
     *  @return <code> true </code> if this node is a leaf in the
     *          hierarchy, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isLeaf() {
        return (this.leaf);
    }


    /**
     *  Designates this node as a leaf.
     */

    protected void leaf() {
        this.leaf = true;
        return;
    }


    /**
     *  Designates this node as a non-leaf.
     */

    protected void unleaf() {
        this.leaf = false;
        return;
    }
}