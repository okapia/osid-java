//
// AbstractPoolProcessorEnablerSearch.java
//
//     A template for making a PoolProcessorEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing pool processor enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPoolProcessorEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.PoolProcessorEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.PoolProcessorEnablerSearchOrder poolProcessorEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of pool processor enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  poolProcessorEnablerIds list of pool processor enablers
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPoolProcessorEnablers(org.osid.id.IdList poolProcessorEnablerIds) {
        while (poolProcessorEnablerIds.hasNext()) {
            try {
                this.ids.add(poolProcessorEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPoolProcessorEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of pool processor enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPoolProcessorEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  poolProcessorEnablerSearchOrder pool processor enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>poolProcessorEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPoolProcessorEnablerResults(org.osid.provisioning.rules.PoolProcessorEnablerSearchOrder poolProcessorEnablerSearchOrder) {
	this.poolProcessorEnablerSearchOrder = poolProcessorEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.PoolProcessorEnablerSearchOrder getPoolProcessorEnablerSearchOrder() {
	return (this.poolProcessorEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given pool processor enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool processor enabler implementing the requested record.
     *
     *  @param poolProcessorEnablerSearchRecordType a pool processor enabler search record
     *         type
     *  @return the pool processor enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorEnablerSearchRecord getPoolProcessorEnablerSearchRecord(org.osid.type.Type poolProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.PoolProcessorEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool processor enabler search. 
     *
     *  @param poolProcessorEnablerSearchRecord pool processor enabler search record
     *  @param poolProcessorEnablerSearchRecordType poolProcessorEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolProcessorEnablerSearchRecord(org.osid.provisioning.rules.records.PoolProcessorEnablerSearchRecord poolProcessorEnablerSearchRecord, 
                                           org.osid.type.Type poolProcessorEnablerSearchRecordType) {

        addRecordType(poolProcessorEnablerSearchRecordType);
        this.records.add(poolProcessorEnablerSearchRecord);        
        return;
    }
}
