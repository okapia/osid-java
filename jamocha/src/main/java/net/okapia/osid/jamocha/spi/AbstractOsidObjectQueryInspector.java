//
// AbstractOsidObjectQueryInspector.java
//
//     Defines an OsidObjectQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidObjectQueryInspector
    extends AbstractOsidIdentifiableQueryInspector
    implements org.osid.OsidObjectQueryInspector {

    private final OsidBrowsableQueryInspector browsable = new OsidBrowsableQueryInspector();


    /**
     *  Gets the display name query terms.
     *
     *  @return the display name terms
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the description query terms.
     *
     *  @return the description terms
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDescriptionTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the genus type query terms.
     *
     *  @return the genus type terms
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getGenusTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the parent genus type query terms.
     *
     *  @return the parent genus type terms
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getParentGenusTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the subject <code>Id</code> query terms.
     *
     *  @return the subject <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubjectIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subject query terms.
     *
     *  @return the subject terms
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the relevancy query terms.
     *
     *  @return the relevancy terms
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getSubjectRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Gets the state <code>Id</code> query terms.
     *
     *  @return the state <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStateIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the state query terms.
     *
     *  @return the state terms
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the comment <code>Id</code> query terms.
     *
     *  @return the comment <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the comment query terms.
     *
     *  @return the comment terms
     */

    @OSID @Override
    public org.osid.commenting.CommentQueryInspector[] getCommentTerms() {
        return (new org.osid.commenting.CommentQueryInspector[0]);
    }


    /**
     *  Gets the journal entry <code>Id</code> query terms.
     *
     *  @return the journal entry <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJournalEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the journal entry query terms.
     *
     *  @return the journal entry terms
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }


    /**
     *  Gets the statistic query terms.
     *
     *  @return the statistic terms
     */

    @OSID @Override
    public org.osid.metering.StatisticQueryInspector[] getStatisticTerms() {
        return (new org.osid.metering.StatisticQueryInspector[0]);
    }


    /**
     *  Gets the credit <code>Id</code> query terms.
     *
     *  @return the credit <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the credit query terms.
     *
     *  @return the credit terms
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQueryInspector[] getCreditTerms() {
        return (new org.osid.acknowledgement.CreditQueryInspector[0]);
    }


    /**
     *  Gets the relationship <code>Id</code> query terms.
     *
     *  @return the relationship <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the relationship query terms.
     *
     *  @return the relationship terms
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQueryInspector[] getRelationshipTerms() {
        return (new org.osid.relationship.RelationshipQueryInspector[0]);
    }


    /**
     *  Gets the relationship peer <code>Id</code> query terms.
     *
     *  @return the relationship peer <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipPeerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the record type query terms.
     *
     *  @return the record type terms
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getRecordTypeTerms() {
        return (this.browsable.getRecordTypeTerms());
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.browsable.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code>true</code> if <code>recordType</code> is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.browsable.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.browsable.addRecordType(recordType);
        return;
    }


    protected class OsidBrowsableQueryInspector
        extends AbstractOsidBrowsableQueryInspector
        implements org.osid.OsidBrowsableQueryInspector,
                   org.osid.OsidExtensibleQueryInspector {


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }
    }
}
