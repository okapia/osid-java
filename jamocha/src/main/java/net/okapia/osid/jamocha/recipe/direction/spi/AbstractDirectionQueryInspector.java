//
// AbstractDirectionQueryInspector.java
//
//     A template for making a DirectionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for directions.
 */

public abstract class AbstractDirectionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.recipe.DirectionQueryInspector {

    private final java.util.Collection<org.osid.recipe.records.DirectionQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the recipe <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the recipe query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQueryInspector[] getRecipeTerms() {
        return (new org.osid.recipe.RecipeQueryInspector[0]);
    }


    /**
     *  Gets the procedure query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcedureIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the procedure query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQueryInspector[] getProcedureTerms() {
        return (new org.osid.recipe.ProcedureQueryInspector[0]);
    }


    /**
     *  Gets the ingredient query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIngredientIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ingredient query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.IngredientQueryInspector[] getIngredientTerms() {
        return (new org.osid.recipe.IngredientQueryInspector[0]);
    }


    /**
     *  Gets the estimated duration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getEstimatedDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the cook book <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCookbookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the cook book query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given direction query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a direction implementing the requested record.
     *
     *  @param directionRecordType a direction record type
     *  @return the direction query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionQueryInspectorRecord getDirectionQueryInspectorRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.DirectionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(directionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this direction query. 
     *
     *  @param directionQueryInspectorRecord direction query inspector
     *         record
     *  @param directionRecordType direction record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDirectionQueryInspectorRecord(org.osid.recipe.records.DirectionQueryInspectorRecord directionQueryInspectorRecord, 
                                                   org.osid.type.Type directionRecordType) {

        addRecordType(directionRecordType);
        nullarg(directionRecordType, "direction record type");
        this.records.add(directionQueryInspectorRecord);        
        return;
    }
}
