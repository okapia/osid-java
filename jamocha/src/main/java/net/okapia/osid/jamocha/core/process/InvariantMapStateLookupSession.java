//
// InvariantMapStateLookupSession
//
//    Implements a State lookup service backed by a fixed collection of
//    states.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process;


/**
 *  Implements a State lookup service backed by a fixed
 *  collection of states. The states are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapStateLookupSession
    extends net.okapia.osid.jamocha.core.process.spi.AbstractMapStateLookupSession
    implements org.osid.process.StateLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapStateLookupSession</code> with no
     *  states.
     *  
     *  @param process the process
     *  @throws org.osid.NullArgumnetException {@code process} is
     *          {@code null}
     */

    public InvariantMapStateLookupSession(org.osid.process.Process process) {
        setProcess(process);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStateLookupSession</code> with a single
     *  state.
     *  
     *  @param process the process
     *  @param state a single state
     *  @throws org.osid.NullArgumentException {@code process} or
     *          {@code state} is <code>null</code>
     */

      public InvariantMapStateLookupSession(org.osid.process.Process process,
                                               org.osid.process.State state) {
        this(process);
        putState(state);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStateLookupSession</code> using an array
     *  of states.
     *  
     *  @param process the process
     *  @param states an array of states
     *  @throws org.osid.NullArgumentException {@code process} or
     *          {@code states} is <code>null</code>
     */

      public InvariantMapStateLookupSession(org.osid.process.Process process,
                                               org.osid.process.State[] states) {
        this(process);
        putStates(states);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStateLookupSession</code> using a
     *  collection of states.
     *
     *  @param process the process
     *  @param states a collection of states
     *  @throws org.osid.NullArgumentException {@code process} or
     *          {@code states} is <code>null</code>
     */

      public InvariantMapStateLookupSession(org.osid.process.Process process,
                                               java.util.Collection<? extends org.osid.process.State> states) {
        this(process);
        putStates(states);
        return;
    }
}
