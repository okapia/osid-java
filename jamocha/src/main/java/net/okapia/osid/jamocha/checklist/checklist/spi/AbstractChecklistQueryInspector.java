//
// AbstractChecklistQueryInspector.java
//
//     A template for making a ChecklistQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for checklists.
 */

public abstract class AbstractChecklistQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.checklist.ChecklistQueryInspector {

    private final java.util.Collection<org.osid.checklist.records.ChecklistQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the todo <code> Id </code> terms. 
     *
     *  @return the todo <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTodoIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the todo terms. 
     *
     *  @return the todo terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the ancestor checklist <code> Id </code> terms. 
     *
     *  @return the ancestor checklist <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorChecklistIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor checklist terms. 
     *
     *  @return the ancestor checklist terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getAncestorChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }


    /**
     *  Gets the descendant checklist <code> Id </code> terms. 
     *
     *  @return the descendant checklist <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantChecklistIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant checklist terms. 
     *
     *  @return the descendant checklist terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getDescendantChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given checklist query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a checklist implementing the requested record.
     *
     *  @param checklistRecordType a checklist record type
     *  @return the checklist query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checklistRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.ChecklistQueryInspectorRecord getChecklistQueryInspectorRecord(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.ChecklistQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checklistRecordType + " is not supported");
    }


    /**
     *  Adds a record to this checklist query. 
     *
     *  @param checklistQueryInspectorRecord checklist query inspector
     *         record
     *  @param checklistRecordType checklist record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addChecklistQueryInspectorRecord(org.osid.checklist.records.ChecklistQueryInspectorRecord checklistQueryInspectorRecord, 
                                                   org.osid.type.Type checklistRecordType) {

        addRecordType(checklistRecordType);
        nullarg(checklistRecordType, "checklist record type");
        this.records.add(checklistQueryInspectorRecord);        
        return;
    }
}
