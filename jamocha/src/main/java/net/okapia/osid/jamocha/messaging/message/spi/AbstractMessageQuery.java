//
// AbstractMessageQuery.java
//
//     A template for making a Message Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.message.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for messages.
 */

public abstract class AbstractMessageQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.messaging.MessageQuery {

    private final java.util.Collection<org.osid.messaging.records.MessageQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a subject line to match. Multiple subject line matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subject subject to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> subject is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> subject </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSubjectLine(String subject, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches messages with any subject line. 
     *
     *  @param  match <code> true </code> to match messages with any subject 
     *          line, <code> false </code> to match messages with no subject 
     *          line 
     */

    @OSID @Override
    public void matchAnySubjectLine(boolean match) {
        return;
    }


    /**
     *  Clears the subject line terms. 
     */

    @OSID @Override
    public void clearSubjectLineTerms() {
        return;
    }


    /**
     *  Adds text to match. Multiple subject line matches can be added to 
     *  perform a boolean <code> OR </code> among them. 
     *
     *  @param  text dtext to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches messages with any text. 
     *
     *  @param  match <code> true </code> to match messages with any text, 
     *          <code> false </code> to match messages with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        return;
    }


    /**
     *  Matches messages that have been sent. 
     *
     *  @param  match <code> true </code> to match sent messages, <code> false 
     *          </code> to match unsent messages 
     */

    @OSID @Override
    public void matchSent(boolean match) {
        return;
    }


    /**
     *  Clears the sent terms. 
     */

    @OSID @Override
    public void clearSentTerms() {
        return;
    }


    /**
     *  Matches messages whose sent time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSentTime(org.osid.calendaring.DateTime startTime, 
                              org.osid.calendaring.DateTime endTime, 
                              boolean match) {
        return;
    }


    /**
     *  Clears the sent time terms. 
     */

    @OSID @Override
    public void clearSentTimeTerms() {
        return;
    }


    /**
     *  Matches the sender of the message. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSenderId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the sender <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSenderIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  senders. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSenderQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSenderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSenderQuery() {
        throw new org.osid.UnimplementedException("supportsSenderQuery() is false");
    }


    /**
     *  Clears the sender terms. 
     */

    @OSID @Override
    public void clearSenderTerms() {
        return;
    }


    /**
     *  Matches the sending agent of the message. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSendingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the sending agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSendingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  senders. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSendingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSendingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getSendingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsSendingAgentQuery() is false");
    }


    /**
     *  Clears the sending agent terms. 
     */

    @OSID @Override
    public void clearSendingAgentTerms() {
        return;
    }


    /**
     *  Matches messages whose received time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReceivedTime(org.osid.calendaring.DateTime startTime, 
                                  org.osid.calendaring.DateTime endTime, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the received time terms. 
     */

    @OSID @Override
    public void clearReceivedTimeTerms() {
        return;
    }


    /**
     *  Matches messages whose delivery duration is between the supplied range 
     *  inclusive. 
     *
     *  @param  start start time 
     *  @param  end end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDeliveryTime(org.osid.calendaring.Duration start, 
                                  org.osid.calendaring.Duration end, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the delivery time terms. 
     */

    @OSID @Override
    public void clearDeliveryTimeTerms() {
        return;
    }


    /**
     *  Matches any recipient of the message. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the recipient <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  recipients. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipient resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the recipient terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        return;
    }


    /**
     *  Matches the receipt of the message. 
     *
     *  @param  receiptId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> receiptId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReceiptId(org.osid.id.Id receiptId, boolean match) {
        return;
    }


    /**
     *  Clears the receipt <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReceiptIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ReceiptQuery </code> is available for querying 
     *  receipts. 
     *
     *  @return <code> true </code> if a receipt query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptQuery() {
        return (false);
    }


    /**
     *  Gets the query for the receipt. 
     *
     *  @return the receipt query 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptQuery getReceiptQuery() {
        throw new org.osid.UnimplementedException("supportsReceiptQuery() is false");
    }


    /**
     *  Matches any received messages. 
     *
     *  @param  match <code> true </code> to match any received messages, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyReceipt(boolean match) {
        return;
    }


    /**
     *  Clears the receipt terms. 
     */

    @OSID @Override
    public void clearReceiptTerms() {
        return;
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query. 
     *
     *  @param  mailboxId the mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMailboxId(org.osid.id.Id mailboxId, boolean match) {
        return;
    }


    /**
     *  Clears the mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMailboxIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsMailboxQuery() is false");
    }


    /**
     *  Clears the mailbox terms. 
     */

    @OSID @Override
    public void clearMailboxTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given message query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a message implementing the requested record.
     *
     *  @param messageRecordType a message record type
     *  @return the message query record
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(messageRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MessageQueryRecord getMessageQueryRecord(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MessageQueryRecord record : this.records) {
            if (record.implementsRecordType(messageRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(messageRecordType + " is not supported");
    }


    /**
     *  Adds a record to this message query. 
     *
     *  @param messageQueryRecord message query record
     *  @param messageRecordType message record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMessageQueryRecord(org.osid.messaging.records.MessageQueryRecord messageQueryRecord, 
                                          org.osid.type.Type messageRecordType) {

        addRecordType(messageRecordType);
        nullarg(messageQueryRecord, "message query record");
        this.records.add(messageQueryRecord);        
        return;
    }
}
