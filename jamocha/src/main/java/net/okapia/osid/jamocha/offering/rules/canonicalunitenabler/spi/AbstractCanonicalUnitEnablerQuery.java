//
// AbstractCanonicalUnitEnablerQuery.java
//
//     A template for making a CanonicalUnitEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for canonical unit enablers.
 */

public abstract class AbstractCanonicalUnitEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.offering.rules.CanonicalUnitEnablerQuery {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the offering constrainer. 
     *
     *  @param  offeringConstrainerId the offering constrainer <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offeringConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledOfferingConstrainerId(org.osid.id.Id offeringConstrainerId, 
                                                boolean match) {
        return;
    }


    /**
     *  Clears the offering constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledOfferingConstrainerIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OfferingConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if an offering constrainer query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledOfferingConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offering constrainer. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the offering constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledOfferingConstrainerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuery getRuledOfferingConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledOfferingConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any offering constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any offering 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no offering constrainers 
     */

    @OSID @Override
    public void matchAnyRuledOfferingConstrainer(boolean match) {
        return;
    }


    /**
     *  Clears the offering constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledOfferingConstrainerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given canonical unit enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a canonical unit enabler implementing the requested record.
     *
     *  @param canonicalUnitEnablerRecordType a canonical unit enabler record type
     *  @return the canonical unit enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitEnablerQueryRecord getCanonicalUnitEnablerQueryRecord(org.osid.type.Type canonicalUnitEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit enabler query. 
     *
     *  @param canonicalUnitEnablerQueryRecord canonical unit enabler query record
     *  @param canonicalUnitEnablerRecordType canonicalUnitEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitEnablerQueryRecord(org.osid.offering.rules.records.CanonicalUnitEnablerQueryRecord canonicalUnitEnablerQueryRecord, 
                                          org.osid.type.Type canonicalUnitEnablerRecordType) {

        addRecordType(canonicalUnitEnablerRecordType);
        nullarg(canonicalUnitEnablerQueryRecord, "canonical unit enabler query record");
        this.records.add(canonicalUnitEnablerQueryRecord);        
        return;
    }
}
