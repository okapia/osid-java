//
// AbstractIndexedMapAcademyLookupSession.java
//
//    A simple framework for providing an Academy lookup service
//    backed by a fixed collection of academies with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Academy lookup service backed by a
 *  fixed collection of academies. The academies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some academies may be compatible
 *  with more types than are indicated through these academy
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Academies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAcademyLookupSession
    extends AbstractMapAcademyLookupSession
    implements org.osid.recognition.AcademyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recognition.Academy> academiesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Academy>());
    private final MultiMap<org.osid.type.Type, org.osid.recognition.Academy> academiesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Academy>());


    /**
     *  Makes an <code>Academy</code> available in this session.
     *
     *  @param  academy an academy
     *  @throws org.osid.NullArgumentException <code>academy<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAcademy(org.osid.recognition.Academy academy) {
        super.putAcademy(academy);

        this.academiesByGenus.put(academy.getGenusType(), academy);
        
        try (org.osid.type.TypeList types = academy.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.academiesByRecord.put(types.getNextType(), academy);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an academy from this session.
     *
     *  @param academyId the <code>Id</code> of the academy
     *  @throws org.osid.NullArgumentException <code>academyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAcademy(org.osid.id.Id academyId) {
        org.osid.recognition.Academy academy;
        try {
            academy = getAcademy(academyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.academiesByGenus.remove(academy.getGenusType());

        try (org.osid.type.TypeList types = academy.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.academiesByRecord.remove(types.getNextType(), academy);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAcademy(academyId);
        return;
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  academy genus <code>Type</code> which does not include
     *  academies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known academies or an error results. Otherwise,
     *  the returned list may contain only those academies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.academy.ArrayAcademyList(this.academiesByGenus.get(academyGenusType)));
    }


    /**
     *  Gets an <code>AcademyList</code> containing the given
     *  academy record <code>Type</code>. In plenary mode, the
     *  returned list contains all known academies or an error
     *  results. Otherwise, the returned list may contain only those
     *  academies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  academyRecordType an academy record type 
     *  @return the returned <code>academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByRecordType(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.academy.ArrayAcademyList(this.academiesByRecord.get(academyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.academiesByGenus.clear();
        this.academiesByRecord.clear();

        super.close();

        return;
    }
}
