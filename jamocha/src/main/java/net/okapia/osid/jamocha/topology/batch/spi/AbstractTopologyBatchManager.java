//
// AbstractTopologyBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTopologyBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.topology.batch.TopologyBatchManager,
               org.osid.topology.batch.TopologyBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractTopologyBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTopologyBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of nodes is available. 
     *
     *  @return <code> true </code> if a node bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of edges is available. 
     *
     *  @return <code> true </code> if an edge bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of graph is available. 
     *
     *  @return <code> true </code> if a graph bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk node 
     *  administration service. 
     *
     *  @return a <code> NodeBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.NodeBatchAdminSession getNodeBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchManager.getNodeBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk node 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.NodeBatchAdminSession getNodeBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchProxyManager.getNodeBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk node 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.NodeBatchAdminSession getNodeBatchAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchManager.getNodeBatchAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk node 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> NodeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.NodeBatchAdminSession getNodeBatchAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchProxyManager.getNodeBatchAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk edge 
     *  administration service. 
     *
     *  @return an <code> EdgeBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.EdgeBatchAdminSession getEdgeBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchManager.getEdgeBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk edge 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.EdgeBatchAdminSession getEdgeBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchProxyManager.getEdgeBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk edge 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.EdgeBatchAdminSession getEdgeBatchAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchManager.getEdgeBatchAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk edge 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.EdgeBatchAdminSession getEdgeBatchAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchProxyManager.getEdgeBatchAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk graph 
     *  administration service. 
     *
     *  @return a <code> GraphBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.GraphBatchAdminSession getGraphBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchManager.getGraphBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk graph 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GraphBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.GraphBatchAdminSession getGraphBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.batch.TopologyBatchProxyManager.getGraphBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
