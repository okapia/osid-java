//
// AbstractMapRuleLookupSession
//
//    A simple framework for providing a Rule lookup service
//    backed by a fixed collection of rules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Rule lookup service backed by a
 *  fixed collection of rules. The rules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Rules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRuleLookupSession
    extends net.okapia.osid.jamocha.rules.spi.AbstractRuleLookupSession
    implements org.osid.rules.RuleLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.rules.Rule> rules = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.rules.Rule>());


    /**
     *  Makes a <code>Rule</code> available in this session.
     *
     *  @param  rule a rule
     *  @throws org.osid.NullArgumentException <code>rule<code>
     *          is <code>null</code>
     */

    protected void putRule(org.osid.rules.Rule rule) {
        this.rules.put(rule.getId(), rule);
        return;
    }


    /**
     *  Makes an array of rules available in this session.
     *
     *  @param  rules an array of rules
     *  @throws org.osid.NullArgumentException <code>rules<code>
     *          is <code>null</code>
     */

    protected void putRules(org.osid.rules.Rule[] rules) {
        putRules(java.util.Arrays.asList(rules));
        return;
    }


    /**
     *  Makes a collection of rules available in this session.
     *
     *  @param  rules a collection of rules
     *  @throws org.osid.NullArgumentException <code>rules<code>
     *          is <code>null</code>
     */

    protected void putRules(java.util.Collection<? extends org.osid.rules.Rule> rules) {
        for (org.osid.rules.Rule rule : rules) {
            this.rules.put(rule.getId(), rule);
        }

        return;
    }


    /**
     *  Removes a Rule from this session.
     *
     *  @param  ruleId the <code>Id</code> of the rule
     *  @throws org.osid.NullArgumentException <code>ruleId<code> is
     *          <code>null</code>
     */

    protected void removeRule(org.osid.id.Id ruleId) {
        this.rules.remove(ruleId);
        return;
    }


    /**
     *  Gets the <code>Rule</code> specified by its <code>Id</code>.
     *
     *  @param  ruleId <code>Id</code> of the <code>Rule</code>
     *  @return the rule
     *  @throws org.osid.NotFoundException <code>ruleId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>ruleId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Rule getRule(org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.Rule rule = this.rules.get(ruleId);
        if (rule == null) {
            throw new org.osid.NotFoundException("rule not found: " + ruleId);
        }

        return (rule);
    }


    /**
     *  Gets all <code>Rules</code>. In plenary mode, the returned
     *  list contains all known rules or an error
     *  results. Otherwise, the returned list may contain only those
     *  rules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Rules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.rule.ArrayRuleList(this.rules.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.rules.clear();
        super.close();
        return;
    }
}
