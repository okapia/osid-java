//
// AbstractRulesSession.java
//
//    Simple implementation framework for providing a Rules
//    service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session provides methods to evaluate and execute rules.
 */

public abstract class AbstractRulesSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.rules.RulesSession {

    private org.osid.rules.Engine engine;


    /**
     *  Gets the <code>Engine</code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Engine Id</code> associated with this session 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this session. 
     *
     *  @return the engine 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.engine);
    }


    /**
     *  Sets the <code>Engine</code>.
     *
     *  @param engine the engine for this session
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can evaluate rules. A return of true does
     *  not guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a <code>PERMISSION_DENIED</code>. This is intended
     *  as a hint to an application that may opt not to offer these
     *  operations.
     *
     *  @return <code>false</code> if evaluation methods are not
     *          authorized, <code>true</code> otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canEvaluateRules() {
        return (true);
    }


    /**
     *  Gets a condition for the given rule. 
     *
     *  @param  ruleId the <code>Id</code> of a <code>Rule</code> 
     *  @return the returned <code>Condition</code> 
     *  @throws org.osid.NotFoundException no <code>Rule</code> found
     *          with the given <code>Id</code>
     *  @throws org.osid.NullArgumentException <code>ruleId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Condition getConditionForRule(org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.nil.rules.condition.UnknownCondition());
    }


    /**
     *  Evaluates a rule based on an input condition. 
     *
     *  @param  ruleId a rule <code>Id</code> 
     *  @param  condition input conditions 
     *  @return result of the evaluation 
     *  @throws org.osid.NotFoundException an <code>Id</code> was not found 
     *  @throws org.osid.NullArgumentException <code>ruleId</code> or
     *          <code>condition</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code>condition</code>
     *          not of this service
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract boolean evaluateRule(org.osid.id.Id ruleId, 
                                         org.osid.rules.Condition condition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Executes a rule based on an input condition and returns a result. 
     *
     *  @param  ruleId a rule <code>Id</code> 
     *  @param  condition input conditions 
     *  @return result of the execution 
     *  @throws org.osid.NotFoundException an <code>Id</code> was not found 
     *  @throws org.osid.NullArgumentException <code>ruleId</code> or
     *          <code>condition</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code>condition</code>
     *          not of this service
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Result executeRule(org.osid.id.Id ruleId, 
                                             org.osid.rules.Condition condition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.builder.rules.result.ResultBuilder().
                booleanValue(evaluateRule(ruleId, condition)).
                build());
    }
}
