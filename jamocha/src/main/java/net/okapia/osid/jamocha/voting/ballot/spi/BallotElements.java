//
// BallotElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.ballot.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BallotElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the effective element Id.
     *
     *  @return the effective element Id
     */

    public static org.osid.id.Id getEffective() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getEffective());
    }


    /**
     *  Gets the start date element Id.
     *
     *  @return the start date element Id
     */

    public static org.osid.id.Id getStartDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getStartDate());
    }


    /**
     *  Gets the end date element Id.
     *
     *  @return the end date element Id
     */

    public static org.osid.id.Id getEndDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getEndDate());
    }


    /**
     *  Gets the date element Id.
     *
     *  @return the date element Id
     */

    public static org.osid.id.Id getDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getDate());
    }


    /**
     *  Gets the BallotElement Id.
     *
     *  @return the ballot element Id
     */

    public static org.osid.id.Id getBallotEntityId() {
        return (makeEntityId("osid.voting.Ballot"));
    }


    /**
     *  Gets the Revote element Id.
     *
     *  @return the Revote element Id
     */

    public static org.osid.id.Id getRevote() {
        return (makeQueryElementId("osid.voting.ballot.Revote"));
    }


    /**
     *  Gets the RaceId element Id.
     *
     *  @return the RaceId element Id
     */

    public static org.osid.id.Id getRaceId() {
        return (makeQueryElementId("osid.voting.ballot.RaceId"));
    }


    /**
     *  Gets the Race element Id.
     *
     *  @return the Race element Id
     */

    public static org.osid.id.Id getRace() {
        return (makeQueryElementId("osid.voting.ballot.Race"));
    }


    /**
     *  Gets the PollsId element Id.
     *
     *  @return the PollsId element Id
     */

    public static org.osid.id.Id getPollsId() {
        return (makeQueryElementId("osid.voting.ballot.PollsId"));
    }


    /**
     *  Gets the Polls element Id.
     *
     *  @return the Polls element Id
     */

    public static org.osid.id.Id getPolls() {
        return (makeQueryElementId("osid.voting.ballot.Polls"));
    }
}
