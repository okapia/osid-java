//
// MutableMapEdgeEnablerLookupSession
//
//    Implements an EdgeEnabler lookup service backed by a collection of
//    edgeEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.rules;


/**
 *  Implements an EdgeEnabler lookup service backed by a collection of
 *  edge enablers. The edge enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of edge enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapEdgeEnablerLookupSession
    extends net.okapia.osid.jamocha.core.topology.rules.spi.AbstractMapEdgeEnablerLookupSession
    implements org.osid.topology.rules.EdgeEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapEdgeEnablerLookupSession}
     *  with no edge enablers.
     *
     *  @param graph the graph
     *  @throws org.osid.NullArgumentException {@code graph} is
     *          {@code null}
     */

      public MutableMapEdgeEnablerLookupSession(org.osid.topology.Graph graph) {
        setGraph(graph);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapEdgeEnablerLookupSession} with a
     *  single edgeEnabler.
     *
     *  @param graph the graph  
     *  @param edgeEnabler an edge enabler
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code edgeEnabler} is {@code null}
     */

    public MutableMapEdgeEnablerLookupSession(org.osid.topology.Graph graph,
                                           org.osid.topology.rules.EdgeEnabler edgeEnabler) {
        this(graph);
        putEdgeEnabler(edgeEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapEdgeEnablerLookupSession}
     *  using an array of edge enablers.
     *
     *  @param graph the graph
     *  @param edgeEnablers an array of edge enablers
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code edgeEnablers} is {@code null}
     */

    public MutableMapEdgeEnablerLookupSession(org.osid.topology.Graph graph,
                                           org.osid.topology.rules.EdgeEnabler[] edgeEnablers) {
        this(graph);
        putEdgeEnablers(edgeEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapEdgeEnablerLookupSession}
     *  using a collection of edge enablers.
     *
     *  @param graph the graph
     *  @param edgeEnablers a collection of edge enablers
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code edgeEnablers} is {@code null}
     */

    public MutableMapEdgeEnablerLookupSession(org.osid.topology.Graph graph,
                                           java.util.Collection<? extends org.osid.topology.rules.EdgeEnabler> edgeEnablers) {

        this(graph);
        putEdgeEnablers(edgeEnablers);
        return;
    }

    
    /**
     *  Makes an {@code EdgeEnabler} available in this session.
     *
     *  @param edgeEnabler an edge enabler
     *  @throws org.osid.NullArgumentException {@code edgeEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putEdgeEnabler(org.osid.topology.rules.EdgeEnabler edgeEnabler) {
        super.putEdgeEnabler(edgeEnabler);
        return;
    }


    /**
     *  Makes an array of edge enablers available in this session.
     *
     *  @param edgeEnablers an array of edge enablers
     *  @throws org.osid.NullArgumentException {@code edgeEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putEdgeEnablers(org.osid.topology.rules.EdgeEnabler[] edgeEnablers) {
        super.putEdgeEnablers(edgeEnablers);
        return;
    }


    /**
     *  Makes collection of edge enablers available in this session.
     *
     *  @param edgeEnablers a collection of edge enablers
     *  @throws org.osid.NullArgumentException {@code edgeEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putEdgeEnablers(java.util.Collection<? extends org.osid.topology.rules.EdgeEnabler> edgeEnablers) {
        super.putEdgeEnablers(edgeEnablers);
        return;
    }


    /**
     *  Removes an EdgeEnabler from this session.
     *
     *  @param edgeEnablerId the {@code Id} of the edge enabler
     *  @throws org.osid.NullArgumentException {@code edgeEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeEdgeEnabler(org.osid.id.Id edgeEnablerId) {
        super.removeEdgeEnabler(edgeEnablerId);
        return;
    }    
}
