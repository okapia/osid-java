//
// AbstractSubscriptionEnablerSearch.java
//
//     A template for making a SubscriptionEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing subscription enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSubscriptionEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.subscription.rules.SubscriptionEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.subscription.rules.records.SubscriptionEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.subscription.rules.SubscriptionEnablerSearchOrder subscriptionEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of subscription enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  subscriptionEnablerIds list of subscription enablers
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSubscriptionEnablers(org.osid.id.IdList subscriptionEnablerIds) {
        while (subscriptionEnablerIds.hasNext()) {
            try {
                this.ids.add(subscriptionEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSubscriptionEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of subscription enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSubscriptionEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  subscriptionEnablerSearchOrder subscription enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>subscriptionEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSubscriptionEnablerResults(org.osid.subscription.rules.SubscriptionEnablerSearchOrder subscriptionEnablerSearchOrder) {
	this.subscriptionEnablerSearchOrder = subscriptionEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.subscription.rules.SubscriptionEnablerSearchOrder getSubscriptionEnablerSearchOrder() {
	return (this.subscriptionEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given subscription enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a subscription enabler implementing the requested record.
     *
     *  @param subscriptionEnablerSearchRecordType a subscription enabler search record
     *         type
     *  @return the subscription enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.rules.records.SubscriptionEnablerSearchRecord getSubscriptionEnablerSearchRecord(org.osid.type.Type subscriptionEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.subscription.rules.records.SubscriptionEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(subscriptionEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subscription enabler search. 
     *
     *  @param subscriptionEnablerSearchRecord subscription enabler search record
     *  @param subscriptionEnablerSearchRecordType subscriptionEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubscriptionEnablerSearchRecord(org.osid.subscription.rules.records.SubscriptionEnablerSearchRecord subscriptionEnablerSearchRecord, 
                                           org.osid.type.Type subscriptionEnablerSearchRecordType) {

        addRecordType(subscriptionEnablerSearchRecordType);
        this.records.add(subscriptionEnablerSearchRecord);        
        return;
    }
}
