//
// AbstractActivityBundleQuery.java
//
//     A template for making an ActivityBundle Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activitybundle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for activity bundles.
 */

public abstract class AbstractActivityBundleQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.course.registration.ActivityBundleQuery {

    private final java.util.Collection<org.osid.course.registration.records.ActivityBundleQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the course offering <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Clears the course offering terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activity 
     *  bundles that have a related course. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches activity bundles that have any activity. 
     *
     *  @param  match <code> true </code> to match activity bundles with any 
     *          activity, <code> false </code> to match activity bundles with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Matches activity bundles with credits between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchCredits(java.math.BigDecimal min, 
                             java.math.BigDecimal max, boolean match) {
        return;
    }


    /**
     *  Matches an activity bundle that has any credits assigned. 
     *
     *  @param  match <code> true </code> to match activity bundles with any 
     *          credits, <code> false </code> to match activity bundles with 
     *          no credits 
     */

    @OSID @Override
    public void matchAnyCredits(boolean match) {
        return;
    }


    /**
     *  Clears the credit terms. 
     */

    @OSID @Override
    public void clearCreditsTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches activity bundles that have any grading option. 
     *
     *  @param  match <code> true </code> to match activity bundles with any 
     *          grading option, <code> false </code> to match activity bundles 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  activity bundles assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given activity bundle query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity bundle implementing the requested record.
     *
     *  @param activityBundleRecordType an activity bundle record type
     *  @return the activity bundle query record
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityBundleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleQueryRecord getActivityBundleQueryRecord(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityBundleQueryRecord record : this.records) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityBundleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity bundle query. 
     *
     *  @param activityBundleQueryRecord activity bundle query record
     *  @param activityBundleRecordType activityBundle record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityBundleQueryRecord(org.osid.course.registration.records.ActivityBundleQueryRecord activityBundleQueryRecord, 
                                          org.osid.type.Type activityBundleRecordType) {

        addRecordType(activityBundleRecordType);
        nullarg(activityBundleQueryRecord, "activity bundle query record");
        this.records.add(activityBundleQueryRecord);        
        return;
    }
}
