//
// AbstractFederatingLogEntryLookupSession.java
//
//     An abstract federating adapter for a LogEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  LogEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingLogEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.tracking.LogEntryLookupSession>
    implements org.osid.tracking.LogEntryLookupSession {

    private boolean parallel = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();


    /**
     *  Constructs a new <code>AbstractFederatingLogEntryLookupSession</code>.
     */

    protected AbstractFederatingLogEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.tracking.LogEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this 
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>LogEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLogEntries() {
        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            if (session.canLookupLogEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>LogEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLogEntryView() {
        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            session.useComparativeLogEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>LogEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLogEntryView() {
        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            session.usePlenaryLogEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include log entries in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            session.useFederatedFrontOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            session.useIsolatedFrontOfficeView();
        }

        return;
    }

     
    /**
     *  Gets the <code>LogEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>LogEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>LogEntry</code> and
     *  retained for compatibility.
     *
     *  @param  logEntryId <code>Id</code> of the
     *          <code>LogEntry</code>
     *  @return the log entry
     *  @throws org.osid.NotFoundException <code>logEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>logEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntry getLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            try {
                return (session.getLogEntry(logEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(logEntryId + " not found");
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  logEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>LogEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  logEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByIds(org.osid.id.IdList logEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.tracking.logentry.MutableLogEntryList ret = new net.okapia.osid.jamocha.tracking.logentry.MutableLogEntryList();

        try (org.osid.id.IdList ids = logEntryIds) {
            while (ids.hasNext()) {
                ret.addLogEntry(getLogEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  log entry genus <code>Type</code> which does not include
     *  log entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known log
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesByGenusType(logEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  log entry genus <code>Type</code> and include any additional
     *  log entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known log
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByParentGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesByParentGenusType(logEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LogEntryList</code> containing the given
     *  log entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known log
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryRecordType a logEntry record type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByRecordType(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesByRecordType(logEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of log entries logged in the date range. In
     *  plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those log entries that are accessible
     *  through this session.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or <code>
     *          to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDate(org.osid.calendaring.DateTime from,
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LogEntryList</code> in the given
     *  <code>Queue.</code> In plenary mode, the returned list
     *  contains all of the log entries, or an error results if a log
     *  entry connected to the issue is not found or
     *  inaccessible. Otherwise, inaccessible <code>LogEntries</code>
     *  may be omitted from the list.
     *
     *  @param queueId a queue <code>Id</code>
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException <code>issueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesForQueue(queueId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of log entries logged in the date range in the
     *  given queue. In plenary mode, the returned list contains all
     *  known availabilities or an error results. Otherwise, the
     *  returned list may contain only those log entries that are
     *  accessible through this session.
     *
     *  @param  queueId a queue <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>queueId, from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDateForQueue(org.osid.id.Id queueId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesByDateForQueue(queueId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LogEntryList</code> in the given
     *  <code>Issue.</code> In plenary mode, the returned list
     *  contains all of the log entries, or an error results if a log
     *  entry connected to the issue is not found or
     *  inaccessible. Otherwise, inaccessible <code>LogEntries</code>
     *  may be omitted from the list.
     *
     *  @param issueId an issue <code>Id</code>
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException <code>issueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesForIssue(issueId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of log entries logged in the date range in the
     *  given issue. In plenary mode, the returned list contains all
     *  known availabilities or an error results. Otherwise, the
     *  returned list may contain only those log entries that are
     *  accessible through this session.
     *
     *  @param  issueId an issue <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>issueId, from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDateForIssue(org.osid.id.Id issueId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntriesByDateForIssue(issueId, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>LogEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>LogEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList ret = getLogEntryList();

        for (org.osid.tracking.LogEntryLookupSession session : getSessions()) {
            ret.addLogEntryList(session.getLogEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.tracking.logentry.FederatingLogEntryList getLogEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.logentry.ParallelLogEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.logentry.CompositeLogEntryList());
        }
    }
}
