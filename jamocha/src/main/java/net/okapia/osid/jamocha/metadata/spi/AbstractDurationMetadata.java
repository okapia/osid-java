//
// AbstractDurationMetadata.java
//
//     Defines a duration Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a duration Metadata.
 */

public abstract class AbstractDurationMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final Types types = new TypeSet();
    private final Types calendarTypes = new TypeSet();
    private final Types timeTypes = new TypeSet();

    private org.osid.calendaring.Duration minimum = net.okapia.osid.primordium.calendaring.GregorianUTCDuration.valueOf("0s");
    private org.osid.calendaring.Duration maximum = net.okapia.osid.primordium.calendaring.GregorianUTCDuration.valueOf("1 epoch");
    private org.osid.calendaring.DateTimeResolution resolution = org.osid.calendaring.DateTimeResolution.SECOND;

    private final java.util.Collection<org.osid.calendaring.Duration> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.Duration> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.Duration> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractDurationMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDurationMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.DURATION, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractDurationMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDurationMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.DURATION, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the smallest resolution of the date time value. 
     *
     *  @return the resolution 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeResolution getDateTimeResolution() {
        return (this.resolution);
    }


    /**
     *  Sets the resolution.
     *
     *  @param resolution the smallest resolution allowed
     *  @throws org.osid.NullARgumentException {@code resolution} is
     *          {@code null}
     */

    protected void setResolution(org.osid.calendaring.DateTimeResolution resolution) {
        nullarg(resolution, "resolution");
        this.resolution = resolution;
        return;
    }


    /**
     *  Gets the set of acceptable calendar types. 
     *
     *  @return the set of calendar types 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getCalendarTypes() {
        return (this.calendarTypes.toArray());
    }
        

    /**
     *  Tests if the given calendar type is supported. 
     *
     *  @param  calendarType a calendar Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarType(org.osid.type.Type calendarType) {
        return (this.calendarTypes.contains(calendarType));
    }


    /**
     *  Add support for a calendar type.
     *
     *  @param calendarType the type of calendar
     *  @throws org.osid.NullArgumentException {@code calendarType} is
     *          {@code null}
     */

    protected void addCalendarType(org.osid.type.Type calendarType) {
        this.calendarTypes.add(calendarType);
        return;
    }


    /**
     *  Gets the set of acceptable time types. 
     *
     *  @return a set of time types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code> or <code>DURATION</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getTimeTypes() {
        return (this.timeTypes.toArray());
    }


    /**
     *  Tests if the given time type is supported. 
     *
     *  @param  timeType a time Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code> or <code>DURATION</code>
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimeType(org.osid.type.Type timeType) {
        return (this.timeTypes.contains(timeType));
    }


    /**
     *  Add support for a time type.
     *
     *  @param timeType the type of time
     *  @throws org.osid.NullArgumentException {@code timeType} is
     *          {@code null}
     */

    protected void addTimeType(org.osid.type.Type timeType) {
        this.timeTypes.add(timeType);
        return;
    }


    /**
     *  Gets the minimum duration value. 
     *
     *  @return the minimum duration 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DURATION </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getMinimumDuration() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum duration value. 
     *
     *  @return the maximum duration 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DURATION 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getMaximumDuration() {
        return (this.maximum);
    }

    
    /**
     *  Sets the duration range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    protected void setDurationRange(org.osid.calendaring.Duration min, org.osid.calendaring.Duration max) {
        nullarg(min, "min");
        nullarg(max, "max");

        if (min.isGreater(max)) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.minimum = min;
        this.maximum = max;

        return;
    }


    /**
     *  Gets the set of acceptable duration values. 
     *
     *  @return a set of durations or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DURATION </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration[] getDurationSet() {
        return (this.set.toArray(new org.osid.calendaring.Duration[this.set.size()]));
    }

    
    /**
     *  Sets the duration set.
     *
     *  @param values a collection of accepted duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDurationSet(java.util.Collection<org.osid.calendaring.Duration> values) {
        this.set.clear();
        addToDurationSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the duration set.
     *
     *  @param values a collection of accepted duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToDurationSet(java.util.Collection<org.osid.calendaring.Duration> values) {
        nullarg(values, "duration set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the duration set.
     *
     *  @param value a duration value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addToDurationSet(org.osid.calendaring.Duration value) {
        nullarg(value, "duration value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the duration set.
     *
     *  @param value a duration value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeFromDurationSet(org.osid.calendaring.Duration value) {
        nullarg(value, "duration value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the duration set.
     */

    protected void clearDurationSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default duration values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default duration values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DURATION </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration[] getDefaultDurationValues() {
        return (this.defvals.toArray(new org.osid.calendaring.Duration[this.defvals.size()]));
    }


    /**
     *  Sets the default duration set.
     *
     *  @param values a collection of default duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        clearDefaultDurationValues();
        addDefaultDurationValues(values);
        return;
    }


    /**
     *  Adds a collection of default duration values.
     *
     *  @param values a collection of default duration values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        nullarg(values, "default duration values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultDurationValue(org.osid.calendaring.Duration value) {
        nullarg(value, "default duration value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultDurationValue(org.osid.calendaring.Duration value) {
        nullarg(value, "default duration value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default duration values.
     */

    protected void clearDefaultDurationValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing duration values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing duration values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          DURATION </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration[] getExistingDurationValues() {
        return (this.existing.toArray(new org.osid.calendaring.Duration[this.existing.size()]));
    }


    /**
     *  Sets the existing duration set.
     *
     *  @param values a collection of existing duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        clearExistingDurationValues();
        addExistingDurationValues(values);
        return;
    }


    /**
     *  Adds a collection of existing duration values.
     *
     *  @param values a collection of existing duration values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        nullarg(values, "existing duration values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingDurationValue(org.osid.calendaring.Duration value) {
        nullarg(value, "existing duration value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingDurationValue(org.osid.calendaring.Duration value) {
        nullarg(value, "existing duration value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing duration values.
     */

    protected void clearExistingDurationValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }        
}