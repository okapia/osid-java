//
// AbstractWarehouse.java
//
//     Defines a Warehouse builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.warehouse.spi;


/**
 *  Defines a <code>Warehouse</code> builder.
 */

public abstract class AbstractWarehouseBuilder<T extends AbstractWarehouseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inventory.warehouse.WarehouseMiter warehouse;


    /**
     *  Constructs a new <code>AbstractWarehouseBuilder</code>.
     *
     *  @param warehouse the warehouse to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractWarehouseBuilder(net.okapia.osid.jamocha.builder.inventory.warehouse.WarehouseMiter warehouse) {
        super(warehouse);
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Builds the warehouse.
     *
     *  @return the new warehouse
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inventory.Warehouse build() {
        (new net.okapia.osid.jamocha.builder.validator.inventory.warehouse.WarehouseValidator(getValidations())).validate(this.warehouse);
        return (new net.okapia.osid.jamocha.builder.inventory.warehouse.ImmutableWarehouse(this.warehouse));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the warehouse miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.inventory.warehouse.WarehouseMiter getMiter() {
        return (this.warehouse);
    }


    /**
     *  Adds a Warehouse record.
     *
     *  @param record a warehouse record
     *  @param recordType the type of warehouse record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inventory.records.WarehouseRecord record, org.osid.type.Type recordType) {
        getMiter().addWarehouseRecord(record, recordType);
        return (self());
    }
}       


