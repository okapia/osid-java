//
// AddressElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.address.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AddressElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the AddressElement Id.
     *
     *  @return the address element Id
     */

    public static org.osid.id.Id getAddressEntityId() {
        return (makeEntityId("osid.contact.Address"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.contact.address.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.contact.address.Resource"));
    }


    /**
     *  Gets the AddressText element Id.
     *
     *  @return the AddressText element Id
     */

    public static org.osid.id.Id getAddressText() {
        return (makeElementId("osid.contact.address.AddressText"));
    }


    /**
     *  Gets the ContactId element Id.
     *
     *  @return the ContactId element Id
     */

    public static org.osid.id.Id getContactId() {
        return (makeQueryElementId("osid.contact.address.ContactId"));
    }


    /**
     *  Gets the Contact element Id.
     *
     *  @return the Contact element Id
     */

    public static org.osid.id.Id getContact() {
        return (makeQueryElementId("osid.contact.address.Contact"));
    }


    /**
     *  Gets the AddressBookId element Id.
     *
     *  @return the AddressBookId element Id
     */

    public static org.osid.id.Id getAddressBookId() {
        return (makeQueryElementId("osid.contact.address.AddressBookId"));
    }


    /**
     *  Gets the AddressBook element Id.
     *
     *  @return the AddressBook element Id
     */

    public static org.osid.id.Id getAddressBook() {
        return (makeQueryElementId("osid.contact.address.AddressBook"));
    }
}
