//
// AbstractMapCanonicalUnitProcessorEnablerLookupSession
//
//    A simple framework for providing a CanonicalUnitProcessorEnabler lookup service
//    backed by a fixed collection of canonical unit processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CanonicalUnitProcessorEnabler lookup service backed by a
 *  fixed collection of canonical unit processor enablers. The canonical unit processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnitProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCanonicalUnitProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractCanonicalUnitProcessorEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.rules.CanonicalUnitProcessorEnabler>());


    /**
     *  Makes a <code>CanonicalUnitProcessorEnabler</code> available in this session.
     *
     *  @param  canonicalUnitProcessorEnabler a canonical unit processor enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitProcessorEnabler(org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler) {
        this.canonicalUnitProcessorEnablers.put(canonicalUnitProcessorEnabler.getId(), canonicalUnitProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of canonical unit processor enablers available in this session.
     *
     *  @param  canonicalUnitProcessorEnablers an array of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitProcessorEnablers(org.osid.offering.rules.CanonicalUnitProcessorEnabler[] canonicalUnitProcessorEnablers) {
        putCanonicalUnitProcessorEnablers(java.util.Arrays.asList(canonicalUnitProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of canonical unit processor enablers available in this session.
     *
     *  @param  canonicalUnitProcessorEnablers a collection of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putCanonicalUnitProcessorEnablers(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablers) {
        for (org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler : canonicalUnitProcessorEnablers) {
            this.canonicalUnitProcessorEnablers.put(canonicalUnitProcessorEnabler.getId(), canonicalUnitProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a CanonicalUnitProcessorEnabler from this session.
     *
     *  @param  canonicalUnitProcessorEnablerId the <code>Id</code> of the canonical unit processor enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId) {
        this.canonicalUnitProcessorEnablers.remove(canonicalUnitProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>CanonicalUnitProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  canonicalUnitProcessorEnablerId <code>Id</code> of the <code>CanonicalUnitProcessorEnabler</code>
     *  @return the canonicalUnitProcessorEnabler
     *  @throws org.osid.NotFoundException <code>canonicalUnitProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnabler getCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler = this.canonicalUnitProcessorEnablers.get(canonicalUnitProcessorEnablerId);
        if (canonicalUnitProcessorEnabler == null) {
            throw new org.osid.NotFoundException("canonicalUnitProcessorEnabler not found: " + canonicalUnitProcessorEnablerId);
        }

        return (canonicalUnitProcessorEnabler);
    }


    /**
     *  Gets all <code>CanonicalUnitProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known canonicalUnitProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonicalUnitProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CanonicalUnitProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessorenabler.ArrayCanonicalUnitProcessorEnablerList(this.canonicalUnitProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitProcessorEnablers.clear();
        super.close();
        return;
    }
}
