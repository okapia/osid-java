//
// UnknownResourceRelationship.java
//
//     Defines an unknown ResourceRelationship.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.resource.resourcerelationship;


/**
 *  Defines an unknown <code>ResourceRelationship</code>.
 */

public final class UnknownResourceRelationship
    extends net.okapia.osid.jamocha.nil.resource.resourcerelationship.spi.AbstractUnknownResourceRelationship
    implements org.osid.resource.ResourceRelationship {


    /**
     *  Constructs a new <code>UnknownResourceRelationship</code>.
     */

    public UnknownResourceRelationship() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownResourceRelationship</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownResourceRelationship(boolean optional) {
        super(optional);
        addResourceRelationshipRecord(new ResourceRelationshipRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown ResourceRelationship.
     *
     *  @return an unknown ResourceRelationship
     */

    public static org.osid.resource.ResourceRelationship create() {
        return (net.okapia.osid.jamocha.builder.validator.resource.resourcerelationship.ResourceRelationshipValidator.validateResourceRelationship(new UnknownResourceRelationship()));
    }


    public class ResourceRelationshipRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.resource.records.ResourceRelationshipRecord {

        
        protected ResourceRelationshipRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
