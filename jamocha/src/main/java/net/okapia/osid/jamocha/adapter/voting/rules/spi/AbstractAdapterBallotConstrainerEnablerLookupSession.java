//
// AbstractAdapterBallotConstrainerEnablerLookupSession.java
//
//    A BallotConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A BallotConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterBallotConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.rules.BallotConstrainerEnablerLookupSession {

    private final org.osid.voting.rules.BallotConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBallotConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBallotConstrainerEnablerLookupSession(org.osid.voting.rules.BallotConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code BallotConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBallotConstrainerEnablers() {
        return (this.session.canLookupBallotConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code BallotConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBallotConstrainerEnablerView() {
        this.session.useComparativeBallotConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code BallotConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBallotConstrainerEnablerView() {
        this.session.usePlenaryBallotConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainer enablers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active ballot constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBallotConstrainerEnablerView() {
        this.session.useActiveBallotConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive ballot constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotConstrainerEnablerView() {
        this.session.useAnyStatusBallotConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code BallotConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code BallotConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code BallotConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param ballotConstrainerEnablerId {@code Id} of the {@code BallotConstrainerEnabler}
     *  @return the ballot constrainer enabler
     *  @throws org.osid.NotFoundException {@code ballotConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnabler getBallotConstrainerEnabler(org.osid.id.Id ballotConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainerEnabler(ballotConstrainerEnablerId));
    }


    /**
     *  Gets a {@code BallotConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ballotConstrainerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code BallotConstrainerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param  ballotConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code BallotConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByIds(org.osid.id.IdList ballotConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainerEnablersByIds(ballotConstrainerEnablerIds));
    }


    /**
     *  Gets a {@code BallotConstrainerEnablerList} corresponding to the given
     *  ballot constrainer enabler genus {@code Type} which does not include
     *  ballot constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param  ballotConstrainerEnablerGenusType a ballotConstrainerEnabler genus type 
     *  @return the returned {@code BallotConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByGenusType(org.osid.type.Type ballotConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainerEnablersByGenusType(ballotConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code BallotConstrainerEnablerList} corresponding to the given
     *  ballot constrainer enabler genus {@code Type} and include any additional
     *  ballot constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param  ballotConstrainerEnablerGenusType a ballotConstrainerEnabler genus type 
     *  @return the returned {@code BallotConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByParentGenusType(org.osid.type.Type ballotConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainerEnablersByParentGenusType(ballotConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code BallotConstrainerEnablerList} containing the given
     *  ballot constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param  ballotConstrainerEnablerRecordType a ballotConstrainerEnabler record type 
     *  @return the returned {@code BallotConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code ballotConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainerEnablersByRecordType(ballotConstrainerEnablerRecordType));
    }


    /**
     *  Gets a {@code BallotConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code BallotConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code BallotConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code BallotConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getBallotConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code BallotConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainer enablers
     *  are returned.
     *
     *  @return a list of {@code BallotConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBallotConstrainerEnablers());
    }
}
