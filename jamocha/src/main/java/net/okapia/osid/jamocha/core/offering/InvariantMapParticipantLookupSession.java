//
// InvariantMapParticipantLookupSession
//
//    Implements a Participant lookup service backed by a fixed collection of
//    participants.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a Participant lookup service backed by a fixed
 *  collection of participants. The participants are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapParticipantLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractMapParticipantLookupSession
    implements org.osid.offering.ParticipantLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapParticipantLookupSession</code> with no
     *  participants.
     *  
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumnetException {@code catalogue} is
     *          {@code null}
     */

    public InvariantMapParticipantLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParticipantLookupSession</code> with a single
     *  participant.
     *  
     *  @param catalogue the catalogue
     *  @param participant a single participant
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code participant} is <code>null</code>
     */

      public InvariantMapParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                               org.osid.offering.Participant participant) {
        this(catalogue);
        putParticipant(participant);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParticipantLookupSession</code> using an array
     *  of participants.
     *  
     *  @param catalogue the catalogue
     *  @param participants an array of participants
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code participants} is <code>null</code>
     */

      public InvariantMapParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                               org.osid.offering.Participant[] participants) {
        this(catalogue);
        putParticipants(participants);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParticipantLookupSession</code> using a
     *  collection of participants.
     *
     *  @param catalogue the catalogue
     *  @param participants a collection of participants
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code participants} is <code>null</code>
     */

      public InvariantMapParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                               java.util.Collection<? extends org.osid.offering.Participant> participants) {
        this(catalogue);
        putParticipants(participants);
        return;
    }
}
