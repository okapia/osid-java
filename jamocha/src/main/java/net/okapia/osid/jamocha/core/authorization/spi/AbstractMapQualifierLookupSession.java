//
// AbstractMapQualifierLookupSession
//
//    A simple framework for providing a Qualifier lookup service
//    backed by a fixed collection of qualifiers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Qualifier lookup service backed by a
 *  fixed collection of qualifiers. The qualifiers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Qualifiers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapQualifierLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractQualifierLookupSession
    implements org.osid.authorization.QualifierLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authorization.Qualifier> qualifiers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authorization.Qualifier>());


    /**
     *  Makes a <code>Qualifier</code> available in this session.
     *
     *  @param  qualifier a qualifier
     *  @throws org.osid.NullArgumentException <code>qualifier<code>
     *          is <code>null</code>
     */

    protected void putQualifier(org.osid.authorization.Qualifier qualifier) {
        this.qualifiers.put(qualifier.getId(), qualifier);
        return;
    }


    /**
     *  Makes an array of qualifiers available in this session.
     *
     *  @param  qualifiers an array of qualifiers
     *  @throws org.osid.NullArgumentException <code>qualifiers<code>
     *          is <code>null</code>
     */

    protected void putQualifiers(org.osid.authorization.Qualifier[] qualifiers) {
        putQualifiers(java.util.Arrays.asList(qualifiers));
        return;
    }


    /**
     *  Makes a collection of qualifiers available in this session.
     *
     *  @param  qualifiers a collection of qualifiers
     *  @throws org.osid.NullArgumentException <code>qualifiers<code>
     *          is <code>null</code>
     */

    protected void putQualifiers(java.util.Collection<? extends org.osid.authorization.Qualifier> qualifiers) {
        for (org.osid.authorization.Qualifier qualifier : qualifiers) {
            this.qualifiers.put(qualifier.getId(), qualifier);
        }

        return;
    }


    /**
     *  Removes a Qualifier from this session.
     *
     *  @param  qualifierId the <code>Id</code> of the qualifier
     *  @throws org.osid.NullArgumentException <code>qualifierId<code> is
     *          <code>null</code>
     */

    protected void removeQualifier(org.osid.id.Id qualifierId) {
        this.qualifiers.remove(qualifierId);
        return;
    }


    /**
     *  Gets the <code>Qualifier</code> specified by its <code>Id</code>.
     *
     *  @param  qualifierId <code>Id</code> of the <code>Qualifier</code>
     *  @return the qualifier
     *  @throws org.osid.NotFoundException <code>qualifierId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>qualifierId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Qualifier getQualifier(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authorization.Qualifier qualifier = this.qualifiers.get(qualifierId);
        if (qualifier == null) {
            throw new org.osid.NotFoundException("qualifier not found: " + qualifierId);
        }

        return (qualifier);
    }


    /**
     *  Gets all <code>Qualifiers</code>. In plenary mode, the returned
     *  list contains all known qualifiers or an error
     *  results. Otherwise, the returned list may contain only those
     *  qualifiers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Qualifiers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.qualifier.ArrayQualifierList(this.qualifiers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.qualifiers.clear();
        super.close();
        return;
    }
}
