//
// AbstractPollsQueryInspector.java
//
//     A template for making a PollsQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.polls.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for pollses.
 */

public abstract class AbstractPollsQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.voting.PollsQueryInspector {

    private final java.util.Collection<org.osid.voting.records.PollsQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the candidate <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCandidateIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the candidate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.CandidateQueryInspector[] getCandidateTerms() {
        return (new org.osid.voting.CandidateQueryInspector[0]);
    }


    /**
     *  Gets the ancestor polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorPollsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getAncestorPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the descendant polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantPollsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getDescendantPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given polls query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a polls implementing the requested record.
     *
     *  @param pollsRecordType a polls record type
     *  @return the polls query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pollsRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsQueryInspectorRecord getPollsQueryInspectorRecord(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.PollsQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pollsRecordType + " is not supported");
    }


    /**
     *  Adds a record to this polls query. 
     *
     *  @param pollsQueryInspectorRecord polls query inspector
     *         record
     *  @param pollsRecordType polls record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPollsQueryInspectorRecord(org.osid.voting.records.PollsQueryInspectorRecord pollsQueryInspectorRecord, 
                                                   org.osid.type.Type pollsRecordType) {

        addRecordType(pollsRecordType);
        nullarg(pollsRecordType, "polls record type");
        this.records.add(pollsQueryInspectorRecord);        
        return;
    }
}
