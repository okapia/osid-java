//
// AbstractAdapterChecklistLookupSession.java
//
//    A Checklist lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Checklist lookup session adapter.
 */

public abstract class AbstractAdapterChecklistLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.checklist.ChecklistLookupSession {

    private final org.osid.checklist.ChecklistLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterChecklistLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterChecklistLookupSession(org.osid.checklist.ChecklistLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Checklist} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupChecklists() {
        return (this.session.canLookupChecklists());
    }


    /**
     *  A complete view of the {@code Checklist} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeChecklistView() {
        this.session.useComparativeChecklistView();
        return;
    }


    /**
     *  A complete view of the {@code Checklist} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryChecklistView() {
        this.session.usePlenaryChecklistView();
        return;
    }

     
    /**
     *  Gets the {@code Checklist} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Checklist} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Checklist} and
     *  retained for compatibility.
     *
     *  @param checklistId {@code Id} of the {@code Checklist}
     *  @return the checklist
     *  @throws org.osid.NotFoundException {@code checklistId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code checklistId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecklist(checklistId));
    }


    /**
     *  Gets a {@code ChecklistList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  checklists specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Checklists} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  checklistIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Checklist} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code checklistIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByIds(org.osid.id.IdList checklistIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecklistsByIds(checklistIds));
    }


    /**
     *  Gets a {@code ChecklistList} corresponding to the given
     *  checklist genus {@code Type} which does not include
     *  checklists of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned {@code Checklist} list
     *  @throws org.osid.NullArgumentException
     *          {@code checklistGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecklistsByGenusType(checklistGenusType));
    }


    /**
     *  Gets a {@code ChecklistList} corresponding to the given
     *  checklist genus {@code Type} and include any additional
     *  checklists with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned {@code Checklist} list
     *  @throws org.osid.NullArgumentException
     *          {@code checklistGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByParentGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecklistsByParentGenusType(checklistGenusType));
    }


    /**
     *  Gets a {@code ChecklistList} containing the given
     *  checklist record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistRecordType a checklist record type 
     *  @return the returned {@code Checklist} list
     *  @throws org.osid.NullArgumentException
     *          {@code checklistRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByRecordType(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecklistsByRecordType(checklistRecordType));
    }


    /**
     *  Gets a {@code ChecklistList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Checklist} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecklistsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Checklists}. 
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Checklists} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklists()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChecklists());
    }
}
