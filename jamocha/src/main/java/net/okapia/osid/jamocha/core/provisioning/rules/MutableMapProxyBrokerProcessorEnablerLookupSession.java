//
// MutableMapProxyBrokerProcessorEnablerLookupSession
//
//    Implements a BrokerProcessorEnabler lookup service backed by a collection of
//    brokerProcessorEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a BrokerProcessorEnabler lookup service backed by a collection of
 *  brokerProcessorEnablers. The brokerProcessorEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of broker processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBrokerProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapBrokerProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyBrokerProcessorEnablerLookupSession}
     *  with no broker processor enablers.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyBrokerProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBrokerProcessorEnablerLookupSession} with a
     *  single broker processor enabler.
     *
     *  @param distributor the distributor
     *  @param brokerProcessorEnabler a broker processor enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerProcessorEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBrokerProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler, org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putBrokerProcessorEnabler(brokerProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBrokerProcessorEnablerLookupSession} using an
     *  array of broker processor enablers.
     *
     *  @param distributor the distributor
     *  @param brokerProcessorEnablers an array of broker processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerProcessorEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBrokerProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                org.osid.provisioning.rules.BrokerProcessorEnabler[] brokerProcessorEnablers, org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putBrokerProcessorEnablers(brokerProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBrokerProcessorEnablerLookupSession} using a
     *  collection of broker processor enablers.
     *
     *  @param distributor the distributor
     *  @param brokerProcessorEnablers a collection of broker processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerProcessorEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBrokerProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                java.util.Collection<? extends org.osid.provisioning.rules.BrokerProcessorEnabler> brokerProcessorEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(distributor, proxy);
        setSessionProxy(proxy);
        putBrokerProcessorEnablers(brokerProcessorEnablers);
        return;
    }

    
    /**
     *  Makes a {@code BrokerProcessorEnabler} available in this session.
     *
     *  @param brokerProcessorEnabler an broker processor enabler
     *  @throws org.osid.NullArgumentException {@code brokerProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerProcessorEnabler(org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler) {
        super.putBrokerProcessorEnabler(brokerProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of brokerProcessorEnablers available in this session.
     *
     *  @param brokerProcessorEnablers an array of broker processor enablers
     *  @throws org.osid.NullArgumentException {@code brokerProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerProcessorEnablers(org.osid.provisioning.rules.BrokerProcessorEnabler[] brokerProcessorEnablers) {
        super.putBrokerProcessorEnablers(brokerProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of broker processor enablers available in this session.
     *
     *  @param brokerProcessorEnablers
     *  @throws org.osid.NullArgumentException {@code brokerProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerProcessorEnablers(java.util.Collection<? extends org.osid.provisioning.rules.BrokerProcessorEnabler> brokerProcessorEnablers) {
        super.putBrokerProcessorEnablers(brokerProcessorEnablers);
        return;
    }


    /**
     *  Removes a BrokerProcessorEnabler from this session.
     *
     *  @param brokerProcessorEnablerId the {@code Id} of the broker processor enabler
     *  @throws org.osid.NullArgumentException {@code brokerProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBrokerProcessorEnabler(org.osid.id.Id brokerProcessorEnablerId) {
        super.removeBrokerProcessorEnabler(brokerProcessorEnablerId);
        return;
    }    
}
