//
// AbstractIndexedMapMessageLookupSession.java
//
//    A simple framework for providing a Message lookup service
//    backed by a fixed collection of messages with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Message lookup service backed by a
 *  fixed collection of messages. The messages are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some messages may be compatible
 *  with more types than are indicated through these message
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Messages</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapMessageLookupSession
    extends AbstractMapMessageLookupSession
    implements org.osid.messaging.MessageLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.messaging.Message> messagesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.messaging.Message>());
    private final MultiMap<org.osid.type.Type, org.osid.messaging.Message> messagesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.messaging.Message>());


    /**
     *  Makes a <code>Message</code> available in this session.
     *
     *  @param  message a message
     *  @throws org.osid.NullArgumentException <code>message<code> is
     *          <code>null</code>
     */

    @Override
    protected void putMessage(org.osid.messaging.Message message) {
        super.putMessage(message);

        this.messagesByGenus.put(message.getGenusType(), message);
        
        try (org.osid.type.TypeList types = message.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.messagesByRecord.put(types.getNextType(), message);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a message from this session.
     *
     *  @param messageId the <code>Id</code> of the message
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeMessage(org.osid.id.Id messageId) {
        org.osid.messaging.Message message;
        try {
            message = getMessage(messageId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.messagesByGenus.remove(message.getGenusType());

        try (org.osid.type.TypeList types = message.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.messagesByRecord.remove(types.getNextType(), message);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeMessage(messageId);
        return;
    }


    /**
     *  Gets a <code>MessageList</code> corresponding to the given
     *  message genus <code>Type</code> which does not include
     *  messages of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known messages or an error results. Otherwise,
     *  the returned list may contain only those messages that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  messageGenusType a message genus type 
     *  @return the returned <code>Message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByGenusType(org.osid.type.Type messageGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.message.ArrayMessageList(this.messagesByGenus.get(messageGenusType)));
    }


    /**
     *  Gets a <code>MessageList</code> containing the given
     *  message record <code>Type</code>. In plenary mode, the
     *  returned list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  messageRecordType a message record type 
     *  @return the returned <code>message</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>messageRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByRecordType(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.message.ArrayMessageList(this.messagesByRecord.get(messageRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.messagesByGenus.clear();
        this.messagesByRecord.clear();

        super.close();

        return;
    }
}
