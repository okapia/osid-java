//
// AbstractShipmentQuery.java
//
//     A template for making a Shipment Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for shipments.
 */

public abstract class AbstractShipmentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.inventory.shipment.ShipmentQuery {

    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a source. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSourceQuery() {
        throw new org.osid.UnimplementedException("supportsSourceQuery() is false");
    }


    /**
     *  Matches shipments with any source. 
     *
     *  @param  match <code> true </code> to match shipments with any source, 
     *          <code> false </code> to match shipments with no source 
     */

    @OSID @Override
    public void matchAnySource(boolean match) {
        return;
    }


    /**
     *  Clears the source terms. 
     */

    @OSID @Override
    public void clearSourceTerms() {
        return;
    }


    /**
     *  Sets the order <code> Id </code> for this query. 
     *
     *  @param  orderId an order <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> orderId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOrderId(org.osid.id.Id orderId, boolean match) {
        return;
    }


    /**
     *  Clears the order <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrderIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OrderQuery </code> is available. 
     *
     *  @return <code> true </code> if an order query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an order. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return an odrer query 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuery getOrderQuery() {
        throw new org.osid.UnimplementedException("supportsOrderQuery() is false");
    }


    /**
     *  Matches shipments with any related order. 
     *
     *  @param  match <code> true </code> to match shipments with any related 
     *          order, <code> false </code> to match shipments with no order 
     */

    @OSID @Override
    public void matchAnyOrder(boolean match) {
        return;
    }


    /**
     *  Clears the order terms. 
     */

    @OSID @Override
    public void clearOrderTerms() {
        return;
    }


    /**
     *  Matches shipment dates within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches shipments with any date. 
     *
     *  @param  match <code> true </code> to match shipments with any date, 
     *          <code> false </code> to match shipments with no date 
     */

    @OSID @Override
    public void matchAnyDate(boolean match) {
        return;
    }


    /**
     *  Clears the shipment date. 
     */

    @OSID @Override
    public void clearDateTerms() {
        return;
    }


    /**
     *  Sets the entry <code> Id </code> for this query. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return an entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches shipments that have any entry. 
     *
     *  @param  match <code> true </code> to match shipments with any entry, 
     *          <code> false </code> to match shipments with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        return;
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match shipments 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given shipment query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a shipment implementing the requested record.
     *
     *  @param shipmentRecordType a shipment record type
     *  @return the shipment query record
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(shipmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentQueryRecord getShipmentQueryRecord(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.ShipmentQueryRecord record : this.records) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(shipmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this shipment query. 
     *
     *  @param shipmentQueryRecord shipment query record
     *  @param shipmentRecordType shipment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addShipmentQueryRecord(org.osid.inventory.shipment.records.ShipmentQueryRecord shipmentQueryRecord, 
                                          org.osid.type.Type shipmentRecordType) {

        addRecordType(shipmentRecordType);
        nullarg(shipmentQueryRecord, "shipment query record");
        this.records.add(shipmentQueryRecord);        
        return;
    }
}
