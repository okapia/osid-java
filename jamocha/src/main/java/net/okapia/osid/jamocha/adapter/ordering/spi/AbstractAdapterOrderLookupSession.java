//
// AbstractAdapterOrderLookupSession.java
//
//    An Order lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Order lookup session adapter.
 */

public abstract class AbstractAdapterOrderLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ordering.OrderLookupSession {

    private final org.osid.ordering.OrderLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOrderLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOrderLookupSession(org.osid.ordering.OrderLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Store/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Store Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the {@code Store} associated with this session.
     *
     *  @return the {@code Store} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform {@code Order} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOrders() {
        return (this.session.canLookupOrders());
    }


    /**
     *  A complete view of the {@code Order} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOrderView() {
        this.session.useComparativeOrderView();
        return;
    }


    /**
     *  A complete view of the {@code Order} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOrderView() {
        this.session.usePlenaryOrderView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include orders in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    
     
    /**
     *  Gets the {@code Order} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Order} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Order} and
     *  retained for compatibility.
     *
     *  @param orderId {@code Id} of the {@code Order}
     *  @return the order
     *  @throws org.osid.NotFoundException {@code orderId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code orderId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder(org.osid.id.Id orderId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrder(orderId));
    }


    /**
     *  Gets an {@code OrderList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  orders specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Orders} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  orderIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Order} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code orderIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByIds(org.osid.id.IdList orderIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersByIds(orderIds));
    }


    /**
     *  Gets an {@code OrderList} corresponding to the given
     *  order genus {@code Type} which does not include
     *  orders of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned {@code Order} list
     *  @throws org.osid.NullArgumentException
     *          {@code orderGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersByGenusType(orderGenusType));
    }


    /**
     *  Gets an {@code OrderList} corresponding to the given
     *  order genus {@code Type} and include any additional
     *  orders with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned {@code Order} list
     *  @throws org.osid.NullArgumentException
     *          {@code orderGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByParentGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersByParentGenusType(orderGenusType));
    }


    /**
     *  Gets an {@code OrderList} containing the given
     *  order record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderRecordType an order record type 
     *  @return the returned {@code Order} list
     *  @throws org.osid.NullArgumentException
     *          {@code orderRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByRecordType(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersByRecordType(orderRecordType));
    }


    /**
     *  Gets a list of all orders corresponding to a customer {@code
     *  Id}. In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list may
     *  contain only those entries that are accessible through this
     *  session.
     *
     *  @param  resourceId the {@code Id} of the customer 
     *  @return the returned {@code OrderList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersByCustomer(resourceId));
    }


    /**
     *  Gets a list of all orders corresponding to a date
     *  range. Entries are returned with a submitted date that falsl
     *  between the requested dates inclusive. In plenary mode, the
     *  returned list contains all known orders or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code OrderList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersByDate(from, to));
    }


    /**
     *  Gets a list of all orders corresponding to a customer {@code
     *  Id} and date range. Entries are returned with submit dates
     *  that fall between the requested dates inclusive. In plenary
     *  mode, the returned list contains all known orders or an error
     *  results.  Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  resourceId the {@code Id} of the customer 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code OrderList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomerAndDate(org.osid.id.Id resourceId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersByCustomerAndDate(resourceId, from, to));
    }


    /**
     *  Gets a list of all orders with an item for a product. Entries
     *  are returned with submit dates that fall between the requested
     *  dates inclusive. In plenary mode, the returned list contains
     *  all known orders or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  productId a product {@code Id} 
     *  @return the returned {@code OrderList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code productId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProduct(org.osid.id.Id productId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrdersForProduct(productId));
    }

    
    /**
     *  Gets a list of all orders with items for a product {@code Id}
     *  and date range. Entries are returned with submit dates that
     *  fall between the requested dates inclusive. In plenary mode,
     *  the returned list contains all known orders or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  productId a product {@code Id} 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code OrderList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code productId, from,} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProductAndDate(org.osid.id.Id productId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOrdersForProductAndDate(productId, from, to));
    }

    
    /**
     *  Gets all {@code Orders}. 
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Orders} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrders());
    }


    /**
     *  Gets all submitted and not closed orders. In plenary mode, the
     *  returned list contains all known entries or an error results.
     *  Otherwise, the returned list may contain only those entries
     *  that are accessible through this session.
     *
     *  @return a list of orders 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOpenOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOpenOrders());
    }


    /**
     *  Gets all closed orders. In plenary mode, the returned list
     *  contains all known entries or an error results. Otherwise, the
     *  returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @return a list of orders 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getClosedOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getClosedOrders());
    }
}
