//
// AbstractStepLookupSession.java
//
//    A starter implementation framework for providing a Step
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Step
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSteps(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractStepLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.StepLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    

    /**
     *  Gets the <code>Office/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this session.
     *
     *  @return the <code>Office</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>Step</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSteps() {
        return (true);
    }


    /**
     *  A complete view of the <code>Step</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Step</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include steps in offices which are children of this
     *  office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active steps are returned by methods in this session.
     */
     
    @OSID @Override
    public void useActiveStepView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive steps are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Step</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Step</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Step</code> and retained for
     *  compatibility.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param stepId <code>Id</code> of the <code>Step</code>
     *  @return the step
     *  @throws org.osid.NotFoundException <code>stepId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Step getStep(org.osid.id.Id stepId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.StepList steps = getSteps()) {
            while (steps.hasNext()) {
                org.osid.workflow.Step step = steps.getNextStep();
                if (step.getId().equals(stepId)) {
                    return (step);
                }
            }
        } 

        throw new org.osid.NotFoundException(stepId + " not found");
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the steps
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Steps</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSteps()</code>.
     *
     *  @param stepIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>stepIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByIds(org.osid.id.IdList stepIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.Step> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = stepIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getStep(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("step " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.step.LinkedStepList(ret));
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given step
     *  genus <code>Type</code> which does not include steps of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSteps()</code>.
     *
     *  @param stepGenusType a step genus type
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.step.StepGenusFilterList(getSteps(), stepGenusType));
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given step
     *  genus <code>Type</code> and include any additional steps with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSteps()</code>.
     *
     *  @param stepGenusType a step genus type
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByParentGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStepsByGenusType(stepGenusType));
    }


    /**
     *  Gets a <code>StepList</code> containing the given step record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSteps()</code>.
     *
     *  @param  stepRecordType a step record type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByRecordType(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.step.StepRecordFilterList(getSteps(), stepRecordType));
    }


    /**
     *  Gets a <code>StepList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param resourceId a resource <code>Id</code>
     *  @return the returned <code>Step</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.workflow.step.StepProviderFilterList(getSteps(), resourceId));
    }


    /**
     *  Gets a list of steps by process. 
     *  
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *  
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  processId a process <code>Id /code> 
     *  @return the returned <code>Step</code> list 
     *  @throws org.osid.NullArgumentException <code>processId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsForProcess(org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.step.StepFilterList(new ProcessFilter(processId), getSteps()));
    }


    /**
     *  Gets a list of steps for which the given state is valid.
     *  
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *  
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stateId a stateId <code> Id </code> 
     *  @return the returned <code> Step </code> list 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByState(org.osid.id.Id stateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.Step> ret = new java.util.ArrayList<>();

        try (org.osid.workflow.StepList steps = getSteps()) {
            while (steps.hasNext()) {
                org.osid.workflow.Step step = steps.getNextStep();
                try (org.osid.id.IdList ids = step.getInputStateIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(stateId)) {
                            ret.add(step);
                        }
                    }
                } 
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.step.LinkedStepList(ret));
    }


    /**
     *  Gets all <code>Steps</code>.
     *
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @return a list of <code>Steps</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.StepList getSteps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the step list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of steps
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.StepList filterStepsOnViews(org.osid.workflow.StepList list)
        throws org.osid.OperationFailedException {

        org.osid.workflow.StepList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.workflow.step.ActiveStepFilterList(ret);
        }

        return (ret);
    }


    public static class ProcessFilter
        implements net.okapia.osid.jamocha.inline.filter.workflow.step.StepFilter {
         
        private final org.osid.id.Id processId;
         
         
        /**
         *  Constructs a new <code>ProcessFilter</code>.
         *
         *  @param processId the process to filter
         *  @throws org.osid.NullArgumentException
         *          <code>processId</code> is <code>null</code>
         */
        
        public ProcessFilter(org.osid.id.Id processId) {
            nullarg(processId, "process Id");
            this.processId = processId;
            return;
        }

         
        /**
         *  Used by the StepFilterList to filter the workflow
         *  event list based on process.
         *
         *  @param step the step
         *  @return <code>true</code> to pass the step,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.workflow.Step step) {
            return (step.getProcessId().equals(this.processId));
        }
    }
}
