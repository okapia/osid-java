//
// AbstractMapProgramOfferingLookupSession
//
//    A simple framework for providing a ProgramOffering lookup service
//    backed by a fixed collection of program offerings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ProgramOffering lookup service backed by a
 *  fixed collection of program offerings. The program offerings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProgramOfferings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProgramOfferingLookupSession
    extends net.okapia.osid.jamocha.course.program.spi.AbstractProgramOfferingLookupSession
    implements org.osid.course.program.ProgramOfferingLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.program.ProgramOffering> programOfferings = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.program.ProgramOffering>());


    /**
     *  Makes a <code>ProgramOffering</code> available in this session.
     *
     *  @param  programOffering a program offering
     *  @throws org.osid.NullArgumentException <code>programOffering<code>
     *          is <code>null</code>
     */

    protected void putProgramOffering(org.osid.course.program.ProgramOffering programOffering) {
        this.programOfferings.put(programOffering.getId(), programOffering);
        return;
    }


    /**
     *  Makes an array of program offerings available in this session.
     *
     *  @param  programOfferings an array of program offerings
     *  @throws org.osid.NullArgumentException <code>programOfferings<code>
     *          is <code>null</code>
     */

    protected void putProgramOfferings(org.osid.course.program.ProgramOffering[] programOfferings) {
        putProgramOfferings(java.util.Arrays.asList(programOfferings));
        return;
    }


    /**
     *  Makes a collection of program offerings available in this session.
     *
     *  @param  programOfferings a collection of program offerings
     *  @throws org.osid.NullArgumentException <code>programOfferings<code>
     *          is <code>null</code>
     */

    protected void putProgramOfferings(java.util.Collection<? extends org.osid.course.program.ProgramOffering> programOfferings) {
        for (org.osid.course.program.ProgramOffering programOffering : programOfferings) {
            this.programOfferings.put(programOffering.getId(), programOffering);
        }

        return;
    }


    /**
     *  Removes a ProgramOffering from this session.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @throws org.osid.NullArgumentException <code>programOfferingId<code> is
     *          <code>null</code>
     */

    protected void removeProgramOffering(org.osid.id.Id programOfferingId) {
        this.programOfferings.remove(programOfferingId);
        return;
    }


    /**
     *  Gets the <code>ProgramOffering</code> specified by its <code>Id</code>.
     *
     *  @param  programOfferingId <code>Id</code> of the <code>ProgramOffering</code>
     *  @return the programOffering
     *  @throws org.osid.NotFoundException <code>programOfferingId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOffering getProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.program.ProgramOffering programOffering = this.programOfferings.get(programOfferingId);
        if (programOffering == null) {
            throw new org.osid.NotFoundException("programOffering not found: " + programOfferingId);
        }

        return (programOffering);
    }


    /**
     *  Gets all <code>ProgramOfferings</code>. In plenary mode, the returned
     *  list contains all known programOfferings or an error
     *  results. Otherwise, the returned list may contain only those
     *  programOfferings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ProgramOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.programoffering.ArrayProgramOfferingList(this.programOfferings.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.programOfferings.clear();
        super.close();
        return;
    }
}
