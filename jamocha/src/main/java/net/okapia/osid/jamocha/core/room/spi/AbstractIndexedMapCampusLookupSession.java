//
// AbstractIndexedMapCampusLookupSession.java
//
//    A simple framework for providing a Campus lookup service
//    backed by a fixed collection of campuses with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Campus lookup service backed by a
 *  fixed collection of campuses. The campuses are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some campuses may be compatible
 *  with more types than are indicated through these campus
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Campuses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCampusLookupSession
    extends AbstractMapCampusLookupSession
    implements org.osid.room.CampusLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.Campus> campusesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Campus>());
    private final MultiMap<org.osid.type.Type, org.osid.room.Campus> campusesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Campus>());


    /**
     *  Makes a <code>Campus</code> available in this session.
     *
     *  @param  campus a campus
     *  @throws org.osid.NullArgumentException <code>campus<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCampus(org.osid.room.Campus campus) {
        super.putCampus(campus);

        this.campusesByGenus.put(campus.getGenusType(), campus);
        
        try (org.osid.type.TypeList types = campus.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.campusesByRecord.put(types.getNextType(), campus);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a campus from this session.
     *
     *  @param campusId the <code>Id</code> of the campus
     *  @throws org.osid.NullArgumentException <code>campusId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCampus(org.osid.id.Id campusId) {
        org.osid.room.Campus campus;
        try {
            campus = getCampus(campusId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.campusesByGenus.remove(campus.getGenusType());

        try (org.osid.type.TypeList types = campus.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.campusesByRecord.remove(types.getNextType(), campus);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCampus(campusId);
        return;
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  campus genus <code>Type</code> which does not include
     *  campuses of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known campuses or an error results. Otherwise,
     *  the returned list may contain only those campuses that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.campus.ArrayCampusList(this.campusesByGenus.get(campusGenusType)));
    }


    /**
     *  Gets a <code>CampusList</code> containing the given
     *  campus record <code>Type</code>. In plenary mode, the
     *  returned list contains all known campuses or an error
     *  results. Otherwise, the returned list may contain only those
     *  campuses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  campusRecordType a campus record type 
     *  @return the returned <code>campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByRecordType(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.campus.ArrayCampusList(this.campusesByRecord.get(campusRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.campusesByGenus.clear();
        this.campusesByRecord.clear();

        super.close();

        return;
    }
}
