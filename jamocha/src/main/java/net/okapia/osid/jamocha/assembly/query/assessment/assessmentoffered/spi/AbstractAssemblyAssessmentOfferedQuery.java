//
// AbstractAssemblyAssessmentOfferedQuery.java
//
//     An AssessmentOfferedQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssessmentOfferedQuery that stores terms.
 */

public abstract class AbstractAssemblyAssessmentOfferedQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.assessment.AssessmentOfferedQuery,
               org.osid.assessment.AssessmentOfferedQueryInspector,
               org.osid.assessment.AssessmentOfferedSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAssessmentOfferedQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAssessmentOfferedQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssessmentColumn(), style);
        return;
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment search order. 
     *
     *  @return an assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the level grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getLevelIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears all level <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        getAssembler().clearTerms(getLevelIdColumn());
        return;
    }


    /**
     *  Gets the level <code> Id </code> query terms. 
     *
     *  @return the level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (getAssembler().getIdTerms(getLevelIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the level of 
     *  difficulty. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLevelColumn(), style);
        return;
    }


    /**
     *  Gets the LevelId column name.
     *
     * @return the column name
     */

    protected String getLevelIdColumn() {
        return ("level_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches an assessment offered that has any level assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any level, 
     *          <code> false </code> to match offerings with no levsls 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        getAssembler().addIdWildcardTerm(getLevelColumn(), match);
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        getAssembler().clearTerms(getLevelColumn());
        return;
    }


    /**
     *  Gets the level query terms. 
     *
     *  @return the level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }


    /**
     *  Gets the Level column name.
     *
     * @return the column name
     */

    protected String getLevelColumn() {
        return ("level");
    }


    /**
     *  Match sequential assessments. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchItemsSequential(boolean match) {
        getAssembler().addBooleanTerm(getItemsSequentialColumn(), match);
        return;
    }


    /**
     *  Clears all sequential terms. 
     */

    @OSID @Override
    public void clearItemsSequentialTerms() {
        getAssembler().clearTerms(getItemsSequentialColumn());
        return;
    }


    /**
     *  Gets the sequential query terms. 
     *
     *  @return the boolean terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getItemsSequentialTerms() {
        return (getAssembler().getBooleanTerms(getItemsSequentialColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the sequential 
     *  flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItemsSequential(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getItemsSequentialColumn(), style);
        return;
    }


    /**
     *  Gets the Sequential column name.
     *
     * @return the column name
     */

    protected String getItemsSequentialColumn() {
        return ("items_sequential");
    }


    /**
     *  Match shuffled item assessments. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchItemsShuffled(boolean match) {
        getAssembler().addBooleanTerm(getItemsShuffledColumn(), match);
        return;
    }


    /**
     *  Clears all shuffled terms. 
     */

    @OSID @Override
    public void clearItemsShuffledTerms() {
        getAssembler().clearTerms(getItemsShuffledColumn());
        return;
    }


    /**
     *  Gets the items shuffled query terms. 
     *
     *  @return the boolean terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getItemsShuffledTerms() {
        return (getAssembler().getBooleanTerms(getItemsShuffledColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  shuffle flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItemsShuffled(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getItemsShuffledColumn(), style);
        return;
    }


    /**
     *  Gets the ItemsShuffled column name.
     *
     *  @return the column name
     */

    protected String getItemsShuffledColumn() {
        return ("items_shuffled");
    }


    /**
     *  Matches assessments whose start time falls between the specified range 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchStartTime(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getStartTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches offerings that has any start time assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any start 
     *          time, <code> false </code> to match offerings with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyStartTime(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getStartTimeColumn(), match);
        return;
    }


    /**
     *  Clears all scheduled terms. 
     */

    @OSID @Override
    public void clearStartTimeTerms() {
        getAssembler().clearTerms(getStartTimeColumn());
        return;
    }


    /**
     *  Gets the start time query terms. 
     *
     *  @return the start time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getStartTimeTerms() {
        return (getAssembler().getDateTimeTerms(getStartTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  start time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartTimeColumn(), style);
        return;
    }


    /**
     *  Gets the StartTime column name.
     *
     * @return the column name
     */

    protected String getStartTimeColumn() {
        return ("start_time");
    }


    /**
     *  Matches assessments whose end time falls between the specified range 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDeadline(org.osid.calendaring.DateTime start, 
                              org.osid.calendaring.DateTime end, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDeadlineColumn(), start, end, match);
        return;
    }


    /**
     *  Matches offerings that have any deadline assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any 
     *          deadline, <code> false </code> to match offerings with no 
     *          deadline 
     */

    @OSID @Override
    public void matchAnyDeadline(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDeadlineColumn(), match);
        return;
    }


    /**
     *  Clears all deadline terms. 
     */

    @OSID @Override
    public void clearDeadlineTerms() {
        getAssembler().clearTerms(getDeadlineColumn());
        return;
    }


    /**
     *  Gets the deadline query terms. 
     *
     *  @return the deadline terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getDeadlineTerms() {
        return (getAssembler().getDateTimeTerms(getDeadlineColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  deadline. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDeadline(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDeadlineColumn(), style);
        return;
    }


    /**
     *  Gets the Deadline column name.
     *
     * @return the column name
     */

    protected String getDeadlineColumn() {
        return ("deadline");
    }


    /**
     *  Matches assessments whose duration falls between the specified range 
     *  inclusive. 
     *
     *  @param  low start range of duration 
     *  @param  high end range of duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        getAssembler().addDurationRangeTerm(getDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Matches offerings that have any duration assigned. 
     *
     *  @param  match <code> true </code> to match offerings with any 
     *          duration, <code> false </code> to match offerings with no 
     *          duration 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getDurationColumn(), match);
        return;
    }


    /**
     *  Clears all duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        getAssembler().clearTerms(getDurationColumn());
        return;
    }


    /**
     *  Gets the deadline query terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (getAssembler().getDurationTerms(getDurationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDurationColumn(), style);
        return;
    }


    /**
     *  Gets the Duration column name.
     *
     * @return the column name
     */

    protected String getDurationColumn() {
        return ("duration");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScoreSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getScoreSystemIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears all grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScoreSystemIdTerms() {
        getAssembler().clearTerms(getScoreSystemIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScoreSystemIdTerms() {
        return (getAssembler().getIdTerms(getScoreSystemIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for scores. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScoreSystem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScoreSystemColumn(), style);
        return;
    }


    /**
     *  Gets the ScoreSystemId column name.
     *
     * @return the column name
     */

    protected String getScoreSystemIdColumn() {
        return ("score_system_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getScoreSystemQuery() {
        throw new org.osid.UnimplementedException("supportsScoreSystemQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade system assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade 
     *          system, <code> false </code> to match assessments with no 
     *          grade system 
     */

    @OSID @Override
    public void matchAnyScoreSystem(boolean match) {
        getAssembler().addIdWildcardTerm(getScoreSystemColumn(), match);
        return;
    }


    /**
     *  Clears all grade system terms. 
     */

    @OSID @Override
    public void clearScoreSystemTerms() {
        getAssembler().clearTerms(getScoreSystemColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getScoreSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system search order is available. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade system search order. 
     *
     *  @return a grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getScoreSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScoreSystemSearchOrder() is false");
    }


    /**
     *  Gets the ScoreSystem column name.
     *
     * @return the column name
     */

    protected String getScoreSystemColumn() {
        return ("score_system");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getGradeSystemIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears all grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeSystemIdTerms() {
        getAssembler().clearTerms(getGradeSystemIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (getAssembler().getIdTerms(getGradeSystemIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for grades. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradeSystem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeSystemColumn(), style);
        return;
    }


    /**
     *  Gets the GradeSystemId column name.
     *
     * @return the column name
     */

    protected String getGradeSystemIdColumn() {
        return ("grade_system_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradeSystemQuery() {
        throw new org.osid.UnimplementedException("supportsGradeSystemQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade system assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade 
     *          system, <code> false </code> to match assessments with no 
     *          grade system 
     */

    @OSID @Override
    public void matchAnyGradeSystem(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeSystemColumn(), match);
        return;
    }


    /**
     *  Clears all grade system terms. 
     */

    @OSID @Override
    public void clearGradeSystemTerms() {
        getAssembler().clearTerms(getGradeSystemColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system search order is available. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade system search order. 
     *
     *  @return a grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGradeSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSystemSearchOrder() is false");
    }


    /**
     *  Gets the GradeSystem column name.
     *
     * @return the column name
     */

    protected String getGradeSystemColumn() {
        return ("grade_system");
    }


    /**
     *  Sets the rubric assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment offered <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRubricId(org.osid.id.Id assessmentOfferedId, 
                              boolean match) {
        getAssembler().addIdTerm(getRubricIdColumn(), assessmentOfferedId, match);
        return;
    }


    /**
     *  Clears all rubric assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRubricIdTerms() {
        getAssembler().clearTerms(getRubricIdColumn());
        return;
    }


    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRubricIdTerms() {
        return (getAssembler().getIdTerms(getRubricIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the rubric 
     *  assessment offered. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRubric(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRubricColumn(), style);
        return;
    }


    /**
     *  Gets the RubricId column name.
     *
     * @return the column name
     */

    protected String getRubricIdColumn() {
        return ("rubric_id");
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if a rubric assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rubric assessment. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> supportsRubricQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getRubricQuery() {
        throw new org.osid.UnimplementedException("supportsRubricQuery() is false");
    }


    /**
     *  Matches an assessment offered that has any rubric assessment assigned. 
     *
     *  @param  match <code> true </code> to match assessments offered with 
     *          any rubric, <code> false </code> to match assessments offered 
     *          with no rubric 
     */

    @OSID @Override
    public void matchAnyRubric(boolean match) {
        getAssembler().addIdWildcardTerm(getRubricColumn(), match);
        return;
    }


    /**
     *  Clears all rubric assessment terms. 
     */

    @OSID @Override
    public void clearRubricTerms() {
        getAssembler().clearTerms(getRubricColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getRubricTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Tests if an assessment offered search order is available. 
     *
     *  @return <code> true </code> if an assessment offered search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment offered search order. 
     *
     *  @return a rubric assessment offered search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRubricSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedSearchOrder getRubricSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRubricSearchOrder() is false");
    }


    /**
     *  Gets the Rubric column name.
     *
     * @return the column name
     */

    protected String getRubricColumn() {
        return ("rubric");
    }


    /**
     *  Sets the assessment taken <code> Id </code> for this query. 
     *
     *  @param  assessmentTakenId an assessment taken <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentTakenId(org.osid.id.Id assessmentTakenId, 
                                       boolean match) {
        getAssembler().addIdTerm(getAssessmentTakenIdColumn(), assessmentTakenId, match);
        return;
    }


    /**
     *  Clears all assessment taken <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenIdTerms() {
        getAssembler().clearTerms(getAssessmentTakenIdColumn());
        return;
    }


    /**
     *  Gets the assessment taken <code> Id </code> query terms. 
     *
     *  @return the assessment taken <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentTakenIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentTakenIdColumn()));
    }


    /**
     *  Gets the AssessmentTakenId column name.
     *
     * @return the column name
     */

    protected String getAssessmentTakenIdColumn() {
        return ("assessment_taken_id");
    }


    /**
     *  Tests if an <code> AssessmentTakenQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment taken query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment taken. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment taken query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuery getAssessmentTakenQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentTakenQuery() is false");
    }


    /**
     *  Matches offerings that have any taken assessment version. 
     *
     *  @param  match <code> true </code> to match offerings with any taken 
     *          assessment, <code> false </code> to match offerings with no 
     *          assessmen taken 
     */

    @OSID @Override
    public void matchAnyAssessmentTaken(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentTakenColumn(), match);
        return;
    }


    /**
     *  Clears all assessment taken terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenTerms() {
        getAssembler().clearTerms(getAssessmentTakenColumn());
        return;
    }


    /**
     *  Gets the assessment taken query terms. 
     *
     *  @return the assessment taken terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQueryInspector[] getAssessmentTakenTerms() {
        return (new org.osid.assessment.AssessmentTakenQueryInspector[0]);
    }


    /**
     *  Gets the AssessmentTaken column name.
     *
     * @return the column name
     */

    protected String getAssessmentTakenColumn() {
        return ("assessment_taken");
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        getAssembler().clearTerms(getBankIdColumn());
        return;
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (getAssembler().getIdTerms(getBankIdColumn()));
    }


    /**
     *  Gets the BankId column name.
     *
     * @return the column name
     */

    protected String getBankIdColumn() {
        return ("bank_id");
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        getAssembler().clearTerms(getBankColumn());
        return;
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the Bank column name.
     *
     * @return the column name
     */

    protected String getBankColumn() {
        return ("bank");
    }


    /**
     *  Tests if this assessmentOffered supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentOfferedRecordType an assessment offered record type 
     *  @return <code>true</code> if the assessmentOfferedRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentOfferedRecordType) {
        for (org.osid.assessment.records.AssessmentOfferedQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  assessmentOfferedRecordType the assessment offered record type 
     *  @return the assessment offered query record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentOfferedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedQueryRecord getAssessmentOfferedQueryRecord(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentOfferedQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  assessmentOfferedRecordType the assessment offered record type 
     *  @return the assessment offered query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentOfferedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord getAssessmentOfferedQueryInspectorRecord(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param assessmentOfferedRecordType the assessment offered record type
     *  @return the assessment offered search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentOfferedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedSearchOrderRecord getAssessmentOfferedSearchOrderRecord(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentOfferedSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this assessment offered. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentOfferedQueryRecord the assessment offered query record
     *  @param assessmentOfferedQueryInspectorRecord the assessment offered query inspector
     *         record
     *  @param assessmentOfferedSearchOrderRecord the assessment offered search order record
     *  @param assessmentOfferedRecordType assessment offered record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedQueryRecord</code>,
     *          <code>assessmentOfferedQueryInspectorRecord</code>,
     *          <code>assessmentOfferedSearchOrderRecord</code> or
     *          <code>assessmentOfferedRecordTypeassessmentOffered</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentOfferedRecords(org.osid.assessment.records.AssessmentOfferedQueryRecord assessmentOfferedQueryRecord, 
                                      org.osid.assessment.records.AssessmentOfferedQueryInspectorRecord assessmentOfferedQueryInspectorRecord, 
                                      org.osid.assessment.records.AssessmentOfferedSearchOrderRecord assessmentOfferedSearchOrderRecord, 
                                      org.osid.type.Type assessmentOfferedRecordType) {

        addRecordType(assessmentOfferedRecordType);

        nullarg(assessmentOfferedQueryRecord, "assessment offered query record");
        nullarg(assessmentOfferedQueryInspectorRecord, "assessment offered query inspector record");
        nullarg(assessmentOfferedSearchOrderRecord, "assessment offered search odrer record");

        this.queryRecords.add(assessmentOfferedQueryRecord);
        this.queryInspectorRecords.add(assessmentOfferedQueryInspectorRecord);
        this.searchOrderRecords.add(assessmentOfferedSearchOrderRecord);
        
        return;
    }
}
