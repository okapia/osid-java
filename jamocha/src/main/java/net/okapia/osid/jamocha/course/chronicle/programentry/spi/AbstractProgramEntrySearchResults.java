//
// AbstractProgramEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProgramEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.chronicle.ProgramEntrySearchResults {

    private org.osid.course.chronicle.ProgramEntryList programEntries;
    private final org.osid.course.chronicle.ProgramEntryQueryInspector inspector;
    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProgramEntrySearchResults.
     *
     *  @param programEntries the result set
     *  @param programEntryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>programEntries</code>
     *          or <code>programEntryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProgramEntrySearchResults(org.osid.course.chronicle.ProgramEntryList programEntries,
                                            org.osid.course.chronicle.ProgramEntryQueryInspector programEntryQueryInspector) {
        nullarg(programEntries, "program entries");
        nullarg(programEntryQueryInspector, "program entry query inspectpr");

        this.programEntries = programEntries;
        this.inspector = programEntryQueryInspector;

        return;
    }


    /**
     *  Gets the program entry list resulting from a search.
     *
     *  @return a program entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntries() {
        if (this.programEntries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.chronicle.ProgramEntryList programEntries = this.programEntries;
        this.programEntries = null;
	return (programEntries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.chronicle.ProgramEntryQueryInspector getProgramEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  program entry search record <code> Type. </code> This method must
     *  be used to retrieve a programEntry implementing the requested
     *  record.
     *
     *  @param programEntrySearchRecordType a programEntry search 
     *         record type 
     *  @return the program entry search
     *  @throws org.osid.NullArgumentException
     *          <code>programEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(programEntrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntrySearchResultsRecord getProgramEntrySearchResultsRecord(org.osid.type.Type programEntrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.chronicle.records.ProgramEntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(programEntrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(programEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record program entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProgramEntryRecord(org.osid.course.chronicle.records.ProgramEntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "program entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
