//
// DeviceEnablerValidator.java
//
//     Validates a DeviceEnabler.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.control.rules.deviceenabler;


/**
 *  Validates a DeviceEnabler.
 */

public final class DeviceEnablerValidator
    extends net.okapia.osid.jamocha.builder.validator.control.rules.deviceenabler.spi.AbstractDeviceEnablerValidator {


    /**
     *  Constructs a new <code>DeviceEnablerValidator</code>.
     */

    public DeviceEnablerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>DeviceEnablerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public DeviceEnablerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a DeviceEnabler with a default validation.
     *
     *  @param deviceEnabler a device enabler to validate
     *  @return the device enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>deviceEnabler</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.control.rules.DeviceEnabler validateDeviceEnabler(org.osid.control.rules.DeviceEnabler deviceEnabler) {
        DeviceEnablerValidator validator = new DeviceEnablerValidator();
        validator.validate(deviceEnabler);
        return (deviceEnabler);
    }


    /**
     *  Validates a DeviceEnabler for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param deviceEnabler a device enabler to validate
     *  @return the device enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>deviceEnabler</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.control.rules.DeviceEnabler validateDeviceEnabler(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.control.rules.DeviceEnabler deviceEnabler) {

        DeviceEnablerValidator validator = new DeviceEnablerValidator(validation);
        validator.validate(deviceEnabler);
        return (deviceEnabler);
    }
}
