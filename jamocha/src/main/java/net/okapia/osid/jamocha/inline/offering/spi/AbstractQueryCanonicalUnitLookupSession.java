//
// AbstractQueryCanonicalUnitLookupSession.java
//
//    An inline adapter that maps a CanonicalUnitLookupSession to
//    a CanonicalUnitQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CanonicalUnitLookupSession to
 *  a CanonicalUnitQuerySession.
 */

public abstract class AbstractQueryCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractCanonicalUnitLookupSession
    implements org.osid.offering.CanonicalUnitLookupSession {

    private boolean activeonly    = false;
    private final org.osid.offering.CanonicalUnitQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCanonicalUnitLookupSession.
     *
     *  @param querySession the underlying canonical unit query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCanonicalUnitLookupSession(org.osid.offering.CanonicalUnitQuerySession querySession) {
        nullarg(querySession, "canonical unit query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnit</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnits() {
        return (this.session.canSearchCanonicalUnits());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical units in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active canonical units are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive canonical units are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CanonicalUnit</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnit</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CanonicalUnit</code> and
     *  retained for compatibility.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitId <code>Id</code> of the
     *          <code>CanonicalUnit</code>
     *  @return the canonical unit
     *  @throws org.osid.NotFoundException <code>canonicalUnitId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnit getCanonicalUnit(org.osid.id.Id canonicalUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CanonicalUnitQuery query = getQuery();
        query.matchId(canonicalUnitId, true);
        org.osid.offering.CanonicalUnitList canonicalUnits = this.session.getCanonicalUnitsByQuery(query);
        if (canonicalUnits.hasNext()) {
            return (canonicalUnits.getNextCanonicalUnit());
        } 
        
        throw new org.osid.NotFoundException(canonicalUnitId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnits specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CanonicalUnits</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByIds(org.osid.id.IdList canonicalUnitIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CanonicalUnitQuery query = getQuery();

        try (org.osid.id.IdList ids = canonicalUnitIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCanonicalUnitsByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  canonical unit genus <code>Type</code> which does not include
     *  canonical units of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CanonicalUnitQuery query = getQuery();
        query.matchGenusType(canonicalUnitGenusType, true);
        return (this.session.getCanonicalUnitsByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  canonical unit genus <code>Type</code> and include any additional
     *  canonical units with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByParentGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CanonicalUnitQuery query = getQuery();
        query.matchParentGenusType(canonicalUnitGenusType, true);
        return (this.session.getCanonicalUnitsByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> containing the given
     *  canonical unit record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitRecordType a canonicalUnit record type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByRecordType(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.CanonicalUnitQuery query = getQuery();
        query.matchRecordType(canonicalUnitRecordType, true);
        return (this.session.getCanonicalUnitsByQuery(query));
    }

    
    /**
     *  Gets all <code>CanonicalUnits</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @return a list of <code>CanonicalUnits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.CanonicalUnitQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCanonicalUnitsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.CanonicalUnitQuery getQuery() {
        org.osid.offering.CanonicalUnitQuery query = this.session.getCanonicalUnitQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
