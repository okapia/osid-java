//
// AbstractAssemblyProcessQuery.java
//
//     A ProcessQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProcessQuery that stores terms.
 */

public abstract class AbstractAssemblyProcessQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.workflow.ProcessQuery,
               org.osid.workflow.ProcessQueryInspector,
               org.osid.workflow.ProcessSearchOrder {

    private final java.util.Collection<org.osid.workflow.records.ProcessQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.ProcessQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.ProcessSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProcessQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProcessQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enabled processes. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        getAssembler().addBooleanTerm(getEnabledColumn(), match);
        return;
    }


    /**
     *  Clears the enabled query terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        getAssembler().clearTerms(getEnabledColumn());
        return;
    }


    /**
     *  Gets the enabled query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (getAssembler().getBooleanTerms(getEnabledColumn()));
    }


    /**
     *  Orders the results by enabled. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnabled(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEnabledColumn(), style);
        return;
    }


    /**
     *  Gets the Enabled column name.
     *
     * @return the column name
     */

    protected String getEnabledColumn() {
        return ("enabled");
    }


    /**
     *  Sets the initial step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchInitialStepId(org.osid.id.Id stepId, boolean match) {
        getAssembler().addIdTerm(getInitialStepIdColumn(), stepId, match);
        return;
    }


    /**
     *  Clears the initial stap <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInitialStepIdTerms() {
        getAssembler().clearTerms(getInitialStepIdColumn());
        return;
    }


    /**
     *  Gets the initial step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInitialStepIdTerms() {
        return (getAssembler().getIdTerms(getInitialStepIdColumn()));
    }


    /**
     *  Orders the results by initial step. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInitialStep(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInitialStepColumn(), style);
        return;
    }


    /**
     *  Gets the InitialStepId column name.
     *
     * @return the column name
     */

    protected String getInitialStepIdColumn() {
        return ("initial_step_id");
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInitialStepQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getInitialStepQuery() {
        throw new org.osid.UnimplementedException("supportsInitialStepQuery() is false");
    }


    /**
     *  Clears the initial step terms. 
     */

    @OSID @Override
    public void clearInitialStepTerms() {
        getAssembler().clearTerms(getInitialStepColumn());
        return;
    }


    /**
     *  Gets the initial step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getInitialStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Tests if an initial step search order is available. 
     *
     *  @return <code> true </code> if a step search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStepSearchOrder() {
        return (false);
    }


    /**
     *  Gets the initial step search order. 
     *
     *  @return the step search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInitialStepSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchOrder getInitialStepSearchOrder() {
        throw new org.osid.UnimplementedException("supportsInitialStepSearchOrder() is false");
    }


    /**
     *  Gets the InitialStep column name.
     *
     * @return the column name
     */

    protected String getInitialStepColumn() {
        return ("initial_step");
    }


    /**
     *  Sets the initial state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInitialStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getInitialStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the initial state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInitialStateIdTerms() {
        getAssembler().clearTerms(getInitialStateIdColumn());
        return;
    }


    /**
     *  Gets the initial state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInitialStateIdTerms() {
        return (getAssembler().getIdTerms(getInitialStateIdColumn()));
    }


    /**
     *  Orders the results by initial state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInitialState(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInitialStateColumn(), style);
        return;
    }


    /**
     *  Gets the InitialStateId column name.
     *
     * @return the column name
     */

    protected String getInitialStateIdColumn() {
        return ("initial_state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInitialStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getInitialStateQuery() {
        throw new org.osid.UnimplementedException("supportsInitialStateQuery() is false");
    }


    /**
     *  Clears the state terms. 
     */

    @OSID @Override
    public void clearInitialStateTerms() {
        getAssembler().clearTerms(getInitialStateColumn());
        return;
    }


    /**
     *  Gets the initial state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getInitialStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Tests if an initial state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the initial state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInitialStateSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getInitialStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsInitialStateSearchOrder() is false");
    }


    /**
     *  Gets the InitialState column name.
     *
     * @return the column name
     */

    protected String getInitialStateColumn() {
        return ("initial_state");
    }


    /**
     *  Sets the step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStepId(org.osid.id.Id stepId, boolean match) {
        getAssembler().addIdTerm(getStepIdColumn(), stepId, match);
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStepIdTerms() {
        getAssembler().clearTerms(getStepIdColumn());
        return;
    }


    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStepIdTerms() {
        return (getAssembler().getIdTerms(getStepIdColumn()));
    }


    /**
     *  Gets the StepId column name.
     *
     * @return the column name
     */

    protected String getStepIdColumn() {
        return ("step_id");
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> supportsStepyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getStepQuery() {
        throw new org.osid.UnimplementedException("supportsStepQuery() is false");
    }


    /**
     *  Matches processes that have any step. 
     *
     *  @param  match <code> true </code> to match processes with any step, 
     *          <code> false </code> to match processes with no step 
     */

    @OSID @Override
    public void matchAnyStep(boolean match) {
        getAssembler().addIdWildcardTerm(getStepColumn(), match);
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearStepTerms() {
        getAssembler().clearTerms(getStepColumn());
        return;
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the Step column name.
     *
     * @return the column name
     */

    protected String getStepColumn() {
        return ("step");
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        getAssembler().addIdTerm(getWorkIdColumn(), workId, match);
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        getAssembler().clearTerms(getWorkIdColumn());
        return;
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (getAssembler().getIdTerms(getWorkIdColumn()));
    }


    /**
     *  Gets the WorkId column name.
     *
     * @return the column name
     */

    protected String getWorkIdColumn() {
        return ("work_id");
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches processs that have any work. 
     *
     *  @param  match <code> true </code> to match processes with any work, 
     *          <code> false </code> to match processes with no work 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        getAssembler().addIdWildcardTerm(getWorkColumn(), match);
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        getAssembler().clearTerms(getWorkColumn());
        return;
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.workflow.WorkQueryInspector[0]);
    }


    /**
     *  Gets the Work column name.
     *
     * @return the column name
     */

    protected String getWorkColumn() {
        return ("work");
    }


    /**
     *  Sets the office <code> Id </code> for this query to match process 
     *  assigned to offices. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        getAssembler().clearTerms(getOfficeIdColumn());
        return;
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (getAssembler().getIdTerms(getOfficeIdColumn()));
    }


    /**
     *  Gets the OfficeId column name.
     *
     * @return the column name
     */

    protected String getOfficeIdColumn() {
        return ("office_id");
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        getAssembler().clearTerms(getOfficeColumn());
        return;
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the Office column name.
     *
     * @return the column name
     */

    protected String getOfficeColumn() {
        return ("office");
    }


    /**
     *  Tests if this process supports the given record
     *  <code>Type</code>.
     *
     *  @param  processRecordType a process record type 
     *  @return <code>true</code> if the processRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type processRecordType) {
        for (org.osid.workflow.records.ProcessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  processRecordType the process record type 
     *  @return the process query record 
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.ProcessQueryRecord getProcessQueryRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.ProcessQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  processRecordType the process record type 
     *  @return the process query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.ProcessQueryInspectorRecord getProcessQueryInspectorRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.ProcessQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param processRecordType the process record type
     *  @return the process search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.ProcessSearchOrderRecord getProcessSearchOrderRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.ProcessSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this process. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param processQueryRecord the process query record
     *  @param processQueryInspectorRecord the process query inspector
     *         record
     *  @param processSearchOrderRecord the process search order record
     *  @param processRecordType process record type
     *  @throws org.osid.NullArgumentException
     *          <code>processQueryRecord</code>,
     *          <code>processQueryInspectorRecord</code>,
     *          <code>processSearchOrderRecord</code> or
     *          <code>processRecordTypeprocess</code> is
     *          <code>null</code>
     */
            
    protected void addProcessRecords(org.osid.workflow.records.ProcessQueryRecord processQueryRecord, 
                                      org.osid.workflow.records.ProcessQueryInspectorRecord processQueryInspectorRecord, 
                                      org.osid.workflow.records.ProcessSearchOrderRecord processSearchOrderRecord, 
                                      org.osid.type.Type processRecordType) {

        addRecordType(processRecordType);

        nullarg(processQueryRecord, "process query record");
        nullarg(processQueryInspectorRecord, "process query inspector record");
        nullarg(processSearchOrderRecord, "process search odrer record");

        this.queryRecords.add(processQueryRecord);
        this.queryInspectorRecords.add(processQueryInspectorRecord);
        this.searchOrderRecords.add(processSearchOrderRecord);
        
        return;
    }
}
