//
// RouteSegmentMiter.java
//
//     Defines a RouteSegment miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.routesegment;


/**
 *  Defines a <code>RouteSegment</code> miter for use with the builders.
 */

public interface RouteSegmentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.mapping.route.RouteSegment {


    /**
     *  Sets the starting instructions.
     *
     *  @param instructions the starting instructions
     *  @throws org.osid.NullArgumentException
     *          <code>instructions</code> is <code>null</code>
     */

    public void setStartingInstructions(org.osid.locale.DisplayText instructions);


    /**
     *  Sets the ending instructions.
     *
     *  @param instructions the ending instructions
     *  @throws org.osid.NullArgumentException
     *          <code>instructions</code> is <code>null</code>
     */

    public void setEndingInstructions(org.osid.locale.DisplayText instructions);


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public void setDistance(org.osid.mapping.Distance distance);


    /**
     *  Sets the ETA.
     *
     *  @param eta the estimated time of arrival
     *  @throws org.osid.NullArgumentException <code>eta</code> is
     *          <code>null</code>
     */

    public void setETA(org.osid.calendaring.Duration eta);


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public void setPath(org.osid.mapping.path.Path path);


    /**
     *  Adds a RouteSegment record.
     *
     *  @param record a routeSegment record
     *  @param recordType the type of routeSegment record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRouteSegmentRecord(org.osid.mapping.route.records.RouteSegmentRecord record, org.osid.type.Type recordType);
}       


