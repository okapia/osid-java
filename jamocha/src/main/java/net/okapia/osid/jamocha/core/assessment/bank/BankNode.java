//
// BankNode.java
//
//     Defines a Bank node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  A class for managing a hierarchy of bank nodes in core.
 */

public final class BankNode
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractBankNode
    implements org.osid.assessment.BankNode {


    /**
     *  Constructs a new <code>BankNode</code> from a single
     *  bank.
     *
     *  @param bank the bank
     *  @throws org.osid.NullArgumentException <code>bank</code> is 
     *          <code>null</code>.
     */

    public BankNode(org.osid.assessment.Bank bank) {
        super(bank);
        return;
    }


    /**
     *  Constructs a new <code>BankNode</code>.
     *
     *  @param bank the bank
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>.
     */

    public BankNode(org.osid.assessment.Bank bank, boolean root, boolean leaf) {
        super(bank, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this bank.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.assessment.BankNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this bank.
     *
     *  @param bank the bank to add as a parent
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.assessment.Bank bank) {
        addParent(new BankNode(bank));
        return;
    }


    /**
     *  Adds a child to this bank.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.assessment.BankNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this bank.
     *
     *  @param bank the bank to add as a child
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.assessment.Bank bank) {
        addChild(new BankNode(bank));
        return;
    }
}
