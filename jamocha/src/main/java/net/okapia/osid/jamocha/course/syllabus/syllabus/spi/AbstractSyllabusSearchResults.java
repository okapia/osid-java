//
// AbstractSyllabusSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSyllabusSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.syllabus.SyllabusSearchResults {

    private org.osid.course.syllabus.SyllabusList syllabi;
    private final org.osid.course.syllabus.SyllabusQueryInspector inspector;
    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSyllabusSearchResults.
     *
     *  @param syllabi the result set
     *  @param syllabusQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>syllabi</code>
     *          or <code>syllabusQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSyllabusSearchResults(org.osid.course.syllabus.SyllabusList syllabi,
                                            org.osid.course.syllabus.SyllabusQueryInspector syllabusQueryInspector) {
        nullarg(syllabi, "syllabi");
        nullarg(syllabusQueryInspector, "syllabus query inspectpr");

        this.syllabi = syllabi;
        this.inspector = syllabusQueryInspector;

        return;
    }


    /**
     *  Gets the syllabus list resulting from a search.
     *
     *  @return a syllabus list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabi() {
        if (this.syllabi == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.syllabus.SyllabusList syllabi = this.syllabi;
        this.syllabi = null;
	return (syllabi);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.syllabus.SyllabusQueryInspector getSyllabusQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  syllabus search record <code> Type. </code> This method must
     *  be used to retrieve a syllabus implementing the requested
     *  record.
     *
     *  @param syllabusSearchRecordType a syllabus search 
     *         record type 
     *  @return the syllabus search
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(syllabusSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusSearchResultsRecord getSyllabusSearchResultsRecord(org.osid.type.Type syllabusSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.syllabus.records.SyllabusSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(syllabusSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(syllabusSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record syllabus search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSyllabusRecord(org.osid.course.syllabus.records.SyllabusSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "syllabus record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
