//
// AbstractTrigger.java
//
//     Defines a Trigger builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.trigger.spi;


/**
 *  Defines a <code>Trigger</code> builder.
 */

public abstract class AbstractTriggerBuilder<T extends AbstractTriggerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.control.trigger.TriggerMiter trigger;


    /**
     *  Constructs a new <code>AbstractTriggerBuilder</code>.
     *
     *  @param trigger the trigger to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractTriggerBuilder(net.okapia.osid.jamocha.builder.control.trigger.TriggerMiter trigger) {
        super(trigger);
        this.trigger = trigger;
        return;
    }


    /**
     *  Builds the trigger.
     *
     *  @return the new trigger
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>trigger</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.control.Trigger build() {
        (new net.okapia.osid.jamocha.builder.validator.control.trigger.TriggerValidator(getValidations())).validate(this.trigger);
        return (new net.okapia.osid.jamocha.builder.control.trigger.ImmutableTrigger(this.trigger));
    }


    /**
     *  Gets the trigger. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new trigger
     */

    @Override
    public net.okapia.osid.jamocha.builder.control.trigger.TriggerMiter getMiter() {
        return (this.trigger);
    }


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>controller</code> is <code>null</code>
     */

    public T controller(org.osid.control.Controller controller) {
        getMiter().setController(controller);
        return (self());
    }


    /**
     *  Listen to on events.
     *
     *  @param listen {@code true} to listen to on events, {@code
     *         false} otherwise
     */

    public T setTurnedOn(boolean listen) {
        getMiter().setTurnedOn(listen);
        return (self());
    }


    /**
     *  Listen to turned off events.
     *
     *  @param listen {@code true} to listen to off events, {@code
     *         false} otherwise
     */
    
    public T turnedOff(boolean listen) {
        getMiter().setTurnedOff(listen);
        return (self());
    }

    
    /**
     *  Sets the changed variable amount.
     *
     *  @param listen {@code true} to listen to changed amount events,
     *         {@code false} otherwise
     */

    public T changedVariableAmount(boolean listen) {
        getMiter().setChangedVariableAmount(listen);
        return (self());
    }


    /**
     *  Listen to changes above the given threshold.
     *
     *  @param max a max threshold
     *  @throws org.osid.NullArgumentException <code>max</code> is
     *          <code>null</code>
     */

    public T changedExceedsVariableAmount(java.math.BigDecimal max) {
        getMiter().setChangedExceedsVariableAmount(max);
        return (self());
    }


    /**
     *  Listen to changes below the given threshold.
     *
     *  @param min a min threshold
     *  @throws org.osid.NullArgumentException <code>min</code> is
     *          <code>null</code>
     */

    public T changedDeceedsVariableAmount(java.math.BigDecimal min) {
        getMiter().setChangedDeceedsVariableAmount(min);
        return (self());
    }
    

    /**
     *  Sets the discreet state.
     *
     *  @param state a discreet state
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public T discreetState(org.osid.process.State state) {
        getMiter().setDiscreetState(state);
        return (self());
    }


    /**
     *  Adds an action group.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    public T actionGroup(org.osid.control.ActionGroup actionGroup) {
        getMiter().addActionGroup(actionGroup);
        return (self());
    }


    /**
     *  Sets all the action groups.
     *
     *  @param actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroups</code> is <code>null</code>
     */

    public T actionGroups(java.util.Collection<org.osid.control.ActionGroup> actionGroups) {
        getMiter().setActionGroups(actionGroups);
        return (self());
    }

    
    /**
     *  Adds a scene.
     *
     *  @param scene a Scene
     *  @throws org.osid.NullArgumentException
     *          <code>scene</code> is <code>null</code>
     */

    public T scene(org.osid.control.Scene scene) {
        getMiter().addScene(scene);
        return (self());
    }


    /**
     *  Sets all the scenes.
     *
     *  @param scenes a collection of scenes
     *  @throws org.osid.NullArgumentException <code>scenes</code> is
     *          <code>null</code>
     */

    public T scenes(java.util.Collection<org.osid.control.Scene> scenes) {
        getMiter().setScenes(scenes);
        return (self());
    }
    

    /**
     *  Adds a setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException
     *          <code>setting</code> is <code>null</code>
     */

    public T setting(org.osid.control.Setting setting) {
        getMiter().addSetting(setting);
        return (self());
    }


    /**
     *  Sets all the settings.
     *
     *  @param settings a collection of settings
     *  @throws org.osid.NullArgumentException <code>settings</code> is
     *          <code>null</code>
     */

    public T settings(java.util.Collection<org.osid.control.Setting> settings) {
        getMiter().setSettings(settings);
        return (self());
    }


    /**
     *  Adds a Trigger record.
     *
     *  @param record a trigger record
     *  @param recordType the type of trigger record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.control.records.TriggerRecord record, org.osid.type.Type recordType) {
        getMiter().addTriggerRecord(record, recordType);
        return (self());
    }
}       


