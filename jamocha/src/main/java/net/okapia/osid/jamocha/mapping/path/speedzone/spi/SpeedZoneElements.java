//
// SpeedZoneElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.speedzone.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SpeedZoneElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the SpeedZoneElement Id.
     *
     *  @return the speed zone element Id
     */

    public static org.osid.id.Id getSpeedZoneEntityId() {
        return (makeEntityId("osid.mapping.path.SpeedZone"));
    }


    /**
     *  Gets the PathId element Id.
     *
     *  @return the PathId element Id
     */

    public static org.osid.id.Id getPathId() {
        return (makeElementId("osid.mapping.path.speedzone.PathId"));
    }


    /**
     *  Gets the Path element Id.
     *
     *  @return the Path element Id
     */

    public static org.osid.id.Id getPath() {
        return (makeElementId("osid.mapping.path.speedzone.Path"));
    }


    /**
     *  Gets the StartingCoordinate element Id.
     *
     *  @return the StartingCoordinate element Id
     */

    public static org.osid.id.Id getStartingCoordinate() {
        return (makeElementId("osid.mapping.path.speedzone.StartingCoordinate"));
    }


    /**
     *  Gets the EndingCoordinate element Id.
     *
     *  @return the EndingCoordinate element Id
     */

    public static org.osid.id.Id getEndingCoordinate() {
        return (makeElementId("osid.mapping.path.speedzone.EndingCoordinate"));
    }


    /**
     *  Gets the SpeedLimit element Id.
     *
     *  @return the SpeedLimit element Id
     */

    public static org.osid.id.Id getSpeedLimit() {
        return (makeElementId("osid.mapping.path.speedzone.SpeedLimit"));
    }


    /**
     *  Gets the Coordinate element Id.
     *
     *  @return the Coordinate element Id
     */

    public static org.osid.id.Id getCoordinate() {
        return (makeQueryElementId("osid.mapping.path.speedzone.Coordinate"));
    }


    /**
     *  Gets the ContainingSpatialUnit element Id.
     *
     *  @return the ContainingSpatialUnit element Id
     */

    public static org.osid.id.Id getContainingSpatialUnit() {
        return (makeQueryElementId("osid.mapping.path.obstacle.ContainingSpatialUnit"));
    }


    /**
     *  Gets the Implicit element Id.
     *
     *  @return the Implicit element Id
     */

    public static org.osid.id.Id getImplicit() {
        return (makeElementId("osid.mapping.path.speedzone.Implicit"));
    }


    /**
     *  Gets the MapId element Id.
     *
     *  @return the MapId element Id
     */

    public static org.osid.id.Id getMapId() {
        return (makeQueryElementId("osid.mapping.path.speedzone.MapId"));
    }


    /**
     *  Gets the Map element Id.
     *
     *  @return the Map element Id
     */

    public static org.osid.id.Id getMap() {
        return (makeQueryElementId("osid.mapping.path.speedzone.Map"));
    }


    /**
     *  Gets the Length element Id.
     *
     *  @return the Length element Id
     */

    public static org.osid.id.Id getLength() {
        return (makeSearchOrderElementId("osid.mapping.path.speedzone.Length"));
    }
}
