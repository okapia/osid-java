//
// AbstractAssemblyRaceConstrainerQuery.java
//
//     A RaceConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.rules.raceconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RaceConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyRaceConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.voting.rules.RaceConstrainerQuery,
               org.osid.voting.rules.RaceConstrainerQueryInspector,
               org.osid.voting.rules.RaceConstrainerSearchOrder {

    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRaceConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRaceConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to a race. 
     *
     *  @param  raceId the race <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledRaceId(org.osid.id.Id raceId, boolean match) {
        getAssembler().addIdTerm(getRuledRaceIdColumn(), raceId, match);
        return;
    }


    /**
     *  Clears the race <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRaceIdTerms() {
        getAssembler().clearTerms(getRuledRaceIdColumn());
        return;
    }


    /**
     *  Gets the race <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRaceIdTerms() {
        return (getAssembler().getIdTerms(getRuledRaceIdColumn()));
    }


    /**
     *  Gets the RuledRaceId column name.
     *
     * @return the column name
     */

    protected String getRuledRaceIdColumn() {
        return ("ruled_race_id");
    }


    /**
     *  Tests if a <code> RaceQuery </code> is available. 
     *
     *  @return <code> true </code> if a race query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRaceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the race query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRaceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuery getRuledRaceQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRaceQuery() is false");
    }


    /**
     *  Matches constrainers mapped to any race. 
     *
     *  @param  match <code> true </code> for constrainers mapped to any race, 
     *          <code> false </code> to match constrainers mapped to no races 
     */

    @OSID @Override
    public void matchAnyRuledRace(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledRaceColumn(), match);
        return;
    }


    /**
     *  Clears the race query terms. 
     */

    @OSID @Override
    public void clearRuledRaceTerms() {
        getAssembler().clearTerms(getRuledRaceColumn());
        return;
    }


    /**
     *  Gets the race query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.RaceQueryInspector[] getRuledRaceTerms() {
        return (new org.osid.voting.RaceQueryInspector[0]);
    }


    /**
     *  Gets the RuledRace column name.
     *
     * @return the column name
     */

    protected String getRuledRaceColumn() {
        return ("ruled_race");
    }


    /**
     *  Matches constrainers mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this raceConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceConstrainerRecordType a race constrainer record type 
     *  @return <code>true</code> if the raceConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceConstrainerRecordType) {
        for (org.osid.voting.rules.records.RaceConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  raceConstrainerRecordType the race constrainer record type 
     *  @return the race constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerQueryRecord getRaceConstrainerQueryRecord(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  raceConstrainerRecordType the race constrainer record type 
     *  @return the race constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord getRaceConstrainerQueryInspectorRecord(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(raceConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param raceConstrainerRecordType the race constrainer record type
     *  @return the race constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerSearchOrderRecord getRaceConstrainerSearchOrderRecord(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(raceConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this race constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceConstrainerQueryRecord the race constrainer query record
     *  @param raceConstrainerQueryInspectorRecord the race constrainer query inspector
     *         record
     *  @param raceConstrainerSearchOrderRecord the race constrainer search order record
     *  @param raceConstrainerRecordType race constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerQueryRecord</code>,
     *          <code>raceConstrainerQueryInspectorRecord</code>,
     *          <code>raceConstrainerSearchOrderRecord</code> or
     *          <code>raceConstrainerRecordTyperaceConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addRaceConstrainerRecords(org.osid.voting.rules.records.RaceConstrainerQueryRecord raceConstrainerQueryRecord, 
                                      org.osid.voting.rules.records.RaceConstrainerQueryInspectorRecord raceConstrainerQueryInspectorRecord, 
                                      org.osid.voting.rules.records.RaceConstrainerSearchOrderRecord raceConstrainerSearchOrderRecord, 
                                      org.osid.type.Type raceConstrainerRecordType) {

        addRecordType(raceConstrainerRecordType);

        nullarg(raceConstrainerQueryRecord, "race constrainer query record");
        nullarg(raceConstrainerQueryInspectorRecord, "race constrainer query inspector record");
        nullarg(raceConstrainerSearchOrderRecord, "race constrainer search odrer record");

        this.queryRecords.add(raceConstrainerQueryRecord);
        this.queryInspectorRecords.add(raceConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(raceConstrainerSearchOrderRecord);
        
        return;
    }
}
