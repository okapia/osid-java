//
// AbstractOsidSourceableForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic OsidSourceableForm.
 */

public abstract class AbstractOsidSourceableForm
    extends AbstractOsidForm
    implements org.osid.OsidSourceableForm {
    
    private org.osid.id.Id provider;
    private boolean providerCleared = false;
    private org.osid.Metadata providerMetadata = getDefaultIdMetadata(getProviderId(), "provider");    
    
    private final java.util.Collection<org.osid.id.Id> brandingIds = new java.util.LinkedHashSet<>();
    private boolean brandingCleared = false;
    private org.osid.Metadata brandingMetadata = getDefaultArrayIdMetadata(getBrandingId(), "branding");    
    
    private String license;
    private boolean licenseCleared = false;
    private org.osid.Metadata licenseMetadata = getDefaultStringMetadata(getLicenseId(), "license"); 


    /** 
     *  Constructs a new {@code AbstractOsidSourceableForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidSourceableForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }


    /**
     *  Gets the metadata for the provider.
     *
     *  @return metadata for the provider 
     */

    @OSID @Override
    public org.osid.Metadata getProviderMetadata() {
        return (this.providerMetadata);
    }


    /**
     *  Sets the provider metadata.
     *
     *  @param metadata the provider metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setProviderMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "provider metadata");
        this.providerMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the provider field.
     *
     *  @return the provider field Id
     */

    protected org.osid.id.Id getProviderId() {
        return (SourceableElements.getProviderId());
    }

    
    /**
     *  Sets a provider. 
     *
     *  @param  provider the new provider 
     *  @throws org.osid.InvalidArgumentException <code> provider </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> provider </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setProvider(org.osid.id.Id provider) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(provider, getProviderMetadata());

        this.provider = provider;
        this.providerCleared = false;

        return;
    }


    /**
     *  Clears the provider. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearProvider() {
        if (getProviderMetadata().isRequired()) {
            throw new org.osid.NoAccessException("provider is required");
        }

        this.provider = null;
        this.providerCleared = true;

        return;
    }


    /**
     *  Tests if a provider has been set in this form.
     *
     *  @return {@code true} if the provider has been set, {@code
     *          false} otherwise
     */

    protected boolean isProviderSet() {
        return (this.provider != null);
    }


    /**
     *  Tests if a provider has been cleared in this form.
     *
     *  @return {@code true} if the provider has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isProviderCleared() {
        return (this.providerCleared);
    }


    /**
     *  Returns the current provider value.
     *
     *  @return the provider value or {@code null} if {@code
     *          isProviderSet()} is {@code false}
     */

    protected org.osid.id.Id getProvider() {
        return (this.provider);
    }


    /**
     *  Gets the metadata for the branding.
     *
     *  @return metadata for the branding 
     */

    @OSID @Override
    public org.osid.Metadata getBrandingMetadata() {
        return (this.brandingMetadata);
    }


    /**
     *  Sets the branding metadata.
     *
     *  @param metadata the branding metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setBrandingMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "branding metadata");
        this.brandingMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the branding field.
     *
     *  @return the branding field Id
     */

    protected org.osid.id.Id getBrandingId() {
        return (SourceableElements.getBrandingId());
    }

    
    /**
     *  Sets a branding. 
     *
     *  @param  assetIds the new branding 
     *  @throws org.osid.InvalidArgumentException <code>assetIds</code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code>assetIds</code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void setBranding(org.osid.id.Id[] assetIds) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(assetIds, getBrandingMetadata());

        this.brandingIds.clear();
        for (org.osid.id.Id id : assetIds) {
            this.brandingIds.add(id);
        }

        this.brandingCleared = false;
        return;
    }


    /**
     *  Clears the branding. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearBranding() {
        if (getBrandingMetadata().isRequired()) {
            throw new org.osid.NoAccessException("branding is required");
        }

        this.brandingIds.clear();
        this.brandingCleared = true;

        return;
    }


    /**
     *  Tests if a branding has been set in this form.
     *
     *  @return {@code true} if the branding has been set, {@code
     *          false} otherwise
     */

    protected boolean isBrandingSet() {
        return (this.brandingIds.size() > 0);
    }


    /**
     *  Tests if a branding has been cleared in this form.
     *
     *  @return {@code true} if the branding has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isBrandingCleared() {
        return (this.brandingCleared);
    }


    /**
     *  Returns the current branding value.
     *
     *  @return the branding value or {@code null} if {@code
     *          isBrandingSet()} is {@code false}
     */

    protected java.util.Collection<org.osid.id.Id> getBranding() {
        return (java.util.Collections.unmodifiableCollection(this.brandingIds));
    }


    /**
     *  Gets the metadata for the license.
     *
     *  @return metadata for the license 
     */

    @OSID @Override
    public org.osid.Metadata getLicenseMetadata() {
        return (this.licenseMetadata);
    }


    /**
     *  Sets the license metadata.
     *
     *  @param metadata the license metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setLicenseMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "license metadata");
        this.licenseMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the license field.
     *
     *  @return the license field Id
     */

    protected org.osid.id.Id getLicenseId() {
        return (SourceableElements.getLicense());
    }

    
    /**
     *  Sets a license. 
     *
     *  @param  license the new license 
     *  @throws org.osid.InvalidArgumentException <code> license </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> license </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setLicense(String license) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(license, getLicenseMetadata());
        this.license = license;
        return;
    }


    /**
     *  Clears the license. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearLicense() {
        if (getLicenseMetadata().isRequired()) {
            throw new org.osid.NoAccessException("license is required");
        }

        this.license = null;
        this.licenseCleared = true;

        return;
    }


    /**
     *  Tests if a license has been set in this form.
     *
     *  @return {@code true} if the license has been set, {@code
     *          false} otherwise
     */

    protected boolean isLicenseSet() {
        return (this.license != null);
    }


    /**
     *  Tests if a license has been cleared in this form.
     *
     *  @return {@code true} if the license has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isLicenseCleared() {
        return (this.licenseCleared);
    }


    /**
     *  Returns the current license value.
     *
     *  @return the license value or {@code null} if {@code
     *          isLicenseSet()} is {@code false}
     */

    protected String getLicense() {
        return (this.license);
    }
}
