//
// FamilyElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.family.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class FamilyElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the FamilyElement Id.
     *
     *  @return the family element Id
     */

    public static org.osid.id.Id getFamilyEntityId() {
        return (makeEntityId("osid.relationship.Family"));
    }


    /**
     *  Gets the RelationshipId element Id.
     *
     *  @return the RelationshipId element Id
     */

    public static org.osid.id.Id getRelationshipId() {
        return (makeQueryElementId("osid.relationship.family.RelationshipId"));
    }


    /**
     *  Gets the Relationship element Id.
     *
     *  @return the Relationship element Id
     */

    public static org.osid.id.Id getRelationship() {
        return (makeQueryElementId("osid.relationship.family.Relationship"));
    }


    /**
     *  Gets the AncestorFamilyId element Id.
     *
     *  @return the AncestorFamilyId element Id
     */

    public static org.osid.id.Id getAncestorFamilyId() {
        return (makeQueryElementId("osid.relationship.family.AncestorFamilyId"));
    }


    /**
     *  Gets the AncestorFamily element Id.
     *
     *  @return the AncestorFamily element Id
     */

    public static org.osid.id.Id getAncestorFamily() {
        return (makeQueryElementId("osid.relationship.family.AncestorFamily"));
    }


    /**
     *  Gets the DescendantFamilyId element Id.
     *
     *  @return the DescendantFamilyId element Id
     */

    public static org.osid.id.Id getDescendantFamilyId() {
        return (makeQueryElementId("osid.relationship.family.DescendantFamilyId"));
    }


    /**
     *  Gets the DescendantFamily element Id.
     *
     *  @return the DescendantFamily element Id
     */

    public static org.osid.id.Id getDescendantFamily() {
        return (makeQueryElementId("osid.relationship.family.DescendantFamily"));
    }
}
