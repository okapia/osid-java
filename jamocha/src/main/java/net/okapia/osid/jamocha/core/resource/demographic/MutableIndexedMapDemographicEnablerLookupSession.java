//
// MutableIndexedMapDemographicEnablerLookupSession
//
//    Implements a DemographicEnabler lookup service backed by a collection of
//    demographicEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic;


/**
 *  Implements a DemographicEnabler lookup service backed by a collection of
 *  demographic enablers. The demographic enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some demographic enablers may be compatible
 *  with more types than are indicated through these demographic enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of demographic enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapDemographicEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resource.demographic.spi.AbstractIndexedMapDemographicEnablerLookupSession
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapDemographicEnablerLookupSession} with no demographic enablers.
     *
     *  @param bin the bin
     *  @throws org.osid.NullArgumentException {@code bin}
     *          is {@code null}
     */

      public MutableIndexedMapDemographicEnablerLookupSession(org.osid.resource.Bin bin) {
        setBin(bin);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDemographicEnablerLookupSession} with a
     *  single demographic enabler.
     *  
     *  @param bin the bin
     *  @param  demographicEnabler a single demographicEnabler
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code demographicEnabler} is {@code null}
     */

    public MutableIndexedMapDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.demographic.DemographicEnabler demographicEnabler) {
        this(bin);
        putDemographicEnabler(demographicEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDemographicEnablerLookupSession} using an
     *  array of demographic enablers.
     *
     *  @param bin the bin
     *  @param  demographicEnablers an array of demographic enablers
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code demographicEnablers} is {@code null}
     */

    public MutableIndexedMapDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.demographic.DemographicEnabler[] demographicEnablers) {
        this(bin);
        putDemographicEnablers(demographicEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapDemographicEnablerLookupSession} using a
     *  collection of demographic enablers.
     *
     *  @param bin the bin
     *  @param  demographicEnablers a collection of demographic enablers
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code demographicEnablers} is {@code null}
     */

    public MutableIndexedMapDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                  java.util.Collection<? extends org.osid.resource.demographic.DemographicEnabler> demographicEnablers) {

        this(bin);
        putDemographicEnablers(demographicEnablers);
        return;
    }
    

    /**
     *  Makes a {@code DemographicEnabler} available in this session.
     *
     *  @param  demographicEnabler a demographic enabler
     *  @throws org.osid.NullArgumentException {@code demographicEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putDemographicEnabler(org.osid.resource.demographic.DemographicEnabler demographicEnabler) {
        super.putDemographicEnabler(demographicEnabler);
        return;
    }


    /**
     *  Makes an array of demographic enablers available in this session.
     *
     *  @param  demographicEnablers an array of demographic enablers
     *  @throws org.osid.NullArgumentException {@code demographicEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putDemographicEnablers(org.osid.resource.demographic.DemographicEnabler[] demographicEnablers) {
        super.putDemographicEnablers(demographicEnablers);
        return;
    }


    /**
     *  Makes collection of demographic enablers available in this session.
     *
     *  @param  demographicEnablers a collection of demographic enablers
     *  @throws org.osid.NullArgumentException {@code demographicEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putDemographicEnablers(java.util.Collection<? extends org.osid.resource.demographic.DemographicEnabler> demographicEnablers) {
        super.putDemographicEnablers(demographicEnablers);
        return;
    }


    /**
     *  Removes a DemographicEnabler from this session.
     *
     *  @param demographicEnablerId the {@code Id} of the demographic enabler
     *  @throws org.osid.NullArgumentException {@code demographicEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDemographicEnabler(org.osid.id.Id demographicEnablerId) {
        super.removeDemographicEnabler(demographicEnablerId);
        return;
    }    
}
