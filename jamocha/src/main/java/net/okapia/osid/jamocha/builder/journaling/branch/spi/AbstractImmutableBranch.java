//
// AbstractImmutableBranch.java
//
//     Wraps a mutable Branch to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.journaling.branch.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Branch</code> to hide modifiers. This
 *  wrapper provides an immutized Branch from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying branch whose state changes are visible.
 */

public abstract class AbstractImmutableBranch
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.journaling.Branch {

    private final org.osid.journaling.Branch branch;


    /**
     *  Constructs a new <code>AbstractImmutableBranch</code>.
     *
     *  @param branch the branch to immutablize
     *  @throws org.osid.NullArgumentException <code>branch</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBranch(org.osid.journaling.Branch branch) {
        super(branch);
        this.branch = branch;
        return;
    }


    /**
     *  Gets the journal entry <code> Id </code> from which this branch was 
     *  started. 
     *
     *  @return the journal entry <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOriginJournalEntryId() {
        return (this.branch.getOriginJournalEntryId());
    }


    /**
     *  Gets the journal entry from which this branch was started. 
     *
     *  @return the journal entry 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry getOriginJournalEntry()
        throws org.osid.OperationFailedException {

        return (this.branch.getOriginJournalEntry());
    }


    /**
     *  Gets the latest journal entry <code> Id </code> in this branch. 
     *
     *  @return the journal entry <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLatestJournalEntryId() {
        return (this.branch.getLatestJournalEntryId());
    }


    /**
     *  Gets the latest journal entry in this branch. 
     *
     *  @return the journal entry 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry getLatestJournalEntry()
        throws org.osid.OperationFailedException {

        return (this.branch.getLatestJournalEntry());
    }


    /**
     *  Gets the branch record corresponding to the given <code> Pollstem 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> branchRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(branchRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  branchRecordType the branch record type 
     *  @return the branch record 
     *  @throws org.osid.NullArgumentException <code> branchRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(branchRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.records.BranchRecord getBranchRecord(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException {

        return (this.branch.getBranchRecord(branchRecordType));
    }
}

