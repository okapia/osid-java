//
// AbstractAdapterCourseCatalogLookupSession.java
//
//    A CourseCatalog lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CourseCatalog lookup session adapter.
 */

public abstract class AbstractAdapterCourseCatalogLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.CourseCatalogLookupSession {

    private final org.osid.course.CourseCatalogLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCourseCatalogLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCourseCatalogLookupSession(org.osid.course.CourseCatalogLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code CourseCatalog} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCourseCatalogs() {
        return (this.session.canLookupCourseCatalogs());
    }


    /**
     *  A complete view of the {@code CourseCatalog} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseCatalogView() {
        this.session.useComparativeCourseCatalogView();
        return;
    }


    /**
     *  A complete view of the {@code CourseCatalog} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseCatalogView() {
        this.session.usePlenaryCourseCatalogView();
        return;
    }

     
    /**
     *  Gets the {@code CourseCatalog} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CourseCatalog} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CourseCatalog} and
     *  retained for compatibility.
     *
     *  @param courseCatalogId {@code Id} of the {@code CourseCatalog}
     *  @return the course catalog
     *  @throws org.osid.NotFoundException {@code courseCatalogId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code courseCatalogId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets a {@code CourseCatalogList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courseCatalogs specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CourseCatalogs} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  courseCatalogIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CourseCatalog} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code courseCatalogIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByIds(org.osid.id.IdList courseCatalogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseCatalogsByIds(courseCatalogIds));
    }


    /**
     *  Gets a {@code CourseCatalogList} corresponding to the given
     *  course catalog genus {@code Type} which does not include
     *  course catalogs of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  courseCatalogGenusType a courseCatalog genus type 
     *  @return the returned {@code CourseCatalog} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseCatalogGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByGenusType(org.osid.type.Type courseCatalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseCatalogsByGenusType(courseCatalogGenusType));
    }


    /**
     *  Gets a {@code CourseCatalogList} corresponding to the given
     *  course catalog genus {@code Type} and include any additional
     *  course catalogs with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  courseCatalogGenusType a courseCatalog genus type 
     *  @return the returned {@code CourseCatalog} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseCatalogGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByParentGenusType(org.osid.type.Type courseCatalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseCatalogsByParentGenusType(courseCatalogGenusType));
    }


    /**
     *  Gets a {@code CourseCatalogList} containing the given
     *  course catalog record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  courseCatalogRecordType a courseCatalog record type 
     *  @return the returned {@code CourseCatalog} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseCatalogRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByRecordType(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseCatalogsByRecordType(courseCatalogRecordType));
    }


    /**
     *  Gets a {@code CourseCatalogList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code CourseCatalog} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseCatalogsByProvider(resourceId));
    }


    /**
     *  Gets all {@code CourseCatalogs}. 
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code CourseCatalogs} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseCatalogs());
    }
}
