//
// AbstractImmutableSummary.java
//
//     Wraps a mutable Summary to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.summary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Wraps a mutable <code>Summary</code> to hide modifiers. This
 *  wrapper provides an immutized Summary from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying summary whose state changes are visible.
 */

public abstract class AbstractImmutableSummary
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCompendium
    implements org.osid.financials.Summary {

    private final org.osid.financials.Summary summary;


    /**
     *  Constructs a new <code>AbstractImmutableSummary</code>.
     *
     *  @param summary the summary to immutablize
     *  @throws org.osid.NullArgumentException <code>summary</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSummary(org.osid.financials.Summary summary) {
        super(summary);
        this.summary = summary;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the account. 
     *
     *  @return the account <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        return (this.summary.getAccountId());
    }


    /**
     *  Gets the account. 
     *
     *  @return the account 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {
        
        return (this.summary.getAccount());
    }


    /**
     *  Gets the <code> Id </code> of the fiscal period 
     *
     *  @return the fiscal period <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFiscalPeriodId() {
        return (this.summary.getFiscalPeriodId());
    }


    /**
     *  Gets the fiscal period 
     *
     *  @return the fiscal period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod()
        throws org.osid.OperationFailedException {
        
        return (this.summary.getFiscalPeriod());
    }


    /**
     *  Gets the credits for this fiscal period. 
     *
     *  @return the credits 
     */

    @OSID @Override
    public org.osid.financials.Currency getCredits() {
        return (this.summary.getCredits());
    }


    /**
     *  Gets the debits for this fiscal period. 
     *
     *  @return the debits 
     */

    @OSID @Override
    public org.osid.financials.Currency getDebits() {
        return (this.summary.getDebits());
    }


    /**
     *  Tests if this the account on this activity has a budget for this time 
     *  period. 
     *
     *  @return <code> true </code> if a budget is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isBudgeted() {
        return (this.summary.isBudgeted());
    }

    
    /**
     *  Gets the budget. 
     *
     *  @return the budget 
     *  @throws org.osid.IllegalStateException <code> isBudgeted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getBudget() {
        return (this.summary.getBudget());
    }


    /**
     *  Tests if this the account on this activity has a forecast for this 
     *  time period. 
     *
     *  @return <code> true </code> if a forecast is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasForecast() {
        return (this.summary.hasForecast());
    }


    /**
     *  Gets the forecast 
     *
     *  @return the forecast 
     *  @throws org.osid.IllegalStateException <code> hasForecast() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getForecast() {
        return (this.summary.getForecast());
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Summary</code> record <code>Type</code>.
     *
     *  @param  summaryRecordType the summary record type 
     *  @return the summary record 
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.SummaryRecord getSummaryRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        return (this.summary.getSummaryRecord(summaryRecordType));
    }
}

