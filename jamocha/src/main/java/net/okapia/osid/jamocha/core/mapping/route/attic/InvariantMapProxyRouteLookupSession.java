//
// InvariantMapProxyRouteLookupSession
//
//    Implements a Route lookup service backed by a fixed
//    collection of routes. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.route;


/**
 *  Implements a Route lookup service backed by a fixed
 *  collection of routes. The routes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyRouteLookupSession
    extends net.okapia.osid.jamocha.core.mapping.route.spi.AbstractMapRouteLookupSession
    implements org.osid.mapping.route.RouteLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProxyRouteLookupSession</code> with no
     *  routes.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     */

    public InvariantMapProxyRouteLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new <code>InvariantMapProxyRouteLookupSession</code> with a
     *  single route.
     *
     *  @param  route a single route
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>route</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxyRouteLookupSession(org.osid.mapping.route.Route route, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRoute(route);
        return;
    }


    /**
     *  Constructs a new <code>InvariantMapProxyRouteLookupSession</code> using
     *  an array of routes.
     *
     *  @param  routes an array of routes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>routes</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxyRouteLookupSession(org.osid.mapping.route.Route[] routes, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRoutes(routes);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProxyRouteLookupSession</code> using a
     *  collection of routes.
     *
     *  @param  routes a collection of routes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>routes</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxyRouteLookupSession(java.util.Collection<? extends org.osid.mapping.route.Route> routes,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRoutes(routes);
        return;
    }
}
