//
// AbstractCheckValidator.java
//
//     Validates a Check.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.rules.check.check.spi;


/**
 *  Validates a Check.
 */

public abstract class AbstractCheckValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractCheckValidator</code>.
     */

    protected AbstractCheckValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractCheckValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractCheckValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Check.
     *
     *  @param check a check to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>check</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.rules.check.Check check) {
        super.validate(check);

        if (check.isFailCheck() && (check.isTimeCheckByDate() || check.isTimeCheckByEvent() ||
                                    check.isTimeCheckByCyclicEvent() || check.isHoldCheck() ||
                                    check.isInquiryCheck() || check.isProcessCheck())) {
            throw new org.osid.BadLogicException("isFailCheck() is true");
        }

        if (check.isTimeCheckByDate() && (check.isFailCheck() || check.isTimeCheckByEvent() ||
                                          check.isTimeCheckByCyclicEvent() || check.isHoldCheck() ||
                                          check.isInquiryCheck() || check.isProcessCheck())) {
            throw new org.osid.BadLogicException("isTimeCheckByDate() is true");
        }


        if (check.isTimeCheckByEvent() && (check.isFailCheck() || check.isTimeCheckByDate() ||
                                           check.isTimeCheckByCyclicEvent() || check.isHoldCheck() ||
                                           check.isInquiryCheck() || check.isProcessCheck())) {
            throw new org.osid.BadLogicException("isTimeCheckByEvent() is true");
        }

        if (check.isTimeCheckByCyclicEvent() && (check.isFailCheck() || check.isTimeCheckByDate() ||
                                                 check.isTimeCheckByEvent() || check.isHoldCheck() ||
                                                 check.isInquiryCheck() || check.isProcessCheck())) {
            throw new org.osid.BadLogicException("isTimeCheckByCyclicEvent() is true");
        }

        if (check.isHoldCheck() && (check.isFailCheck() || check.isTimeCheckByDate() ||
                                    check.isTimeCheckByCyclicEvent() || check.isTimeCheckByEvent() ||
                                    check.isInquiryCheck() || check.isProcessCheck())) {
            throw new org.osid.BadLogicException("isHoldCheck() is true");
        }

        if (check.isInquiryCheck() && (check.isFailCheck() || check.isTimeCheckByDate() ||
                                    check.isTimeCheckByCyclicEvent() || check.isTimeCheckByEvent() ||
                                    check.isHoldCheck() || check.isProcessCheck())) {
            throw new org.osid.BadLogicException("isHoldCheck() is true");
        }

        if (check.isProcessCheck() && (check.isFailCheck() || check.isTimeCheckByDate() ||
                                       check.isTimeCheckByCyclicEvent() || check.isTimeCheckByEvent() ||
                                       check.isHoldCheck() || check.isInquiryCheck())) {
            throw new org.osid.BadLogicException("isProcessCheck() is true");
        }

        testConditionalMethod(check, "getTimeCheckStartDate", check.isTimeCheckByDate(), "isTimeCheckByDate()");
        testConditionalMethod(check, "getTimeCheckEndDate", check.isTimeCheckByDate(), "isTimeCheckByDate()");

        testConditionalMethod(check, "getTimeCheckEventId", check.isTimeCheckByEvent(), "isTimeCheckByEvent()");
        testConditionalMethod(check, "getTimeCheckEvent", check.isTimeCheckByEvent(), "isTimeCheckByEvent()");
        if (check.isTimeCheckByEvent()) {
            testNestedObject(check, "getTimeCheckEvent");
        }

        testConditionalMethod(check, "getTimeCheckCyclicEventId", check.isTimeCheckByCyclicEvent(), "isTimeCheckByCyclicEvent()");
        testConditionalMethod(check, "getTimeCheckCyclicEvent", check.isTimeCheckByCyclicEvent(), "isTimeCheckByCyclicEvent()");
        if (check.isTimeCheckByCyclicEvent()) {
            testNestedObject(check, "getTimeCheckCyclicEvent");
        }

        testConditionalMethod(check, "getHoldCheckBlockId", check.isHoldCheck(), "isHoldCheck()");
        testConditionalMethod(check, "getHoldCheckBlock", check.isHoldCheck(), "isHoldCheck()");
        if (check.isHoldCheck()) {
            testNestedObject(check, "getHoldCheckBlock");
        }

        testConditionalMethod(check, "getInquiryCheckAuditId", check.isInquiryCheck(), "isInquiryCheck()");
        testConditionalMethod(check, "getInquiryCheckAudit", check.isInquiryCheck(), "isInquiryCheck()");
        if (check.isInquiryCheck()) {
            testNestedObject(check, "getInquiryCheckAudit");
        }

        testConditionalMethod(check, "getProcessCheckAgendaId", check.isProcessCheck(), "isProcessCheck()");
        testConditionalMethod(check, "getProcessCheckAgenda", check.isProcessCheck(), "isProcessCheck()");
        if (check.isProcessCheck()) {
            testNestedObject(check, "getProcessCheckAgenda");
        }

        return;
    }
}
