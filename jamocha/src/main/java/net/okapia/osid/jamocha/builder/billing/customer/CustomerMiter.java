//
// CustomerMiter.java
//
//     Defines a Customer miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.customer;


/**
 *  Defines a <code>Customer</code> miter for use with the builders.
 */

public interface CustomerMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            org.osid.billing.Customer {


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the customer number.
     *
     *  @param mumber a customer number
     *  @throws org.osid.NullArgumentException <code>mumber</code> is
     *          <code>null</code>
     */

    public void setCustomerNumber(String mumber);


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public void setActivity(org.osid.financials.Activity activity);


    /**
     *  Adds a Customer record.
     *
     *  @param record a customer record
     *  @param recordType the type of customer record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCustomerRecord(org.osid.billing.records.CustomerRecord record, org.osid.type.Type recordType);
}       


