//
// AbstractProcess.java
//
//     Defines a Process builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.process.spi;


/**
 *  Defines a <code>Process</code> builder.
 */

public abstract class AbstractProcessBuilder<T extends AbstractProcessBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.workflow.process.ProcessMiter process;


    /**
     *  Constructs a new <code>AbstractProcessBuilder</code>.
     *
     *  @param process the process to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProcessBuilder(net.okapia.osid.jamocha.builder.workflow.process.ProcessMiter process) {
        super(process);
        this.process = process;
        return;
    }


    /**
     *  Builds the process.
     *
     *  @return the new process
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.workflow.Process build() {
        (new net.okapia.osid.jamocha.builder.validator.workflow.process.ProcessValidator(getValidations())).validate(this.process);
        return (new net.okapia.osid.jamocha.builder.workflow.process.ImmutableProcess(this.process));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the process miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.workflow.process.ProcessMiter getMiter() {
        return (this.process);
    }


    /**
     *  Sets the initial step.
     *
     *  @param step an initial step
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>step</code> is
     *          <code>null</code>
     */

    public T initialStep(org.osid.workflow.Step step) {
        getMiter().setInitialStep(step);
        return (self());
    }


    /**
     *  Sets the initial state.
     *
     *  @param state an initial state
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public T initialState(org.osid.process.State state) {
        getMiter().setInitialState(state);
        return (self());
    }


    /**
     *  Adds a Process record.
     *
     *  @param record a process record
     *  @param recordType the type of process record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.workflow.records.ProcessRecord record, org.osid.type.Type recordType) {
        getMiter().addProcessRecord(record, recordType);
        return (self());
    }
}       


