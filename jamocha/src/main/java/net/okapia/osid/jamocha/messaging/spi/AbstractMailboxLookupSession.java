//
// AbstractMailboxLookupSession.java
//
//    A starter implementation framework for providing a Mailbox
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Mailbox
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getMailboxes(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractMailboxLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.messaging.MailboxLookupSession {

    private boolean pedantic = false;
    private org.osid.messaging.Mailbox mailbox = new net.okapia.osid.jamocha.nil.messaging.mailbox.UnknownMailbox();


    /**
     *  Tests if this user can perform <code>Mailbox</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMailboxes() {
        return (true);
    }


    /**
     *  A complete view of the <code>Mailbox</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMailboxView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Mailbox</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMailboxView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Mailbox</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Mailbox</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Mailbox</code> and
     *  retained for compatibility.
     *
     *  @param  mailboxId <code>Id</code> of the
     *          <code>Mailbox</code>
     *  @return the mailbox
     *  @throws org.osid.NotFoundException <code>mailboxId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>mailboxId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.messaging.MailboxList mailboxes = getMailboxes()) {
            while (mailboxes.hasNext()) {
                org.osid.messaging.Mailbox mailbox = mailboxes.getNextMailbox();
                if (mailbox.getId().equals(mailboxId)) {
                    return (mailbox);
                }
            }
        } 

        throw new org.osid.NotFoundException(mailboxId + " not found");
    }


    /**
     *  Gets a <code>MailboxList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  mailboxes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Mailboxes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getMailboxes()</code>.
     *
     *  @param  mailboxIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Mailbox</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByIds(org.osid.id.IdList mailboxIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.messaging.Mailbox> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = mailboxIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getMailbox(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("mailbox " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.messaging.mailbox.LinkedMailboxList(ret));
    }


    /**
     *  Gets a <code>MailboxList</code> corresponding to the given
     *  mailbox genus <code>Type</code> which does not include
     *  mailboxes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getMailboxes()</code>.
     *
     *  @param  mailboxGenusType a mailbox genus type 
     *  @return the returned <code>Mailbox</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByGenusType(org.osid.type.Type mailboxGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.mailbox.MailboxGenusFilterList(getMailboxes(), mailboxGenusType));
    }


    /**
     *  Gets a <code>MailboxList</code> corresponding to the given
     *  mailbox genus <code>Type</code> and include any additional
     *  mailboxes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getMailboxes()</code>.
     *
     *  @param  mailboxGenusType a mailbox genus type 
     *  @return the returned <code>Mailbox</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByParentGenusType(org.osid.type.Type mailboxGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getMailboxesByGenusType(mailboxGenusType));
    }


    /**
     *  Gets a <code>MailboxList</code> containing the given
     *  mailbox record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getMailboxes()</code>.
     *
     *  @param  mailboxRecordType a mailbox record type 
     *  @return the returned <code>Mailbox</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByRecordType(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.messaging.mailbox.MailboxRecordFilterList(getMailboxes(), mailboxRecordType));
    }


    /**
     *  Gets a <code>MailboxList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known mailboxes or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  mailboxes that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Mailbox</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.messaging.mailbox.MailboxProviderFilterList(getMailboxes(), resourceId));
    }


    /**
     *  Gets all <code>Mailboxes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  mailboxes or an error results. Otherwise, the returned list
     *  may contain only those mailboxes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Mailboxes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.messaging.MailboxList getMailboxes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the mailbox list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of mailboxes
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.messaging.MailboxList filterMailboxesOnViews(org.osid.messaging.MailboxList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
