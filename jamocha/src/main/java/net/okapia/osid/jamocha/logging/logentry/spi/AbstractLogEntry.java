//
// AbstractLogEntry.java
//
//     Defines a LogEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.logentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>LogEntry</code>.
 */

public abstract class AbstractLogEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.logging.LogEntry {

    private org.osid.type.Type priority;
    private org.osid.calendaring.DateTime timestamp;
    private org.osid.resource.Resource resource;
    private org.osid.authentication.Agent agent;

    private final java.util.Collection<org.osid.logging.records.LogEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the priority level of this entry. 
     *
     *  @return the priority level 
     */

    @OSID @Override
    public org.osid.type.Type getPriority() {
        return (this.priority);
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @throws org.osid.NullArgumentException
     *          <code>priority</code> is <code>null</code>
     */

    protected void setPriority(org.osid.type.Type priority) {
        nullarg(priority, "priority");
        this.priority = priority;
        return;
    }


    /**
     *  Gets the time this entry was logged. 
     *
     *  @return the time stamp of this entry 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.timestamp);
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException
     *          <code>timestamp</code> is <code>null</code>
     */

    protected void setTimestamp(org.osid.calendaring.DateTime timestamp) {
        nullarg(timestamp, "timestamp");
        this.timestamp = timestamp;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> who created this entry. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the <code> Resource </code> who created this entry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the agent <code> Id </code> who created this entry. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.agent.getId());
    }


    /**
     *  Gets the <code> Agent </code> who created this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Tests if this logEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  logEntryRecordType a log entry record type 
     *  @return <code>true</code> if the logEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type logEntryRecordType) {
        for (org.osid.logging.records.LogEntryRecord record : this.records) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>LogEntry</code> record <code>Type</code>.
     *
     *  @param  logEntryRecordType the log entry record type 
     *  @return the log entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogEntryRecord getLogEntryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogEntryRecord record : this.records) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this log entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param logEntryRecord the log entry record
     *  @param logEntryRecordType log entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecord</code> or
     *          <code>logEntryRecordTypelogEntry</code> is
     *          <code>null</code>
     */
            
    protected void addLogEntryRecord(org.osid.logging.records.LogEntryRecord logEntryRecord, 
                                     org.osid.type.Type logEntryRecordType) {

        nullarg(logEntryRecord, "log entry record");
        addRecordType(logEntryRecordType);
        this.records.add(logEntryRecord);
        
        return;
    }
}
