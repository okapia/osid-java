//
// AbstractFederatingGradeSystemLookupSession.java
//
//     An abstract federating adapter for a GradeSystemLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  GradeSystemLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingGradeSystemLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.grading.GradeSystemLookupSession> 
    implements org.osid.grading.GradeSystemLookupSession {

    private boolean parallel = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();


    /**
     *  Constructs a new
     *  <code>AbstractFederatingGradeSystemLookupSession</code>.
     */
    
    protected AbstractFederatingGradeSystemLookupSession() {
        return;
    }

    
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.grading.GradeSystemLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Gradebook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the <code>Gradebook</code>.
     *
     *  @param  gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can perform <code>GradeSystem</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradeSystems() {
        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            if (session.canLookupGradeSystems()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>GradeSystem</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeSystemView() {
        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            session.useComparativeGradeSystemView();
        }

        return;
    }


    /**
     *  A complete view of the <code>GradeSystem</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeSystemView() {
        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            session.usePlenaryGradeSystemView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade systems in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            session.useFederatedGradebookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            session.useIsolatedGradebookView();
        }

        return;
    }

     
    /**
     *  Gets the <code>GradeSystem</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradeSystem</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradeSystem</code> and
     *  retained for compatibility.
     *
     *  @param  gradeSystemId <code>Id</code> of the
     *          <code>GradeSystem</code>
     *  @return the grade system
     *  @throws org.osid.NotFoundException <code>gradeSystemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradeSystemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem(org.osid.id.Id gradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            try {
                return (session.getGradeSystem(gradeSystemId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(gradeSystemId + " not found");
    }


    /**
     *  Gets a <code>GradeSystemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeSystems specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>GradeSystems</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  gradeSystemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByIds(org.osid.id.IdList gradeSystemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.grading.gradesystem.MutableGradeSystemList ret = new net.okapia.osid.jamocha.grading.gradesystem.MutableGradeSystemList();

        try (org.osid.id.IdList ids = gradeSystemIds) {
            while (ids.hasNext()) {
                ret.addGradeSystem(getGradeSystem(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>GradeSystemList</code> corresponding to the given
     *  grade system genus <code>Type</code> which does not include
     *  grade systems of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemGenusType a gradeSystem genus type 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByGenusType(org.osid.type.Type gradeSystemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradesystem.FederatingGradeSystemList ret = getGradeSystemList();

        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            ret.addGradeSystemList(session.getGradeSystemsByGenusType(gradeSystemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeSystemList</code> corresponding to the given
     *  grade system genus <code>Type</code> and include any additional
     *  grade systems with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemGenusType a gradeSystem genus type 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByParentGenusType(org.osid.type.Type gradeSystemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradesystem.FederatingGradeSystemList ret = getGradeSystemList();

        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            ret.addGradeSystemList(session.getGradeSystemsByParentGenusType(gradeSystemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>GradeSystemList</code> containing the given
     *  grade system record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemRecordType a gradeSystem record type 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByRecordType(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradesystem.FederatingGradeSystemList ret = getGradeSystemList();

        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            ret.addGradeSystemList(session.getGradeSystemsByRecordType(gradeSystemRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the <code>GradeSystem</code> by a <code>Grade</code>
     *  <code>Id</code>.
     *
     *  @param  gradeId <code>Id</code> of a <code>Grade</code>
     *  @return the grade system
     *  @throws org.osid.NotFoundException <code>gradeId</code> not found
     *  @throws org.osid.NullArgumentException <code>gradeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystemByGrade(org.osid.id.Id gradeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            try {
                return (session.getGradeSystemByGrade(gradeId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(gradeId + " not found");
    }


    /**
     *  Gets all <code>GradeSystems</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>GradeSystems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.grading.gradesystem.FederatingGradeSystemList ret = getGradeSystemList();

        for (org.osid.grading.GradeSystemLookupSession session : getSessions()) {
            ret.addGradeSystemList(session.getGradeSystems());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.grading.gradesystem.FederatingGradeSystemList getGradeSystemList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.gradesystem.ParallelGradeSystemList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.grading.gradesystem.CompositeGradeSystemList());
        }
    }
}
