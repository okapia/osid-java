//
// AbstractLogQuery.java
//
//     A template for making a Log Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.log.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for logs.
 */

public abstract class AbstractLogQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.logging.LogQuery {

    private final java.util.Collection<org.osid.logging.records.LogQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a log entry <code> Id. </code> 
     *
     *  @param  logEntryId a log entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLogEntryId(org.osid.id.Id logEntryId, boolean match) {
        return;
    }


    /**
     *  Clesrs the log entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLogEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a log entry query is available. 
     *
     *  @return <code> true </code> if a log entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log entry. 
     *
     *  @return the log entry query 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuery getLogEntryQuery() {
        throw new org.osid.UnimplementedException("supportsLogEntryQuery() is false");
    }


    /**
     *  Matches logs with any log entry. 
     *
     *  @param  match <code> true </code> to match logs with any entry, <code> 
     *          false </code> to match logs with no log entries 
     */

    @OSID @Override
    public void matchAnyLogEntry(boolean match) {
        return;
    }


    /**
     *  Clesrs the log entry terms. 
     */

    @OSID @Override
    public void clearLogEntryTerms() {
        return;
    }


    /**
     *  Sets the log <code> Id </code> for this query to match logs that have 
     *  the specified log as an ancestor. 
     *
     *  @param  logId a log <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorLogId(org.osid.id.Id logId, boolean match) {
        return;
    }


    /**
     *  Clesrs the ancestor log <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorLogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LogQuery </code> is available. 
     *
     *  @return <code> true </code> if a log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorLogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the log query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorLogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuery getAncestorLogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorLogQuery() is false");
    }


    /**
     *  Matches logs with any ancestor. 
     *
     *  @param  match <code> true </code> to match logs with any ancestor, 
     *          <code> false </code> to match root logs 
     */

    @OSID @Override
    public void matchAnyAncestorLog(boolean match) {
        return;
    }


    /**
     *  Clesrs the ancestor log terms. 
     */

    @OSID @Override
    public void clearAncestorLogTerms() {
        return;
    }


    /**
     *  Sets the log <code> Id </code> for this query to match logs that have 
     *  the specified log as a descendant. 
     *
     *  @param  logId a log <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantLogId(org.osid.id.Id logId, boolean match) {
        return;
    }


    /**
     *  Clesrs the descendant log <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantLogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LogQuery </code> is available. 
     *
     *  @return <code> true </code> if a log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantLogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the log query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantLogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuery getDescendantLogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantLogQuery() is false");
    }


    /**
     *  Matches logs with any descendant. 
     *
     *  @param  match <code> true </code> to match logs with any descendant, 
     *          <code> false </code> to match leaf logs 
     */

    @OSID @Override
    public void matchAnyDescendantLog(boolean match) {
        return;
    }


    /**
     *  Clesrs the descendant log terms. 
     */

    @OSID @Override
    public void clearDescendantLogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given log query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a log implementing the requested record.
     *
     *  @param logRecordType a log record type
     *  @return the log query record
     *  @throws org.osid.NullArgumentException
     *          <code>logRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogQueryRecord getLogQueryRecord(org.osid.type.Type logRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogQueryRecord record : this.records) {
            if (record.implementsRecordType(logRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logRecordType + " is not supported");
    }


    /**
     *  Adds a record to this log query. 
     *
     *  @param logQueryRecord log query record
     *  @param logRecordType log record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLogQueryRecord(org.osid.logging.records.LogQueryRecord logQueryRecord, 
                                          org.osid.type.Type logRecordType) {

        addRecordType(logRecordType);
        nullarg(logQueryRecord, "log query record");
        this.records.add(logQueryRecord);        
        return;
    }
}
