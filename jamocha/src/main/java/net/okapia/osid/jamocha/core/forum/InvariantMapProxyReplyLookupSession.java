//
// InvariantMapProxyReplyLookupSession
//
//    Implements a Reply lookup service backed by a fixed
//    collection of replies. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum;


/**
 *  Implements a Reply lookup service backed by a fixed
 *  collection of replies. The replies are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyReplyLookupSession
    extends net.okapia.osid.jamocha.core.forum.spi.AbstractMapReplyLookupSession
    implements org.osid.forum.ReplyLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyReplyLookupSession} with no
     *  replies.
     *
     *  @param forum the forum
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyReplyLookupSession(org.osid.forum.Forum forum,
                                                  org.osid.proxy.Proxy proxy) {
        setForum(forum);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyReplyLookupSession} with a single
     *  reply.
     *
     *  @param forum the forum
     *  @param reply a single reply
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum},
     *          {@code reply} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyReplyLookupSession(org.osid.forum.Forum forum,
                                                  org.osid.forum.Reply reply, org.osid.proxy.Proxy proxy) {

        this(forum, proxy);
        putReply(reply);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyReplyLookupSession} using
     *  an array of replies.
     *
     *  @param forum the forum
     *  @param replies an array of replies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum},
     *          {@code replies} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyReplyLookupSession(org.osid.forum.Forum forum,
                                                  org.osid.forum.Reply[] replies, org.osid.proxy.Proxy proxy) {

        this(forum, proxy);
        putReplies(replies);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyReplyLookupSession} using a
     *  collection of replies.
     *
     *  @param forum the forum
     *  @param replies a collection of replies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum},
     *          {@code replies} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyReplyLookupSession(org.osid.forum.Forum forum,
                                                  java.util.Collection<? extends org.osid.forum.Reply> replies,
                                                  org.osid.proxy.Proxy proxy) {

        this(forum, proxy);
        putReplies(replies);
        return;
    }
}
