//
// AliasResponseMiter.java
//
//     Defines an AliasResponse miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transaction.batch.aliasresponse;


/**
 *  Defines an <code>AliasResponse</code> miter for use with the builders.
 */

public interface AliasResponseMiter
    extends net.okapia.osid.jamocha.builder.spi.Miter,
            org.osid.transaction.batch.AliasResponse {


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    public void setReferenceId(org.osid.id.Id referenceId);


    /**
     *  Sets the alias id.
     *
     *  @param aliasId an alias id
     *  @throws org.osid.NullArgumentException
     *          <code>aliasId</code> is <code>null</code>
     */

    public void setAliasId(org.osid.id.Id aliasId);


    /**
     *  Sets the error message.
     *
     *  @param message an error message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public void setErrorMessage(org.osid.locale.DisplayText message);
}       


