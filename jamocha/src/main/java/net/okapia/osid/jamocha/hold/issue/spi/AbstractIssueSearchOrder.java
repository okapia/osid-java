//
// AbstractIssueSearchOdrer.java
//
//     Defines an IssueSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.issue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code IssueSearchOrder}.
 */

public abstract class AbstractIssueSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.hold.IssueSearchOrder {

    private final java.util.Collection<org.osid.hold.records.IssueSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBureau(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBureauSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getBureauSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBureauSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  issueRecordType an issue record type 
     *  @return {@code true} if the issueRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type issueRecordType) {
        for (org.osid.hold.records.IssueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  issueRecordType the issue record type 
     *  @return the issue search order record
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(issueRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.hold.records.IssueSearchOrderRecord getIssueSearchOrderRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.IssueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this issue. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param issueRecord the issue search odrer record
     *  @param issueRecordType issue record type
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecord} or
     *          {@code issueRecordTypeissue} is
     *          {@code null}
     */
            
    protected void addIssueRecord(org.osid.hold.records.IssueSearchOrderRecord issueSearchOrderRecord, 
                                     org.osid.type.Type issueRecordType) {

        addRecordType(issueRecordType);
        this.records.add(issueSearchOrderRecord);
        
        return;
    }
}
