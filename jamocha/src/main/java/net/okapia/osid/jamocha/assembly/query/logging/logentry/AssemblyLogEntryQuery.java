//
// AssemblyLogEntryQuery.java
//
//     A LogEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.logging.logentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A LogEntryQuery that stores terms.
 */

public final class AssemblyLogEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.logging.logentry.spi.AbstractAssemblyLogEntryQuery
    implements org.osid.logging.LogEntryQuery,
               org.osid.logging.LogEntryQueryInspector,
               org.osid.logging.LogEntrySearchOrder {


    /** 
     *  Constructs a new <code>AssemblyLogEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    public AssemblyLogEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }


    /**
     *  Gets the query assembler.
     *
     *  @return the query assembler
     */

    public net.okapia.osid.jamocha.assembly.query.QueryAssembler getAssembler() {
        return (super.getAssembler());
    }
}
