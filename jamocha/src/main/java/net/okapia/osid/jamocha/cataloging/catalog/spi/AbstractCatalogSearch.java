//
// AbstractCatalogSearch.java
//
//     A template for making a Catalog Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.catalog.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing catalog searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCatalogSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.cataloging.CatalogSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.cataloging.records.CatalogSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.cataloging.CatalogSearchOrder catalogSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of catalogs. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  catalogIds list of catalogs
     *  @throws org.osid.NullArgumentException
     *          <code>catalogIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCatalogs(org.osid.id.IdList catalogIds) {
        while (catalogIds.hasNext()) {
            try {
                this.ids.add(catalogIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCatalogs</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of catalog Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCatalogIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  catalogSearchOrder catalog search order 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>catalogSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCatalogResults(org.osid.cataloging.CatalogSearchOrder catalogSearchOrder) {
	this.catalogSearchOrder = catalogSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.cataloging.CatalogSearchOrder getCatalogSearchOrder() {
	return (this.catalogSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given catalog search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a catalog implementing the requested record.
     *
     *  @param catalogSearchRecordType a catalog search record
     *         type
     *  @return the catalog search record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.records.CatalogSearchRecord getCatalogSearchRecord(org.osid.type.Type catalogSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.cataloging.records.CatalogSearchRecord record : this.records) {
            if (record.implementsRecordType(catalogSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalog search. 
     *
     *  @param catalogSearchRecord catalog search record
     *  @param catalogSearchRecordType catalog search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogSearchRecord(org.osid.cataloging.records.CatalogSearchRecord catalogSearchRecord, 
                                           org.osid.type.Type catalogSearchRecordType) {

        addRecordType(catalogSearchRecordType);
        this.records.add(catalogSearchRecord);        
        return;
    }
}
