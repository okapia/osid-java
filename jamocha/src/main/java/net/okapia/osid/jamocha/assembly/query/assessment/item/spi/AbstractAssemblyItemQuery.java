//
// AbstractAssemblyItemQuery.java
//
//     An ItemQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ItemQuery that stores terms.
 */

public abstract class AbstractAssemblyItemQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.assessment.ItemQuery,
               org.osid.assessment.ItemQueryInspector,
               org.osid.assessment.ItemSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.ItemQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.ItemQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.ItemSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyItemQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyItemQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the learning objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId a learning objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getLearningObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears all learning objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        getAssembler().clearTerms(getLearningObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the learning objective <code> Id </code> query terms. 
     *
     *  @return the learning objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getLearningObjectiveIdColumn()));
    }


    /**
     *  Gets the LearningObjectiveId column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveIdColumn() {
        return ("learning_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if a learning objective query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the learning objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches an item with any objective. 
     *
     *  @param  match <code> true </code> to match items with any learning 
     *          objective, <code> false </code> to match items with no 
     *          learning objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getLearningObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears all learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        getAssembler().clearTerms(getLearningObjectiveColumn());
        return;
    }


    /**
     *  Gets the learning objective query terms. 
     *
     *  @return the learning objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the LearningObjective column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveColumn() {
        return ("learning_objective");
    }


    /**
     *  Sets the question <code> Id </code> for this query. 
     *
     *  @param  questionId a question <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> questionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQuestionId(org.osid.id.Id questionId, boolean match) {
        getAssembler().addIdTerm(getQuestionIdColumn(), questionId, match);
        return;
    }


    /**
     *  Clears all question <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearQuestionIdTerms() {
        getAssembler().clearTerms(getQuestionIdColumn());
        return;
    }


    /**
     *  Gets the question <code> Id </code> query terms. 
     *
     *  @return the question <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQuestionIdTerms() {
        return (getAssembler().getIdTerms(getQuestionIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the question. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuestion(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQuestionColumn(), style);
        return;
    }


    /**
     *  Gets the QuestionId column name.
     *
     * @return the column name
     */

    protected String getQuestionIdColumn() {
        return ("question_id");
    }


    /**
     *  Tests if a <code> QuestionQuery </code> is available. 
     *
     *  @return <code> true </code> if a question query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQuestionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a question. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the question query 
     *  @throws org.osid.UnimplementedException <code> supportsQuestionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.QuestionQuery getQuestionQuery() {
        throw new org.osid.UnimplementedException("supportsQuestionQuery() is false");
    }


    /**
     *  Matches an item with any question. 
     *
     *  @param  match <code> true </code> to match items with any question, 
     *          <code> false </code> to match items with no questions 
     */

    @OSID @Override
    public void matchAnyQuestion(boolean match) {
        getAssembler().addIdWildcardTerm(getQuestionColumn(), match);
        return;
    }


    /**
     *  Clears all question terms. 
     */

    @OSID @Override
    public void clearQuestionTerms() {
        getAssembler().clearTerms(getQuestionColumn());
        return;
    }


    /**
     *  Gets the question query terms. 
     *
     *  @return the question terms 
     */

    @OSID @Override
    public org.osid.assessment.QuestionQueryInspector[] getQuestionTerms() {
        return (new org.osid.assessment.QuestionQueryInspector[0]);
    }


    /**
     *  Tests if a question search order is available. 
     *
     *  @return <code> true </code> if a question search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQuestionSearchOrder() {
        return (false);
    }


    /**
     *  Gets a question search order. 
     *
     *  @return a question search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQuestionSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.QuestionSearchOrder getQuestionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQuestionSearchOrder() is false");
    }


    /**
     *  Gets the Question column name.
     *
     * @return the column name
     */

    protected String getQuestionColumn() {
        return ("question");
    }


    /**
     *  Sets the answer <code> Id </code> for this query. 
     *
     *  @param  answerId an answer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> answerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAnswerId(org.osid.id.Id answerId, boolean match) {
        getAssembler().addIdTerm(getAnswerIdColumn(), answerId, match);
        return;
    }


    /**
     *  Clears all answer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAnswerIdTerms() {
        getAssembler().clearTerms(getAnswerIdColumn());
        return;
    }


    /**
     *  Gets the answer <code> Id </code> query terms. 
     *
     *  @return the answer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAnswerIdTerms() {
        return (getAssembler().getIdTerms(getAnswerIdColumn()));
    }


    /**
     *  Gets the AnswerId column name.
     *
     * @return the column name
     */

    protected String getAnswerIdColumn() {
        return ("answer_id");
    }


    /**
     *  Tests if an <code> AnswerQuery </code> is available. 
     *
     *  @return <code> true </code> if an answer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAnswerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an answer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the answer query 
     *  @throws org.osid.UnimplementedException <code> supportsAnswerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AnswerQuery getAnswerQuery() {
        throw new org.osid.UnimplementedException("supportsAnswerQuery() is false");
    }


    /**
     *  Matches an item with any answer. 
     *
     *  @param  match <code> true </code> to match items with any answer, 
     *          <code> false </code> to match items with no answers 
     */

    @OSID @Override
    public void matchAnyAnswer(boolean match) {
        getAssembler().addIdWildcardTerm(getAnswerColumn(), match);
        return;
    }


    /**
     *  Clears all answer terms. 
     */

    @OSID @Override
    public void clearAnswerTerms() {
        getAssembler().clearTerms(getAnswerColumn());
        return;
    }


    /**
     *  Gets the answer query terms. 
     *
     *  @return the answer terms 
     */

    @OSID @Override
    public org.osid.assessment.AnswerQueryInspector[] getAnswerTerms() {
        return (new org.osid.assessment.AnswerQueryInspector[0]);
    }


    /**
     *  Gets the Answer column name.
     *
     * @return the column name
     */

    protected String getAnswerColumn() {
        return ("answer");
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches an item with any assessment. 
     *
     *  @param  match <code> true </code> to match items with any assessment, 
     *          <code> false </code> to match items with no assessments 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentColumn(), match);
        return;
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        getAssembler().clearTerms(getBankIdColumn());
        return;
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (getAssembler().getIdTerms(getBankIdColumn()));
    }


    /**
     *  Gets the BankId column name.
     *
     * @return the column name
     */

    protected String getBankIdColumn() {
        return ("bank_id");
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        getAssembler().clearTerms(getBankColumn());
        return;
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the Bank column name.
     *
     * @return the column name
     */

    protected String getBankColumn() {
        return ("bank");
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.assessment.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.ItemQueryInspectorRecord getItemQueryInspectorRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.ItemQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param itemRecordType the item record type
     *  @return the item search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.ItemSearchOrderRecord getItemSearchOrderRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.ItemSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this item. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemQueryRecord the item query record
     *  @param itemQueryInspectorRecord the item query inspector
     *         record
     *  @param itemSearchOrderRecord the item search order record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          <code>itemQueryRecord</code>,
     *          <code>itemQueryInspectorRecord</code>,
     *          <code>itemSearchOrderRecord</code> or
     *          <code>itemRecordTypeitem</code> is
     *          <code>null</code>
     */
            
    protected void addItemRecords(org.osid.assessment.records.ItemQueryRecord itemQueryRecord, 
                                      org.osid.assessment.records.ItemQueryInspectorRecord itemQueryInspectorRecord, 
                                      org.osid.assessment.records.ItemSearchOrderRecord itemSearchOrderRecord, 
                                      org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);

        nullarg(itemQueryRecord, "item query record");
        nullarg(itemQueryInspectorRecord, "item query inspector record");
        nullarg(itemSearchOrderRecord, "item search odrer record");

        this.queryRecords.add(itemQueryRecord);
        this.queryInspectorRecords.add(itemQueryInspectorRecord);
        this.searchOrderRecords.add(itemSearchOrderRecord);
        
        return;
    }
}
