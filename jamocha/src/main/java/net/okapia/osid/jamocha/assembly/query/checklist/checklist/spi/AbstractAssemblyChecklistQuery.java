//
// AbstractAssemblyChecklistQuery.java
//
//     A ChecklistQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.checklist.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ChecklistQuery that stores terms.
 */

public abstract class AbstractAssemblyChecklistQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.checklist.ChecklistQuery,
               org.osid.checklist.ChecklistQueryInspector,
               org.osid.checklist.ChecklistSearchOrder {

    private final java.util.Collection<org.osid.checklist.records.ChecklistQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.checklist.records.ChecklistQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.checklist.records.ChecklistSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyChecklistQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyChecklistQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the todo <code> Id </code> for this query to match todos assigned 
     *  to checklists. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTodoId(org.osid.id.Id todoId, boolean match) {
        getAssembler().addIdTerm(getTodoIdColumn(), todoId, match);
        return;
    }


    /**
     *  Clears the todo <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTodoIdTerms() {
        getAssembler().clearTerms(getTodoIdColumn());
        return;
    }


    /**
     *  Gets the todo <code> Id </code> terms. 
     *
     *  @return the todo <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTodoIdTerms() {
        return (getAssembler().getIdTerms(getTodoIdColumn()));
    }


    /**
     *  Gets the TodoId column name.
     *
     * @return the column name
     */

    protected String getTodoIdColumn() {
        return ("todo_id");
    }


    /**
     *  Tests if a todo query is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getTodoQuery() {
        throw new org.osid.UnimplementedException("supportsTodoQuery() is false");
    }


    /**
     *  Matches checklists with any todo. 
     *
     *  @param  match <code> true </code> to match checklists with any todo, 
     *          <code> false </code> to match checklists with no todos 
     */

    @OSID @Override
    public void matchAnyTodo(boolean match) {
        getAssembler().addIdWildcardTerm(getTodoColumn(), match);
        return;
    }


    /**
     *  Clears the todo terms. 
     */

    @OSID @Override
    public void clearTodoTerms() {
        getAssembler().clearTerms(getTodoColumn());
        return;
    }


    /**
     *  Gets the todo terms. 
     *
     *  @return the todo terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the Todo column name.
     *
     * @return the column name
     */

    protected String getTodoColumn() {
        return ("todo");
    }


    /**
     *  Sets the checklist <code> Id </code> for this query to match 
     *  checklists that have the specified checklist as an ancestor. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, a <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorChecklistId(org.osid.id.Id checklistId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorChecklistIdColumn(), checklistId, match);
        return;
    }


    /**
     *  Clears the ancestor checklist <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorChecklistIdTerms() {
        getAssembler().clearTerms(getAncestorChecklistIdColumn());
        return;
    }


    /**
     *  Gets the ancestor checklist <code> Id </code> terms. 
     *
     *  @return the ancestor checklist <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorChecklistIdTerms() {
        return (getAssembler().getIdTerms(getAncestorChecklistIdColumn()));
    }


    /**
     *  Gets the AncestorChecklistId column name.
     *
     * @return the column name
     */

    protected String getAncestorChecklistIdColumn() {
        return ("ancestor_checklist_id");
    }


    /**
     *  Tests if a <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorChecklistQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getAncestorChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorChecklistQuery() is false");
    }


    /**
     *  Matches checklists with any ancestor. 
     *
     *  @param  match <code> true </code> to match checklists with any 
     *          ancestor, <code> false </code> to match root checklists 
     */

    @OSID @Override
    public void matchAnyAncestorChecklist(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorChecklistColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor checklist terms. 
     */

    @OSID @Override
    public void clearAncestorChecklistTerms() {
        getAssembler().clearTerms(getAncestorChecklistColumn());
        return;
    }


    /**
     *  Gets the ancestor checklist terms. 
     *
     *  @return the ancestor checklist terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getAncestorChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }


    /**
     *  Gets the AncestorChecklist column name.
     *
     * @return the column name
     */

    protected String getAncestorChecklistColumn() {
        return ("ancestor_checklist");
    }


    /**
     *  Sets the checklist <code> Id </code> for this query to match 
     *  checklists that have the specified checklist as a descendant. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantChecklistId(org.osid.id.Id checklistId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantChecklistIdColumn(), checklistId, match);
        return;
    }


    /**
     *  Clears the descendant checklist <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantChecklistIdTerms() {
        getAssembler().clearTerms(getDescendantChecklistIdColumn());
        return;
    }


    /**
     *  Gets the descendant checklist <code> Id </code> terms. 
     *
     *  @return the descendant checklist <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantChecklistIdTerms() {
        return (getAssembler().getIdTerms(getDescendantChecklistIdColumn()));
    }


    /**
     *  Gets the DescendantChecklistId column name.
     *
     * @return the column name
     */

    protected String getDescendantChecklistIdColumn() {
        return ("descendant_checklist_id");
    }


    /**
     *  Tests if a <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantChecklistQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getDescendantChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantChecklistQuery() is false");
    }


    /**
     *  Matches checklists with any descendant. 
     *
     *  @param  match <code> true </code> to match checklists with any 
     *          descendant, <code> false </code> to match leaf checklists 
     */

    @OSID @Override
    public void matchAnyDescendantChecklist(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantChecklistColumn(), match);
        return;
    }


    /**
     *  Clears the descendant checklist terms. 
     */

    @OSID @Override
    public void clearDescendantChecklistTerms() {
        getAssembler().clearTerms(getDescendantChecklistColumn());
        return;
    }


    /**
     *  Gets the descendant checklist terms. 
     *
     *  @return the descendant checklist terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getDescendantChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }


    /**
     *  Gets the DescendantChecklist column name.
     *
     * @return the column name
     */

    protected String getDescendantChecklistColumn() {
        return ("descendant_checklist");
    }


    /**
     *  Tests if this checklist supports the given record
     *  <code>Type</code>.
     *
     *  @param  checklistRecordType a checklist record type 
     *  @return <code>true</code> if the checklistRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type checklistRecordType) {
        for (org.osid.checklist.records.ChecklistQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  checklistRecordType the checklist record type 
     *  @return the checklist query record 
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checklistRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.ChecklistQueryRecord getChecklistQueryRecord(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.ChecklistQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checklistRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  checklistRecordType the checklist record type 
     *  @return the checklist query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checklistRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.ChecklistQueryInspectorRecord getChecklistQueryInspectorRecord(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.ChecklistQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checklistRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param checklistRecordType the checklist record type
     *  @return the checklist search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checklistRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.ChecklistSearchOrderRecord getChecklistSearchOrderRecord(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.ChecklistSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(checklistRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checklistRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this checklist. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param checklistQueryRecord the checklist query record
     *  @param checklistQueryInspectorRecord the checklist query inspector
     *         record
     *  @param checklistSearchOrderRecord the checklist search order record
     *  @param checklistRecordType checklist record type
     *  @throws org.osid.NullArgumentException
     *          <code>checklistQueryRecord</code>,
     *          <code>checklistQueryInspectorRecord</code>,
     *          <code>checklistSearchOrderRecord</code> or
     *          <code>checklistRecordTypechecklist</code> is
     *          <code>null</code>
     */
            
    protected void addChecklistRecords(org.osid.checklist.records.ChecklistQueryRecord checklistQueryRecord, 
                                      org.osid.checklist.records.ChecklistQueryInspectorRecord checklistQueryInspectorRecord, 
                                      org.osid.checklist.records.ChecklistSearchOrderRecord checklistSearchOrderRecord, 
                                      org.osid.type.Type checklistRecordType) {

        addRecordType(checklistRecordType);

        nullarg(checklistQueryRecord, "checklist query record");
        nullarg(checklistQueryInspectorRecord, "checklist query inspector record");
        nullarg(checklistSearchOrderRecord, "checklist search odrer record");

        this.queryRecords.add(checklistQueryRecord);
        this.queryInspectorRecords.add(checklistQueryInspectorRecord);
        this.searchOrderRecords.add(checklistSearchOrderRecord);
        
        return;
    }
}
