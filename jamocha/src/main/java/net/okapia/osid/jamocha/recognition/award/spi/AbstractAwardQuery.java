//
// AbstractAwardQuery.java
//
//     A template for making an Award Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.award.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for awards.
 */

public abstract class AbstractAwardQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.recognition.AwardQuery {

    private final java.util.Collection<org.osid.recognition.records.AwardQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the conferral <code> Id </code> for this query to match 
     *  conferrals assigned to awards. 
     *
     *  @param  conferralId a conferral <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> conferralId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConferralId(org.osid.id.Id conferralId, boolean match) {
        return;
    }


    /**
     *  Clears the conferral <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConferralIdTerms() {
        return;
    }


    /**
     *  Tests if a conferral query is available. 
     *
     *  @return <code> true </code> if a conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award. 
     *
     *  @return the conferral query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuery getConferralQuery() {
        throw new org.osid.UnimplementedException("supportsConferralQuery() is false");
    }


    /**
     *  Matches awards with any conferral. 
     *
     *  @param  match <code> true </code> to match awards with any conferral, 
     *          <code> false </code> to match awards with no conferrals 
     */

    @OSID @Override
    public void matchAnyConferral(boolean match) {
        return;
    }


    /**
     *  Clears the conferral terms. 
     */

    @OSID @Override
    public void clearConferralTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match conferrals 
     *  assigned to academies. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAcademyId(org.osid.id.Id academyId, boolean match) {
        return;
    }


    /**
     *  Clears the academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAcademyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAcademyQuery() is false");
    }


    /**
     *  Clears the academy terms. 
     */

    @OSID @Override
    public void clearAcademyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given award query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an award implementing the requested record.
     *
     *  @param awardRecordType an award record type
     *  @return the award query record
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AwardQueryRecord getAwardQueryRecord(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AwardQueryRecord record : this.records) {
            if (record.implementsRecordType(awardRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardRecordType + " is not supported");
    }


    /**
     *  Adds a record to this award query. 
     *
     *  @param awardQueryRecord award query record
     *  @param awardRecordType award record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAwardQueryRecord(org.osid.recognition.records.AwardQueryRecord awardQueryRecord, 
                                          org.osid.type.Type awardRecordType) {

        addRecordType(awardRecordType);
        nullarg(awardQueryRecord, "award query record");
        this.records.add(awardQueryRecord);        
        return;
    }
}
