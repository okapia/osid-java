//
// StepElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.step.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class StepElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the StepElement Id.
     *
     *  @return the step element Id
     */

    public static org.osid.id.Id getStepEntityId() {
        return (makeEntityId("osid.workflow.Step"));
    }


    /**
     *  Gets the ProcessId element Id.
     *
     *  @return the ProcessId element Id
     */

    public static org.osid.id.Id getProcessId() {
        return (makeElementId("osid.workflow.step.ProcessId"));
    }


    /**
     *  Gets the Process element Id.
     *
     *  @return the Process element Id
     */

    public static org.osid.id.Id getProcess() {
        return (makeElementId("osid.workflow.step.Process"));
    }


    /**
     *  Gets the ResourceIds element Id.
     *
     *  @return the ResourceIds element Id
     */

    public static org.osid.id.Id getResourceIds() {
        return (makeElementId("osid.workflow.step.ResourceIds"));
    }


    /**
     *  Gets the Resources element Id.
     *
     *  @return the Resources element Id
     */

    public static org.osid.id.Id getResources() {
        return (makeElementId("osid.workflow.step.Resources"));
    }


    /**
     *  Gets the InputStateIds element Id.
     *
     *  @return the InputStateIds element Id
     */

    public static org.osid.id.Id getInputStateIds() {
        return (makeElementId("osid.workflow.step.InputStateIds"));
    }


    /**
     *  Gets the InputStates element Id.
     *
     *  @return the InputStates element Id
     */

    public static org.osid.id.Id getInputStates() {
        return (makeElementId("osid.workflow.step.InputStates"));
    }


    /**
     *  Gets the NextStateId element Id.
     *
     *  @return the NextStateId element Id
     */

    public static org.osid.id.Id getNextStateId() {
        return (makeElementId("osid.workflow.step.NextStateId"));
    }


    /**
     *  Gets the NextState element Id.
     *
     *  @return the NextState element Id
     */

    public static org.osid.id.Id getNextState() {
        return (makeElementId("osid.workflow.step.NextState"));
    }


    /**
     *  Gets the Initial element Id.
     *
     *  @return the Initial element Id
     */

    public static org.osid.id.Id getInitial() {
        return (makeQueryElementId("osid.workflow.step.Initial"));
    }


    /**
     *  Gets the Terminal element Id.
     *
     *  @return the Terminal element Id
     */

    public static org.osid.id.Id getTerminal() {
        return (makeQueryElementId("osid.workflow.step.Terminal"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeQueryElementId("osid.workflow.step.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeQueryElementId("osid.workflow.step.Work"));
    }


    /**
     *  Gets the OfficeId element Id.
     *
     *  @return the OfficeId element Id
     */

    public static org.osid.id.Id getOfficeId() {
        return (makeQueryElementId("osid.workflow.step.OfficeId"));
    }


    /**
     *  Gets the Office element Id.
     *
     *  @return the Office element Id
     */

    public static org.osid.id.Id getOffice() {
        return (makeQueryElementId("osid.workflow.step.Office"));
    }
}
