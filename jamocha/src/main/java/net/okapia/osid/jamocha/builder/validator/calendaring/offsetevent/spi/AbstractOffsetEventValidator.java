//
// AbstractOffsetEventValidator.java
//
//     Validates an OffsetEvent.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.calendaring.offsetevent.spi;


/**
 *  Validates an OffsetEvent.
 */

public abstract class AbstractOffsetEventValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractOffsetEventValidator</code>.
     */

    protected AbstractOffsetEventValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractOffsetEventValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractOffsetEventValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an OffsetEvent.
     *
     *  @param offsetEvent an offset event to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>offsetEvent</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.calendaring.OffsetEvent offsetEvent) {
        super.validate(offsetEvent);

        testConditionalMethod(offsetEvent, "getFixedStartTime", offsetEvent.hasFixedStartTime(), "hasFixedStartTime()");
        testConditionalMethod(offsetEvent, "getStartReferenceEventId", !offsetEvent.hasFixedStartTime(), "hasFixedStartTime()");
        testConditionalMethod(offsetEvent, "getStartReferenceEvent", !offsetEvent.hasFixedStartTime(), "hasFixedStartTime()");
        if (!offsetEvent.hasFixedStartTime()) {
            testNestedObject(offsetEvent, "getStartReferenceEvent");
        }

        testConditionalMethod(offsetEvent, "getFixedStartOffset", offsetEvent.hasFixedStartOffset(), "hasFixedStartOffset()");
        testConditionalMethod(offsetEvent, "getRelativeWeekdayStartOffset", offsetEvent.hasRelativeWeekdayStartOffset(), "hasRelativeWeekdayStartOffset()");
        testConditionalMethod(offsetEvent, "getRelativeStartWeekday", offsetEvent.hasRelativeWeekdayStartOffset(), "hasRelativeWeekdayStartOffset()");

        //        testConditionalMethod(offsetEvent, "getDuration", offsetEvent.hasFixedDuration(), "hasFixedDuration()");
        testConditionalMethod(offsetEvent, "getEndReferenceEventId", !offsetEvent.hasFixedDuration(), "!hasFixedDuration()");
        testConditionalMethod(offsetEvent, "getEndReferenceEvent", !offsetEvent.hasFixedDuration(), "!hasFixedDuration()");

        if (!offsetEvent.hasFixedDuration()) {
            testNestedObject(offsetEvent, "getEndReferenceEvent");
        }

        testConditionalMethod(offsetEvent, "getFixedEndOffset", offsetEvent.hasFixedEndOffset(), "!hasFixedEndOffset()");
        testConditionalMethod(offsetEvent, "getRelativeWeekdayEndOffset", offsetEvent.hasRelativeWeekdayEndOffset(), "hasRelativeWeekdayEndOffset()");
        testConditionalMethod(offsetEvent, "getRelativeEndWeekday", offsetEvent.hasRelativeWeekdayEndOffset(), "hasRelativeWeekdayEndOffset()");


        test(offsetEvent.getLocationDescription(), "getLocationDescription()");
        testConditionalMethod(offsetEvent, "getLocationId", offsetEvent.hasLocation(), "hasLocation()");
        testConditionalMethod(offsetEvent, "getLocation", offsetEvent.hasLocation(), "hasLocation()");
        if (offsetEvent.hasLocation()) {
            testNestedObject(offsetEvent, "getLocation");
        }

        testConditionalMethod(offsetEvent, "getSponsorIds", offsetEvent.hasSponsors(), "hasSponsors()");
        testConditionalMethod(offsetEvent, "getSponsors", offsetEvent.hasSponsors(), "hasSponsors()");

        if (offsetEvent.hasSponsors()) {
            testNestedObjects(offsetEvent, "getSponsorIds", "getSponsors");
        }

        if (offsetEvent.hasFixedStartOffset() && offsetEvent.hasFixedStartTime()) {
            throw new org.osid.BadLogicException("hasFixedStartOffset() and hasFixedStartTime() are both true");
        }

        if (offsetEvent.hasRelativeWeekdayStartOffset() && offsetEvent.hasFixedStartTime()) {
            throw new org.osid.BadLogicException("hasRelativeWeekdayStartOffset() and hasFixedStartTime() are both true");
        }

        if (offsetEvent.hasFixedDuration() && offsetEvent.hasFixedEndOffset()) {
            throw new org.osid.BadLogicException("hasFixedDuration() and hasFixedEndOffset() are both true");
        }

        if (offsetEvent.hasRelativeWeekdayEndOffset() && offsetEvent.hasFixedEndOffset()) {
            throw new org.osid.BadLogicException("hasRelativeWeekdayEndOffset() and hasFixedEndOffset() are both true");
        }

        return;
    }
}
