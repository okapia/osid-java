//
// SpeedZoneMiter.java
//
//     Defines a SpeedZone miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.speedzone;


/**
 *  Defines a <code>SpeedZone</code> miter for use with the builders.
 */

public interface SpeedZoneMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.mapping.path.SpeedZone {


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public void setPath(org.osid.mapping.path.Path path);


    /**
     *  Sets the starting coordinate.
     *
     *  @param coordinate a starting coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public void setStartingCoordinate(org.osid.mapping.Coordinate coordinate);


    /**
     *  Sets the ending coordinate.
     *
     *  @param coordinate an ending coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public void setEndingCoordinate(org.osid.mapping.Coordinate coordinate);


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this speed zone is
     *         implicit, <code> false </code> if explicitly managed
     */

    public void setImplicit(boolean implicit);


    /**
     *  Sets the speed limit.
     *
     *  @param limit a speed limit
     *  @throws org.osid.NullArgumentException <code>limit</code> is
     *          <code>null</code>
     */

    public void setSpeedLimit(org.osid.mapping.Speed limit);


    /**
     *  Adds a SpeedZone record.
     *
     *  @param record a speedZone record
     *  @param recordType the type of speedZone record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSpeedZoneRecord(org.osid.mapping.path.records.SpeedZoneRecord record, org.osid.type.Type recordType);
}       


