//
// AbstractBlockQueryInspector.java
//
//     A template for making a BlockQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.block.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for blocks.
 */

public abstract class AbstractBlockQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.hold.BlockQueryInspector {

    private final java.util.Collection<org.osid.hold.records.BlockQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.hold.IssueQueryInspector[0]);
    }


    /**
     *  Gets the oubliette <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOublietteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the oubliette query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.OublietteQueryInspector[] getOublietteTerms() {
        return (new org.osid.hold.OublietteQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given block query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a block implementing the requested record.
     *
     *  @param blockRecordType a block record type
     *  @return the block query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blockRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.BlockQueryInspectorRecord getBlockQueryInspectorRecord(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.BlockQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(blockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blockRecordType + " is not supported");
    }


    /**
     *  Adds a record to this block query. 
     *
     *  @param blockQueryInspectorRecord block query inspector
     *         record
     *  @param blockRecordType block record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBlockQueryInspectorRecord(org.osid.hold.records.BlockQueryInspectorRecord blockQueryInspectorRecord, 
                                                   org.osid.type.Type blockRecordType) {

        addRecordType(blockRecordType);
        nullarg(blockRecordType, "block record type");
        this.records.add(blockQueryInspectorRecord);        
        return;
    }
}
