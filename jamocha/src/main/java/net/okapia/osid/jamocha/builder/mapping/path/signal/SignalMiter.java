//
// SignalMiter.java
//
//     Defines a Signal miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.signal;


/**
 *  Defines a <code>Signal</code> miter for use with the builders.
 */

public interface SignalMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.mapping.path.Signal {


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public void setPath(org.osid.mapping.path.Path path);


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate a coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public void setCoordinate(org.osid.mapping.Coordinate coordinate);


    /**
     *  Adds a state.
     *
     *  @param state a state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public void addState(org.osid.process.State state);


    /**
     *  Sets all the states.
     *
     *  @param states a collection of states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    public void setStates(java.util.Collection<org.osid.process.State> states);


    /**
     *  Adds a Signal record.
     *
     *  @param record a signal record
     *  @param recordType the type of signal record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSignalRecord(org.osid.mapping.path.records.SignalRecord record, org.osid.type.Type recordType);
}       


