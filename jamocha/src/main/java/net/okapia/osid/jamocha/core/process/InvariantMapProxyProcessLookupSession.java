//
// InvariantMapProxyProcessLookupSession
//
//    Implements a Process lookup service backed by a fixed
//    collection of processes. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process;


/**
 *  Implements a Process lookup service backed by a fixed
 *  collection of processes. The processes are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyProcessLookupSession
    extends net.okapia.osid.jamocha.core.process.spi.AbstractMapProcessLookupSession
    implements org.osid.process.ProcessLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProcessLookupSession} with no
     *  processes.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyProcessLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyProcessLookupSession} with a
     *  single process.
     *
     *  @param process a single process
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code process} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProcessLookupSession(org.osid.process.Process process, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putProcess(process);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyProcessLookupSession} using
     *  an array of processes.
     *
     *  @param processes an array of processes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code processes} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProcessLookupSession(org.osid.process.Process[] processes, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putProcesses(processes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProcessLookupSession} using a
     *  collection of processes.
     *
     *  @param processes a collection of processes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code processes} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProcessLookupSession(java.util.Collection<? extends org.osid.process.Process> processes,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putProcesses(processes);
        return;
    }
}
