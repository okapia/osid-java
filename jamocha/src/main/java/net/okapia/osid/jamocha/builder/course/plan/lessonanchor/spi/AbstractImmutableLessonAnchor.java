//
// AbstractImmutableLessonAnchor.java
//
//     Wraps a mutable LessonAnchor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.lessonanchor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>LessonAnchor</code> to hide modifiers. This
 *  wrapper provides an immutized LessonAnchor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying lesson anchor whose state changes are visible.
 */

public abstract class AbstractImmutableLessonAnchor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutable
    implements org.osid.course.plan.LessonAnchor {

    private final org.osid.course.plan.LessonAnchor lessonAnchor;


    /**
     *  Constructs a new <code>AbstractImmutableLessonAnchor</code>.
     *
     *  @param lessonAnchor the lessonAnchor to immutablize
     *  @throws org.osid.NullArgumentException <code>lessonAnchor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableLessonAnchor(org.osid.course.plan.LessonAnchor lessonAnchor) {
        super(lessonAnchor);
        this.lessonAnchor = lessonAnchor;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the lesson. 
     *
     *  @return the lesson <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLessonId() {
        return (this.lessonAnchor.getLessonId());
    }


    /**
     *  Gets the lesson. 
     *
     *  @return the lesson 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.plan.Lesson getLesson()
        throws org.osid.OperationFailedException {

        return (this.lessonAnchor.getLesson());
    }


    /**
     *  Gets the <code> Id </code> of the activity. 
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.lessonAnchor.getActivityId());
    }


    /**
     *  Gets the activity. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.lessonAnchor.getActivity());
    }


    /**
     *  Gets the time offset from the start of the acttvity where the anchor 
     *  exists. 
     *
     *  @return a duration from the start of the activity 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTime() {
        return (this.lessonAnchor.getTime());
    }
}
