//
// AbstractMutableProvision.java
//
//     Defines a mutable Provision.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Provision</code>.
 */

public abstract class AbstractMutableProvision
    extends net.okapia.osid.jamocha.provisioning.provision.spi.AbstractProvision
    implements org.osid.provisioning.Provision,
               net.okapia.osid.jamocha.builder.provisioning.provision.ProvisionMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this provision. 
     *
     *  @param record provision record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addProvisionRecord(org.osid.provisioning.records.ProvisionRecord record, org.osid.type.Type recordType) {
        super.addProvisionRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this provision is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this provision ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this provision.
     *
     *  @param displayName the name for this provision
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this provision.
     *
     *  @param description the description of this provision
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this provision
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException <code>broker</code> is
     *          <code>null</code>
     */

    @Override
    public void setBroker(org.osid.provisioning.Broker broker) {
        super.setBroker(broker);
        return;
    }


    /**
     *  Sets the provisionable.
     *
     *  @param provisionable a provisionable
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    @Override
    public void setProvisionable(org.osid.provisioning.Provisionable provisionable) {
        super.setProvisionable(provisionable);
        return;
    }


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException <code>recipient</code> is
     *          <code>null</code>
     */

    @Override
    public void setRecipient(org.osid.resource.Resource recipient) {
        super.setRecipient(recipient);
        return;
    }


    /**
     *  Sets the request.
     *
     *  @param request a request
     *  @throws org.osid.NullArgumentException <code>request</code> is
     *          <code>null</code>
     */

    @Override
    public void setRequest(org.osid.provisioning.Request request) {
        super.setRequest(request);
        return;
    }


    /**
     *  Sets the provision date.
     *
     *  @param date a provision date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setProvisionDate(org.osid.calendaring.DateTime date) {
        super.setProvisionDate(date);
        return;
    }


    /**
     *  Sets the leased flag.
     *
     *  @param leased <code>true</code> if this is a lease,
     *          <code>false</code> if the provision is permanent
     */

    @Override
    public void setLeased(boolean leased) {
        super.setLeased(leased);
        return;
    }


    /**
     * Sets the must return flag.
     *
     *  @param mustReturn <code>true</code> if this is must be
     *          returned, <code>false</code> otherwise
     */

    @Override
    public void setMustReturn(boolean mustReturn) {
        super.setMustReturn(mustReturn);
        return;
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setDueDate(org.osid.calendaring.DateTime date) {
        super.setDueDate(date);
        return;
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    @Override
    public void setCost(org.osid.financials.Currency cost) {
        super.setCost(cost);
        return;
    }


    /**
     *  Sets the rate amount.
     *
     *  @param amount a rate amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void setRateAmount(org.osid.financials.Currency amount) {
        super.setRateAmount(amount);
        return;
    }


    /**
     *  Sets the rate period.
     *
     *  @param period a rate period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    @Override
    public void setRatePeriod(org.osid.calendaring.Duration period) {
        super.setRatePeriod(period);
        return;
    }


    /**
     *  Sets the provision return.
     *
     *  @param provisionReturn a provision return
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturn</code> is <code>null</code>
     */

    @Override
    public void setProvisionReturn(org.osid.provisioning.ProvisionReturn provisionReturn) {
        super.setProvisionReturn(provisionReturn);
        return;
    }
}

