//
// ResultElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.result.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ResultElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the ResultElement Id.
     *
     *  @return the result element Id
     */

    public static org.osid.id.Id getResultEntityId() {
        return (makeEntityId("osid.offering.Result"));
    }


    /**
     *  Gets the ParticipantId element Id.
     *
     *  @return the ParticipantId element Id
     */

    public static org.osid.id.Id getParticipantId() {
        return (makeElementId("osid.offering.result.ParticipantId"));
    }


    /**
     *  Gets the Participant element Id.
     *
     *  @return the Participant element Id
     */

    public static org.osid.id.Id getParticipant() {
        return (makeElementId("osid.offering.result.Participant"));
    }


    /**
     *  Gets the GradeId element Id.
     *
     *  @return the GradeId element Id
     */

    public static org.osid.id.Id getGradeId() {
        return (makeElementId("osid.offering.result.GradeId"));
    }


    /**
     *  Gets the Grade element Id.
     *
     *  @return the Grade element Id
     */

    public static org.osid.id.Id getGrade() {
        return (makeElementId("osid.offering.result.Grade"));
    }


    /**
     *  Gets the Value element Id.
     *
     *  @return the Value element Id
     */

    public static org.osid.id.Id getValue() {
        return (makeElementId("osid.offering.result.Value"));
    }


    /**
     *  Gets the CatalogueId element Id.
     *
     *  @return the CatalogueId element Id
     */

    public static org.osid.id.Id getCatalogueId() {
        return (makeQueryElementId("osid.offering.result.CatalogueId"));
    }


    /**
     *  Gets the Catalogue element Id.
     *
     *  @return the Catalogue element Id
     */

    public static org.osid.id.Id getCatalogue() {
        return (makeQueryElementId("osid.offering.result.Catalogue"));
    }
}
