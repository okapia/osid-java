//
// AbstractPlanSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPlanSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.plan.PlanSearchResults {

    private org.osid.course.plan.PlanList plans;
    private final org.osid.course.plan.PlanQueryInspector inspector;
    private final java.util.Collection<org.osid.course.plan.records.PlanSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPlanSearchResults.
     *
     *  @param plans the result set
     *  @param planQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>plans</code>
     *          or <code>planQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPlanSearchResults(org.osid.course.plan.PlanList plans,
                                            org.osid.course.plan.PlanQueryInspector planQueryInspector) {
        nullarg(plans, "plans");
        nullarg(planQueryInspector, "plan query inspectpr");

        this.plans = plans;
        this.inspector = planQueryInspector;

        return;
    }


    /**
     *  Gets the plan list resulting from a search.
     *
     *  @return a plan list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlans() {
        if (this.plans == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.plan.PlanList plans = this.plans;
        this.plans = null;
	return (plans);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.plan.PlanQueryInspector getPlanQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  plan search record <code> Type. </code> This method must
     *  be used to retrieve a plan implementing the requested
     *  record.
     *
     *  @param planSearchRecordType a plan search 
     *         record type 
     *  @return the plan search
     *  @throws org.osid.NullArgumentException
     *          <code>planSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(planSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanSearchResultsRecord getPlanSearchResultsRecord(org.osid.type.Type planSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.plan.records.PlanSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(planSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(planSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record plan search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPlanRecord(org.osid.course.plan.records.PlanSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "plan record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
