//
// AbstractActivityQuery.java
//
//     A template for making an Activity Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for activities.
 */

public abstract class AbstractActivityQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.financials.ActivityQuery {

    private final java.util.Collection<org.osid.financials.records.ActivityQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the organization <code> Id </code> for this query. 
     *
     *  @param  organizationId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOrganizationId(org.osid.id.Id organizationId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the organization <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrganizationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsOrganizationQuery() is false");
    }


    /**
     *  Matches an activity that has any organization. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          organization, <code> false </code> to match activities with no 
     *          organization 
     */

    @OSID @Override
    public void matchAnyOrganization(boolean match) {
        return;
    }


    /**
     *  Clears the organization terms. 
     */

    @OSID @Override
    public void clearOrganizationTerms() {
        return;
    }


    /**
     *  Sets the supervisor <code> Id </code> for this query. 
     *
     *  @param  supervisorId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> supervisorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSupervisorId(org.osid.id.Id supervisorId, boolean match) {
        return;
    }


    /**
     *  Clears the supervisor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupervisorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a supervisor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupervisorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a supervisor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupervisorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSupervisorQuery() {
        throw new org.osid.UnimplementedException("supportsSupervisorQuery() is false");
    }


    /**
     *  Matches an activity that has any supervisor. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          supervisor, <code> false </code> to match activities with no 
     *          supervisor 
     */

    @OSID @Override
    public void matchAnySupervisor(boolean match) {
        return;
    }


    /**
     *  Clears the supervisor terms. 
     */

    @OSID @Override
    public void clearSupervisorTerms() {
        return;
    }


    /**
     *  Matches an activity code. 
     *
     *  @param  code a code 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any code assigned. 
     *
     *  @param  match <code> true </code> to match activities with any code, 
     *          <code> false </code> to match activities with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        return;
    }


    /**
     *  Clears the code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        return;
    }


    /**
     *  Tests if a <code> SummaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a summery query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSummaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a summary. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the summery query 
     *  @throws org.osid.UnimplementedException <code> supportsSummeryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.SummaryQuery getSummaryQuery() {
        throw new org.osid.UnimplementedException("supportsSummaryQuery() is false");
    }


    /**
     *  Clears the summary terms. 
     */

    @OSID @Override
    public void clearSummaryTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activities 
     *  that have the specified activity as an ancestor. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorActivityId(org.osid.id.Id activityId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the ancestor activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getAncestorActivityQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorActivityQuery() is false");
    }


    /**
     *  Matches activities with any activity ancestor. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          ancestor, <code> false </code> to match root activities 
     */

    @OSID @Override
    public void matchAnyAncestorActivity(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor activity query terms. 
     */

    @OSID @Override
    public void clearAncestorActivityTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activities 
     *  that have the specified activity as a descendant. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantActivityId(org.osid.id.Id activityId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the descendant activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getDescendantActivityQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantActivityQuery() is false");
    }


    /**
     *  Matches activities with any activity descendant. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          descendant, <code> false </code> to match leaf activities 
     */

    @OSID @Override
    public void matchAnyDescendantActivity(boolean match) {
        return;
    }


    /**
     *  Clears the descendant activity query terms. 
     */

    @OSID @Override
    public void clearDescendantActivityTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match activities 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given activity query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity implementing the requested record.
     *
     *  @param activityRecordType an activity record type
     *  @return the activity query record
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.ActivityQueryRecord getActivityQueryRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.ActivityQueryRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity query. 
     *
     *  @param activityQueryRecord activity query record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityQueryRecord(org.osid.financials.records.ActivityQueryRecord activityQueryRecord, 
                                          org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);
        nullarg(activityQueryRecord, "activity query record");
        this.records.add(activityQueryRecord);        
        return;
    }
}
