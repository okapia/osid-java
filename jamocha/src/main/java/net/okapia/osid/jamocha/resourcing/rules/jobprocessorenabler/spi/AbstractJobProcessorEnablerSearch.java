//
// AbstractJobProcessorEnablerSearch.java
//
//     A template for making a JobProcessorEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing job processor enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractJobProcessorEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resourcing.rules.JobProcessorEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resourcing.rules.JobProcessorEnablerSearchOrder jobProcessorEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of job processor enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  jobProcessorEnablerIds list of job processor enablers
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongJobProcessorEnablers(org.osid.id.IdList jobProcessorEnablerIds) {
        while (jobProcessorEnablerIds.hasNext()) {
            try {
                this.ids.add(jobProcessorEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongJobProcessorEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of job processor enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getJobProcessorEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  jobProcessorEnablerSearchOrder job processor enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>jobProcessorEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderJobProcessorEnablerResults(org.osid.resourcing.rules.JobProcessorEnablerSearchOrder jobProcessorEnablerSearchOrder) {
	this.jobProcessorEnablerSearchOrder = jobProcessorEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resourcing.rules.JobProcessorEnablerSearchOrder getJobProcessorEnablerSearchOrder() {
	return (this.jobProcessorEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given job processor enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a job processor enabler implementing the requested record.
     *
     *  @param jobProcessorEnablerSearchRecordType a job processor enabler search record
     *         type
     *  @return the job processor enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorEnablerSearchRecord getJobProcessorEnablerSearchRecord(org.osid.type.Type jobProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resourcing.rules.records.JobProcessorEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(jobProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job processor enabler search. 
     *
     *  @param jobProcessorEnablerSearchRecord job processor enabler search record
     *  @param jobProcessorEnablerSearchRecordType jobProcessorEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobProcessorEnablerSearchRecord(org.osid.resourcing.rules.records.JobProcessorEnablerSearchRecord jobProcessorEnablerSearchRecord, 
                                           org.osid.type.Type jobProcessorEnablerSearchRecordType) {

        addRecordType(jobProcessorEnablerSearchRecordType);
        this.records.add(jobProcessorEnablerSearchRecord);        
        return;
    }
}
