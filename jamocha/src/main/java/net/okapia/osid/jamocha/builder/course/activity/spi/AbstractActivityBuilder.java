//
// AbstractActivity.java
//
//     Defines an Activity builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activity.spi;


/**
 *  Defines an <code>Activity</code> builder.
 */

public abstract class AbstractActivityBuilder<T extends AbstractActivityBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.activity.ActivityMiter activity;


    /**
     *  Constructs a new <code>AbstractActivityBuilder</code>.
     *
     *  @param activity the activity to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActivityBuilder(net.okapia.osid.jamocha.builder.course.activity.ActivityMiter activity) {
        super(activity);
        this.activity = activity;
        return;
    }


    /**
     *  Builds the activity.
     *
     *  @return the new activity
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.Activity build() {
        (new net.okapia.osid.jamocha.builder.validator.course.activity.ActivityValidator(getValidations())).validate(this.activity);
        return (new net.okapia.osid.jamocha.builder.course.activity.ImmutableActivity(this.activity));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the activity miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.activity.ActivityMiter getMiter() {
        return (this.activity);
    }


    /**
     *  Sets the activity unit.
     *
     *  @param activityUnit an activity unit
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnit</code> is <code>null</code>
     */

    public T activityUnit(org.osid.course.ActivityUnit activityUnit) {
        getMiter().setActivityUnit(activityUnit);
        return (self());
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering the course offering
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>courseOffering</code> is
     *          <code>null</code>
     */

    public T courseOffering(org.osid.course.CourseOffering courseOffering) {
        getMiter().setCourseOffering(courseOffering);
        return (self());
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public T term(org.osid.course.Term term) {
        getMiter().setTerm(term);
        return (self());
    }


    /**
     *  Sets the implicit flag.
     *
     *  @return the builder
     */

    public T implicit() {
        getMiter().setImplicit(true);
        return (self());
    }


    /**
     *  Unsets the implicit flag.
     *
     *  @return the builder
     */

    public T explicit() {
        getMiter().setImplicit(false);
        return (self());
    }


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public T schedule(org.osid.calendaring.Schedule schedule) {
        getMiter().addSchedule(schedule);
        return (self());
    }


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    public T schedules(java.util.Collection<org.osid.calendaring.Schedule> schedules) {
        getMiter().setSchedules(schedules);
        return (self());
    }


    /**
     *  Adds a superseding activity.
     *
     *  @param activity a superseding activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T supersedingActivity(org.osid.course.Activity activity) {
        getMiter().addSupersedingActivity(activity);
        return (self());
    }


    /**
     *  Sets all the superseding activities.
     *
     *  @param activities a collection of superseding activities
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    public T supersedingActivities(java.util.Collection<org.osid.course.Activity> activities) {
        getMiter().setSupersedingActivities(activities);
        return (self());
    }


    /**
     *  Adds a specific meeting time.
     *
     *  @param time a specific meeting time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T specificMeetingTime(org.osid.calendaring.MeetingTime time) {
        getMiter().addSpecificMeetingTime(time);
        return (self());
    }


    /**
     *  Sets all the specific meeting times.
     *
     *  @param times a collection of specific meeting times
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>times</code> is
     *          <code>null</code>
     */

    public T specificMeetingTimes(java.util.Collection<org.osid.calendaring.MeetingTime> times) {
        getMiter().setSpecificMeetingTimes(times);
        return (self());
    }


    /**
     *  Adds a blackout.
     *
     *  @param blackout a blackout
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>blackout</code>
     *          is <code>null</code>
     */

    public T blackout(org.osid.calendaring.DateTimeInterval blackout) {
        getMiter().addBlackout(blackout);
        return (self());
    }


    /**
     *  Sets all the blackouts.
     *
     *  @param blackouts a collection of blackouts
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>blackouts</code>
     *          is <code>null</code>
     */

    public T blackouts(java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts) {
        getMiter().setBlackouts(blackouts);
        return (self());
    }


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>instructor</code>
     *          is <code>null</code>
     */

    public T instructor(org.osid.resource.Resource instructor) {
        getMiter().addInstructor(instructor);
        return (self());
    }


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    public T instructors(java.util.Collection<org.osid.resource.Resource> instructors) {
        getMiter().setInstructors(instructors);
        return (self());
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public T minimumSeats(long seats) {
        getMiter().setMinimumSeats(seats);
        return (self());
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public T maximumSeats(long seats) {
        getMiter().setMaximumSeats(seats);
        return (self());
    }


    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public T totalTargetEffort(org.osid.calendaring.Duration effort) {
        getMiter().setTotalTargetEffort(effort);
        return (self());
    }


    /**
     *  Sets the contact.
     *
     *  @param contact <code> true </code> if this is a contact
     *         activity, <code> false </code> if an independent
     *         activity
     */

    public T contact(boolean contact) {
        getMiter().setContact(contact);
        return (self());
    }


    /**
     *  Sets the total target contact time.
     *
     *  @param contactTime a total target contact time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    public T totalTargetContactTime(org.osid.calendaring.Duration contactTime) {
        getMiter().setTotalTargetContactTime(contactTime);
        return (self());
    }


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public T totalTargetIndividualEffort(org.osid.calendaring.Duration effort) {
        getMiter().setTotalTargetIndividualEffort(effort);
        return (self());
    }


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public T weeklyEffort(org.osid.calendaring.Duration effort) {
        getMiter().setWeeklyEffort(effort);
        return (self());
    }


    /**
     *  Sets the weekly contact time.
     *
     *  @param contactTime a weekly contact time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    public T weeklyContactTime(org.osid.calendaring.Duration contactTime) {
        getMiter().setWeeklyContactTime(contactTime);
        return (self());
    }


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public T weeklyIndividualEffort(org.osid.calendaring.Duration effort) {
        getMiter().setWeeklyIndividualEffort(effort);
        return (self());
    }


    /**
     *  Adds an Activity record.
     *
     *  @param record an activity record
     *  @param recordType the type of activity record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.records.ActivityRecord record, org.osid.type.Type recordType) {
        getMiter().addActivityRecord(record, recordType);
        return (self());
    }
}       


