//
// AbstractPriceQuery.java
//
//     A template for making a Price Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.price.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for prices.
 */

public abstract class AbstractPriceQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.ordering.PriceQuery {

    private final java.util.Collection<org.osid.ordering.records.PriceQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the price schedule <code> Id </code> for this query. 
     *
     *  @param  priceScheduleId a price schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceScheduleId(org.osid.id.Id priceScheduleId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the price schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceScheduleIdTerms() {
        return;
    }


    /**
     *  Tests if a price schedule query is available. 
     *
     *  @return <code> true </code> if a price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price schedule. 
     *
     *  @return the price schedule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuery getPriceScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleQuery() is false");
    }


    /**
     *  Clears the price schedule terms. 
     */

    @OSID @Override
    public void clearPriceScheduleTerms() {
        return;
    }


    /**
     *  Matches the minimum quantity between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMinimumQuantity(long low, long high, boolean match) {
        return;
    }


    /**
     *  Clears the minimum quantity terms. 
     */

    @OSID @Override
    public void clearMinimumQuantityTerms() {
        return;
    }


    /**
     *  Matches the maximum quantity between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchMaximumQuantity(long low, long high, boolean match) {
        return;
    }


    /**
     *  Clears the maximum quantity terms. 
     */

    @OSID @Override
    public void clearMaximumQuantityTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resource </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDemographicId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDemographicIdTerms() {
        return;
    }


    /**
     *  Tests if a resource query is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsDemographicQuery() is false");
    }


    /**
     *  Matches prices with any demographic. 
     *
     *  @param  match <code> true </code> to match prices with any 
     *          demographic, <code> false </code> to match prices with no 
     *          orders 
     */

    @OSID @Override
    public void matchAnyDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearDemographicTerms() {
        return;
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        return;
    }


    /**
     *  Matches items that have any amount set. 
     *
     *  @param  match <code> true </code> to match items with any amount, 
     *          <code> false </code> to match items with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        return;
    }


    /**
     *  Matches the a minimum price amount. 
     *
     *  @param  amount an amount 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumAmount(org.osid.financials.Currency amount, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the minimum amount terms. 
     */

    @OSID @Override
    public void clearMinimumAmountTerms() {
        return;
    }


    /**
     *  Matches the recurring interval between the given duration range 
     *  inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRecurringInterval(org.osid.calendaring.Duration low, 
                                       org.osid.calendaring.Duration high, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches prices with any recurring interval. 
     *
     *  @param  match <code> true </code> to match prices with any recurring 
     *          interval, <code> false </code> to match one-time prices 
     */

    @OSID @Override
    public void matchAnyRecurringInterval(boolean match) {
        return;
    }


    /**
     *  Clears the recurring interval terms. 
     */

    @OSID @Override
    public void clearRecurringIntervalTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an item query is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches prices used with any item. 
     *
     *  @param  match <code> true </code> to match prices with any order, 
     *          <code> false </code> to match prices with no orders 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Sets the price <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given price query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a price implementing the requested record.
     *
     *  @param priceRecordType a price record type
     *  @return the price query record
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceQueryRecord getPriceQueryRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceQueryRecord record : this.records) {
            if (record.implementsRecordType(priceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price query. 
     *
     *  @param priceQueryRecord price query record
     *  @param priceRecordType price record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceQueryRecord(org.osid.ordering.records.PriceQueryRecord priceQueryRecord, 
                                          org.osid.type.Type priceRecordType) {

        addRecordType(priceRecordType);
        nullarg(priceQueryRecord, "price query record");
        this.records.add(priceQueryRecord);        
        return;
    }
}
