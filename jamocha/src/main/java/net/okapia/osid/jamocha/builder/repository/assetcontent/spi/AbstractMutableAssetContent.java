//
// AbstractMutableAssetContent.java
//
//     Defines a mutable AssetContent.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.assetcontent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>AssetContent</code>.
 */

public abstract class AbstractMutableAssetContent
    extends net.okapia.osid.jamocha.repository.assetcontent.spi.AbstractAssetContent
    implements org.osid.repository.AssetContent,
               net.okapia.osid.jamocha.builder.repository.assetcontent.AssetContentMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this asset content. 
     *
     *  @param record asset content record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAssetContentRecord(org.osid.repository.records.AssetContentRecord record, org.osid.type.Type recordType) {
        super.addAssetContentRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this asset content.
     *
     *  @param displayName the name for this asset content
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this asset content.
     *
     *  @param description the description of this asset content
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    @Override
    public void setAsset(org.osid.repository.Asset asset) {
        super.setAsset(asset);
        return;
    }


    /**
     *  Adds an accessibility type.
     *
     *  @param accessibilityType an accessibility type
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityType</code> is <code>null</code>
     */

    @Override
    public void addAccessibilityType(org.osid.type.Type accessibilityType) {
        super.addAccessibilityType(accessibilityType);
        return;
    }


    /**
     *  Sets all the accessibility types.
     *
     *  @param accessibilityTypes a collection of accessibility types
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityTypes</code> is <code>null</code>
     */

    @Override
    public void setAccessibilityTypes(java.util.Collection<org.osid.type.Type> accessibilityTypes) {
        super.setAccessibilityTypes(accessibilityTypes);
        return;
    }


    /**
     *  Sets the data length. Sets the data length. This length
     *  overrides the length of the data buffer.
     *
     *  @param length a data length
     *  @throws org.osid.InvalidArgumentException <code>length</code>
     *          is negative
     */

    @Override
    public void setDataLength(long length) {
        super.setDataLength(length);
        return;
    }


    /**
     *  Sets the asset content data. 
     *
     *  @param data the flipped data buffer
     *  @throws org.osid.NullArgumentException <code>data</code> is
     *          <code>null</code>
     */

    @Override
    public void setData(java.nio.ByteBuffer data) {
        super.setData(data);
        return;
    }


    /**
     *  Sets the url.
     *
     *  @param url a url
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setURL(String url) {
        super.setURL(url);
        return;
    }
}

