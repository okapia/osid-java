//
// AbstractEdgeLookupSession.java
//
//    A starter implementation framework for providing an Edge
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Edge lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEdges(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEdgeLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.topology.EdgeLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();
    

    /**
     *  Gets the <code>Graph/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this session.
     *
     *  @return the <code>Graph</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the <code>Graph</code>.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException <code>graph</code>
     *          is <code>null</code>
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can perform <code>Edge</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEdges() {
        return (true);
    }


    /**
     *  A complete view of the <code>Edge</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEdgeView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Edge</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEdgeView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edges in graphs which are children of this
     *  graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only edges whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveEdgeView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All edges of any effective dates are returned by all methods
     *  in this session.
     */

    @OSID @Override
    public void useAnyEffectiveEdgeView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Edge</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Edge</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Edge</code> and retained for
     *  compatibility.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeId <code>Id</code> of the
     *          <code>Edge</code>
     *  @return the edge
     *  @throws org.osid.NotFoundException <code>edgeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>edgeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Edge getEdge(org.osid.id.Id edgeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.topology.EdgeList edges = getEdges()) {
            while (edges.hasNext()) {
                org.osid.topology.Edge edge = edges.getNextEdge();
                if (edge.getId().equals(edgeId)) {
                    return (edge);
                }
            }
        } 

        throw new org.osid.NotFoundException(edgeId + " not found");
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the edges
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Edges</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEdges()</code>.
     *
     *  @param edgeIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByIds(org.osid.id.IdList edgeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.topology.Edge> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = edgeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getEdge(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("edge " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.topology.edge.LinkedEdgeList(ret));
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given edge
     *  genus <code>Type</code> which does not include edges of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEdges()</code>.
     *
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeGenusFilterList(getEdges(), edgeGenusType));
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given edge
     *  genus <code>Type</code> and include any additional edges with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEdges()</code>.
     *
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByParentGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEdgesByGenusType(edgeGenusType));
    }


    /**
     *  Gets an <code>EdgeList</code> containing the given edge record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEdges()</code>.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByRecordType(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeRecordFilterList(getEdges(), edgeRecordType));
    }


    /**
     *  Gets an <code>EdgeList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In active mode, edges are returned that are currently
     *  active. In any status mode, active and inactive edges are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Edge</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.EdgeList getEdgesOnDate(org.osid.calendaring.DateTime from, 
                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdges(), from, to));
    }
        


    /**
     *  Gets an <code>EdgeList</code> by genus type effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In active mode, edges are returned that are currently
     *  active. In any status mode, active and inactive edges are
     *  returned.
     *
     *  @param edgeGenusType edge genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Edge</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeOnDate(org.osid.type.Type edgeGenusType,
                                                                org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdgesByGenusType(edgeGenusType), from, to));
    }
        

    /**
     *  Gets a list of edges corresponding to a source node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForSourceNode(org.osid.id.Id sourceNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeFilterList(new SourceNodeFilter(sourceNodeId), getEdges()));
    }

    
    /**
     *  Gets a list of edges corresponding to a source node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the source node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForSourceNodeOnDate(org.osid.id.Id sourceNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdgesForSourceNode(sourceNodeId), from, to));
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a source
     *  node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code> or
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNode(org.osid.id.Id sourceNodeId,
                                                                       org.osid.type.Type edgeGenusType)
                                                                       
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeGenusFilterList(getEdgesForSourceNode(sourceNodeId), edgeGenusType));
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a source
     *  node <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the source node
     *  @param edgeGenusType an edge genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code>, <code>edgeGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNodeOnDate(org.osid.id.Id sourceNodeId,
                                                                             org.osid.type.Type edgeGenusType,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdgesByGenusTypeForSourceNode(sourceNodeId, edgeGenusType), from, to));
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.EdgeList getEdgesForDestinationNode(org.osid.id.Id destinationNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeFilterList(new DestinationNodeFilter(destinationNodeId), getEdges()));
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param destinationNodeId the <code>Id</code> of the
     *  destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>destinationNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForDestinationNodeOnDate(org.osid.id.Id destinationNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdgesForDestinationNode(destinationNodeId), from, to));
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a
     *  destination node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code> or
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNode(org.osid.id.Id destinationNodeId,
                                                                            org.osid.type.Type edgeGenusType)
                                                                       
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeGenusFilterList(getEdgesForDestinationNode(destinationNodeId), edgeGenusType));
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a
     *  destination node <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param destinationNodeId the <code>Id</code> of the
     *  destination node
     *  @param edgeGenusType an edge genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code>, <code>edgeGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNodeOnDate(org.osid.id.Id destinationNodeId,
                                                                                  org.osid.type.Type edgeGenusType,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdgesByGenusTypeForDestinationNode(destinationNodeId, edgeGenusType), from, to));
    }


    /**
     *  Gets a list of edges corresponding to source node and
     *  destination node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodes(org.osid.id.Id sourceNodeId,
                                                       org.osid.id.Id destinationNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeFilterList(new DestinationNodeFilter(destinationNodeId), getEdgesForSourceNode(sourceNodeId)));
    }


    /**
     *  Gets a list of edges corresponding to source node and
     *  destination node <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the source node
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodesOnDate(org.osid.id.Id sourceNodeId,
                                                             org.osid.id.Id destinationNodeId,
                                                             org.osid.calendaring.DateTime from,
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdgesForNodes(sourceNodeId, destinationNodeId), from, to));
    }


    /**
     *  Gets a list of edges of a genus type corresponding to source
     *  node and destination node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code> or
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodes(org.osid.id.Id sourceNodeId,
                                                                  org.osid.id.Id destinationNodeId,
                                                                  org.osid.type.Type edgeGenusType)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeGenusFilterList(getEdgesForNodes(sourceNodeId, destinationNodeId), edgeGenusType));
    }


    /**
     *  Gets a list of edges of a genus type corresponding to source
     *  node and destination node <code>Ids</code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the destination
     *  node
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param edgeGenusType an edge genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code>,
     *          <code>edgeGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodesOnDate(org.osid.id.Id sourceNodeId,
                                                                        org.osid.id.Id destinationNodeId,
                                                                        org.osid.type.Type edgeGenusType,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.edge.TemporalEdgeFilterList(getEdgesByGenusTypeForNodes(sourceNodeId, destinationNodeId, edgeGenusType), from, to));
    }


    /**
     *  Gets all <code>Edges</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Edges</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.topology.EdgeList getEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the edge list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of edges
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.topology.EdgeList filterEdgesOnViews(org.osid.topology.EdgeList list)
        throws org.osid.OperationFailedException {

        org.osid.topology.EdgeList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.topology.edge.EffectiveEdgeFilterList(ret);
        }

        return (ret);
    }


    public static class SourceNodeFilter
        implements net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeFilter {
         
        private final org.osid.id.Id sourceNodeId;
         
         
        /**
         *  Constructs a new <code>SourceNodeFilter</code>.
         *
         *  @param sourceNodeId the source node to filter
         *  @throws org.osid.NullArgumentException
         *          <code>sourceNodeId</code> is <code>null</code>
         */
        
        public SourceNodeFilter(org.osid.id.Id sourceNodeId) {
            nullarg(sourceNodeId, "source node Id");
            this.sourceNodeId = sourceNodeId;
            return;
        }

         
        /**
         *  Used by the EdgeFilterList to filter the 
         *  edge list based on source node.
         *
         *  @param edge the edge
         *  @return <code>true</code> to pass the edge,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.topology.Edge edge) {
            return (edge.getSourceNodeId().equals(this.sourceNodeId));
        }
    }


    public static class DestinationNodeFilter
        implements net.okapia.osid.jamocha.inline.filter.topology.edge.EdgeFilter {
         
        private final org.osid.id.Id destinationNodeId;
         
         
        /**
         *  Constructs a new <code>DestinationNodeFilter</code>.
         *
         *  @param destinationNodeId the destination node to filter
         *  @throws org.osid.NullArgumentException
         *          <code>destinationNodeId</code> is <code>null</code>
         */
        
        public DestinationNodeFilter(org.osid.id.Id destinationNodeId) {
            nullarg(destinationNodeId, "destination node Id");
            this.destinationNodeId = destinationNodeId;
            return;
        }

         
        /**
         *  Used by the EdgeFilterList to filter the 
         *  edge list based on destination node.
         *
         *  @param edge the edge
         *  @return <code>true</code> to pass the edge,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.topology.Edge edge) {
            return (edge.getDestinationNodeId().equals(this.destinationNodeId));
        }
    }
}
