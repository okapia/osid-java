//
// InvariantMapRequisiteLookupSession
//
//    Implements a Requisite lookup service backed by a fixed collection of
//    requisites.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.requisite;


/**
 *  Implements a Requisite lookup service backed by a fixed
 *  collection of requisites. The requisites are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRequisiteLookupSession
    extends net.okapia.osid.jamocha.core.course.requisite.spi.AbstractMapRequisiteLookupSession
    implements org.osid.course.requisite.RequisiteLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRequisiteLookupSession</code> with no
     *  requisites.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRequisiteLookupSession</code> with a single
     *  requisite.
     *  
     *  @param courseCatalog the course catalog
     *  @param requisite a single requisite
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code requisite} is <code>null</code>
     */

      public InvariantMapRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.requisite.Requisite requisite) {
        this(courseCatalog);
        putRequisite(requisite);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRequisiteLookupSession</code> using an array
     *  of requisites.
     *  
     *  @param courseCatalog the course catalog
     *  @param requisites an array of requisites
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code requisites} is <code>null</code>
     */

      public InvariantMapRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.requisite.Requisite[] requisites) {
        this(courseCatalog);
        putRequisites(requisites);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRequisiteLookupSession</code> using a
     *  collection of requisites.
     *
     *  @param courseCatalog the course catalog
     *  @param requisites a collection of requisites
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code requisites} is <code>null</code>
     */

      public InvariantMapRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.requisite.Requisite> requisites) {
        this(courseCatalog);
        putRequisites(requisites);
        return;
    }
}
