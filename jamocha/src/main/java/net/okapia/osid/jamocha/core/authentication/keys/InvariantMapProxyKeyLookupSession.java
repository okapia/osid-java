//
// InvariantMapProxyKeyLookupSession
//
//    Implements a Key lookup service backed by a fixed
//    collection of keys. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.keys;


/**
 *  Implements a Key lookup service backed by a fixed
 *  collection of keys. The keys are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyKeyLookupSession
    extends net.okapia.osid.jamocha.core.authentication.keys.spi.AbstractMapKeyLookupSession
    implements org.osid.authentication.keys.KeyLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyKeyLookupSession} with no
     *  keys.
     *
     *  @param agency the agency
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyKeyLookupSession(org.osid.authentication.Agency agency,
                                                  org.osid.proxy.Proxy proxy) {
        setAgency(agency);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyKeyLookupSession} with a single
     *  key.
     *
     *  @param agency the agency
     *  @param key a single key
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code key} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyKeyLookupSession(org.osid.authentication.Agency agency,
                                                  org.osid.authentication.keys.Key key, org.osid.proxy.Proxy proxy) {

        this(agency, proxy);
        putKey(key);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyKeyLookupSession} using
     *  an array of keys.
     *
     *  @param agency the agency
     *  @param keys an array of keys
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code keys} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyKeyLookupSession(org.osid.authentication.Agency agency,
                                                  org.osid.authentication.keys.Key[] keys, org.osid.proxy.Proxy proxy) {

        this(agency, proxy);
        putKeys(keys);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyKeyLookupSession} using a
     *  collection of keys.
     *
     *  @param agency the agency
     *  @param keys a collection of keys
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency},
     *          {@code keys} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyKeyLookupSession(org.osid.authentication.Agency agency,
                                                  java.util.Collection<? extends org.osid.authentication.keys.Key> keys,
                                                  org.osid.proxy.Proxy proxy) {

        this(agency, proxy);
        putKeys(keys);
        return;
    }
}
