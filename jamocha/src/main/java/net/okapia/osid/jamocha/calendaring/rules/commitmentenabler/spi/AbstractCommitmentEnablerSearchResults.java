//
// AbstractCommitmentEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCommitmentEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.rules.CommitmentEnablerSearchResults {

    private org.osid.calendaring.rules.CommitmentEnablerList commitmentEnablers;
    private final org.osid.calendaring.rules.CommitmentEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.rules.records.CommitmentEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCommitmentEnablerSearchResults.
     *
     *  @param commitmentEnablers the result set
     *  @param commitmentEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>commitmentEnablers</code>
     *          or <code>commitmentEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCommitmentEnablerSearchResults(org.osid.calendaring.rules.CommitmentEnablerList commitmentEnablers,
                                            org.osid.calendaring.rules.CommitmentEnablerQueryInspector commitmentEnablerQueryInspector) {
        nullarg(commitmentEnablers, "commitment enablers");
        nullarg(commitmentEnablerQueryInspector, "commitment enabler query inspectpr");

        this.commitmentEnablers = commitmentEnablers;
        this.inspector = commitmentEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the commitment enabler list resulting from a search.
     *
     *  @return a commitment enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablers() {
        if (this.commitmentEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.rules.CommitmentEnablerList commitmentEnablers = this.commitmentEnablers;
        this.commitmentEnablers = null;
	return (commitmentEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.rules.CommitmentEnablerQueryInspector getCommitmentEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  commitment enabler search record <code> Type. </code> This method must
     *  be used to retrieve a commitmentEnabler implementing the requested
     *  record.
     *
     *  @param commitmentEnablerSearchRecordType a commitmentEnabler search 
     *         record type 
     *  @return the commitment enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(commitmentEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.CommitmentEnablerSearchResultsRecord getCommitmentEnablerSearchResultsRecord(org.osid.type.Type commitmentEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.rules.records.CommitmentEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(commitmentEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(commitmentEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record commitment enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCommitmentEnablerRecord(org.osid.calendaring.rules.records.CommitmentEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "commitment enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
