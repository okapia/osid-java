//
// AbstractQueryGraphLookupSession.java
//
//    An inline adapter that maps a GraphLookupSession to
//    a GraphQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a GraphLookupSession to
 *  a GraphQuerySession.
 */

public abstract class AbstractQueryGraphLookupSession
    extends net.okapia.osid.jamocha.topology.spi.AbstractGraphLookupSession
    implements org.osid.topology.GraphLookupSession {

    private final org.osid.topology.GraphQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryGraphLookupSession.
     *
     *  @param querySession the underlying graph query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryGraphLookupSession(org.osid.topology.GraphQuerySession querySession) {
        nullarg(querySession, "graph query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Graph</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGraphs() {
        return (this.session.canSearchGraphs());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Graph</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Graph</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Graph</code> and
     *  retained for compatibility.
     *
     *  @param  graphId <code>Id</code> of the
     *          <code>Graph</code>
     *  @return the graph
     *  @throws org.osid.NotFoundException <code>graphId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>graphId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.GraphQuery query = getQuery();
        query.matchId(graphId, true);
        org.osid.topology.GraphList graphs = this.session.getGraphsByQuery(query);
        if (graphs.hasNext()) {
            return (graphs.getNextGraph());
        } 
        
        throw new org.osid.NotFoundException(graphId + " not found");
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  graphs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Graphs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  graphIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>graphIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByIds(org.osid.id.IdList graphIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.GraphQuery query = getQuery();

        try (org.osid.id.IdList ids = graphIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getGraphsByQuery(query));
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  graph genus <code>Type</code> which does not include
     *  graphs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.GraphQuery query = getQuery();
        query.matchGenusType(graphGenusType, true);
        return (this.session.getGraphsByQuery(query));
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  graph genus <code>Type</code> and include any additional
     *  graphs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByParentGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.GraphQuery query = getQuery();
        query.matchParentGenusType(graphGenusType, true);
        return (this.session.getGraphsByQuery(query));
    }


    /**
     *  Gets a <code>GraphList</code> containing the given
     *  graph record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  graphRecordType a graph record type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByRecordType(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.GraphQuery query = getQuery();
        query.matchRecordType(graphRecordType, true);
        return (this.session.getGraphsByQuery(query));
    }


    /**
     *  Gets a <code>GraphList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known graphs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  graphs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Graph</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.GraphQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getGraphsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Graphs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  graphs or an error results. Otherwise, the returned list
     *  may contain only those graphs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Graphs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.GraphQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getGraphsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.topology.GraphQuery getQuery() {
        org.osid.topology.GraphQuery query = this.session.getGraphQuery();
        return (query);
    }
}
