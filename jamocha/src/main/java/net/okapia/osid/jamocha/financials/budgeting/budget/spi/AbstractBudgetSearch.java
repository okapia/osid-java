//
// AbstractBudgetSearch.java
//
//     A template for making a Budget Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing budget searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBudgetSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.financials.budgeting.BudgetSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.financials.budgeting.BudgetSearchOrder budgetSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of budgets. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  budgetIds list of budgets
     *  @throws org.osid.NullArgumentException
     *          <code>budgetIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBudgets(org.osid.id.IdList budgetIds) {
        while (budgetIds.hasNext()) {
            try {
                this.ids.add(budgetIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBudgets</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of budget Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBudgetIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  budgetSearchOrder budget search order 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>budgetSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBudgetResults(org.osid.financials.budgeting.BudgetSearchOrder budgetSearchOrder) {
	this.budgetSearchOrder = budgetSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.financials.budgeting.BudgetSearchOrder getBudgetSearchOrder() {
	return (this.budgetSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given budget search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a budget implementing the requested record.
     *
     *  @param budgetSearchRecordType a budget search record
     *         type
     *  @return the budget search record
     *  @throws org.osid.NullArgumentException
     *          <code>budgetSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetSearchRecord getBudgetSearchRecord(org.osid.type.Type budgetSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.financials.budgeting.records.BudgetSearchRecord record : this.records) {
            if (record.implementsRecordType(budgetSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this budget search. 
     *
     *  @param budgetSearchRecord budget search record
     *  @param budgetSearchRecordType budget search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBudgetSearchRecord(org.osid.financials.budgeting.records.BudgetSearchRecord budgetSearchRecord, 
                                           org.osid.type.Type budgetSearchRecordType) {

        addRecordType(budgetSearchRecordType);
        this.records.add(budgetSearchRecord);        
        return;
    }
}
