//
// MutableMapCredentialEntryLookupSession
//
//    Implements a CredentialEntry lookup service backed by a collection of
//    credentialEntries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements a CredentialEntry lookup service backed by a collection of
 *  credential entries. The credential entries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of credential entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCredentialEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapCredentialEntryLookupSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {


    /**
     *  Constructs a new {@code MutableMapCredentialEntryLookupSession}
     *  with no credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog} is
     *          {@code null}
     */

      public MutableMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCredentialEntryLookupSession} with a
     *  single credentialEntry.
     *
     *  @param courseCatalog the course catalog  
     *  @param credentialEntry a credential entry
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentialEntry} is {@code null}
     */

    public MutableMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.chronicle.CredentialEntry credentialEntry) {
        this(courseCatalog);
        putCredentialEntry(credentialEntry);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCredentialEntryLookupSession}
     *  using an array of credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param credentialEntries an array of credential entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentialEntries} is {@code null}
     */

    public MutableMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.chronicle.CredentialEntry[] credentialEntries) {
        this(courseCatalog);
        putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCredentialEntryLookupSession}
     *  using a collection of credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param credentialEntries a collection of credential entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentialEntries} is {@code null}
     */

    public MutableMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           java.util.Collection<? extends org.osid.course.chronicle.CredentialEntry> credentialEntries) {

        this(courseCatalog);
        putCredentialEntries(credentialEntries);
        return;
    }

    
    /**
     *  Makes a {@code CredentialEntry} available in this session.
     *
     *  @param credentialEntry a credential entry
     *  @throws org.osid.NullArgumentException {@code credentialEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putCredentialEntry(org.osid.course.chronicle.CredentialEntry credentialEntry) {
        super.putCredentialEntry(credentialEntry);
        return;
    }


    /**
     *  Makes an array of credential entries available in this session.
     *
     *  @param credentialEntries an array of credential entries
     *  @throws org.osid.NullArgumentException {@code credentialEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putCredentialEntries(org.osid.course.chronicle.CredentialEntry[] credentialEntries) {
        super.putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Makes collection of credential entries available in this session.
     *
     *  @param credentialEntries a collection of credential entries
     *  @throws org.osid.NullArgumentException {@code credentialEntries{@code  is
     *          {@code null}
     */

    @Override
    public void putCredentialEntries(java.util.Collection<? extends org.osid.course.chronicle.CredentialEntry> credentialEntries) {
        super.putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Removes a CredentialEntry from this session.
     *
     *  @param credentialEntryId the {@code Id} of the credential entry
     *  @throws org.osid.NullArgumentException {@code credentialEntryId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCredentialEntry(org.osid.id.Id credentialEntryId) {
        super.removeCredentialEntry(credentialEntryId);
        return;
    }    
}
