//
// MutableIndexedMapProxyAddressLookupSession
//
//    Implements an Address lookup service backed by a collection of
//    addresses indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact;


/**
 *  Implements an Address lookup service backed by a collection of
 *  addresses. The addresses are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some addresses may be compatible
 *  with more types than are indicated through these address
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of addresses can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyAddressLookupSession
    extends net.okapia.osid.jamocha.core.contact.spi.AbstractIndexedMapAddressLookupSession
    implements org.osid.contact.AddressLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAddressLookupSession} with
     *  no address.
     *
     *  @param addressBook the address book
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAddressLookupSession(org.osid.contact.AddressBook addressBook,
                                                       org.osid.proxy.Proxy proxy) {
        setAddressBook(addressBook);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAddressLookupSession} with
     *  a single address.
     *
     *  @param addressBook the address book
     *  @param  address an address
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook},
     *          {@code address}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAddressLookupSession(org.osid.contact.AddressBook addressBook,
                                                       org.osid.contact.Address address, org.osid.proxy.Proxy proxy) {

        this(addressBook, proxy);
        putAddress(address);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAddressLookupSession} using
     *  an array of addresses.
     *
     *  @param addressBook the address book
     *  @param  addresses an array of addresses
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook},
     *          {@code addresses}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAddressLookupSession(org.osid.contact.AddressBook addressBook,
                                                       org.osid.contact.Address[] addresses, org.osid.proxy.Proxy proxy) {

        this(addressBook, proxy);
        putAddresses(addresses);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAddressLookupSession} using
     *  a collection of addresses.
     *
     *  @param addressBook the address book
     *  @param  addresses a collection of addresses
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook},
     *          {@code addresses}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAddressLookupSession(org.osid.contact.AddressBook addressBook,
                                                       java.util.Collection<? extends org.osid.contact.Address> addresses,
                                                       org.osid.proxy.Proxy proxy) {
        this(addressBook, proxy);
        putAddresses(addresses);
        return;
    }

    
    /**
     *  Makes an {@code Address} available in this session.
     *
     *  @param  address an address
     *  @throws org.osid.NullArgumentException {@code address{@code 
     *          is {@code null}
     */

    @Override
    public void putAddress(org.osid.contact.Address address) {
        super.putAddress(address);
        return;
    }


    /**
     *  Makes an array of addresses available in this session.
     *
     *  @param  addresses an array of addresses
     *  @throws org.osid.NullArgumentException {@code addresses{@code 
     *          is {@code null}
     */

    @Override
    public void putAddresses(org.osid.contact.Address[] addresses) {
        super.putAddresses(addresses);
        return;
    }


    /**
     *  Makes collection of addresses available in this session.
     *
     *  @param  addresses a collection of addresses
     *  @throws org.osid.NullArgumentException {@code address{@code 
     *          is {@code null}
     */

    @Override
    public void putAddresses(java.util.Collection<? extends org.osid.contact.Address> addresses) {
        super.putAddresses(addresses);
        return;
    }


    /**
     *  Removes an Address from this session.
     *
     *  @param addressId the {@code Id} of the address
     *  @throws org.osid.NullArgumentException {@code addressId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAddress(org.osid.id.Id addressId) {
        super.removeAddress(addressId);
        return;
    }    
}
