//
// InvariantMapProxyBallotLookupSession
//
//    Implements a Ballot lookup service backed by a fixed
//    collection of ballots. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Ballot lookup service backed by a fixed
 *  collection of ballots. The ballots are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyBallotLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapBallotLookupSession
    implements org.osid.voting.BallotLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBallotLookupSession} with no
     *  ballots.
     *
     *  @param polls the polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.proxy.Proxy proxy) {
        setPolls(polls);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyBallotLookupSession} with a single
     *  ballot.
     *
     *  @param polls the polls
     *  @param ballot a single ballot
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballot} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Ballot ballot, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putBallot(ballot);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyBallotLookupSession} using
     *  an array of ballots.
     *
     *  @param polls the polls
     *  @param ballots an array of ballots
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballots} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Ballot[] ballots, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putBallots(ballots);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBallotLookupSession} using a
     *  collection of ballots.
     *
     *  @param polls the polls
     *  @param ballots a collection of ballots
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballots} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.Ballot> ballots,
                                                  org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putBallots(ballots);
        return;
    }
}
