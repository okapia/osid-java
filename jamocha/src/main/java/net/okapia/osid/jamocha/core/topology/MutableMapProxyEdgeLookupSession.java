//
// MutableMapProxyEdgeLookupSession
//
//    Implements an Edge lookup service backed by a collection of
//    edges that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements an Edge lookup service backed by a collection of
 *  edges. The edges are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of edges can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyEdgeLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractMapEdgeLookupSession
    implements org.osid.topology.EdgeLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyEdgeLookupSession}
     *  with no edges.
     *
     *  @param graph the graph
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyEdgeLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.proxy.Proxy proxy) {
        setGraph(graph);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyEdgeLookupSession} with a
     *  single edge.
     *
     *  @param graph the graph
     *  @param edge an edge
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code edge}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEdgeLookupSession(org.osid.topology.Graph graph,
                                                org.osid.topology.Edge edge, org.osid.proxy.Proxy proxy) {
        this(graph, proxy);
        putEdge(edge);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEdgeLookupSession} using an
     *  array of edges.
     *
     *  @param graph the graph
     *  @param edges an array of edges
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code edges}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEdgeLookupSession(org.osid.topology.Graph graph,
                                                org.osid.topology.Edge[] edges, org.osid.proxy.Proxy proxy) {
        this(graph, proxy);
        putEdges(edges);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEdgeLookupSession} using a
     *  collection of edges.
     *
     *  @param graph the graph
     *  @param edges a collection of edges
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code edges}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEdgeLookupSession(org.osid.topology.Graph graph,
                                                java.util.Collection<? extends org.osid.topology.Edge> edges,
                                                org.osid.proxy.Proxy proxy) {
   
        this(graph, proxy);
        setSessionProxy(proxy);
        putEdges(edges);
        return;
    }

    
    /**
     *  Makes a {@code Edge} available in this session.
     *
     *  @param edge an edge
     *  @throws org.osid.NullArgumentException {@code edge{@code 
     *          is {@code null}
     */

    @Override
    public void putEdge(org.osid.topology.Edge edge) {
        super.putEdge(edge);
        return;
    }


    /**
     *  Makes an array of edges available in this session.
     *
     *  @param edges an array of edges
     *  @throws org.osid.NullArgumentException {@code edges{@code 
     *          is {@code null}
     */

    @Override
    public void putEdges(org.osid.topology.Edge[] edges) {
        super.putEdges(edges);
        return;
    }


    /**
     *  Makes collection of edges available in this session.
     *
     *  @param edges
     *  @throws org.osid.NullArgumentException {@code edge{@code 
     *          is {@code null}
     */

    @Override
    public void putEdges(java.util.Collection<? extends org.osid.topology.Edge> edges) {
        super.putEdges(edges);
        return;
    }


    /**
     *  Removes a Edge from this session.
     *
     *  @param edgeId the {@code Id} of the edge
     *  @throws org.osid.NullArgumentException {@code edgeId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEdge(org.osid.id.Id edgeId) {
        super.removeEdge(edgeId);
        return;
    }    
}
