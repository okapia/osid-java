//
// InvariantMapAppointmentLookupSession
//
//    Implements an Appointment lookup service backed by a fixed collection of
//    appointments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements an Appointment lookup service backed by a fixed
 *  collection of appointments. The appointments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAppointmentLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapAppointmentLookupSession
    implements org.osid.personnel.AppointmentLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAppointmentLookupSession</code> with no
     *  appointments.
     *  
     *  @param realm the realm
     *  @throws org.osid.NullArgumnetException {@code realm} is
     *          {@code null}
     */

    public InvariantMapAppointmentLookupSession(org.osid.personnel.Realm realm) {
        setRealm(realm);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAppointmentLookupSession</code> with a single
     *  appointment.
     *  
     *  @param realm the realm
     *  @param appointment an single appointment
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code appointment} is <code>null</code>
     */

      public InvariantMapAppointmentLookupSession(org.osid.personnel.Realm realm,
                                               org.osid.personnel.Appointment appointment) {
        this(realm);
        putAppointment(appointment);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAppointmentLookupSession</code> using an array
     *  of appointments.
     *  
     *  @param realm the realm
     *  @param appointments an array of appointments
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code appointments} is <code>null</code>
     */

      public InvariantMapAppointmentLookupSession(org.osid.personnel.Realm realm,
                                               org.osid.personnel.Appointment[] appointments) {
        this(realm);
        putAppointments(appointments);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAppointmentLookupSession</code> using a
     *  collection of appointments.
     *
     *  @param realm the realm
     *  @param appointments a collection of appointments
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code appointments} is <code>null</code>
     */

      public InvariantMapAppointmentLookupSession(org.osid.personnel.Realm realm,
                                               java.util.Collection<? extends org.osid.personnel.Appointment> appointments) {
        this(realm);
        putAppointments(appointments);
        return;
    }
}
