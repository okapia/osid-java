//
// AbstractSourceableOsidObjectQueryInspector.java
//
//     Defines a SourceableOsidObjectQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractSourceableOsidObjectQueryInspector
    extends AbstractOsidObjectQueryInspector
    implements org.osid.OsidSourceableQueryInspector,
               org.osid.OsidObjectQueryInspector {
    
    private final OsidSourceableQueryInspector inspector = new OsidSourceableQueryInspector();


    /**
     *  Gets the provider <code>Id</code> query terms.
     *
     *  @return the provider <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProviderIdTerms() {
        return (this.inspector.getProviderIdTerms());
    }


    /**
     *  Adds a provider <code>Id</code> term.
     *
     *  @param term a provider <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addProviderIdTerm(org.osid.search.terms.IdTerm term) {
        this.inspector.addProviderIdTerm(term);
        return;
    }


    /**
     *  Adds a collection of provider <code>Id</code> terms.
     *
     *  @param terms a collection of provider <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addProviderIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        this.inspector.addProviderIdTerms(terms);
        return;
    }


    /**
     *  Adds a provider <code>Id</code> term.
     *
     *  @param resourceId the provider <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or is <code>null</code>
     */

    protected void addProviderIdTerm(org.osid.id.Id resourceId, boolean match) {
        this.inspector.addProviderIdTerm(resourceId, match);
        return;
    }


    /**
     *  Gets the provider query terms.
     *
     *  @return the provider terms
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getProviderTerms() {
        return (this.inspector.getProviderTerms());
    }

    
    /**
     *  Adds a provider term.
     *
     *  @param term a provider term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addProviderTerm(org.osid.resource.ResourceQueryInspector term) {
        this.inspector.addProviderTerm(term);
        return;
    }


    /**
     *  Adds a collection of provider terms.
     *
     *  @param terms a collection of provider terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addProviderTerms(java.util.Collection<org.osid.resource.ResourceQueryInspector> terms) {
        this.inspector.addProviderTerms(terms);
        return;
    }


    /**
     *  Gets the asset <code>Id</code> query terms.
     *
     *  @return the asset <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrandingIdTerms() {
        return (this.inspector.getBrandingIdTerms());
    }

    
    /**
     *  Adds an asset <code>Id</code> term.
     *
     *  @param term an asset <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addBrandingIdTerm(org.osid.search.terms.IdTerm term) {
        this.inspector.addBrandingIdTerm(term);
        return;
    }


    /**
     *  Adds a collection of asset <code>Id</code> terms.
     *
     *  @param terms a collection of asset <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addBrandingIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        this.inspector.addBrandingIdTerms(terms);
        return;
    }


    /**
     *  Adds an asset <code>Id</code> term.
     *
     *  @param assetId the asset <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>assetId</code>
     *          or is <code>null</code>
     */

    protected void addBrandingIdTerm(org.osid.id.Id assetId, boolean match) {
        this.inspector.addBrandingIdTerm(assetId, match);
        return;
    }


    /**
     *  Gets the asset query terms.
     *
     *  @return the asset terms
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getBrandingTerms() {
        return (this.inspector.getBrandingTerms());
    }

    
    /**
     *  Adds an asset term.
     *
     *  @param term an asset term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addBrandingTerm(org.osid.repository.AssetQueryInspector term) {
        this.inspector.addBrandingTerm(term);
        return;
    }


    /**
     *  Adds a collection of asset terms.
     *
     *  @param terms a collection of asset terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addBrandingTerms(java.util.Collection<org.osid.repository.AssetQueryInspector> terms) {
        this.inspector.addBrandingTerms(terms);
        return;
    }


    /**
     *  Gets the license query terms.
     *
     *  @return the license terms
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLicenseTerms() {
        return (this.inspector.getLicenseTerms());
    }

    
    /**
     *  Adds a license term.
     *
     *  @param term a license term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addLicenseTerm(org.osid.search.terms.StringTerm term) {
        this.inspector.addLicenseTerm(term);
        return;
    }


    /**
     *  Adds a collection of license terms.
     *
     *  @param terms a collection of license terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addLicenseTerms(java.util.Collection<org.osid.search.terms.StringTerm> terms) {
        this.inspector.addLicenseTerms(terms);
        return;
    }


    /**
     *  Adds a license term.
     *
     *  @param license the license
     *  @param stringMatchType a string match type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>id</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */

    protected void addLicenseTerm(String license, org.osid.type.Type stringMatchType, boolean match) {
        this.inspector.addLicenseTerm(license, stringMatchType, match);
        return;
    }


    protected class OsidSourceableQueryInspector 
        extends AbstractOsidSourceableQueryInspector
        implements org.osid.OsidSourceableQueryInspector {


        /**
         *  Adds a provider <code>Id</code> term.
         *
         *  @param term a provider <code>Id</code> term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addProviderIdTerm(org.osid.search.terms.IdTerm term) {
            super.addProviderIdTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of provider <code>Id</code> terms.
         *
         *  @param terms a collection of provider <code>Id</code> terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addProviderIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
            super.addProviderIdTerms(terms);
            return;
        }
        
        
        /**
         *  Adds a provider <code>Id</code> term.
         *
         *  @param resourceId the provider <code>Id</code>
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>resourceId</code>
         *          or is <code>null</code>
         */
        
        @Override
        protected void addProviderIdTerm(org.osid.id.Id resourceId, boolean match) {
            super.addProviderIdTerm(resourceId, match);
            return;
        }
        
        
        /**
         *  Adds a provider term.
         *
         *  @param term a provider term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addProviderTerm(org.osid.resource.ResourceQueryInspector term) {
            super.addProviderTerm(term);
            return;
        }

        
        /**
         *  Adds a collection of provider terms.
         *
         *  @param terms a collection of provider terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addProviderTerms(java.util.Collection<org.osid.resource.ResourceQueryInspector> terms) {
            super.addProviderTerms(terms);
            return;
        }
        
    
        /**
         *  Adds an asset <code>Id</code> term.
         *
         *  @param term an asset <code>Id</code> term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addBrandingIdTerm(org.osid.search.terms.IdTerm term) {
            super.addBrandingIdTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of asset <code>Id</code> terms.
         *
         *  @param terms a collection of asset <code>Id</code> terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addBrandingIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
            super.addBrandingIdTerms(terms);
            return;
        }
        
        
        /**
         *  Adds an asset <code>Id</code> term.
         *
         *  @param assetId the asset <code>Id</code>
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>assetId</code>
         *          or is <code>null</code>
         */

        @Override
        protected void addBrandingIdTerm(org.osid.id.Id assetId, boolean match) {
            super.addBrandingIdTerm(assetId, match);
            return;
        }
        
    
        /**
         *  Adds an asset term.
         *
         *  @param term an asset term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addBrandingTerm(org.osid.repository.AssetQueryInspector term) {
            super.addBrandingTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of asset terms.
         *
         *  @param terms a collection of asset terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addBrandingTerms(java.util.Collection<org.osid.repository.AssetQueryInspector> terms) {
            super.addBrandingTerms(terms);
            return;
        }
        

        /**
         *  Adds a license term.
         *
         *  @param term a license term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addLicenseTerm(org.osid.search.terms.StringTerm term) {
            super.addLicenseTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of license terms.
         *
         *  @param terms a collection of license terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addLicenseTerms(java.util.Collection<org.osid.search.terms.StringTerm> terms) {
            super.addLicenseTerms(terms);
            return;
        }
        
        
        /**
         *  Adds a license term.
         *
         *  @param license the license
         *  @param stringMatchType a string match type
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>id</code> or
         *          <code>stringMatchType</code> is <code>null</code>
         */
        
        @Override
        protected void addLicenseTerm(String license, org.osid.type.Type stringMatchType, boolean match) {
            super.addLicenseTerm(license, stringMatchType, match);
            return;
        }
    }
}
