//
// AbstractAdapterItemLookupSession.java
//
//    An Item lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Item lookup session adapter.
 */

public abstract class AbstractAdapterItemLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.ItemLookupSession {

    private final org.osid.assessment.ItemLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterItemLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterItemLookupSession(org.osid.assessment.ItemLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@code Bank} associated with this session.
     *
     *  @return the {@code Bank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@code Item} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupItems() {
        return (this.session.canLookupItems());
    }


    /**
     *  A complete view of the {@code Item} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeItemView() {
        this.session.useComparativeItemView();
        return;
    }


    /**
     *  A complete view of the {@code Item} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryItemView() {
        this.session.usePlenaryItemView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
     
    /**
     *  Gets the {@code Item} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Item} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Item} and
     *  retained for compatibility.
     *
     *  @param itemId {@code Id} of the {@code Item}
     *  @return the item
     *  @throws org.osid.NotFoundException {@code itemId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code itemId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItem(itemId));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Items} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  itemIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code itemIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByIds(itemIds));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  item genus {@code Type} which does not include
     *  items of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByGenusType(itemGenusType));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  item genus {@code Type} and include any additional
     *  items with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByParentGenusType(itemGenusType));
    }


    /**
     *  Gets an <code> ItemList </code> containing the given
     *  question. <code> </code> In plenary mode, the returned list
     *  contains all known items or an error results. Otherwise, the
     *  returned list may contain only those assessment items that are
     *  accessible through this session.
     *
     *  @param  questionId a question <code> Id </code> 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> questionId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByQuestion(org.osid.id.Id questionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getItemsByQuestion(questionId));
    }


    /**
     *  Gets an <code> ItemList </code> containing the given
     *  answer. <code> </code> In plenary mode, the returned list
     *  contains all known items or an error results. Otherwise, the
     *  returned list may contain only those assessment items that are
     *  accessible through this session.
     *
     *  @param  answerId an answer <code> Id </code> 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> answerId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByAnswer(org.osid.id.Id answerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByAnswer(answerId));
    }


    /**
     *  Gets an {@code ItemList} containing the given learning
     *  objective. In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those assessment items that are accessible
     *  through this session.
     *
     *  @param  objectiveId a learning objective {@code Id} 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code objectiveId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByLearningObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getItemsByLearningObjective(objectiveId));
    }


    /**
     *  Gets an {@code ItemList} containing the given learning
     *  objectives. In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those assessment items that are accessible
     *  through this session.
     *
     *  @param  objectiveIds a list of learning objective {@code Ids} 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code objectiveIds} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByLearningObjectives(org.osid.id.IdList objectiveIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByLearningObjectives(objectiveIds));
    }


    /**
     *  Gets an {@code ItemList} containing the given
     *  item record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByRecordType(itemRecordType));
    }


    /**
     *  Gets all {@code Items}. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Items} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItems());
    }
}
