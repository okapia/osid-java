//
// AbstractPoolConstrainerEnablerSearch.java
//
//     A template for making a PoolConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing pool constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPoolConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.PoolConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.PoolConstrainerEnablerSearchOrder poolConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of pool constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  poolConstrainerEnablerIds list of pool constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPoolConstrainerEnablers(org.osid.id.IdList poolConstrainerEnablerIds) {
        while (poolConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(poolConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPoolConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of pool constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPoolConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  poolConstrainerEnablerSearchOrder pool constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>poolConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPoolConstrainerEnablerResults(org.osid.provisioning.rules.PoolConstrainerEnablerSearchOrder poolConstrainerEnablerSearchOrder) {
	this.poolConstrainerEnablerSearchOrder = poolConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.PoolConstrainerEnablerSearchOrder getPoolConstrainerEnablerSearchOrder() {
	return (this.poolConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given pool constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool constrainer enabler implementing the requested record.
     *
     *  @param poolConstrainerEnablerSearchRecordType a pool constrainer enabler search record
     *         type
     *  @return the pool constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchRecord getPoolConstrainerEnablerSearchRecord(org.osid.type.Type poolConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(poolConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool constrainer enabler search. 
     *
     *  @param poolConstrainerEnablerSearchRecord pool constrainer enabler search record
     *  @param poolConstrainerEnablerSearchRecordType poolConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolConstrainerEnablerSearchRecord(org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchRecord poolConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type poolConstrainerEnablerSearchRecordType) {

        addRecordType(poolConstrainerEnablerSearchRecordType);
        this.records.add(poolConstrainerEnablerSearchRecord);        
        return;
    }
}
