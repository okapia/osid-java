//
// AbstractMapPoolConstrainerEnablerLookupSession
//
//    A simple framework for providing a PoolConstrainerEnabler lookup service
//    backed by a fixed collection of pool constrainer enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a PoolConstrainerEnabler lookup service backed by a
 *  fixed collection of pool constrainer enablers. The pool constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPoolConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractPoolConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.PoolConstrainerEnabler> poolConstrainerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.PoolConstrainerEnabler>());


    /**
     *  Makes a <code>PoolConstrainerEnabler</code> available in this session.
     *
     *  @param  poolConstrainerEnabler a pool constrainer enabler
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnabler<code>
     *          is <code>null</code>
     */

    protected void putPoolConstrainerEnabler(org.osid.provisioning.rules.PoolConstrainerEnabler poolConstrainerEnabler) {
        this.poolConstrainerEnablers.put(poolConstrainerEnabler.getId(), poolConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of pool constrainer enablers available in this session.
     *
     *  @param  poolConstrainerEnablers an array of pool constrainer enablers
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putPoolConstrainerEnablers(org.osid.provisioning.rules.PoolConstrainerEnabler[] poolConstrainerEnablers) {
        putPoolConstrainerEnablers(java.util.Arrays.asList(poolConstrainerEnablers));
        return;
    }


    /**
     *  Makes a collection of pool constrainer enablers available in this session.
     *
     *  @param  poolConstrainerEnablers a collection of pool constrainer enablers
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putPoolConstrainerEnablers(java.util.Collection<? extends org.osid.provisioning.rules.PoolConstrainerEnabler> poolConstrainerEnablers) {
        for (org.osid.provisioning.rules.PoolConstrainerEnabler poolConstrainerEnabler : poolConstrainerEnablers) {
            this.poolConstrainerEnablers.put(poolConstrainerEnabler.getId(), poolConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes a PoolConstrainerEnabler from this session.
     *
     *  @param  poolConstrainerEnablerId the <code>Id</code> of the pool constrainer enabler
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removePoolConstrainerEnabler(org.osid.id.Id poolConstrainerEnablerId) {
        this.poolConstrainerEnablers.remove(poolConstrainerEnablerId);
        return;
    }


    /**
     *  Gets the <code>PoolConstrainerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  poolConstrainerEnablerId <code>Id</code> of the <code>PoolConstrainerEnabler</code>
     *  @return the poolConstrainerEnabler
     *  @throws org.osid.NotFoundException <code>poolConstrainerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnabler getPoolConstrainerEnabler(org.osid.id.Id poolConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.PoolConstrainerEnabler poolConstrainerEnabler = this.poolConstrainerEnablers.get(poolConstrainerEnablerId);
        if (poolConstrainerEnabler == null) {
            throw new org.osid.NotFoundException("poolConstrainerEnabler not found: " + poolConstrainerEnablerId);
        }

        return (poolConstrainerEnabler);
    }


    /**
     *  Gets all <code>PoolConstrainerEnablers</code>. In plenary mode, the returned
     *  list contains all known poolConstrainerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  poolConstrainerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>PoolConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.ArrayPoolConstrainerEnablerList(this.poolConstrainerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolConstrainerEnablers.clear();
        super.close();
        return;
    }
}
