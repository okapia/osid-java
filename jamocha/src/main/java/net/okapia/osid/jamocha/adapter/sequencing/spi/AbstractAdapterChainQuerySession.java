//
// AbstractQueryChainLookupSession.java
//
//    A ChainQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ChainQuerySession adapter.
 */

public abstract class AbstractAdapterChainQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.sequencing.ChainQuerySession {

    private final org.osid.sequencing.ChainQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterChainQuerySession.
     *
     *  @param session the underlying chain query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterChainQuerySession(org.osid.sequencing.ChainQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeAntimatroid</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeAntimatroid Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAntimatroidId() {
        return (this.session.getAntimatroidId());
    }


    /**
     *  Gets the {@codeAntimatroid</code> associated with this 
     *  session.
     *
     *  @return the {@codeAntimatroid</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Antimatroid getAntimatroid()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAntimatroid());
    }


    /**
     *  Tests if this user can perform {@codeChain</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchChains() {
        return (this.session.canSearchChains());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include chains in antimatroids which are children
     *  of this antimatroid in the antimatroid hierarchy.
     */

    @OSID @Override
    public void useFederatedAntimatroidView() {
        this.session.useFederatedAntimatroidView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this antimatroid only.
     */
    
    @OSID @Override
    public void useIsolatedAntimatroidView() {
        this.session.useIsolatedAntimatroidView();
        return;
    }
    
      
    /**
     *  Gets a chain query. The returned query will not have an
     *  extension query.
     *
     *  @return the chain query 
     */
      
    @OSID @Override
    public org.osid.sequencing.ChainQuery getChainQuery() {
        return (this.session.getChainQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  chainQuery the chain query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code chainQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code chainQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByQuery(org.osid.sequencing.ChainQuery chainQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getChainsByQuery(chainQuery));
    }
}
