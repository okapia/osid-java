//
// AbstractScheduleSlotBatchFormList
//
//     Implements a filter for a ScheduleSlotBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.batch.scheduleslotbatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ScheduleSlotBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedScheduleSlotBatchFormList
 *  to improve performance.
 */

public abstract class AbstractScheduleSlotBatchFormFilterList
    extends net.okapia.osid.jamocha.calendaring.batch.scheduleslotbatchform.spi.AbstractScheduleSlotBatchFormList
    implements org.osid.calendaring.batch.ScheduleSlotBatchFormList,
               net.okapia.osid.jamocha.inline.filter.calendaring.batch.scheduleslotbatchform.ScheduleSlotBatchFormFilter {

    private org.osid.calendaring.batch.ScheduleSlotBatchForm scheduleSlotBatchForm;
    private final org.osid.calendaring.batch.ScheduleSlotBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractScheduleSlotBatchFormFilterList</code>.
     *
     *  @param scheduleSlotBatchFormList a <code>ScheduleSlotBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotBatchFormList</code> is <code>null</code>
     */

    protected AbstractScheduleSlotBatchFormFilterList(org.osid.calendaring.batch.ScheduleSlotBatchFormList scheduleSlotBatchFormList) {
        nullarg(scheduleSlotBatchFormList, "schedule slot batch form list");
        this.list = scheduleSlotBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.scheduleSlotBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ScheduleSlotBatchForm </code> in this list. 
     *
     *  @return the next <code> ScheduleSlotBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ScheduleSlotBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.batch.ScheduleSlotBatchForm getNextScheduleSlotBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.batch.ScheduleSlotBatchForm scheduleSlotBatchForm = this.scheduleSlotBatchForm;
            this.scheduleSlotBatchForm = null;
            return (scheduleSlotBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in schedule slot batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.scheduleSlotBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ScheduleSlotBatchForms.
     *
     *  @param scheduleSlotBatchForm the schedule slot batch form to filter
     *  @return <code>true</code> if the schedule slot batch form passes the filter,
     *          <code>false</code> if the schedule slot batch form should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.batch.ScheduleSlotBatchForm scheduleSlotBatchForm);


    protected void prime() {
        if (this.scheduleSlotBatchForm != null) {
            return;
        }

        org.osid.calendaring.batch.ScheduleSlotBatchForm scheduleSlotBatchForm = null;

        while (this.list.hasNext()) {
            try {
                scheduleSlotBatchForm = this.list.getNextScheduleSlotBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(scheduleSlotBatchForm)) {
                this.scheduleSlotBatchForm = scheduleSlotBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
