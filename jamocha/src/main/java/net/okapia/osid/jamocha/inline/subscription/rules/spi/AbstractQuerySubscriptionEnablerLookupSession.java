//
// AbstractQuerySubscriptionEnablerLookupSession.java
//
//    An inline adapter that maps a SubscriptionEnablerLookupSession to
//    a SubscriptionEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.subscription.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a SubscriptionEnablerLookupSession to
 *  a SubscriptionEnablerQuerySession.
 */

public abstract class AbstractQuerySubscriptionEnablerLookupSession
    extends net.okapia.osid.jamocha.subscription.rules.spi.AbstractSubscriptionEnablerLookupSession
    implements org.osid.subscription.rules.SubscriptionEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.subscription.rules.SubscriptionEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQuerySubscriptionEnablerLookupSession.
     *
     *  @param querySession the underlying subscription enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQuerySubscriptionEnablerLookupSession(org.osid.subscription.rules.SubscriptionEnablerQuerySession querySession) {
        nullarg(querySession, "subscription enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Publisher</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Publisher Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.session.getPublisherId());
    }


    /**
     *  Gets the <code>Publisher</code> associated with this 
     *  session.
     *
     *  @return the <code>Publisher</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPublisher());
    }


    /**
     *  Tests if this user can perform
     *  <code>SubscriptionEnabler</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSubscriptionEnablers() {
        return (this.session.canSearchSubscriptionEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscription enablers in publishers which
     *  are children of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.session.useFederatedPublisherView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.session.useIsolatedPublisherView();
        return;
    }
    

    /**
     *  Only active subscription enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveSubscriptionEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive subscription enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusSubscriptionEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SubscriptionEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SubscriptionEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>SubscriptionEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, subscription enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  subscription enablers are returned.
     *
     *  @param  subscriptionEnablerId <code>Id</code> of the
     *          <code>SubscriptionEnabler</code>
     *  @return the subscription enabler
     *  @throws org.osid.NotFoundException <code>subscriptionEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>subscriptionEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnabler getSubscriptionEnabler(org.osid.id.Id subscriptionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.rules.SubscriptionEnablerQuery query = getQuery();
        query.matchId(subscriptionEnablerId, true);
        org.osid.subscription.rules.SubscriptionEnablerList subscriptionEnablers = this.session.getSubscriptionEnablersByQuery(query);
        if (subscriptionEnablers.hasNext()) {
            return (subscriptionEnablers.getNextSubscriptionEnabler());
        } 
        
        throw new org.osid.NotFoundException(subscriptionEnablerId + " not found");
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  subscriptionEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>SubscriptionEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, subscription enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  subscription enablers are returned.
     *
     *  @param  subscriptionEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByIds(org.osid.id.IdList subscriptionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.rules.SubscriptionEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = subscriptionEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getSubscriptionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> corresponding to
     *  the given subscription enabler genus <code>Type</code> which
     *  does not include subscription enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the
     *  returned list may contain only those subscription enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, subscription enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  subscription enablers are returned.
     *
     *  @param  subscriptionEnablerGenusType a subscriptionEnabler genus type 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByGenusType(org.osid.type.Type subscriptionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.rules.SubscriptionEnablerQuery query = getQuery();
        query.matchGenusType(subscriptionEnablerGenusType, true);
        return (this.session.getSubscriptionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> corresponding to
     *  the given subscription enabler genus <code>Type</code> and
     *  include any additional subscription enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the
     *  returned list may contain only those subscription enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, subscription enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  subscription enablers are returned.
     *
     *  @param  subscriptionEnablerGenusType a subscriptionEnabler genus type 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByParentGenusType(org.osid.type.Type subscriptionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.rules.SubscriptionEnablerQuery query = getQuery();
        query.matchParentGenusType(subscriptionEnablerGenusType, true);
        return (this.session.getSubscriptionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> containing the
     *  given subscription enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the
     *  returned list may contain only those subscription enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, subscription enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  subscription enablers are returned.
     *
     *  @param  subscriptionEnablerRecordType a subscriptionEnabler record type 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByRecordType(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.rules.SubscriptionEnablerQuery query = getQuery();
        query.matchRecordType(subscriptionEnablerRecordType, true);
        return (this.session.getSubscriptionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the
     *  returned list may contain only those subscription enablers
     *  that are accessible through this session.
     *  
     *  In active mode, subscription enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  subscription enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SubscriptionEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.rules.SubscriptionEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getSubscriptionEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>SubscriptionEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the
     *  returned list may contain only those subscription enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, subscription enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  subscription enablers are returned.
     *
     *  @return a list of <code>SubscriptionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.subscription.rules.SubscriptionEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getSubscriptionEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.subscription.rules.SubscriptionEnablerQuery getQuery() {
        org.osid.subscription.rules.SubscriptionEnablerQuery query = this.session.getSubscriptionEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
