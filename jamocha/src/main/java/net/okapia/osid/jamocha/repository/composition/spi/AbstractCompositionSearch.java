//
// AbstractCompositionSearch.java
//
//     A template for making a Composition Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing composition searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCompositionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.repository.CompositionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.repository.records.CompositionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.repository.CompositionSearchOrder compositionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of compositions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  compositionIds list of compositions
     *  @throws org.osid.NullArgumentException
     *          <code>compositionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCompositions(org.osid.id.IdList compositionIds) {
        while (compositionIds.hasNext()) {
            try {
                this.ids.add(compositionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCompositions</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of composition Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCompositionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  compositionSearchOrder composition search order 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>compositionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCompositionResults(org.osid.repository.CompositionSearchOrder compositionSearchOrder) {
	this.compositionSearchOrder = compositionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.repository.CompositionSearchOrder getCompositionSearchOrder() {
	return (this.compositionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given composition search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a composition implementing the requested record.
     *
     *  @param compositionSearchRecordType a composition search record
     *         type
     *  @return the composition search record
     *  @throws org.osid.NullArgumentException
     *          <code>compositionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionSearchRecord getCompositionSearchRecord(org.osid.type.Type compositionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.repository.records.CompositionSearchRecord record : this.records) {
            if (record.implementsRecordType(compositionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this composition search. 
     *
     *  @param compositionSearchRecord composition search record
     *  @param compositionSearchRecordType composition search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCompositionSearchRecord(org.osid.repository.records.CompositionSearchRecord compositionSearchRecord, 
                                           org.osid.type.Type compositionSearchRecordType) {

        addRecordType(compositionSearchRecordType);
        this.records.add(compositionSearchRecord);        
        return;
    }
}
