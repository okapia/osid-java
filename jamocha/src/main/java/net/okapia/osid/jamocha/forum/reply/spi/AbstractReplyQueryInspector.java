//
// AbstractReplyQueryInspector.java
//
//     A template for making a ReplyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for replies.
 */

public abstract class AbstractReplyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractContainableOsidObjectQueryInspector
    implements org.osid.forum.ReplyQueryInspector {

    private final java.util.Collection<org.osid.forum.records.ReplyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the post <code> Id </code> terms. 
     *
     *  @return the post <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the post terms. 
     *
     *  @return the post terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getPostTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the timestamp terms. 
     *
     *  @return the timestamp terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getTimestampTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the poster <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPosterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the poster terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getPosterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the posting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the posting agent terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getPostingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the subject line terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSubjectLineTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the text terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the containing reply <code> Id </code> terms. 
     *
     *  @return the containing reply <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingReplyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the containing reply terms. 
     *
     *  @return the containing reply terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getContainingReplyTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the contained reply <code> Id </code> terms. 
     *
     *  @return the contained reply <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainedReplyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the contained reply terms. 
     *
     *  @return the contained reply terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getContainedReplyTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the forum <code> Id </code> terms. 
     *
     *  @return the forum <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getForumIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the forum terms. 
     *
     *  @return the forum terms 
     */

    @OSID @Override
    public org.osid.forum.ForumQueryInspector[] getForumTerms() {
        return (new org.osid.forum.ForumQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given reply query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a reply implementing the requested record.
     *
     *  @param replyRecordType a reply record type
     *  @return the reply query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(replyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplyQueryInspectorRecord getReplyQueryInspectorRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ReplyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(replyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this reply query. 
     *
     *  @param replyQueryInspectorRecord reply query inspector
     *         record
     *  @param replyRecordType reply record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addReplyQueryInspectorRecord(org.osid.forum.records.ReplyQueryInspectorRecord replyQueryInspectorRecord, 
                                                   org.osid.type.Type replyRecordType) {

        addRecordType(replyRecordType);
        nullarg(replyRecordType, "reply record type");
        this.records.add(replyQueryInspectorRecord);        
        return;
    }
}
