//
// AbstractOsidRelationship.java
//
//     Defines a simple OSID relationship to draw from.
//
//
// Tom Coppeto
// Okapia
// 22 January 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OSID relationship to draw from. using this
 *  abstract class requires that <code>setId()</code> must be done
 *  before passing this interface to a consumer in order to maintain
 *  compliance with the OSID specification.
 */

public abstract class AbstractOsidRelationship
    extends AbstractTemporalOsidObject
    implements org.osid.OsidRelationship {

    private org.osid.process.State reason;

    private final Temporal temporal = new Temporal();


    /**
     *  Tests if a reason this relationship came to an end is known. 
     *
     *  @return <code> true </code> if an end reason is available, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code>isEffective</code> is
     *          <code>true</code>
     */

    @OSID @Override
    public boolean hasEndReason() {
        if (isEffective()) {
            throw new org.osid.IllegalStateException("isEffective() is true");
        }

        return (this.reason != null);
    }


    /**
     *  Gets a state <code> Id </code> indicating why this relationship has 
     *  ended. 
     *
     *  @return a state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndReasonId() {
        if (!hasEndReason()) {
            throw new org.osid.IllegalStateException("hasEndReason() is false");
        }

        return (this.reason.getId());
    }


    /**
     *  Gets a state indicating why this relationship has ended. 
     *
     *  @return a state 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getEndReason()
        throws org.osid.OperationFailedException {

        if (!hasEndReason()) {
            throw new org.osid.IllegalStateException("hasEndReason() is false");
        }

        return (this.reason);
    }


    /**
     *  Sets the end state.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    protected void setEndReason(org.osid.process.State state) {
        nullarg(state, "end state");
        this.reason = state;
        return;
    }
}
