//
// AbstractCookbookQuery.java
//
//     A template for making a Cookbook Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.cookbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for cookbooks.
 */

public abstract class AbstractCookbookQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.recipe.CookbookQuery {

    private final java.util.Collection<org.osid.recipe.records.CookbookQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the recipe <code> Id </code> for this query. 
     *
     *  @param  recipeId a recipe <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recipeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipeId(org.osid.id.Id recipeId, boolean match) {
        return;
    }


    /**
     *  Clears the recipe <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipeIdTerms() {
        return;
    }


    /**
     *  Tests if a recipe query is available. 
     *
     *  @return <code> true </code> if a recipe query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipe. 
     *
     *  @return the recipe query 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuery getRecipeQuery() {
        throw new org.osid.UnimplementedException("supportsRecipeQuery() is false");
    }


    /**
     *  Matches cook books with any recipe. 
     *
     *  @param  match <code> true </code> to match cook books with any recipe, 
     *          <code> false </code> to match cook books with no recipes 
     */

    @OSID @Override
    public void matchAnyRecipe(boolean match) {
        return;
    }


    /**
     *  Clears the procedure terms. 
     */

    @OSID @Override
    public void clearRecipeTerms() {
        return;
    }


    /**
     *  Sets the procedure <code> Id </code> for this query to match 
     *  procedures assigned to cook books. 
     *
     *  @param  procedureId a procedure <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> procedureId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcedureId(org.osid.id.Id procedureId, boolean match) {
        return;
    }


    /**
     *  Clears the procedure <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProcedureIdTerms() {
        return;
    }


    /**
     *  Tests if a procedure query is available. 
     *
     *  @return <code> true </code> if a procedure query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureQuery() {
        return (false);
    }


    /**
     *  Gets the query for an procedure. 
     *
     *  @return the procedure query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuery getProcedureQuery() {
        throw new org.osid.UnimplementedException("supportsProcedureQuery() is false");
    }


    /**
     *  Matches cook books with any procedure. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          procedure, <code> false </code> to match cook books with no 
     *          procedures 
     */

    @OSID @Override
    public void matchAnyProcedure(boolean match) {
        return;
    }


    /**
     *  Clears the procedure terms. 
     */

    @OSID @Override
    public void clearProcedureTerms() {
        return;
    }


    /**
     *  Sets a direction <code> Id. </code> 
     *
     *  @param  directionId a direction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> directionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDirectionId(org.osid.id.Id directionId, boolean match) {
        return;
    }


    /**
     *  Clears the direction <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDirectionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DirectionQuery </code> is available. 
     *
     *  @return <code> true </code> if a direction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a direction query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the direction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuery getDirectionQuery() {
        throw new org.osid.UnimplementedException("supportsDirectionQuery() is false");
    }


    /**
     *  Matches any direction. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          direction, <code> false </code> to match cook books with no 
     *          directions 
     */

    @OSID @Override
    public void matchAnyDirection(boolean match) {
        return;
    }


    /**
     *  Clears the direction terms. 
     */

    @OSID @Override
    public void clearDirectionTerms() {
        return;
    }


    /**
     *  Sets the cook book <code> Id </code> for this query to match cook 
     *  books that have the specified cook book as an ancestor. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCookbookId(org.osid.id.Id cookbookId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the ancestor cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCookbookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCookbookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getAncestorCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCookbookQuery() is false");
    }


    /**
     *  Matches cook books with any ancestor. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          ancestor, <code> false </code> to match root cook books 
     */

    @OSID @Override
    public void matchAnyAncestorCookbook(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor cook book terms. 
     */

    @OSID @Override
    public void clearAncestorCookbookTerms() {
        return;
    }


    /**
     *  Sets the cook book <code> Id </code> for this query to match cook 
     *  books that have the specified cook book as a descendant. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCookbookId(org.osid.id.Id cookbookId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the descendant cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCookbookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCookbookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getDescendantCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCookbookQuery() is false");
    }


    /**
     *  Matches cook books with any descendant. 
     *
     *  @param  match <code> true </code> to match cook books with any 
     *          descendant, <code> false </code> to match leaf cook books 
     */

    @OSID @Override
    public void matchAnyDescendantCookbook(boolean match) {
        return;
    }


    /**
     *  Clears the descendant cook book terms. 
     */

    @OSID @Override
    public void clearDescendantCookbookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given cookbook query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a cookbook implementing the requested record.
     *
     *  @param cookbookRecordType a cookbook record type
     *  @return the cookbook query record
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cookbookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.CookbookQueryRecord getCookbookQueryRecord(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.CookbookQueryRecord record : this.records) {
            if (record.implementsRecordType(cookbookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cookbookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this cookbook query. 
     *
     *  @param cookbookQueryRecord cookbook query record
     *  @param cookbookRecordType cookbook record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCookbookQueryRecord(org.osid.recipe.records.CookbookQueryRecord cookbookQueryRecord, 
                                          org.osid.type.Type cookbookRecordType) {

        addRecordType(cookbookRecordType);
        nullarg(cookbookQueryRecord, "cookbook query record");
        this.records.add(cookbookQueryRecord);        
        return;
    }
}
