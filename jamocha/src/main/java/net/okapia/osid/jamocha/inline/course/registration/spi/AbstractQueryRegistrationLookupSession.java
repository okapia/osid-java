//
// AbstractQueryRegistrationLookupSession.java
//
//    An inline adapter that maps a RegistrationLookupSession to
//    a RegistrationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RegistrationLookupSession to
 *  a RegistrationQuerySession.
 */

public abstract class AbstractQueryRegistrationLookupSession
    extends net.okapia.osid.jamocha.course.registration.spi.AbstractRegistrationLookupSession
    implements org.osid.course.registration.RegistrationLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.course.registration.RegistrationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRegistrationLookupSession.
     *
     *  @param querySession the underlying registration query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRegistrationLookupSession(org.osid.course.registration.RegistrationQuerySession querySession) {
        nullarg(querySession, "registration query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Registration</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRegistrations() {
        return (this.session.canSearchRegistrations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include registrations in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only registrations whose effective dates are current are
     *  returned by methods in this session.
     */

    public void useEffectiveRegistrationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All registrations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRegistrationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Registration</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Registration</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Registration</code> and
     *  retained for compatibility.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationId <code>Id</code> of the
     *          <code>Registration</code>
     *  @return the registration
     *  @throws org.osid.NotFoundException <code>registrationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>registrationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.Registration getRegistration(org.osid.id.Id registrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchId(registrationId, true);
        org.osid.course.registration.RegistrationList registrations = this.session.getRegistrationsByQuery(query);
        if (registrations.hasNext()) {
            return (registrations.getNextRegistration());
        } 
        
        throw new org.osid.NotFoundException(registrationId + " not found");
    }


    /**
     *  Gets a <code>RegistrationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  registrations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Registrations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, registrations are returned that are currently effective.
     *  In any effective mode, effective registrations and those currently expired
     *  are returned.
     *
     *  @param  registrationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>registrationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByIds(org.osid.id.IdList registrationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();

        try (org.osid.id.IdList ids = registrationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a <code>RegistrationList</code> corresponding to the given
     *  registration genus <code>Type</code> which does not include
     *  registrations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently effective.
     *  In any effective mode, effective registrations and those currently expired
     *  are returned.
     *
     *  @param  registrationGenusType a registration genus type 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByGenusType(org.osid.type.Type registrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchGenusType(registrationGenusType, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a <code>RegistrationList</code> corresponding to the given
     *  registration genus <code>Type</code> and include any additional
     *  registrations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationGenusType a registration genus type 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByParentGenusType(org.osid.type.Type registrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchParentGenusType(registrationGenusType, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a <code>RegistrationList</code> containing the given
     *  registration record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationRecordType a registration record type 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByRecordType(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchRecordType(registrationRecordType, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a <code>RegistrationList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Registration</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsOnDate(org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRegistrationsByQuery(query));
    }
        

    /**
     *  Gets a list of registrations corresponding to a activity
     *  bundle <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the <code>Id</code> of the activity bundle
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundle(org.osid.id.Id activityBundleId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchActivityBundleId(activityBundleId, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of registrations corresponding to a activity bundle
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param activityBundleId the <code>Id</code> of the activity
     *         bundle
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleOnDate(org.osid.id.Id activityBundleId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchActivityBundleId(activityBundleId, true);
        query.matchDate(from, to, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of registrations corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.RegistrationList getRegistrationsForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of registrations corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of registrations corresponding to activity bundle
     *  and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the <code>Id</code> of the activity bundle
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleAndStudent(org.osid.id.Id activityBundleId,
                                                                                                     org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchActivityBundleId(activityBundleId, true);
        query.matchStudentId(resourceId, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of registrations corresponding to activity bundle
     *  and student <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param activityBundleId the <code>Id</code> of the activity
     *  bundle
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleAndStudentOnDate(org.osid.id.Id activityBundleId,
                                                                                                           org.osid.id.Id resourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchActivityBundleId(activityBundleId, true);
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of registrations corresponding to a course
     *  offering <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param courseOfferingId the <code>Id</code> of the course
     *         offering
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.RegistrationList getRegistrationsForCourseOffering(org.osid.id.Id courseOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        if (query.supportsActivityBundleQuery()) {
            query.getActivityBundleQuery().matchCourseOfferingId(courseOfferingId, true);
            return (this.session.getRegistrationsByQuery(query));
        }

        return (super.getRegistrationsForCourseOffering(courseOfferingId));
    }


    /**
     *  Gets a list of registrations corresponding to a course offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param courseOfferingId the <code>Id</code> of the course
     *         offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        if (query.supportsActivityBundleQuery()) {
            query.getActivityBundleQuery().matchCourseOfferingId(courseOfferingId, true);
            query.matchDate(from, to, true);
            return (this.session.getRegistrationsByQuery(query));
        }

        return (super.getRegistrationsForCourseOfferingOnDate(courseOfferingId, from, to));
    }


    /**
     *  Gets a list of registrations corresponding to course offering
     *  and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingAndStudent(org.osid.id.Id courseOfferingId,
                                                                                                     org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.RegistrationQuery query = getQuery();
        if (query.supportsActivityBundleQuery()) {
            query.getActivityBundleQuery().matchCourseOfferingId(courseOfferingId, true);
            query.matchStudentId(resourceId, true);
            return (this.session.getRegistrationsByQuery(query));
        }

        return (super.getRegistrationsForCourseOfferingAndStudent(courseOfferingId, resourceId));
    }


    /**
     *  Gets a list of registrations corresponding to course offering
     *  and student <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param courseOfferingId the <code>Id</code> of the course
     *  offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingAndStudentOnDate(org.osid.id.Id courseOfferingId,
                                                                                                           org.osid.id.Id resourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.RegistrationQuery query = getQuery();
        if (query.supportsActivityBundleQuery()) {
            query.getActivityBundleQuery().matchCourseOfferingId(courseOfferingId, true);
            query.matchStudentId(resourceId, true);
            query.matchDate(from, to, true);            
            return (this.session.getRegistrationsByQuery(query));
        }

        return (super.getRegistrationsForCourseOfferingAndStudentOnDate(courseOfferingId, resourceId, from, to));
    }

    
    /**
     *  Gets all <code>Registrations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Registrations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.RegistrationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRegistrationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.registration.RegistrationQuery getQuery() {
        org.osid.course.registration.RegistrationQuery query = this.session.getRegistrationQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
