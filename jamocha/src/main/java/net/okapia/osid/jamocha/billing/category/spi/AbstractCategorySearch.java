//
// AbstractCategorySearch.java
//
//     A template for making a Category Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.category.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing category searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCategorySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.billing.CategorySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.billing.records.CategorySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.billing.CategorySearchOrder categorySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of categories. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  categoryIds list of categories
     *  @throws org.osid.NullArgumentException
     *          <code>categoryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCategories(org.osid.id.IdList categoryIds) {
        while (categoryIds.hasNext()) {
            try {
                this.ids.add(categoryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCategories</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of category Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCategoryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  categorySearchOrder category search order 
     *  @throws org.osid.NullArgumentException
     *          <code>categorySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>categorySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCategoryResults(org.osid.billing.CategorySearchOrder categorySearchOrder) {
	this.categorySearchOrder = categorySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.billing.CategorySearchOrder getCategorySearchOrder() {
	return (this.categorySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given category search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a category implementing the requested record.
     *
     *  @param categorySearchRecordType a category search record
     *         type
     *  @return the category search record
     *  @throws org.osid.NullArgumentException
     *          <code>categorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(categorySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategorySearchRecord getCategorySearchRecord(org.osid.type.Type categorySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.billing.records.CategorySearchRecord record : this.records) {
            if (record.implementsRecordType(categorySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(categorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this category search. 
     *
     *  @param categorySearchRecord category search record
     *  @param categorySearchRecordType category search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCategorySearchRecord(org.osid.billing.records.CategorySearchRecord categorySearchRecord, 
                                           org.osid.type.Type categorySearchRecordType) {

        addRecordType(categorySearchRecordType);
        this.records.add(categorySearchRecord);        
        return;
    }
}
