//
// UnknownQualifier.java
//
//     Defines an unknown Qualifier.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.authorization.qualifier;


/**
 *  Defines an unknown <code>Qualifier</code>.
 */

public final class UnknownQualifier
    extends net.okapia.osid.jamocha.nil.authorization.qualifier.spi.AbstractUnknownQualifier
    implements org.osid.authorization.Qualifier {


    /**
     *  Constructs a new <code>UnknownQualifier</code>.
     */

    public UnknownQualifier() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownQualifier</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownQualifier(boolean optional) {
        super(optional);
        addQualifierRecord(new QualifierRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Qualifier.
     *
     *  @return an unknown Qualifier
     */

    public static org.osid.authorization.Qualifier create() {
        return (net.okapia.osid.jamocha.builder.validator.authorization.qualifier.QualifierValidator.validateQualifier(new UnknownQualifier()));
    }


    public class QualifierRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.authorization.records.QualifierRecord {

        
        protected QualifierRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
