//
// AbstractParticipant.java
//
//     Defines a Participant.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Participant</code>.
 */

public abstract class AbstractParticipant
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.offering.Participant {

    private org.osid.offering.Offering offering;
    private org.osid.resource.Resource resource;
    private org.osid.calendaring.TimePeriod timePeriod;
    private final java.util.Collection<org.osid.grading.GradeSystem> resultOptions = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.offering.records.ParticipantRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the offering to which this participant 
     *  is assigned. 
     *
     *  @return the <code> Offering </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOfferingId() {
        return (this.offering.getId());
    }


    /**
     *  Gets the offering for this participant. 
     *
     *  @return the <code> Offering </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.offering.Offering getOffering()
        throws org.osid.OperationFailedException {

        return (this.offering);
    }


    /**
     *  Sets the offering.
     *
     *  @param offering an offering
     *  @throws org.osid.NullArgumentException
     *          <code>offering</code> is <code>null</code>
     */

    protected void setOffering(org.osid.offering.Offering offering) {
        nullarg(offering, "offering");
        this.offering = offering;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource for this participant. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the time period. 
     *
     *  @return the <code> TimePeriod </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        return (this.timePeriod.getId());
    }


    /**
     *  Gets the time period for this participant. 
     *
     *  @return the <code> TimePeriod </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        return (this.timePeriod);
    }


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriod</code> is <code>null</code>
     */

    protected void setTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        nullarg(timePeriod, "time period");
        this.timePeriod = timePeriod;
        return;
    }


    /**
     *  Tests if ththere are result options that constrain results.
     *
     *  @return <code> true </code> if there are results results,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasResults() {
        return (this.resultOptions.size() > 0);
    }


    /**
     *  Gets the various result option <code> Ids </code> applied to this 
     *  participation. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getResultOptionIds() {
        try {
            org.osid.grading.GradeSystemList resultOptions = getResultOptions();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.gradesystem.GradeSystemToIdList(resultOptions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the various result option <code> Ids </code> applied to this 
     *  participation. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getResultOptions()
        throws org.osid.OperationFailedException {

        if (!hasResults()) {
            throw new org.osid.IllegalStateException("hasResults() is false");
        }

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.resultOptions));
    }


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    protected void addResultOption(org.osid.grading.GradeSystem resultOption) {
        this.resultOptions.add(resultOption);
        return;
    }


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    protected void setResultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions) {
        this.resultOptions.clear();
        this.resultOptions.addAll(resultOptions);
        return;
    }

    /**
     *  Tests if this participant supports the given record
     *  <code>Type</code>.
     *
     *  @param  participantRecordType a participant record type 
     *  @return <code>true</code> if the participantRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type participantRecordType) {
        for (org.osid.offering.records.ParticipantRecord record : this.records) {
            if (record.implementsRecordType(participantRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  participantRecordType the participant record type 
     *  @return the participant record 
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(participantRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantRecord getParticipantRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ParticipantRecord record : this.records) {
            if (record.implementsRecordType(participantRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantRecordType + " is not supported");
    }


    /**
     *  Adds a record to this participant. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param participantRecord the participant record
     *  @param participantRecordType participant record type
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecord</code> or
     *          <code>participantRecordTypeparticipant</code> is
     *          <code>null</code>
     */
            
    protected void addParticipantRecord(org.osid.offering.records.ParticipantRecord participantRecord, 
                                        org.osid.type.Type participantRecordType) {

        nullarg(participantRecord, "participant record");
        addRecordType(participantRecordType);
        this.records.add(participantRecord);
        
        return;
    }
}
