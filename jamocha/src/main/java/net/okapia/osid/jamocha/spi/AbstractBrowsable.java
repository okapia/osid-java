//
// AbstractBrowsable.java
//
//     Defines a simple OSID object to draw from.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OSID object to draw from. using this abstract
 *  class requires that <code>setId()</code> must be done before
 *  passing this interface to a consumer in order to maintain
 *  compliance with the OSID specification.
 */

public abstract class AbstractBrowsable
    extends AbstractExtensible
    implements org.osid.Browsable {

    private final java.util.Map<org.osid.type.Type, java.util.Collection<org.osid.Property>> properties = java.util.Collections.synchronizedMap(new java.util.HashMap<org.osid.type.Type, java.util.Collection<org.osid.Property>>());


    /**
     *  Gets a list of all properties of this object including those
     *  corresponding to data within this object's records. Properties
     *  provide a means for applications to display a representation
     *  of the contents of an object without understanding its record
     *  interface specifications. Applications needing to examine a
     *  specific property or perform updates should use the methods
     *  defined by the object's record <code> Type. </code>
     *
     *  @return a list of properties 
     */
    
    @OSID @Override
    public org.osid.PropertyList getProperties()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.List<org.osid.Property> properties = new java.util.ArrayList<org.osid.Property>();

        for (java.util.Collection<org.osid.Property> set : this.properties.values()) {
            properties.addAll(set);
        }

        return (new net.okapia.osid.jamocha.property.ArrayPropertyList(properties));
    }


    /**
     *  Gets the properties by record type.
     *
     *  @param  recordType the record type corresponding to the
     *          properties set to retrieve
     *  @return a list of properties 
     *  @throws org.osid.NullArgumentException <code> recordType
     *          </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(recordType) </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.PropertyList getPropertiesByRecordType(org.osid.type.Type recordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (!hasRecordType(recordType)) {
            throw new org.osid.UnsupportedException("record type not supported");
        }
        
        java.util.Collection<org.osid.Property> set = this.properties.get(recordType);
        if (set == null) {
            return (new net.okapia.osid.jamocha.nil.property.EmptyPropertyList());
        } else {
            return (new net.okapia.osid.jamocha.property.ArrayPropertyList(set));
        }
    }

    
    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    protected void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        nullarg(set, "set");
        nullarg(recordType, "record type");
        this.properties.put(recordType, set);
        return;
    }
}
