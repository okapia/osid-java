//
// AbstractQualifier.java
//
//     Defines a Qualifier builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.qualifier.spi;


/**
 *  Defines a <code>Qualifier</code> builder.
 */

public abstract class AbstractQualifierBuilder<T extends AbstractQualifierBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.authorization.qualifier.QualifierMiter qualifier;


    /**
     *  Constructs a new <code>AbstractQualifierBuilder</code>.
     *
     *  @param qualifier the qualifier to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractQualifierBuilder(net.okapia.osid.jamocha.builder.authorization.qualifier.QualifierMiter qualifier) {
        super(qualifier);
        this.qualifier = qualifier;
        return;
    }


    /**
     *  Builds the qualifier.
     *
     *  @return the new qualifier
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.authorization.Qualifier build() {
        (new net.okapia.osid.jamocha.builder.validator.authorization.qualifier.QualifierValidator(getValidations())).validate(this.qualifier);
        return (new net.okapia.osid.jamocha.builder.authorization.qualifier.ImmutableQualifier(this.qualifier));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the qualifier miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.authorization.qualifier.QualifierMiter getMiter() {
        return (this.qualifier);
    }


    /**
     *  Adds a Qualifier record.
     *
     *  @param record a qualifier record
     *  @param recordType the type of qualifier record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.authorization.records.QualifierRecord record, org.osid.type.Type recordType) {
        getMiter().addQualifierRecord(record, recordType);
        return (self());
    }
}       


