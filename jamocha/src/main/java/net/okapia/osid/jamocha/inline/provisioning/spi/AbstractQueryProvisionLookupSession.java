//
// AbstractQueryProvisionLookupSession.java
//
//    An inline adapter that maps a ProvisionLookupSession to
//    a ProvisionQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProvisionLookupSession to
 *  a ProvisionQuerySession.
 */

public abstract class AbstractQueryProvisionLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractProvisionLookupSession
    implements org.osid.provisioning.ProvisionLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.provisioning.ProvisionQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProvisionLookupSession.
     *
     *  @param querySession the underlying provision query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProvisionLookupSession(org.osid.provisioning.ProvisionQuerySession querySession) {
        nullarg(querySession, "provision query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>Provision</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProvisions() {
        return (this.session.canSearchProvisions());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisions in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only provisions whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProvisionView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All provisions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProvisionView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Provision</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Provision</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Provision</code> and
     *  retained for compatibility.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionId <code>Id</code> of the
     *          <code>Provision</code>
     *  @return the provision
     *  @throws org.osid.NotFoundException <code>provisionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>provisionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getProvision(org.osid.id.Id provisionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchId(provisionId, true);
        org.osid.provisioning.ProvisionList provisions = this.session.getProvisionsByQuery(query);
        if (provisions.hasNext()) {
            return (provisions.getNextProvision());
        } 
        
        throw new org.osid.NotFoundException(provisionId + " not found");
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  provisions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Provisions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, provisions are returned that are currently effective.
     *  In any effective mode, effective provisions and those currently expired
     *  are returned.
     *
     *  @param  provisionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>provisionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByIds(org.osid.id.IdList provisionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();

        try (org.osid.id.IdList ids = provisionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  provision genus <code>Type</code> which does not include
     *  provisions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently effective.
     *  In any effective mode, effective provisions and those currently expired
     *  are returned.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchGenusType(provisionGenusType, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  provision genus <code>Type</code> and include any additional
     *  provisions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByParentGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchParentGenusType(provisionGenusType, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionList</code> containing the given
     *  provision record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByRecordType(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchRecordType(provisionRecordType, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *  
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Provision</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getProvisionsByQuery(query));
    }
        

    /**
     *  Gets a list of provisions for a supplied broker.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *  
     *  In effective mode, provisions are returned that are currently
     *  effective. In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  brokerId a broker <code> Id </code> 
     *  @return the returned <code> Provision </code> list 
     *  @throws org.osid.NullArgumentException <code> brokerId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchBrokerId(brokerId, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a list of provisions for a supplied broker. and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *  
     *  In effective mode, provisions are returned that are currently
     *  effective in addition to being effective in the guven date
     *  range. In any effective mode, effective provisions and those
     *  currently expired are returned.
     *
     *  @param  brokerId a broker <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Provision </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> brokerId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForBrokerOnDate(org.osid.id.Id brokerId, 
                                                                            org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchBrokerId(brokerId, true);
        query.matchDate(from, to, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForProvisionable(org.osid.id.Id provisionableId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchProvisionableId(provisionableId, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableOnDate(org.osid.id.Id provisionableId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchProvisionableId(provisionableId, true);
        query.matchDate(from, to, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForRecipient(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and
     *  recipient <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipient(org.osid.id.Id provisionableId,
                                                                                         org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchProvisionableId(provisionableId, true);
        query.matchRecipientId(resourceId, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and recipient
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipientOnDate(org.osid.id.Id provisionableId,
                                                                                               org.osid.id.Id resourceId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchProvisionableId(provisionableId, true);
        query.matchRecipientId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getProvisionsByQuery(query));
    }

    
    /**
     *  Gets a list of provisions for a request. 
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session.
     *  
     *  In effective mode, provisions are returned that are currently
     *  effective. In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  requestId a request <code> Id </code> 
     *  @return the returned <code> Provision </code> list 
     *  @throws org.osid.NullArgumentException <code> requestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForRequest(org.osid.id.Id requestId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchRequestId(requestId, true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets all <code>Provisions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Provisions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProvisionsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.ProvisionQuery getQuery() {
        org.osid.provisioning.ProvisionQuery query = this.session.getProvisionQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
