//
// AbstractImmutableRequest.java
//
//     Wraps a mutable Request to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Request</code> to hide modifiers. This
 *  wrapper provides an immutized Request from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying request whose state changes are visible.
 */

public abstract class AbstractImmutableRequest
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.provisioning.Request {

    private final org.osid.provisioning.Request request;


    /**
     *  Constructs a new <code>AbstractImmutableRequest</code>.
     *
     *  @param request the request to immutablize
     *  @throws org.osid.NullArgumentException <code>request</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRequest(org.osid.provisioning.Request request) {
        super(request);
        this.request = request;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the transaction in which this request is 
     *  a part. Requests can be made individually or as part of a transaction 
     *  group to provide an atomic compound request. 
     *
     *  @return the request transaction <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRequestTransactionId() {
        return (this.request.getRequestTransactionId());
    }


    /**
     *  Gets the <code> Id </code> of the transaction in which this request is 
     *  a part. Requests can be made individually or as part of a transaction 
     *  group to provide an atomic compound request. 
     *
     *  @return the request transaction 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransaction getRequestTransaction()
        throws org.osid.OperationFailedException {

        return (this.request.getRequestTransaction());
    }


    /**
     *  Gets the <code> Id </code> of the queue. 
     *
     *  @return the queue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQueueId() {
        return (this.request.getQueueId());
    }


    /**
     *  Gets the queue. 
     *
     *  @return the queue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Queue getQueue()
        throws org.osid.OperationFailedException {

        return (this.request.getQueue());
    }


    /**
     *  Gets the date of the request. 
     *
     *  @return the request date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getRequestDate() {
        return (this.request.getRequestDate());
    }


    /**
     *  Gets the <code> Id </code> of the queued resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRequesterId() {
        return (this.request.getRequesterId());
    }


    /**
     *  Gets the queued resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRequester()
        throws org.osid.OperationFailedException {

        return (this.request.getRequester());
    }


    /**
     *  Gets the <code> Id </code> of the requesting agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRequestingAgentId() {
        return (this.request.getRequestingAgentId());
    }


    /**
     *  Gets the requesting agent. 
     *
     *  @return the requesting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getRequestingAgent()
        throws org.osid.OperationFailedException {

        return (this.request.getRequestingAgent());
    }


    /**
     *  Tests if this request is qualified by a pool. An unqualified request 
     *  may be allocated out of any pool in the broker. 
     *
     *  @return <code> true </code> if this request has a pool, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasPool() {
        return (this.request.hasPool());
    }


    /**
     *  Gets the <code> Id </code> of the pool. 
     *
     *  @return the pool <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasPool() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPoolId() {
        return (this.request.getPoolId());
    }


    /**
     *  Gets the pool. 
     *
     *  @return the pool 
     *  @throws org.osid.IllegalStateException <code> hasPool() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool()
        throws org.osid.OperationFailedException {

        return (this.request.getPool());
    }


    /**
     *  Gets the <code> Ids </code> of the requested provisionables. 
     *
     *  @return the requested provisionable <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRequestedProvisionableIds() {
        return (this.request.getRequestedProvisionableIds());
    }


    /**
     *  Gets the requested provisionables. 
     *
     *  @return the requested provisionables 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getRequestedProvisionables()
        throws org.osid.OperationFailedException {

        return (this.request.getRequestedProvisionables());
    }


    /**
     *  Tests if this request was created to exchange a provision. 
     *
     *  @return <code> true </code> if this request is an exchange request, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isExchange() {
        return (this.request.isExchange());
    }


    /**
     *  Gets the <code> Id </code> of the provision to be exchanged if this 
     *  request is provisioned. 
     *
     *  @return the exchange provision <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isExchange() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getExchangeProvisionId() {
        return (this.request.getExchangeProvisionId());
    }


    /**
     *  Gets the provision to be exchanged if this request is provisioned. 
     *
     *  @return the exchange provision 
     *  @throws org.osid.IllegalStateException <code> isExchange() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getExchangeProvision()
        throws org.osid.OperationFailedException {

        return (this.request.getExchangeProvision());
    }


    /**
     *  Tests if this request was created as a result of another provision. 
     *
     *  @return <code> true </code> if this request is a provision result, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isProvisionResult() {
        return (this.request.isProvisionResult());
    }


    /**
     *  Gets the <code> Id </code> of the provision that resulted in this 
     *  request. 
     *
     *  @return the origin provision <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isProvisionResult() 
     *          </code> is f <code> alse </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOriginProvisionId() {
        return (this.request.getOriginProvisionId());
    }


    /**
     *  Gets the provision that resulted in this request. 
     *
     *  @return the origin provision 
     *  @throws org.osid.IllegalStateException <code> isProvisionResult() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getOriginProvision()
        throws org.osid.OperationFailedException {

        return (this.request.getOriginProvision());
    }


    /**
     *  Tests if this request has a position in the queue. A position may be 
     *  indicate the rank of the request among waiting requests for the same 
     *  pool. 
     *
     *  @return true if this request has a position, false otherwise 
     */

    @OSID @Override
    public boolean hasPosition() {
        return (this.request.hasPosition());
    }


    /**
     *  Gets the position of this request in the queue. A position may be 
     *  indicate the rank of the request among waiting request for the same 
     *  pool. 
     *
     *  @return the position 
     *  @throws org.osid.IllegalStateException <code> hasPosition() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getPosition() {
        return (this.request.getPosition());
    }


    /**
     *  Tests if there is an estimated waiting time for a provision out of the 
     *  pool. 
     *
     *  @return <code> true </code> if this request has an estimated time, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasEWA() {
        return (this.request.hasEWA());
    }


    /**
     *  Gets the estimated waiting time for a provision out of the pool. 
     *
     *  @return the estimated waiting time 
     *  @throws org.osid.IllegalStateException <code> hasEWA() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getEWA() {
        return (this.request.getEWA());
    }


    /**
     *  Gets the request record corresponding to the given <code> Request 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> requestRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(requestRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  requestRecordType the type of request record to retrieve 
     *  @return the request record 
     *  @throws org.osid.NullArgumentException <code> requestRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(requestRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestRecord getRequestRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        return (this.request.getRequestRecord(requestRecordType));
    }
}

