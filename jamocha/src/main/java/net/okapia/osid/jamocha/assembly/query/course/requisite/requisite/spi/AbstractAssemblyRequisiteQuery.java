//
// AbstractAssemblyRequisiteQuery.java
//
//     A RequisiteQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RequisiteQuery that stores terms.
 */

public abstract class AbstractAssemblyRequisiteQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.course.requisite.RequisiteQuery,
               org.osid.course.requisite.RequisiteQueryInspector,
               org.osid.course.requisite.RequisiteSearchOrder {

    private final java.util.Collection<org.osid.course.requisite.records.RequisiteQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.RequisiteQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.RequisiteSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidContainableQuery query;


    /** 
     *  Constructs a new <code>AbstractAssemblyRequisiteQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRequisiteQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidContainableQuery(assembler);
        return;
    }
    

    /**
     *  Match containables that are sequestered. 
     *
     *  @param  match <code> true </code> to match any sequestered 
     *          containables, <code> false </code> to match non-sequestered 
     *          containables 
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.query.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms. 
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.query.clearSequesteredTerms();
        return;
    }

    
    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.query.getSequesteredTerms());
    }

    
    /**
     *  Specifies a preference for ordering the result set by the sequestered 
     *  flag. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.query.orderBySequestered(style);
        return;
    }


    /**
     *  Gets the column name for the sequestered field.
     *
     *  @return the column name
     */

    protected String getSequesteredColumn() {
        return (this.query.getSequesteredColumn());
    }


    /**
     *  Sets the requisite <code> Id </code> for this query to match 
     *  requisites that a requisite option. 
     *
     *  @param  requisiteId a requisite <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequisiteOptionId(org.osid.id.Id requisiteId, 
                                       boolean match) {
        getAssembler().addIdTerm(getRequisiteOptionIdColumn(), requisiteId, match);
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRequisiteOptionIdTerms() {
        getAssembler().clearTerms(getRequisiteOptionIdColumn());
        return;
    }


    /**
     *  Gets the requisite option <code> Id </code> query terms. 
     *
     *  @return the requisite <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequisiteOptionIdTerms() {
        return (getAssembler().getIdTerms(getRequisiteOptionIdColumn()));
    }


    /**
     *  Gets the RequisiteOptionId column name.
     *
     * @return the column name
     */

    protected String getRequisiteOptionIdColumn() {
        return ("requisite_option_id");
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getRequisiteOptionQuery() {
        throw new org.osid.UnimplementedException("supportsRequisiteOptionQuery() is false");
    }


    /**
     *  Matches requisites that have any requisite option. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          requisite option, <code> false </code> to match requisites 
     *          with no requisite options 
     */

    @OSID @Override
    public void matchAnyRequisiteOption(boolean match) {
        getAssembler().addIdWildcardTerm(getRequisiteOptionColumn(), match);
        return;
    }


    /**
     *  Clears the requisite option terms. 
     */

    @OSID @Override
    public void clearRequisiteOptionTerms() {
        getAssembler().clearTerms(getRequisiteOptionColumn());
        return;
    }


    /**
     *  Gets the requisite option query terms. 
     *
     *  @return the requisite query terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getRequisiteOptionTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the RequisiteOption column name.
     *
     * @return the column name
     */

    protected String getRequisiteOptionColumn() {
        return ("requisite_option");
    }


    /**
     *  Sets the course <code> Id </code> for this query to match requisites 
     *  that have a requirement for the given course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course requirement. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches requisites that have any course requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any course 
     *          requirement, <code> false </code> to match requisites with no 
     *          course requirements 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseColumn(), match);
        return;
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Sets the program <code> Id </code> for this query to match requisites 
     *  that have a requirement for the given program. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        getAssembler().addIdTerm(getProgramIdColumn(), programId, match);
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        getAssembler().clearTerms(getProgramIdColumn());
        return;
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (getAssembler().getIdTerms(getProgramIdColumn()));
    }


    /**
     *  Gets the ProgramId column name.
     *
     * @return the column name
     */

    protected String getProgramIdColumn() {
        return ("program_id");
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program requirement. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Matches requisites that have any program requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any program 
     *          requirement, <code> false </code> to match requisites with no 
     *          program requirements 
     */

    @OSID @Override
    public void matchAnyProgram(boolean match) {
        getAssembler().addIdWildcardTerm(getProgramColumn(), match);
        return;
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        getAssembler().clearTerms(getProgramColumn());
        return;
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Gets the Program column name.
     *
     * @return the column name
     */

    protected String getProgramColumn() {
        return ("program");
    }


    /**
     *  Sets the credential <code> Id </code> for this query to match 
     *  requisites that have a requirement for the given credential. 
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> credentialId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCredentialId(org.osid.id.Id credentialId, boolean match) {
        getAssembler().addIdTerm(getCredentialIdColumn(), credentialId, match);
        return;
    }


    /**
     *  Clears the credential <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCredentialIdTerms() {
        getAssembler().clearTerms(getCredentialIdColumn());
        return;
    }


    /**
     *  Gets the credential <code> Id </code> query terms. 
     *
     *  @return the credential <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCredentialIdTerms() {
        return (getAssembler().getIdTerms(getCredentialIdColumn()));
    }


    /**
     *  Gets the CredentialId column name.
     *
     * @return the column name
     */

    protected String getCredentialIdColumn() {
        return ("credential_id");
    }


    /**
     *  Tests if a <code> CredentialQuery </code> is available. 
     *
     *  @return <code> true </code> if a credential query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credential requirement. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return a credential query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuery getCredentialQuery() {
        throw new org.osid.UnimplementedException("supportsCredentialQuery() is false");
    }


    /**
     *  Matches requisites that have any credential requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          credential requirement, <code> false </code> to match 
     *          requisites with no credential requirements 
     */

    @OSID @Override
    public void matchAnyCredential(boolean match) {
        getAssembler().addIdWildcardTerm(getCredentialColumn(), match);
        return;
    }


    /**
     *  Clears the credential terms. 
     */

    @OSID @Override
    public void clearCredentialTerms() {
        getAssembler().clearTerms(getCredentialColumn());
        return;
    }


    /**
     *  Gets the credential query terms. 
     *
     *  @return the credential terms 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQueryInspector[] getCredentialTerms() {
        return (new org.osid.course.program.CredentialQueryInspector[0]);
    }


    /**
     *  Gets the Credential column name.
     *
     * @return the column name
     */

    protected String getCredentialColumn() {
        return ("credential");
    }


    /**
     *  Sets the learning objective <code> Id </code> for this query to match 
     *  requisites that have a requirement for the given learning objective. 
     *
     *  @param  objectiveId a learning objective <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getLearningObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the learning objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        getAssembler().clearTerms(getLearningObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the learning objective <code> Id </code> query terms. 
     *
     *  @return the learning objective <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getLearningObjectiveIdColumn()));
    }


    /**
     *  Gets the LearningObjectiveId column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveIdColumn() {
        return ("learning_objective_id");
    }


    /**
     *  Tests if a <code> LearningObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if a learning objective query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective requirement. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return a learning objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches requisites that have any learning objective requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          learning objective requirement, <code> false </code> to match 
     *          requisites with no learning objective requirements 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getLearningObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        getAssembler().clearTerms(getLearningObjectiveColumn());
        return;
    }


    /**
     *  Gets the learning objective query terms. 
     *
     *  @return the learning objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the LearningObjective column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveColumn() {
        return ("learning_objective");
    }


    /**
     *  Sets the assessment <code> Id </code> for this query to match 
     *  requisites that have a requirement for the given assessment. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a assessment requirement. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return an assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches requisites that have any assessment requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          assessment requirement, <code> false </code> to match 
     *          requisites with no assessment requirements 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentColumn(), match);
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match requisites 
     *  that have a requirement for the given award. 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        getAssembler().addIdTerm(getAwardIdColumn(), awardId, match);
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        getAssembler().clearTerms(getAwardIdColumn());
        return;
    }


    /**
     *  Gets the award <code> Id </code> query terms. 
     *
     *  @return the award <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (getAssembler().getIdTerms(getAwardIdColumn()));
    }


    /**
     *  Gets the AwardId column name.
     *
     * @return the column name
     */

    protected String getAwardIdColumn() {
        return ("award_id");
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for a award requirement. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Matches requisites that have any award requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any award 
     *          requirement, <code> false </code> to match requisites with no 
     *          award requirements 
     */

    @OSID @Override
    public void matchAnyAward(boolean match) {
        getAssembler().addIdWildcardTerm(getAwardColumn(), match);
        return;
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        getAssembler().clearTerms(getAwardColumn());
        return;
    }


    /**
     *  Gets the award query terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the Award column name.
     *
     * @return the column name
     */

    protected String getAwardColumn() {
        return ("award");
    }


    /**
     *  Sets the requisite <code> Id </code> for this query to match 
     *  requisites that have the specified requisite as an ancestor. 
     *
     *  @param  requisiteId a requisite <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingRequisiteId(org.osid.id.Id requisiteId, 
                                           boolean match) {
        getAssembler().addIdTerm(getContainingRequisiteIdColumn(), requisiteId, match);
        return;
    }


    /**
     *  Clears the containing requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingRequisiteIdTerms() {
        getAssembler().clearTerms(getContainingRequisiteIdColumn());
        return;
    }


    /**
     *  Gets the containing This method must be implemented. <code> Id </code> 
     *  terms. 
     *
     *  @return the This method must be implemented. <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContainingRequisiteIdTerms() {
        return (getAssembler().getIdTerms(getContainingRequisiteIdColumn()));
    }


    /**
     *  Gets the ContainingRequisiteId column name.
     *
     * @return the column name
     */

    protected String getContainingRequisiteIdColumn() {
        return ("containing_requisite_id");
    }


    /**
     *  Tests if a containing requisite query is available. 
     *
     *  @return <code> true </code> if a containing requisite query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingRequisiteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a containing requisite. 
     *
     *  @return the containing requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingRequisiteQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getContainingRequisiteQuery() {
        throw new org.osid.UnimplementedException("supportsContainingRequisiteQuery() is false");
    }


    /**
     *  Matches requisites with any ancestor requisite. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          ancestor requisite, <code> false </code> to match requisites 
     *          with no ancestor requisites 
     */

    @OSID @Override
    public void matchAnyContainingRequisite(boolean match) {
        getAssembler().addIdWildcardTerm(getContainingRequisiteColumn(), match);
        return;
    }


    /**
     *  Clears the containing requisite terms. 
     */

    @OSID @Override
    public void clearContainingRequisiteTerms() {
        getAssembler().clearTerms(getContainingRequisiteColumn());
        return;
    }


    /**
     *  Gets the containing This method must be implemented. terms. 
     *
     *  @return the requisite terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getContainingRequisiteTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the ContainingRequisite column name.
     *
     * @return the column name
     */

    protected String getContainingRequisiteColumn() {
        return ("containing_requisite");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  requisites assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this requisite supports the given record
     *  <code>Type</code>.
     *
     *  @param  requisiteRecordType a requisite record type 
     *  @return <code>true</code> if the requisiteRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requisiteRecordType) {
        for (org.osid.course.requisite.records.RequisiteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  requisiteRecordType the requisite record type 
     *  @return the requisite query record 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requisiteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteQueryRecord getRequisiteQueryRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.RequisiteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  requisiteRecordType the requisite record type 
     *  @return the requisite query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requisiteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteQueryInspectorRecord getRequisiteQueryInspectorRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.RequisiteQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param requisiteRecordType the requisite record type
     *  @return the requisite search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requisiteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteSearchOrderRecord getRequisiteSearchOrderRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.RequisiteSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this requisite. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requisiteQueryRecord the requisite query record
     *  @param requisiteQueryInspectorRecord the requisite query inspector
     *         record
     *  @param requisiteSearchOrderRecord the requisite search order record
     *  @param requisiteRecordType requisite record type
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteQueryRecord</code>,
     *          <code>requisiteQueryInspectorRecord</code>,
     *          <code>requisiteSearchOrderRecord</code> or
     *          <code>requisiteRecordTyperequisite</code> is
     *          <code>null</code>
     */
            
    protected void addRequisiteRecords(org.osid.course.requisite.records.RequisiteQueryRecord requisiteQueryRecord, 
                                      org.osid.course.requisite.records.RequisiteQueryInspectorRecord requisiteQueryInspectorRecord, 
                                      org.osid.course.requisite.records.RequisiteSearchOrderRecord requisiteSearchOrderRecord, 
                                      org.osid.type.Type requisiteRecordType) {

        addRecordType(requisiteRecordType);

        nullarg(requisiteQueryRecord, "requisite query record");
        nullarg(requisiteQueryInspectorRecord, "requisite query inspector record");
        nullarg(requisiteSearchOrderRecord, "requisite search odrer record");

        this.queryRecords.add(requisiteQueryRecord);
        this.queryInspectorRecords.add(requisiteQueryInspectorRecord);
        this.searchOrderRecords.add(requisiteSearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidContainableQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidContainableQuery
        implements org.osid.OsidContainableQuery,
                   org.osid.OsidContainableQueryInspector,
                   org.osid.OsidContainableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidContainableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidContainableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the sequestered field.
         *
         *  @return the column name
         */
        
        protected String getSequesteredColumn() {
            return (super.getSequesteredColumn());
        }
    }    
}
