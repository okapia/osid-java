//
// AbstractAssemblyResultQuery.java
//
//     A ResultQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.result.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ResultQuery that stores terms.
 */

public abstract class AbstractAssemblyResultQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.offering.ResultQuery,
               org.osid.offering.ResultQueryInspector,
               org.osid.offering.ResultSearchOrder {

    private final java.util.Collection<org.osid.offering.records.ResultQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.ResultQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.ResultSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyResultQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyResultQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a participant <code> Id. </code> 
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> participantId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParticipantId(org.osid.id.Id participantId, boolean match) {
        getAssembler().addIdTerm(getParticipantIdColumn(), participantId, match);
        return;
    }


    /**
     *  Clears all participant <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParticipantIdTerms() {
        getAssembler().clearTerms(getParticipantIdColumn());
        return;
    }


    /**
     *  Gets the participant <code> Id </code> query terms. 
     *
     *  @return the participant <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParticipantIdTerms() {
        return (getAssembler().getIdTerms(getParticipantIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the participant. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByParticipant(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getParticipantColumn(), style);
        return;
    }


    /**
     *  Gets the ParticipantId column name.
     *
     * @return the column name
     */

    protected String getParticipantIdColumn() {
        return ("participant_id");
    }


    /**
     *  Tests if a <code> ParticipantUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a participant query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantQuery() {
        return (false);
    }


    /**
     *  Gets the query for a participant query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the participant query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuery getParticipantQuery() {
        throw new org.osid.UnimplementedException("supportsParticipantQuery() is false");
    }


    /**
     *  Clears all participant terms. 
     */

    @OSID @Override
    public void clearParticipantTerms() {
        getAssembler().clearTerms(getParticipantColumn());
        return;
    }


    /**
     *  Gets the participant query terms. 
     *
     *  @return the participant terms 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQueryInspector[] getParticipantTerms() {
        return (new org.osid.offering.ParticipantQueryInspector[0]);
    }


    /**
     *  Tests if a participant search order is available. 
     *
     *  @return <code> true </code> if a participant search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantSearchOrder() {
        return (false);
    }


    /**
     *  Gets the participant search order. 
     *
     *  @return the participant search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchOrder getParticipantSearchOrder() {
        throw new org.osid.UnimplementedException("supportsParticipantSearchOrder() is false");
    }


    /**
     *  Gets the Participant column name.
     *
     * @return the column name
     */

    protected String getParticipantColumn() {
        return ("participant");
    }


    /**
     *  Sets a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getGradeIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears all grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        getAssembler().clearTerms(getGradeIdColumn());
        return;
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (getAssembler().getIdTerms(getGradeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the commitment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeColumn(), style);
        return;
    }


    /**
     *  Gets the GradeId column name.
     *
     * @return the column name
     */

    protected String getGradeIdColumn() {
        return ("grade_id");
    }


    /**
     *  Tests if a <code> GradetQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches results with any grade. 
     *
     *  @param  match <code> true </code> to match results with any grade, 
     *          <code> false </code> to match results with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeColumn(), match);
        return;
    }


    /**
     *  Clears all grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        getAssembler().clearTerms(getGradeColumn());
        return;
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade search order. 
     *
     *  @return the grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Gets the Grade column name.
     *
     * @return the column name
     */

    protected String getGradeColumn() {
        return ("grade");
    }


    /**
     *  Matches a value between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchValue(java.math.BigDecimal from, java.math.BigDecimal to, 
                           boolean match) {
        getAssembler().addDecimalRangeTerm(getValueColumn(), from, to, match);
        return;
    }


    /**
     *  Matches results with any value. 
     *
     *  @param  match <code> true </code> to match results with any value, 
     *          <code> false </code> to match results with no value 
     */

    @OSID @Override
    public void matchAnyValue(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getValueColumn(), match);
        return;
    }


    /**
     *  Clears all value terms. 
     */

    @OSID @Override
    public void clearValueTerms() {
        getAssembler().clearTerms(getValueColumn());
        return;
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the value terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getValueTerms() {
        return (getAssembler().getDecimalRangeTerms(getValueColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the value. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getValueColumn(), style);
        return;
    }


    /**
     *  Gets the Value column name.
     *
     * @return the column name
     */

    protected String getValueColumn() {
        return ("value");
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match results 
     *  assigned to catalogues. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        getAssembler().addIdTerm(getCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears all catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        getAssembler().clearTerms(getCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getCatalogueIdColumn()));
    }


    /**
     *  Gets the CatalogueId column name.
     *
     * @return the column name
     */

    protected String getCatalogueIdColumn() {
        return ("catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears all catalogue terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        getAssembler().clearTerms(getCatalogueColumn());
        return;
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the Catalogue column name.
     *
     * @return the column name
     */

    protected String getCatalogueColumn() {
        return ("catalogue");
    }


    /**
     *  Tests if this result supports the given record
     *  <code>Type</code>.
     *
     *  @param  resultRecordType a result record type 
     *  @return <code>true</code> if the resultRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resultRecordType) {
        for (org.osid.offering.records.ResultQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(resultRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  resultRecordType the result record type 
     *  @return the result query record 
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ResultQueryRecord getResultQueryRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ResultQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(resultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resultRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  resultRecordType the result record type 
     *  @return the result query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ResultQueryInspectorRecord getResultQueryInspectorRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ResultQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(resultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resultRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param resultRecordType the result record type
     *  @return the result search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ResultSearchOrderRecord getResultSearchOrderRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ResultSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(resultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resultRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this result. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resultQueryRecord the result query record
     *  @param resultQueryInspectorRecord the result query inspector
     *         record
     *  @param resultSearchOrderRecord the result search order record
     *  @param resultRecordType result record type
     *  @throws org.osid.NullArgumentException
     *          <code>resultQueryRecord</code>,
     *          <code>resultQueryInspectorRecord</code>,
     *          <code>resultSearchOrderRecord</code> or
     *          <code>resultRecordTyperesult</code> is
     *          <code>null</code>
     */
            
    protected void addResultRecords(org.osid.offering.records.ResultQueryRecord resultQueryRecord, 
                                      org.osid.offering.records.ResultQueryInspectorRecord resultQueryInspectorRecord, 
                                      org.osid.offering.records.ResultSearchOrderRecord resultSearchOrderRecord, 
                                      org.osid.type.Type resultRecordType) {

        addRecordType(resultRecordType);

        nullarg(resultQueryRecord, "result query record");
        nullarg(resultQueryInspectorRecord, "result query inspector record");
        nullarg(resultSearchOrderRecord, "result search odrer record");

        this.queryRecords.add(resultQueryRecord);
        this.queryInspectorRecords.add(resultQueryInspectorRecord);
        this.searchOrderRecords.add(resultSearchOrderRecord);
        
        return;
    }
}
