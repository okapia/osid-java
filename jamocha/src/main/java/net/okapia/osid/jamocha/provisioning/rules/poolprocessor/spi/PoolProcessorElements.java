//
// PoolProcessorElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessor.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PoolProcessorElements
    extends net.okapia.osid.jamocha.spi.OsidProcessorElements {


    /**
     *  Gets the PoolProcessorElement Id.
     *
     *  @return the pool processor element Id
     */

    public static org.osid.id.Id getPoolProcessorEntityId() {
        return (makeEntityId("osid.provisioning.rules.PoolProcessor"));
    }


    /**
     *  Gets the AllocatesByLeastUse element Id.
     *
     *  @return the AllocatesByLeastUse element Id
     */

    public static org.osid.id.Id getAllocatesByLeastUse() {
        return (makeElementId("osid.provisioning.rules.poolprocessor.AllocatesByLeastUse"));
    }


    /**
     *  Gets the AllocatesByMostUse element Id.
     *
     *  @return the AllocatesByMostUse element Id
     */

    public static org.osid.id.Id getAllocatesByMostUse() {
        return (makeElementId("osid.provisioning.rules.poolprocessor.AllocatesByMostUse"));
    }


    /**
     *  Gets the AllocatesByLeastCost element Id.
     *
     *  @return the AllocatesByLeastCost element Id
     */

    public static org.osid.id.Id getAllocatesByLeastCost() {
        return (makeQueryElementId("osid.provisioning.rules.poolprocessor.AllocatesByLeastCost"));
    }


    /**
     *  Gets the AllocatesByMostCost element Id.
     *
     *  @return the AllocatesByMostCost element Id
     */

    public static org.osid.id.Id getAllocatesByMostCost() {
        return (makeElementId("osid.provisioning.rules.poolprocessor.AllocatesByMostCost"));
    }


    /**
     *  Gets the RuledPoolId element Id.
     *
     *  @return the RuledPoolId element Id
     */

    public static org.osid.id.Id getRuledPoolId() {
        return (makeQueryElementId("osid.provisioning.rules.poolprocessor.RuledPoolId"));
    }


    /**
     *  Gets the RuledPool element Id.
     *
     *  @return the RuledPool element Id
     */

    public static org.osid.id.Id getRuledPool() {
        return (makeQueryElementId("osid.provisioning.rules.poolprocessor.RuledPool"));
    }


    /**
     *  Gets the DistributorId element Id.
     *
     *  @return the DistributorId element Id
     */

    public static org.osid.id.Id getDistributorId() {
        return (makeQueryElementId("osid.provisioning.rules.poolprocessor.DistributorId"));
    }


    /**
     *  Gets the Distributor element Id.
     *
     *  @return the Distributor element Id
     */

    public static org.osid.id.Id getDistributor() {
        return (makeQueryElementId("osid.provisioning.rules.poolprocessor.Distributor"));
    }
}
