//
// AbstractAuthenticationBatchManager.java
//
//     An adapter for a AuthenticationBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AuthenticationBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAuthenticationBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.authentication.batch.AuthenticationBatchManager>
    implements org.osid.authentication.batch.AuthenticationBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAuthenticationBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAuthenticationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of agents is available. 
     *
     *  @return <code> true </code> if an agent bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentBatchAdmin() {
        return (getAdapteeManager().supportsAgentBatchAdmin());
    }


    /**
     *  Tests if bulk administration of agencies is available. 
     *
     *  @return <code> true </code> if an agency bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyBatchAdmin() {
        return (getAdapteeManager().supportsAgencyBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service. 
     *
     *  @return an <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgentBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agent 
     *  administration service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the <code> Agency </code> 
     *  @return an <code> AgentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Agency </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgentBatchAdminSession getAgentBatchAdminSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgentBatchAdminSessionForAgency(agencyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk agency 
     *  administration service. 
     *
     *  @return an <code> AgencyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AgencyBatchAdminSession getAgencyBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgencyBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
