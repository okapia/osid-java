//
// AbstractBrokerConstrainerEnablerQuery.java
//
//     A template for making a BrokerConstrainerEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for broker constrainer enablers.
 */

public abstract class AbstractBrokerConstrainerEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the broker constrainer. 
     *
     *  @param  brokerConstrainerId the broker constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerConstrainerId(org.osid.id.Id brokerConstrainerId, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the broker constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerConstrainerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker constrainer query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the broker constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerQuery getRuledBrokerConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any broker constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any broker 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no broker constrainers 
     */

    @OSID @Override
    public void matchAnyRuledBrokerConstrainer(boolean match) {
        return;
    }


    /**
     *  Clears the broker constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerConstrainerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given broker constrainer enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker constrainer enabler implementing the requested record.
     *
     *  @param brokerConstrainerEnablerRecordType a broker constrainer enabler record type
     *  @return the broker constrainer enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord getBrokerConstrainerEnablerQueryRecord(org.osid.type.Type brokerConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(brokerConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker constrainer enabler query. 
     *
     *  @param brokerConstrainerEnablerQueryRecord broker constrainer enabler query record
     *  @param brokerConstrainerEnablerRecordType brokerConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerConstrainerEnablerQueryRecord(org.osid.provisioning.rules.records.BrokerConstrainerEnablerQueryRecord brokerConstrainerEnablerQueryRecord, 
                                          org.osid.type.Type brokerConstrainerEnablerRecordType) {

        addRecordType(brokerConstrainerEnablerRecordType);
        nullarg(brokerConstrainerEnablerQueryRecord, "broker constrainer enabler query record");
        this.records.add(brokerConstrainerEnablerQueryRecord);        
        return;
    }
}
