//
// InvariantMapPoolProcessorEnablerLookupSession
//
//    Implements a PoolProcessorEnabler lookup service backed by a fixed collection of
//    poolProcessorEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolProcessorEnabler lookup service backed by a fixed
 *  collection of pool processor enablers. The pool processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPoolProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.PoolProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorEnablerLookupSession</code> with no
     *  pool processor enablers.
     *  
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumnetException {@code distributor} is
     *          {@code null}
     */

    public InvariantMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorEnablerLookupSession</code> with a single
     *  pool processor enabler.
     *  
     *  @param distributor the distributor
     *  @param poolProcessorEnabler a single pool processor enabler
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessorEnabler} is <code>null</code>
     */

      public InvariantMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler) {
        this(distributor);
        putPoolProcessorEnabler(poolProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorEnablerLookupSession</code> using an array
     *  of pool processor enablers.
     *  
     *  @param distributor the distributor
     *  @param poolProcessorEnablers an array of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessorEnablers} is <code>null</code>
     */

      public InvariantMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolProcessorEnabler[] poolProcessorEnablers) {
        this(distributor);
        putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorEnablerLookupSession</code> using a
     *  collection of pool processor enablers.
     *
     *  @param distributor the distributor
     *  @param poolProcessorEnablers a collection of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessorEnablers} is <code>null</code>
     */

      public InvariantMapPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                               java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablers) {
        this(distributor);
        putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }
}
