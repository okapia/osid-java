//
// AbstractPoolProcessorQueryInspector.java
//
//     A template for making a PoolProcessorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for pool processors.
 */

public abstract class AbstractPoolProcessorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQueryInspector
    implements org.osid.provisioning.rules.PoolProcessorQueryInspector {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the allocates by least use query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByLeastUseTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the allocates by most use query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByMostUseTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the allocates by least cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByLeastCostTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the allocates by most cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAllocatesByMostCostTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledPoolIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getRuledPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given pool processor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a pool processor implementing the requested record.
     *
     *  @param poolProcessorRecordType a pool processor record type
     *  @return the pool processor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord getPoolProcessorQueryInspectorRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool processor query. 
     *
     *  @param poolProcessorQueryInspectorRecord pool processor query inspector
     *         record
     *  @param poolProcessorRecordType poolProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolProcessorQueryInspectorRecord(org.osid.provisioning.rules.records.PoolProcessorQueryInspectorRecord poolProcessorQueryInspectorRecord, 
                                                   org.osid.type.Type poolProcessorRecordType) {

        addRecordType(poolProcessorRecordType);
        nullarg(poolProcessorRecordType, "pool processor record type");
        this.records.add(poolProcessorQueryInspectorRecord);        
        return;
    }
}
