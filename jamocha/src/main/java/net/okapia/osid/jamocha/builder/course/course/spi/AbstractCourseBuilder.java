//
// AbstractCourse.java
//
//     Defines a Course builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.course.spi;


/**
 *  Defines a <code>Course</code> builder.
 */

public abstract class AbstractCourseBuilder<T extends AbstractCourseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.course.CourseMiter course;


    /**
     *  Constructs a new <code>AbstractCourseBuilder</code>.
     *
     *  @param course the course to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCourseBuilder(net.okapia.osid.jamocha.builder.course.course.CourseMiter course) {
        super(course);
        this.course = course;
        return;
    }


    /**
     *  Builds the course.
     *
     *  @return the new course
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.Course build() {
        (new net.okapia.osid.jamocha.builder.validator.course.course.CourseValidator(getValidations())).validate(this.course);
        return (new net.okapia.osid.jamocha.builder.course.course.ImmutableCourse(this.course));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the course miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.course.CourseMiter getMiter() {
        return (this.course);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T number(String number) {
        getMiter().setNumber(number);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T creditAmount(org.osid.grading.Grade amount) {
        getMiter().addCreditAmount(amount);
        return (self());
    }


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    public T creditAmounts(java.util.Collection<org.osid.grading.Grade> amounts) {
        getMiter().setCreditAmounts(amounts);
        return (self());
    }


    /**
     *  Sets the prerequisites info.
     *
     *  @param info a prerequisites info
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public T prerequisitesInfo(org.osid.locale.DisplayText info) {
        getMiter().setPrerequisitesInfo(info);
        return (self());
    }


    /**
     *  Adds a prerequisite.
     *
     *  @param prerequisite a prerequisite
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisite</code> is <code>null</code>
     */

    public T prerequisite(org.osid.course.requisite.Requisite prerequisite) {
        getMiter().addPrerequisite(prerequisite);
        return (self());
    }


    /**
     *  Sets all the prerequisites.
     *
     *  @param prerequisites a collection of prerequisites
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisites</code> is <code>null</code>
     */

    public T prerequisites(java.util.Collection<org.osid.course.requisite.Requisite> prerequisites) {
        getMiter().setPrerequisites(prerequisites);
        return (self());
    }


    /**
     *  Adds a level.
     *
     *  @param level a level
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public T level(org.osid.grading.Grade level) {
        getMiter().addLevel(level);
        return (self());
    }


    /**
     *  Sets all the levels.
     *
     *  @param levels a collection of levels
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>levels</code> is
     *          <code>null</code>
     */

    public T levels(java.util.Collection<org.osid.grading.Grade> levels) {
        getMiter().setLevels(levels);
        return (self());
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public T gradingOption(org.osid.grading.GradeSystem option) {
        getMiter().addGradingOption(option);
        return (self());
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public T gradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        getMiter().setGradingOptions(options);
        return (self());
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public T learningObjective(org.osid.learning.Objective objective) {
        getMiter().addLearningObjective(objective);
        return (self());
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    public T learningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        getMiter().setLearningObjectives(objectives);
        return (self());
    }


    /**
     *  Adds a Course record.
     *
     *  @param record a course record
     *  @param recordType the type of course record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.records.CourseRecord record, org.osid.type.Type recordType) {
        getMiter().addCourseRecord(record, recordType);
        return (self());
    }
}       


