//
// AbstractResponseSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.response.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractResponseSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inquiry.ResponseSearchResults {

    private org.osid.inquiry.ResponseList responses;
    private final org.osid.inquiry.ResponseQueryInspector inspector;
    private final java.util.Collection<org.osid.inquiry.records.ResponseSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractResponseSearchResults.
     *
     *  @param responses the result set
     *  @param responseQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>responses</code>
     *          or <code>responseQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractResponseSearchResults(org.osid.inquiry.ResponseList responses,
                                            org.osid.inquiry.ResponseQueryInspector responseQueryInspector) {
        nullarg(responses, "responses");
        nullarg(responseQueryInspector, "response query inspectpr");

        this.responses = responses;
        this.inspector = responseQueryInspector;

        return;
    }


    /**
     *  Gets the response list resulting from a search.
     *
     *  @return a response list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponses() {
        if (this.responses == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inquiry.ResponseList responses = this.responses;
        this.responses = null;
	return (responses);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inquiry.ResponseQueryInspector getResponseQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  response search record <code> Type. </code> This method must
     *  be used to retrieve a response implementing the requested
     *  record.
     *
     *  @param responseSearchRecordType a response search 
     *         record type 
     *  @return the response search
     *  @throws org.osid.NullArgumentException
     *          <code>responseSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(responseSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseSearchResultsRecord getResponseSearchResultsRecord(org.osid.type.Type responseSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inquiry.records.ResponseSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(responseSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(responseSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record response search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addResponseRecord(org.osid.inquiry.records.ResponseSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "response record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
