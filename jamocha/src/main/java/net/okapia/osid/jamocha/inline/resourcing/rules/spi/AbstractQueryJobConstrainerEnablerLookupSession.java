//
// AbstractQueryJobConstrainerEnablerLookupSession.java
//
//    An inline adapter that maps a JobConstrainerEnablerLookupSession to
//    a JobConstrainerEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a JobConstrainerEnablerLookupSession to
 *  a JobConstrainerEnablerQuerySession.
 */

public abstract class AbstractQueryJobConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.resourcing.rules.spi.AbstractJobConstrainerEnablerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.resourcing.rules.JobConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryJobConstrainerEnablerLookupSession.
     *
     *  @param querySession the underlying job constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryJobConstrainerEnablerLookupSession(org.osid.resourcing.rules.JobConstrainerEnablerQuerySession querySession) {
        nullarg(querySession, "job constrainer enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>JobConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJobConstrainerEnablers() {
        return (this.session.canSearchJobConstrainerEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job constrainer enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active job constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive job constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>JobConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JobConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>JobConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param  jobConstrainerEnablerId <code>Id</code> of the
     *          <code>JobConstrainerEnabler</code>
     *  @return the job constrainer enabler
     *  @throws org.osid.NotFoundException <code>jobConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnabler getJobConstrainerEnabler(org.osid.id.Id jobConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = getQuery();
        query.matchId(jobConstrainerEnablerId, true);
        org.osid.resourcing.rules.JobConstrainerEnablerList jobConstrainerEnablers = this.session.getJobConstrainerEnablersByQuery(query);
        if (jobConstrainerEnablers.hasNext()) {
            return (jobConstrainerEnablers.getNextJobConstrainerEnabler());
        } 
        
        throw new org.osid.NotFoundException(jobConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>JobConstrainerEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  jobConstrainerEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>JobConstrainerEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, job constrainer enablers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainer enablers are returned.
     *
     *  @param  jobConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JobConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByIds(org.osid.id.IdList jobConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = jobConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getJobConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>JobConstrainerEnablerList</code> corresponding to
     *  the given job constrainer enabler genus <code>Type</code>
     *  which does not include job constrainer enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known job
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those job constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, job constrainer enablers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainer enablers are returned.
     *
     *  @param  jobConstrainerEnablerGenusType a jobConstrainerEnabler genus type 
     *  @return the returned <code>JobConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByGenusType(org.osid.type.Type jobConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = getQuery();
        query.matchGenusType(jobConstrainerEnablerGenusType, true);
        return (this.session.getJobConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>JobConstrainerEnablerList</code> corresponding to
     *  the given job constrainer enabler genus <code>Type</code> and
     *  include any additional job constrainer enablers with genus
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known job
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those job constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, job constrainer enablers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainer enablers are returned.
     *
     *  @param  jobConstrainerEnablerGenusType a jobConstrainerEnabler genus type 
     *  @return the returned <code>JobConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByParentGenusType(org.osid.type.Type jobConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = getQuery();
        query.matchParentGenusType(jobConstrainerEnablerGenusType, true);
        return (this.session.getJobConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>JobConstrainerEnablerList</code> containing the
     *  given job constrainer enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known job
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those job constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, job constrainer enablers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainer enablers are returned.
     *
     *  @param  jobConstrainerEnablerRecordType a jobConstrainerEnabler record type 
     *  @return the returned <code>JobConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByRecordType(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = getQuery();
        query.matchRecordType(jobConstrainerEnablerRecordType, true);
        return (this.session.getJobConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>JobConstrainerEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known job
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those job constrainer enablers
     *  that are accessible through this session.
     *  
     *  In active mode, job constrainer enablers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainer enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>JobConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getJobConstrainerEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>JobConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known job
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those job constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, job constrainer enablers are returned that are
     *  currently active. In any status mode, active and inactive job
     *  constrainer enablers are returned.
     *
     *  @return a list of <code>JobConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getJobConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.rules.JobConstrainerEnablerQuery getQuery() {
        org.osid.resourcing.rules.JobConstrainerEnablerQuery query = this.session.getJobConstrainerEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
