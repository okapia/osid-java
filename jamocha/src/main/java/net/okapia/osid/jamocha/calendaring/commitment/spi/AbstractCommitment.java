//
// AbstractCommitment.java
//
//     Defines a Commitment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.commitment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Commitment</code>.
 */

public abstract class AbstractCommitment
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.calendaring.Commitment {

    private org.osid.calendaring.Event event;
    private org.osid.resource.Resource resource;

    private final java.util.Collection<org.osid.calendaring.records.CommitmentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the event <code> Id. </code> 
     *
     *  @return the event <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEventId() {
        return (this.event.getId());
    }


    /**
     *  Gets the <code> Event. </code> 
     *
     *  @return the event 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent()
        throws org.osid.OperationFailedException {

        return (this.event);
    }


    /**
     *  Sets the event.
     *
     *  @param event an event
     *  @throws org.osid.NullArgumentException
     *          <code>event</code> is <code>null</code>
     */

    protected void setEvent(org.osid.calendaring.Event event) {
        nullarg(event, "event");
        this.event = event;
        return;
    }


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this commitment supports the given record
     *  <code>Type</code>.
     *
     *  @param  commitmentRecordType a commitment record type 
     *  @return <code>true</code> if the commitmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commitmentRecordType) {
        for (org.osid.calendaring.records.CommitmentRecord record : this.records) {
            if (record.implementsRecordType(commitmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Commitment</code> record <code>Type</code>.
     *
     *  @param  commitmentRecordType the commitment record type 
     *  @return the commitment record 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CommitmentRecord getCommitmentRecord(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CommitmentRecord record : this.records) {
            if (record.implementsRecordType(commitmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commitment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commitmentRecord the commitment record
     *  @param commitmentRecordType commitment record type
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecord</code> or
     *          <code>commitmentRecordType</code> is <code>null</code>
     */
            
    protected void addCommitmentRecord(org.osid.calendaring.records.CommitmentRecord commitmentRecord, 
                                       org.osid.type.Type commitmentRecordType) {
        
        nullarg(commitmentRecord, "commitment record");
        addRecordType(commitmentRecordType);
        this.records.add(commitmentRecord);
        
        return;
    }
}
