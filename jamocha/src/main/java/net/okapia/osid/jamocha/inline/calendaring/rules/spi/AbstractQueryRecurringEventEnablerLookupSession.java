//
// AbstractQueryRecurringEventEnablerLookupSession.java
//
//    An inline adapter that maps a RecurringEventEnablerLookupSession to
//    a RecurringEventEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RecurringEventEnablerLookupSession to
 *  a RecurringEventEnablerQuerySession.
 */

public abstract class AbstractQueryRecurringEventEnablerLookupSession
    extends net.okapia.osid.jamocha.calendaring.rules.spi.AbstractRecurringEventEnablerLookupSession
    implements org.osid.calendaring.rules.RecurringEventEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;    
    private final org.osid.calendaring.rules.RecurringEventEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRecurringEventEnablerLookupSession.
     *
     *  @param querySession the underlying recurring event enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRecurringEventEnablerLookupSession(org.osid.calendaring.rules.RecurringEventEnablerQuerySession querySession) {
        nullarg(querySession, "recurring event enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>RecurringEventEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRecurringEventEnablers() {
        return (this.session.canSearchRecurringEventEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recurring event enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active recurring event enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRecurringEventEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive recurring event enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRecurringEventEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RecurringEventEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RecurringEventEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RecurringEventEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerId <code>Id</code> of the
     *          <code>RecurringEventEnabler</code>
     *  @return the recurring event enabler
     *  @throws org.osid.NotFoundException <code>recurringEventEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnabler getRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.RecurringEventEnablerQuery query = getQuery();
        query.matchId(recurringEventEnablerId, true);
        org.osid.calendaring.rules.RecurringEventEnablerList recurringEventEnablers = this.session.getRecurringEventEnablersByQuery(query);
        if (recurringEventEnablers.hasNext()) {
            return (recurringEventEnablers.getNextRecurringEventEnabler());
        } 
        
        throw new org.osid.NotFoundException(recurringEventEnablerId + " not found");
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recurringEventEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RecurringEventEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByIds(org.osid.id.IdList recurringEventEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.RecurringEventEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = recurringEventEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRecurringEventEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> corresponding to the given
     *  recurring event enabler genus <code>Type</code> which does not include
     *  recurring event enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerGenusType a recurringEventEnabler genus type 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByGenusType(org.osid.type.Type recurringEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.RecurringEventEnablerQuery query = getQuery();
        query.matchGenusType(recurringEventEnablerGenusType, true);
        return (this.session.getRecurringEventEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> corresponding to the given
     *  recurring event enabler genus <code>Type</code> and include any additional
     *  recurring event enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerGenusType a recurringEventEnabler genus type 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByParentGenusType(org.osid.type.Type recurringEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.RecurringEventEnablerQuery query = getQuery();
        query.matchParentGenusType(recurringEventEnablerGenusType, true);
        return (this.session.getRecurringEventEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> containing the given
     *  recurring event enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerRecordType a recurringEventEnabler record type 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByRecordType(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.RecurringEventEnablerQuery query = getQuery();
        query.matchRecordType(recurringEventEnablerRecordType, true);
        return (this.session.getRecurringEventEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible
     *  through this session.
     *  
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RecurringEventEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.RecurringEventEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRecurringEventEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>RecurringEventEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @return a list of <code>RecurringEventEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.RecurringEventEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRecurringEventEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.rules.RecurringEventEnablerQuery getQuery() {
        org.osid.calendaring.rules.RecurringEventEnablerQuery query = this.session.getRecurringEventEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
