//
// AbstractCommission.java
//
//     Defines a Commission.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.commission.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.percentarg;


/**
 *  Defines a <code>Commission</code>.
 */

public abstract class AbstractCommission
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.resourcing.Commission {

    private org.osid.resource.Resource resource;
    private org.osid.resourcing.Work work;
    private org.osid.resourcing.Competency competency;
    private long percentage;

    private final java.util.Collection<org.osid.resourcing.records.CommissionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the commissioned resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the commissioned resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the work. 
     *
     *  @return the work <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkId() {
        return (this.work.getId());
    }


    /**
     *  Gets the work. 
     *
     *  @return the work 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Work getWork()
        throws org.osid.OperationFailedException {

        return (this.work);
    }


    /**
     *  Sets the work.
     *
     *  @param work a work
     *  @throws org.osid.NullArgumentException
     *          <code>work</code> is <code>null</code>
     */

    protected void setWork(org.osid.resourcing.Work work) {
        nullarg(work, "work");
        this.work = work;
        return;
    }


    /**
     *  Tests if a competency is specified for this commission. 
     *
     *  @return <code> true </code> if a competency is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isCompetent() {
        return (this.competency != null);
    }


    /**
     *  Gets the competency <code> Id. </code> 
     *
     *  @return the competency <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCompetencyId() {
        if (!isCompetent()) {
            throw new org.osid.IllegalStateException("isCompetent() is false");
        }

        return (this.competency.getId());
    }


    /**
     *  Gets the competency. 
     *
     *  @return the competency 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency()
        throws org.osid.OperationFailedException {

        if (!isCompetent()) {
            throw new org.osid.IllegalStateException("isCompetent() is false");
        }

        return (this.competency);
    }


    /**
     *  Sets the competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException
     *          <code>competency</code> is <code>null</code>
     */

    protected void setCompetency(org.osid.resourcing.Competency competency) {
        nullarg(competency, "competency");
        this.competency = competency;
        return;
    }


    /**
     *  Gets the percentage commitment. 
     *
     *  @return the percentage commitment 
     */

    @OSID @Override
    public long getPercentage() {
        return (this.percentage);
    }


    /**
     *  Sets the percentage commitment.
     *
     *  @param percentage a percentage (0-100)
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    protected void setPercentage(long percentage) {
        percentarg(percentage, "percentage");
        this.percentage = percentage;
        return;
    }


    /**
     *  Tests if this commission supports the given record
     *  <code>Type</code>.
     *
     *  @param  commissionRecordType a commission record type 
     *  @return <code>true</code> if the commissionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commissionRecordType) {
        for (org.osid.resourcing.records.CommissionRecord record : this.records) {
            if (record.implementsRecordType(commissionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Commission</code> record <code>Type</code>.
     *
     *  @param  commissionRecordType the commission record type 
     *  @return the commission record 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CommissionRecord getCommissionRecord(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CommissionRecord record : this.records) {
            if (record.implementsRecordType(commissionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commission. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commissionRecord the commission record
     *  @param commissionRecordType commission record type
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecord</code> or
     *          <code>commissionRecordTypecommission</code> is
     *          <code>null</code>
     */
            
    protected void addCommissionRecord(org.osid.resourcing.records.CommissionRecord commissionRecord, 
                                       org.osid.type.Type commissionRecordType) {

        nullarg(commissionRecord, "commission record");
        addRecordType(commissionRecordType);
        this.records.add(commissionRecord);
        
        return;
    }
}
