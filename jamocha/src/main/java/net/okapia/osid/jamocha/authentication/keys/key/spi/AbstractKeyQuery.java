//
// AbstractKeyQuery.java
//
//     A template for making a Key Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for keys.
 */

public abstract class AbstractKeyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.authentication.keys.KeyQuery {

    private final java.util.Collection<org.osid.authentication.keys.records.KeyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Includes an agent query for making relations with <code> Agents. 
     *  </code> Multiple retrievals return separate query terms nested inside 
     *  this query term, each which are treated as a boolean <code> OR. 
     *  </code> 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given key query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a key implementing the requested record.
     *
     *  @param keyRecordType a key record type
     *  @return the key query record
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(keyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeyQueryRecord getKeyQueryRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.keys.records.KeyQueryRecord record : this.records) {
            if (record.implementsRecordType(keyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this key query. 
     *
     *  @param keyQueryRecord key query record
     *  @param keyRecordType key record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addKeyQueryRecord(org.osid.authentication.keys.records.KeyQueryRecord keyQueryRecord, 
                                          org.osid.type.Type keyRecordType) {

        addRecordType(keyRecordType);
        nullarg(keyQueryRecord, "key query record");
        this.records.add(keyQueryRecord);        
        return;
    }
}
