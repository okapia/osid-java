//
// AbstractDeviceSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.device.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDeviceSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.DeviceSearchResults {

    private org.osid.control.DeviceList devices;
    private final org.osid.control.DeviceQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.DeviceSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDeviceSearchResults.
     *
     *  @param devices the result set
     *  @param deviceQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>devices</code>
     *          or <code>deviceQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDeviceSearchResults(org.osid.control.DeviceList devices,
                                            org.osid.control.DeviceQueryInspector deviceQueryInspector) {
        nullarg(devices, "devices");
        nullarg(deviceQueryInspector, "device query inspectpr");

        this.devices = devices;
        this.inspector = deviceQueryInspector;

        return;
    }


    /**
     *  Gets the device list resulting from a search.
     *
     *  @return a device list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevices() {
        if (this.devices == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.DeviceList devices = this.devices;
        this.devices = null;
	return (devices);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.DeviceQueryInspector getDeviceQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  device search record <code> Type. </code> This method must
     *  be used to retrieve a device implementing the requested
     *  record.
     *
     *  @param deviceSearchRecordType a device search 
     *         record type 
     *  @return the device search
     *  @throws org.osid.NullArgumentException
     *          <code>deviceSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(deviceSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.DeviceSearchResultsRecord getDeviceSearchResultsRecord(org.osid.type.Type deviceSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.DeviceSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(deviceSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(deviceSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record device search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDeviceRecord(org.osid.control.records.DeviceSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "device record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
