//
// AbstractIndexedMapPollsLookupSession.java
//
//    A simple framework for providing a Polls lookup service
//    backed by a fixed collection of polls with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Polls lookup service backed by a
 *  fixed collection of polls. The polls are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some polls may be compatible
 *  with more types than are indicated through these polls
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Polls</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPollsLookupSession
    extends AbstractMapPollsLookupSession
    implements org.osid.voting.PollsLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.Polls> pollsesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Polls>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.Polls> pollsesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Polls>());


    /**
     *  Makes a <code>Polls</code> available in this session.
     *
     *  @param  polls a polls
     *  @throws org.osid.NullArgumentException <code>polls<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPolls(org.osid.voting.Polls polls) {
        super.putPolls(polls);

        this.pollsesByGenus.put(polls.getGenusType(), polls);
        
        try (org.osid.type.TypeList types = polls.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.pollsesByRecord.put(types.getNextType(), polls);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a polls from this session.
     *
     *  @param pollsId the <code>Id</code> of the polls
     *  @throws org.osid.NullArgumentException <code>pollsId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePolls(org.osid.id.Id pollsId) {
        org.osid.voting.Polls polls;
        try {
            polls = getPolls(pollsId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.pollsesByGenus.remove(polls.getGenusType());

        try (org.osid.type.TypeList types = polls.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.pollsesByRecord.remove(types.getNextType(), polls);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePolls(pollsId);
        return;
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  polls genus <code>Type</code> which does not include
     *  polls of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known polls or an error results. Otherwise,
     *  the returned list may contain only those polls that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.polls.ArrayPollsList(this.pollsesByGenus.get(pollsGenusType)));
    }


    /**
     *  Gets a <code>PollsList</code> containing the given
     *  polls record <code>Type</code>. In plenary mode, the
     *  returned list contains all known polls or an error
     *  results. Otherwise, the returned list may contain only those
     *  polls that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return the returned <code>polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByRecordType(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.polls.ArrayPollsList(this.pollsesByRecord.get(pollsRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.pollsesByGenus.clear();
        this.pollsesByRecord.clear();

        super.close();

        return;
    }
}
