//
// AbstractGradebook.java
//
//     Defines a GradebookNode within an in core hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract class for managing a hierarchy of gradebook
 *  nodes in core.
 */

public abstract class AbstractGradebookNode
    extends net.okapia.osid.jamocha.spi.AbstractOsidNode
    implements org.osid.grading.GradebookNode,
               org.osid.hierarchy.Node {

    private final org.osid.grading.Gradebook gradebook;
    private final java.util.Collection<org.osid.grading.GradebookNode> parents  = new java.util.HashSet<org.osid.grading.GradebookNode>();
    private final java.util.Collection<org.osid.grading.GradebookNode> children = new java.util.HashSet<org.osid.grading.GradebookNode>();


    /**
     *  Constructs a new <code>AbstractGradebookNode</code> from a
     *  single gradebook.
     *
     *  @param gradebook the gradebook
     *  @throws org.osid.NullArgumentException <code>gradebook</code> is 
     *          <code>null</code>.
     */

    protected AbstractGradebookNode(org.osid.grading.Gradebook gradebook) {
        setId(gradebook.getId());
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Constructs a new <code>AbstractGradebookNode</code> from a
     *  single gradebook.
     *
     *  @param gradebook the gradebook
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>gradebook</code> is 
     *          <code>null</code>.
     */

    protected AbstractGradebookNode(org.osid.grading.Gradebook gradebook, boolean root, boolean leaf) {
        this(gradebook);

        if (root) {
            root();
        } else {
            unroot();
        }

        if (leaf) {
            leaf();
        } else {
            unleaf();
        }
        
        return;
    }


    /**
     *  Adds a parent to this gradebook.
     *
     *  @param node the parent gradebook node to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    protected void addParent(org.osid.grading.GradebookNode node) {
        nullarg(node, "node");

        if (isRoot()) {
            throw new org.osid.IllegalStateException(getId() + " is a root");
        }

        this.parents.add(node);
        return;
    }


    /**
     *  Adds a child to this gradebook.
     *
     *  @param node the child gradebook node to add
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void addChild(org.osid.grading.GradebookNode node) {
        nullarg(node, "gradebook node");
        this.children.add(node);
        return;
    }


    /**
     *  Gets the <code> Gradebook </code> at this node.
     *
     *  @return the gradebook represented by this node
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook() {
        return (this.gradebook);
    }


    /**
     *  Tests if any parents are available in this node structure. There may 
     *  be no more parents in this node structure however there may be parents 
     *  that exist in the hierarchy. 
     *
     *  @return <code> true </code> if this node has parents, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean hasParents() {
        return (this.parents.size() > 0);
    }


    /**
     *  Tests if any children are available in this node structure. There may 
     *  be no more children available in this node structure but this node may 
     *  have children in the hierarchy. 
     *
     *  @return <code> true </code> if this node has children, <code>
     *          false </code> otherwise
     */
    
    @OSID @Override
    public boolean hasChildren() {
        return (this.children.size() > 0);
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of this node
     */

    @OSID @Override
    public org.osid.id.IdList getParentIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToIdList(this.parents));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getParents() {
        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToNodeList(getParentGradebookNodes()));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.grading.GradebookNodeList getParentGradebookNodes() {
        return (new net.okapia.osid.jamocha.grading.gradebooknode.ArrayGradebookNodeList(this.parents));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of this node
     */

    @OSID @Override
    public org.osid.id.IdList getChildIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToIdList(this.children));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getChildren() {
        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToNodeList(getChildGradebookNodes()));
    }


    /**
     *  Gets the child nodes of this gradebook.
     *
     *  @return the child nodes of this gradebook
     */

    @OSID @Override
    public org.osid.grading.GradebookNodeList getChildGradebookNodes() {
        return (new net.okapia.osid.jamocha.grading.gradebooknode.ArrayGradebookNodeList(this.children));
    }
}
