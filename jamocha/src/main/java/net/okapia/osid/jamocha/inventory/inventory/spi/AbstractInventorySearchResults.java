//
// AbstractInventorySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractInventorySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inventory.InventorySearchResults {

    private org.osid.inventory.InventoryList inventories;
    private final org.osid.inventory.InventoryQueryInspector inspector;
    private final java.util.Collection<org.osid.inventory.records.InventorySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractInventorySearchResults.
     *
     *  @param inventories the result set
     *  @param inventoryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>inventories</code>
     *          or <code>inventoryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractInventorySearchResults(org.osid.inventory.InventoryList inventories,
                                            org.osid.inventory.InventoryQueryInspector inventoryQueryInspector) {
        nullarg(inventories, "inventories");
        nullarg(inventoryQueryInspector, "inventory query inspectpr");

        this.inventories = inventories;
        this.inspector = inventoryQueryInspector;

        return;
    }


    /**
     *  Gets the inventory list resulting from a search.
     *
     *  @return an inventory list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventories() {
        if (this.inventories == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inventory.InventoryList inventories = this.inventories;
        this.inventories = null;
	return (inventories);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inventory.InventoryQueryInspector getInventoryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  inventory search record <code> Type. </code> This method must
     *  be used to retrieve an inventory implementing the requested
     *  record.
     *
     *  @param inventorySearchRecordType an inventory search 
     *         record type 
     *  @return the inventory search
     *  @throws org.osid.NullArgumentException
     *          <code>inventorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(inventorySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.InventorySearchResultsRecord getInventorySearchResultsRecord(org.osid.type.Type inventorySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inventory.records.InventorySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(inventorySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(inventorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record inventory search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addInventoryRecord(org.osid.inventory.records.InventorySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "inventory record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
