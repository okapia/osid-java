//
// AbstractMapObstacleLookupSession
//
//    A simple framework for providing an Obstacle lookup service
//    backed by a fixed collection of obstacles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Obstacle lookup service backed by a
 *  fixed collection of obstacles. The obstacles are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Obstacles</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapObstacleLookupSession
    extends net.okapia.osid.jamocha.mapping.path.spi.AbstractObstacleLookupSession
    implements org.osid.mapping.path.ObstacleLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.Obstacle> obstacles = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.Obstacle>());


    /**
     *  Makes an <code>Obstacle</code> available in this session.
     *
     *  @param  obstacle an obstacle
     *  @throws org.osid.NullArgumentException <code>obstacle<code>
     *          is <code>null</code>
     */

    protected void putObstacle(org.osid.mapping.path.Obstacle obstacle) {
        this.obstacles.put(obstacle.getId(), obstacle);
        return;
    }


    /**
     *  Makes an array of obstacles available in this session.
     *
     *  @param  obstacles an array of obstacles
     *  @throws org.osid.NullArgumentException <code>obstacles<code>
     *          is <code>null</code>
     */

    protected void putObstacles(org.osid.mapping.path.Obstacle[] obstacles) {
        putObstacles(java.util.Arrays.asList(obstacles));
        return;
    }


    /**
     *  Makes a collection of obstacles available in this session.
     *
     *  @param  obstacles a collection of obstacles
     *  @throws org.osid.NullArgumentException <code>obstacles<code>
     *          is <code>null</code>
     */

    protected void putObstacles(java.util.Collection<? extends org.osid.mapping.path.Obstacle> obstacles) {
        for (org.osid.mapping.path.Obstacle obstacle : obstacles) {
            this.obstacles.put(obstacle.getId(), obstacle);
        }

        return;
    }


    /**
     *  Removes an Obstacle from this session.
     *
     *  @param  obstacleId the <code>Id</code> of the obstacle
     *  @throws org.osid.NullArgumentException <code>obstacleId<code> is
     *          <code>null</code>
     */

    protected void removeObstacle(org.osid.id.Id obstacleId) {
        this.obstacles.remove(obstacleId);
        return;
    }


    /**
     *  Gets the <code>Obstacle</code> specified by its <code>Id</code>.
     *
     *  @param  obstacleId <code>Id</code> of the <code>Obstacle</code>
     *  @return the obstacle
     *  @throws org.osid.NotFoundException <code>obstacleId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>obstacleId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Obstacle getObstacle(org.osid.id.Id obstacleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.Obstacle obstacle = this.obstacles.get(obstacleId);
        if (obstacle == null) {
            throw new org.osid.NotFoundException("obstacle not found: " + obstacleId);
        }

        return (obstacle);
    }


    /**
     *  Gets all <code>Obstacles</code>. In plenary mode, the returned
     *  list contains all known obstacles or an error
     *  results. Otherwise, the returned list may contain only those
     *  obstacles that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Obstacles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstacles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.obstacle.ArrayObstacleList(this.obstacles.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.obstacles.clear();
        super.close();
        return;
    }
}
