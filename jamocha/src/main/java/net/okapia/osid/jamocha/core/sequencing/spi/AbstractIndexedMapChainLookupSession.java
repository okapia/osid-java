//
// AbstractIndexedMapChainLookupSession.java
//
//    A simple framework for providing a Chain lookup service
//    backed by a fixed collection of chains with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Chain lookup service backed by a
 *  fixed collection of chains. The chains are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some chains may be compatible
 *  with more types than are indicated through these chain
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Chains</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapChainLookupSession
    extends AbstractMapChainLookupSession
    implements org.osid.sequencing.ChainLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.sequencing.Chain> chainsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.sequencing.Chain>());
    private final MultiMap<org.osid.type.Type, org.osid.sequencing.Chain> chainsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.sequencing.Chain>());


    /**
     *  Makes a <code>Chain</code> available in this session.
     *
     *  @param  chain a chain
     *  @throws org.osid.NullArgumentException <code>chain<code> is
     *          <code>null</code>
     */

    @Override
    protected void putChain(org.osid.sequencing.Chain chain) {
        super.putChain(chain);

        this.chainsByGenus.put(chain.getGenusType(), chain);
        
        try (org.osid.type.TypeList types = chain.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.chainsByRecord.put(types.getNextType(), chain);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of chains available in this session.
     *
     *  @param  chains an array of chains
     *  @throws org.osid.NullArgumentException <code>chains<code>
     *          is <code>null</code>
     */

    @Override
    protected void putChains(org.osid.sequencing.Chain[] chains) {
        for (org.osid.sequencing.Chain chain : chains) {
            putChain(chain);
        }

        return;
    }


    /**
     *  Makes a collection of chains available in this session.
     *
     *  @param  chains a collection of chains
     *  @throws org.osid.NullArgumentException <code>chains<code>
     *          is <code>null</code>
     */

    @Override
    protected void putChains(java.util.Collection<? extends org.osid.sequencing.Chain> chains) {
        for (org.osid.sequencing.Chain chain : chains) {
            putChain(chain);
        }

        return;
    }


    /**
     *  Removes a chain from this session.
     *
     *  @param chainId the <code>Id</code> of the chain
     *  @throws org.osid.NullArgumentException <code>chainId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeChain(org.osid.id.Id chainId) {
        org.osid.sequencing.Chain chain;
        try {
            chain = getChain(chainId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.chainsByGenus.remove(chain.getGenusType());

        try (org.osid.type.TypeList types = chain.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.chainsByRecord.remove(types.getNextType(), chain);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeChain(chainId);
        return;
    }


    /**
     *  Gets a <code>ChainList</code> corresponding to the given
     *  chain genus <code>Type</code> which does not include
     *  chains of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known chains or an error results. Otherwise,
     *  the returned list may contain only those chains that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  chainGenusType a chain genus type 
     *  @return the returned <code>Chain</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>chainGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByGenusType(org.osid.type.Type chainGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.sequencing.chain.ArrayChainList(this.chainsByGenus.get(chainGenusType)));
    }


    /**
     *  Gets a <code>ChainList</code> containing the given
     *  chain record <code>Type</code>. In plenary mode, the
     *  returned list contains all known chains or an error
     *  results. Otherwise, the returned list may contain only those
     *  chains that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  chainRecordType a chain record type 
     *  @return the returned <code>chain</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByRecordType(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.sequencing.chain.ArrayChainList(this.chainsByRecord.get(chainRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.chainsByGenus.clear();
        this.chainsByRecord.clear();

        super.close();

        return;
    }
}
