//
// AbstractPayerQuery.java
//
//     A template for making a Payer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for payers.
 */

public abstract class AbstractPayerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.billing.payment.PayerQuery {

    private final java.util.Collection<org.osid.billing.payment.records.PayerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the customer <code> Id </code> for this query. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        return;
    }


    /**
     *  Clears the customer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CustomereQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Matches payers with any related customer. 
     *
     *  @param  match <code> true </code> to match payers with any related 
     *          customer, <code> false </code> to match payers with no 
     *          customer 
     */

    @OSID @Override
    public void matchAnyCustomer(boolean match) {
        return;
    }


    /**
     *  Clears the customer terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        return;
    }


    /**
     *  Matches payers using their activity account. 
     *
     *  @param  match <code> true </code> to match payers using an activity, 
     *          false otherwise 
     */

    @OSID @Override
    public void matchUsesActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearUsesActivityTerms() {
        return;
    }


    /**
     *  Matches payers using cash. 
     *
     *  @param  match <code> true </code> to match payers using cash, false 
     *          otherwise 
     */

    @OSID @Override
    public void matchUsesCash(boolean match) {
        return;
    }


    /**
     *  Clears the cash terms. 
     */

    @OSID @Override
    public void clearUsesCashTerms() {
        return;
    }


    /**
     *  Matches credit card numbers. 
     *
     *  @param  number a credit card number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditCardNumber(String number, 
                                      org.osid.type.Type stringMatchType, 
                                      boolean match) {
        return;
    }


    /**
     *  Matches payers with any credit card. 
     *
     *  @param  match <code> true </code> to match payers with any credit 
     *          card, <code> false </code> to match payers with no credit card 
     */

    @OSID @Override
    public void matchAnyCreditCardNumber(boolean match) {
        return;
    }


    /**
     *  Clears the credit card number terms. 
     */

    @OSID @Override
    public void clearCreditCardNumberTerms() {
        return;
    }


    /**
     *  Matches credit card expirations between the given date range 
     *  inclusive. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditCardExpiration(org.osid.calendaring.DateTime from, 
                                          org.osid.calendaring.DateTime to, 
                                          boolean match) {
        return;
    }


    /**
     *  Matches payers with any credit card expiration date. 
     *
     *  @param  match <code> true </code> to match payers with any credit card 
     *          expiration, <code> false </code> to match payers with no 
     *          credit card expiration 
     */

    @OSID @Override
    public void matchAnyCreditCardExpiration(boolean match) {
        return;
    }


    /**
     *  Clears the credit card expiration terms. 
     */

    @OSID @Override
    public void clearCreditCardExpirationTerms() {
        return;
    }


    /**
     *  Matches credit card security codes. 
     *
     *  @param  code a credit card code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditCardCode(String code, 
                                    org.osid.type.Type stringMatchType, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches payers with any credit card security code. 
     *
     *  @param  match <code> true </code> to match payers with any credit card 
     *          security code, <code> false </code> to match payers with no 
     *          credit card security code 
     */

    @OSID @Override
    public void matchAnyCreditCardCode(boolean match) {
        return;
    }


    /**
     *  Clears the credit card code terms. 
     */

    @OSID @Override
    public void clearCreditCardCodeTerms() {
        return;
    }


    /**
     *  Matches bank routing numbers. 
     *
     *  @param  number a bank routing number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBankRoutingNumber(String number, 
                                       org.osid.type.Type stringMatchType, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches payers with any bank routing number. 
     *
     *  @param  match <code> true </code> to match payers with any bank 
     *          routing number, <code> false </code> to match payers with no 
     *          bank routing number 
     */

    @OSID @Override
    public void matchAnyBankRoutingNumber(boolean match) {
        return;
    }


    /**
     *  Clears the bank routing number terms. 
     */

    @OSID @Override
    public void clearBankRoutingNumberTerms() {
        return;
    }


    /**
     *  Matches bank account numbers. 
     *
     *  @param  number a bank account number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBankAccountNumber(String number, 
                                       org.osid.type.Type stringMatchType, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches payers with any bank account number. 
     *
     *  @param  match <code> true </code> to match payers with any bank 
     *          account number, <code> false </code> to match payers with no 
     *          bank account number 
     */

    @OSID @Override
    public void matchAnyBankAccountNumber(boolean match) {
        return;
    }


    /**
     *  Clears the bank account number terms. 
     */

    @OSID @Override
    public void clearBankAccountNumberTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match payers 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given payer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a payer implementing the requested record.
     *
     *  @param payerRecordType a payer record type
     *  @return the payer query record
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(payerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerQueryRecord getPayerQueryRecord(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PayerQueryRecord record : this.records) {
            if (record.implementsRecordType(payerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(payerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payer query. 
     *
     *  @param payerQueryRecord payer query record
     *  @param payerRecordType payer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPayerQueryRecord(org.osid.billing.payment.records.PayerQueryRecord payerQueryRecord, 
                                          org.osid.type.Type payerRecordType) {

        addRecordType(payerRecordType);
        nullarg(payerQueryRecord, "payer query record");
        this.records.add(payerQueryRecord);        
        return;
    }
}
