//
// AbstractIndexedMapVoteLookupSession.java
//
//    A simple framework for providing a Vote lookup service
//    backed by a fixed collection of votes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Vote lookup service backed by a
 *  fixed collection of votes. The votes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some votes may be compatible
 *  with more types than are indicated through these vote
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Votes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapVoteLookupSession
    extends AbstractMapVoteLookupSession
    implements org.osid.voting.VoteLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.Vote> votesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Vote>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.Vote> votesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Vote>());


    /**
     *  Makes a <code>Vote</code> available in this session.
     *
     *  @param  vote a vote
     *  @throws org.osid.NullArgumentException <code>vote<code> is
     *          <code>null</code>
     */

    @Override
    protected void putVote(org.osid.voting.Vote vote) {
        super.putVote(vote);

        this.votesByGenus.put(vote.getGenusType(), vote);
        
        try (org.osid.type.TypeList types = vote.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.votesByRecord.put(types.getNextType(), vote);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a vote from this session.
     *
     *  @param voteId the <code>Id</code> of the vote
     *  @throws org.osid.NullArgumentException <code>voteId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeVote(org.osid.id.Id voteId) {
        org.osid.voting.Vote vote;
        try {
            vote = getVote(voteId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.votesByGenus.remove(vote.getGenusType());

        try (org.osid.type.TypeList types = vote.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.votesByRecord.remove(types.getNextType(), vote);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeVote(voteId);
        return;
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  vote genus <code>Type</code> which does not include
     *  votes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known votes or an error results. Otherwise,
     *  the returned list may contain only those votes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  voteGenusType a vote genus type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.vote.ArrayVoteList(this.votesByGenus.get(voteGenusType)));
    }


    /**
     *  Gets a <code>VoteList</code> containing the given
     *  vote record <code>Type</code>. In plenary mode, the
     *  returned list contains all known votes or an error
     *  results. Otherwise, the returned list may contain only those
     *  votes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  voteRecordType a vote record type 
     *  @return the returned <code>vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByRecordType(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.vote.ArrayVoteList(this.votesByRecord.get(voteRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.votesByGenus.clear();
        this.votesByRecord.clear();

        super.close();

        return;
    }
}
