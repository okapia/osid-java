//
// AbstractAssetContentQueryInspector.java
//
//     A template for making an AssetContentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.assetcontent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for asset contents.
 */

public abstract class AbstractAssetContentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.repository.AssetContentQueryInspector {

    private final java.util.Collection<org.osid.repository.records.AssetContentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the accesibility type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getAccessibilityTypeTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the data length query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getDataLengthTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the data query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BytesTerm[] getDataTerms() {
        return (new org.osid.search.terms.BytesTerm[0]);
    }


    /**
     *  Gets the url query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getURLTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }



    /**
     *  Gets the record corresponding to the given asset content query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an asset content implementing the requested record.
     *
     *  @param assetContentRecordType an asset content record type
     *  @return the asset content query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetContentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetContentQueryInspectorRecord getAssetContentQueryInspectorRecord(org.osid.type.Type assetContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetContentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(assetContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetContentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this asset content query. 
     *
     *  @param assetContentQueryInspectorRecord asset content query inspector
     *         record
     *  @param assetContentRecordType assetContent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssetContentQueryInspectorRecord(org.osid.repository.records.AssetContentQueryInspectorRecord assetContentQueryInspectorRecord, 
                                                   org.osid.type.Type assetContentRecordType) {

        addRecordType(assetContentRecordType);
        nullarg(assetContentRecordType, "asset content record type");
        this.records.add(assetContentQueryInspectorRecord);        
        return;
    }
}
