//
// AbstractRoutingQueueProcessorLookupSession
//
//     A routing federating adapter for a QueueProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for a QueueProcessorLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of a queue processor Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the queue processor is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingQueueProcessorLookupSession
    extends AbstractFederatingQueueProcessorLookupSession
    implements org.osid.provisioning.rules.QueueProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.QueueProcessorLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.QueueProcessorLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueProcessorLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueProcessorLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingQueueProcessorLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingQueueProcessorLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingQueueProcessorLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingQueueProcessorLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.provisioning.rules.QueueProcessorLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.provisioning.rules.QueueProcessorLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.provisioning.rules.QueueProcessorLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.provisioning.rules.QueueProcessorLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>QueueProcessor</code> specified by its <code>Id</code>. 
     *
     *  @param  queueProcessorId <code>Id</code> of the
     *          <code>QueueProcessor</code>
     *  @return the queue processor
     *  @throws org.osid.NotFoundException <code>queueProcessorId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>queueProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessor getQueueProcessor(org.osid.id.Id queueProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.QueueProcessorLookupSession session = this.sessionsById.get(queueProcessorId.getAuthority());
        if (session != null) {
            return (session.getQueueProcessor(queueProcessorId));
        }
        
        if (this.idFallback) {
            return (super.getQueueProcessor(queueProcessorId));
        }

        throw new org.osid.NotFoundException(queueProcessorId + " not found");
    }


    /**
     *  Gets a <code>QueueProcessorList</code> corresponding to the given
     *  queueProcessor genus <code>Type</code> which does not include
     *  queue processors of types derived from the specified
     *  <code>Type</code>.
     *  
     *
     *  @param  queueProcessorGenusType a queue processor genus type 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByGenusType(org.osid.type.Type queueProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueprocessor.FederatingQueueProcessorList ret = getQueueProcessorList();
        java.util.Collection<org.osid.provisioning.rules.QueueProcessorLookupSession> sessions = this.sessionsByType.get(queueProcessorGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getQueueProcessorsByGenusType(queueProcessorGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.provisioning.rules.queueprocessor.EmptyQueueProcessorList());
            }
        }


        for (org.osid.provisioning.rules.QueueProcessorLookupSession session : sessions) {
            ret.addQueueProcessorList(session.getQueueProcessorsByGenusType(queueProcessorGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueProcessorList</code> corresponding to the given
     *  queue processor genus <code>Type</code> and include any additional
     *  queue processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  queueProcessorGenusType a queue processor genus type 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByParentGenusType(org.osid.type.Type queueProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueprocessor.FederatingQueueProcessorList ret = getQueueProcessorList();
        java.util.Collection<org.osid.provisioning.rules.QueueProcessorLookupSession> sessions = this.sessionsByType.get(queueProcessorGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getQueueProcessorsByParentGenusType(queueProcessorGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.provisioning.rules.queueprocessor.EmptyQueueProcessorList());
            }
        }


        for (org.osid.provisioning.rules.QueueProcessorLookupSession session : sessions) {
            ret.addQueueProcessorList(session.getQueueProcessorsByParentGenusType(queueProcessorGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueProcessorList</code> containing the given
     *  queue processor record <code>Type</code>.
     *
     *  @param  queueProcessorRecordType a queue processor record type 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByRecordType(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.queueprocessor.FederatingQueueProcessorList ret = getQueueProcessorList();
        java.util.Collection<org.osid.provisioning.rules.QueueProcessorLookupSession> sessions = this.sessionsByType.get(queueProcessorRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getQueueProcessorsByRecordType(queueProcessorRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.provisioning.rules.queueprocessor.EmptyQueueProcessorList());
            }
        }

        for (org.osid.provisioning.rules.QueueProcessorLookupSession session : sessions) {
            ret.addQueueProcessorList(session.getQueueProcessorsByRecordType(queueProcessorRecordType));
        }
        
        ret.noMore();
        return (ret);
    }
}
