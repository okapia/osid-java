//
// AbstractRelevancySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.relevancy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRelevancySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ontology.RelevancySearchResults {

    private org.osid.ontology.RelevancyList relevancies;
    private final org.osid.ontology.RelevancyQueryInspector inspector;
    private final java.util.Collection<org.osid.ontology.records.RelevancySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRelevancySearchResults.
     *
     *  @param relevancies the result set
     *  @param relevancyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>relevancies</code>
     *          or <code>relevancyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRelevancySearchResults(org.osid.ontology.RelevancyList relevancies,
                                            org.osid.ontology.RelevancyQueryInspector relevancyQueryInspector) {
        nullarg(relevancies, "relevancies");
        nullarg(relevancyQueryInspector, "relevancy query inspectpr");

        this.relevancies = relevancies;
        this.inspector = relevancyQueryInspector;

        return;
    }


    /**
     *  Gets the relevancy list resulting from a search.
     *
     *  @return a relevancy list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevancies() {
        if (this.relevancies == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ontology.RelevancyList relevancies = this.relevancies;
        this.relevancies = null;
	return (relevancies);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ontology.RelevancyQueryInspector getRelevancyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  relevancy search record <code> Type. </code> This method must
     *  be used to retrieve a relevancy implementing the requested
     *  record.
     *
     *  @param relevancySearchRecordType a relevancy search 
     *         record type 
     *  @return the relevancy search
     *  @throws org.osid.NullArgumentException
     *          <code>relevancySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(relevancySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancySearchResultsRecord getRelevancySearchResultsRecord(org.osid.type.Type relevancySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ontology.records.RelevancySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(relevancySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(relevancySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record relevancy search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRelevancyRecord(org.osid.ontology.records.RelevancySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "relevancy record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
