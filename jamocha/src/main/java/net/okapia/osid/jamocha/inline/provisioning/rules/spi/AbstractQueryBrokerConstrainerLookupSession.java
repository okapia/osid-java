//
// AbstractQueryBrokerConstrainerLookupSession.java
//
//    An inline adapter that maps a BrokerConstrainerLookupSession to
//    a BrokerConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BrokerConstrainerLookupSession to
 *  a BrokerConstrainerQuerySession.
 */

public abstract class AbstractQueryBrokerConstrainerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractBrokerConstrainerLookupSession
    implements org.osid.provisioning.rules.BrokerConstrainerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.provisioning.rules.BrokerConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBrokerConstrainerLookupSession.
     *
     *  @param querySession the underlying broker constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBrokerConstrainerLookupSession(org.osid.provisioning.rules.BrokerConstrainerQuerySession querySession) {
        nullarg(querySession, "broker constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>BrokerConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokerConstrainers() {
        return (this.session.canSearchBrokerConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker constrainers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active broker constrainers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveBrokerConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive broker constrainers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BrokerConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BrokerConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>BrokerConstrainer</code> and retained for compatibility.
     *
     *  In active mode, broker constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker constrainers are returned.
     *
     *  @param  brokerConstrainerId <code>Id</code> of the
     *          <code>BrokerConstrainer</code>
     *  @return the broker constrainer
     *  @throws org.osid.NotFoundException <code>brokerConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainer getBrokerConstrainer(org.osid.id.Id brokerConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerConstrainerQuery query = getQuery();
        query.matchId(brokerConstrainerId, true);
        org.osid.provisioning.rules.BrokerConstrainerList brokerConstrainers = this.session.getBrokerConstrainersByQuery(query);
        if (brokerConstrainers.hasNext()) {
            return (brokerConstrainers.getNextBrokerConstrainer());
        } 
        
        throw new org.osid.NotFoundException(brokerConstrainerId + " not found");
    }


    /**
     *  Gets a <code>BrokerConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerConstrainers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>BrokerConstrainers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, broker constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker constrainers are returned.
     *
     *  @param  brokerConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BrokerConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByIds(org.osid.id.IdList brokerConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = brokerConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBrokerConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerConstrainerList</code> corresponding to the
     *  given broker constrainer genus <code>Type</code> which does
     *  not include broker constrainers of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker constrainers are returned.
     *
     *  @param  brokerConstrainerGenusType a brokerConstrainer genus type 
     *  @return the returned <code>BrokerConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByGenusType(org.osid.type.Type brokerConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerConstrainerQuery query = getQuery();
        query.matchGenusType(brokerConstrainerGenusType, true);
        return (this.session.getBrokerConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerConstrainerList</code> corresponding to the
     *  given broker constrainer genus <code>Type</code> and include
     *  any additional broker constrainers with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker constrainers are returned.
     *
     *  @param  brokerConstrainerGenusType a brokerConstrainer genus type 
     *  @return the returned <code>BrokerConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByParentGenusType(org.osid.type.Type brokerConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerConstrainerQuery query = getQuery();
        query.matchParentGenusType(brokerConstrainerGenusType, true);
        return (this.session.getBrokerConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerConstrainerList</code> containing the given
     *  broker constrainer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  broker constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker constrainers are returned.
     *
     *  @param  brokerConstrainerRecordType a brokerConstrainer record type 
     *  @return the returned <code>BrokerConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByRecordType(org.osid.type.Type brokerConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerConstrainerQuery query = getQuery();
        query.matchRecordType(brokerConstrainerRecordType, true);
        return (this.session.getBrokerConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>BrokerConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known broker
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, broker constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  broker constrainers are returned.
     *
     *  @return a list of <code>BrokerConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.BrokerConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBrokerConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.rules.BrokerConstrainerQuery getQuery() {
        org.osid.provisioning.rules.BrokerConstrainerQuery query = this.session.getBrokerConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
