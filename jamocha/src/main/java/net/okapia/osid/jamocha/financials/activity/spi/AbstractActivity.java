//
// AbstractActivity.java
//
//     Defines an Activity.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Activity</code>.
 */

public abstract class AbstractActivity
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.financials.Activity {

    private org.osid.resource.Resource organization;
    private org.osid.resource.Resource supervisor;
    private String code;

    private final java.util.Collection<org.osid.financials.records.ActivityRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the organization <code> Id </code> associated with this activity. 
     *
     *  @return the organization <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrganizationId() {
        return (this.organization.getId());
    }


    /**
     *  Gets the organization associated with this activity. 
     *
     *  @return the organization 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getOrganization()
        throws org.osid.OperationFailedException {

        return (this.organization);
    }


    /**
     *  Sets the organization.
     *
     *  @param organization an organization
     *  @throws org.osid.NullArgumentException
     *          <code>organization</code> is <code>null</code>
     */

    protected void setOrganization(org.osid.resource.Resource organization) {
        nullarg(organization, "organization");
        this.organization = organization;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> supervising this activity
     *  account.
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSupervisorId() {
        return (this.supervisor.getId());
    }


    /**
     *  Gets the resource supervising this activity account. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSupervisor()
        throws org.osid.OperationFailedException {

        return (this.supervisor);
    }


    /**
     *  Sets the supervisor.
     *
     *  @param supervisor a supervisor
     *  @throws org.osid.NullArgumentException
     *          <code>supervisor</code> is <code>null</code>
     */

    protected void setSupervisor(org.osid.resource.Resource supervisor) {
        nullarg(supervisor, "supervisor");
        this.supervisor = supervisor;
        return;
    }


    /**
     *  Gets the code for this activity account. 
     *
     *  @return the activity code 
     */

    @OSID @Override
    public String getCode() {
        return (this.code);
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    protected void setCode(String code) {
        nullarg(code, "code");
        this.code = code;
        return;
    }


    /**
     *  Tests if this activity supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return <code>true</code> if the activityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRecordType) {
        for (org.osid.financials.records.ActivityRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Activity</code> record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.ActivityRecord getActivityRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.ActivityRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityRecord the activity record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecord</code> or
     *          <code>activityRecordTypeactivity</code> is
     *          <code>null</code>
     */
            
    protected void addActivityRecord(org.osid.financials.records.ActivityRecord activityRecord, 
                                     org.osid.type.Type activityRecordType) {
        
        nullarg(activityRecord, "activity record");
        addRecordType(activityRecordType);
        this.records.add(activityRecord);
        
        return;
    }
}
