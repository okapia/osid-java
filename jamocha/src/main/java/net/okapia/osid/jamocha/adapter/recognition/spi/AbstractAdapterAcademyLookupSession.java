//
// AbstractAdapterAcademyLookupSession.java
//
//    An Academy lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recognition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Academy lookup session adapter.
 */

public abstract class AbstractAdapterAcademyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recognition.AcademyLookupSession {

    private final org.osid.recognition.AcademyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAcademyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAcademyLookupSession(org.osid.recognition.AcademyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Academy} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAcademies() {
        return (this.session.canLookupAcademies());
    }


    /**
     *  A complete view of the {@code Academy} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAcademyView() {
        this.session.useComparativeAcademyView();
        return;
    }


    /**
     *  A complete view of the {@code Academy} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAcademyView() {
        this.session.usePlenaryAcademyView();
        return;
    }

     
    /**
     *  Gets the {@code Academy} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Academy} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Academy} and
     *  retained for compatibility.
     *
     *  @param academyId {@code Id} of the {@code Academy}
     *  @return the academy
     *  @throws org.osid.NotFoundException {@code academyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code academyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAcademy(academyId));
    }


    /**
     *  Gets an {@code AcademyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  academies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Academies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  academyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Academy} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code academyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByIds(org.osid.id.IdList academyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAcademiesByIds(academyIds));
    }


    /**
     *  Gets an {@code AcademyList} corresponding to the given
     *  academy genus {@code Type} which does not include
     *  academies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned {@code Academy} list
     *  @throws org.osid.NullArgumentException
     *          {@code academyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAcademiesByGenusType(academyGenusType));
    }


    /**
     *  Gets an {@code AcademyList} corresponding to the given
     *  academy genus {@code Type} and include any additional
     *  academies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned {@code Academy} list
     *  @throws org.osid.NullArgumentException
     *          {@code academyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByParentGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAcademiesByParentGenusType(academyGenusType));
    }


    /**
     *  Gets an {@code AcademyList} containing the given
     *  academy record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyRecordType an academy record type 
     *  @return the returned {@code Academy} list
     *  @throws org.osid.NullArgumentException
     *          {@code academyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByRecordType(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAcademiesByRecordType(academyRecordType));
    }


    /**
     *  Gets an {@code AcademyList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Academy} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAcademiesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Academies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Academies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAcademies());
    }
}
