//
// MutableIndexedMapSpeedZoneEnablerLookupSession
//
//    Implements a SpeedZoneEnabler lookup service backed by a collection of
//    speedZoneEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules;


/**
 *  Implements a SpeedZoneEnabler lookup service backed by a collection of
 *  speed zone enablers. The speed zone enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some speed zone enablers may be compatible
 *  with more types than are indicated through these speed zone enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of speed zone enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapSpeedZoneEnablerLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.rules.spi.AbstractIndexedMapSpeedZoneEnablerLookupSession
    implements org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapSpeedZoneEnablerLookupSession} with no speed zone enablers.
     *
     *  @param map the map
     *  @throws org.osid.NullArgumentException {@code map}
     *          is {@code null}
     */

      public MutableIndexedMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSpeedZoneEnablerLookupSession} with a
     *  single speed zone enabler.
     *  
     *  @param map the map
     *  @param  speedZoneEnabler a single speedZoneEnabler
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZoneEnabler} is {@code null}
     */

    public MutableIndexedMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler) {
        this(map);
        putSpeedZoneEnabler(speedZoneEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSpeedZoneEnablerLookupSession} using an
     *  array of speed zone enablers.
     *
     *  @param map the map
     *  @param  speedZoneEnablers an array of speed zone enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZoneEnablers} is {@code null}
     */

    public MutableIndexedMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.path.rules.SpeedZoneEnabler[] speedZoneEnablers) {
        this(map);
        putSpeedZoneEnablers(speedZoneEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSpeedZoneEnablerLookupSession} using a
     *  collection of speed zone enablers.
     *
     *  @param map the map
     *  @param  speedZoneEnablers a collection of speed zone enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZoneEnablers} is {@code null}
     */

    public MutableIndexedMapSpeedZoneEnablerLookupSession(org.osid.mapping.Map map,
                                                  java.util.Collection<? extends org.osid.mapping.path.rules.SpeedZoneEnabler> speedZoneEnablers) {

        this(map);
        putSpeedZoneEnablers(speedZoneEnablers);
        return;
    }
    

    /**
     *  Makes a {@code SpeedZoneEnabler} available in this session.
     *
     *  @param  speedZoneEnabler a speed zone enabler
     *  @throws org.osid.NullArgumentException {@code speedZoneEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putSpeedZoneEnabler(org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler) {
        super.putSpeedZoneEnabler(speedZoneEnabler);
        return;
    }


    /**
     *  Makes an array of speed zone enablers available in this session.
     *
     *  @param  speedZoneEnablers an array of speed zone enablers
     *  @throws org.osid.NullArgumentException {@code speedZoneEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putSpeedZoneEnablers(org.osid.mapping.path.rules.SpeedZoneEnabler[] speedZoneEnablers) {
        super.putSpeedZoneEnablers(speedZoneEnablers);
        return;
    }


    /**
     *  Makes collection of speed zone enablers available in this session.
     *
     *  @param  speedZoneEnablers a collection of speed zone enablers
     *  @throws org.osid.NullArgumentException {@code speedZoneEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putSpeedZoneEnablers(java.util.Collection<? extends org.osid.mapping.path.rules.SpeedZoneEnabler> speedZoneEnablers) {
        super.putSpeedZoneEnablers(speedZoneEnablers);
        return;
    }


    /**
     *  Removes a SpeedZoneEnabler from this session.
     *
     *  @param speedZoneEnablerId the {@code Id} of the speed zone enabler
     *  @throws org.osid.NullArgumentException {@code speedZoneEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSpeedZoneEnabler(org.osid.id.Id speedZoneEnablerId) {
        super.removeSpeedZoneEnabler(speedZoneEnablerId);
        return;
    }    
}
