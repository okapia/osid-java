//
// ForumNodeToIdList.java
//
//     Implements a ForumNode IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.forum.forumnode;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying ForumNodeList to a
 *  list of Ids.
 */

public final class ForumNodeToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.forum.ForumNodeList forumNodeList;


    /**
     *  Creates a new {@code ForumNodeToIdList}.
     *
     *  @param forumNodeList a {@code ForumNodeList}
     *  @throws org.osid.NullArgumentException {@code forumNodeList}
     *          is {@code null}
     */

    public ForumNodeToIdList(org.osid.forum.ForumNodeList forumNodeList) {
        super(forumNodeList);
        this.forumNodeList = forumNodeList;
        return;
    }


    /**
     *  Creates a new {@code ForumNodeToIdList}.
     *
     *  @param forumNodes a collection of ForumNodes
     *  @throws org.osid.NullArgumentException {@code forumNodes}
     *          is {@code null}
     */

    public ForumNodeToIdList(java.util.Collection<org.osid.forum.ForumNode> forumNodes) {
        this(new net.okapia.osid.jamocha.forum.forumnode.ArrayForumNodeList(forumNodes)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.forumNodeList.getNextForumNode().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.forumNodeList.close();
        return;
    }
}
