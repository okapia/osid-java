//
// AbstractMapInquiryEnablerLookupSession
//
//    A simple framework for providing an InquiryEnabler lookup service
//    backed by a fixed collection of inquiry enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an InquiryEnabler lookup service backed by a
 *  fixed collection of inquiry enablers. The inquiry enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>InquiryEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInquiryEnablerLookupSession
    extends net.okapia.osid.jamocha.inquiry.rules.spi.AbstractInquiryEnablerLookupSession
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inquiry.rules.InquiryEnabler>());


    /**
     *  Makes an <code>InquiryEnabler</code> available in this session.
     *
     *  @param  inquiryEnabler an inquiry enabler
     *  @throws org.osid.NullArgumentException <code>inquiryEnabler<code>
     *          is <code>null</code>
     */

    protected void putInquiryEnabler(org.osid.inquiry.rules.InquiryEnabler inquiryEnabler) {
        this.inquiryEnablers.put(inquiryEnabler.getId(), inquiryEnabler);
        return;
    }


    /**
     *  Makes an array of inquiry enablers available in this session.
     *
     *  @param  inquiryEnablers an array of inquiry enablers
     *  @throws org.osid.NullArgumentException <code>inquiryEnablers<code>
     *          is <code>null</code>
     */

    protected void putInquiryEnablers(org.osid.inquiry.rules.InquiryEnabler[] inquiryEnablers) {
        putInquiryEnablers(java.util.Arrays.asList(inquiryEnablers));
        return;
    }


    /**
     *  Makes a collection of inquiry enablers available in this session.
     *
     *  @param  inquiryEnablers a collection of inquiry enablers
     *  @throws org.osid.NullArgumentException <code>inquiryEnablers<code>
     *          is <code>null</code>
     */

    protected void putInquiryEnablers(java.util.Collection<? extends org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers) {
        for (org.osid.inquiry.rules.InquiryEnabler inquiryEnabler : inquiryEnablers) {
            this.inquiryEnablers.put(inquiryEnabler.getId(), inquiryEnabler);
        }

        return;
    }


    /**
     *  Removes an InquiryEnabler from this session.
     *
     *  @param  inquiryEnablerId the <code>Id</code> of the inquiry enabler
     *  @throws org.osid.NullArgumentException <code>inquiryEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeInquiryEnabler(org.osid.id.Id inquiryEnablerId) {
        this.inquiryEnablers.remove(inquiryEnablerId);
        return;
    }


    /**
     *  Gets the <code>InquiryEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  inquiryEnablerId <code>Id</code> of the <code>InquiryEnabler</code>
     *  @return the inquiryEnabler
     *  @throws org.osid.NotFoundException <code>inquiryEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>inquiryEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnabler getInquiryEnabler(org.osid.id.Id inquiryEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inquiry.rules.InquiryEnabler inquiryEnabler = this.inquiryEnablers.get(inquiryEnablerId);
        if (inquiryEnabler == null) {
            throw new org.osid.NotFoundException("inquiryEnabler not found: " + inquiryEnablerId);
        }

        return (inquiryEnabler);
    }


    /**
     *  Gets all <code>InquiryEnablers</code>. In plenary mode, the returned
     *  list contains all known inquiryEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  inquiryEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>InquiryEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.rules.inquiryenabler.ArrayInquiryEnablerList(this.inquiryEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inquiryEnablers.clear();
        super.close();
        return;
    }
}
