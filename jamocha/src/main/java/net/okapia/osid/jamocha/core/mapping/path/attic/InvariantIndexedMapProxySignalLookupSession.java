//
// InvariantIndexedMapProxySignalLookupSession
//
//    Implements a Signal lookup service backed by a fixed
//    collection of signals indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Signal lookup service backed by a fixed
 *  collection of signals. The signals are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some signals may be compatible
 *  with more types than are indicated through these signal
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxySignalLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractIndexedMapSignalLookupSession
    implements org.osid.mapping.path.SignalLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantIndexedMapProxySignalLookupSession</code>
     *  using an array of signals.
     *
     *  @param  signals an array of signals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>signals</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantIndexedMapProxySignalLookupSession(org.osid.mapping.path.Signal[] signals, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSignals(signals);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantIndexedMapProxySignalLookupSession</code>
     *  using a collection of signals.
     *
     *  @param  signals a collection of signals
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>signals</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantIndexedMapProxySignalLookupSession(java.util.Collection<? extends org.osid.mapping.path.Signal> signals,
                                                         org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSignals(signals);
        return;
    }
}
