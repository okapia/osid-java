//
// AbstractQueryActivityLookupSession.java
//
//    An inline adapter that maps an ActivityLookupSession to
//    an ActivityQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ActivityLookupSession to
 *  an ActivityQuerySession.
 */

public abstract class AbstractQueryActivityLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractActivityLookupSession
    implements org.osid.course.ActivityLookupSession {

    private boolean effectiveonly = false;    
    private boolean denormalized = false;

    private final org.osid.course.ActivityQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryActivityLookupSession.
     *
     *  @param querySession the underlying activity query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryActivityLookupSession(org.osid.course.ActivityQuerySession querySession) {
        nullarg(querySession, "activity query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Activity</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (this.session.canSearchActivities());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only activities whose effective dates are current are returned
     *  by methods in this session.
     */

    public void useEffectiveActivityView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All activities of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveActivityView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }


    /**
     *  A normalized view uses a single <code> Activity </code> to
     *  represent a set of recurring activities.
     */

    @OSID @Override
    public void useNormalizedActivityView() {
        this.denormalized = false;
        return;
    }


    /**
     *  A denormalized view expands recurring activities into a series
     *  of activities.
     */

    @OSID @Override
    public void useDenormalizedActivityView() {
        this.denormalized = true;
        return;
    }


    /**
     *  Tests if a normalized or denormalized view is set.
     *
     *  @return <code>true</code> if denormalized</code>,
     *          <code>false</code> if normalized
     */

    protected boolean isDenormalized() {
        return (this.denormalized);
    }


    /**
     *  Gets the <code>Activity</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Activity</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Activity</code> and
     *  retained for compatibility.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param activityId <code>Id</code> of the <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchId(activityId, true);
        org.osid.course.ActivityList activities = this.session.getActivitiesByQuery(query);
        if (activities.hasNext()) {
            return (activities.getNextActivity());
        } 
        
        throw new org.osid.NotFoundException(activityId + " not found");
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *     
     *  @param  activityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();

        try (org.osid.id.IdList ids = activityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchGenusType(activityGenusType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> and include any additional
     *  activities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchParentGenusType(activityGenusType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchRecordType(activityRecordType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets a <code>ActivityList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getActivitiesByQuery(query));
    }

    
    /**
     *  Gets all <code> Activities </code> associated with a given
     *  <code> ActivityUnit. </code> 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In active mode, actviites are returned that are currently
     *  active. In any status mode, active and inactive activities are
     *  returned.
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code> activityUnitId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchActivityUnitId(activityUnitId, true);
        return (this.session.getActivitiesByQuery(query));
    }   


    /**
     *  Gets a <code>ActivityList</code> for an activity unit and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code> Id </code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitOnDate(org.osid.id.Id activityUnitId, 
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchActivityUnitId(activityUnitId, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets all <code> Activities </code> associated with a given
     *  <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In active mode, actviites are returned that are currently
     *  active. In any status mode, active and inactive activities are
     *  returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code>termId</code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchTermId(termId, true);
        return (this.session.getActivitiesByQuery(query));
    }   


    /**
     *  Gets a <code>ActivityList</code> for a term and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTermOnDate(org.osid.id.Id termId, 
                                                                   org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchTermId(termId, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets all <code> Activities </code> associated with a given
     *  <code> ActivityUnit </code> for a term. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In active mode, actviites are returned that are currently
     *  active. In any status mode, active and inactive activities are
     *  returned.
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @param  termId a term <code> Id </code> 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code> activityUnitId </code> 
     *          or <code> termId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTerm(org.osid.id.Id activityUnitId, 
                                                                            org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchActivityUnitId(activityUnitId, true);
        query.matchTermId(termId, true);
        return (this.session.getActivitiesByQuery(query));
    }
    

    /**
     *  Gets a <code>ActivityList</code> for an activity unit and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code> Id </code>
     *  @param  termId a term <code> Id </code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code>, <code>termid</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTermOnDate(org.osid.id.Id activityUnitId, 
                                                                                  org.osid.id.Id termId,
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchDate(from, to, true);
        query.matchActivityUnitId(activityUnitId, true);
        query.matchTermId(termId, true);
        return (this.session.getActivitiesByQuery(query));
    }

    
    /**
     *  Gets all <code> Activities </code> associated with a given
     *  <code> CourseOffering. </code> 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  courseOfferingId a course <code> Id </code> 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.ActivityQuery query = getQuery();
        query.matchCourseOfferingId(courseOfferingId, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets all <code>Activities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.course.ActivityQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.ActivityQuery getQuery() {
        org.osid.course.ActivityQuery query = this.session.getActivityQuery();

        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        query.matchImplicit(isDenormalized());
        return (query);
    }
}
