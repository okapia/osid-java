//
// LocationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.location.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class LocationElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the LocationElement Id.
     *
     *  @return the location element Id
     */

    public static org.osid.id.Id getLocationEntityId() {
        return (makeEntityId("osid.mapping.Location"));
    }


    /**
     *  Gets the SpatialUnit element Id.
     *
     *  @return the SpatialUnit element Id
     */

    public static org.osid.id.Id getSpatialUnit() {
        return (makeElementId("osid.mapping.location.SpatialUnit"));
    }


    /**
     *  Gets the Coordinate element Id.
     *
     *  @return the Coordinate element Id
     */

    public static org.osid.id.Id getCoordinate() {
        return (makeQueryElementId("osid.mapping.location.Coordinate"));
    }


    /**
     *  Gets the ContainedSpatialUnit element Id.
     *
     *  @return the ContainedSpatialUnit element Id
     */

    public static org.osid.id.Id getContainedSpatialUnit() {
        return (makeQueryElementId("osid.mapping.location.ContainedSpatialUnit"));
    }


    /**
     *  Gets the OverlappingSpatialUnit element Id.
     *
     *  @return the OverlappingSpatialUnit element Id
     */

    public static org.osid.id.Id getOverlappingSpatialUnit() {
        return (makeQueryElementId("osid.mapping.location.OverlappingSpatialUnit"));
    }


    /**
     *  Gets the RouteId element Id.
     *
     *  @return the RouteId element Id
     */

    public static org.osid.id.Id getRouteId() {
        return (makeQueryElementId("osid.mapping.location.RouteId"));
    }


    /**
     *  Gets the Route element Id.
     *
     *  @return the Route element Id
     */

    public static org.osid.id.Id getRoute() {
        return (makeQueryElementId("osid.mapping.location.Route"));
    }


    /**
     *  Gets the PathId element Id.
     *
     *  @return the PathId element Id
     */

    public static org.osid.id.Id getPathId() {
        return (makeQueryElementId("osid.mapping.location.PathId"));
    }


    /**
     *  Gets the Path element Id.
     *
     *  @return the Path element Id
     */

    public static org.osid.id.Id getPath() {
        return (makeQueryElementId("osid.mapping.location.Path"));
    }


    /**
     *  Gets the ContainingLocationId element Id.
     *
     *  @return the ContainingLocationId element Id
     */

    public static org.osid.id.Id getContainingLocationId() {
        return (makeQueryElementId("osid.mapping.location.ContainingLocationId"));
    }


    /**
     *  Gets the ContainingLocation element Id.
     *
     *  @return the ContainingLocation element Id
     */

    public static org.osid.id.Id getContainingLocation() {
        return (makeQueryElementId("osid.mapping.location.ContainingLocation"));
    }


    /**
     *  Gets the ContainedLocationId element Id.
     *
     *  @return the ContainedLocationId element Id
     */

    public static org.osid.id.Id getContainedLocationId() {
        return (makeQueryElementId("osid.mapping.location.ContainedLocationId"));
    }


    /**
     *  Gets the ContainedLocation element Id.
     *
     *  @return the ContainedLocation element Id
     */

    public static org.osid.id.Id getContainedLocation() {
        return (makeQueryElementId("osid.mapping.location.ContainedLocation"));
    }


    /**
     *  Gets the MapId element Id.
     *
     *  @return the MapId element Id
     */

    public static org.osid.id.Id getMapId() {
        return (makeQueryElementId("osid.mapping.location.MapId"));
    }


    /**
     *  Gets the Map element Id.
     *
     *  @return the Map element Id
     */

    public static org.osid.id.Id getMap() {
        return (makeQueryElementId("osid.mapping.location.Map"));
    }


    /**
     *  Gets the Distance element Id.
     *
     *  @return the Distance element Id
     */

    public static org.osid.id.Id getDistance() {
        return (makeSearchOrderElementId("osid.mapping.location.Distance"));
    }
}
