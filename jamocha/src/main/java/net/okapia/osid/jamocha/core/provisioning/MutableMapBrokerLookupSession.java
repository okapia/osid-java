//
// MutableMapBrokerLookupSession
//
//    Implements a Broker lookup service backed by a collection of
//    brokers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Broker lookup service backed by a collection of
 *  brokers. The brokers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of brokers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapBrokerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapBrokerLookupSession
    implements org.osid.provisioning.BrokerLookupSession {


    /**
     *  Constructs a new {@code MutableMapBrokerLookupSession}
     *  with no brokers.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapBrokerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBrokerLookupSession} with a
     *  single broker.
     *
     *  @param distributor the distributor  
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code broker} is {@code null}
     */

    public MutableMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.Broker broker) {
        this(distributor);
        putBroker(broker);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBrokerLookupSession}
     *  using an array of brokers.
     *
     *  @param distributor the distributor
     *  @param brokers an array of brokers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code brokers} is {@code null}
     */

    public MutableMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.Broker[] brokers) {
        this(distributor);
        putBrokers(brokers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBrokerLookupSession}
     *  using a collection of brokers.
     *
     *  @param distributor the distributor
     *  @param brokers a collection of brokers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code brokers} is {@code null}
     */

    public MutableMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.Broker> brokers) {

        this(distributor);
        putBrokers(brokers);
        return;
    }

    
    /**
     *  Makes a {@code Broker} available in this session.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException {@code broker{@code  is
     *          {@code null}
     */

    @Override
    public void putBroker(org.osid.provisioning.Broker broker) {
        super.putBroker(broker);
        return;
    }


    /**
     *  Makes an array of brokers available in this session.
     *
     *  @param brokers an array of brokers
     *  @throws org.osid.NullArgumentException {@code brokers{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokers(org.osid.provisioning.Broker[] brokers) {
        super.putBrokers(brokers);
        return;
    }


    /**
     *  Makes collection of brokers available in this session.
     *
     *  @param brokers a collection of brokers
     *  @throws org.osid.NullArgumentException {@code brokers{@code  is
     *          {@code null}
     */

    @Override
    public void putBrokers(java.util.Collection<? extends org.osid.provisioning.Broker> brokers) {
        super.putBrokers(brokers);
        return;
    }


    /**
     *  Removes a Broker from this session.
     *
     *  @param brokerId the {@code Id} of the broker
     *  @throws org.osid.NullArgumentException {@code brokerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeBroker(org.osid.id.Id brokerId) {
        super.removeBroker(brokerId);
        return;
    }    
}
