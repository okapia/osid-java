//
// AbstractImmutableAvailability.java
//
//     Wraps a mutable Availability to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.availability.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Availability</code> to hide modifiers. This
 *  wrapper provides an immutized Availability from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying availability whose state changes are visible.
 */

public abstract class AbstractImmutableAvailability
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.resourcing.Availability {

    private final org.osid.resourcing.Availability availability;


    /**
     *  Constructs a new <code>AbstractImmutableAvailability</code>.
     *
     *  @param availability the availability to immutablize
     *  @throws org.osid.NullArgumentException <code>availability</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAvailability(org.osid.resourcing.Availability availability) {
        super(availability);
        this.availability = availability;
        return;
    }


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.availability.getResourceId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.availability.getResource());
    }


    /**
     *  Gets the job <code> Id. </code> 
     *
     *  @return the job <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getJobId() {
        return (this.availability.getJobId());
    }


    /**
     *  Gets the job. 
     *
     *  @return the job 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob()
        throws org.osid.OperationFailedException {

        return (this.availability.getJob());
    }


    /**
     *  Tests if a competency is specified for this availability. 
     *
     *  @return <code> true </code> if a competency is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isCompetent() {
        return (this.availability.isCompetent());
    }


    /**
     *  Gets the competency <code> Id. </code> 
     *
     *  @return the competency <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCompetencyId() {
        return (this.availability.getCompetencyId());
    }


    /**
     *  Gets the competency. 
     *
     *  @return the competency 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency()
        throws org.osid.OperationFailedException {

        return (this.availability.getCompetency());
    }


    /**
     *  Gets the percentage availability. 
     *
     *  @return the percentage availability 
     */

    @OSID @Override
    public long getPercentage() {
        return (this.availability.getPercentage());
    }


    /**
     *  Gets the availability record corresponding to the given <code> 
     *  Availability </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  availabilityRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(availabilityRecordType) </code> is <code> true </code> . 
     *
     *  @param  availabilityRecordType the type of availability record to 
     *          retrieve 
     *  @return the availability record 
     *  @throws org.osid.NullArgumentException <code> availabilityRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(availabilityRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilityRecord getAvailabilityRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        return (this.availability.getAvailabilityRecord(availabilityRecordType));
    }
}

