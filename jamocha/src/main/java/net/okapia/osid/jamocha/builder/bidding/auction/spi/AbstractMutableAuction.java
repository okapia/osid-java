//
// AbstractMutableAuction.java
//
//     Defines a mutable Auction.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Auction</code>.
 */

public abstract class AbstractMutableAuction
    extends net.okapia.osid.jamocha.bidding.auction.spi.AbstractAuction
    implements org.osid.bidding.Auction,
               net.okapia.osid.jamocha.builder.bidding.auction.AuctionMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this auction. 
     *
     *  @param record auction record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAuctionRecord(org.osid.bidding.records.AuctionRecord record, org.osid.type.Type recordType) {
        super.addAuctionRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this auction is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this auction ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the provider for this auction.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(net.okapia.osid.provider.Provider provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Sets the provider of this auction
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(org.osid.resource.Resource provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Adds an asset for the provider branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    @Override
    public void addAssetToBranding(org.osid.repository.Asset asset) {
        super.addAssetToBranding(asset);
        return;
    }


    /**
     *  Adds assets for the provider branding.
     *
     *  @param assets an array of assets to add
     *  @throws org.osid.NullArgumentException <code>assets</code>
     *          is <code>null</code>
     */

    @Override
    public void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        super.setBranding(assets);
        return;
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code>
     *          is <code>null</code>
     */

    @Override
    public void setLicense(org.osid.locale.DisplayText license) {
        super.setLicense(license);
        return;
    }


    /**
     *  Enables this auction. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this auction. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this auction.
     *
     *  @param displayName the name for this auction
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this auction.
     *
     *  @param description the description of this auction
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the currency type.
     *
     *  @param currencyType a currency type
     *  @throws org.osid.NullArgumentException
     *          <code>currencyType</code> is <code>null</code>
     */

    @Override
    public void setCurrencyType(org.osid.type.Type currencyType) {
        super.setCurrencyType(currencyType);
        return;
    }


    /**
     *  Sets the minimum bidders.
     *
     *  @param bidders a minimum bidders
     *  @throws org.osid.InvalidArgumentException <code>bidders</code>
     *          is negative
     */

    @Override
    public void setMinimumBidders(long bidders) {
        super.setMinimumBidders(bidders);
        return;
    }


    /**
     *  Sets the sealed flag.
     *
     *  @param sealed <code> true </code> if this auction is sealed,
     *          <code> false </code> otherwise
     */

    @Override
    public void setSealed(boolean sealed) {
        super.setSealed(sealed);
        return;
    }


    /**
     *  Sets the seller.
     *
     *  @param seller a seller
     *  @throws org.osid.NullArgumentException
     *          <code>seller</code> is <code>null</code>
     */

    @Override
    public void setSeller(org.osid.resource.Resource seller) {
        super.setSeller(seller);
        return;
    }


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException
     *          <code>item</code> is <code>null</code>
     */

    @Override
    public void setItem(org.osid.resource.Resource item) {
        super.setItem(item);
        return;
    }


    /**
     *  Sets the lot size.
     *
     *  @param size a lot size
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    @Override
    public void setLotSize(long size) {
        super.setLotSize(size);
        return;
    }


    /**
     *  Sets the remaining items.
     *
     *  @param number a remaining items
     *  @throws org.osid.InvalidArgumentException <code>number</code>
     *          is negative
     */

    @Override
    public void setRemainingItems(long number) {
        super.setRemainingItems(number);
        return;
    }


    /**
     *  Sets the item limit.
     *
     *  @param limit an item limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    @Override
    public void setItemLimit(long limit) {
        super.setItemLimit(limit);
        return;
    }


    /**
     *  Sets the starting price.
     *
     *  @param price a starting price
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    @Override
    public void setStartingPrice(org.osid.financials.Currency price) {
        super.setStartingPrice(price);
        return;
    }


    /**
     *  Sets the price increment.
     *
     *  @param increment a price increment
     *  @throws org.osid.NullArgumentException
     *          <code>increment</code> is <code>null</code>
     */

    @Override
    public void setPriceIncrement(org.osid.financials.Currency increment) {
        super.setPriceIncrement(increment);
        return;
    }


    /**
     *  Sets the reserve price.
     *
     *  @param price a reserve price
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    @Override
    public void setReservePrice(org.osid.financials.Currency price) {
        super.setReservePrice(price);
        return;
    }


    /**
     *  Sets the buyout price.
     *
     *  @param price a buyout price
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    @Override
    public void setBuyoutPrice(org.osid.financials.Currency price) {
        super.setBuyoutPrice(price);
        return;
    }
}

