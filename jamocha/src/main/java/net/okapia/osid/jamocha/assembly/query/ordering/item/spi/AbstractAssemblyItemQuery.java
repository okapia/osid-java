//
// AbstractAssemblyItemQuery.java
//
//     An ItemQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ordering.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ItemQuery that stores terms.
 */

public abstract class AbstractAssemblyItemQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.ordering.ItemQuery,
               org.osid.ordering.ItemQueryInspector,
               org.osid.ordering.ItemSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.ItemQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.ItemQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.ItemSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyItemQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyItemQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the order <code> Id </code> for this query to match orders 
     *  assigned to items. 
     *
     *  @param  orderId a order <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> orderId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOrderId(org.osid.id.Id orderId, boolean match) {
        getAssembler().addIdTerm(getOrderIdColumn(), orderId, match);
        return;
    }


    /**
     *  Clears the order <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrderIdTerms() {
        getAssembler().clearTerms(getOrderIdColumn());
        return;
    }


    /**
     *  Gets the order <code> Id </code> terms. 
     *
     *  @return the order <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrderIdTerms() {
        return (getAssembler().getIdTerms(getOrderIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the order. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOrder(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOrderColumn(), style);
        return;
    }


    /**
     *  Gets the OrderId column name.
     *
     * @return the column name
     */

    protected String getOrderIdColumn() {
        return ("order_id");
    }


    /**
     *  Tests if a order query is available. 
     *
     *  @return <code> true </code> if a order query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. 
     *
     *  @return the order query 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuery getOrderQuery() {
        throw new org.osid.UnimplementedException("supportsOrderQuery() is false");
    }


    /**
     *  Clears the order terms. 
     */

    @OSID @Override
    public void clearOrderTerms() {
        getAssembler().clearTerms(getOrderColumn());
        return;
    }


    /**
     *  Gets the order terms. 
     *
     *  @return the order terms 
     */

    @OSID @Override
    public org.osid.ordering.OrderQueryInspector[] getOrderTerms() {
        return (new org.osid.ordering.OrderQueryInspector[0]);
    }


    /**
     *  Tests if an order order is available. 
     *
     *  @return <code> true </code> if an order order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the order order. 
     *
     *  @return the order search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchOrder getOrderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOrderSearchOrder() is false");
    }


    /**
     *  Gets the Order column name.
     *
     * @return the column name
     */

    protected String getOrderColumn() {
        return ("order");
    }


    /**
     *  Matches items that are derived. 
     *
     *  @param  match <code> true </code> to match derived items, <code> false 
     *          </code> to match selected items 
     */

    @OSID @Override
    public void matchDerived(boolean match) {
        getAssembler().addBooleanTerm(getDerivedColumn(), match);
        return;
    }


    /**
     *  Clears the derived terms. 
     */

    @OSID @Override
    public void clearDerivedTerms() {
        getAssembler().clearTerms(getDerivedColumn());
        return;
    }


    /**
     *  Gets the derived terms. 
     *
     *  @return the derived terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDerivedTerms() {
        return (getAssembler().getBooleanTerms(getDerivedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the derived 
     *  flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDerived(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDerivedColumn(), style);
        return;
    }


    /**
     *  Gets the Derived column name.
     *
     * @return the column name
     */

    protected String getDerivedColumn() {
        return ("derived");
    }


    /**
     *  Sets the product <code> Id </code> for this query. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        getAssembler().addIdTerm(getProductIdColumn(), productId, match);
        return;
    }


    /**
     *  Clears the product <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        getAssembler().clearTerms(getProductIdColumn());
        return;
    }


    /**
     *  Gets the product <code> Id </code> terms. 
     *
     *  @return the product <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProductIdTerms() {
        return (getAssembler().getIdTerms(getProductIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the product. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProduct(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProductColumn(), style);
        return;
    }


    /**
     *  Gets the ProductId column name.
     *
     * @return the column name
     */

    protected String getProductIdColumn() {
        return ("product_id");
    }


    /**
     *  Tests if a product query is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        getAssembler().clearTerms(getProductColumn());
        return;
    }


    /**
     *  Gets the product terms. 
     *
     *  @return the product terms 
     */

    @OSID @Override
    public org.osid.ordering.ProductQueryInspector[] getProductTerms() {
        return (new org.osid.ordering.ProductQueryInspector[0]);
    }


    /**
     *  Tests if a product order is available. 
     *
     *  @return <code> true </code> if a product order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductSearchOrder() {
        return (false);
    }


    /**
     *  Gets the product order. 
     *
     *  @return the product search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchOrder getProductSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProductSearchOrder() is false");
    }


    /**
     *  Gets the Product column name.
     *
     * @return the column name
     */

    protected String getProductColumn() {
        return ("product");
    }


    /**
     *  Matches quantities between the given range inclusive. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchQuantity(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getQuantityColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the quantity terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        getAssembler().clearTerms(getQuantityColumn());
        return;
    }


    /**
     *  Gets the quantity terms. 
     *
     *  @return the quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getQuantityTerms() {
        return (getAssembler().getCardinalRangeTerms(getQuantityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the quantity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuantity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQuantityColumn(), style);
        return;
    }


    /**
     *  Gets the Quantity column name.
     *
     * @return the column name
     */

    protected String getQuantityColumn() {
        return ("quantity");
    }


    /**
     *  Matches costs between the given range inclusive. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCost(org.osid.financials.Currency low, 
                          org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getCostColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the cost terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        getAssembler().clearTerms(getCostColumn());
        return;
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (getAssembler().getCurrencyRangeTerms(getCostColumn()));
    }


    /**
     *  Gets the Cost column name.
     *
     * @return the column name
     */

    protected String getCostColumn() {
        return ("cost");
    }


    /**
     *  Matches minimum costs between the given range inclusive. 
     *
     *  @param  cost cost 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cost </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMinimumCost(org.osid.financials.Currency cost, 
                                 boolean match) {
        getAssembler().addCurrencyTerm(getMinimumCostColumn(), cost, match);
        return;
    }


    /**
     *  Clears the minimum cost terms. 
     */

    @OSID @Override
    public void clearMinimumCostTerms() {
        getAssembler().clearTerms(getMinimumCostColumn());
        return;
    }


    /**
     *  Gets the minimum cost terms. 
     *
     *  @return the minimum cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getMinimumCostTerms() {
        return (getAssembler().getCurrencyTerms(getMinimumCostColumn()));
    }


    /**
     *  Gets the MinimumCost column name.
     *
     * @return the column name
     */

    protected String getMinimumCostColumn() {
        return ("minimum_cost");
    }


    /**
     *  Sets the item <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        getAssembler().clearTerms(getStoreIdColumn());
        return;
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (getAssembler().getIdTerms(getStoreIdColumn()));
    }


    /**
     *  Gets the StoreId column name.
     *
     * @return the column name
     */

    protected String getStoreIdColumn() {
        return ("store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        getAssembler().clearTerms(getStoreColumn());
        return;
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the Store column name.
     *
     * @return the column name
     */

    protected String getStoreColumn() {
        return ("store");
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.ordering.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ItemQueryInspectorRecord getItemQueryInspectorRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ItemQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param itemRecordType the item record type
     *  @return the item search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ItemSearchOrderRecord getItemSearchOrderRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ItemSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this item. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemQueryRecord the item query record
     *  @param itemQueryInspectorRecord the item query inspector
     *         record
     *  @param itemSearchOrderRecord the item search order record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          <code>itemQueryRecord</code>,
     *          <code>itemQueryInspectorRecord</code>,
     *          <code>itemSearchOrderRecord</code> or
     *          <code>itemRecordTypeitem</code> is
     *          <code>null</code>
     */
            
    protected void addItemRecords(org.osid.ordering.records.ItemQueryRecord itemQueryRecord, 
                                      org.osid.ordering.records.ItemQueryInspectorRecord itemQueryInspectorRecord, 
                                      org.osid.ordering.records.ItemSearchOrderRecord itemSearchOrderRecord, 
                                      org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);

        nullarg(itemQueryRecord, "item query record");
        nullarg(itemQueryInspectorRecord, "item query inspector record");
        nullarg(itemSearchOrderRecord, "item search odrer record");

        this.queryRecords.add(itemQueryRecord);
        this.queryInspectorRecords.add(itemQueryInspectorRecord);
        this.searchOrderRecords.add(itemSearchOrderRecord);
        
        return;
    }
}
