//
// MutableMapProxyJobProcessorLookupSession
//
//    Implements a JobProcessor lookup service backed by a collection of
//    jobProcessors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobProcessor lookup service backed by a collection of
 *  jobProcessors. The jobProcessors are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of job processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyJobProcessorLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobProcessorLookupSession
    implements org.osid.resourcing.rules.JobProcessorLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyJobProcessorLookupSession}
     *  with no job processors.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyJobProcessorLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyJobProcessorLookupSession} with a
     *  single job processor.
     *
     *  @param foundry the foundry
     *  @param jobProcessor a job processor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobProcessor}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyJobProcessorLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.rules.JobProcessor jobProcessor, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putJobProcessor(jobProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyJobProcessorLookupSession} using an
     *  array of job processors.
     *
     *  @param foundry the foundry
     *  @param jobProcessors an array of job processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobProcessors}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyJobProcessorLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.rules.JobProcessor[] jobProcessors, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putJobProcessors(jobProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyJobProcessorLookupSession} using a
     *  collection of job processors.
     *
     *  @param foundry the foundry
     *  @param jobProcessors a collection of job processors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code jobProcessors}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyJobProcessorLookupSession(org.osid.resourcing.Foundry foundry,
                                                java.util.Collection<? extends org.osid.resourcing.rules.JobProcessor> jobProcessors,
                                                org.osid.proxy.Proxy proxy) {
   
        this(foundry, proxy);
        setSessionProxy(proxy);
        putJobProcessors(jobProcessors);
        return;
    }

    
    /**
     *  Makes a {@code JobProcessor} available in this session.
     *
     *  @param jobProcessor an job processor
     *  @throws org.osid.NullArgumentException {@code jobProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putJobProcessor(org.osid.resourcing.rules.JobProcessor jobProcessor) {
        super.putJobProcessor(jobProcessor);
        return;
    }


    /**
     *  Makes an array of jobProcessors available in this session.
     *
     *  @param jobProcessors an array of job processors
     *  @throws org.osid.NullArgumentException {@code jobProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putJobProcessors(org.osid.resourcing.rules.JobProcessor[] jobProcessors) {
        super.putJobProcessors(jobProcessors);
        return;
    }


    /**
     *  Makes collection of job processors available in this session.
     *
     *  @param jobProcessors
     *  @throws org.osid.NullArgumentException {@code jobProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putJobProcessors(java.util.Collection<? extends org.osid.resourcing.rules.JobProcessor> jobProcessors) {
        super.putJobProcessors(jobProcessors);
        return;
    }


    /**
     *  Removes a JobProcessor from this session.
     *
     *  @param jobProcessorId the {@code Id} of the job processor
     *  @throws org.osid.NullArgumentException {@code jobProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeJobProcessor(org.osid.id.Id jobProcessorId) {
        super.removeJobProcessor(jobProcessorId);
        return;
    }    
}
