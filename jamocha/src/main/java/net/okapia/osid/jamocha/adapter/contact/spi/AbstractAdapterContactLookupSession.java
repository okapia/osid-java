//
// AbstractAdapterContactLookupSession.java
//
//    A Contact lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Contact lookup session adapter.
 */

public abstract class AbstractAdapterContactLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.contact.ContactLookupSession {

    private final org.osid.contact.ContactLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterContactLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterContactLookupSession(org.osid.contact.ContactLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AddressBook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AddressBook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.session.getAddressBookId());
    }


    /**
     *  Gets the {@code AddressBook} associated with this session.
     *
     *  @return the {@code AddressBook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAddressBook());
    }


    /**
     *  Tests if this user can perform {@code Contact} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupContacts() {
        return (this.session.canLookupContacts());
    }


    /**
     *  A complete view of the {@code Contact} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeContactView() {
        this.session.useComparativeContactView();
        return;
    }


    /**
     *  A complete view of the {@code Contact} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryContactView() {
        this.session.usePlenaryContactView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include contacts in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.session.useFederatedAddressBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.session.useIsolatedAddressBookView();
        return;
    }
    

    /**
     *  Only contacts whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveContactView() {
        this.session.useEffectiveContactView();
        return;
    }
    

    /**
     *  All contacts of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveContactView() {
        this.session.useAnyEffectiveContactView();
        return;
    }

     
    /**
     *  Gets the {@code Contact} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Contact} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Contact} and
     *  retained for compatibility.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param contactId {@code Id} of the {@code Contact}
     *  @return the contact
     *  @throws org.osid.NotFoundException {@code contactId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code contactId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Contact getContact(org.osid.id.Id contactId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContact(contactId));
    }


    /**
     *  Gets a {@code ContactList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  contacts specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Contacts} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Contact} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code contactIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByIds(org.osid.id.IdList contactIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByIds(contactIds));
    }


    /**
     *  Gets a {@code ContactList} corresponding to the given
     *  contact genus {@code Type} which does not include
     *  contacts of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactGenusType a contact genus type 
     *  @return the returned {@code Contact} list
     *  @throws org.osid.NullArgumentException
     *          {@code contactGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByGenusType(contactGenusType));
    }


    /**
     *  Gets a {@code ContactList} corresponding to the given
     *  contact genus {@code Type} and include any additional
     *  contacts with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactGenusType a contact genus type 
     *  @return the returned {@code Contact} list
     *  @throws org.osid.NullArgumentException
     *          {@code contactGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByParentGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByParentGenusType(contactGenusType));
    }


    /**
     *  Gets a {@code ContactList} containing the given
     *  contact record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactRecordType a contact record type 
     *  @return the returned {@code Contact} list
     *  @throws org.osid.NullArgumentException
     *          {@code contactRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByRecordType(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByRecordType(contactRecordType));
    }


    /**
     *  Gets a {@code ContactList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *  
     *  In active mode, contacts are returned that are currently
     *  active. In any status mode, active and inactive contacts
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Contact} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.contact.ContactList getContactsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsOnDate(from, to));
    }
        

    /**
     *  Gets a list of contacts of a genus type and with an effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactGenusType a contact genus type 
     *  @param  from the starting date 
     *  @param  to the ending date 
     *  @return the returned {@code ContactList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code contactGenusType, from, 
     *         } or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeOnDate(org.osid.type.Type contactGenusType, 
                                                                     org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByGenusTypeOnDate(contactGenusType, from, to));
    }


    /**
     *  Gets a list of contacts corresponding to a reference
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @return the returned {@code ContactList}
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsForReference(referenceId));
    }


    /**
     *  Gets a list of contacts corresponding to a reference
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ContactList}
     *  @throws org.osid.NullArgumentException {@code referenceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsForReferenceOnDate(referenceId, from, to));
    }


    /**
     *  Gets a list of contacts of the given genus type corresponding
     *  to a reference {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference 
     *  @param  contactGenusType the genus type of the contact 
     *  @return the returned {@code ContactList} 
     *  @throws org.osid.NullArgumentException {@code referenceId} or 
     *          {@code contactGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReference(org.osid.id.Id referenceId, 
                                                                           org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getContactsByGenusTypeForReference(referenceId, contactGenusType));
    }


    /**
     *  Gets a list of all contacts of the given genus type
     *  corresponding to a reference {@code Id} and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId a reference {@code Id} 
     *  @param  contactGenusType the genus type of the contact 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code ContactList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code referenceId, 
     *          contactGenusType, from} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId, 
                                                                                 org.osid.type.Type contactGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByGenusTypeForReferenceOnDate(referenceId, contactGenusType, from, to));
    }


    /**
     *  Gets a list of contacts corresponding to an address
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  addressId the {@code Id} of the address
     *  @return the returned {@code ContactList}
     *  @throws org.osid.NullArgumentException {@code addressId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForAddress(org.osid.id.Id addressId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsForAddress(addressId));
    }


    /**
     *  Gets a list of contacts corresponding to an address
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  addressId the {@code Id} of the address
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ContactList}
     *  @throws org.osid.NullArgumentException {@code addressId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForAddressOnDate(org.osid.id.Id addressId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsForAddressOnDate(addressId, from, to));
    }


    /**
     *  Gets a list of all contacts of the given genus type
     *  corresponding to an address {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  addressId the {@code Id} of the address 
     *  @param  contactGenusType the genus type of the contact 
     *  @return the returned {@code ContactList} 
     *  @throws org.osid.NullArgumentException {@code addressId} or 
     *          {@code contactGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForAddress(org.osid.id.Id addressId, 
                                                                         org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByGenusTypeForAddress(addressId, contactGenusType));
    }


    /**
     *  Gets a list of all contacts of the given genus type
     *  corresponding to an address {@code Id} and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  addressId an address {@code Id} 
     *  @param  contactGenusType the genus type of the contact 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code ContactList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code addressId, 
     *          contactGenusType, from} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForAddressOnDate(org.osid.id.Id addressId, 
                                                                               org.osid.type.Type contactGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByGenusTypeForAddressOnDate(addressId, contactGenusType, from, to));
    }


    /**
     *  Gets a list of contacts corresponding to reference and address
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  addressId the {@code Id} of the address
     *  @return the returned {@code ContactList}
     *  @throws org.osid.NullArgumentException {@code referenceId},
     *          {@code addressId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForReferenceAndAddress(org.osid.id.Id referenceId,
                                                                          org.osid.id.Id addressId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsForReferenceAndAddress(referenceId, addressId));
    }


    /**
     *  Gets a list of contacts corresponding to reference and address
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective. In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  addressId the {@code Id} of the address
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ContactList}
     *  @throws org.osid.NullArgumentException {@code referenceId},
     *          {@code addressId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForReferenceAndAddressOnDate(org.osid.id.Id referenceId,
                                                                                org.osid.id.Id addressId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsForReferenceAndAddressOnDate(referenceId, addressId, from, to));
    }


    /**
     *  Gets a list of all contacts with the given genus type
     *  corresponding to a reference and address {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference 
     *  @param  addressId the {@code Id} of the address 
     *  @param  contactGenusType the genus type of the contact 
     *  @return the returned {@code ContactList} 
     *  @throws org.osid.NullArgumentException {@code referenceId, addressId 
     *         } or {@code contactGenusType} is {@code null 
     *         } 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReferenceAndAddress(org.osid.id.Id referenceId, 
                                                                                     org.osid.id.Id addressId, 
                                                                                     org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByGenusTypeForReferenceAndAddress(referenceId, addressId, contactGenusType));
    }


    /**
     *  Gets a list of all contacts with the given genus type
     *  corresponding to a reference and address {@code Id} and
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference 
     *  @param  addressId an address {@code Id} 
     *  @param  contactGenusType the genus type of the contact 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code ContactList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code referenceId, addressId, 
     *          contactGenusType, from} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReferenceAndAddressOnDate(org.osid.id.Id referenceId, 
                                                                                           org.osid.id.Id addressId, 
                                                                                           org.osid.type.Type contactGenusType, 
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContactsByGenusTypeForReferenceAndAddressOnDate(referenceId, addressId, contactGenusType, from, to));
    }


    /**
     *  Gets all {@code Contacts}. 
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Contacts} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContacts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getContacts());
    }
}
