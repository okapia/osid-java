//
// MutableMapProxyPathLookupSession
//
//    Implements a Path lookup service backed by a collection of
//    paths that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Path lookup service backed by a collection of
 *  paths. The paths are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of paths can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyPathLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapPathLookupSession
    implements org.osid.mapping.path.PathLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyPathLookupSession}
     *  with no paths.
     *
     *  @param map the map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyPathLookupSession(org.osid.mapping.Map map,
                                                  org.osid.proxy.Proxy proxy) {
        setMap(map);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyPathLookupSession} with a
     *  single path.
     *
     *  @param map the map
     *  @param path a path
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code path}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyPathLookupSession(org.osid.mapping.Map map,
                                                org.osid.mapping.path.Path path, org.osid.proxy.Proxy proxy) {
        this(map, proxy);
        putPath(path);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyPathLookupSession} using an
     *  array of paths.
     *
     *  @param map the map
     *  @param paths an array of paths
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code paths}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyPathLookupSession(org.osid.mapping.Map map,
                                                org.osid.mapping.path.Path[] paths, org.osid.proxy.Proxy proxy) {
        this(map, proxy);
        putPaths(paths);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyPathLookupSession} using a
     *  collection of paths.
     *
     *  @param map the map
     *  @param paths a collection of paths
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code paths}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyPathLookupSession(org.osid.mapping.Map map,
                                                java.util.Collection<? extends org.osid.mapping.path.Path> paths,
                                                org.osid.proxy.Proxy proxy) {
   
        this(map, proxy);
        setSessionProxy(proxy);
        putPaths(paths);
        return;
    }

    
    /**
     *  Makes a {@code Path} available in this session.
     *
     *  @param path an path
     *  @throws org.osid.NullArgumentException {@code path{@code 
     *          is {@code null}
     */

    @Override
    public void putPath(org.osid.mapping.path.Path path) {
        super.putPath(path);
        return;
    }


    /**
     *  Makes an array of paths available in this session.
     *
     *  @param paths an array of paths
     *  @throws org.osid.NullArgumentException {@code paths{@code 
     *          is {@code null}
     */

    @Override
    public void putPaths(org.osid.mapping.path.Path[] paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Makes collection of paths available in this session.
     *
     *  @param paths
     *  @throws org.osid.NullArgumentException {@code path{@code 
     *          is {@code null}
     */

    @Override
    public void putPaths(java.util.Collection<? extends org.osid.mapping.path.Path> paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Removes a Path from this session.
     *
     *  @param pathId the {@code Id} of the path
     *  @throws org.osid.NullArgumentException {@code pathId{@code  is
     *          {@code null}
     */

    @Override
    public void removePath(org.osid.id.Id pathId) {
        super.removePath(pathId);
        return;
    }    
}
