//
// InvariantMapProxyOffsetEventEnablerLookupSession
//
//    Implements an OffsetEventEnabler lookup service backed by a fixed
//    collection of offsetEventEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements an OffsetEventEnabler lookup service backed by a fixed
 *  collection of offset event enablers. The offset event enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyOffsetEventEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractMapOffsetEventEnablerLookupSession
    implements org.osid.calendaring.rules.OffsetEventEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOffsetEventEnablerLookupSession} with no
     *  offset event enablers.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyOffsetEventEnablerLookupSession} with a single
     *  offset event enabler.
     *
     *  @param calendar the calendar
     *  @param offsetEventEnabler an single offset event enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEventEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putOffsetEventEnabler(offsetEventEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyOffsetEventEnablerLookupSession} using
     *  an array of offset event enablers.
     *
     *  @param calendar the calendar
     *  @param offsetEventEnablers an array of offset event enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEventEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.rules.OffsetEventEnabler[] offsetEventEnablers, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putOffsetEventEnablers(offsetEventEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOffsetEventEnablerLookupSession} using a
     *  collection of offset event enablers.
     *
     *  @param calendar the calendar
     *  @param offsetEventEnablers a collection of offset event enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code offsetEventEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putOffsetEventEnablers(offsetEventEnablers);
        return;
    }
}
