//
// AbstractIndexedMapRequestLookupSession.java
//
//    A simple framework for providing a Request lookup service
//    backed by a fixed collection of requests with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Request lookup service backed by a
 *  fixed collection of requests. The requests are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some requests may be compatible
 *  with more types than are indicated through these request
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Requests</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRequestLookupSession
    extends AbstractMapRequestLookupSession
    implements org.osid.provisioning.RequestLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Request> requestsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Request>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Request> requestsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Request>());


    /**
     *  Makes a <code>Request</code> available in this session.
     *
     *  @param  request a request
     *  @throws org.osid.NullArgumentException <code>request<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRequest(org.osid.provisioning.Request request) {
        super.putRequest(request);

        this.requestsByGenus.put(request.getGenusType(), request);
        
        try (org.osid.type.TypeList types = request.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.requestsByRecord.put(types.getNextType(), request);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a request from this session.
     *
     *  @param requestId the <code>Id</code> of the request
     *  @throws org.osid.NullArgumentException <code>requestId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRequest(org.osid.id.Id requestId) {
        org.osid.provisioning.Request request;
        try {
            request = getRequest(requestId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.requestsByGenus.remove(request.getGenusType());

        try (org.osid.type.TypeList types = request.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.requestsByRecord.remove(types.getNextType(), request);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRequest(requestId);
        return;
    }


    /**
     *  Gets a <code>RequestList</code> corresponding to the given
     *  request genus <code>Type</code> which does not include
     *  requests of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known requests or an error results. Otherwise,
     *  the returned list may contain only those requests that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  requestGenusType a request genus type 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByGenusType(org.osid.type.Type requestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.request.ArrayRequestList(this.requestsByGenus.get(requestGenusType)));
    }


    /**
     *  Gets a <code>RequestList</code> containing the given
     *  request record <code>Type</code>. In plenary mode, the
     *  returned list contains all known requests or an error
     *  results. Otherwise, the returned list may contain only those
     *  requests that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  requestRecordType a request record type 
     *  @return the returned <code>request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByRecordType(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.request.ArrayRequestList(this.requestsByRecord.get(requestRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.requestsByGenus.clear();
        this.requestsByRecord.clear();

        super.close();

        return;
    }
}
