//
// AbstractSummary.java
//
//     Defines a Summary builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.summary.spi;


/**
 *  Defines a <code>Summary</code> builder.
 */

public abstract class AbstractSummaryBuilder<T extends AbstractSummaryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCompendiumBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.financials.summary.SummaryMiter summary;


    /**
     *  Constructs a new <code>AbstractSummaryBuilder</code>.
     *
     *  @param summary the summary to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSummaryBuilder(net.okapia.osid.jamocha.builder.financials.summary.SummaryMiter summary) {
        super(summary);
        this.summary = summary;
        return;
    }


    /**
     *  Builds the summary.
     *
     *  @return the new summary
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.financials.Summary build() {
        (new net.okapia.osid.jamocha.builder.validator.financials.summary.SummaryValidator(getValidations())).validate(this.summary);
        return (new net.okapia.osid.jamocha.builder.financials.summary.ImmutableSummary(this.summary));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the summary miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.financials.summary.SummaryMiter getMiter() {
        return (this.summary);
    }


    /**
     *  Sets the account.
     *
     *  @param account the account
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    public T account(org.osid.financials.Account account) {
        getMiter().setAccount(account);
        return (self());
    }


    /**
     *  Sets the fiscal period.
     *
     *  @param period the fiscal period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public T fiscalPeriod(org.osid.financials.FiscalPeriod period) {
        getMiter().setFiscalPeriod(period);
        return (self());
    }


    /**
     *  Sets the credits.
     *
     *  @param credits the credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T credits(org.osid.financials.Currency credits) {
        getMiter().setCredits(credits);
        return (self());
    }


    /**
     *  Sets the debits.
     *
     *  @param debits the debits
     *  @throws org.osid.NullArgumentException <code>debits</code> is
     *          <code>null</code>
     */

    public T debits(org.osid.financials.Currency debits) {
        getMiter().setDebits(debits);
        return (self());
    }


    /**
     *  Sets the budget.
     *
     *  @param budget the budget
     *  @throws org.osid.NullArgumentException <code>budget</code> is
     *          <code>null</code>
     */

    public T budget(org.osid.financials.Currency budget) {
        getMiter().setBudget(budget);
        return (self());
    }


    /**
     *  Sets the forecast.
     *
     *  @param forecast the forecast
     *  @throws org.osid.NullArgumentException <code>forecast</code> is
     *          <code>null</code>
     */

    public T forecast(org.osid.financials.Currency forecast) {
        getMiter().setForecast(forecast);
        return (self());
    }


    /**
     *  Adds a Summary record.
     *
     *  @param record a summary record
     *  @param recordType the type of summary record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.financials.records.SummaryRecord record, org.osid.type.Type recordType) {
        getMiter().addSummaryRecord(record, recordType);
        return (self());
    }
}       


