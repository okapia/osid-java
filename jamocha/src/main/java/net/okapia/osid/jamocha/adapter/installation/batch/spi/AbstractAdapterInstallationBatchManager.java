//
// AbstractInstallationBatchManager.java
//
//     An adapter for a InstallationBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.installation.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InstallationBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInstallationBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.installation.batch.InstallationBatchManager>
    implements org.osid.installation.batch.InstallationBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterInstallationBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInstallationBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInstallationBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInstallationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of packages is available. 
     *
     *  @return <code> true </code> if a package bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageBatchAdmin() {
        return (getAdapteeManager().supportsPackageBatchAdmin());
    }


    /**
     *  Tests if bulk administration of depots is available. 
     *
     *  @return <code> true </code> if a depot bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotBatchAdmin() {
        return (getAdapteeManager().supportsDepotBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk package 
     *  administration service. 
     *
     *  @return a <code> PackageBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.PackageBatchAdminSession getPackageBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk package 
     *  administration service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the <code> Depot </code> 
     *  @return a <code> PackageBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Depot </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.PackageBatchAdminSession getPackageBatchAdminSessionForDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageBatchAdminSessionForDepot(depotId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk depot 
     *  administration service. 
     *
     *  @return a <code> DepotBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.DepotBatchAdminSession getDepotBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
