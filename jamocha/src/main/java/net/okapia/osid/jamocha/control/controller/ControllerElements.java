//
// ControllerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.controller;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class ControllerElements
    extends net.okapia.osid.jamocha.spi.OperableOsidObjectElements {


    /**
     *  Gets the ControllerElement Id.
     *
     *  @return the controller element Id
     */

    public static org.osid.id.Id getControllerEntityId() {
        return (makeEntityId("osid.control.Controller"));
    }


    /**
     *  Gets the Address element Id.
     *
     *  @return the Address element Id
     */

    public static org.osid.id.Id getAddress() {
        return (makeElementId("osid.control.controller.Address"));
    }


    /**
     *  Gets the ModelId element Id.
     *
     *  @return the ModelId element Id
     */

    public static org.osid.id.Id getModelId() {
        return (makeElementId("osid.control.controller.ModelId"));
    }


    /**
     *  Gets the Model element Id.
     *
     *  @return the Model element Id
     */

    public static org.osid.id.Id getModel() {
        return (makeElementId("osid.control.controller.Model"));
    }


    /**
     *  Gets the Version element Id.
     *
     *  @return the Version element Id
     */

    public static org.osid.id.Id getVersion() {
        return (makeElementId("osid.control.controller.Version"));
    }


    /**
     *  Gets the VariableMinimum element Id.
     *
     *  @return the VariableMinimum element Id
     */

    public static org.osid.id.Id getVariableMinimum() {
        return (makeElementId("osid.control.controller.VariableMinimum"));
    }


    /**
     *  Gets the VariableMaximum element Id.
     *
     *  @return the VariableMaximum element Id
     */

    public static org.osid.id.Id getVariableMaximum() {
        return (makeElementId("osid.control.controller.VariableMaximum"));
    }


    /**
     *  Gets the VariableIncrement element Id.
     *
     *  @return the VariableIncrement element Id
     */

    public static org.osid.id.Id getVariableIncrement() {
        return (makeElementId("osid.control.controller.VariableIncrement"));
    }


    /**
     *  Gets the DiscreetStateIds element Id.
     *
     *  @return the DiscreetStateIds element Id
     */

    public static org.osid.id.Id getDiscreetStateIds() {
        return (makeElementId("osid.control.controller.DiscreetStateIds"));
    }


    /**
     *  Gets the DiscreetStates element Id.
     *
     *  @return the DiscreetStates element Id
     */

    public static org.osid.id.Id getDiscreetStates() {
        return (makeElementId("osid.control.controller.DiscreetStates"));
    }


    /**
     *  Gets the VersionSince element Id.
     *
     *  @return the VersionSince element Id
     */

    public static org.osid.id.Id getVersionSince() {
        return (makeQueryElementId("osid.control.controller.VersionSince"));
    }


    /**
     *  Gets the Toggleable element Id.
     *
     *  @return the Toggleable element Id
     */

    public static org.osid.id.Id getToggleable() {
        return (makeQueryElementId("osid.control.controller.Toggleable"));
    }


    /**
     *  Gets the Variable element Id.
     *
     *  @return the Variable element Id
     */

    public static org.osid.id.Id getVariable() {
        return (makeQueryElementId("osid.control.controller.Variable"));
    }


    /**
     *  Gets the VariableByPercentage element Id.
     *
     *  @return the VariableByPercentage element Id
     */

    public static org.osid.id.Id getVariableByPercentage() {
        return (makeQueryElementId("osid.control.controller.VariableByPercentage"));
    }


    /**
     *  Gets the SystemId element Id.
     *
     *  @return the SystemId element Id
     */

    public static org.osid.id.Id getSystemId() {
        return (makeQueryElementId("osid.control.controller.SystemId"));
    }


    /**
     *  Gets the System element Id.
     *
     *  @return the System element Id
     */

    public static org.osid.id.Id getSystem() {
        return (makeQueryElementId("osid.control.controller.System"));
    }


    /**
     *  Gets the VariablePercentage element Id.
     *
     *  @return the VariablePercentage element Id
     */

    public static org.osid.id.Id getVariablePercentage() {
        return (makeElementId("osid.control.controller.VariablePercentage"));
    }
}
