//
// InlineGradebookColumnCalculationNotifier.java
//
//     A callback interface for performing gradebook column calculation
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.grading.calculation.spi;


/**
 *  A callback interface for performing gradebook column calculation
 *  notifications.
 */

public interface InlineGradebookColumnCalculationNotifier {



    /**
     *  Notifies the creation of a new gradebook column calculation.
     *
     *  @param gradebookColumnCalculationId the {@code Id} of the new 
     *         gradebook column calculation
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId);


    /**
     *  Notifies the change of an updated gradebook column calculation.
     *
     *  @param gradebookColumnCalculationId the {@code Id} of the changed
     *         gradebook column calculation
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId);


    /**
     *  Notifies the deletion of an gradebook column calculation.
     *
     *  @param gradebookColumnCalculationId the {@code Id} of the deleted
     *         gradebook column calculation
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId);

}
