//
// AbstractMutableActivity.java
//
//     Defines a mutable Activity.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activity.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Activity</code>.
 */

public abstract class AbstractMutableActivity
    extends net.okapia.osid.jamocha.course.activity.spi.AbstractActivity
    implements org.osid.course.Activity,
               net.okapia.osid.jamocha.builder.course.activity.ActivityMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this activity. 
     *
     *  @param record activity record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addActivityRecord(org.osid.course.records.ActivityRecord record, org.osid.type.Type recordType) {
        super.addActivityRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }



    /**
     *  Sets the start date for when this activity is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this activity ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this activity.
     *
     *  @param displayName the name for this activity
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this activity.
     *
     *  @param description the description of this activity
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this activity
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the activity unit.
     *
     *  @param activityUnit an activity unit
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnit</code> is <code>null</code>
     */

    @Override
    public void setActivityUnit(org.osid.course.ActivityUnit activityUnit) {
        super.setActivityUnit(activityUnit);
        return;
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering the course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    @Override
    public void setCourseOffering(org.osid.course.CourseOffering courseOffering) {
        super.setCourseOffering(courseOffering);
        return;
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException
     *          <code>term</code> is <code>null</code>
     */

    @Override
    public void setTerm(org.osid.course.Term term) {
        super.setTerm(term);
        return;
    }


    /**
     *  Sets this activity as implicit.
     *
     *  @param implicit <code> true </code> if this is an implicit
     *         activity, <code> false </code> if an explicit activity
     */

    @Override
    public void setImplicit(boolean implicit) {
        super.setImplicit(implicit);
        return;
    }


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @throws org.osid.NullArgumentException
     *          <code>schedule</code> is <code>null</code>
     */

    @Override
    public void addSchedule(org.osid.calendaring.Schedule schedule) {
        super.addSchedule(schedule);
        return;
    }


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @throws org.osid.NullArgumentException
     *          <code>schedules</code> is <code>null</code>
     */

    @Override
    public void setSchedules(java.util.Collection<org.osid.calendaring.Schedule> schedules) {
        super.setSchedules(schedules);
        return;
    }


    /**
     *  Adds a superseding activity.
     *
     *  @param activity a superseding activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    @Override
    public void addSupersedingActivity(org.osid.course.Activity activity) {
        super.addSupersedingActivity(activity);
        return;
    }


    /**
     *  Sets all the superseding activities.
     *
     *  @param activities a collection of superseding activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    @Override
    public void setSupersedingActivities(java.util.Collection<org.osid.course.Activity> activities) {
        super.setSupersedingActivities(activities);
        return;
    }


    /**
     *  Adds a specific meeting time.
     *
     *  @param time a specific meeting time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void addSpecificMeetingTime(org.osid.calendaring.MeetingTime time) {
        super.addSpecificMeetingTime(time);
        return;
    }


    /**
     *  Sets all the specific meeting times.
     *
     *  @param times a collection of specific meeting times
     *  @throws org.osid.NullArgumentException <code>times</code> is
     *          <code>null</code>
     */

    @Override
    public void setSpecificMeetingTimes(java.util.Collection<org.osid.calendaring.MeetingTime> times) {
        super.setSpecificMeetingTimes(times);
        return;
    }


    /**
     *  Adds a blackout.
     *
     *  @param blackout a blackout
     *  @throws org.osid.NullArgumentException <code>blackout</code>
     *          is <code>null</code>
     */

    @Override
    public void addBlackout(org.osid.calendaring.DateTimeInterval blackout) {
        super.addBlackout(blackout);
        return;
    }


    /**
     *  Sets all the blackouts.
     *
     *  @param blackouts a collection of blackouts
     *  @throws org.osid.NullArgumentException <code>blackouts</code>
     *          is <code>null</code>
     */

    @Override
    public void setBlackouts(java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts) {
        super.setBlackouts(blackouts);
        return;
    }


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @throws org.osid.NullArgumentException <code>instructor</code>
     *          is <code>null</code>
     */

    @Override
    public void addInstructor(org.osid.resource.Resource instructor) {
        super.addInstructor(instructor);
        return;
    }


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    @Override
    public void setInstructors(java.util.Collection<org.osid.resource.Resource> instructors) {
        super.setInstructors(instructors);
        return;
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code> is
     *          negative
     */

    @Override
    public void setMinimumSeats(long seats) {
        super.setMinimumSeats(seats);
        return;
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code> is
     *          negative
     */

    @Override
    public void setMaximumSeats(long seats) {
        super.setMaximumSeats(seats);
        return;
    }


    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setTotalTargetEffort(org.osid.calendaring.Duration effort) {
        super.setTotalTargetEffort(effort);
        return;
    }


    /**
     *  Sets the contact.
     *
     *  @param contact <code> true </code> if this is a contact
     *         activity, <code> false </code> if an independent
     *         activity
     */

    @Override
    public void setContact(boolean contact) {
        super.setContact(contact);
        return;
    }


    /**
     *  Sets the total target contact time.
     *
     *  @param contactTime a total target contact time
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    @Override
    public void setTotalTargetContactTime(org.osid.calendaring.Duration contactTime) {
        super.setTotalTargetContactTime(contactTime);
        return;
    }


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setTotalTargetIndividualEffort(org.osid.calendaring.Duration effort) {
        super.setTotalTargetIndividualEffort(effort);
        return;
    }


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setWeeklyEffort(org.osid.calendaring.Duration effort) {
        super.setWeeklyEffort(effort);
        return;
    }


    /**
     *  Sets the weekly contact time.
     *
     *  @param contactTime a weekly contact time
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    @Override
    public void setWeeklyContactTime(org.osid.calendaring.Duration contactTime) {
        super.setWeeklyContactTime(contactTime);
        return;
    }


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setWeeklyIndividualEffort(org.osid.calendaring.Duration effort) {
        super.setWeeklyIndividualEffort(effort);
        return;
    }
}

