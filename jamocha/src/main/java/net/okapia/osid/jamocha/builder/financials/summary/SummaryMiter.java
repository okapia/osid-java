//
// SummaryMiter.java
//
//     Defines a Summary miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.summary;


/**
 *  Defines a <code>Summary</code> miter for use with the builders.
 */

public interface SummaryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidCompendiumMiter,
            org.osid.financials.Summary {


    /**
     *  Sets the account.
     *
     *  @param account the account
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    public void setAccount(org.osid.financials.Account account);


    /**
     *  Sets the fiscal period.
     *
     *  @param period the fiscal period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public void setFiscalPeriod(org.osid.financials.FiscalPeriod period);


    /**
     *  Sets the credits.
     *
     *  @param credits the credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public void setCredits(org.osid.financials.Currency credits);


    /**
     *  Sets the debits.
     *
     *  @param debits the debits
     *  @throws org.osid.NullArgumentException <code>debits</code> is
     *          <code>null</code>
     */

    public void setDebits(org.osid.financials.Currency debits);


    /**
     *  Sets the budget.
     *
     *  @param budget the budget
     *  @throws org.osid.NullArgumentException <code>budget</code> is
     *          <code>null</code>
     */

    public void setBudget(org.osid.financials.Currency budget);


    /**
     *  Sets the forecast.
     *
     *  @param forecast the forecast
     *  @throws org.osid.NullArgumentException <code>forecast</code> is
     *          <code>null</code>
     */

    public void setForecast(org.osid.financials.Currency forecast);


    /**
     *  Adds a Summary record.
     *
     *  @param record a summary record
     *  @param recordType the type of summary record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSummaryRecord(org.osid.financials.records.SummaryRecord record, org.osid.type.Type recordType);
}       


