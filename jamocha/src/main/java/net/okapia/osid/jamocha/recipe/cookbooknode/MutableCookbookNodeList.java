//
// MutableCookbookNodeList.java
//
//     Implements a CookbookNodeList. This list allows CookbookNodes to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.cookbooknode;


/**
 *  <p>Implements a CookbookNodeList. This list allows CookbookNodes to be
 *  added after this cookbookNode has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this cookbookNode must
 *  invoke <code>eol()</code> when there are no more cookbookNodes to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>CookbookNodeList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more cookbookNodes are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more cookbookNodes to be added.</p>
 */

public final class MutableCookbookNodeList
    extends net.okapia.osid.jamocha.recipe.cookbooknode.spi.AbstractMutableCookbookNodeList
    implements org.osid.recipe.CookbookNodeList {


    /**
     *  Creates a new empty <code>MutableCookbookNodeList</code>.
     */

    public MutableCookbookNodeList() {
        super();
    }


    /**
     *  Creates a new <code>MutableCookbookNodeList</code>.
     *
     *  @param cookbookNode a <code>CookbookNode</code>
     *  @throws org.osid.NullArgumentException <code>cookbookNode</code>
     *          is <code>null</code>
     */

    public MutableCookbookNodeList(org.osid.recipe.CookbookNode cookbookNode) {
        super(cookbookNode);
        return;
    }


    /**
     *  Creates a new <code>MutableCookbookNodeList</code>.
     *
     *  @param array an array of cookbooknodes
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableCookbookNodeList(org.osid.recipe.CookbookNode[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableCookbookNodeList</code>.
     *
     *  @param collection a java.util.Collection of cookbooknodes
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableCookbookNodeList(java.util.Collection<org.osid.recipe.CookbookNode> collection) {
        super(collection);
        return;
    }
}
