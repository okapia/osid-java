//
// AbstractAssemblyRelevancyEnablerQuery.java
//
//     A RelevancyEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ontology.rules.relevancyenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RelevancyEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyRelevancyEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.ontology.rules.RelevancyEnablerQuery,
               org.osid.ontology.rules.RelevancyEnablerQueryInspector,
               org.osid.ontology.rules.RelevancyEnablerSearchOrder {

    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRelevancyEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRelevancyEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the relevancy. 
     *
     *  @param  relevancyId the relevancy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relevancyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRelevancyId(org.osid.id.Id relevancyId, 
                                      boolean match) {
        getAssembler().addIdTerm(getRuledRelevancyIdColumn(), relevancyId, match);
        return;
    }


    /**
     *  Clears the relevancy <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRelevancyIdTerms() {
        getAssembler().clearTerms(getRuledRelevancyIdColumn());
        return;
    }


    /**
     *  Gets the relevancy <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRelevancyIdTerms() {
        return (getAssembler().getIdTerms(getRuledRelevancyIdColumn()));
    }


    /**
     *  Gets the RuledRelevancyId column name.
     *
     * @return the column name
     */

    protected String getRuledRelevancyIdColumn() {
        return ("ruled_relevancy_id");
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available. 
     *
     *  @return <code> true </code> if a relevancy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relevancy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the relevancy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getRuledRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRelevancyQuery() is false");
    }


    /**
     *  Matches enablers mapped to any relevancy. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          relevancy, <code> false </code> to match enablers mapped to no 
     *          relevancy 
     */

    @OSID @Override
    public void matchAnyRuledRelevancy(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledRelevancyColumn(), match);
        return;
    }


    /**
     *  Clears the relevancy query terms. 
     */

    @OSID @Override
    public void clearRuledRelevancyTerms() {
        getAssembler().clearTerms(getRuledRelevancyColumn());
        return;
    }


    /**
     *  Gets the relevancy query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getRuledRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Gets the RuledRelevancy column name.
     *
     * @return the column name
     */

    protected String getRuledRelevancyColumn() {
        return ("ruled_relevancy");
    }


    /**
     *  Matches enablers mapped to the ontology. 
     *
     *  @param  ontologyId the ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOntologyId(org.osid.id.Id ontologyId, boolean match) {
        getAssembler().addIdTerm(getOntologyIdColumn(), ontologyId, match);
        return;
    }


    /**
     *  Clears the ontology <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOntologyIdTerms() {
        getAssembler().clearTerms(getOntologyIdColumn());
        return;
    }


    /**
     *  Gets the ontology <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOntologyIdTerms() {
        return (getAssembler().getIdTerms(getOntologyIdColumn()));
    }


    /**
     *  Gets the OntologyId column name.
     *
     * @return the column name
     */

    protected String getOntologyIdColumn() {
        return ("ontology_id");
    }


    /**
     *  Tests if a <code> OntologyQuery </code> is available. 
     *
     *  @return <code> true </code> if a ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsOntologyQuery() is false");
    }


    /**
     *  Clears the ontology query terms. 
     */

    @OSID @Override
    public void clearOntologyTerms() {
        getAssembler().clearTerms(getOntologyColumn());
        return;
    }


    /**
     *  Gets the ontology query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }


    /**
     *  Gets the Ontology column name.
     *
     * @return the column name
     */

    protected String getOntologyColumn() {
        return ("ontology");
    }


    /**
     *  Tests if this relevancyEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  relevancyEnablerRecordType a relevancy enabler record type 
     *  @return <code>true</code> if the relevancyEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type relevancyEnablerRecordType) {
        for (org.osid.ontology.rules.records.RelevancyEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  relevancyEnablerRecordType the relevancy enabler record type 
     *  @return the relevancy enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerQueryRecord getRelevancyEnablerQueryRecord(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.rules.records.RelevancyEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  relevancyEnablerRecordType the relevancy enabler record type 
     *  @return the relevancy enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord getRelevancyEnablerQueryInspectorRecord(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param relevancyEnablerRecordType the relevancy enabler record type
     *  @return the relevancy enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerSearchOrderRecord getRelevancyEnablerSearchOrderRecord(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.rules.records.RelevancyEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this relevancy enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param relevancyEnablerQueryRecord the relevancy enabler query record
     *  @param relevancyEnablerQueryInspectorRecord the relevancy enabler query inspector
     *         record
     *  @param relevancyEnablerSearchOrderRecord the relevancy enabler search order record
     *  @param relevancyEnablerRecordType relevancy enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerQueryRecord</code>,
     *          <code>relevancyEnablerQueryInspectorRecord</code>,
     *          <code>relevancyEnablerSearchOrderRecord</code> or
     *          <code>relevancyEnablerRecordTyperelevancyEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addRelevancyEnablerRecords(org.osid.ontology.rules.records.RelevancyEnablerQueryRecord relevancyEnablerQueryRecord, 
                                      org.osid.ontology.rules.records.RelevancyEnablerQueryInspectorRecord relevancyEnablerQueryInspectorRecord, 
                                      org.osid.ontology.rules.records.RelevancyEnablerSearchOrderRecord relevancyEnablerSearchOrderRecord, 
                                      org.osid.type.Type relevancyEnablerRecordType) {

        addRecordType(relevancyEnablerRecordType);

        nullarg(relevancyEnablerQueryRecord, "relevancy enabler query record");
        nullarg(relevancyEnablerQueryInspectorRecord, "relevancy enabler query inspector record");
        nullarg(relevancyEnablerSearchOrderRecord, "relevancy enabler search odrer record");

        this.queryRecords.add(relevancyEnablerQueryRecord);
        this.queryInspectorRecords.add(relevancyEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(relevancyEnablerSearchOrderRecord);
        
        return;
    }
}
