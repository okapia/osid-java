//
// AbstractCyclicTimePeriodLookupSession.java
//
//    A starter implementation framework for providing a CyclicTimePeriod
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CyclicTimePeriod
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCyclicTimePeriods(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>CyclicTimePeriod</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCyclicTimePeriods() {
        return (true);
    }


    /**
     *  A complete view of the <code>CyclicTimePeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCyclicTimePeriodView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CyclicTimePeriod</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCyclicTimePeriodView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic time periods in calendars which are
     *  children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>CyclicTimePeriod</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CyclicTimePeriod</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CyclicTimePeriod</code> and
     *  retained for compatibility.
     *
     *  @param  cyclicTimePeriodId <code>Id</code> of the
     *          <code>CyclicTimePeriod</code>
     *  @return the cyclic time period
     *  @throws org.osid.NotFoundException <code>cyclicTimePeriodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriod getCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.cycle.CyclicTimePeriodList cyclicTimePeriods = getCyclicTimePeriods()) {
            while (cyclicTimePeriods.hasNext()) {
                org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod = cyclicTimePeriods.getNextCyclicTimePeriod();
                if (cyclicTimePeriod.getId().equals(cyclicTimePeriodId)) {
                    return (cyclicTimePeriod);
                }
            }
        } 

        throw new org.osid.NotFoundException(cyclicTimePeriodId + " not found");
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicTimePeriods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CyclicTimePeriods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCyclicTimePeriods()</code>.
     *
     *  @param  cyclicTimePeriodIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByIds(org.osid.id.IdList cyclicTimePeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.cycle.CyclicTimePeriod> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = cyclicTimePeriodIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCyclicTimePeriod(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("cyclic time period " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.LinkedCyclicTimePeriodList(ret));
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the
     *  given cyclic time period genus <code>Type</code> which does
     *  not include cyclic time periods of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known cyclic
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCyclicTimePeriods()</code>.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.cycle.cyclictimeperiod.CyclicTimePeriodGenusFilterList(getCyclicTimePeriods(), cyclicTimePeriodGenusType));
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  cyclic time period genus <code>Type</code> and include any additional
     *  cyclic time periods with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCyclicTimePeriods()</code>.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByParentGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCyclicTimePeriodsByGenusType(cyclicTimePeriodGenusType));
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> containing the given
     *  cyclic time period record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known cyclic
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCyclicTimePeriods()</code>.
     *
     *  @param  cyclicTimePeriodRecordType a cyclicTimePeriod record type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByRecordType(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.cycle.cyclictimeperiod.CyclicTimePeriodRecordFilterList(getCyclicTimePeriods(), cyclicTimePeriodRecordType));
    }


    /**
     *  Gets all <code>CyclicTimePeriods</code>. 
     *
     *  In plenary mode, the returned list contains all known cyclic
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  @return a list of <code>CyclicTimePeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the cyclic time period list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of cyclic time periods
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.cycle.CyclicTimePeriodList filterCyclicTimePeriodsOnViews(org.osid.calendaring.cycle.CyclicTimePeriodList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
