//
// AbstractAdapterStateLookupSession.java
//
//    A State lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.process.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A State lookup session adapter.
 */

public abstract class AbstractAdapterStateLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.process.StateLookupSession {

    private final org.osid.process.StateLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStateLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStateLookupSession(org.osid.process.StateLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Process/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Process Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.session.getProcessId());
    }


    /**
     *  Gets the {@code Process} associated with this session.
     *
     *  @return the {@code Process} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.Process getProcess()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProcess());
    }


    /**
     *  Tests if this user can perform {@code State} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupStates() {
        return (this.session.canLookupStates());
    }


    /**
     *  A complete view of the {@code State} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStateView() {
        this.session.useComparativeStateView();
        return;
    }


    /**
     *  A complete view of the {@code State} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStateView() {
        this.session.usePlenaryStateView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include states in processes which are children
     *  of this process in the process hierarchy.
     */

    @OSID @Override
    public void useFederatedProcessView() {
        this.session.useFederatedProcessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this process only.
     */

    @OSID @Override
    public void useIsolatedProcessView() {
        this.session.useIsolatedProcessView();
        return;
    }
    
     
    /**
     *  Gets the {@code State} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code State} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code State} and
     *  retained for compatibility.
     *
     *  @param stateId {@code Id} of the {@code State}
     *  @return the state
     *  @throws org.osid.NotFoundException {@code stateId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code stateId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.State getState(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getState(stateId));
    }


    /**
     *  Gets a {@code StateList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  states specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code States} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  stateIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code State} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code stateIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByIds(org.osid.id.IdList stateIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStatesByIds(stateIds));
    }


    /**
     *  Gets a {@code StateList} corresponding to the given
     *  state genus {@code Type} which does not include
     *  states of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned {@code State} list
     *  @throws org.osid.NullArgumentException
     *          {@code stateGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStatesByGenusType(stateGenusType));
    }


    /**
     *  Gets a {@code StateList} corresponding to the given
     *  state genus {@code Type} and include any additional
     *  states with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned {@code State} list
     *  @throws org.osid.NullArgumentException
     *          {@code stateGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByParentGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStatesByParentGenusType(stateGenusType));
    }


    /**
     *  Gets a {@code StateList} containing the given
     *  state record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateRecordType a state record type 
     *  @return the returned {@code State} list
     *  @throws org.osid.NullArgumentException
     *          {@code stateRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByRecordType(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStatesByRecordType(stateRecordType));
    }


    /**
     *  Gets all {@code States}. 
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code States} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStates());
    }


    /**
     *  Gets the next valid states for the given state. 
     *
     *  @param  stateId a state <code> Id </code> 
     *  @return the valid next <code> States </code> 
     *  @throws org.osid.NotFoundException <code> stateId </code> is not found 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.process.StateList getValidNextStates(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValidNextStates(stateId));
    }
}
