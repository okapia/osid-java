//
// AbstractResponseSearch.java
//
//     A template for making a Response Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.response.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing response searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractResponseSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inquiry.ResponseSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.ResponseSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inquiry.ResponseSearchOrder responseSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of responses. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  responseIds list of responses
     *  @throws org.osid.NullArgumentException
     *          <code>responseIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongResponses(org.osid.id.IdList responseIds) {
        while (responseIds.hasNext()) {
            try {
                this.ids.add(responseIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongResponses</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of response Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getResponseIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  responseSearchOrder response search order 
     *  @throws org.osid.NullArgumentException
     *          <code>responseSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>responseSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderResponseResults(org.osid.inquiry.ResponseSearchOrder responseSearchOrder) {
	this.responseSearchOrder = responseSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inquiry.ResponseSearchOrder getResponseSearchOrder() {
	return (this.responseSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given response search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a response implementing the requested record.
     *
     *  @param responseSearchRecordType a response search record
     *         type
     *  @return the response search record
     *  @throws org.osid.NullArgumentException
     *          <code>responseSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseSearchRecord getResponseSearchRecord(org.osid.type.Type responseSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inquiry.records.ResponseSearchRecord record : this.records) {
            if (record.implementsRecordType(responseSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this response search. 
     *
     *  @param responseSearchRecord response search record
     *  @param responseSearchRecordType response search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResponseSearchRecord(org.osid.inquiry.records.ResponseSearchRecord responseSearchRecord, 
                                           org.osid.type.Type responseSearchRecordType) {

        addRecordType(responseSearchRecordType);
        this.records.add(responseSearchRecord);        
        return;
    }
}
