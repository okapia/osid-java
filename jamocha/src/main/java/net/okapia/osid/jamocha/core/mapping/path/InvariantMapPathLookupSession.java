//
// InvariantMapPathLookupSession
//
//    Implements a Path lookup service backed by a fixed collection of
//    paths.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Path lookup service backed by a fixed
 *  collection of paths. The paths are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPathLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapPathLookupSession
    implements org.osid.mapping.path.PathLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPathLookupSession</code> with no
     *  paths.
     *  
     *  @param map the map
     *  @throws org.osid.NullArgumnetException {@code map} is
     *          {@code null}
     */

    public InvariantMapPathLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPathLookupSession</code> with a single
     *  path.
     *  
     *  @param map the map
     *  @param path a single path
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code path} is <code>null</code>
     */

      public InvariantMapPathLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.path.Path path) {
        this(map);
        putPath(path);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPathLookupSession</code> using an array
     *  of paths.
     *  
     *  @param map the map
     *  @param paths an array of paths
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code paths} is <code>null</code>
     */

      public InvariantMapPathLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.path.Path[] paths) {
        this(map);
        putPaths(paths);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPathLookupSession</code> using a
     *  collection of paths.
     *
     *  @param map the map
     *  @param paths a collection of paths
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code paths} is <code>null</code>
     */

      public InvariantMapPathLookupSession(org.osid.mapping.Map map,
                                               java.util.Collection<? extends org.osid.mapping.path.Path> paths) {
        this(map);
        putPaths(paths);
        return;
    }
}
