//
// AbstractTimePeriodSearchOdrer.java
//
//     Defines a TimePeriodSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code TimePeriodSearchOrder}.
 */

public abstract class AbstractTimePeriodSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.calendaring.TimePeriodSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.TimePeriodSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the start time. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStart(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the end time. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnd(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the time period 
     *  duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  timePeriodRecordType a time period record type 
     *  @return {@code true} if the timePeriodRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code timePeriodRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type timePeriodRecordType) {
        for (org.osid.calendaring.records.TimePeriodSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  timePeriodRecordType the time period record type 
     *  @return the time period search order record
     *  @throws org.osid.NullArgumentException
     *          {@code timePeriodRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(timePeriodRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodSearchOrderRecord getTimePeriodSearchOrderRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.TimePeriodSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this time period. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param timePeriodRecord the time period search odrer record
     *  @param timePeriodRecordType time period record type
     *  @throws org.osid.NullArgumentException
     *          {@code timePeriodRecord} or
     *          {@code timePeriodRecordTypetimePeriod} is
     *          {@code null}
     */
            
    protected void addTimePeriodRecord(org.osid.calendaring.records.TimePeriodSearchOrderRecord timePeriodSearchOrderRecord, 
                                     org.osid.type.Type timePeriodRecordType) {

        addRecordType(timePeriodRecordType);
        this.records.add(timePeriodSearchOrderRecord);
        
        return;
    }
}
