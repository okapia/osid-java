//
// AbstractCourseSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCourseSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.CourseSearchResults {

    private org.osid.course.CourseList courses;
    private final org.osid.course.CourseQueryInspector inspector;
    private final java.util.Collection<org.osid.course.records.CourseSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCourseSearchResults.
     *
     *  @param courses the result set
     *  @param courseQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>courses</code>
     *          or <code>courseQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCourseSearchResults(org.osid.course.CourseList courses,
                                            org.osid.course.CourseQueryInspector courseQueryInspector) {
        nullarg(courses, "courses");
        nullarg(courseQueryInspector, "course query inspectpr");

        this.courses = courses;
        this.inspector = courseQueryInspector;

        return;
    }


    /**
     *  Gets the course list resulting from a search.
     *
     *  @return a course list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.CourseList getCourses() {
        if (this.courses == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.CourseList courses = this.courses;
        this.courses = null;
	return (courses);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.CourseQueryInspector getCourseQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  course search record <code> Type. </code> This method must
     *  be used to retrieve a course implementing the requested
     *  record.
     *
     *  @param courseSearchRecordType a course search 
     *         record type 
     *  @return the course search
     *  @throws org.osid.NullArgumentException
     *          <code>courseSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(courseSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseSearchResultsRecord getCourseSearchResultsRecord(org.osid.type.Type courseSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.records.CourseSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(courseSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(courseSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record course search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCourseRecord(org.osid.course.records.CourseSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "course record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
