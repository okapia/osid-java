//
// AbstractNodeConfigurationHierarchySession.java
//
//     Defines a Configuration hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a configuration hierarchy session for delivering a hierarchy
 *  of configurations using the ConfigurationNode interface.
 */

public abstract class AbstractNodeConfigurationHierarchySession
    extends net.okapia.osid.jamocha.configuration.spi.AbstractConfigurationHierarchySession
    implements org.osid.configuration.ConfigurationHierarchySession {

    private java.util.Collection<org.osid.configuration.ConfigurationNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root configuration <code> Ids </code> in this hierarchy.
     *
     *  @return the root configuration <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootConfigurationIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.configuration.configurationnode.ConfigurationNodeToIdList(this.roots));
    }


    /**
     *  Gets the root configurations in the configuration hierarchy. A node
     *  with no parents is an orphan. While all configuration <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root configurations 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getRootConfigurations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.configuration.configurationnode.ConfigurationNodeToConfigurationList(new net.okapia.osid.jamocha.configuration.configurationnode.ArrayConfigurationNodeList(this.roots)));
    }


    /**
     *  Adds a root configuration node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootConfiguration(org.osid.configuration.ConfigurationNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root configuration nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootConfigurations(java.util.Collection<org.osid.configuration.ConfigurationNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root configuration node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootConfiguration(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.configuration.ConfigurationNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Configuration </code> has any parents. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @return <code> true </code> if the configuration has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentConfigurations(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getConfigurationNode(configurationId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  configuration.
     *
     *  @param  id an <code> Id </code> 
     *  @param  configurationId the <code> Id </code> of a configuration 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> configurationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> configurationId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfConfiguration(org.osid.id.Id id, org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.configuration.ConfigurationNodeList parents = getConfigurationNode(configurationId).getParentConfigurationNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextConfigurationNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given configuration. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @return the parent <code> Ids </code> of the configuration 
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentConfigurationIds(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.configuration.configuration.ConfigurationToIdList(getParentConfigurations(configurationId)));
    }


    /**
     *  Gets the parents of the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> to query 
     *  @return the parents of the configuration 
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getParentConfigurations(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.configuration.configurationnode.ConfigurationNodeToConfigurationList(getConfigurationNode(configurationId).getParentConfigurationNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  configuration.
     *
     *  @param  id an <code> Id </code> 
     *  @param  configurationId the Id of a configuration 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> configurationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfConfiguration(org.osid.id.Id id, org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfConfiguration(id, configurationId)) {
            return (true);
        }

        try (org.osid.configuration.ConfigurationList parents = getParentConfigurations(configurationId)) {
            while (parents.hasNext()) {
                if (isAncestorOfConfiguration(id, parents.getNextConfiguration().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a configuration has any children. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @return <code> true </code> if the <code> configurationId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildConfigurations(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getConfigurationNode(configurationId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  configuration.
     *
     *  @param  id an <code> Id </code> 
     *  @param configurationId the <code> Id </code> of a 
     *         configuration
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> configurationId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> configurationId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfConfiguration(org.osid.id.Id id, org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfConfiguration(configurationId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  configuration.
     *
     *  @param  configurationId the <code> Id </code> to query 
     *  @return the children of the configuration 
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildConfigurationIds(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.configuration.configuration.ConfigurationToIdList(getChildConfigurations(configurationId)));
    }


    /**
     *  Gets the children of the given configuration. 
     *
     *  @param  configurationId the <code> Id </code> to query 
     *  @return the children of the configuration 
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getChildConfigurations(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.configuration.configurationnode.ConfigurationNodeToConfigurationList(getConfigurationNode(configurationId).getChildConfigurationNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  configuration.
     *
     *  @param  id an <code> Id </code> 
     *  @param configurationId the <code> Id </code> of a 
     *         configuration
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> configurationId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfConfiguration(org.osid.id.Id id, org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfConfiguration(configurationId, id)) {
            return (true);
        }

        try (org.osid.configuration.ConfigurationList children = getChildConfigurations(configurationId)) {
            while (children.hasNext()) {
                if (isDescendantOfConfiguration(id, children.getNextConfiguration().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  configuration.
     *
     *  @param  configurationId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified configuration node 
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getConfigurationNodeIds(org.osid.id.Id configurationId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.configuration.configurationnode.ConfigurationNodeToNode(getConfigurationNode(configurationId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given configuration.
     *
     *  @param  configurationId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified configuration node 
     *  @throws org.osid.NotFoundException <code> configurationId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> configurationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationNode getConfigurationNodes(org.osid.id.Id configurationId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getConfigurationNode(configurationId));
    }


    /**
     *  Closes this <code>ConfigurationHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a configuration node.
     *
     *  @param configurationId the id of the configuration node
     *  @throws org.osid.NotFoundException <code>configurationId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>configurationId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.configuration.ConfigurationNode getConfigurationNode(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(configurationId, "configuration Id");
        for (org.osid.configuration.ConfigurationNode configuration : this.roots) {
            if (configuration.getId().equals(configurationId)) {
                return (configuration);
            }

            org.osid.configuration.ConfigurationNode r = findConfiguration(configuration, configurationId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(configurationId + " is not found");
    }


    protected org.osid.configuration.ConfigurationNode findConfiguration(org.osid.configuration.ConfigurationNode node, 
                                                            org.osid.id.Id configurationId)
	throws org.osid.OperationFailedException { 

        try (org.osid.configuration.ConfigurationNodeList children = node.getChildConfigurationNodes()) {
            while (children.hasNext()) {
                org.osid.configuration.ConfigurationNode configuration = children.getNextConfigurationNode();
                if (configuration.getId().equals(configurationId)) {
                    return (configuration);
                }
                
                configuration = findConfiguration(configuration, configurationId);
                if (configuration != null) {
                    return (configuration);
                }
            }
        }

        return (null);
    }
}
