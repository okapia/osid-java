//
// AbstractAdapterOntologyLookupSession.java
//
//    An Ontology lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Ontology lookup session adapter.
 */

public abstract class AbstractAdapterOntologyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ontology.OntologyLookupSession {

    private final org.osid.ontology.OntologyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOntologyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOntologyLookupSession(org.osid.ontology.OntologyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Ontology} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOntologies() {
        return (this.session.canLookupOntologies());
    }


    /**
     *  A complete view of the {@code Ontology} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOntologyView() {
        this.session.useComparativeOntologyView();
        return;
    }


    /**
     *  A complete view of the {@code Ontology} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOntologyView() {
        this.session.usePlenaryOntologyView();
        return;
    }

     
    /**
     *  Gets the {@code Ontology} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Ontology} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Ontology} and
     *  retained for compatibility.
     *
     *  @param ontologyId {@code Id} of the {@code Ontology}
     *  @return the ontology
     *  @throws org.osid.NotFoundException {@code ontologyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code ontologyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOntology(ontologyId));
    }


    /**
     *  Gets an {@code OntologyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ontologies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Ontologies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  ontologyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Ontology} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code ontologyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByIds(org.osid.id.IdList ontologyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOntologiesByIds(ontologyIds));
    }


    /**
     *  Gets an {@code OntologyList} corresponding to the given
     *  ontology genus {@code Type} which does not include
     *  ontologies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyGenusType an ontology genus type 
     *  @return the returned {@code Ontology} list
     *  @throws org.osid.NullArgumentException
     *          {@code ontologyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByGenusType(org.osid.type.Type ontologyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOntologiesByGenusType(ontologyGenusType));
    }


    /**
     *  Gets an {@code OntologyList} corresponding to the given
     *  ontology genus {@code Type} and include any additional
     *  ontologies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyGenusType an ontology genus type 
     *  @return the returned {@code Ontology} list
     *  @throws org.osid.NullArgumentException
     *          {@code ontologyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByParentGenusType(org.osid.type.Type ontologyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOntologiesByParentGenusType(ontologyGenusType));
    }


    /**
     *  Gets an {@code OntologyList} containing the given
     *  ontology record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyRecordType an ontology record type 
     *  @return the returned {@code Ontology} list
     *  @throws org.osid.NullArgumentException
     *          {@code ontologyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByRecordType(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOntologiesByRecordType(ontologyRecordType));
    }


    /**
     *  Gets an {@code OntologyList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Ontology} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOntologiesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Ontologies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Ontologies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOntologies());
    }
}
