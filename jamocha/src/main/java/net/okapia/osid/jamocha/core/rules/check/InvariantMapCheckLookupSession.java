//
// InvariantMapCheckLookupSession
//
//    Implements a Check lookup service backed by a fixed collection of
//    checks.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check;


/**
 *  Implements a Check lookup service backed by a fixed
 *  collection of checks. The checks are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCheckLookupSession
    extends net.okapia.osid.jamocha.core.rules.check.spi.AbstractMapCheckLookupSession
    implements org.osid.rules.check.CheckLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCheckLookupSession</code> with no
     *  checks.
     *  
     *  @param engine the engine
     *  @throws org.osid.NullArgumnetException {@code engine} is
     *          {@code null}
     */

    public InvariantMapCheckLookupSession(org.osid.rules.Engine engine) {
        setEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCheckLookupSession</code> with a single
     *  check.
     *  
     *  @param engine the engine
     *  @param check a single check
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code check} is <code>null</code>
     */

      public InvariantMapCheckLookupSession(org.osid.rules.Engine engine,
                                               org.osid.rules.check.Check check) {
        this(engine);
        putCheck(check);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCheckLookupSession</code> using an array
     *  of checks.
     *  
     *  @param engine the engine
     *  @param checks an array of checks
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code checks} is <code>null</code>
     */

      public InvariantMapCheckLookupSession(org.osid.rules.Engine engine,
                                               org.osid.rules.check.Check[] checks) {
        this(engine);
        putChecks(checks);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCheckLookupSession</code> using a
     *  collection of checks.
     *
     *  @param engine the engine
     *  @param checks a collection of checks
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code checks} is <code>null</code>
     */

      public InvariantMapCheckLookupSession(org.osid.rules.Engine engine,
                                               java.util.Collection<? extends org.osid.rules.check.Check> checks) {
        this(engine);
        putChecks(checks);
        return;
    }
}
