//
// AbstractImmutableInstallation.java
//
//     Wraps a mutable Installation to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Installation</code> to hide modifiers. This
 *  wrapper provides an immutized Installation from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying installation whose state changes are visible.
 */

public abstract class AbstractImmutableInstallation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.installation.Installation {

    private final org.osid.installation.Installation installation;


    /**
     *  Constructs a new <code>AbstractImmutableInstallation</code>.
     *
     *  @param installation the installation to immutablize
     *  @throws org.osid.NullArgumentException <code>installation</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableInstallation(org.osid.installation.Installation installation) {
        super(installation);
        this.installation = installation;
        return;
    }


    /**
     *  Gets the <code> Site Id </code> in which this installation is 
     *  installed. 
     *
     *  @return the site <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSiteId() {
        return (this.installation.getSiteId());
    }


    /**
     *  Gets the <code> Site </code> in which this installation is installed. 
     *
     *  @return the package site 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.Site getSite()
        throws org.osid.OperationFailedException {

        return (this.installation.getSite());
    }


    /**
     *  Gets the package <code> Id </code> of this installation. 
     *
     *  @return the package <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPackageId() {
        return (this.installation.getPackageId());
    }


    /**
     *  Gets the package. 
     *
     *  @return the package 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage()
        throws org.osid.OperationFailedException {

        return (this.installation.getPackage());
    }


    /**
     *  Gets the <code> Id </code> of depot from which the package was 
     *  installed. 
     *
     *  @return the depot <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDepotId() {
        return (this.installation.getDepotId());
    }


    /**
     *  Gets the depot from which the package was installed. 
     *
     *  @return the depot 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException {

        return (this.installation.getDepot());
    }


    /**
     *  Gets the date the package was installed. 
     *
     *  @return the installation date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getInstallDate() {
        return (this.installation.getInstallDate());
    }


    /**
     *  Gets the <code> Id </code> of the agent who installed this package. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.installation.getAgentId());
    }


    /**
     *  Gets the agent who installed this package. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.installation.getAgent());
    }


    /**
     *  Gets the date the installation was last checked for updates. 
     *
     *  @return the last check date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastCheckDate() {
        return (this.installation.getLastCheckDate());
    }


    /**
     *  Gets the installation record corresponding to the given <code> 
     *  Installation </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  installationRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(installationRecordType) </code> is <code> true </code> . 
     *
     *  @param  installationRecordType the type of the record to retrieve 
     *  @return the installation record 
     *  @throws org.osid.NullArgumentException <code> installationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(installationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.records.InstallationRecord getInstallationRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        return (this.installation.getInstallationRecord(installationRecordType));
    }
}

