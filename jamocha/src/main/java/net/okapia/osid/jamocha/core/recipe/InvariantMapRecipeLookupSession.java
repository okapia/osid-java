//
// InvariantMapRecipeLookupSession
//
//    Implements a Recipe lookup service backed by a fixed collection of
//    recipes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Recipe lookup service backed by a fixed
 *  collection of recipes. The recipes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRecipeLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractMapRecipeLookupSession
    implements org.osid.recipe.RecipeLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRecipeLookupSession</code> with no
     *  recipes.
     *  
     *  @param cookbook the cookbook
     *  @throws org.osid.NullArgumnetException {@code cookbook} is
     *          {@code null}
     */

    public InvariantMapRecipeLookupSession(org.osid.recipe.Cookbook cookbook) {
        setCookbook(cookbook);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecipeLookupSession</code> with a single
     *  recipe.
     *  
     *  @param cookbook the cookbook
     *  @param recipe a single recipe
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code recipe} is <code>null</code>
     */

      public InvariantMapRecipeLookupSession(org.osid.recipe.Cookbook cookbook,
                                               org.osid.recipe.Recipe recipe) {
        this(cookbook);
        putRecipe(recipe);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecipeLookupSession</code> using an array
     *  of recipes.
     *  
     *  @param cookbook the cookbook
     *  @param recipes an array of recipes
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code recipes} is <code>null</code>
     */

      public InvariantMapRecipeLookupSession(org.osid.recipe.Cookbook cookbook,
                                               org.osid.recipe.Recipe[] recipes) {
        this(cookbook);
        putRecipes(recipes);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecipeLookupSession</code> using a
     *  collection of recipes.
     *
     *  @param cookbook the cookbook
     *  @param recipes a collection of recipes
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code recipes} is <code>null</code>
     */

      public InvariantMapRecipeLookupSession(org.osid.recipe.Cookbook cookbook,
                                               java.util.Collection<? extends org.osid.recipe.Recipe> recipes) {
        this(cookbook);
        putRecipes(recipes);
        return;
    }
}
