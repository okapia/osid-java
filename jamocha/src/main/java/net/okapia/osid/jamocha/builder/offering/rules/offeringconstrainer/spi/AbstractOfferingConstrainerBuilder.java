//
// AbstractOfferingConstrainer.java
//
//     Defines an OfferingConstrainer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.rules.offeringconstrainer.spi;


/**
 *  Defines an <code>OfferingConstrainer</code> builder.
 */

public abstract class AbstractOfferingConstrainerBuilder<T extends AbstractOfferingConstrainerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidConstrainerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.offering.rules.offeringconstrainer.OfferingConstrainerMiter offeringConstrainer;


    /**
     *  Constructs a new <code>AbstractOfferingConstrainerBuilder</code>.
     *
     *  @param offeringConstrainer the offering constrainer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractOfferingConstrainerBuilder(net.okapia.osid.jamocha.builder.offering.rules.offeringconstrainer.OfferingConstrainerMiter offeringConstrainer) {
        super(offeringConstrainer);
        this.offeringConstrainer = offeringConstrainer;
        return;
    }


    /**
     *  Builds the offering constrainer.
     *
     *  @return the new offering constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>offeringConstrainer</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.offering.rules.OfferingConstrainer build() {
        (new net.okapia.osid.jamocha.builder.validator.offering.rules.offeringconstrainer.OfferingConstrainerValidator(getValidations())).validate(this.offeringConstrainer);
        return (new net.okapia.osid.jamocha.builder.offering.rules.offeringconstrainer.ImmutableOfferingConstrainer(this.offeringConstrainer));
    }


    /**
     *  Gets the offering constrainer. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new offeringConstrainer
     */

    @Override
    public net.okapia.osid.jamocha.builder.offering.rules.offeringconstrainer.OfferingConstrainerMiter getMiter() {
        return (this.offeringConstrainer);
    }


    /**
     *  Adds an OfferingConstrainer record.
     *
     *  @param record an offering constrainer record
     *  @param recordType the type of offering constrainer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.offering.rules.records.OfferingConstrainerRecord record, org.osid.type.Type recordType) {
        getMiter().addOfferingConstrainerRecord(record, recordType);
        return (self());
    }


    /**
     *  Sets the can override description.
     *
     *  @param canOverrideDescription <code>true</code> if the
     *         description can be overriden at offering,
     *         <code>false</code> otherwise
     */

    public T canOverrideDescription(boolean canOverrideDescription) {
        getMiter().setCanOverrideDescription(canOverrideDescription);
        return (self());
    }


    /**
     *  Sets the can override title.
     *
     *  @param canOverrideTitle <code>true</code> if the title can be
     *         overriden at offering, <code>false</code> otherwise
     */

    public T canOverrideTitle(boolean canOverrideTitle) {
        getMiter().setCanOverrideTitle(canOverrideTitle);
        return (self());
    }


    /**
     *  Sets the can override code.
     *
     *  @param canOverrideCode <code>true</code> if the code can be
     *         overriden at offering, <code>false</code> otherwise
     */

    public T canOverrideCode(boolean canOverrideCode) {
        getMiter().setCanOverrideCode(canOverrideCode);
        return (self());
    }


    /**
     *  Sets the can override time periods.
     *
     *  @param canOverrideTimePeriods <code>true</code> if the time
     *         periods can be overriden at offering,
     *         <code>false</code> otherwise
     */

    public T canOverrideTimePeriods(boolean canOverrideTimePeriods) {
        getMiter().setCanOverrideTimePeriods(canOverrideTimePeriods);
        return (self());
    }


    /**
     *  Sets the can constrain time periods.
     *
     *  @param canConstrainTimePeriods <code>true</code> if the time
     *         periods can be constrained at offering,
     *         <code>false</code> otherwise
     */

    public T canConstrainTimePeriods(boolean canConstrainTimePeriods) {
        getMiter().setCanConstrainTimePeriods(canConstrainTimePeriods);
        return (self());
    }


    /**
     *  Sets the can override result options.
     *
     *  @param canOverrideResultOptions <code>true</code> if the
     *         result options can be overidden at offering,
     *         <code>false</code> otherwise
     */

    public T canOverrideResultOptions(boolean canOverrideResultOptions) {
        getMiter().setCanOverrideResultOptions(canOverrideResultOptions);
        return (self());
    }


    /**
     *  Sets the can constrain result options.
     *
     *  @param canConstrainResultOptions <code>true</code> if the
     *         result options can be constrained at offering,
     *         <code>false</code> otherwise
     */

    public T canConstrainResultOptions(boolean canConstrainResultOptions) {
        getMiter().setCanConstrainResultOptions(canConstrainResultOptions);
        return (self());
    }


    /**
     *  Sets the can override sponsors.
     *
     *  @param canOverrideSponsors <code>true</code> if the sponsors
     *         can be overidden at offering, <code>false</code>
     *         otherwise
     */

    public T canOverrideSponsors(boolean canOverrideSponsors) {
        getMiter().setCanOverrideSponsors(canOverrideSponsors);
        return (self());
    }


    /**
     *  Sets the can constrain sponsors.
     *
     *  @param canConstrainSponsors <code>true</code> if the
     *         sponsors can be constrained at offering,
     *         <code>false</code> otherwise
     */

    public T canConstrainSponsors(boolean canConstrainSponsors) {
        getMiter().setCanConstrainSponsors(canConstrainSponsors);
        return (self());
    }
}       


