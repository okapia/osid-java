//
// AbstractCourseRegistrationManager.java
//
//     An adapter for a CourseRegistrationManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseRegistrationManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseRegistrationManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.course.registration.CourseRegistrationManager>
    implements org.osid.course.registration.CourseRegistrationManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseRegistrationManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseRegistrationManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseRegistrationManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseRegistrationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up activity bundles is supported. 
     *
     *  @return <code> true </code> if activity bundle lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleLookup() {
        return (getAdapteeManager().supportsActivityBundleLookup());
    }


    /**
     *  Tests if querying activity bundles is supported. 
     *
     *  @return <code> true </code> if activity bundle query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleQuery() {
        return (getAdapteeManager().supportsActivityBundleQuery());
    }


    /**
     *  Tests if searching activity bundles is supported. 
     *
     *  @return <code> true </code> if activity bundle search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleSearch() {
        return (getAdapteeManager().supportsActivityBundleSearch());
    }


    /**
     *  Tests if an activity bundle <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity bundle administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleAdmin() {
        return (getAdapteeManager().supportsActivityBundleAdmin());
    }


    /**
     *  Tests if an activity bundle <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity bundle notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleNotification() {
        return (getAdapteeManager().supportsActivityBundleNotification());
    }


    /**
     *  Tests if an activity bundle cataloging service is supported. 
     *
     *  @return <code> true </code> if activity bundle catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleCourseCatalog() {
        return (getAdapteeManager().supportsActivityBundleCourseCatalog());
    }


    /**
     *  Tests if an activity bundle cataloging service is supported. A 
     *  cataloging service maps activity bundles to catalogs. 
     *
     *  @return <code> true </code> if activity bundle cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleCourseCatalogAssignment() {
        return (getAdapteeManager().supportsActivityBundleCourseCatalogAssignment());
    }


    /**
     *  Tests if an activity bundle smart course catalog session is available. 
     *
     *  @return <code> true </code> if an activity bundle smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBundleSmartCourseCatalog() {
        return (getAdapteeManager().supportsActivityBundleSmartCourseCatalog());
    }


    /**
     *  Tests if looking up registrations is supported. 
     *
     *  @return <code> true </code> if registration lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationLookup() {
        return (getAdapteeManager().supportsRegistrationLookup());
    }


    /**
     *  Tests if querying registrations is supported. 
     *
     *  @return <code> true </code> if registration query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationQuery() {
        return (getAdapteeManager().supportsRegistrationQuery());
    }


    /**
     *  Tests if searching registrations is supported. 
     *
     *  @return <code> true </code> if registration search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationSearch() {
        return (getAdapteeManager().supportsRegistrationSearch());
    }


    /**
     *  Tests if course <code> </code> offering <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if registration administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationAdmin() {
        return (getAdapteeManager().supportsRegistrationAdmin());
    }


    /**
     *  Tests if a registration <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if registration notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationNotification() {
        return (getAdapteeManager().supportsRegistrationNotification());
    }


    /**
     *  Tests if a registration cataloging service is supported. 
     *
     *  @return <code> true </code> if registration catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationCourseCatalog() {
        return (getAdapteeManager().supportsRegistrationCourseCatalog());
    }


    /**
     *  Tests if a registration cataloging service is supported. A cataloging 
     *  service maps registrations to catalogs. 
     *
     *  @return <code> true </code> if registration cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationCourseCatalogAssignment() {
        return (getAdapteeManager().supportsRegistrationCourseCatalogAssignment());
    }


    /**
     *  Tests if a registration smart course catalog session is available. 
     *
     *  @return <code> true </code> if a registration smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationSmartCourseCatalog() {
        return (getAdapteeManager().supportsRegistrationSmartCourseCatalog());
    }


    /**
     *  Tests if looking up activity registrations is supported. 
     *
     *  @return <code> true </code> if activity registration lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationLookup() {
        return (getAdapteeManager().supportsActivityRegistrationLookup());
    }


    /**
     *  Tests if querying activity registrations is supported. 
     *
     *  @return <code> true </code> if activity registration query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationQuery() {
        return (getAdapteeManager().supportsActivityRegistrationQuery());
    }


    /**
     *  Tests if searching activity registrations is supported. 
     *
     *  @return <code> true </code> if activity registration search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationSearch() {
        return (getAdapteeManager().supportsActivityRegistrationSearch());
    }


    /**
     *  Tests if an activity registration <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if activity registration administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationAdmin() {
        return (getAdapteeManager().supportsActivityRegistrationAdmin());
    }


    /**
     *  Tests if an activity registration <code> </code> notification service 
     *  is supported. 
     *
     *  @return <code> true </code> if activity registration notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationNotification() {
        return (getAdapteeManager().supportsActivityRegistrationNotification());
    }


    /**
     *  Tests if an activity registration cataloging service is supported. 
     *
     *  @return <code> true </code> if activity registration catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationCourseCatalog() {
        return (getAdapteeManager().supportsActivityRegistrationCourseCatalog());
    }


    /**
     *  Tests if an activity registration cataloging service is supported. A 
     *  cataloging service maps activity registrations to catalogs. 
     *
     *  @return <code> true </code> if activity registration cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationCourseCatalogAssignment() {
        return (getAdapteeManager().supportsActivityRegistrationCourseCatalogAssignment());
    }


    /**
     *  Tests if an activity registration smart course catalog session is 
     *  available. 
     *
     *  @return <code> true </code> if an activity registration smart course 
     *          catalog session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationSmartCourseCatalog() {
        return (getAdapteeManager().supportsActivityRegistrationSmartCourseCatalog());
    }


    /**
     *  Tests if a course registration batch service is available. 
     *
     *  @return <code> true </code> if a course registration service session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseRegistrationBatch() {
        return (getAdapteeManager().supportsCourseRegistrationBatch());
    }


    /**
     *  Gets the supported <code> ActivityBundle </code> record types. 
     *
     *  @return a list containing the supported <code> ActivityBundle </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityBundleRecordTypes() {
        return (getAdapteeManager().getActivityBundleRecordTypes());
    }


    /**
     *  Tests if the given <code> ActivityBundle </code> record type is 
     *  supported. 
     *
     *  @param  activityBundleRecordType a <code> Type </code> indicating an 
     *          <code> ActivityBundle </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityBundleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityBundleRecordType(org.osid.type.Type activityBundleRecordType) {
        return (getAdapteeManager().supportsActivityBundleRecordType(activityBundleRecordType));
    }


    /**
     *  Gets the supported <code> ActivityBundle </code> search record types. 
     *
     *  @return a list containing the supported <code> ActivityBundle </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityBundleSearchRecordTypes() {
        return (getAdapteeManager().getActivityBundleSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ActivityBundle </code> search record type is 
     *  supported. 
     *
     *  @param  activityBundleSearchRecordType a <code> Type </code> 
     *          indicating an <code> ActivityBundle </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityBundleSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityBundleSearchRecordType(org.osid.type.Type activityBundleSearchRecordType) {
        return (getAdapteeManager().supportsActivityBundleSearchRecordType(activityBundleSearchRecordType));
    }


    /**
     *  Gets the supported <code> Registration </code> record types. 
     *
     *  @return a list containing the supported <code> Registration </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRegistrationRecordTypes() {
        return (getAdapteeManager().getRegistrationRecordTypes());
    }


    /**
     *  Tests if the given <code> Registration </code> record type is 
     *  supported. 
     *
     *  @param  registrationRecordType a <code> Type </code> indicating an 
     *          <code> Registration </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> registrationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRegistrationRecordType(org.osid.type.Type registrationRecordType) {
        return (getAdapteeManager().supportsRegistrationRecordType(registrationRecordType));
    }


    /**
     *  Gets the supported <code> Registration </code> search record types. 
     *
     *  @return a list containing the supported <code> Registration </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRegistrationSearchRecordTypes() {
        return (getAdapteeManager().getRegistrationSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Registration </code> search record type is 
     *  supported. 
     *
     *  @param  registrationSearchRecordType a <code> Type </code> indicating 
     *          an <code> Registration </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          registrationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRegistrationSearchRecordType(org.osid.type.Type registrationSearchRecordType) {
        return (getAdapteeManager().supportsRegistrationSearchRecordType(registrationSearchRecordType));
    }


    /**
     *  Gets the supported <code> ActivityRegistration </code> record types. 
     *
     *  @return a list containing the supported <code> ActivityRegistration 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRegistrationRecordTypes() {
        return (getAdapteeManager().getActivityRegistrationRecordTypes());
    }


    /**
     *  Tests if the given <code> ActivityRegistration </code> record type is 
     *  supported. 
     *
     *  @param  activityRegistrationRecordType a <code> Type </code> 
     *          indicating an <code> ActivityRegistration </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationRecordType(org.osid.type.Type activityRegistrationRecordType) {
        return (getAdapteeManager().supportsActivityRegistrationRecordType(activityRegistrationRecordType));
    }


    /**
     *  Gets the supported <code> ActivityRegistration </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> ActivityRegistration 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRegistrationSearchRecordTypes() {
        return (getAdapteeManager().getActivityRegistrationSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ActivityRegistration </code> search record 
     *  type is supported. 
     *
     *  @param  activityRegistrationSearchRecordType a <code> Type </code> 
     *          indicating an <code> ActivityRegistration </code> search 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsActivityRegistrationSearchRecordType(org.osid.type.Type activityRegistrationSearchRecordType) {
        return (getAdapteeManager().supportsActivityRegistrationSearchRecordType(activityRegistrationSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle lookup service. 
     *
     *  @return an <code> ActivityBundleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleLookupSession getActivityBundleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityBundleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleLookupSession getActivityBundleLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle query service. 
     *
     *  @return an <code> ActivityBundleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuerySession getActivityBundleQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleQuerySession getActivityBundleQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle search service. 
     *
     *  @return an <code> ActivityBundleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSearchSession getActivityBundleSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSearchSession getActivityBundleSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle administration service. 
     *
     *  @return an <code> ActivityBundleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleAdminSession getActivityBundleAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleAdminSession getActivityBundleAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle notification service. 
     *
     *  @param  activityBundleReceiver the notification callback 
     *  @return an <code> ActivityBundleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityBundleReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleNotificationSession getActivityBundleNotificationSession(org.osid.course.registration.ActivityBundleReceiver activityBundleReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleNotificationSession(activityBundleReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle notification service for the given course catalog. 
     *
     *  @param  activityBundleReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityBundleReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleNotificationSession getActivityBundleNotificationSessionForCourseCatalog(org.osid.course.registration.ActivityBundleReceiver activityBundleReceiver, 
                                                                                                                               org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleNotificationSessionForCourseCatalog(activityBundleReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity bundle/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityBundleCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleCourseCatalogSession getActivityBundleCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  bundles to course catalogs. 
     *
     *  @return an <code> ActivityBundleCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleCourseCatalogAssignmentSession getActivityBundleCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  bundle smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBundleSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBundleSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleSmartCourseCatalogSession getActivityBundleSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBundleSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  lookup service. 
     *
     *  @return a <code> RegistrationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationLookupSession getRegistrationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> RegistrationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationLookupSession getRegistrationLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  query service. 
     *
     *  @return a <code> RegistrationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuerySession getRegistrationQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuerySession getRegistrationQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  search service. 
     *
     *  @return a <code> RegistrationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchSession getRegistrationSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchSession getRegistrationSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  administration service. 
     *
     *  @return a <code> RegistrationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationAdminSession getRegistrationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationAdminSession getRegistrationAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  notification service. 
     *
     *  @param  registrationReceiver the notification callback 
     *  @return a <code> RegistrationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> registrationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationNotificationSession getRegistrationNotificationSession(org.osid.course.registration.RegistrationReceiver registrationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationNotificationSession(registrationReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  notification service for the given course catalog. 
     *
     *  @param  registrationReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> registrationReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationNotificationSession getRegistrationNotificationSessionForCourseCatalog(org.osid.course.registration.RegistrationReceiver registrationReceiver, 
                                                                                                                           org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationNotificationSessionForCourseCatalog(registrationReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup registration/catalog 
     *  mappings. 
     *
     *  @return a <code> RegistrationCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationCourseCatalogSession getRegistrationCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  registrations to course catalogs. 
     *
     *  @return a <code> RegistrationCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationCourseCatalogAssignmentSession getRegistrationCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the registration 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RegistrationSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSmartCourseCatalogSession getRegistrationSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRegistrationSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration lookup service. 
     *
     *  @return an <code> ActivityRegistrationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationLookupSession getActivityRegistrationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityRegistrationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationLookupSession getActivityRegistrationLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration query service. 
     *
     *  @return an <code> ActivityRegistrationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationQuerySession getActivityRegistrationQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationQuerySession getActivityRegistrationQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration search service. 
     *
     *  @return an <code> ActivityRegistrationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSearchSession getActivityRegistrationSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSearchSession getActivityRegistrationSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration administration service. 
     *
     *  @return an <code> ActivityRegistrationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationAdminSession getActivityRegistrationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationAdminSession getActivityRegistrationAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration notification service. 
     *
     *  @param  activityRegistrationReceiver the notification callback 
     *  @return an <code> ActivityRegistrationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationNotificationSession getActivityRegistrationNotificationSession(org.osid.course.registration.ActivityRegistrationReceiver activityRegistrationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationNotificationSession(activityRegistrationReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration notification service for the given course catalog. 
     *
     *  @param  activityRegistrationReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationReceiver </code> or <code> courseCatalogId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationNotificationSession getActivityRegistrationNotificationSessionForCourseCatalog(org.osid.course.registration.ActivityRegistrationReceiver activityRegistrationReceiver, 
                                                                                                                                           org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationNotificationSessionForCourseCatalog(activityRegistrationReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity 
     *  registration/catalog mappings. 
     *
     *  @return an <code> ActivityRegistrationCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationCourseCatalog() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationCourseCatalogSession getActivityRegistrationCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  registrations to course catalogs. 
     *
     *  @return an <code> ActivityRegistrationCourseCatalogAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationCourseCatalogAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationCourseCatalogAssignmentSession getActivityRegistrationCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  registration smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityRegistrationSmartCourseCatalogSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityRegistrationSmartCourseCatalog() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationSmartCourseCatalogSession getActivityRegistrationSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityRegistrationSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets a <code> CourseRegistrationBatchManager. </code> 
     *
     *  @return a <code> CourseRegistrationBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRegistrationBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.batch.CourseRegistrationBatchManager getCourseRegistrationBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseRegistrationBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
