//
// AbstractMapRecurringEventEnablerLookupSession
//
//    A simple framework for providing a RecurringEventEnabler lookup service
//    backed by a fixed collection of recurring event enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RecurringEventEnabler lookup service backed by a
 *  fixed collection of recurring event enablers. The recurring event enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RecurringEventEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRecurringEventEnablerLookupSession
    extends net.okapia.osid.jamocha.calendaring.rules.spi.AbstractRecurringEventEnablerLookupSession
    implements org.osid.calendaring.rules.RecurringEventEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.rules.RecurringEventEnabler> recurringEventEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.rules.RecurringEventEnabler>());


    /**
     *  Makes a <code>RecurringEventEnabler</code> available in this session.
     *
     *  @param  recurringEventEnabler a recurring event enabler
     *  @throws org.osid.NullArgumentException <code>recurringEventEnabler<code>
     *          is <code>null</code>
     */

    protected void putRecurringEventEnabler(org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler) {
        this.recurringEventEnablers.put(recurringEventEnabler.getId(), recurringEventEnabler);
        return;
    }


    /**
     *  Makes an array of recurring event enablers available in this session.
     *
     *  @param  recurringEventEnablers an array of recurring event enablers
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablers<code>
     *          is <code>null</code>
     */

    protected void putRecurringEventEnablers(org.osid.calendaring.rules.RecurringEventEnabler[] recurringEventEnablers) {
        putRecurringEventEnablers(java.util.Arrays.asList(recurringEventEnablers));
        return;
    }


    /**
     *  Makes a collection of recurring event enablers available in this session.
     *
     *  @param  recurringEventEnablers a collection of recurring event enablers
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablers<code>
     *          is <code>null</code>
     */

    protected void putRecurringEventEnablers(java.util.Collection<? extends org.osid.calendaring.rules.RecurringEventEnabler> recurringEventEnablers) {
        for (org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler : recurringEventEnablers) {
            this.recurringEventEnablers.put(recurringEventEnabler.getId(), recurringEventEnabler);
        }

        return;
    }


    /**
     *  Removes a RecurringEventEnabler from this session.
     *
     *  @param  recurringEventEnablerId the <code>Id</code> of the recurring event enabler
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId) {
        this.recurringEventEnablers.remove(recurringEventEnablerId);
        return;
    }


    /**
     *  Gets the <code>RecurringEventEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  recurringEventEnablerId <code>Id</code> of the <code>RecurringEventEnabler</code>
     *  @return the recurringEventEnabler
     *  @throws org.osid.NotFoundException <code>recurringEventEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnabler getRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler = this.recurringEventEnablers.get(recurringEventEnablerId);
        if (recurringEventEnabler == null) {
            throw new org.osid.NotFoundException("recurringEventEnabler not found: " + recurringEventEnablerId);
        }

        return (recurringEventEnabler);
    }


    /**
     *  Gets all <code>RecurringEventEnablers</code>. In plenary mode, the returned
     *  list contains all known recurringEventEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  recurringEventEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RecurringEventEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.ArrayRecurringEventEnablerList(this.recurringEventEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.recurringEventEnablers.clear();
        super.close();
        return;
    }
}
