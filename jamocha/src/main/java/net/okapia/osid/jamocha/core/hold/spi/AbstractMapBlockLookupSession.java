//
// AbstractMapBlockLookupSession
//
//    A simple framework for providing a Block lookup service
//    backed by a fixed collection of blocks.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Block lookup service backed by a
 *  fixed collection of blocks. The blocks are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Blocks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBlockLookupSession
    extends net.okapia.osid.jamocha.hold.spi.AbstractBlockLookupSession
    implements org.osid.hold.BlockLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.hold.Block> blocks = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.hold.Block>());


    /**
     *  Makes a <code>Block</code> available in this session.
     *
     *  @param  block a block
     *  @throws org.osid.NullArgumentException <code>block<code>
     *          is <code>null</code>
     */

    protected void putBlock(org.osid.hold.Block block) {
        this.blocks.put(block.getId(), block);
        return;
    }


    /**
     *  Makes an array of blocks available in this session.
     *
     *  @param  blocks an array of blocks
     *  @throws org.osid.NullArgumentException <code>blocks<code>
     *          is <code>null</code>
     */

    protected void putBlocks(org.osid.hold.Block[] blocks) {
        putBlocks(java.util.Arrays.asList(blocks));
        return;
    }


    /**
     *  Makes a collection of blocks available in this session.
     *
     *  @param  blocks a collection of blocks
     *  @throws org.osid.NullArgumentException <code>blocks<code>
     *          is <code>null</code>
     */

    protected void putBlocks(java.util.Collection<? extends org.osid.hold.Block> blocks) {
        for (org.osid.hold.Block block : blocks) {
            this.blocks.put(block.getId(), block);
        }

        return;
    }


    /**
     *  Removes a Block from this session.
     *
     *  @param  blockId the <code>Id</code> of the block
     *  @throws org.osid.NullArgumentException <code>blockId<code> is
     *          <code>null</code>
     */

    protected void removeBlock(org.osid.id.Id blockId) {
        this.blocks.remove(blockId);
        return;
    }


    /**
     *  Gets the <code>Block</code> specified by its <code>Id</code>.
     *
     *  @param  blockId <code>Id</code> of the <code>Block</code>
     *  @return the block
     *  @throws org.osid.NotFoundException <code>blockId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>blockId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Block getBlock(org.osid.id.Id blockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.hold.Block block = this.blocks.get(blockId);
        if (block == null) {
            throw new org.osid.NotFoundException("block not found: " + blockId);
        }

        return (block);
    }


    /**
     *  Gets all <code>Blocks</code>. In plenary mode, the returned
     *  list contains all known blocks or an error
     *  results. Otherwise, the returned list may contain only those
     *  blocks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Blocks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.block.ArrayBlockList(this.blocks.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.blocks.clear();
        super.close();
        return;
    }
}
