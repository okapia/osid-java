//
// AbstractAssemblyAwardQuery.java
//
//     An AwardQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recognition.award.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AwardQuery that stores terms.
 */

public abstract class AbstractAssemblyAwardQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.recognition.AwardQuery,
               org.osid.recognition.AwardQueryInspector,
               org.osid.recognition.AwardSearchOrder {

    private final java.util.Collection<org.osid.recognition.records.AwardQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.AwardQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.AwardSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAwardQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAwardQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the conferral <code> Id </code> for this query to match 
     *  conferrals assigned to awards. 
     *
     *  @param  conferralId a conferral <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> conferralId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConferralId(org.osid.id.Id conferralId, boolean match) {
        getAssembler().addIdTerm(getConferralIdColumn(), conferralId, match);
        return;
    }


    /**
     *  Clears the conferral <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConferralIdTerms() {
        getAssembler().clearTerms(getConferralIdColumn());
        return;
    }


    /**
     *  Gets the conferral <code> Id </code> terms. 
     *
     *  @return the conferral <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConferralIdTerms() {
        return (getAssembler().getIdTerms(getConferralIdColumn()));
    }


    /**
     *  Gets the ConferralId column name.
     *
     * @return the column name
     */

    protected String getConferralIdColumn() {
        return ("conferral_id");
    }


    /**
     *  Tests if a conferral query is available. 
     *
     *  @return <code> true </code> if a conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award. 
     *
     *  @return the conferral query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuery getConferralQuery() {
        throw new org.osid.UnimplementedException("supportsConferralQuery() is false");
    }


    /**
     *  Matches awards with any conferral. 
     *
     *  @param  match <code> true </code> to match awards with any conferral, 
     *          <code> false </code> to match awards with no conferrals 
     */

    @OSID @Override
    public void matchAnyConferral(boolean match) {
        getAssembler().addIdWildcardTerm(getConferralColumn(), match);
        return;
    }


    /**
     *  Clears the conferral terms. 
     */

    @OSID @Override
    public void clearConferralTerms() {
        getAssembler().clearTerms(getConferralColumn());
        return;
    }


    /**
     *  Gets the conferral terms. 
     *
     *  @return the conferral terms 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQueryInspector[] getConferralTerms() {
        return (new org.osid.recognition.ConferralQueryInspector[0]);
    }


    /**
     *  Gets the Conferral column name.
     *
     * @return the column name
     */

    protected String getConferralColumn() {
        return ("conferral");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match conferrals 
     *  assigned to academies. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAcademyId(org.osid.id.Id academyId, boolean match) {
        getAssembler().addIdTerm(getAcademyIdColumn(), academyId, match);
        return;
    }


    /**
     *  Clears the academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAcademyIdTerms() {
        getAssembler().clearTerms(getAcademyIdColumn());
        return;
    }


    /**
     *  Gets the academy <code> Id </code> terms. 
     *
     *  @return the academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAcademyIdTerms() {
        return (getAssembler().getIdTerms(getAcademyIdColumn()));
    }


    /**
     *  Gets the AcademyId column name.
     *
     * @return the column name
     */

    protected String getAcademyIdColumn() {
        return ("academy_id");
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAcademyQuery() is false");
    }


    /**
     *  Clears the academy terms. 
     */

    @OSID @Override
    public void clearAcademyTerms() {
        getAssembler().clearTerms(getAcademyColumn());
        return;
    }


    /**
     *  Gets the academy terms. 
     *
     *  @return the academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }


    /**
     *  Gets the Academy column name.
     *
     * @return the column name
     */

    protected String getAcademyColumn() {
        return ("academy");
    }


    /**
     *  Tests if this award supports the given record
     *  <code>Type</code>.
     *
     *  @param  awardRecordType an award record type 
     *  @return <code>true</code> if the awardRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type awardRecordType) {
        for (org.osid.recognition.records.AwardQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(awardRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  awardRecordType the award record type 
     *  @return the award query record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AwardQueryRecord getAwardQueryRecord(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AwardQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(awardRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  awardRecordType the award record type 
     *  @return the award query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AwardQueryInspectorRecord getAwardQueryInspectorRecord(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AwardQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(awardRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param awardRecordType the award record type
     *  @return the award search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AwardSearchOrderRecord getAwardSearchOrderRecord(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AwardSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(awardRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this award. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param awardQueryRecord the award query record
     *  @param awardQueryInspectorRecord the award query inspector
     *         record
     *  @param awardSearchOrderRecord the award search order record
     *  @param awardRecordType award record type
     *  @throws org.osid.NullArgumentException
     *          <code>awardQueryRecord</code>,
     *          <code>awardQueryInspectorRecord</code>,
     *          <code>awardSearchOrderRecord</code> or
     *          <code>awardRecordTypeaward</code> is
     *          <code>null</code>
     */
            
    protected void addAwardRecords(org.osid.recognition.records.AwardQueryRecord awardQueryRecord, 
                                      org.osid.recognition.records.AwardQueryInspectorRecord awardQueryInspectorRecord, 
                                      org.osid.recognition.records.AwardSearchOrderRecord awardSearchOrderRecord, 
                                      org.osid.type.Type awardRecordType) {

        addRecordType(awardRecordType);

        nullarg(awardQueryRecord, "award query record");
        nullarg(awardQueryInspectorRecord, "award query inspector record");
        nullarg(awardSearchOrderRecord, "award search odrer record");

        this.queryRecords.add(awardQueryRecord);
        this.queryInspectorRecords.add(awardQueryInspectorRecord);
        this.searchOrderRecords.add(awardSearchOrderRecord);
        
        return;
    }
}
