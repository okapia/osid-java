//
// AbstractAdapterCompositionLookupSession.java
//
//    A Composition lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.repository.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Composition lookup session adapter.
 */

public abstract class AbstractAdapterCompositionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.repository.CompositionLookupSession {

    private final org.osid.repository.CompositionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCompositionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCompositionLookupSession(org.osid.repository.CompositionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Repository/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Repository Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.session.getRepositoryId());
    }


    /**
     *  Gets the {@code Repository} associated with this session.
     *
     *  @return the {@code Repository} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRepository());
    }


    /**
     *  Tests if this user can perform {@code Composition} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCompositions() {
        return (this.session.canLookupCompositions());
    }


    /**
     *  A complete view of the {@code Composition} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCompositionView() {
        this.session.useComparativeCompositionView();
        return;
    }


    /**
     *  A complete view of the {@code Composition} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCompositionView() {
        this.session.usePlenaryCompositionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include compositions in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.session.useFederatedRepositoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.session.useIsolatedRepositoryView();
        return;
    }
    

    /**
     *  Only active compositions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCompositionView() {
        this.session.useActiveCompositionView();
        return;
    }


    /**
     *  Active and inactive compositions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCompositionView() {
        this.session.useAnyStatusCompositionView();
        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  compositions.
     */

    @OSID @Override
    public void useSequesteredCompositionView() {
        this.session.useSequesteredCompositionView();
        return;
    }


    /**
     *  All compositions are returned including sequestered
     *  compositions.
     */

    @OSID @Override
    public void useUnsequesteredCompositionView() {
        this.session.useUnsequesteredCompositionView();
        return;
    }

     
    /**
     *  Gets the {@code Composition} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Composition} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Composition} and
     *  retained for compatibility.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param compositionId {@code Id} of the {@code Composition}
     *  @return the composition
     *  @throws org.osid.NotFoundException {@code compositionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code compositionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Composition getComposition(org.osid.id.Id compositionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getComposition(compositionId));
    }


    /**
     *  Gets a {@code CompositionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  compositions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Compositions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Composition} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code compositionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByIds(org.osid.id.IdList compositionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionsByIds(compositionIds));
    }


    /**
     *  Gets a {@code CompositionList} corresponding to the given
     *  composition genus {@code Type} which does not include
     *  compositions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionGenusType a composition genus type 
     *  @return the returned {@code Composition} list
     *  @throws org.osid.NullArgumentException
     *          {@code compositionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByGenusType(org.osid.type.Type compositionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionsByGenusType(compositionGenusType));
    }


    /**
     *  Gets a {@code CompositionList} corresponding to the given
     *  composition genus {@code Type} and include any additional
     *  compositions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionGenusType a composition genus type 
     *  @return the returned {@code Composition} list
     *  @throws org.osid.NullArgumentException
     *          {@code compositionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByParentGenusType(org.osid.type.Type compositionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionsByParentGenusType(compositionGenusType));
    }


    /**
     *  Gets a {@code CompositionList} containing the given
     *  composition record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionRecordType a composition record type 
     *  @return the returned {@code Composition} list
     *  @throws org.osid.NullArgumentException
     *          {@code compositionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByRecordType(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionsByRecordType(compositionRecordType));
    }


    /**
     *  Gets a {@code CompositionList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Composition} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Compositions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @return a list of {@code Compositions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositions());
    }
}
