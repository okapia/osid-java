//
// AbstractHoldProxyManager.java
//
//     An adapter for a HoldProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a HoldProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterHoldProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.hold.HoldProxyManager>
    implements org.osid.hold.HoldProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterHoldProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterHoldProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterHoldProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterHoldProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any oubliette federation is exposed. Federation is exposed 
     *  when a specific oubliette may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of oubliettes appears as a single oubliette. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a blocking service is supported for the current agent. 
     *
     *  @return <code> true </code> if blockiings service is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlocking() {
        return (getAdapteeManager().supportsBlocking());
    }


    /**
     *  Tests if a my hold service is supported for the current agent. 
     *
     *  @return <code> true </code> if my hold service is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyHold() {
        return (getAdapteeManager().supportsMyHold());
    }


    /**
     *  Tests if looking up blocks is supported. 
     *
     *  @return <code> true </code> if block lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockLookup() {
        return (getAdapteeManager().supportsBlockLookup());
    }


    /**
     *  Tests if querying blocks is supported. 
     *
     *  @return <code> true </code> if block query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockQuery() {
        return (getAdapteeManager().supportsBlockQuery());
    }


    /**
     *  Tests if searching blocks is supported. 
     *
     *  @return <code> true </code> if block search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockSearch() {
        return (getAdapteeManager().supportsBlockSearch());
    }


    /**
     *  Tests if block administrative service is supported. 
     *
     *  @return <code> true </code> if block administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockAdmin() {
        return (getAdapteeManager().supportsBlockAdmin());
    }


    /**
     *  Tests if a block notification service is supported. 
     *
     *  @return <code> true </code> if block notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockNotification() {
        return (getAdapteeManager().supportsBlockNotification());
    }


    /**
     *  Tests if a block oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a block oubliette lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockOubliette() {
        return (getAdapteeManager().supportsBlockOubliette());
    }


    /**
     *  Tests if a block oubliette service is supported. 
     *
     *  @return <code> true </code> if block to oubliette assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockOublietteAssignment() {
        return (getAdapteeManager().supportsBlockOublietteAssignment());
    }


    /**
     *  Tests if a block smart oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a block smart oubliette service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockSmartOubliette() {
        return (getAdapteeManager().supportsBlockSmartOubliette());
    }


    /**
     *  Tests if looking up issues is supported. 
     *
     *  @return <code> true </code> if issue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueLookup() {
        return (getAdapteeManager().supportsIssueLookup());
    }


    /**
     *  Tests if querying issues is supported. 
     *
     *  @return <code> true </code> if issue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (getAdapteeManager().supportsIssueQuery());
    }


    /**
     *  Tests if searching issues is supported. 
     *
     *  @return <code> true </code> if issue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSearch() {
        return (getAdapteeManager().supportsIssueSearch());
    }


    /**
     *  Tests if issue <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if issue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueAdmin() {
        return (getAdapteeManager().supportsIssueAdmin());
    }


    /**
     *  Tests if an issue <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if issue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueNotification() {
        return (getAdapteeManager().supportsIssueNotification());
    }


    /**
     *  Tests if an issue oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if an issue oubliette lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueOubliette() {
        return (getAdapteeManager().supportsIssueOubliette());
    }


    /**
     *  Tests if an issue oubliette assignment service is supported. 
     *
     *  @return <code> true </code> if an issue to oubliette assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueOublietteAssignment() {
        return (getAdapteeManager().supportsIssueOublietteAssignment());
    }


    /**
     *  Tests if an issue smart oubliette service is supported. 
     *
     *  @return <code> true </code> if an issue smart oubliette service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSmartOubliette() {
        return (getAdapteeManager().supportsIssueSmartOubliette());
    }


    /**
     *  Tests if looking up holds is supported. 
     *
     *  @return <code> true </code> if hold lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldLookup() {
        return (getAdapteeManager().supportsHoldLookup());
    }


    /**
     *  Tests if querying holds is supported. 
     *
     *  @return <code> true </code> if hold query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldQuery() {
        return (getAdapteeManager().supportsHoldQuery());
    }


    /**
     *  Tests if searching holds is supported. 
     *
     *  @return <code> true </code> if hold search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldSearch() {
        return (getAdapteeManager().supportsHoldSearch());
    }


    /**
     *  Tests if hold administrative service is supported. 
     *
     *  @return <code> true </code> if hold administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldAdmin() {
        return (getAdapteeManager().supportsHoldAdmin());
    }


    /**
     *  Tests if a hold notification service is supported. 
     *
     *  @return <code> true </code> if hold notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldNotification() {
        return (getAdapteeManager().supportsHoldNotification());
    }


    /**
     *  Tests if a hold oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a hold oubliette lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldOubliette() {
        return (getAdapteeManager().supportsHoldOubliette());
    }


    /**
     *  Tests if a hold oubliette service is supported. 
     *
     *  @return <code> true </code> if hold to oubliette assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldOublietteAssignment() {
        return (getAdapteeManager().supportsHoldOublietteAssignment());
    }


    /**
     *  Tests if a hold smart oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a hold smart oubliette service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldSmartOubliette() {
        return (getAdapteeManager().supportsHoldSmartOubliette());
    }


    /**
     *  Tests if looking up oubliettes is supported. 
     *
     *  @return <code> true </code> if oubliette lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteLookup() {
        return (getAdapteeManager().supportsOublietteLookup());
    }


    /**
     *  Tests if querying oubliettes is supported. 
     *
     *  @return <code> true </code> if an oubliette query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (getAdapteeManager().supportsOublietteQuery());
    }


    /**
     *  Tests if searching oubliettes is supported. 
     *
     *  @return <code> true </code> if oubliette search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteSearch() {
        return (getAdapteeManager().supportsOublietteSearch());
    }


    /**
     *  Tests if oubliette administrative service is supported. 
     *
     *  @return <code> true </code> if oubliette administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteAdmin() {
        return (getAdapteeManager().supportsOublietteAdmin());
    }


    /**
     *  Tests if an oubliette <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if oubliette notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteNotification() {
        return (getAdapteeManager().supportsOublietteNotification());
    }


    /**
     *  Tests for the availability of an oubliette hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if oubliette hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteHierarchy() {
        return (getAdapteeManager().supportsOublietteHierarchy());
    }


    /**
     *  Tests for the availability of an oubliette hierarchy design service. 
     *
     *  @return <code> true </code> if oubliette hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteHierarchyDesign() {
        return (getAdapteeManager().supportsOublietteHierarchyDesign());
    }


    /**
     *  Tests for the availability of a hold batch service. 
     *
     *  @return <code> true </code> if a hold batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldBatch() {
        return (getAdapteeManager().supportsHoldBatch());
    }


    /**
     *  Tests for the availability of a hold rules service. 
     *
     *  @return <code> true </code> if a hold rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldRules() {
        return (getAdapteeManager().supportsHoldRules());
    }


    /**
     *  Gets the supported <code> Block </code> record types. 
     *
     *  @return a list containing the supported <code> Block </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlockRecordTypes() {
        return (getAdapteeManager().getBlockRecordTypes());
    }


    /**
     *  Tests if the given <code> Block </code> record type is supported. 
     *
     *  @param  blockRecordType a <code> Type </code> indicating a <code> 
     *          Block </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blockRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlockRecordType(org.osid.type.Type blockRecordType) {
        return (getAdapteeManager().supportsBlockRecordType(blockRecordType));
    }


    /**
     *  Gets the supported <code> Block </code> search record types. 
     *
     *  @return a list containing the supported <code> Block </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlockSearchRecordTypes() {
        return (getAdapteeManager().getBlockSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Block </code> search record type is 
     *  supported. 
     *
     *  @param  blockSearchRecordType a <code> Type </code> indicating a 
     *          <code> Block </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blockSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlockSearchRecordType(org.osid.type.Type blockSearchRecordType) {
        return (getAdapteeManager().supportsBlockSearchRecordType(blockSearchRecordType));
    }


    /**
     *  Gets the supported <code> Issue </code> record types. 
     *
     *  @return a list containing the supported <code> Issue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueRecordTypes() {
        return (getAdapteeManager().getIssueRecordTypes());
    }


    /**
     *  Tests if the given <code> Issue </code> record type is supported. 
     *
     *  @param  issueRecordType a <code> Type </code> indicating an <code> 
     *          Issue </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueRecordType(org.osid.type.Type issueRecordType) {
        return (getAdapteeManager().supportsIssueRecordType(issueRecordType));
    }


    /**
     *  Gets the supported <code> Issue </code> search types. 
     *
     *  @return a list containing the supported <code> Issue </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueSearchRecordTypes() {
        return (getAdapteeManager().getIssueSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Issue </code> search type is supported. 
     *
     *  @param  issueSearchRecordType a <code> Type </code> indicating an 
     *          <code> Issue </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        return (getAdapteeManager().supportsIssueSearchRecordType(issueSearchRecordType));
    }


    /**
     *  Gets the supported <code> Hold </code> record types. 
     *
     *  @return a list containing the supported <code> Hold </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldRecordTypes() {
        return (getAdapteeManager().getHoldRecordTypes());
    }


    /**
     *  Tests if the given <code> Hold </code> record type is supported. 
     *
     *  @param  holdRecordType a <code> Type </code> indicating a <code> Hold 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> holdRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldRecordType(org.osid.type.Type holdRecordType) {
        return (getAdapteeManager().supportsHoldRecordType(holdRecordType));
    }


    /**
     *  Gets the supported <code> Hold </code> search record types. 
     *
     *  @return a list containing the supported <code> Hold </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldSearchRecordTypes() {
        return (getAdapteeManager().getHoldSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Hold </code> search record type is 
     *  supported. 
     *
     *  @param  holdSearchRecordType a <code> Type </code> indicating a <code> 
     *          Hold </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> holdSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldSearchRecordType(org.osid.type.Type holdSearchRecordType) {
        return (getAdapteeManager().supportsHoldSearchRecordType(holdSearchRecordType));
    }


    /**
     *  Gets the supported <code> Oubliette </code> record types. 
     *
     *  @return a list containing the supported <code> Oubliette </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOublietteRecordTypes() {
        return (getAdapteeManager().getOublietteRecordTypes());
    }


    /**
     *  Tests if the given <code> Oubliette </code> record type is supported. 
     *
     *  @param  oublietteRecordType a <code> Type </code> indicating a <code> 
     *          Oubliette </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> oublietteRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOublietteRecordType(org.osid.type.Type oublietteRecordType) {
        return (getAdapteeManager().supportsOublietteRecordType(oublietteRecordType));
    }


    /**
     *  Gets the supported <code> Oubliette </code> search types. 
     *
     *  @return a list containing the supported <code> Oubliette </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOublietteSearchRecordTypes() {
        return (getAdapteeManager().getOublietteSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Oubliette </code> search record type is 
     *  supported. 
     *
     *  @param  oublietteSearchRecordType a <code> Type </code> indicating a 
     *          <code> Oubliette </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          oublietteSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOublietteSearchRecordType(org.osid.type.Type oublietteSearchRecordType) {
        return (getAdapteeManager().supportsOublietteSearchRecordType(oublietteSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block service 
     *  to check for blocks. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlock() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSession getBlockSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block service 
     *  for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @param  proxy a proxy 
     *  @return a <code> BlockSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlock() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSession getBlockSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my hold 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyHoldSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyHold() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.MyHoldSession getMyHoldSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyHoldSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my hold 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @param  proxy a proxy 
     *  @return a <code> MyHoldSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyHold() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.MyHoldSession getMyHoldSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyHoldSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockLookupSession getBlockLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockLookupSession getBlockLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockLookupSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuerySession getBlockQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockQuerySession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuerySession getBlockQuerySessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockQuerySessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchSession getBlockSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockSearchSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchSession getBlockSearchSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockSearchSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockAdminSession getBlockAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockAdminSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockAdminSession getBlockAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockAdminSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  notification service. 
     *
     *  @param  blockReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BlockNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> blockReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockNotificationSession getBlockNotificationSession(org.osid.hold.BlockReceiver blockReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockNotificationSession(blockReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  notification service for the given oubliette. 
     *
     *  @param  blockReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> blockReceiver, 
     *          oublietteId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockNotificationSession getBlockNotificationSessionForOubliette(org.osid.hold.BlockReceiver blockReceiver, 
                                                                                          org.osid.id.Id oublietteId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockNotificationSessionForOubliette(blockReceiver, oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup block/oubliette 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockOublietteSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockOublietteSession getBlockOublietteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockOublietteSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning blocks 
     *  to oubliettes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockOublietteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockOublietteAssignmentSession getBlockOublietteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockOublietteAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage block smart oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSmartOublietteSession getBlockSmartOublietteSession(org.osid.id.Id oublietteId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockSmartOublietteSession(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueLookupSession getIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueLookupSession getIssueLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueLookupSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuerySession getIssueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuerySession getIssueQuerySessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueQuerySessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSearchSession getIssueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSearchSession getIssueSearchSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueSearchSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueAdminSession getIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueAdminSession getIssueAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueAdminSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueNotificationSession getIssueNotificationSession(org.osid.hold.IssueReceiver issueReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueNotificationSession(issueReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service for the given oubliette. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver, </code> 
     *          <code> oublietteId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueNotificationSession getIssueNotificationSessionForOubliette(org.osid.hold.IssueReceiver issueReceiver, 
                                                                                          org.osid.id.Id oublietteId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueNotificationSessionForOubliette(issueReceiver, oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup issue/oubliette holds. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueOublietteSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueOublietteSession getIssueOublietteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueOublietteSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning mappings 
     *  to oubliettes. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueOublietteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueOublietteAssignmentSession getIssueOublietteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueOublietteAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSmartOublietteSession getIssueSmartOublietteSession(org.osid.id.Id oublietteId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueSmartOublietteSession(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldLookupSession getHoldLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldLookupSession getHoldLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldLookupSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuerySession getHoldQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuerySession getHoldQuerySessionForOubliette(org.osid.id.Id oublietteId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldQuerySessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSearchSession getHoldSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSearchSession getHoldSearchSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldSearchSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldAdminSession getHoldAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  administrative service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldAdminSession getHoldAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldAdminSessionForOubliette(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  notification service. 
     *
     *  @param  holdReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> HoldNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> holdReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldNotificationSession getHoldNotificationSession(org.osid.hold.HoldReceiver holdReceiver, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldNotificationSession(holdReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  notification service for the given oubliette. 
     *
     *  @param  holdReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> holdReceiver, 
     *          oublietteId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldNotificationSession getHoldNotificationSessionForOubliette(org.osid.hold.HoldReceiver holdReceiver, 
                                                                                        org.osid.id.Id oublietteId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldNotificationSessionForOubliette(holdReceiver, oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup hold/oubliette mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldOublietteSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldOubliette() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldOublietteSession getHoldOublietteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldOublietteSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to oubliettes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldOublietteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldOublietteAssignmentSession getHoldOublietteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldOublietteAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage hold smart oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSmartOublietteSession getHoldSmartOublietteSession(org.osid.id.Id oublietteId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldSmartOublietteSession(oublietteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteLookupSession getOublietteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuerySession getOublietteQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteSearchSession getOublietteSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteAdminSession getOublietteAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  notification service. 
     *
     *  @param  oublietteReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> OublietteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteNotificationSession getOublietteNotificationSession(org.osid.hold.OublietteReceiver oublietteReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteNotificationSession(oublietteReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteHierarchySession getOublietteHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for oubliettes 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteHierarchyDesignSession getOublietteHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOublietteHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> HoldBatchProxyManager. </code> 
     *
     *  @return a <code> HoldBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchProxyManager getHoldBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldBatchProxyManager());
    }


    /**
     *  Gets a <code> HoldRulesProxyManager. </code> 
     *
     *  @return a <code> HoldRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldRulesProxyManager getHoldRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
