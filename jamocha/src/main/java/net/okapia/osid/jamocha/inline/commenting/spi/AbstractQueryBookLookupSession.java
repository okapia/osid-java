//
// AbstractQueryBookLookupSession.java
//
//    An inline adapter that maps a BookLookupSession to
//    a BookQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BookLookupSession to
 *  a BookQuerySession.
 */

public abstract class AbstractQueryBookLookupSession
    extends net.okapia.osid.jamocha.commenting.spi.AbstractBookLookupSession
    implements org.osid.commenting.BookLookupSession {

    private final org.osid.commenting.BookQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBookLookupSession.
     *
     *  @param querySession the underlying book query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBookLookupSession(org.osid.commenting.BookQuerySession querySession) {
        nullarg(querySession, "book query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Book</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBooks() {
        return (this.session.canSearchBooks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Book</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Book</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Book</code> and
     *  retained for compatibility.
     *
     *  @param  bookId <code>Id</code> of the
     *          <code>Book</code>
     *  @return the book
     *  @throws org.osid.NotFoundException <code>bookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>bookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.BookQuery query = getQuery();
        query.matchId(bookId, true);
        org.osid.commenting.BookList books = this.session.getBooksByQuery(query);
        if (books.hasNext()) {
            return (books.getNextBook());
        } 
        
        throw new org.osid.NotFoundException(bookId + " not found");
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  books specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Books</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  bookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>bookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByIds(org.osid.id.IdList bookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.BookQuery query = getQuery();

        try (org.osid.id.IdList ids = bookIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBooksByQuery(query));
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  book genus <code>Type</code> which does not include
     *  books of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.BookQuery query = getQuery();
        query.matchGenusType(bookGenusType, true);
        return (this.session.getBooksByQuery(query));
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  book genus <code>Type</code> and include any additional
     *  books with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByParentGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.BookQuery query = getQuery();
        query.matchParentGenusType(bookGenusType, true);
        return (this.session.getBooksByQuery(query));
    }


    /**
     *  Gets a <code>BookList</code> containing the given
     *  book record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookRecordType a book record type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByRecordType(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.BookQuery query = getQuery();
        query.matchRecordType(bookRecordType, true);
        return (this.session.getBooksByQuery(query));
    }


    /**
     *  Gets a <code>BookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known books or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  books that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Book</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.BookQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getBooksByQuery(query));        
    }

    
    /**
     *  Gets all <code>Books</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Books</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.BookQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBooksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.commenting.BookQuery getQuery() {
        org.osid.commenting.BookQuery query = this.session.getBookQuery();
        return (query);
    }
}
