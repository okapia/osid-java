//
// DispatchMiter.java
//
//     Defines a Dispatch miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.subscription.dispatch;


/**
 *  Defines a <code>Dispatch</code> miter for use with the builders.
 */

public interface DispatchMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidGovernatorMiter,
            org.osid.subscription.Dispatch {


    /**
     *  Adds an address genus type.
     *
     *  @param addressGenusType an address genus type
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code>null</code>
     */

    public void addAddressGenusType(org.osid.type.Type addressGenusType);


    /**
     *  Sets all the address genus types.
     *
     *  @param addressGenusTypes a collection of address genus types
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusTypes</code> is <code>null</code>
     */

    public void setAddressGenusTypes(java.util.Collection<org.osid.type.Type> addressGenusTypes);


    /**
     *  Adds a Dispatch record.
     *
     *  @param record a dispatch record
     *  @param recordType the type of dispatch record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addDispatchRecord(org.osid.subscription.records.DispatchRecord record, org.osid.type.Type recordType);
}       


