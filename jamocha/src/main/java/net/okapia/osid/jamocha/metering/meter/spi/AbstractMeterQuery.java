//
// AbstractMeterQuery.java
//
//     A template for making a Meter Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.meter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for meters.
 */

public abstract class AbstractMeterQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.metering.MeterQuery {

    private final java.util.Collection<org.osid.metering.records.MeterQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the utility <code> Id </code> for this query. 
     *
     *  @param  utilityId the utility <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchUtilityId(org.osid.id.Id utilityId, boolean match) {
        return;
    }


    /**
     *  Clears the utility <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearUtilityIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> UtilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a utility. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the utility query 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuery getUtilityQuery() {
        throw new org.osid.UnimplementedException("supportsUtilityQuery() is false");
    }


    /**
     *  Clears the utility query terms. 
     */

    @OSID @Override
    public void clearUtilityTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given meter query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a meter implementing the requested record.
     *
     *  @param meterRecordType a meter record type
     *  @return the meter query record
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(meterRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.MeterQueryRecord getMeterQueryRecord(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.MeterQueryRecord record : this.records) {
            if (record.implementsRecordType(meterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(meterRecordType + " is not supported");
    }


    /**
     *  Adds a record to this meter query. 
     *
     *  @param meterQueryRecord meter query record
     *  @param meterRecordType meter record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMeterQueryRecord(org.osid.metering.records.MeterQueryRecord meterQueryRecord, 
                                          org.osid.type.Type meterRecordType) {

        addRecordType(meterRecordType);
        nullarg(meterQueryRecord, "meter query record");
        this.records.add(meterQueryRecord);        
        return;
    }
}
