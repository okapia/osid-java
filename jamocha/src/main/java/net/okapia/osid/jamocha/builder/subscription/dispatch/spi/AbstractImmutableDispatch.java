//
// AbstractImmutableDispatch.java
//
//     Wraps a mutable Dispatch to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.subscription.dispatch.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Dispatch</code> to hide modifiers. This
 *  wrapper provides an immutized Dispatch from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying dispatch whose state changes are visible.
 */

public abstract class AbstractImmutableDispatch
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.subscription.Dispatch {

    private final org.osid.subscription.Dispatch dispatch;


    /**
     *  Constructs a new <code>AbstractImmutableDispatch</code>.
     *
     *  @param dispatch the dispatch to immutablize
     *  @throws org.osid.NullArgumentException <code>dispatch</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDispatch(org.osid.subscription.Dispatch dispatch) {
        super(dispatch);
        this.dispatch = dispatch;
        return;
    }


    /**
     *  Gets the list of address genus types accepted by this dispatch. 
     *
     *  @return a list of address genus types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAddressGenusTypes() {
        return (this.dispatch.getAddressGenusTypes());
    }


    /**
     *  Gets the dispatch record corresponding to the given <code> Dispatch 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  dispatchRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(dispatchRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  dispatchRecordType the type of dispatch record to retrieve 
     *  @return the dispatch record 
     *  @throws org.osid.NullArgumentException <code> dispatchRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(dispatchRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchRecord getDispatchRecord(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException {

        return (this.dispatch.getDispatchRecord(dispatchRecordType));
    }
}

