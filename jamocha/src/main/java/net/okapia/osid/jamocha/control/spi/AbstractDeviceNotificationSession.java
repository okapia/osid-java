//
// AbstractDeviceNotificationSession.java
//
//     A template for making DeviceNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Device} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Device} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for device entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractDeviceNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.DeviceNotificationSession {

    private boolean federated = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();


    /**
     *  Gets the {@code System/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }

    
    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the {@code System}.
     *
     *  @param system the system for this session
     *  @throws org.osid.NullArgumentException {@code system}
     *          is {@code null}
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can register for {@code Device}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForDeviceNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeDeviceNotification() </code>.
     */

    @OSID @Override
    public void reliableDeviceNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableDeviceNotifications() {
        return;
    }


    /**
     *  Acknowledge a device notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeDeviceNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for devices in systems
     *  which are children of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new devices. {@code
     *  DeviceReceiver.newDevice()} is invoked when a new {@code
     *  Device} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewDevices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated devices. {@code
     *  DeviceReceiver.changedDevice()} is invoked when a device is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedDevices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated device. {@code
     *  DeviceReceiver.changedDevice()} is invoked when the specified
     *  device is changed.
     *
     *  @param deviceId the {@code Id} of the {@code Device} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code deviceId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedDevice(org.osid.id.Id deviceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted devices. {@code
     *  DeviceReceiver.deletedDevice()} is invoked when a device is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedDevices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted device. {@code
     *  DeviceReceiver.deletedDevice()} is invoked when the specified
     *  device is deleted.
     *
     *  @param deviceId the {@code Id} of the
     *          {@code Device} to monitor
     *  @throws org.osid.NullArgumentException {@code deviceId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedDevice(org.osid.id.Id deviceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
