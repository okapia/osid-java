//
// AbstractProfileEntryEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.rules.profileentryenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProfileEntryEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.profile.rules.ProfileEntryEnablerSearchResults {

    private org.osid.profile.rules.ProfileEntryEnablerList profileEntryEnablers;
    private final org.osid.profile.rules.ProfileEntryEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.profile.rules.records.ProfileEntryEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProfileEntryEnablerSearchResults.
     *
     *  @param profileEntryEnablers the result set
     *  @param profileEntryEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>profileEntryEnablers</code>
     *          or <code>profileEntryEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProfileEntryEnablerSearchResults(org.osid.profile.rules.ProfileEntryEnablerList profileEntryEnablers,
                                            org.osid.profile.rules.ProfileEntryEnablerQueryInspector profileEntryEnablerQueryInspector) {
        nullarg(profileEntryEnablers, "profile entry enablers");
        nullarg(profileEntryEnablerQueryInspector, "profile entry enabler query inspectpr");

        this.profileEntryEnablers = profileEntryEnablers;
        this.inspector = profileEntryEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the profile entry enabler list resulting from a search.
     *
     *  @return a profile entry enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablers() {
        if (this.profileEntryEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.profile.rules.ProfileEntryEnablerList profileEntryEnablers = this.profileEntryEnablers;
        this.profileEntryEnablers = null;
	return (profileEntryEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.profile.rules.ProfileEntryEnablerQueryInspector getProfileEntryEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  profile entry enabler search record <code> Type. </code> This method must
     *  be used to retrieve a profileEntryEnabler implementing the requested
     *  record.
     *
     *  @param profileEntryEnablerSearchRecordType a profileEntryEnabler search 
     *         record type 
     *  @return the profile entry enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(profileEntryEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerSearchResultsRecord getProfileEntryEnablerSearchResultsRecord(org.osid.type.Type profileEntryEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.profile.rules.records.ProfileEntryEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(profileEntryEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(profileEntryEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record profile entry enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProfileEntryEnablerRecord(org.osid.profile.rules.records.ProfileEntryEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "profile entry enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
