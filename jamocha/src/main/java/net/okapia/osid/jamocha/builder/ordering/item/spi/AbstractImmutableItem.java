//
// AbstractImmutableItem.java
//
//     Wraps a mutable Item to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.item.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Item</code> to hide modifiers. This
 *  wrapper provides an immutized Item from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying item whose state changes are visible.
 */

public abstract class AbstractImmutableItem
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.ordering.Item {

    private final org.osid.ordering.Item item;


    /**
     *  Constructs a new <code>AbstractImmutableItem</code>.
     *
     *  @param item the item to immutablize
     *  @throws org.osid.NullArgumentException <code>item</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableItem(org.osid.ordering.Item item) {
        super(item);
        this.item = item;
        return;
    }


    /**
     *  Gets the order <code> Id </code> for this item. 
     *
     *  @return the order <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrderId() {
        return (this.item.getOrderId());
    }


    /**
     *  Gets the order for this item. 
     *
     *  @return the order 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder()
        throws org.osid.OperationFailedException {

        return (this.item.getOrder());
    }


    /**
     *  Tests if the item is a derived item as opposed to one that has been 
     *  explicitly selected. 
     *
     *  @return <code> true </code> if this item is derived, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isDerived() {
        return (this.item.isDerived());
    }


    /**
     *  Gets the product <code> Id </code> for this item. 
     *
     *  @return the product <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProductId() {
        return (this.item.getProductId());
    }


    /**
     *  Gets the product for this item. 
     *
     *  @return the product 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct()
        throws org.osid.OperationFailedException {

        return (this.item.getProduct());
    }


    /**
     *  Gets the price <code> Ids </code> for this item. 
     *
     *  @return the price <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getUnitPriceIds() {
        return (this.item.getUnitPriceIds());
    }


    /**
     *  Gets the prices for this item. There may be different price types for 
     *  a single item. 
     *
     *  @return the prices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceList getUnitPrices()
        throws org.osid.OperationFailedException {

        return (this.item.getUnitPrices());
    }


    /**
     *  Gets the quantity of the product. 
     *
     *  @return the quantity of the product 
     */

    @OSID @Override
    public long getQuantity() {
        return (this.item.getQuantity());
    }


    /**
     *  Gets the line item costs. 
     *
     *  @return the costs. 
     */

    @OSID @Override
    public org.osid.ordering.CostList getCosts() {
        return (this.item.getCosts());
    }


    /**
     *  Gets the item record corresponding to the given <code> Item </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> itemRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(itemRecordType) </code> is <code> true </code> . 
     *
     *  @param  itemRecordType the type of item record to retrieve 
     *  @return the item record 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(itemRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        return (this.item.getItemRecord(itemRecordType));
    }
}

