//
// InvariantMapProxyControllerLookupSession
//
//    Implements a Controller lookup service backed by a fixed
//    collection of controllers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Controller lookup service backed by a fixed
 *  collection of controllers. The controllers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyControllerLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapControllerLookupSession
    implements org.osid.control.ControllerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyControllerLookupSession} with no
     *  controllers.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyControllerLookupSession(org.osid.control.System system,
                                                  org.osid.proxy.Proxy proxy) {
        setSystem(system);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyControllerLookupSession} with a single
     *  controller.
     *
     *  @param system the system
     *  @param controller a single controller
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code controller} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyControllerLookupSession(org.osid.control.System system,
                                                  org.osid.control.Controller controller, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putController(controller);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyControllerLookupSession} using
     *  an array of controllers.
     *
     *  @param system the system
     *  @param controllers an array of controllers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code controllers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyControllerLookupSession(org.osid.control.System system,
                                                  org.osid.control.Controller[] controllers, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putControllers(controllers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyControllerLookupSession} using a
     *  collection of controllers.
     *
     *  @param system the system
     *  @param controllers a collection of controllers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code controllers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyControllerLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.Controller> controllers,
                                                  org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putControllers(controllers);
        return;
    }
}
