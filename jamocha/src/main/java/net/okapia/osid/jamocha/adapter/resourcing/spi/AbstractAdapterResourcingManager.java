//
// AbstractResourcingManager.java
//
//     An adapter for a ResourcingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ResourcingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterResourcingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.resourcing.ResourcingManager>
    implements org.osid.resourcing.ResourcingManager {


    /**
     *  Constructs a new {@code AbstractAdapterResourcingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterResourcingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterResourcingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterResourcingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any foundry federation is exposed. Federation is exposed when 
     *  a specific foundry may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  foundries appears as a single foundry. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if an availability service is supported for the current agent. 
     *
     *  @return <code> true </code> if my availability is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyWork() {
        return (getAdapteeManager().supportsMyWork());
    }


    /**
     *  Tests if looking up jobs is supported. 
     *
     *  @return <code> true </code> if job lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobLookup() {
        return (getAdapteeManager().supportsJobLookup());
    }


    /**
     *  Tests if querying jobs is supported. 
     *
     *  @return <code> true </code> if job query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (getAdapteeManager().supportsJobQuery());
    }


    /**
     *  Tests if searching jobs is supported. 
     *
     *  @return <code> true </code> if job search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobSearch() {
        return (getAdapteeManager().supportsJobSearch());
    }


    /**
     *  Tests if job administrative service is supported. 
     *
     *  @return <code> true </code> if job administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobAdmin() {
        return (getAdapteeManager().supportsJobAdmin());
    }


    /**
     *  Tests if a job notification service is supported. 
     *
     *  @return <code> true </code> if job notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobNotification() {
        return (getAdapteeManager().supportsJobNotification());
    }


    /**
     *  Tests if a job foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobFoundry() {
        return (getAdapteeManager().supportsJobFoundry());
    }


    /**
     *  Tests if a job foundry service is supported. 
     *
     *  @return <code> true </code> if job to foundry assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobFoundryAssignment() {
        return (getAdapteeManager().supportsJobFoundryAssignment());
    }


    /**
     *  Tests if a job smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobSmartFoundry() {
        return (getAdapteeManager().supportsJobSmartFoundry());
    }


    /**
     *  Tests if looking up work is supported. 
     *
     *  @return <code> true </code> if work lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkLookup() {
        return (getAdapteeManager().supportsWorkLookup());
    }


    /**
     *  Tests if querying work is supported. 
     *
     *  @return <code> true </code> if work query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (getAdapteeManager().supportsWorkQuery());
    }


    /**
     *  Tests if searching work is supported. 
     *
     *  @return <code> true </code> if work search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSearch() {
        return (getAdapteeManager().supportsWorkSearch());
    }


    /**
     *  Tests if work administrative service is supported. 
     *
     *  @return <code> true </code> if work administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkAdmin() {
        return (getAdapteeManager().supportsWorkAdmin());
    }


    /**
     *  Tests if a work notification service is supported. 
     *
     *  @return <code> true </code> if work notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkNotification() {
        return (getAdapteeManager().supportsWorkNotification());
    }


    /**
     *  Tests if a work foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a work foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkFoundry() {
        return (getAdapteeManager().supportsWorkFoundry());
    }


    /**
     *  Tests if a work foundry service is supported. 
     *
     *  @return <code> true </code> if work to foundry assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkFoundryAssignment() {
        return (getAdapteeManager().supportsWorkFoundryAssignment());
    }


    /**
     *  Tests if a work smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a work smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSmartFoundry() {
        return (getAdapteeManager().supportsWorkSmartFoundry());
    }


    /**
     *  Tests if looking up competencies is supported. 
     *
     *  @return <code> true </code> if competency lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyLookup() {
        return (getAdapteeManager().supportsCompetencyLookup());
    }


    /**
     *  Tests if querying competencies is supported. 
     *
     *  @return <code> true </code> if competency query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (getAdapteeManager().supportsCompetencyQuery());
    }


    /**
     *  Tests if searching competencies is supported. 
     *
     *  @return <code> true </code> if competency search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencySearch() {
        return (getAdapteeManager().supportsCompetencySearch());
    }


    /**
     *  Tests if competency administrative service is supported. 
     *
     *  @return <code> true </code> if competency administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyAdmin() {
        return (getAdapteeManager().supportsCompetencyAdmin());
    }


    /**
     *  Tests if a competency notification service is supported. 
     *
     *  @return <code> true </code> if competency notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyNotification() {
        return (getAdapteeManager().supportsCompetencyNotification());
    }


    /**
     *  Tests if a competency foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a competency foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyFoundry() {
        return (getAdapteeManager().supportsCompetencyFoundry());
    }


    /**
     *  Tests if a competency foundry service is supported. 
     *
     *  @return <code> true </code> if competency to foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyFoundryAssignment() {
        return (getAdapteeManager().supportsCompetencyFoundryAssignment());
    }


    /**
     *  Tests if a competency smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a competency smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencySmartFoundry() {
        return (getAdapteeManager().supportsCompetencySmartFoundry());
    }


    /**
     *  Tests if looking up availabilities is supported. 
     *
     *  @return <code> true </code> if availability lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityLookup() {
        return (getAdapteeManager().supportsAvailabilityLookup());
    }


    /**
     *  Tests if querying availabilities is supported. 
     *
     *  @return <code> true </code> if availability query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (getAdapteeManager().supportsAvailabilityQuery());
    }


    /**
     *  Tests if searching availabilities is supported. 
     *
     *  @return <code> true </code> if availability search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilitySearch() {
        return (getAdapteeManager().supportsAvailabilitySearch());
    }


    /**
     *  Tests if availability <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if availability administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityAdmin() {
        return (getAdapteeManager().supportsAvailabilityAdmin());
    }


    /**
     *  Tests if an availability <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if availability notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityNotification() {
        return (getAdapteeManager().supportsAvailabilityNotification());
    }


    /**
     *  Tests if an availability foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an availability foundry lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityFoundry() {
        return (getAdapteeManager().supportsAvailabilityFoundry());
    }


    /**
     *  Tests if an availability foundry assignment service is supported. 
     *
     *  @return <code> true </code> if an availability to foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityFoundryAssignment() {
        return (getAdapteeManager().supportsAvailabilityFoundryAssignment());
    }


    /**
     *  Tests if an availability smart foundry service is supported. 
     *
     *  @return <code> true </code> if an availability smart foundry service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilitySmartFoundry() {
        return (getAdapteeManager().supportsAvailabilitySmartFoundry());
    }


    /**
     *  Tests if looking up commissions is supported. 
     *
     *  @return <code> true </code> if commission lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionLookup() {
        return (getAdapteeManager().supportsCommissionLookup());
    }


    /**
     *  Tests if querying commissions is supported. 
     *
     *  @return <code> true </code> if commission query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionQuery() {
        return (getAdapteeManager().supportsCommissionQuery());
    }


    /**
     *  Tests if searching commissions is supported. 
     *
     *  @return <code> true </code> if commission search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionSearch() {
        return (getAdapteeManager().supportsCommissionSearch());
    }


    /**
     *  Tests if commission administrative service is supported. 
     *
     *  @return <code> true </code> if commission administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionAdmin() {
        return (getAdapteeManager().supportsCommissionAdmin());
    }


    /**
     *  Tests if a commission notification service is supported. 
     *
     *  @return <code> true </code> if commission notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionNotification() {
        return (getAdapteeManager().supportsCommissionNotification());
    }


    /**
     *  Tests if a commission foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionFoundry() {
        return (getAdapteeManager().supportsCommissionFoundry());
    }


    /**
     *  Tests if a commission foundry service is supported. 
     *
     *  @return <code> true </code> if commission to foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionFoundryAssignment() {
        return (getAdapteeManager().supportsCommissionFoundryAssignment());
    }


    /**
     *  Tests if a commission smart foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionSmartFoundry() {
        return (getAdapteeManager().supportsCommissionSmartFoundry());
    }


    /**
     *  Tests if looking up efforts is supported. 
     *
     *  @return <code> true </code> if effort lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortLookup() {
        return (getAdapteeManager().supportsEffortLookup());
    }


    /**
     *  Tests if querying efforts is supported. 
     *
     *  @return <code> true </code> if effort query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortQuery() {
        return (getAdapteeManager().supportsEffortQuery());
    }


    /**
     *  Tests if searching efforts is supported. 
     *
     *  @return <code> true </code> if effort search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortSearch() {
        return (getAdapteeManager().supportsEffortSearch());
    }


    /**
     *  Tests if an effort administrative service is supported. 
     *
     *  @return <code> true </code> if effort administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortAdmin() {
        return (getAdapteeManager().supportsEffortAdmin());
    }


    /**
     *  Tests if an effort <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if effort notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortNotification() {
        return (getAdapteeManager().supportsEffortNotification());
    }


    /**
     *  Tests if an effort foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an effort foundry lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortFoundry() {
        return (getAdapteeManager().supportsEffortFoundry());
    }


    /**
     *  Tests if an effort foundry assignment service is supported. 
     *
     *  @return <code> true </code> if an effort to foundry assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortFoundryAssignment() {
        return (getAdapteeManager().supportsEffortFoundryAssignment());
    }


    /**
     *  Tests if an effort smart foundry service is supported. 
     *
     *  @return <code> true </code> if an v smart foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortSmartFoundry() {
        return (getAdapteeManager().supportsEffortSmartFoundry());
    }


    /**
     *  Tests if looking up foundries is supported. 
     *
     *  @return <code> true </code> if foundry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryLookup() {
        return (getAdapteeManager().supportsFoundryLookup());
    }


    /**
     *  Tests if querying foundries is supported. 
     *
     *  @return <code> true </code> if a foundry query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (getAdapteeManager().supportsFoundryQuery());
    }


    /**
     *  Tests if searching foundries is supported. 
     *
     *  @return <code> true </code> if foundry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundrySearch() {
        return (getAdapteeManager().supportsFoundrySearch());
    }


    /**
     *  Tests if foundry administrative service is supported. 
     *
     *  @return <code> true </code> if foundry administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryAdmin() {
        return (getAdapteeManager().supportsFoundryAdmin());
    }


    /**
     *  Tests if a foundry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if foundry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryNotification() {
        return (getAdapteeManager().supportsFoundryNotification());
    }


    /**
     *  Tests for the availability of a foundry hierarchy traversal service. 
     *
     *  @return <code> true </code> if foundry hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryHierarchy() {
        return (getAdapteeManager().supportsFoundryHierarchy());
    }


    /**
     *  Tests for the availability of a foundry hierarchy design service. 
     *
     *  @return <code> true </code> if foundry hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryHierarchyDesign() {
        return (getAdapteeManager().supportsFoundryHierarchyDesign());
    }


    /**
     *  Tests for the availability of a resourcing batch service. 
     *
     *  @return <code> true </code> if a resourcing batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingBatch() {
        return (getAdapteeManager().supportsResourcingBatch());
    }


    /**
     *  Tests for the availability of a resourcing rules service. 
     *
     *  @return <code> true </code> if a resourcing rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingRules() {
        return (getAdapteeManager().supportsResourcingRules());
    }


    /**
     *  Gets the supported <code> Job </code> record types. 
     *
     *  @return a list containing the supported <code> Job </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobRecordTypes() {
        return (getAdapteeManager().getJobRecordTypes());
    }


    /**
     *  Tests if the given <code> Job </code> record type is supported. 
     *
     *  @param  jobRecordType a <code> Type </code> indicating a <code> Job 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobRecordType(org.osid.type.Type jobRecordType) {
        return (getAdapteeManager().supportsJobRecordType(jobRecordType));
    }


    /**
     *  Gets the supported <code> Job </code> search record types. 
     *
     *  @return a list containing the supported <code> Job </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobSearchRecordTypes() {
        return (getAdapteeManager().getJobSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Job </code> search record type is supported. 
     *
     *  @param  jobSearchRecordType a <code> Type </code> indicating a <code> 
     *          Job </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobSearchRecordType(org.osid.type.Type jobSearchRecordType) {
        return (getAdapteeManager().supportsJobSearchRecordType(jobSearchRecordType));
    }


    /**
     *  Gets the supported <code> Work </code> record types. 
     *
     *  @return a list containing the supported <code> Work </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkRecordTypes() {
        return (getAdapteeManager().getWorkRecordTypes());
    }


    /**
     *  Tests if the given <code> Work </code> record type is supported. 
     *
     *  @param  workRecordType a <code> Type </code> indicating a <code> Work 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkRecordType(org.osid.type.Type workRecordType) {
        return (getAdapteeManager().supportsWorkRecordType(workRecordType));
    }


    /**
     *  Gets the supported <code> Work </code> search record types. 
     *
     *  @return a list containing the supported <code> Work </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getWorkSearchRecordTypes() {
        return (getAdapteeManager().getWorkSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Work </code> search record type is 
     *  supported. 
     *
     *  @param  workSearchRecordType a <code> Type </code> indicating a <code> 
     *          Work </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> workSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsWorkSearchRecordType(org.osid.type.Type workSearchRecordType) {
        return (getAdapteeManager().supportsWorkSearchRecordType(workSearchRecordType));
    }


    /**
     *  Gets the supported <code> Competency </code> record types. 
     *
     *  @return a list containing the supported <code> Competency </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompetencyRecordTypes() {
        return (getAdapteeManager().getCompetencyRecordTypes());
    }


    /**
     *  Tests if the given <code> Competency </code> record type is supported. 
     *
     *  @param  competencyRecordType a <code> Type </code> indicating a <code> 
     *          Competency </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> competencyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompetencyRecordType(org.osid.type.Type competencyRecordType) {
        return (getAdapteeManager().supportsCompetencyRecordType(competencyRecordType));
    }


    /**
     *  Gets the supported <code> Competency </code> search record types. 
     *
     *  @return a list containing the supported <code> Competency </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompetencySearchRecordTypes() {
        return (getAdapteeManager().getCompetencySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Competency </code> search record type is 
     *  supported. 
     *
     *  @param  competencySearchRecordType a <code> Type </code> indicating a 
     *          <code> Competency </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          competencSearchyRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompetencySearchRecordType(org.osid.type.Type competencySearchRecordType) {
        return (getAdapteeManager().supportsCompetencySearchRecordType(competencySearchRecordType));
    }


    /**
     *  Gets the supported <code> Availability </code> record types. 
     *
     *  @return a list containing the supported <code> Availability </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilityRecordTypes() {
        return (getAdapteeManager().getAvailabilityRecordTypes());
    }


    /**
     *  Tests if the given <code> Availability </code> record type is 
     *  supported. 
     *
     *  @param  availabilityRecordType a <code> Type </code> indicating an 
     *          <code> Availability </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> availabilityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAvailabilityRecordType(org.osid.type.Type availabilityRecordType) {
        return (getAdapteeManager().supportsAvailabilityRecordType(availabilityRecordType));
    }


    /**
     *  Gets the supported <code> Availability </code> search types. 
     *
     *  @return a list containing the supported <code> Availability </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilitySearchRecordTypes() {
        return (getAdapteeManager().getAvailabilitySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Availability </code> search type is 
     *  supported. 
     *
     *  @param  availabilitySearchRecordType a <code> Type </code> indicating 
     *          an <code> Availability </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilitySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAvailabilitySearchRecordType(org.osid.type.Type availabilitySearchRecordType) {
        return (getAdapteeManager().supportsAvailabilitySearchRecordType(availabilitySearchRecordType));
    }


    /**
     *  Gets the supported <code> Commission </code> record types. 
     *
     *  @return a list containing the supported <code> Commission </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionRecordTypes() {
        return (getAdapteeManager().getCommissionRecordTypes());
    }


    /**
     *  Tests if the given <code> Commission </code> record type is supported. 
     *
     *  @param  commissionRecordType a <code> Type </code> indicating a <code> 
     *          Commission </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commissionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommissionRecordType(org.osid.type.Type commissionRecordType) {
        return (getAdapteeManager().supportsCommissionRecordType(commissionRecordType));
    }


    /**
     *  Gets the supported <code> Commission </code> search record types. 
     *
     *  @return a list containing the supported <code> Commission </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionSearchRecordTypes() {
        return (getAdapteeManager().getCommissionSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Commission </code> search record type is 
     *  supported. 
     *
     *  @param  commissionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Commission </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommissionSearchRecordType(org.osid.type.Type commissionSearchRecordType) {
        return (getAdapteeManager().supportsCommissionSearchRecordType(commissionSearchRecordType));
    }


    /**
     *  Gets the supported <code> Effort </code> record types. 
     *
     *  @return a list containing the supported <code> Effort </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEffortRecordTypes() {
        return (getAdapteeManager().getEffortRecordTypes());
    }


    /**
     *  Tests if the given <code> Effort </code> record type is supported. 
     *
     *  @param  effortRecordType a <code> Type </code> indicating an <code> 
     *          Effort </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effortRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEffortRecordType(org.osid.type.Type effortRecordType) {
        return (getAdapteeManager().supportsEffortRecordType(effortRecordType));
    }


    /**
     *  Gets the supported <code> Effort </code> search types. 
     *
     *  @return a list containing the supported <code> Effort </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEffortSearchRecordTypes() {
        return (getAdapteeManager().getEffortSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Effort </code> search type is supported. 
     *
     *  @param  effortSearchRecordType a <code> Type </code> indicating an 
     *          <code> Effort </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effiortSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEffortSearchRecordType(org.osid.type.Type effortSearchRecordType) {
        return (getAdapteeManager().supportsEffortSearchRecordType(effortSearchRecordType));
    }


    /**
     *  Gets the supported <code> Foundry </code> record types. 
     *
     *  @return a list containing the supported <code> Foundry </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFoundryRecordTypes() {
        return (getAdapteeManager().getFoundryRecordTypes());
    }


    /**
     *  Tests if the given <code> Foundry </code> record type is supported. 
     *
     *  @param  foundryRecordType a <code> Type </code> indicating a <code> 
     *          Foundry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> foundryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFoundryRecordType(org.osid.type.Type foundryRecordType) {
        return (getAdapteeManager().supportsFoundryRecordType(foundryRecordType));
    }


    /**
     *  Gets the supported <code> Foundry </code> search record types. 
     *
     *  @return a list containing the supported <code> Foundry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFoundrySearchRecordTypes() {
        return (getAdapteeManager().getFoundrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Foundry </code> search record type is 
     *  supported. 
     *
     *  @param  foundrySearchRecordType a <code> Type </code> indicating a 
     *          <code> Foundry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> foundrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFoundrySearchRecordType(org.osid.type.Type foundrySearchRecordType) {
        return (getAdapteeManager().supportsFoundrySearchRecordType(foundrySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my work 
     *  service. 
     *
     *  @return a <code> MyWorkSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyWork() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.MyWorkSession getMyWorkSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyWorkSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my work 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @return a <code> MyWorkSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyWork() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.MyWorkSession getMyWorkSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyWorkSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job lookup 
     *  service. 
     *
     *  @return a <code> JobLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobLookupSession getJobLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobLookupSession getJobLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job query 
     *  service. 
     *
     *  @return a <code> JobQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuerySession getJobQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuerySession getJobQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job search 
     *  service. 
     *
     *  @return a <code> JobSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchSession getJobSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchSession getJobSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  administration service. 
     *
     *  @return a <code> JobAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobAdminSession getJobAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobAdminSession getJobAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  notification service. 
     *
     *  @param  jobReceiver the notification callback 
     *  @return a <code> JobNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobNotificationSession getJobNotificationSession(org.osid.resourcing.JobReceiver jobReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobNotificationSession(jobReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  notification service for the given foundry. 
     *
     *  @param  jobReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobReceiver </code> or 
     *          <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobNotificationSession getJobNotificationSessionForFoundry(org.osid.resourcing.JobReceiver jobReceiver, 
                                                                                          org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobNotificationSessionForFoundry(jobReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job/foundry mappings. 
     *
     *  @return a <code> JobFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobFoundrySession getJobFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning jobs to 
     *  foundries. 
     *
     *  @return a <code> JobFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobFoundryAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobFoundryAssignmentSession getJobFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSmartFoundrySession getJobSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service. 
     *
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkLookupSession getWorkLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkLookupSession getWorkLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service. 
     *
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuerySession getWorkQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuerySession getWorkQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service. 
     *
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSearchSession getWorkSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSearchSession getWorkSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service. 
     *
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkAdminSession getWorkAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkAdminSession getWorkAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service. 
     *
     *  @param  workReceiver the notification callback 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkNotificationSession getWorkNotificationSession(org.osid.resourcing.WorkReceiver workReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkNotificationSession(workReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the work 
     *  notification service for the given foundry. 
     *
     *  @param  workReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> workReceiver </code> or 
     *          <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkNotificationSession getWorkNotificationSessionForFoundry(org.osid.resourcing.WorkReceiver workReceiver, 
                                                                                            org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkNotificationSessionForFoundry(workReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup work/foundry mappings. 
     *
     *  @return a <code> WorkFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsWorkFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkFoundrySession getWorkFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning work to 
     *  foundries. 
     *
     *  @return a <code> WorkFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkbFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkFoundryAssignmentSession getWorkFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage work smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSmartFoundrySession getWorkSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  lookup service. 
     *
     *  @return a <code> CompetencyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyLookupSession getCompetencyLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyLookupSession getCompetencyLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  query service. 
     *
     *  @return a <code> CompetencyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuerySession getCompetencyQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuerySession getCompetencyQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  search service. 
     *
     *  @return a <code> CompetencySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchSession getCompetencySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencySearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchSession getCompetencySearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencySearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  administration service. 
     *
     *  @return a <code> CompetencyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyAdminSession getCompetencyAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyAdminSession getCompetencyAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  notification service. 
     *
     *  @param  competencyReceiver the notification callback 
     *  @return a <code> CompetencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> competencyReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyNotificationSession getCompetencyNotificationSession(org.osid.resourcing.CompetencyReceiver competencyReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyNotificationSession(competencyReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the competency 
     *  notification service for the given foundry. 
     *
     *  @param  competencyReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> competencyReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyNotificationSession getCompetencyNotificationSessionForFoundry(org.osid.resourcing.CompetencyReceiver competencyReceiver, 
                                                                                                        org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyNotificationSessionForFoundry(competencyReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup competency/foundry 
     *  mappings. 
     *
     *  @return a <code> CompetencyFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyFoundrySession getCompetencyFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to foundries. 
     *
     *  @return a <code> CompetencyFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyFoundryAssignmentSession getCompetencyFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencyFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage competency smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @return a <code> CompetencySmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencySmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySmartFoundrySession getCompetencySmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCompetencySmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  lookup service. 
     *
     *  @return an <code> AvailabilityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityLookupSession getAvailabilityLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the foundry 
     *  @return an <code> AvailabilityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityLookupSession getAvailabilityLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  query service. 
     *
     *  @return an <code> AvailabilityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuerySession getAvailabilityQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuerySession getAvailabilityQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  search service. 
     *
     *  @return an <code> AvailabilitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySearchSession getAvailabilitySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilitySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilitySearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySearchSession getAvailabilitySearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilitySearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  administration service. 
     *
     *  @return an <code> AvailabilityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityAdminSession getAvailabilityAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityAdminSession getAvailabilityAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  notification service. 
     *
     *  @param  availabilityReceiver the notification callback 
     *  @return an <code> AvailabilityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> availabilityReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityNotificationSession getAvailabilityNotificationSession(org.osid.resourcing.AvailabilityReceiver availabilityReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityNotificationSession(availabilityReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  notification service for the given foundry. 
     *
     *  @param  availabilityReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> availabilityReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityNotificationSession getAvailabilityNotificationSessionForFoundry(org.osid.resourcing.AvailabilityReceiver availabilityReceiver, 
                                                                                                            org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityNotificationSessionForFoundry(availabilityReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup availability/foundry 
     *  mappings. 
     *
     *  @return an <code> AvailabilityFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityFoundrySession getAvailabilityFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  availabilities to foundries. 
     *
     *  @return an <code> AvailabilityFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityFoundryAssignmentSession getAvailabilityFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilitySmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilitySmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilitySmartFoundrySession getAvailabilitySmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilitySmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  lookup service. 
     *
     *  @return a <code> CommissionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionLookupSession getCommissionLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionLookupSession getCommissionLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  query service. 
     *
     *  @return a <code> CommissionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuerySession getCommissionQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CCommissionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQuerySession getCommissionQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  search service. 
     *
     *  @return a <code> CommissionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSearchSession getCommissionSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  search service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSearchSession getCommissionSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  administrative service. 
     *
     *  @return a <code> CommissionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionAdminSession getCommissionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  administrative service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionAdminSession getCommissionAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  notification service. 
     *
     *  @param  commissionReceiver the notification callback 
     *  @return a <code> CommissionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commissionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionNotificationSession getCommissionNotificationSession(org.osid.resourcing.CommissionReceiver commissionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionNotificationSession(commissionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  notification service for the given foundry. 
     *
     *  @param  commissionReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> commissionReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionNotificationSession getCommissionNotificationSessionForFoundry(org.osid.resourcing.CommissionReceiver commissionReceiver, 
                                                                                                        org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionNotificationSessionForFoundry(commissionReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commission/foundry 
     *  mappings. 
     *
     *  @return a <code> CommissionFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionFoundrySession getCommissionFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to foundries. 
     *
     *  @return a <code> CommissionyFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionFoundryAssignmentSession getCommissionFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commission smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionSmartFoundrySession getCommissionSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort lookup 
     *  service. 
     *
     *  @return an <code> EffortLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortLookupSession getEffortLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort lookup 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortLookupSession getEffortLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort query 
     *  service. 
     *
     *  @return an <code> EffortQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuerySession getEffortQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort query 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortQuerySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuerySession getEffortQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort search 
     *  service. 
     *
     *  @return an <code> EffortSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortSearchSession getEffortSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort search 
     *  service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortSearchSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortSearchSession getEffortSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  administration service. 
     *
     *  @return an <code> EffortAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortAdminSession getEffortAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortAdminSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortAdminSession getEffortAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  notification service. 
     *
     *  @param  effortReceiver the notification callback 
     *  @return an <code> EffortNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> effortReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortNotificationSession getEffortNotificationSession(org.osid.resourcing.EffortReceiver effortReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortNotificationSession(effortReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the effort 
     *  notification service for the given foundry. 
     *
     *  @param  effortReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> effortReceiver </code> 
     *          or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortNotificationSession getEffortNotificationSessionForFoundry(org.osid.resourcing.EffortReceiver effortReceiver, 
                                                                                                org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortNotificationSessionForFoundry(effortReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup effort/foundry mappings. 
     *
     *  @return an <code> EffortFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEffortFoundry() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundrySession getEffortFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning efforts 
     *  to foundries. 
     *
     *  @return an <code> EffortFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortFoundryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundryAssignmentSession getEffortFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage effort smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortSmartFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortFoundrySession getEffortSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEffortSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry lookup 
     *  service. 
     *
     *  @return a <code> FoundryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryLookupSession getFoundryLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFoundryLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry query 
     *  service. 
     *
     *  @return a <code> FoundryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuerySession getFoundryQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFoundryQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry search 
     *  service. 
     *
     *  @return a <code> FoundrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundrySearchSession getFoundrySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFoundrySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  administrative service. 
     *
     *  @return a <code> FoundryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryAdminSession getFoundryAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFoundryAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  notification service. 
     *
     *  @param  foundryReceiver the notification callback 
     *  @return a <code> FoundryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> foundryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryNotificationSession getFoundryNotificationSession(org.osid.resourcing.FoundryReceiver foundryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFoundryNotificationSession(foundryReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  hierarchy service. 
     *
     *  @return a <code> FoundryHierarchySession </code> for foundries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryHierarchySession getFoundryHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFoundryHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the foundry 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for foundries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryHierarchyDesignSession getFoundryHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFoundryHierarchyDesignSession());
    }


    /**
     *  Gets the <code> ResourcingBatchManager. </code> 
     *
     *  @return a <code> ResourcingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.ResourcingBatchManager getResourcingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcingBatchManager());
    }


    /**
     *  Gets the <code> ResourcingRulesManager. </code> 
     *
     *  @return a <code> ResourcingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.ResourcingRulesManager getResourcingRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcingRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
