//
// AbstractAdapterSequenceRuleEnablerLookupSession.java
//
//    A SequenceRuleEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SequenceRuleEnabler lookup session adapter.
 */

public abstract class AbstractAdapterSequenceRuleEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.authoring.SequenceRuleEnablerLookupSession {

    private final org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSequenceRuleEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSequenceRuleEnablerLookupSession(org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@code Bank} associated with this session.
     *
     *  @return the {@code Bank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@code SequenceRuleEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSequenceRuleEnablers() {
        return (this.session.canLookupSequenceRuleEnablers());
    }


    /**
     *  A complete view of the {@code SequenceRuleEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSequenceRuleEnablerView() {
        this.session.useComparativeSequenceRuleEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code SequenceRuleEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySequenceRuleEnablerView() {
        this.session.usePlenarySequenceRuleEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rule enablers in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    

    /**
     *  Only active sequence rule enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveSequenceRuleEnablerView() {
        this.session.useActiveSequenceRuleEnablerView();
        return;
    }


    /**
     *  Active and inactive sequence rule enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusSequenceRuleEnablerView() {
        this.session.useAnyStatusSequenceRuleEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code SequenceRuleEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SequenceRuleEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SequenceRuleEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param sequenceRuleEnablerId {@code Id} of the {@code
     *         SequenceRuleEnabler}
     *  @return the sequence rule enabler
     *  @throws org.osid.NotFoundException {@code sequenceRuleEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code sequenceRuleEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnabler getSequenceRuleEnabler(org.osid.id.Id sequenceRuleEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRuleEnabler(sequenceRuleEnablerId));
    }


    /**
     *  Gets a {@code SequenceRuleEnablerList} corresponding to the
     *  given {@code IdList}.
     *
     *  In plenary mode, the returned list contains all of the
     *  sequenceRuleEnablers specified in the {@code Id} list, in the
     *  order of the list, including duplicates, or an error results
     *  if an {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code
     *  SequenceRuleEnablers} may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SequenceRuleEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByIds(org.osid.id.IdList sequenceRuleEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRuleEnablersByIds(sequenceRuleEnablerIds));
    }


    /**
     *  Gets a {@code SequenceRuleEnablerList} corresponding to the
     *  given sequence rule enabler genus {@code Type} which does not
     *  include sequence rule enablers of types derived from the
     *  specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerGenusType a sequenceRuleEnabler genus type 
     *  @return the returned {@code SequenceRuleEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByGenusType(org.osid.type.Type sequenceRuleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRuleEnablersByGenusType(sequenceRuleEnablerGenusType));
    }


    /**
     *  Gets a {@code SequenceRuleEnablerList} corresponding to the given
     *  sequence rule enabler genus {@code Type} and include any additional
     *  sequence rule enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerGenusType a sequenceRuleEnabler genus type 
     *  @return the returned {@code SequenceRuleEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleEnablerGenusType} is {@code }
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByParentGenusType(org.osid.type.Type sequenceRuleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRuleEnablersByParentGenusType(sequenceRuleEnablerGenusType));
    }


    /**
     *  Gets a {@code SequenceRuleEnablerList} containing the given
     *  sequence rule enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerRecordType a sequenceRuleEnabler record type 
     *  @return the returned {@code SequenceRuleEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code sequenceRuleEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByRecordType(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRuleEnablersByRecordType(sequenceRuleEnablerRecordType));
    }


    /**
     *  Gets a {@code SequenceRuleEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session.
     *  
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code SequenceRuleEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRuleEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code SequenceRuleEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rule enablers or an error results. Otherwise, the returned list
     *  may contain only those sequence rule enablers that are accessible
     *  through this session.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code SequenceRuleEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getSequenceRuleEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code SequenceRuleEnablers}. 
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @return a list of {@code SequenceRuleEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSequenceRuleEnablers());
    }
}
