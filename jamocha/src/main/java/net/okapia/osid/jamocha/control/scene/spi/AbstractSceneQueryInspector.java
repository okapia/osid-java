//
// AbstractSceneQueryInspector.java
//
//     A template for making a SceneQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.scene.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for scenes.
 */

public abstract class AbstractSceneQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.control.SceneQueryInspector {

    private final java.util.Collection<org.osid.control.records.SceneQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the setting <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSettingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the setting query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SettingQueryInspector[] getSettingTerms() {
        return (new org.osid.control.SettingQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given scene query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a scene implementing the requested record.
     *
     *  @param sceneRecordType a scene record type
     *  @return the scene query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sceneRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SceneQueryInspectorRecord getSceneQueryInspectorRecord(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SceneQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(sceneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sceneRecordType + " is not supported");
    }


    /**
     *  Adds a record to this scene query. 
     *
     *  @param sceneQueryInspectorRecord scene query inspector
     *         record
     *  @param sceneRecordType scene record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSceneQueryInspectorRecord(org.osid.control.records.SceneQueryInspectorRecord sceneQueryInspectorRecord, 
                                                   org.osid.type.Type sceneRecordType) {

        addRecordType(sceneRecordType);
        nullarg(sceneRecordType, "scene record type");
        this.records.add(sceneQueryInspectorRecord);        
        return;
    }
}
