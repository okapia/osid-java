//
// AbstractMapLocationLookupSession
//
//    A simple framework for providing a Location lookup service
//    backed by a fixed collection of locations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Location lookup service backed by a
 *  fixed collection of locations. The locations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Locations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapLocationLookupSession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.Location> locations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.Location>());


    /**
     *  Makes a <code>Location</code> available in this session.
     *
     *  @param  location a location
     *  @throws org.osid.NullArgumentException <code>location<code>
     *          is <code>null</code>
     */

    protected void putLocation(org.osid.mapping.Location location) {
        this.locations.put(location.getId(), location);
        return;
    }


    /**
     *  Makes an array of locations available in this session.
     *
     *  @param  locations an array of locations
     *  @throws org.osid.NullArgumentException <code>locations<code>
     *          is <code>null</code>
     */

    protected void putLocations(org.osid.mapping.Location[] locations) {
        putLocations(java.util.Arrays.asList(locations));
        return;
    }


    /**
     *  Makes a collection of locations available in this session.
     *
     *  @param  locations a collection of locations
     *  @throws org.osid.NullArgumentException <code>locations<code>
     *          is <code>null</code>
     */

    protected void putLocations(java.util.Collection<? extends org.osid.mapping.Location> locations) {
        for (org.osid.mapping.Location location : locations) {
            this.locations.put(location.getId(), location);
        }

        return;
    }


    /**
     *  Removes a Location from this session.
     *
     *  @param  locationId the <code>Id</code> of the location
     *  @throws org.osid.NullArgumentException <code>locationId<code> is
     *          <code>null</code>
     */

    protected void removeLocation(org.osid.id.Id locationId) {
        this.locations.remove(locationId);
        return;
    }


    /**
     *  Gets the <code>Location</code> specified by its <code>Id</code>.
     *
     *  @param  locationId <code>Id</code> of the <code>Location</code>
     *  @return the location
     *  @throws org.osid.NotFoundException <code>locationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>locationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.Location location = this.locations.get(locationId);
        if (location == null) {
            throw new org.osid.NotFoundException("location not found: " + locationId);
        }

        return (location);
    }


    /**
     *  Gets all <code>Locations</code>. In plenary mode, the returned
     *  list contains all known locations or an error
     *  results. Otherwise, the returned list may contain only those
     *  locations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Locations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.location.ArrayLocationList(this.locations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.locations.clear();
        super.close();
        return;
    }
}
