//
// AbstractConferral.java
//
//     Defines a Conferral builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recognition.conferral.spi;


/**
 *  Defines a <code>Conferral</code> builder.
 */

public abstract class AbstractConferralBuilder<T extends AbstractConferralBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.recognition.conferral.ConferralMiter conferral;


    /**
     *  Constructs a new <code>AbstractConferralBuilder</code>.
     *
     *  @param conferral the conferral to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractConferralBuilder(net.okapia.osid.jamocha.builder.recognition.conferral.ConferralMiter conferral) {
        super(conferral);
        this.conferral = conferral;
        return;
    }


    /**
     *  Builds the conferral.
     *
     *  @return the new conferral
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.recognition.Conferral build() {
        (new net.okapia.osid.jamocha.builder.validator.recognition.conferral.ConferralValidator(getValidations())).validate(this.conferral);
        return (new net.okapia.osid.jamocha.builder.recognition.conferral.ImmutableConferral(this.conferral));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the conferral miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.recognition.conferral.ConferralMiter getMiter() {
        return (this.conferral);
    }


    /**
     *  Sets the award.
     *
     *  @param award an award
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>award</code> is
     *          <code>null</code>
     */

    public T award(org.osid.recognition.Award award) {
        getMiter().setAward(award);
        return (self());
    }


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>recipient</code>
     *          is <code>null</code>
     */

    public T recipient(org.osid.resource.Resource recipient) {
        getMiter().setRecipient(recipient);
        return (self());
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    public T referenceId(org.osid.id.Id referenceId) {
        getMiter().setReferenceId(referenceId);
        return (self());
    }


    /**
     *  Sets the convocation.
     *
     *  @param convocation a convocation
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>convocation</code> is <code>null</code>
     */

    public T convocation(org.osid.recognition.Convocation convocation) {
        getMiter().setConvocation(convocation);
        return (self());
    }


    /**
     *  Adds a Conferral record.
     *
     *  @param record a conferral record
     *  @param recordType the type of conferral record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.recognition.records.ConferralRecord record, org.osid.type.Type recordType) {
        getMiter().addConferralRecord(record, recordType);
        return (self());
    }
}       


