//
// AbstractImmutableFoundry.java
//
//     Wraps a mutable Foundry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.foundry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Foundry</code> to hide modifiers. This
 *  wrapper provides an immutized Foundry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying foundry whose state changes are visible.
 */

public abstract class AbstractImmutableFoundry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.resourcing.Foundry {

    private final org.osid.resourcing.Foundry foundry;


    /**
     *  Constructs a new <code>AbstractImmutableFoundry</code>.
     *
     *  @param foundry the foundry to immutablize
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableFoundry(org.osid.resourcing.Foundry foundry) {
        super(foundry);
        this.foundry = foundry;
        return;
    }


    /**
     *  Gets the foundry record corresponding to the given <code> Foundry 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> foundryRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(foundryRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  foundryRecordType the type of foundry record to retrieve 
     *  @return the foundry record 
     *  @throws org.osid.NullArgumentException <code> foundryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(foundryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.records.FoundryRecord getFoundryRecord(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException {

        return (this.foundry.getFoundryRecord(foundryRecordType));
    }
}

