//
// AbstractActivityRegistrationNotificationSession.java
//
//     A template for making ActivityRegistrationNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code ActivityRegistration} objects. This session
 *  is intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code ActivityRegistration} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for activity registration entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractActivityRegistrationNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.registration.ActivityRegistrationNotificationSession {

    private boolean federated = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }

    
    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the {@code CourseCatalog}.
     *
     *  @param courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  ActivityRegistration} notifications.  A return of true does
     *  not guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForActivityRegistrationNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeActivityRegistrationNotification() </code>.
     */

    @OSID @Override
    public void reliableActivityRegistrationNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableActivityRegistrationNotifications() {
        return;
    }


    /**
     *  Acknowledge an activity registration notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeActivityRegistrationNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for activity bundles in course
     *  catalogs which are children of this course catalog in the
     *  course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new activity
     *  registrations. {@code
     *  ActivityRegistrationReceiver.newActivityRegistration()} is
     *  invoked when an new {@code ActivityRegistration} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewActivityRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new registrations for the given
     *  registration. {@code
     *  ActivityRegistrationReceiver.newActivityRegistration()} is
     *  invoked when a new {@code ActivityRegistration} appears in
     *  this course catalog.
     *
     *  @param  registrationId the {@code Id} of the {@code 
     *          Registration} to monitor 
     *  @throws org.osid.NullArgumentException {@code registrationId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewActivityRegistrationsForRegistration(org.osid.id.Id registrationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new activityRegistrations for
     *  the given activity {@code Id}. {@code
     *  ActivityRegistrationReceiver.newActivityRegistration()} is
     *  invoked when a new {@code ActivityRegistration} is created.
     *
     *  @param  activityId the {@code Id} of the activity to monitor
     *  @throws org.osid.NullArgumentException {@code activityId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewActivityRegistrationsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new activityRegistrations for
     *  the given student {@code Id}. {@code
     *  ActivityRegistrationReceiver.newActivityRegistration()} is
     *  invoked when a new {@code ActivityRegistration} is created.
     *
     *  @param  resourceId the {@code Id} of the student to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewActivityRegistrationsForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated activity
     *  registrations. {@code
     *  ActivityRegistrationReceiver.changedActivityRegistration()} is
     *  invoked when an activity registration is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedActivityRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated activity registrations
     *  for the given registration. {@code
     *  ActivityRegistrationReceiver.changedActivityRegistration()} is
     *  invoked when a course in this course catalog is changed.
     *
     *  @param  registrationId the {@code Id} of the {@code 
     *          Regitsration} to monitor 
     *  @throws org.osid.NullArgumentException {@code registrationId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedActivityRegistrationsForRegistration(org.osid.id.Id registrationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated activityRegistrations
     *  for the given activity {@code Id}. {@code
     *  ActivityRegistrationReceiver.changedActivityRegistration()} is
     *  invoked when a {@code ActivityRegistration} in this
     *  courseCatalog is changed.
     *
     *  @param  activityId the {@code Id} of the activity to monitor
     *  @throws org.osid.NullArgumentException {@code activityId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedActivityRegistrationsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated activityRegistrations
     *  for the given student {@code Id}. {@code
     *  ActivityRegistrationReceiver.changedActivityRegistration()} is
     *  invoked when a {@code ActivityRegistration} in this
     *  courseCatalog is changed.
     *
     *  @param  resourceId the {@code Id} of the student to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedActivityRegistrationsForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated activity
     *  registration. {@code
     *  ActivityRegistrationReceiver.changedActivityRegistration()} is
     *  invoked when the specified activity registration is changed.
     *
     *  @param activityRegistrationId the {@code Id} of the {@code ActivityRegistration} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code activityRegistrationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedActivityRegistration(org.osid.id.Id activityRegistrationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted activity
     *  registrations. {@code
     *  ActivityRegistrationReceiver.deletedActivityRegistration()} is
     *  invoked when an activity registration is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedActivityRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted activity registrations
     *  for the given registration. {@code
     *  ActivityRegistrationReceiver.deletedActivityRegistration()} is
     *  invoked when an activity registration is deleted or removed
     *  from this course catalog.
     *
     *  @param registrationId the {@code Id} of the {@code
     *          Registration} to monitor
     *  @throws org.osid.NullArgumentException {@code registrationId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedActivityRegistrationsForRegistration(org.osid.id.Id registrationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted activityRegistrations
     *  for the given activity {@code Id}. {@code
     *  ActivityRegistrationReceiver.deletedActivityRegistration()} is
     *  invoked when a {@code ActivityRegistration} is deleted or
     *  removed from this courseCatalog.
     *
     *  @param  activityId the {@code Id} of the activity to monitor
     *  @throws org.osid.NullArgumentException {@code activityId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedActivityRegistrationsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted activityRegistrations
     *  for the given student {@code Id}. {@code
     *  ActivityRegistrationReceiver.deletedActivityRegistration()} is
     *  invoked when a {@code ActivityRegistration} is deleted or
     *  removed from this courseCatalog.
     *
     *  @param  resourceId the {@code Id} of the student to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedActivityRegistrationsForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted activity
     *  registration. {@code
     *  ActivityRegistrationReceiver.deletedActivityRegistration()} is
     *  invoked when the specified activity registration is deleted.
     *
     *  @param activityRegistrationId the {@code Id} of the
     *          {@code ActivityRegistration} to monitor
     *  @throws org.osid.NullArgumentException {@code activityRegistrationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedActivityRegistration(org.osid.id.Id activityRegistrationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
