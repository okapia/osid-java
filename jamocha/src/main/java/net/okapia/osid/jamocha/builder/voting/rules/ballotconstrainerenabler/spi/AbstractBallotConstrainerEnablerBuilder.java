//
// AbstractBallotConstrainerEnabler.java
//
//     Defines a BallotConstrainerEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainerenabler.spi;


/**
 *  Defines a <code>BallotConstrainerEnabler</code> builder.
 */

public abstract class AbstractBallotConstrainerEnablerBuilder<T extends AbstractBallotConstrainerEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainerenabler.BallotConstrainerEnablerMiter ballotConstrainerEnabler;


    /**
     *  Constructs a new <code>AbstractBallotConstrainerEnablerBuilder</code>.
     *
     *  @param ballotConstrainerEnabler the ballot constrainer enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBallotConstrainerEnablerBuilder(net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainerenabler.BallotConstrainerEnablerMiter ballotConstrainerEnabler) {
        super(ballotConstrainerEnabler);
        this.ballotConstrainerEnabler = ballotConstrainerEnabler;
        return;
    }


    /**
     *  Builds the ballot constrainer enabler.
     *
     *  @return the new ballot constrainer enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.voting.rules.BallotConstrainerEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.voting.rules.ballotconstrainerenabler.BallotConstrainerEnablerValidator(getValidations())).validate(this.ballotConstrainerEnabler);
        return (new net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainerenabler.ImmutableBallotConstrainerEnabler(this.ballotConstrainerEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the ballot constrainer enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.voting.rules.ballotconstrainerenabler.BallotConstrainerEnablerMiter getMiter() {
        return (this.ballotConstrainerEnabler);
    }


    /**
     *  Adds a BallotConstrainerEnabler record.
     *
     *  @param record a ballot constrainer enabler record
     *  @param recordType the type of ballot constrainer enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.voting.rules.records.BallotConstrainerEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addBallotConstrainerEnablerRecord(record, recordType);
        return (self());
    }
}       


