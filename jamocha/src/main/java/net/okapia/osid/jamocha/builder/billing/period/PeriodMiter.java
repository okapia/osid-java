//
// PeriodMiter.java
//
//     Defines a Period miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.period;


/**
 *  Defines a <code>Period</code> miter for use with the builders.
 */

public interface PeriodMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.billing.Period {


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    public void setDisplayLabel(org.osid.locale.DisplayText label);


    /**
     *  Sets the open date.
     *
     *  @param date an open date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setOpenDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the close date.
     *
     *  @param date a close date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setCloseDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the billing date.
     *
     *  @param date a billing date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setBillingDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDueDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a Period record.
     *
     *  @param record a period record
     *  @param recordType the type of period record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPeriodRecord(org.osid.billing.records.PeriodRecord record, org.osid.type.Type recordType);
}       


