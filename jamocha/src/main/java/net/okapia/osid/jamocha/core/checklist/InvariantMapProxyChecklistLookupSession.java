//
// InvariantMapProxyChecklistLookupSession
//
//    Implements a Checklist lookup service backed by a fixed
//    collection of checklists. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist;


/**
 *  Implements a Checklist lookup service backed by a fixed
 *  collection of checklists. The checklists are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyChecklistLookupSession
    extends net.okapia.osid.jamocha.core.checklist.spi.AbstractMapChecklistLookupSession
    implements org.osid.checklist.ChecklistLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyChecklistLookupSession} with no
     *  checklists.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyChecklistLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyChecklistLookupSession} with a
     *  single checklist.
     *
     *  @param checklist a single checklist
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyChecklistLookupSession(org.osid.checklist.Checklist checklist, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putChecklist(checklist);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyChecklistLookupSession} using
     *  an array of checklists.
     *
     *  @param checklists an array of checklists
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklists} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyChecklistLookupSession(org.osid.checklist.Checklist[] checklists, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putChecklists(checklists);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyChecklistLookupSession} using a
     *  collection of checklists.
     *
     *  @param checklists a collection of checklists
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklists} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyChecklistLookupSession(java.util.Collection<? extends org.osid.checklist.Checklist> checklists,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putChecklists(checklists);
        return;
    }
}
