//
// AbstractShipmentSearch.java
//
//     A template for making a Shipment Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.shipment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing shipment searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractShipmentSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inventory.shipment.ShipmentSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inventory.shipment.ShipmentSearchOrder shipmentSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of shipments. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  shipmentIds list of shipments
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongShipments(org.osid.id.IdList shipmentIds) {
        while (shipmentIds.hasNext()) {
            try {
                this.ids.add(shipmentIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongShipments</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of shipment Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getShipmentIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  shipmentSearchOrder shipment search order 
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>shipmentSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderShipmentResults(org.osid.inventory.shipment.ShipmentSearchOrder shipmentSearchOrder) {
	this.shipmentSearchOrder = shipmentSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inventory.shipment.ShipmentSearchOrder getShipmentSearchOrder() {
	return (this.shipmentSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given shipment search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a shipment implementing the requested record.
     *
     *  @param shipmentSearchRecordType a shipment search record
     *         type
     *  @return the shipment search record
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(shipmentSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentSearchRecord getShipmentSearchRecord(org.osid.type.Type shipmentSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inventory.shipment.records.ShipmentSearchRecord record : this.records) {
            if (record.implementsRecordType(shipmentSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(shipmentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this shipment search. 
     *
     *  @param shipmentSearchRecord shipment search record
     *  @param shipmentSearchRecordType shipment search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addShipmentSearchRecord(org.osid.inventory.shipment.records.ShipmentSearchRecord shipmentSearchRecord, 
                                           org.osid.type.Type shipmentSearchRecordType) {

        addRecordType(shipmentSearchRecordType);
        this.records.add(shipmentSearchRecord);        
        return;
    }
}
