//
// AbstractIndexedMapFiscalPeriodLookupSession.java
//
//    A simple framework for providing a FiscalPeriod lookup service
//    backed by a fixed collection of fiscal periods with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a FiscalPeriod lookup service backed by a
 *  fixed collection of fiscal periods. The fiscal periods are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some fiscal periods may be compatible
 *  with more types than are indicated through these fiscal period
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>FiscalPeriods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapFiscalPeriodLookupSession
    extends AbstractMapFiscalPeriodLookupSession
    implements org.osid.financials.FiscalPeriodLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.financials.FiscalPeriod> fiscalPeriodsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.FiscalPeriod>());
    private final MultiMap<org.osid.type.Type, org.osid.financials.FiscalPeriod> fiscalPeriodsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.FiscalPeriod>());


    /**
     *  Makes a <code>FiscalPeriod</code> available in this session.
     *
     *  @param  fiscalPeriod a fiscal period
     *  @throws org.osid.NullArgumentException <code>fiscalPeriod<code> is
     *          <code>null</code>
     */

    @Override
    protected void putFiscalPeriod(org.osid.financials.FiscalPeriod fiscalPeriod) {
        super.putFiscalPeriod(fiscalPeriod);

        this.fiscalPeriodsByGenus.put(fiscalPeriod.getGenusType(), fiscalPeriod);
        
        try (org.osid.type.TypeList types = fiscalPeriod.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.fiscalPeriodsByRecord.put(types.getNextType(), fiscalPeriod);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a fiscal period from this session.
     *
     *  @param fiscalPeriodId the <code>Id</code> of the fiscal period
     *  @throws org.osid.NullArgumentException <code>fiscalPeriodId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeFiscalPeriod(org.osid.id.Id fiscalPeriodId) {
        org.osid.financials.FiscalPeriod fiscalPeriod;
        try {
            fiscalPeriod = getFiscalPeriod(fiscalPeriodId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.fiscalPeriodsByGenus.remove(fiscalPeriod.getGenusType());

        try (org.osid.type.TypeList types = fiscalPeriod.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.fiscalPeriodsByRecord.remove(types.getNextType(), fiscalPeriod);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeFiscalPeriod(fiscalPeriodId);
        return;
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> corresponding to the given
     *  fiscal period genus <code>Type</code> which does not include
     *  fiscal periods of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known fiscal periods or an error results. Otherwise,
     *  the returned list may contain only those fiscal periods that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  fiscalPeriodGenusType a fiscal period genus type 
     *  @return the returned <code>FiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByGenusType(org.osid.type.Type fiscalPeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.fiscalperiod.ArrayFiscalPeriodList(this.fiscalPeriodsByGenus.get(fiscalPeriodGenusType)));
    }


    /**
     *  Gets a <code>FiscalPeriodList</code> containing the given
     *  fiscal period record <code>Type</code>. In plenary mode, the
     *  returned list contains all known fiscal periods or an error
     *  results. Otherwise, the returned list may contain only those
     *  fiscal periods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  fiscalPeriodRecordType a fiscal period record type 
     *  @return the returned <code>fiscalPeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByRecordType(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.fiscalperiod.ArrayFiscalPeriodList(this.fiscalPeriodsByRecord.get(fiscalPeriodRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.fiscalPeriodsByGenus.clear();
        this.fiscalPeriodsByRecord.clear();

        super.close();

        return;
    }
}
