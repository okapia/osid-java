//
// AbstractQueryRelationshipLookupSession.java
//
//    A RelationshipQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RelationshipQuerySession adapter.
 */

public abstract class AbstractAdapterRelationshipQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.relationship.RelationshipQuerySession {

    private final org.osid.relationship.RelationshipQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterRelationshipQuerySession.
     *
     *  @param session the underlying relationship query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRelationshipQuerySession(org.osid.relationship.RelationshipQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeFamily</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeFamily Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.session.getFamilyId());
    }


    /**
     *  Gets the {@codeFamily</code> associated with this 
     *  session.
     *
     *  @return the {@codeFamily</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFamily());
    }


    /**
     *  Tests if this user can perform {@codeRelationship</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchRelationships() {
        return (this.session.canSearchRelationships());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationships in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.session.useFederatedFamilyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this family only.
     */
    
    @OSID @Override
    public void useIsolatedFamilyView() {
        this.session.useIsolatedFamilyView();
        return;
    }
    
      
    /**
     *  Gets a relationship query. The returned query will not have an
     *  extension query.
     *
     *  @return the relationship query 
     */
      
    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRelationshipQuery() {
        return (this.session.getRelationshipQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  relationshipQuery the relationship query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code relationshipQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code relationshipQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByQuery(org.osid.relationship.RelationshipQuery relationshipQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getRelationshipsByQuery(relationshipQuery));
    }
}
