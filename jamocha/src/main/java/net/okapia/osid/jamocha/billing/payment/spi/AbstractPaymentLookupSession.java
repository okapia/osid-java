//
// AbstractPaymentLookupSession.java
//
//    A starter implementation framework for providing a Payment
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Payment
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPayments(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPaymentLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.billing.payment.PaymentLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }

    /**
     *  Tests if this user can perform <code>Payment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPayments() {
        return (true);
    }


    /**
     *  A complete view of the <code>Payment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePaymentView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Payment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPaymentView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include payments in businesses which are
     *  children of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Payment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Payment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Payment</code> and
     *  retained for compatibility.
     *
     *  @param  paymentId <code>Id</code> of the
     *          <code>Payment</code>
     *  @return the payment
     *  @throws org.osid.NotFoundException <code>paymentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>paymentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payment getPayment(org.osid.id.Id paymentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.billing.payment.PaymentList payments = getPayments()) {
            while (payments.hasNext()) {
                org.osid.billing.payment.Payment payment = payments.getNextPayment();
                if (payment.getId().equals(paymentId)) {
                    return (payment);
                }
            }
        } 

        throw new org.osid.NotFoundException(paymentId + " not found");
    }


    /**
     *  Gets a <code>PaymentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  payments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Payments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPayments()</code>.
     *
     *  @param  paymentIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>paymentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByIds(org.osid.id.IdList paymentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.payment.Payment> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = paymentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPayment(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("payment " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.billing.payment.payment.LinkedPaymentList(ret));
    }


    /**
     *  Gets a <code>PaymentList</code> corresponding to the given
     *  payment genus <code>Type</code> which does not include
     *  payments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPayments()</code>.
     *
     *  @param  paymentGenusType a payment genus type 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByGenusType(org.osid.type.Type paymentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentGenusFilterList(getPayments(), paymentGenusType));
    }


    /**
     *  Gets a <code>PaymentList</code> corresponding to the given
     *  payment genus <code>Type</code> and include any additional
     *  payments with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPayments()</code>.
     *
     *  @param  paymentGenusType a payment genus type 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByParentGenusType(org.osid.type.Type paymentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPaymentsByGenusType(paymentGenusType));
    }


    /**
     *  Gets a <code>PaymentList</code> containing the given
     *  payment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPayments()</code>.
     *
     *  @param  paymentRecordType a payment record type 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByRecordType(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentRecordFilterList(getPayments(), paymentRecordType));
    }


    /**
     *  Gets a <code> PaymentList </code> in the given period. In
     *  plenary mode, the returned list contains all known payments or
     *  an error results. Otherwise, the returned list may contain
     *  only those payments that are accessible through this session.
     *
     *  @param  periodId a period <code> Id </code>
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.NullArgumentException <code> periodId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriod(org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new PeriodFilter(periodId), getPayments()));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer. In
     *  plenary mode, the returned list contains all known payments or
     *  an error results. Otherwise, the returned list may contain
     *  only those payments that are accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code>
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.NullArgumentException <code> payerId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayer(org.osid.id.Id payerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new PayerFilter(payerId), getPayments()));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer in a
     *  billing period. In plenary mode, the returned list contains
     *  all known payments or an error results. Otherwise, the
     *  returned list may contain only those payments that are
     *  accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code>
     *  @param  periodId a period <code> Id </code>
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.NullArgumentException <code> payerId </code> or
     *          <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForPayer(org.osid.id.Id payerId,
                                                                            org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new PeriodFilter(periodId), getPaymentsForPayer(payerId)));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer made
     *  within the given date range inclusive. In plenary mode, the
     *  returned list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> payerId </code> or
     *          <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerOnDate(org.osid.id.Id payerId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new DateFilter(from, to), getPaymentsForPayer(payerId)));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given customer. In
     *  plenary mode, the returned list contains all known payments or
     *  an error results. Otherwise, the returned list may contain
     *  only those payments that are accessible through this session.
     *
     *  @param  customerId a customer <code> Id </code>
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.NullArgumentException <code> customerId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForCustomer(org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new CustomerFilter(customerId), getPayments()));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given customer in a
     *  billing period. In plenary mode, the returned list contains
     *  all known payments or an error results. Otherwise, the
     *  returned list may contain only those payments that are
     *  accessible through this session.
     *
     *  @param  customerId a customer <code> Id </code>
     *  @param  periodId a period <code> Id </code>
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.NullArgumentException <code> customerId </code> or
     *          <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForCustomer(org.osid.id.Id customerId,
                                                                               org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new PeriodFilter(periodId), getPaymentsForCustomer(customerId)));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given customer made
     *  within the given date range inclusive. In plenary mode, the
     *  returned list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  customerId a customer <code> Id </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> customerId </code> or
     *          <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForCustomerOnDate(org.osid.id.Id customerId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new DateFilter(from, to), getPaymentsForCustomer(customerId)));
    }

    
    /**
     *  Gets a <code>PaymentList</code> for the given payer and
     *  customer. In plenary mode, the returned list contains all
     *  known payments or an error results. Otherwise, the returned
     *  list may contain only those payments that are accessible
     *  through this session.
     *
     *  @param  payerId a payer <code> Id </code>
     *  @param  customerId a customerId <code> Id </code>
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.NullArgumentException <code> payerId </code> or
     *          <code> customerId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerAndCustomer(org.osid.id.Id payerId,
                                                                               org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new PayerFilter(payerId), getPaymentsForCustomer(customerId)));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer and
     *  customer in a billing period. In plenary mode, the returned
     *  list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code>
     *  @param  customerId a customerId <code> Id </code>
     *  @param  periodId a period <code> Id </code>
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.NullArgumentException <code> payerId, customerId,
     *          </code> or <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForPayerAndCustomer(org.osid.id.Id payerId,
                                                                                       org.osid.id.Id customerId,
                                                                                       org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new PeriodFilter(periodId), getPaymentsForPayerAndCustomer(payerId, customerId)));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given customer and payer
     *  made within the given date range inclusive. In plenary mode, the
     *  returned list contains all known payments or an error results.
     *  Otherwise, the returned list may contain only those payments that are
     *  accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code>
     *  @param  customerId a customerId <code> Id </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Payment </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> payerId, customerId,
     *          </code> or <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerAndCustomerOnDate(org.osid.id.Id payerId,
                                                                                     org.osid.id.Id customerId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilterList(new DateFilter(from, to), getPaymentsForPayerAndCustomer(payerId, customerId)));
    }


    /**
     *  Gets all <code>Payments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Payments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.billing.payment.PaymentList getPayments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the payment list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of payments
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.billing.payment.PaymentList filterPaymentsOnViews(org.osid.billing.payment.PaymentList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }


    public static class PeriodFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilter {

        private final org.osid.id.Id periodId;


        /**
         *  Constructs a new <code>PeriodFilter</code>.
         *
         *  @param periodId the period to filter
         *  @throws org.osid.NullArgumentException
         *          <code>periodId</code> is <code>null</code>
         */

        public PeriodFilter(org.osid.id.Id periodId) {
            nullarg(periodId, "period Id");
            this.periodId = periodId;
            return;
        }


        /**
         *  Used by the PaymentFilterList to filter the payment list
         *  based on period.
         *
         *  @param payment the payment
         *  @return <code>true</code> to pass the payment,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.billing.payment.Payment payment) {
            return (payment.getPeriodId().equals(this.periodId));
        }
    }


    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;


        /**
         *  Constructs a new <code>DateFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public DateFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "starting date");
            nullarg(to, "ending date");

            this.from = from;
            this.to = to;

            return;
        }


        /**
         *  Used by the PaymentFilterList to filter the payment list
         *  based on date..
         *
         *  @param payment the payment
         *  @return <code>true</code> to pass the payment,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.billing.payment.Payment payment) {
            if (payment.getPaymentDate().isLess(this.from)) {
                return (false);
            }

            if (payment.getPaymentDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }


    public static class PayerFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilter {

        private final org.osid.id.Id payerId;


        /**
         *  Constructs a new <code>PayerFilter</code>.
         *
         *  @param payerId the payer to filter
         *  @throws org.osid.NullArgumentException
         *          <code>payerId</code> is <code>null</code>
         */

        public PayerFilter(org.osid.id.Id payerId) {
            nullarg(payerId, "payer Id");
            this.payerId = payerId;
            return;
        }


        /**
         *  Used by the PaymentFilterList to filter the
         *  payment list based on payer.
         *
         *  @param payment the payment
         *  @return <code>true</code> to pass the payment,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.billing.payment.Payment payment) {
            return (payment.getPayerId().equals(this.payerId));
        }
    }


    public static class CustomerFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.payment.payment.PaymentFilter {

        private final org.osid.id.Id customerId;


        /**
         *  Constructs a new <code>CustomerFilter</code>.
         *
         *  @param customerId the customer to filter
         *  @throws org.osid.NullArgumentException
         *          <code>customerId</code> is <code>null</code>
         */

        public CustomerFilter(org.osid.id.Id customerId) {
            nullarg(customerId, "customer Id");
            this.customerId = customerId;
            return;
        }


        /**
         *  Used by the PaymentFilterList to filter the
         *  payment list based on customer.
         *
         *  @param payment the payment
         *  @return <code>true</code> to pass the payment,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.billing.payment.Payment payment) {
            return (payment.getCustomerId().equals(this.customerId));
        }
    }
}
