//
// AbstractFederatingLeaseLookupSession.java
//
//     An abstract federating adapter for a LeaseLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a LeaseLookupSession. Sessions
 *  are added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingLeaseLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.room.squatting.LeaseLookupSession>
    implements org.osid.room.squatting.LeaseLookupSession {

    private boolean parallel = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();


    /**
     *  Constructs a new <code>AbstractFederatingLeaseLookupSession</code>.
     */

    protected AbstractFederatingLeaseLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.room.squatting.LeaseLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Lease</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLeases() {
        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            if (session.canLookupLeases()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Lease</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLeaseView() {
        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            session.useComparativeLeaseView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Lease</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLeaseView() {
        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            session.usePlenaryLeaseView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include leases in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            session.useFederatedCampusView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            session.useIsolatedCampusView();
        }

        return;
    }


    /**
     *  Only leases whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveLeaseView() {
        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            session.useEffectiveLeaseView();
        }

        return;
    }


    /**
     *  All leases of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveLeaseView() {
        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            session.useAnyEffectiveLeaseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Lease</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Lease</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Lease</code> and
     *  retained for compatibility.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  leaseId <code>Id</code> of the
     *          <code>Lease</code>
     *  @return the lease
     *  @throws org.osid.NotFoundException <code>leaseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>leaseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Lease getLease(org.osid.id.Id leaseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            try {
                return (session.getLease(leaseId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(leaseId + " not found");
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  leases specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Leases</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>leaseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByIds(org.osid.id.IdList leaseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.room.squatting.lease.MutableLeaseList ret = new net.okapia.osid.jamocha.room.squatting.lease.MutableLeaseList();

        try (org.osid.id.IdList ids = leaseIds) {
            while (ids.hasNext()) {
                ret.addLease(getLease(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  lease genus <code>Type</code> which does not include
     *  leases of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusType(leaseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  lease genus <code>Type</code> and include any additional
     *  leases with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByParentGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByParentGenusType(leaseGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LeaseList</code> containing the given
     *  lease record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByRecordType(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByRecordType(leaseRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LeaseList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *  
     *  In active mode, leases are returned that are currently
     *  active. In any status mode, active and inactive leases
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Lease</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of all leases of a lease genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeOnDate(org.osid.type.Type leaseGenusType,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusTypeOnDate(leaseGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of leases corresponding to a room
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.LeaseList getLeasesForRoom(org.osid.id.Id roomId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesForRoom(roomId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of leases corresponding to a room
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomOnDate(org.osid.id.Id roomId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesForRoomOnDate(roomId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all leases corresponding to a genus type and
     *  date range. Leases are returned with start effective dates
     *  that fall between the requested dates inclusive.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code>
     *  @param  leaseGenusType a lease genus type
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> or
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoom(org.osid.id.Id roomId,
                                                                         org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusTypeForRoom(roomId, leaseGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all leases for a room and of a lease genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code>
     *  @param  leaseGenusType a lease genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>leaseGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomOnDate(org.osid.id.Id roomId,
                                                                               org.osid.type.Type leaseGenusType,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusTypeForRoomOnDate(roomId, leaseGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of leases corresponding to a tenant
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.LeaseList getLeasesForTenant(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesForTenant(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of leases corresponding to a tenant
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForTenantOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesForTenantOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all leases corresponding to a tenant
     *  <code>Id</code> and date range. Leases are returned with start
     *  effective dates that fall between the requested dates
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resourceId <code>Id</code>
     *  @param  leaseGenusType a lease genus type
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenant(org.osid.id.Id resourceId,
                                                                           org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusTypeForTenant(resourceId, leaseGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all leases for a tenant and of a lease genus
     *  type effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  leaseGenusType a lease genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>leaseGenusType</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenantOnDate(org.osid.id.Id resourceId,
                                                                                 org.osid.type.Type leaseGenusType,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusTypeForTenantOnDate(resourceId, leaseGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of leases corresponding to room and tenant
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenant(org.osid.id.Id roomId,
                                                                       org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesForRoomAndTenant(roomId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of leases corresponding to room and tenant
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenantOnDate(org.osid.id.Id roomId,
                                                                             org.osid.id.Id resourceId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesForRoomAndTenantOnDate(roomId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all leases corresponding for a room, tenant,
     *  and a lease genus type.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code>
     *  @param  resourceId a tenant <code>Id</code>
     *  @param  leaseGenusType a lease genus type
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code>, or
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenant(org.osid.id.Id roomId,
                                                                                  org.osid.id.Id resourceId,
                                                                                  org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusTypeForRoomAndTenant(roomId, resourceId, leaseGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a list of all leases for a roomm tenant, and of a genus
     *  type effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code>
     *  @param  resourceId a resource <code>Id</code>
     *  @param  leaseGenusType a lease genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *  <code>resourceId</code>, <code>leaseGenusType</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenantOnDate(org.osid.id.Id roomId,
                                                                                        org.osid.id.Id resourceId,
                                                                                        org.osid.type.Type leaseGenusType,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeasesByGenusTypeForRoomAndTenantOnDate(roomId, resourceId, leaseGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Leases</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Leases</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeases()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList ret = getLeaseList();

        for (org.osid.room.squatting.LeaseLookupSession session : getSessions()) {
            ret.addLeaseList(session.getLeases());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.FederatingLeaseList getLeaseList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.ParallelLeaseList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.room.squatting.lease.CompositeLeaseList());
        }
    }
}
