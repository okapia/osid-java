//
// InvariantMapProxyAgencyLookupSession
//
//    Implements an Agency lookup service backed by a fixed
//    collection of agencies. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication;


/**
 *  Implements an Agency lookup service backed by a fixed
 *  collection of agencies. The agencies are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAgencyLookupSession
    extends net.okapia.osid.jamocha.core.authentication.spi.AbstractMapAgencyLookupSession
    implements org.osid.authentication.AgencyLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAgencyLookupSession} with no
     *  agencies.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyAgencyLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAgencyLookupSession} with a
     *  single agency.
     *
     *  @param agency an single agency
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAgencyLookupSession(org.osid.authentication.Agency agency, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAgency(agency);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAgencyLookupSession} using
     *  an array of agencies.
     *
     *  @param agencies an array of agencies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agencies} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAgencyLookupSession(org.osid.authentication.Agency[] agencies, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAgencies(agencies);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAgencyLookupSession} using a
     *  collection of agencies.
     *
     *  @param agencies a collection of agencies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code agencies} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAgencyLookupSession(java.util.Collection<? extends org.osid.authentication.Agency> agencies,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAgencies(agencies);
        return;
    }
}
