//
// AbstractTodo.java
//
//     Defines a Todo.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.todo.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Todo</code>.
 */

public abstract class AbstractTodo
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.checklist.Todo {

    private org.osid.type.Type priority;
    private org.osid.calendaring.DateTime dueDate;
    private final java.util.Collection<org.osid.checklist.Todo> dependencies = new java.util.LinkedHashSet<>();
    private boolean complete = false;

    private final java.util.Collection<org.osid.checklist.records.TodoRecord> records = new java.util.LinkedHashSet<>();    
    private final Containable containable = new Containable();


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    protected void setSequestered(boolean sequestered) {
        this.containable.setSequestered(sequestered);
        return;
    }


    /**
     *  Tests if this todo has been checked off. 
     *
     *  @return <code> true </code> if this todo is complete, <code>
     *          false </code> if not complete
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.complete);
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if this todo is complete,
     *         <code> false </code> if not complete
     */

    protected void setComplete(boolean complete) {
        this.complete = complete;
        return;
    }


    /**
     *  Gets a priority of this item. 
     *
     *  @return a priority type 
     */

    @OSID @Override
    public org.osid.type.Type getPriority() {
        return (this.priority);
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @throws org.osid.NullArgumentException
     *          <code>priority</code> is <code>null</code>
     */

    protected void setPriority(org.osid.type.Type priority) {
        nullarg(priority, "priority");
        this.priority = priority;
        return;
    }


    /**
     *  Gets the due date. 
     *
     *  @return a date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        return (this.dueDate);
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDueDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "due date");
        this.dueDate = date;
        return;
    }


    /**
     *  Gets a list of todo <code> Ids </code> on which this todo is 
     *  dependent. 
     *
     *  @return a list of todo <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getDependencyIds() {
        try {
            org.osid.checklist.TodoList dependencies = getDependencies();
            return (new net.okapia.osid.jamocha.adapter.converter.checklist.todo.TodoToIdList(dependencies));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of todos on which this todo is dependent. 
     *
     *  @return a list of todos 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getDependencies()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.checklist.todo.ArrayTodoList(this.dependencies));
    }


    /**
     *  Adds a dependency.
     *
     *  @param dependency a dependency
     *  @throws org.osid.NullArgumentException
     *          <code>dependency</code> is <code>null</code>
     */

    protected void addDependency(org.osid.checklist.Todo dependency) {
        nullarg(dependency, "dependency");
        this.dependencies.add(dependency);
        return;
    }


    /**
     *  Sets all the dependencies.
     *
     *  @param dependencies a collection of dependencies
     *  @throws org.osid.NullArgumentException
     *          <code>dependencies</code> is <code>null</code>
     */

    protected void setDependencies(java.util.Collection<org.osid.checklist.Todo> dependencies) {
        nullarg(dependencies, "dependencies");

        this.dependencies.clear();
        this.dependencies.addAll(dependencies);

        return;
    }


    /**
     *  Tests if this todo supports the given record
     *  <code>Type</code>.
     *
     *  @param  todoRecordType a todo record type 
     *  @return <code>true</code> if the todoRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type todoRecordType) {
        for (org.osid.checklist.records.TodoRecord record : this.records) {
            if (record.implementsRecordType(todoRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Todo</code>
     *  record <code>Type</code>.
     *
     *  @param  todoRecordType the todo record type 
     *  @return the todo record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.TodoRecord getTodoRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.TodoRecord record : this.records) {
            if (record.implementsRecordType(todoRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoRecordType + " is not supported");
    }


    /**
     *  Adds a record to this todo. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param todoRecord the todo record
     *  @param todoRecordType todo record type
     *  @throws org.osid.NullArgumentException <code>todoRecord</code>
     *          or <code>todoRecordType</code> is <code>null</code>
     */
            
    protected void addTodoRecord(org.osid.checklist.records.TodoRecord todoRecord, 
                                 org.osid.type.Type todoRecordType) {

        nullarg(todoRecord, "todo record");
        addRecordType(todoRecordType);
        this.records.add(todoRecord);
        
        return;
    }


    protected class Containable
        extends net.okapia.osid.jamocha.spi.AbstractContainable
        implements org.osid.Containable {


        /**
         *  Sets the sequestered flag.
         *
         *  @param sequestered <code> true </code> if this containable is
         *         sequestered, <code> false </code> if this containable
         *         may appear outside its aggregate
         */

        @Override
        protected void setSequestered(boolean sequestered) {
            super.setSequestered(sequestered);
            return;
        }
    }
}
