//
// AbstractRoomSquattingBatchManager.java
//
//     An adapter for a RoomSquattingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.squatting.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RoomSquattingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRoomSquattingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.room.squatting.batch.RoomSquattingBatchManager>
    implements org.osid.room.squatting.batch.RoomSquattingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterRoomSquattingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRoomSquattingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRoomSquattingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRoomSquattingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of leases is available. 
     *
     *  @return <code> true </code> if a lease bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseBatchAdmin() {
        return (getAdapteeManager().supportsLeaseBatchAdmin());
    }


    /**
     *  Tests if bulk administration of deeds is available. 
     *
     *  @return <code> true </code> if a deed bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedBatchAdmin() {
        return (getAdapteeManager().supportsDeedBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk lease 
     *  administration service. 
     *
     *  @return a <code> LeaseBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.LeaseBatchAdminSession getLeaseBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk lease 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> LeaseBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.LeaseBatchAdminSession getLeaseBatchAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseBatchAdminSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk deed 
     *  administration service. 
     *
     *  @return a <code> DeedBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.DeedBatchAdminSession getDeedBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk deed 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> DeedBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.DeedBatchAdminSession getDeedBatchAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedBatchAdminSessionForCampus(campusId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
