//
// AbstractVoteNotificationSession.java
//
//     A template for making VoteNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Vote} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Vote} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for vote entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractVoteNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.VoteNotificationSession {

    private boolean federated = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Gets the {@code Polls/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }

    
    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the {@code Polls}.
     *
     *  @param polls the polls for this session
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can register for {@code Vote}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForVoteNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include votes in polls which are children of this
     *  polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new votes. {@code
     *  VoteReceiver.newVote()} is invoked when a new {@code Vote} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new votes for the given
     *  ballot. {@code VoteReceiver.newVote()} is invoked when a new
     *  vote is created.
     *
     *  @param ballotId the {@code Id} of the {@code Ballot} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code ballotId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewVotesForBallot(org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new votes for the given
     *  race. {@code VoteReceiver.newVote()} is invoked when a new
     *  vote is created.
     *
     *  @param raceId the {@code Id} of the {@code Race} to monitor
     *  @throws org.osid.NullArgumentException {@code raceId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewVotesForRace(org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new votes for the given
     *  candidate {@code Id}. {@code VoteReceiver.newVote()} is
     *  invoked when a new {@code Vote} is created.
     *
     *  @param  candidateId the {@code Id} of the candidate to monitor
     *  @throws org.osid.NullArgumentException {@code candidateId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewVotesForCandidate(org.osid.id.Id candidateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new votes for the given voter
     *  {@code Id}. {@code VoteReceiver.newVote()} is invoked when a
     *  new {@code Vote} is created.
     *
     *  @param  resourceId the {@code Id} of the voter to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewVotesForVoter(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated votes. {@code
     *  VoteReceiver.changedVote()} is invoked when a vote is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated votes for the given
     *  ballot.  {@code VoteReceiver.changedVote()} is invoked when a
     *  vote is changed.
     *
     *  @param ballotId the {@code Id} of the {@code Ballot} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code ballotId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedVotesForBallot(org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated votes for the given
     *  race. {@code VoteReceiver.changedVote()} is invoked when a
     *  vote is changed.
     *
     *  @param raceId the {@code Id} of the {@code Race} to monitor
     *  @throws org.osid.NullArgumentException {@code raceId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedVotesForRace(org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated votes for the given
     *  candidate {@code Id}. {@code VoteReceiver.changedVote()} is
     *  invoked when a {@code Vote} in this polls is changed.
     *
     *  @param  candidateId the {@code Id} of the candidate to monitor
     *  @throws org.osid.NullArgumentException {@code candidateId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedVotesForCandidate(org.osid.id.Id candidateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated votes for the given
     *  voter {@code Id}. {@code VoteReceiver.changedVote()} is
     *  invoked when a {@code Vote} in this polls is changed.
     *
     *  @param  resourceId the {@code Id} of the voter to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedVotesForVoter(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated vote. {@code
     *  VoteReceiver.changedVote()} is invoked when the specified vote
     *  is changed.
     *
     *  @param voteId the {@code Id} of the {@code Vote} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code voteId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedVote(org.osid.id.Id voteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted votes. {@code
     *  VoteReceiver.deletedVote()} is invoked when a vote is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted votes for the given
     *  ballot.  {@code VoteReceiver.deletedVote()} is invoked when a
     *  vote is deleted.
     *
     *  @param ballotId the {@code Id} of the {@code Ballot} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code ballotId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedVotesForBallot(org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted votes for the given
     *  race. {@code VoteReceiver.deletedVote()} is invoked when a
     *  vote is deleted.
     *
     *  @param raceId the {@code Id} of the {@code Race} to monitor
     *  @throws org.osid.NullArgumentException {@code raceId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedVotesForRace(org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted votes for the given
     *  candidate {@code Id}. {@code VoteReceiver.deletedVote()} is
     *  invoked when a {@code Vote} is deleted or removed from this
     *  polls.
     *
     *  @param  candidateId the {@code Id} of the candidate to monitor
     *  @throws org.osid.NullArgumentException {@code candidateId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedVotesForCandidate(org.osid.id.Id candidateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted votes for the given
     *  voter {@code Id}. {@code VoteReceiver.deletedVote()} is
     *  invoked when a {@code Vote} is deleted or removed from this
     *  polls.
     *
     *  @param  resourceId the {@code Id} of the voter to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedVotesForVoter(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted vote. {@code
     *  VoteReceiver.deletedVote()} is invoked when the specified vote
     *  is deleted.
     *
     *  @param voteId the {@code Id} of the
     *          {@code Vote} to monitor
     *  @throws org.osid.NullArgumentException {@code voteId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedVote(org.osid.id.Id voteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
