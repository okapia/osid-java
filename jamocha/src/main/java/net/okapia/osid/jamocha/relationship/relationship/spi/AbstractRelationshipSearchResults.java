//
// AbstractRelationshipSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRelationshipSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.relationship.RelationshipSearchResults {

    private org.osid.relationship.RelationshipList relationships;
    private final org.osid.relationship.RelationshipQueryInspector inspector;
    private final java.util.Collection<org.osid.relationship.records.RelationshipSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRelationshipSearchResults.
     *
     *  @param relationships the result set
     *  @param relationshipQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>relationships</code>
     *          or <code>relationshipQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRelationshipSearchResults(org.osid.relationship.RelationshipList relationships,
                                            org.osid.relationship.RelationshipQueryInspector relationshipQueryInspector) {
        nullarg(relationships, "relationships");
        nullarg(relationshipQueryInspector, "relationship query inspectpr");

        this.relationships = relationships;
        this.inspector = relationshipQueryInspector;

        return;
    }


    /**
     *  Gets the relationship list resulting from a search.
     *
     *  @return a relationship list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationships() {
        if (this.relationships == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.relationship.RelationshipList relationships = this.relationships;
        this.relationships = null;
	return (relationships);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.relationship.RelationshipQueryInspector getRelationshipQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  relationship search record <code> Type. </code> This method must
     *  be used to retrieve a relationship implementing the requested
     *  record.
     *
     *  @param relationshipSearchRecordType a relationship search 
     *         record type 
     *  @return the relationship search
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(relationshipSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipSearchResultsRecord getRelationshipSearchResultsRecord(org.osid.type.Type relationshipSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.relationship.records.RelationshipSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(relationshipSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(relationshipSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record relationship search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRelationshipRecord(org.osid.relationship.records.RelationshipSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "relationship record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
