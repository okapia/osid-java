//
// AbstractAssemblyLessonQuery.java
//
//     A LessonQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A LessonQuery that stores terms.
 */

public abstract class AbstractAssemblyLessonQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.plan.LessonQuery,
               org.osid.course.plan.LessonQueryInspector,
               org.osid.course.plan.LessonSearchOrder {

    private final java.util.Collection<org.osid.course.plan.records.LessonQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.plan.records.LessonQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.plan.records.LessonSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyLessonQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyLessonQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the plan <code> Id </code> for this query. 
     *
     *  @param  planId a plan <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> planId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPlanId(org.osid.id.Id planId, boolean match) {
        getAssembler().addIdTerm(getPlanIdColumn(), planId, match);
        return;
    }


    /**
     *  Clears the plan <code> Id </code> tems. 
     */

    @OSID @Override
    public void clearPlanIdTerms() {
        getAssembler().clearTerms(getPlanIdColumn());
        return;
    }


    /**
     *  Gets the plan <code> Id </code> terms. 
     *
     *  @return the plan <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPlanIdTerms() {
        return (getAssembler().getIdTerms(getPlanIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the plan. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPlan(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPlanColumn(), style);
        return;
    }


    /**
     *  Gets the PlanId column name.
     *
     * @return the column name
     */

    protected String getPlanIdColumn() {
        return ("plan_id");
    }


    /**
     *  Tests if a plan query is available. 
     *
     *  @return <code> true </code> if a plan query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanQuery() {
        return (false);
    }


    /**
     *  Gets the query for a plan. 
     *
     *  @return the plan query 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuery getPlanQuery() {
        throw new org.osid.UnimplementedException("supportsPlanQuery() is false");
    }


    /**
     *  Clears the plan terms. 
     */

    @OSID @Override
    public void clearPlanTerms() {
        getAssembler().clearTerms(getPlanColumn());
        return;
    }


    /**
     *  Gets the plan terms. 
     *
     *  @return the plan terms 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQueryInspector[] getPlanTerms() {
        return (new org.osid.course.plan.PlanQueryInspector[0]);
    }


    /**
     *  Tests if a <code> PlanSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a plan search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a plan. 
     *
     *  @return the plan search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchOrder getPlanSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPlanSearchOrder() is false");
    }


    /**
     *  Gets the Plan column name.
     *
     * @return the column name
     */

    protected String getPlanColumn() {
        return ("plan");
    }


    /**
     *  Sets the docet <code> Id </code> for this query. 
     *
     *  @param  docetId a docet <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> docetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDocetId(org.osid.id.Id docetId, boolean match) {
        getAssembler().addIdTerm(getDocetIdColumn(), docetId, match);
        return;
    }


    /**
     *  Clears the docet <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDocetIdTerms() {
        getAssembler().clearTerms(getDocetIdColumn());
        return;
    }


    /**
     *  Gets the docet <code> Id </code> terms. 
     *
     *  @return the docet <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDocetIdTerms() {
        return (getAssembler().getIdTerms(getDocetIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the docet. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDocet(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDocetColumn(), style);
        return;
    }


    /**
     *  Gets the DocetId column name.
     *
     * @return the column name
     */

    protected String getDocetIdColumn() {
        return ("docet_id");
    }


    /**
     *  Tests if a docet query is available. 
     *
     *  @return <code> true </code> if a docet query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetQuery() {
        return (false);
    }


    /**
     *  Gets the query for a docet. 
     *
     *  @return the docet query 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuery getDocetQuery() {
        throw new org.osid.UnimplementedException("supportsDocetQuery() is false");
    }


    /**
     *  Matches a lesson that has any docet. 
     *
     *  @param  match <code> true </code> to match lessons with any docet, 
     *          <code> false </code> to match lessons with no docet 
     */

    @OSID @Override
    public void matchAnyDocet(boolean match) {
        getAssembler().addIdWildcardTerm(getDocetColumn(), match);
        return;
    }


    /**
     *  Clears the docet terms. 
     */

    @OSID @Override
    public void clearDocetTerms() {
        getAssembler().clearTerms(getDocetColumn());
        return;
    }


    /**
     *  Gets the docet terms. 
     *
     *  @return the docet terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQueryInspector[] getDocetTerms() {
        return (new org.osid.course.syllabus.DocetQueryInspector[0]);
    }


    /**
     *  Tests if a <code> DocetSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a docet search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a docet. 
     *
     *  @return the docet search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDocetSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetSearchOrder getDocetSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDocetSearchOrder() is false");
    }


    /**
     *  Gets the Docet column name.
     *
     * @return the column name
     */

    protected String getDocetColumn() {
        return ("docet");
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activitytId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activitytId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activitytId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an activity query is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches a lesson that has any activity. 
     *
     *  @param  match <code> true </code> to match lessons with any activity, 
     *          <code> false </code> to match lessons with no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Matches a planned start time within the given duration range 
     *  inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPlannedStartTime(org.osid.calendaring.Duration from, 
                                      org.osid.calendaring.Duration to, 
                                      boolean match) {
        getAssembler().addDurationRangeTerm(getPlannedStartTimeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a lesson that has any planned start time. 
     *
     *  @param  match <code> true </code> to match lessons with any planned 
     *          start time, <code> false </code> to match lessons with no 
     *          planned start time 
     */

    @OSID @Override
    public void matchAnyPlannedStartTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getPlannedStartTimeColumn(), match);
        return;
    }


    /**
     *  Clears the planned start time terms. 
     */

    @OSID @Override
    public void clearPlannedStartTimeTerms() {
        getAssembler().clearTerms(getPlannedStartTimeColumn());
        return;
    }


    /**
     *  Gets the planned start time query terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getPlannedStartTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getPlannedStartTimeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the planned start time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPlannedStartTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPlannedStartTimeColumn(), style);
        return;
    }


    /**
     *  Gets the PlannedStartTime column name.
     *
     * @return the column name
     */

    protected String getPlannedStartTimeColumn() {
        return ("planned_start_time");
    }


    /**
     *  Specified a preference for ordering results by the begun flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBegun(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBegunColumn(), style);
        return;
    }


    /**
     *  Gets the Begun column name.
     *
     * @return the column name
     */

    protected String getBegunColumn() {
        return ("begun");
    }


    /**
     *  Matches an actual start time within the given date range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualStartTime(org.osid.calendaring.Duration from, 
                                     org.osid.calendaring.Duration to, 
                                     boolean match) {
        getAssembler().addDurationRangeTerm(getActualStartTimeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a lesson that has any actual start time. 
     *
     *  @param  match <code> true </code> to match lessons with any actual 
     *          start time, <code> false </code> to match lessons with no 
     *          actual start time 
     */

    @OSID @Override
    public void matchAnyActualStartTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getActualStartTimeColumn(), match);
        return;
    }


    /**
     *  Clears the actual start time terms. 
     */

    @OSID @Override
    public void clearActualStartTimeTerms() {
        getAssembler().clearTerms(getActualStartTimeColumn());
        return;
    }


    /**
     *  Gets actual start time query terms. 
     *
     *  @return the terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getActualStartTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getActualStartTimeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the actual start time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualStartTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActualStartTimeColumn(), style);
        return;
    }


    /**
     *  Gets the ActualStartTime column name.
     *
     * @return the column name
     */

    protected String getActualStartTimeColumn() {
        return ("actual_start_time");
    }


    /**
     *  Sets the starting activity <code> Id </code> for this query. 
     *
     *  @param  activitytId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActualStartingActivityId(org.osid.id.Id activitytId, 
                                              boolean match) {
        getAssembler().addIdTerm(getActualStartingActivityIdColumn(), activitytId, match);
        return;
    }


    /**
     *  Clears the starting activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActualStartingActivityIdTerms() {
        getAssembler().clearTerms(getActualStartingActivityIdColumn());
        return;
    }


    /**
     *  Gets the starting activity <code> Id </code> terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActualStartingActivityIdTerms() {
        return (getAssembler().getIdTerms(getActualStartingActivityIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the actual starting 
     *  activity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualStartingActivity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActualStartingActivityColumn(), style);
        return;
    }


    /**
     *  Gets the ActualStartingActivityId column name.
     *
     * @return the column name
     */

    protected String getActualStartingActivityIdColumn() {
        return ("actual_starting_activity_id");
    }


    /**
     *  Tests if a starting activity query is available. 
     *
     *  @return <code> true </code> if a starting activity query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualStartingActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a starting activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActualStartingActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActualStartingActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActualStartingActivityQuery() is false");
    }


    /**
     *  Matches a lesson that has any starting activity. 
     *
     *  @param  match <code> true </code> to match lessons with any starting 
     *          activity, <code> false </code> to match lessons with no 
     *          starting activity 
     */

    @OSID @Override
    public void matchAnyActualStartingActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActualStartingActivityColumn(), match);
        return;
    }


    /**
     *  Clears the starting activity terms. 
     */

    @OSID @Override
    public void clearActualStartingActivityTerms() {
        getAssembler().clearTerms(getActualStartingActivityColumn());
        return;
    }


    /**
     *  Gets the starting activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActualStartingActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Tests if an <code> ActivitySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualStartingActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an actual starting actvity. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActualStartingActivitySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchOrder getActualStartingActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActualStartingActivitySearchOrder() is false");
    }


    /**
     *  Gets the ActualStartingActivity column name.
     *
     * @return the column name
     */

    protected String getActualStartingActivityColumn() {
        return ("actual_starting_activity");
    }


    /**
     *  Matches a lesson that has been completed. 
     *
     *  @param  match <code> true </code> to match lessons that have been 
     *          completed, <code> false </code> to match lessons that have not 
     *          been completed 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        getAssembler().addBooleanTerm(getCompleteColumn(), match);
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        getAssembler().clearTerms(getCompleteColumn());
        return;
    }


    /**
     *  Gets completed query terms. 
     *
     *  @return the terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (getAssembler().getBooleanTerms(getCompleteColumn()));
    }


    /**
     *  Specified a preference for ordering results by the complete flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompleteColumn(), style);
        return;
    }


    /**
     *  Gets the Complete column name.
     *
     * @return the column name
     */

    protected String getCompleteColumn() {
        return ("complete");
    }


    /**
     *  Matches a lesson that has been skipped. 
     *
     *  @param  match <code> true </code> to match lessons that have been 
     *          skipped, <code> false </code> to match lessons that have not 
     *          been skipped 
     */

    @OSID @Override
    public void matchSkipped(boolean match) {
        getAssembler().addBooleanTerm(getSkippedColumn(), match);
        return;
    }


    /**
     *  Clears the skipped terms. 
     */

    @OSID @Override
    public void clearSkippedTerms() {
        getAssembler().clearTerms(getSkippedColumn());
        return;
    }


    /**
     *  Gets skipped query terms. 
     *
     *  @return the terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSkippedTerms() {
        return (getAssembler().getBooleanTerms(getSkippedColumn()));
    }


    /**
     *  Specified a preference for ordering results by the skipped flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySkipped(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSkippedColumn(), style);
        return;
    }


    /**
     *  Gets the Skipped column name.
     *
     * @return the column name
     */

    protected String getSkippedColumn() {
        return ("skipped");
    }


    /**
     *  Matches an actual ending time within the given date range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualEndTime(org.osid.calendaring.Duration from, 
                                   org.osid.calendaring.Duration to, 
                                   boolean match) {
        getAssembler().addDurationRangeTerm(getActualEndTimeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a lesson that has any actual end time. 
     *
     *  @param  match <code> true </code> to match lessons with any actual end 
     *          time, <code> false </code> to match lessons with no actual end 
     *          time 
     */

    @OSID @Override
    public void matchAnyActualEndTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getActualEndTimeColumn(), match);
        return;
    }


    /**
     *  Clears the actual end time terms. 
     */

    @OSID @Override
    public void clearActualEndTimeTerms() {
        getAssembler().clearTerms(getActualEndTimeColumn());
        return;
    }


    /**
     *  Gets the actual end time query terms. 
     *
     *  @return the time time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getActualEndTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getActualEndTimeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the actual end time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualEndTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActualEndTimeColumn(), style);
        return;
    }


    /**
     *  Gets the ActualEndTime column name.
     *
     * @return the column name
     */

    protected String getActualEndTimeColumn() {
        return ("actual_end_time");
    }


    /**
     *  Sets the ending activity <code> Id </code> for this query. 
     *
     *  @param  activitytId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActualEndingActivityId(org.osid.id.Id activitytId, 
                                            boolean match) {
        getAssembler().addIdTerm(getActualEndingActivityIdColumn(), activitytId, match);
        return;
    }


    /**
     *  Clears the ending activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActualEndingActivityIdTerms() {
        getAssembler().clearTerms(getActualEndingActivityIdColumn());
        return;
    }


    /**
     *  Gets the ending activity <code> Id </code> terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActualEndingActivityIdTerms() {
        return (getAssembler().getIdTerms(getActualEndingActivityIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the actual ending 
     *  activity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualEndingActivity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActualEndingActivityColumn(), style);
        return;
    }


    /**
     *  Gets the ActualEndingActivityId column name.
     *
     * @return the column name
     */

    protected String getActualEndingActivityIdColumn() {
        return ("actual_ending_activity_id");
    }


    /**
     *  Tests if an ending activity query is available. 
     *
     *  @return <code> true </code> if an ending activity query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualEndingActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ending activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingActivityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActualEndingActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActualEndingActivityQuery() is false");
    }


    /**
     *  Matches a lesson that has any ending activity. 
     *
     *  @param  match <code> true </code> to match lessons with any ending 
     *          activity, <code> false </code> to match lessons with no ending 
     *          activity 
     */

    @OSID @Override
    public void matchAnyActualEndingActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActualEndingActivityColumn(), match);
        return;
    }


    /**
     *  Clears the ending activity terms. 
     */

    @OSID @Override
    public void clearActualEndingActivityTerms() {
        getAssembler().clearTerms(getActualEndingActivityColumn());
        return;
    }


    /**
     *  Gets the ending activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActualEndingActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Tests if an <code> ActivitySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActualEndingActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an actual ending actvity. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActualEndingActivitySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchOrder getActualEndingActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActualEndingActivitySearchOrder() is false");
    }


    /**
     *  Gets the ActualEndingActivity column name.
     *
     * @return the column name
     */

    protected String getActualEndingActivityColumn() {
        return ("actual_ending_activity");
    }


    /**
     *  Matches an actual time spent within the given duration range 
     *  inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to a ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualTimeSpent(org.osid.calendaring.Duration from, 
                                     org.osid.calendaring.Duration to, 
                                     boolean match) {
        getAssembler().addDurationRangeTerm(getActualTimeSpentColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a lesson that has any actual time spent. 
     *
     *  @param  match <code> true </code> to match lessons with any actual 
     *          time spent, <code> false </code> to match lessons with no time 
     *          spent 
     */

    @OSID @Override
    public void matchAnyActualTimeSpent(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getActualTimeSpentColumn(), match);
        return;
    }


    /**
     *  Clears the actual time spent terms. 
     */

    @OSID @Override
    public void clearActualTimeSpentTerms() {
        getAssembler().clearTerms(getActualTimeSpentColumn());
        return;
    }


    /**
     *  Gets the actual time spent terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getActualTimeSpentTerms() {
        return (getAssembler().getDurationRangeTerms(getActualTimeSpentColumn()));
    }


    /**
     *  Specified a preference for ordering results by the actual time spent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualTimeSpent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActualTimeSpentColumn(), style);
        return;
    }


    /**
     *  Gets the ActualTimeSpent column name.
     *
     * @return the column name
     */

    protected String getActualTimeSpentColumn() {
        return ("actual_time_spent");
    }


    /**
     *  Sets the lesson <code> Id </code> for this query to match syllabi 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this lesson supports the given record
     *  <code>Type</code>.
     *
     *  @param  lessonRecordType a lesson record type 
     *  @return <code>true</code> if the lessonRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type lessonRecordType) {
        for (org.osid.course.plan.records.LessonQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  lessonRecordType the lesson record type 
     *  @return the lesson query record 
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(lessonRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonQueryRecord getLessonQueryRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.LessonQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(lessonRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  lessonRecordType the lesson record type 
     *  @return the lesson query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(lessonRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonQueryInspectorRecord getLessonQueryInspectorRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.LessonQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(lessonRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param lessonRecordType the lesson record type
     *  @return the lesson search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(lessonRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonSearchOrderRecord getLessonSearchOrderRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.LessonSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(lessonRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this lesson. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param lessonQueryRecord the lesson query record
     *  @param lessonQueryInspectorRecord the lesson query inspector
     *         record
     *  @param lessonSearchOrderRecord the lesson search order record
     *  @param lessonRecordType lesson record type
     *  @throws org.osid.NullArgumentException
     *          <code>lessonQueryRecord</code>,
     *          <code>lessonQueryInspectorRecord</code>,
     *          <code>lessonSearchOrderRecord</code> or
     *          <code>lessonRecordTypelesson</code> is
     *          <code>null</code>
     */
            
    protected void addLessonRecords(org.osid.course.plan.records.LessonQueryRecord lessonQueryRecord, 
                                      org.osid.course.plan.records.LessonQueryInspectorRecord lessonQueryInspectorRecord, 
                                      org.osid.course.plan.records.LessonSearchOrderRecord lessonSearchOrderRecord, 
                                      org.osid.type.Type lessonRecordType) {

        addRecordType(lessonRecordType);

        nullarg(lessonQueryRecord, "lesson query record");
        nullarg(lessonQueryInspectorRecord, "lesson query inspector record");
        nullarg(lessonSearchOrderRecord, "lesson search odrer record");

        this.queryRecords.add(lessonQueryRecord);
        this.queryInspectorRecords.add(lessonQueryInspectorRecord);
        this.searchOrderRecords.add(lessonSearchOrderRecord);
        
        return;
    }
}
