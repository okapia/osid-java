//
// AbstractRequisite.java
//
//     Defines a Requisite.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Requisite</code>.
 */

public abstract class AbstractRequisite
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnabler
    implements org.osid.course.requisite.Requisite {

    private final java.util.Collection<org.osid.course.requisite.Requisite> requisiteOptions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.CourseRequirement> courseRequirements = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.ProgramRequirement> programRequirements = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.CredentialRequirement> credentialRequirements = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.LearningObjectiveRequirement> learningObjectiveRequirements = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.AssessmentRequirement> assessmentRequirements = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.AwardRequirement> awardRequirements = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.course.requisite.records.RequisiteRecord> records = new java.util.LinkedHashSet<>();
    private final Containable containable = new Containable();    


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    protected void setSequestered(boolean sequestered) {
        this.containable.setSequestered(sequestered);
        return;
    }


    /**
     *  Tests if any requisite options are defined. If no requisite
     *  options are defined, this requisite term evaluates to <code>
     *  true. </code> If requisite options are defined, then at least
     *  one <code> Requisite </code> must evaluate to <code> true
     *  </code> for the requisite option term to be <code>
     *  true. </code>
     *
     *  @return <code> true </code> if any requisite options are
     *          defined, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasRequisiteOptions() {
        return ((this.requisiteOptions != null) && (this.requisiteOptions.size() > 0));
    }


    /**
     *  Gets the requisite options for the <code> Requisite </code>
     *  options term. If any active requisite options are available,
     *  meeting the requirements of any one of these requisite options
     *  evaluates this requisite options term as <code> true. </code>
     *  If no active requisite options apply then this term evaluates
     *  to <code> false. </code>
     *
     *  @return the requsites 
     *  @throws org.osid.IllegalStateException <code>
     *          hasRequisiteOptions() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getRequisiteOptions() {
        if (!hasRequisiteOptions()) {
            throw new org.osid.IllegalStateException("hasRequisiteOptions() is false");
        }

        return (this.requisiteOptions.toArray(new org.osid.course.requisite.Requisite[this.requisiteOptions.size()]));
    }


    /**
     *  Adds a requisite option.
     *
     *  @param requisiteOption a requisite options
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteOption</code> is <code>null</code>
     */

    protected void addRequisiteOption(org.osid.course.requisite.Requisite requisiteOption) {
        nullarg(requisiteOption, "requisite option");
        this.requisiteOptions.add(requisiteOption);
        return;
    }


    /**
     *  Sets all the requisite options.
     *
     *  @param requisiteOptions a collection of requisite options
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteOptions</code> is <code>null</code>
     */

    protected void setRequisiteOptions(java.util.Collection<org.osid.course.requisite.Requisite> requisiteOptions) {
        nullarg(requisiteOptions, "requisite options");

        this.requisiteOptions.clear();
        this.requisiteOptions.addAll(requisiteOptions);

        return;
    }


    /**
     *  Tests if any course requirements are defined. If no course
     *  requirements are defined, this course requirements term
     *  evaluates to <code> true. </code> If course requirements are
     *  defined, then at least one <code> CourseRequirement </code>
     *  must evaluate to <code> true </code> for the course
     *  requirement term to be <code> true. </code>
     *
     *  @return <code> true </code> if any course requirements are
     *          defined, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasCourseRequirements() {
        return ((this.courseRequirements != null) && (this.courseRequirements.size() > 0));
    }


    /**
     *  Gets the course requirements term. If any <code>
     *  CourseRequirements </code> are available, meeting the
     *  requirements of any one of the <code> CourseRequirements
     *  </code> evaluates this course requirements term as <code>
     *  true. </code> If no active course requirements apply then this
     *  course requirements term is <code> false. </code>
     *
     *  @return the course requirements 
     *  @throws org.osid.IllegalStateException <code>
     *          hasCourseRequirements() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirement[] getCourseRequirements() {
        if (!hasCourseRequirements()) {
            throw new org.osid.IllegalStateException("hasCourseRequirements() is false");
        }

        return (this.courseRequirements.toArray(new org.osid.course.requisite.CourseRequirement[this.courseRequirements.size()]));
    }


    /**
     *  Adds a course requirement.
     *
     *  @param requirement a course requirememt
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addCourseRequirement(org.osid.course.requisite.CourseRequirement requirement) {
        nullarg(requirement, "course requirement");
        this.courseRequirements.add(requirement);
        return;
    }


    /**
     *  Sets all the course requirements.
     *
     *  @param requirements a collection of course requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setCourseRequirements(java.util.Collection<org.osid.course.requisite.CourseRequirement> requirements) {
        nullarg(requirements, "course requirements");

        this.courseRequirements.clear();
        this.courseRequirements.addAll(courseRequirements);

        return;
    }


    /**
     *  Tests if any program requirements are defined. If no program
     *  requirements are defined, this program requirements term
     *  evaluates to <code> true. </code> If program requirements are
     *  defined, then at least one <code> ProgramRequirement </code>
     *  must evaluate to <code> true </code> for the program
     *  requirement term to be <code> true.  </code>
     *
     *  @return <code> true </code> if any program requirements are defined, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgramRequirements() {
        return ((this.programRequirements != null) && (this.programRequirements.size() > 0));
    }


    /**
     *  Gets the program requirements term. If any <code>
     *  ProgramRequirements </code> are available, meeting the
     *  requirements of any one of the <code> ProgramRequirements
     *  </code> evaluates this program requirements term as <code>
     *  true. </code> If no program requirements apply then this
     *  program requirements term evaluates to <code> false. </code>
     *
     *  @return the program requirements 
     *  @throws org.osid.IllegalStateException <code>
     *          hasProgramRequirements() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirement[] getProgramRequirements() {
        if (!hasProgramRequirements()) {
            throw new org.osid.IllegalStateException("hasProgramRequirements() is false");
        }

        return (this.programRequirements.toArray(new org.osid.course.requisite.ProgramRequirement[this.programRequirements.size()]));
    }


    /**
     *  Adds a program requirement.
     *
     *  @param requirement a program requirememt
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addProgramRequirement(org.osid.course.requisite.ProgramRequirement requirement) {
        nullarg(requirement, "program requirement");
        this.programRequirements.add(requirement);
        return;
    }


    /**
     *  Sets all the program requirements.
     *
     *  @param requirements a collection of program requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setProgramRequirements(java.util.Collection<org.osid.course.requisite.ProgramRequirement> requirements) {
        nullarg(requirements, "program requirements");

        this.programRequirements.clear();
        this.programRequirements.addAll(programRequirements);

        return;
    }


    /**
     *  Tests if any credential requirements are defined. If no
     *  credential requirements are defined, this credential
     *  requirements term evaluates to <code> true. </code> If
     *  credential requirements are defined, then at least one <code>
     *  CredentialRequirement </code> must evaluate to <code> true
     *  </code> for the credential requirement term to be <code>
     *  true. </code>
     *
     *  @return <code> true </code> if any credential requirements are
     *          defined, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasCredentialRequirements() {
        return ((this.programRequirements != null) && (this.programRequirements.size() > 0));
    }


    /**
     *  Gets the credential requirements term. If any <code>
     *  CredentialRequirements </code> are available, meeting the
     *  requirements of any one of the <code> CredentialRequirements
     *  </code> evaluates this credential requirements term as <code>
     *  true. </code> If no credential requirements are available,
     *  this credential requirements term evaluates to <code>
     *  false. </code>
     *
     *  @return the credential requirements 
     *  @throws org.osid.IllegalStateException <code>
     *          hasCredentialRequirements() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirement[] getCredentialRequirements() {
        if (!hasCredentialRequirements()) {
            throw new org.osid.IllegalStateException("hasCredentialRequirements() is false");
        }

        return (this.credentialRequirements.toArray(new org.osid.course.requisite.CredentialRequirement[this.credentialRequirements.size()]));
    }


    /**
     *  Adds a credential requirement.
     *
     *  @param requirement a credential requirememt
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addCredentialRequirement(org.osid.course.requisite.CredentialRequirement requirement) {
        nullarg(requirement, "credential requirement");
        this.credentialRequirements.add(requirement);
        return;
    }


    /**
     *  Sets all the credential requirements.
     *
     *  @param requirements a collection of credential requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setCredentialRequirements(java.util.Collection<org.osid.course.requisite.CredentialRequirement> requirements) {
        nullarg(requirements, "credential requirements");

        this.credentialRequirements.clear();
        this.credentialRequirements.addAll(credentialRequirements);

        return;
    }


    /**
     *  Tests if any learning objective requirements are defined. If
     *  no learning objective requirements are defined, this learning
     *  objective requirements term evaluates to <code> true. </code>
     *  If learning objective requirements are defined, then at least
     *  one <code> LearningObjectiveRequirement </code> must evaluate
     *  to <code> true </code> for the learning objective requirement
     *  term to be <code> true.  </code>
     *
     *  @return <code> true </code> if any learning objective
     *          requirements are defined, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean hasLearningObjectiveRequirements() {
        return ((this.learningObjectiveRequirements != null) && 
                (this.learningObjectiveRequirements.size() > 0));
    }


    /**
     *  Gets the learning objective requirements term. If any <code>
     *  LearningObjectiveRequirements </code> are available, meeting
     *  the requirements of any one of the <code>
     *  LearningObjectiveRequirements </code> evaluates this learning
     *  objective requirements term as <code> true. </code> If no
     *  learning objective requirements applythen this learning
     *  objective requirements term evaluates to <code> false.
     *  </code>
     *
     *  @return the learning objective requirements 
     *  @throws org.osid.IllegalStateException <code>
     *          hasLearningObjectiveRequirements() </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirement[] getLearningObjectiveRequirements() {
        if (!hasLearningObjectiveRequirements()) {
            throw new org.osid.IllegalStateException("hasLearningObjectiveRequirements() is false");
        }

        return (this.learningObjectiveRequirements.toArray(new org.osid.course.requisite.LearningObjectiveRequirement[this.learningObjectiveRequirements.size()]));
    }


    /**
     *  Adds a learning objective requirement.
     *
     *  @param requirement a learning objective requirememt
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addLearningObjectiveRequirement(org.osid.course.requisite.LearningObjectiveRequirement requirement) {
        nullarg(requirement, "learning objective requirement");
        this.learningObjectiveRequirements.add(requirement);
        return;
    }


    /**
     *  Sets all the learning objective requirements.
     *
     *  @param requirements a collection of learning objective requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setLearningObjectiveRequirements(java.util.Collection<org.osid.course.requisite.LearningObjectiveRequirement> requirements) {
        nullarg(requirements, "learning objective requirements");

        this.learningObjectiveRequirements.clear();
        this.learningObjectiveRequirements.addAll(learningObjectiveRequirements);

        return;
    }


    /**
     *  Tests if any assessment requirements are defined. If no
     *  assessment requirements are defined, this assessment
     *  requirements term evaluates to <code> true. </code> If
     *  assessment requirements are defined, then at least one <code>
     *  AssessmentRequirement </code> must evaluate to <code> true
     *  </code> for the assessment requirement term to be <code>
     *  true. </code>
     *
     *  @return <code> true </code> if any assessment requirements are
     *          defined, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasAssessmentRequirements() {
        return ((this.assessmentRequirements != null) &&
                (this.assessmentRequirements.size() > 0));
    }


    /**
     *  Gets the assessment requirements term. If any <code>
     *  AssessmentRequirements </code> are available, meeting the
     *  requirements of any one of the <code> AssessmentRequirements
     *  </code> evaluates this assessment requirements term as <code>
     *  true. </code> If no assessment requirements are available,
     *  this assessment requirements term evaluates to <code>
     *  false. </code>
     *
     *  @return the assessment requirements 
     *  @throws org.osid.IllegalStateException <code>
     *          hasAssessmentRequirements() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirement[] getAssessmentRequirements() {
        if (!hasAssessmentRequirements()) {
            throw new org.osid.IllegalStateException("hasAssessmentRequirements() is false");
        }

        return (this.assessmentRequirements.toArray(new org.osid.course.requisite.AssessmentRequirement[this.assessmentRequirements.size()]));
    }


    /**
     *  Adds an assessment requirement.
     *
     *  @param requirement an assessment requirememt
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addAssessmentRequirement(org.osid.course.requisite.AssessmentRequirement requirement) {
        nullarg(requirement, "assessment requirement");
        this.assessmentRequirements.add(requirement);
        return;
    }


    /**
     *  Sets all the assessment requirements.
     *
     *  @param requirements a collection of assessment requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setAssessmentRequirements(java.util.Collection<org.osid.course.requisite.AssessmentRequirement> requirements) {
        nullarg(requirements, "assessment requirements");

        this.assessmentRequirements.clear();
        this.assessmentRequirements.addAll(assessmentRequirements);

        return;
    }


    /**
     *  Tests if any award requirements are defined. If no award
     *  requirements are defined, this award requirements term
     *  evaluates to <code> true.  </code> If award requirements are
     *  defined, then at least one <code> AwardRequirement </code>
     *  must evaluate to <code> true </code> for the award requirement
     *  term to be <code> true. </code>
     *
     *  @return <code> true </code> if any award requirements are
     *          defined, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasAwardRequirements() {
        return ((this.awardRequirements != null) && (this.awardRequirements.size() > 0));
    }


    /**
     *  Gets the award requirements term. If any <code>
     *  AwardRequirements </code> are available, meeting the
     *  requirements of any one of the <code> AwardRequirements
     *  </code> evaluates this award requirements term as <code>
     *  true. </code> If no award requirements are available, this
     *  award requirements term evaluates to <code> false. </code>
     *
     *  @return the award requirements 
     *  @throws org.osid.IllegalStateException <code>
     *          hasAwardRequirements() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirement[] getAwardRequirements() {
        if (!hasAwardRequirements()) {
            throw new org.osid.IllegalStateException("hasAwardRequirements() is false");
        }

        return (this.awardRequirements.toArray(new org.osid.course.requisite.AwardRequirement[this.awardRequirements.size()]));
    }


    /**
     *  Adds an award requirement.
     *
     *  @param requirement an award requirememt
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addAwardRequirement(org.osid.course.requisite.AwardRequirement requirement) {
        nullarg(requirement, "award requirement");
        this.awardRequirements.add(requirement);
        return;
    }


    /**
     *  Sets all the award requirements.
     *
     *  @param requirements a collection of award requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setAwardRequirements(java.util.Collection<org.osid.course.requisite.AwardRequirement> requirements) {
        nullarg(requirements, "award requirements");

        this.awardRequirements.clear();
        this.awardRequirements.addAll(awardRequirements);

        return;
    }


    /**
     *  Tests if this requisite supports the given record
     *  <code>Type</code>.
     *
     *  @param  requisiteRecordType a requisite record type 
     *  @return <code>true</code> if the requisiteRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requisiteRecordType) {
        for (org.osid.course.requisite.records.RequisiteRecord record : this.records) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Requisite</code> record <code>Type</code>.
     *
     *  @param  requisiteRecordType the requisite record type 
     *  @return the requisite record 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requisiteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteRecord getRequisiteRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.RequisiteRecord record : this.records) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this requisite. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requisiteRecord the requisite record
     *  @param requisiteRecordType requisite record type
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecord</code> or
     *          <code>requisiteRecordTyperequisite</code> is
     *          <code>null</code>
     */
            
    protected void addRequisiteRecord(org.osid.course.requisite.records.RequisiteRecord requisiteRecord, 
                                      org.osid.type.Type requisiteRecordType) {

        nullarg(requisiteRecord, "requisite record");
        addRecordType(requisiteRecordType);
        this.records.add(requisiteRecord);
        
        return;
    }


    protected class Containable
        extends net.okapia.osid.jamocha.spi.AbstractContainable
        implements org.osid.Containable {


        /**
         *  Sets the sequestered flag.
         *
         *  @param sequestered <code> true </code> if this containable is
         *         sequestered, <code> false </code> if this containable
         *         may appear outside its aggregate
         */

        @Override
        protected void setSequestered(boolean sequestered) {
            super.setSequestered(sequestered);
            return;
        }
    }
}
