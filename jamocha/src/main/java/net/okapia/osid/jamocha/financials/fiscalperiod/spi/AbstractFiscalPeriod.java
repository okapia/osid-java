//
// AbstractFiscalPeriod.java
//
//     Defines a FiscalPeriod.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>FiscalPeriod</code>.
 */

public abstract class AbstractFiscalPeriod
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.financials.FiscalPeriod {

    private long fiscalYear;
    private org.osid.locale.DisplayText displayLabel;
    private org.osid.calendaring.DateTime startDate;
    private org.osid.calendaring.DateTime endDate;
    private org.osid.calendaring.DateTime budgetDeadline;
    private org.osid.calendaring.DateTime postingDeadline;
    private org.osid.calendaring.DateTime closing;

    private boolean requiresBudgetSubmission = false;

    private final java.util.Collection<org.osid.financials.records.FiscalPeriodRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets a display label for this fiscal period which may be less formal 
     *  than the display name. 
     *
     *  @return the display label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.displayLabel);
    }


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    protected void setDisplayLabel(org.osid.locale.DisplayText label) {
        nullarg(label, "display label");
        this.displayLabel = label;
        return;
    }


    /**
     *  Ges the fiscal year. 
     *
     *  @return the fiscal year 
     */

    @OSID @Override
    public long getFiscalYear() {
        return (this.fiscalYear);
    }


    /**
     *  Sets the fiscal year.
     *
     *  @param year a fiscal year
     */

    protected void setFiscalYear(long year) {
        this.fiscalYear = year;
        return;
    }


    /**
     *  Get sthe start date of this period. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.startDate);
    }


    /**
     *  Sets the start date.
     *
     *  @param date a start date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setStartDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "start date");
        this.startDate = date;
        return;
    }


    /**
     *  Get sthe end date of this period. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.endDate);
    }


    /**
     *  Sets the end date.
     *
     *  @param date an end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setEndDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "end date");
        this.endDate = date;
        return;
    }


    /**
     *  Tests if this fiscal period has milestones. 
     *
     *  @return <code> true </code> if milestones are available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasMilestones() {
        return ((this.budgetDeadline != null) || (this.postingDeadline != null) ||
                (this.closing != null));
    }


    /**
     *  Tests if budgets need to be submiited for this fiscal period. 
     *
     *  @return <code> true </code> if budgets require submission, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public boolean requiresBudgetSubmission() {
        if (!hasMilestones()) {
            throw new org.osid.IllegalStateException("hasMilestones() is false");
        }

        return (this.requiresBudgetSubmission);
    }


    /**
     *  Sets the requires budget submission.
     *
     *  @param requires <code> true </code> if budgets require
     *         submission, <code> false </code> otherwise
     */

    protected void setRequiresBudgetSubmission(boolean requires) {
        this.requiresBudgetSubmission = requires;
        return;
    }


    /**
     *  Gets the budget deadline. 
     *
     *  @return the closing date 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          or <code> requiresBudgetSubmission() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getBudgetDeadline() {
        if (!requiresBudgetSubmission()) {
            throw new org.osid.IllegalStateException("requiresBudgetSubmission() is false");
        }

        return (this.budgetDeadline);
    }


    /**
     *  Sets the budget deadline.
     *
     *  @param deadline a budget deadline
     *  @throws org.osid.NullArgumentException <code>deadline</code>
     *          is <code>null</code>
     */

    protected void setBudgetDeadline(org.osid.calendaring.DateTime deadline) {
        nullarg(deadline, "budget deadline");
        this.budgetDeadline = deadline;
        return;
    }


    /**
     *  Tests if this period has a cutoff date for posting transactions. 
     *
     *  @return <code> true </code> if a posting deadline date is available, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public boolean hasPostingDeadline() {
        if (!hasMilestones()) {
            throw new org.osid.IllegalStateException("hasMilestones() is false");
        }

        return (this.postingDeadline != null);
    }


    /**
     *  Gets the last date transactions can be posted for this period. 
     *
     *  @return the cutoff date 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          or <code> hasPostingDeadline() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPostingDeadline() {
        if (!hasPostingDeadline()) {
            throw new org.osid.IllegalStateException("hasPostingDeadline() is false");
        }

        return (this.postingDeadline);
    }


    /**
     *  Sets the posting deadline.
     *
     *  @param deadline a posting deadline
     *  @throws org.osid.NullArgumentException <code>deadline</code>
     *          is <code>null</code>
     */

    protected void setPostingDeadline(org.osid.calendaring.DateTime deadline) {
        nullarg(deadline, "posting deadline");
        this.postingDeadline = deadline;
        return;
    }


    /**
     *  Tests if this period has a closing date. 
     *
     *  @return <code> true </code> if a closing date is available, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public boolean hasClosing() {
        if (!hasMilestones()) {
            throw new org.osid.IllegalStateException("hasMilestones() is false");
        }

        return (this.closing != null);
    }


    /**
     *  Gets the date of the closing for this period. 
     *
     *  @return the closing date 
     *  @throws org.osid.IllegalStateException <code> hasMilestones() </code> 
     *          or <code> hasClosing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClosing() {
        if (!hasClosing()) {
            throw new org.osid.IllegalStateException("hasClosing() is false");
        }

        return (this.closing);
    }


    /**
     *  Sets the closing.
     *
     *  @param closing a closing
     *  @throws org.osid.NullArgumentException
     *          <code>closing</code> is <code>null</code>
     */

    protected void setClosing(org.osid.calendaring.DateTime closing) {
        nullarg(closing, "closing");
        this.closing = closing;
        return;
    }


    /**
     *  Tests if this fiscalPeriod supports the given record
     *  <code>Type</code>.
     *
     *  @param  fiscalPeriodRecordType a fiscal period record type 
     *  @return <code>true</code> if the fiscalPeriodRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type fiscalPeriodRecordType) {
        for (org.osid.financials.records.FiscalPeriodRecord record : this.records) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>FiscalPeriod</code> record <code>Type</code>.
     *
     *  @param  fiscalPeriodRecordType the fiscal period record type 
     *  @return the fiscal period record 
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fiscalPeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodRecord getFiscalPeriodRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.FiscalPeriodRecord record : this.records) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fiscalPeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this fiscal period. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param fiscalPeriodRecord the fiscal period record
     *  @param fiscalPeriodRecordType fiscal period record type
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecord</code> or
     *          <code>fiscalPeriodRecordTypefiscalPeriod</code> is
     *          <code>null</code>
     */
            
    protected void addFiscalPeriodRecord(org.osid.financials.records.FiscalPeriodRecord fiscalPeriodRecord, 
                                         org.osid.type.Type fiscalPeriodRecordType) {

        nullarg(fiscalPeriodRecord, "fiscal period record");
        addRecordType(fiscalPeriodRecordType);
        this.records.add(fiscalPeriodRecord);
        
        return;
    }
}
