//
// AbstractFiscalPeriodSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractFiscalPeriodSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.financials.FiscalPeriodSearchResults {

    private org.osid.financials.FiscalPeriodList fiscalPeriods;
    private final org.osid.financials.FiscalPeriodQueryInspector inspector;
    private final java.util.Collection<org.osid.financials.records.FiscalPeriodSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractFiscalPeriodSearchResults.
     *
     *  @param fiscalPeriods the result set
     *  @param fiscalPeriodQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>fiscalPeriods</code>
     *          or <code>fiscalPeriodQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractFiscalPeriodSearchResults(org.osid.financials.FiscalPeriodList fiscalPeriods,
                                            org.osid.financials.FiscalPeriodQueryInspector fiscalPeriodQueryInspector) {
        nullarg(fiscalPeriods, "fiscal periods");
        nullarg(fiscalPeriodQueryInspector, "fiscal period query inspectpr");

        this.fiscalPeriods = fiscalPeriods;
        this.inspector = fiscalPeriodQueryInspector;

        return;
    }


    /**
     *  Gets the fiscal period list resulting from a search.
     *
     *  @return a fiscal period list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriods() {
        if (this.fiscalPeriods == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.financials.FiscalPeriodList fiscalPeriods = this.fiscalPeriods;
        this.fiscalPeriods = null;
	return (fiscalPeriods);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.financials.FiscalPeriodQueryInspector getFiscalPeriodQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  fiscal period search record <code> Type. </code> This method must
     *  be used to retrieve a fiscalPeriod implementing the requested
     *  record.
     *
     *  @param fiscalPeriodSearchRecordType a fiscalPeriod search 
     *         record type 
     *  @return the fiscal period search
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(fiscalPeriodSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodSearchResultsRecord getFiscalPeriodSearchResultsRecord(org.osid.type.Type fiscalPeriodSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.financials.records.FiscalPeriodSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(fiscalPeriodSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(fiscalPeriodSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record fiscal period search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addFiscalPeriodRecord(org.osid.financials.records.FiscalPeriodSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "fiscal period record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
