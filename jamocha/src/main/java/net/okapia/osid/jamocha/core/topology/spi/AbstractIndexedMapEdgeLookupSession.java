//
// AbstractIndexedMapEdgeLookupSession.java
//
//    A simple framework for providing an Edge lookup service
//    backed by a fixed collection of edges with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Edge lookup service backed by a
 *  fixed collection of edges. The edges are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some edges may be compatible
 *  with more types than are indicated through these edge
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Edges</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEdgeLookupSession
    extends AbstractMapEdgeLookupSession
    implements org.osid.topology.EdgeLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.topology.Edge> edgesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.Edge>());
    private final MultiMap<org.osid.type.Type, org.osid.topology.Edge> edgesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.Edge>());


    /**
     *  Makes an <code>Edge</code> available in this session.
     *
     *  @param  edge an edge
     *  @throws org.osid.NullArgumentException <code>edge<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEdge(org.osid.topology.Edge edge) {
        super.putEdge(edge);

        this.edgesByGenus.put(edge.getGenusType(), edge);
        
        try (org.osid.type.TypeList types = edge.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.edgesByRecord.put(types.getNextType(), edge);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an edge from this session.
     *
     *  @param edgeId the <code>Id</code> of the edge
     *  @throws org.osid.NullArgumentException <code>edgeId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEdge(org.osid.id.Id edgeId) {
        org.osid.topology.Edge edge;
        try {
            edge = getEdge(edgeId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.edgesByGenus.remove(edge.getGenusType());

        try (org.osid.type.TypeList types = edge.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.edgesByRecord.remove(types.getNextType(), edge);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEdge(edgeId);
        return;
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  edge genus <code>Type</code> which does not include
     *  edges of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known edges or an error results. Otherwise,
     *  the returned list may contain only those edges that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.edge.ArrayEdgeList(this.edgesByGenus.get(edgeGenusType)));
    }


    /**
     *  Gets an <code>EdgeList</code> containing the given
     *  edge record <code>Type</code>. In plenary mode, the
     *  returned list contains all known edges or an error
     *  results. Otherwise, the returned list may contain only those
     *  edges that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return the returned <code>edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByRecordType(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.edge.ArrayEdgeList(this.edgesByRecord.get(edgeRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.edgesByGenus.clear();
        this.edgesByRecord.clear();

        super.close();

        return;
    }
}
