//
// PriceEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.priceenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PriceEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the PriceEnablerElement Id.
     *
     *  @return the price enabler element Id
     */

    public static org.osid.id.Id getPriceEnablerEntityId() {
        return (makeEntityId("osid.ordering.rules.PriceEnabler"));
    }


    /**
     *  Gets the RuledPriceId element Id.
     *
     *  @return the RuledPriceId element Id
     */

    public static org.osid.id.Id getRuledPriceId() {
        return (makeQueryElementId("osid.ordering.rules.priceenabler.RuledPriceId"));
    }


    /**
     *  Gets the RuledPrice element Id.
     *
     *  @return the RuledPrice element Id
     */

    public static org.osid.id.Id getRuledPrice() {
        return (makeQueryElementId("osid.ordering.rules.priceenabler.RuledPrice"));
    }


    /**
     *  Gets the StoreId element Id.
     *
     *  @return the StoreId element Id
     */

    public static org.osid.id.Id getStoreId() {
        return (makeQueryElementId("osid.ordering.rules.priceenabler.StoreId"));
    }


    /**
     *  Gets the Store element Id.
     *
     *  @return the Store element Id
     */

    public static org.osid.id.Id getStore() {
        return (makeQueryElementId("osid.ordering.rules.priceenabler.Store"));
    }
}
