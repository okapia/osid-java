//
// MutableIndexedMapSequenceRuleLookupSession
//
//    Implements a SequenceRule lookup service backed by a collection of
//    sequenceRules indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements a SequenceRule lookup service backed by a collection of
 *  sequence rules. The sequence rules are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some sequence rules may be compatible
 *  with more types than are indicated through these sequence rule
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of sequence rules can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapSequenceRuleLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractIndexedMapSequenceRuleLookupSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapSequenceRuleLookupSession} with no sequence rules.
     *
     *  @param bank the bank
     *  @throws org.osid.NullArgumentException {@code bank}
     *          is {@code null}
     */

      public MutableIndexedMapSequenceRuleLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSequenceRuleLookupSession} with a
     *  single sequence rule.
     *  
     *  @param bank the bank
     *  @param  sequenceRule a single sequenceRule
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code sequenceRule} is {@code null}
     */

    public MutableIndexedMapSequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.authoring.SequenceRule sequenceRule) {
        this(bank);
        putSequenceRule(sequenceRule);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSequenceRuleLookupSession} using an
     *  array of sequence rules.
     *
     *  @param bank the bank
     *  @param  sequenceRules an array of sequence rules
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code sequenceRules} is {@code null}
     */

    public MutableIndexedMapSequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.authoring.SequenceRule[] sequenceRules) {
        this(bank);
        putSequenceRules(sequenceRules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSequenceRuleLookupSession} using a
     *  collection of sequence rules.
     *
     *  @param bank the bank
     *  @param  sequenceRules a collection of sequence rules
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code sequenceRules} is {@code null}
     */

    public MutableIndexedMapSequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                  java.util.Collection<? extends org.osid.assessment.authoring.SequenceRule> sequenceRules) {

        this(bank);
        putSequenceRules(sequenceRules);
        return;
    }
    

    /**
     *  Makes a {@code SequenceRule} available in this session.
     *
     *  @param  sequenceRule a sequence rule
     *  @throws org.osid.NullArgumentException {@code sequenceRule{@code  is
     *          {@code null}
     */

    @Override
    public void putSequenceRule(org.osid.assessment.authoring.SequenceRule sequenceRule) {
        super.putSequenceRule(sequenceRule);
        return;
    }


    /**
     *  Makes an array of sequence rules available in this session.
     *
     *  @param  sequenceRules an array of sequence rules
     *  @throws org.osid.NullArgumentException {@code sequenceRules{@code 
     *          is {@code null}
     */

    @Override
    public void putSequenceRules(org.osid.assessment.authoring.SequenceRule[] sequenceRules) {
        super.putSequenceRules(sequenceRules);
        return;
    }


    /**
     *  Makes collection of sequence rules available in this session.
     *
     *  @param  sequenceRules a collection of sequence rules
     *  @throws org.osid.NullArgumentException {@code sequenceRule{@code  is
     *          {@code null}
     */

    @Override
    public void putSequenceRules(java.util.Collection<? extends org.osid.assessment.authoring.SequenceRule> sequenceRules) {
        super.putSequenceRules(sequenceRules);
        return;
    }


    /**
     *  Removes a SequenceRule from this session.
     *
     *  @param sequenceRuleId the {@code Id} of the sequence rule
     *  @throws org.osid.NullArgumentException {@code sequenceRuleId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSequenceRule(org.osid.id.Id sequenceRuleId) {
        super.removeSequenceRule(sequenceRuleId);
        return;
    }    
}
