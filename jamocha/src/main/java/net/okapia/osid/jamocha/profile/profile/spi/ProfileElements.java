//
// ProfileElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profile.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProfileElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the ProfileElement Id.
     *
     *  @return the profile element Id
     */

    public static org.osid.id.Id getProfileEntityId() {
        return (makeEntityId("osid.profile.Profile"));
    }


    /**
     *  Gets the ProfileItemId element Id.
     *
     *  @return the ProfileItemId element Id
     */

    public static org.osid.id.Id getProfileItemId() {
        return (makeQueryElementId("osid.profile.profile.ProfileItemId"));
    }


    /**
     *  Gets the ProfileItem element Id.
     *
     *  @return the ProfileItem element Id
     */

    public static org.osid.id.Id getProfileItem() {
        return (makeQueryElementId("osid.profile.profile.ProfileItem"));
    }


    /**
     *  Gets the ProfileEntryId element Id.
     *
     *  @return the ProfileEntryId element Id
     */

    public static org.osid.id.Id getProfileEntryId() {
        return (makeQueryElementId("osid.profile.profile.ProfileEntryId"));
    }


    /**
     *  Gets the ProfileEntry element Id.
     *
     *  @return the ProfileEntry element Id
     */

    public static org.osid.id.Id getProfileEntry() {
        return (makeQueryElementId("osid.profile.profile.ProfileEntry"));
    }


    /**
     *  Gets the AncestorProfileId element Id.
     *
     *  @return the AncestorProfileId element Id
     */

    public static org.osid.id.Id getAncestorProfileId() {
        return (makeQueryElementId("osid.profile.profile.AncestorProfileId"));
    }


    /**
     *  Gets the AncestorProfile element Id.
     *
     *  @return the AncestorProfile element Id
     */

    public static org.osid.id.Id getAncestorProfile() {
        return (makeQueryElementId("osid.profile.profile.AncestorProfile"));
    }


    /**
     *  Gets the DescendantProfileId element Id.
     *
     *  @return the DescendantProfileId element Id
     */

    public static org.osid.id.Id getDescendantProfileId() {
        return (makeQueryElementId("osid.profile.profile.DescendantProfileId"));
    }


    /**
     *  Gets the DescendantProfile element Id.
     *
     *  @return the DescendantProfile element Id
     */

    public static org.osid.id.Id getDescendantProfile() {
        return (makeQueryElementId("osid.profile.profile.DescendantProfile"));
    }
}
