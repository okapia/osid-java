//
// AbstractImmutableOsidCatalog
//
//     Defines an immutable wrapper for an OSID Catalog.
//
//
//
// Tom Coppeto
// OnTapSolutions
// 5 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package org.osid.impl.builder;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an immutable wrapper for an OSID Catalog.
 */

public abstract class AbstractImmutableOsidCatalog
    extends AbstractImmutableOsidObject
    implements org.osid.OsidCatalog {

    private final org.osid.OsidCatalog catalog;


    /**
     *  Constructs a new <code>AbstractImmutableOsidCatalog</code>.
     *
     *  @param catalog
     *  @throws org.osid.NullArgumentException <code>catalog</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOsidCatalog(org.osid.OsidCatalog catalog) {
	super(catalog);
	this.catalog = catalog;
	return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Provider </code> of this 
     *  <code> Catalog. </code> 
     *
     *  @return the <code> Provider Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProviderId() {
	return (this.catalog.getProviderId());
    }


    /**
     *  Gets the <code> Resource </code> representing the provider of this 
     *  catalog. 
     *
     *  @return the provider 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getProvider()
        throws org.osid.OperationFailedException {
	
	return (this.catalog.getProvider());
    }


    /**
     *  Gets a branding, such as an image or logo, expressed using the <code> 
     *  Asset </code> interface. 
     *
     *  @return a list of assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getBranding()
        throws org.osid.OperationFailedException {

	return (this.catalog.getBranding());
    }
}
