//
// AbstractQueryPriceScheduleLookupSession.java
//
//    An inline adapter that maps a PriceScheduleLookupSession to
//    a PriceScheduleQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PriceScheduleLookupSession to
 *  a PriceScheduleQuerySession.
 */

public abstract class AbstractQueryPriceScheduleLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractPriceScheduleLookupSession
    implements org.osid.ordering.PriceScheduleLookupSession {

    private final org.osid.ordering.PriceScheduleQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPriceScheduleLookupSession.
     *
     *  @param querySession the underlying price schedule query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPriceScheduleLookupSession(org.osid.ordering.PriceScheduleQuerySession querySession) {
        nullarg(querySession, "price schedule query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Store</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform <code>PriceSchedule</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPriceSchedules() {
        return (this.session.canSearchPriceSchedules());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price schedules in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    
     
    /**
     *  Gets the <code>PriceSchedule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PriceSchedule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PriceSchedule</code> and
     *  retained for compatibility.
     *
     *  @param  priceScheduleId <code>Id</code> of the
     *          <code>PriceSchedule</code>
     *  @return the price schedule
     *  @throws org.osid.NotFoundException <code>priceScheduleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>priceScheduleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.PriceScheduleQuery query = getQuery();
        query.matchId(priceScheduleId, true);
        org.osid.ordering.PriceScheduleList priceSchedules = this.session.getPriceSchedulesByQuery(query);
        if (priceSchedules.hasNext()) {
            return (priceSchedules.getNextPriceSchedule());
        } 
        
        throw new org.osid.NotFoundException(priceScheduleId + " not found");
    }

    
    /**
     *  Gets the <code> PriceSchedule </code> by a <code> Price
     *  </code> <code> Id. </code>
     *
     *  @param  priceId <code> Id </code> of a <code> Price </code> 
     *  @return the price schedule 
     *  @throws org.osid.NotFoundException <code> priceId </code> not found 
     *  @throws org.osid.NullArgumentException <code> priceId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceScheduleByPrice(org.osid.id.Id priceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.PriceScheduleQuery query = getQuery();
        query.matchId(priceId, true);
        org.osid.ordering.PriceScheduleList priceSchedules = this.session.getPriceSchedulesByQuery(query);
        if (priceSchedules.hasNext()) {
            return (priceSchedules.getNextPriceSchedule());
        }

        throw new org.osid.NotFoundException(priceId + " not found");
    }


    /**
     *  Gets a <code>PriceScheduleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  priceSchedules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>PriceSchedules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  priceScheduleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PriceSchedule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByIds(org.osid.id.IdList priceScheduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.PriceScheduleQuery query = getQuery();

        try (org.osid.id.IdList ids = priceScheduleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPriceSchedulesByQuery(query));
    }


    /**
     *  Gets a <code>PriceScheduleList</code> corresponding to the given
     *  price schedule genus <code>Type</code> which does not include
     *  price schedules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  priceScheduleGenusType a priceSchedule genus type 
     *  @return the returned <code>PriceSchedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByGenusType(org.osid.type.Type priceScheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.PriceScheduleQuery query = getQuery();
        query.matchGenusType(priceScheduleGenusType, true);
        return (this.session.getPriceSchedulesByQuery(query));
    }


    /**
     *  Gets a <code>PriceScheduleList</code> corresponding to the given
     *  price schedule genus <code>Type</code> and include any additional
     *  price schedules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  priceScheduleGenusType a priceSchedule genus type 
     *  @return the returned <code>PriceSchedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByParentGenusType(org.osid.type.Type priceScheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.PriceScheduleQuery query = getQuery();
        query.matchParentGenusType(priceScheduleGenusType, true);
        return (this.session.getPriceSchedulesByQuery(query));
    }


    /**
     *  Gets a <code>PriceScheduleList</code> containing the given
     *  price schedule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  priceScheduleRecordType a priceSchedule record type 
     *  @return the returned <code>PriceSchedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedulesByRecordType(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.PriceScheduleQuery query = getQuery();
        query.matchRecordType(priceScheduleRecordType, true);
        return (this.session.getPriceSchedulesByQuery(query));
    }

    
    /**
     *  Gets all <code>PriceSchedules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  price schedules or an error results. Otherwise, the returned list
     *  may contain only those price schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>PriceSchedules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.PriceScheduleQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPriceSchedulesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.ordering.PriceScheduleQuery getQuery() {
        org.osid.ordering.PriceScheduleQuery query = this.session.getPriceScheduleQuery();
        return (query);
    }
}
