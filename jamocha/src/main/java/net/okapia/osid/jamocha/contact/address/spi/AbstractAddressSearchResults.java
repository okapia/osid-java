//
// AbstractAddressSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.address.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAddressSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.contact.AddressSearchResults {

    private org.osid.contact.AddressList addresses;
    private final org.osid.contact.AddressQueryInspector inspector;
    private final java.util.Collection<org.osid.contact.records.AddressSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAddressSearchResults.
     *
     *  @param addresses the result set
     *  @param addressQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>addresses</code>
     *          or <code>addressQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAddressSearchResults(org.osid.contact.AddressList addresses,
                                            org.osid.contact.AddressQueryInspector addressQueryInspector) {
        nullarg(addresses, "addresses");
        nullarg(addressQueryInspector, "address query inspectpr");

        this.addresses = addresses;
        this.inspector = addressQueryInspector;

        return;
    }


    /**
     *  Gets the address list resulting from a search.
     *
     *  @return an address list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddresses() {
        if (this.addresses == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.contact.AddressList addresses = this.addresses;
        this.addresses = null;
	return (addresses);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.contact.AddressQueryInspector getAddressQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  address search record <code> Type. </code> This method must
     *  be used to retrieve an address implementing the requested
     *  record.
     *
     *  @param addressSearchRecordType an address search 
     *         record type 
     *  @return the address search
     *  @throws org.osid.NullArgumentException
     *          <code>addressSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(addressSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressSearchResultsRecord getAddressSearchResultsRecord(org.osid.type.Type addressSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.contact.records.AddressSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(addressSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(addressSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record address search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAddressRecord(org.osid.contact.records.AddressSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "address record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
