//
// AbstractFederatingRaceProcessorLookupSession.java
//
//     An abstract federating adapter for a RaceProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RaceProcessorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRaceProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.rules.RaceProcessorLookupSession>
    implements org.osid.voting.rules.RaceProcessorLookupSession {

    private boolean parallel = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Constructs a new <code>AbstractFederatingRaceProcessorLookupSession</code>.
     */

    protected AbstractFederatingRaceProcessorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.rules.RaceProcessorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>RaceProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceProcessors() {
        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            if (session.canLookupRaceProcessors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>RaceProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceProcessorView() {
        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            session.useComparativeRaceProcessorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>RaceProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceProcessorView() {
        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            session.usePlenaryRaceProcessorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race processors in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            session.useFederatedPollsView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            session.useIsolatedPollsView();
        }

        return;
    }


    /**
     *  Only active race processors are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveRaceProcessorView() {
        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            session.useActiveRaceProcessorView();
        }

        return;
    }


    /**
     *  Active and inactive race processors are returned by methods in
     *  this session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceProcessorView() {
        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            session.useAnyStatusRaceProcessorView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>RaceProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RaceProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @param  raceProcessorId <code>Id</code> of the
     *          <code>RaceProcessor</code>
     *  @return the race processor
     *  @throws org.osid.NotFoundException <code>raceProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessor getRaceProcessor(org.osid.id.Id raceProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            try {
                return (session.getRaceProcessor(raceProcessorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(raceProcessorId + " not found");
    }


    /**
     *  Gets a <code>RaceProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  raceProcessors specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>RaceProcessors</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, race processors are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processors are returned.
     *
     *  @param  raceProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByIds(org.osid.id.IdList raceProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.rules.raceprocessor.MutableRaceProcessorList ret = new net.okapia.osid.jamocha.voting.rules.raceprocessor.MutableRaceProcessorList();

        try (org.osid.id.IdList ids = raceProcessorIds) {
            while (ids.hasNext()) {
                ret.addRaceProcessor(getRaceProcessor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RaceProcessorList</code> corresponding to the
     *  given race processor genus <code>Type</code> which does not
     *  include race processors of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race processors are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processors are returned.
     *
     *  @param  raceProcessorGenusType a raceProcessor genus type 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByGenusType(org.osid.type.Type raceProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessor.FederatingRaceProcessorList ret = getRaceProcessorList();

        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            ret.addRaceProcessorList(session.getRaceProcessorsByGenusType(raceProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceProcessorList</code> corresponding to the
     *  given race processor genus <code>Type</code> and include any
     *  additional race processors with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race processors are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processors are returned.
     *
     *  @param  raceProcessorGenusType a raceProcessor genus type 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByParentGenusType(org.osid.type.Type raceProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessor.FederatingRaceProcessorList ret = getRaceProcessorList();

        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            ret.addRaceProcessorList(session.getRaceProcessorsByParentGenusType(raceProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceProcessorList</code> containing the given
     *  race processor record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known race
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race processors are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processors are returned.
     *
     *  @param  raceProcessorRecordType a raceProcessor record type 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByRecordType(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessor.FederatingRaceProcessorList ret = getRaceProcessorList();

        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            ret.addRaceProcessorList(session.getRaceProcessorsByRecordType(raceProcessorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>RaceProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @return a list of <code>RaceProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessor.FederatingRaceProcessorList ret = getRaceProcessorList();

        for (org.osid.voting.rules.RaceProcessorLookupSession session : getSessions()) {
            ret.addRaceProcessorList(session.getRaceProcessors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessor.FederatingRaceProcessorList getRaceProcessorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessor.ParallelRaceProcessorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessor.CompositeRaceProcessorList());
        }
    }
}
