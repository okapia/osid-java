//
// AbstractResourcingRulesManager.java
//
//     An adapter for a ResourcingRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ResourcingRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterResourcingRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.resourcing.rules.ResourcingRulesManager>
    implements org.osid.resourcing.rules.ResourcingRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterResourcingRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterResourcingRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterResourcingRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterResourcingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any job federation is exposed. Federation is exposed when a 
     *  specific job may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of jobs appears 
     *  as a single job. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up availability enabler is supported. 
     *
     *  @return <code> true </code> if availability enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerLookup() {
        return (getAdapteeManager().supportsAvailabilityEnablerLookup());
    }


    /**
     *  Tests if querying availability enabler is supported. 
     *
     *  @return <code> true </code> if availability enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerQuery() {
        return (getAdapteeManager().supportsAvailabilityEnablerQuery());
    }


    /**
     *  Tests if searching availability enabler is supported. 
     *
     *  @return <code> true </code> if availability enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerSearch() {
        return (getAdapteeManager().supportsAvailabilityEnablerSearch());
    }


    /**
     *  Tests if an availability enabler administrative service is supported. 
     *
     *  @return <code> true </code> if availability enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerAdmin() {
        return (getAdapteeManager().supportsAvailabilityEnablerAdmin());
    }


    /**
     *  Tests if an availability enabler notification service is supported. 
     *
     *  @return <code> true </code> if availability enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerNotification() {
        return (getAdapteeManager().supportsAvailabilityEnablerNotification());
    }


    /**
     *  Tests if an availability enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an availability enabler foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerFoundry() {
        return (getAdapteeManager().supportsAvailabilityEnablerFoundry());
    }


    /**
     *  Tests if an availability enabler foundry service is supported. 
     *
     *  @return <code> true </code> if availability enabler foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerFoundryAssignment() {
        return (getAdapteeManager().supportsAvailabilityEnablerFoundryAssignment());
    }


    /**
     *  Tests if an availability enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an availability enabler foundry service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerSmartFoundry() {
        return (getAdapteeManager().supportsAvailabilityEnablerSmartFoundry());
    }


    /**
     *  Tests if an availability enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an availability enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerRuleLookup() {
        return (getAdapteeManager().supportsAvailabilityEnablerRuleLookup());
    }


    /**
     *  Tests if an availability enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if an availability enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerRuleApplication() {
        return (getAdapteeManager().supportsAvailabilityEnablerRuleApplication());
    }


    /**
     *  Tests if looking up commission enabler is supported. 
     *
     *  @return <code> true </code> if commission enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerLookup() {
        return (getAdapteeManager().supportsCommissionEnablerLookup());
    }


    /**
     *  Tests if querying commission enabler is supported. 
     *
     *  @return <code> true </code> if commission enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerQuery() {
        return (getAdapteeManager().supportsCommissionEnablerQuery());
    }


    /**
     *  Tests if searching commission enabler is supported. 
     *
     *  @return <code> true </code> if commission enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerSearch() {
        return (getAdapteeManager().supportsCommissionEnablerSearch());
    }


    /**
     *  Tests if a commission enabler administrative service is supported. 
     *
     *  @return <code> true </code> if commission enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerAdmin() {
        return (getAdapteeManager().supportsCommissionEnablerAdmin());
    }


    /**
     *  Tests if a commission enabler notification service is supported. 
     *
     *  @return <code> true </code> if commission enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerNotification() {
        return (getAdapteeManager().supportsCommissionEnablerNotification());
    }


    /**
     *  Tests if a commission enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission enabler foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerFoundry() {
        return (getAdapteeManager().supportsCommissionEnablerFoundry());
    }


    /**
     *  Tests if a commission enabler foundry service is supported. 
     *
     *  @return <code> true </code> if commission enabler foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerFoundryAssignment() {
        return (getAdapteeManager().supportsCommissionEnablerFoundryAssignment());
    }


    /**
     *  Tests if a commission enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission enabler foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerSmartFoundry() {
        return (getAdapteeManager().supportsCommissionEnablerSmartFoundry());
    }


    /**
     *  Tests if a commission enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a commission enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerRuleLookup() {
        return (getAdapteeManager().supportsCommissionEnablerRuleLookup());
    }


    /**
     *  Tests if a commission enabler rule application service is supported. 
     *
     *  @return <code> true </code> if a commission enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerRuleApplication() {
        return (getAdapteeManager().supportsCommissionEnablerRuleApplication());
    }


    /**
     *  Tests if looking up job constrainer is supported. 
     *
     *  @return <code> true </code> if job constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerLookup() {
        return (getAdapteeManager().supportsJobConstrainerLookup());
    }


    /**
     *  Tests if querying job constrainer is supported. 
     *
     *  @return <code> true </code> if job constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerQuery() {
        return (getAdapteeManager().supportsJobConstrainerQuery());
    }


    /**
     *  Tests if searching job constrainer is supported. 
     *
     *  @return <code> true </code> if job constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerSearch() {
        return (getAdapteeManager().supportsJobConstrainerSearch());
    }


    /**
     *  Tests if a job constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if job constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerAdmin() {
        return (getAdapteeManager().supportsJobConstrainerAdmin());
    }


    /**
     *  Tests if a job constrainer notification service is supported. 
     *
     *  @return <code> true </code> if job constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerNotification() {
        return (getAdapteeManager().supportsJobConstrainerNotification());
    }


    /**
     *  Tests if a job constrainer foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerFoundry() {
        return (getAdapteeManager().supportsJobConstrainerFoundry());
    }


    /**
     *  Tests if a job constrainer foundry service is supported. 
     *
     *  @return <code> true </code> if job constrainer foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerFoundryAssignment() {
        return (getAdapteeManager().supportsJobConstrainerFoundryAssignment());
    }


    /**
     *  Tests if a job constrainer foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerSmartFoundry() {
        return (getAdapteeManager().supportsJobConstrainerSmartFoundry());
    }


    /**
     *  Tests if a job constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerRuleLookup() {
        return (getAdapteeManager().supportsJobConstrainerRuleLookup());
    }


    /**
     *  Tests if a job constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a job constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerRuleApplication() {
        return (getAdapteeManager().supportsJobConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up job constrainer enablers is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsJobConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying job constrainer enablers is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsJobConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching job constrainer enablers is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsJobConstrainerEnablerSearch());
    }


    /**
     *  Tests if a job constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if job constrainer enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsJobConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a job constrainer enabler notification service is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsJobConstrainerEnablerNotification());
    }


    /**
     *  Tests if a job constrainer enabler foundry lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a job constrainer enabler foundry 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerFoundry() {
        return (getAdapteeManager().supportsJobConstrainerEnablerFoundry());
    }


    /**
     *  Tests if a job constrainer enabler foundry service is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler foundry 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerFoundryAssignment() {
        return (getAdapteeManager().supportsJobConstrainerEnablerFoundryAssignment());
    }


    /**
     *  Tests if a job constrainer enabler foundry lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a job constrainer enabler foundry 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerSmartFoundry() {
        return (getAdapteeManager().supportsJobConstrainerEnablerSmartFoundry());
    }


    /**
     *  Tests if a job constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsJobConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a job constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if job constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsJobConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up job processor is supported. 
     *
     *  @return <code> true </code> if job processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorLookup() {
        return (getAdapteeManager().supportsJobProcessorLookup());
    }


    /**
     *  Tests if querying job processor is supported. 
     *
     *  @return <code> true </code> if job processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorQuery() {
        return (getAdapteeManager().supportsJobProcessorQuery());
    }


    /**
     *  Tests if searching job processor is supported. 
     *
     *  @return <code> true </code> if job processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorSearch() {
        return (getAdapteeManager().supportsJobProcessorSearch());
    }


    /**
     *  Tests if a job processor administrative service is supported. 
     *
     *  @return <code> true </code> if job processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorAdmin() {
        return (getAdapteeManager().supportsJobProcessorAdmin());
    }


    /**
     *  Tests if a job processor notification service is supported. 
     *
     *  @return <code> true </code> if job processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorNotification() {
        return (getAdapteeManager().supportsJobProcessorNotification());
    }


    /**
     *  Tests if a job processor foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor foundry lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorFoundry() {
        return (getAdapteeManager().supportsJobProcessorFoundry());
    }


    /**
     *  Tests if a job processor foundry service is supported. 
     *
     *  @return <code> true </code> if job processor foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorFoundryAssignment() {
        return (getAdapteeManager().supportsJobProcessorFoundryAssignment());
    }


    /**
     *  Tests if a job processor foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorSmartFoundry() {
        return (getAdapteeManager().supportsJobProcessorSmartFoundry());
    }


    /**
     *  Tests if a job processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorRuleLookup() {
        return (getAdapteeManager().supportsJobProcessorRuleLookup());
    }


    /**
     *  Tests if a job processor rule application service is supported. 
     *
     *  @return <code> true </code> if job processor rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorRuleApplication() {
        return (getAdapteeManager().supportsJobProcessorRuleApplication());
    }


    /**
     *  Tests if looking up job processor enablers is supported. 
     *
     *  @return <code> true </code> if job processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerLookup() {
        return (getAdapteeManager().supportsJobProcessorEnablerLookup());
    }


    /**
     *  Tests if querying job processor enablers is supported. 
     *
     *  @return <code> true </code> if job processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerQuery() {
        return (getAdapteeManager().supportsJobProcessorEnablerQuery());
    }


    /**
     *  Tests if searching job processor enablers is supported. 
     *
     *  @return <code> true </code> if job processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerSearch() {
        return (getAdapteeManager().supportsJobProcessorEnablerSearch());
    }


    /**
     *  Tests if a job processor enabler administrative service is supported. 
     *
     *  @return <code> true </code> if job processor enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsJobProcessorEnablerAdmin());
    }


    /**
     *  Tests if a job processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if job processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerNotification() {
        return (getAdapteeManager().supportsJobProcessorEnablerNotification());
    }


    /**
     *  Tests if a job processor enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor enabler foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerFoundry() {
        return (getAdapteeManager().supportsJobProcessorEnablerFoundry());
    }


    /**
     *  Tests if a job processor enabler foundry service is supported. 
     *
     *  @return <code> true </code> if job processor enabler foundry 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerFoundryAssignment() {
        return (getAdapteeManager().supportsJobProcessorEnablerFoundryAssignment());
    }


    /**
     *  Tests if a job processor enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor enabler foundry service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerSmartFoundry() {
        return (getAdapteeManager().supportsJobProcessorEnablerSmartFoundry());
    }


    /**
     *  Tests if a job processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsJobProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a job processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if job processor enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsJobProcessorEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> AvailabilityEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> AvailabilityEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilityEnablerRecordTypes() {
        return (getAdapteeManager().getAvailabilityEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> AvailabilityEnabler </code> record type is 
     *  supported. 
     *
     *  @param  availabilityEnablerRecordType a <code> Type </code> indicating 
     *          an <code> AvailabilityEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerRecordType(org.osid.type.Type availabilityEnablerRecordType) {
        return (getAdapteeManager().supportsAvailabilityEnablerRecordType(availabilityEnablerRecordType));
    }


    /**
     *  Gets the supported <code> AvailabilityEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AvailabilityEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilityEnablerSearchRecordTypes() {
        return (getAdapteeManager().getAvailabilityEnablerSearchRecordTypes());
    }


    /**
     *  Gets the supported <code> CommissionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CommissionEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionEnablerRecordTypes() {
        return (getAdapteeManager().getCommissionEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> CommissionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  commissionEnablerRecordType a <code> Type </code> indicating a 
     *          <code> CommissionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerRecordType(org.osid.type.Type commissionEnablerRecordType) {
        return (getAdapteeManager().supportsCommissionEnablerRecordType(commissionEnablerRecordType));
    }


    /**
     *  Gets the supported <code> CommissionEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CommissionEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionEnablerSearchRecordTypes() {
        return (getAdapteeManager().getCommissionEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CommissionEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  commissionEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CommissionEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerSearchRecordType(org.osid.type.Type commissionEnablerSearchRecordType) {
        return (getAdapteeManager().supportsCommissionEnablerSearchRecordType(commissionEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> JobConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> JobConstrainer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerRecordTypes() {
        return (getAdapteeManager().getJobConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> JobConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  jobConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> JobConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerRecordType(org.osid.type.Type jobConstrainerRecordType) {
        return (getAdapteeManager().supportsJobConstrainerRecordType(jobConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> JobConstrainer </code> search record types. 
     *
     *  @return a list containing the supported <code> JobConstrainer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getJobConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> JobConstrainer </code> search record type is 
     *  supported. 
     *
     *  @param  jobConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> JobConstrainer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerSearchRecordType(org.osid.type.Type jobConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsJobConstrainerSearchRecordType(jobConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> JobConstrainerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> JobConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getJobConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> JobConstrainerEnabler </code> record type is 
     *  supported. 
     *
     *  @param  jobConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> JobConstrainerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerRecordType(org.osid.type.Type jobConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsJobConstrainerEnablerRecordType(jobConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> JobConstrainerEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> JobConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getJobConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> JobConstrainerEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  jobConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> JobConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerSearchRecordType(org.osid.type.Type jobConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsJobConstrainerEnablerSearchRecordType(jobConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> JobProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> JobProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorRecordTypes() {
        return (getAdapteeManager().getJobProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> JobProcessor </code> record type is 
     *  supported. 
     *
     *  @param  jobProcessorRecordType a <code> Type </code> indicating a 
     *          <code> JobProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorRecordType(org.osid.type.Type jobProcessorRecordType) {
        return (getAdapteeManager().supportsJobProcessorRecordType(jobProcessorRecordType));
    }


    /**
     *  Gets the supported <code> JobProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> JobProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorSearchRecordTypes() {
        return (getAdapteeManager().getJobProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> JobProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  jobProcessorSearchRecordType a <code> Type </code> indicating 
     *          a <code> JobProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorSearchRecordType(org.osid.type.Type jobProcessorSearchRecordType) {
        return (getAdapteeManager().supportsJobProcessorSearchRecordType(jobProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> JobProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> JobProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getJobProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> JobProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  jobProcessorEnablerRecordType a <code> Type </code> indicating 
     *          a <code> JobProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerRecordType(org.osid.type.Type jobProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsJobProcessorEnablerRecordType(jobProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> JobProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> JobProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getJobProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> JobProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  jobProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> JobProcessorEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerSearchRecordType(org.osid.type.Type jobProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsJobProcessorEnablerSearchRecordType(jobProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler lookup service. 
     *
     *  @return an <code> AvailabilityEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerLookupSession getAvailabilityEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerLookupSession getAvailabilityEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler query service. 
     *
     *  @return an <code> AvailabilityEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerQuerySession getAvailabilityEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerQuerySession getAvailabilityEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler search service. 
     *
     *  @return an <code> AvailabilityEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSearchSession getAvailabilityEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSearchSession getAvailabilityEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler administration service. 
     *
     *  @return an <code> AvailabilityEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerAdminSession getAvailabilityEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerAdminSession getAvailabilityEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler notification service. 
     *
     *  @param  availabilityConstrainerReceiver the notification callback 
     *  @return an <code> AvailabilityEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerNotificationSession getAvailabilityEnablerNotificationSession(org.osid.resourcing.rules.AvailabilityEnablerReceiver availabilityConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerNotificationSession(availabilityConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler notification service for the given foundry. 
     *
     *  @param  availabilityConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityConstrainerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerNotificationSession getAvailabilityEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.AvailabilityEnablerReceiver availabilityConstrainerReceiver, 
                                                                                                                                org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerNotificationSessionForFoundry(availabilityConstrainerReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup availability 
     *  enabler/foundry mappings for availability enablers. 
     *
     *  @return an <code> AvailabilityEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerFoundrySession getAvailabilityEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  availability enabler to foundries. 
     *
     *  @return an <code> AvailabilityEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerFoundryAssignmentSession getAvailabilityEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage availability enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSmartFoundrySession getAvailabilityEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler mapping lookup service for looking up the rules applied to an 
     *  availability. 
     *
     *  @return an <code> AvailabilityEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleLookupSession getAvailabilityEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler mapping lookup service for the given foundry for looking up 
     *  rules applied to an availability. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleLookupSession getAvailabilityEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerRuleLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler assignment service to apply to availabilities. 
     *
     *  @return an <code> AvailabilityEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleApplicationSession getAvailabilityEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler assignment service for the given foundry to apply to 
     *  availabilities. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleApplicationSession getAvailabilityEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAvailabilityEnablerRuleApplicationSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler lookup service. 
     *
     *  @return a <code> CommissionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerLookupSession getCommissionEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerLookupSession getCommissionEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler query service. 
     *
     *  @return a <code> CommissionEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerQuerySession getCommissionEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerQuerySession getCommissionEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler search service. 
     *
     *  @return a <code> CommissionEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSearchSession getCommissionEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSearchSession getCommissionEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler administration service. 
     *
     *  @return a <code> CommissionEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerAdminSession getCommissionEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerAdminSession getCommissionEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler notification service. 
     *
     *  @param  commissionConstrainerReceiver the notification callback 
     *  @return a <code> CommissionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerNotificationSession getCommissionEnablerNotificationSession(org.osid.resourcing.rules.CommissionEnablerReceiver commissionConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerNotificationSession(commissionConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler notification service for the given foundry. 
     *
     *  @param  commissionConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionConstrainerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerNotificationSession getCommissionEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.CommissionEnablerReceiver commissionConstrainerReceiver, 
                                                                                                                            org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerNotificationSessionForFoundry(commissionConstrainerReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commission 
     *  enabler/foundry mappings for commission enablers. 
     *
     *  @return a <code> CommissionEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerFoundrySession getCommissionEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  commission enabler to foundries. 
     *
     *  @return a <code> CommissionEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerFoundryAssignmentSession getCommissionEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commission enabler smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSmartFoundrySession getCommissionEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  commission. 
     *
     *  @return a <code> CommissionEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleLookupSession getCommissionEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler mapping lookup service for the given foundry for looking up 
     *  rules applied to a commission. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleLookupSession getCommissionEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerRuleLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler assignment service to apply to commissions. 
     *
     *  @return a <code> CommissionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleApplicationSession getCommissionEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler assignment service for the given foundry to apply to 
     *  commissions. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleApplicationSession getCommissionEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCommissionEnablerRuleApplicationSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer lookup service. 
     *
     *  @return a <code> JobConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerLookupSession getJobConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerLookupSession getJobConstrainerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer query service. 
     *
     *  @return a <code> JobConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuerySession getJobConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuerySession getJobConstrainerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer search service. 
     *
     *  @return a <code> JobConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSearchSession getJobConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSearchSession getJobConstrainerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer administration service. 
     *
     *  @return a <code> JobConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerAdminSession getJobConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerAdminSession getJobConstrainerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer notification service. 
     *
     *  @param  jobConstrainerReceiver the notification callback 
     *  @return a <code> JobConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerNotificationSession getJobConstrainerNotificationSession(org.osid.resourcing.rules.JobConstrainerReceiver jobConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerNotificationSession(jobConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer notification service for the given foundry. 
     *
     *  @param  jobConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerNotificationSession getJobConstrainerNotificationSessionForFoundry(org.osid.resourcing.rules.JobConstrainerReceiver jobConstrainerReceiver, 
                                                                                                                      org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerNotificationSessionForFoundry(jobConstrainerReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job constrainer/foundry 
     *  mappings for job constrainers. 
     *
     *  @return a <code> JobConstrainerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerFoundrySession getJobConstrainerFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  constrainer to foundries. 
     *
     *  @return a <code> JobConstrainerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerFoundryAssignmentSession getJobConstrainerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job constrainer smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSmartFoundrySession getJobConstrainerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a job. 
     *
     *  @return a <code> JobConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleLookupSession getJobConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer mapping lookup service for the given foundry for looking 
     *  up rules applied to a job. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleLookupSession getJobConstrainerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerRuleLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer assignment service to apply to jobs. 
     *
     *  @return a <code> JobConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleApplicationSession getJobConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer assignment service for the given foundry to apply to jobs. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleApplicationSession getJobConstrainerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerRuleApplicationSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> JobConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerLookupSession getJobConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerLookupSession getJobConstrainerEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler query service. 
     *
     *  @return a <code> JobConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerQuerySession getJobConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerQuerySession getJobConstrainerEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler search service. 
     *
     *  @return a <code> JobConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSearchSession getJobConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enablers earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSearchSession getJobConstrainerEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> JobConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerAdminSession getJobConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerAdminSession getJobConstrainerEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler notification service. 
     *
     *  @param  jobConstrainerEnablerReceiver the notification callback 
     *  @return a <code> JobConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerNotificationSession getJobConstrainerEnablerNotificationSession(org.osid.resourcing.rules.JobConstrainerEnablerReceiver jobConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerNotificationSession(jobConstrainerEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler notification service for the given foundry. 
     *
     *  @param  jobConstrainerEnablerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerNotificationSession getJobConstrainerEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.JobConstrainerEnablerReceiver jobConstrainerEnablerReceiver, 
                                                                                                                                    org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerNotificationSessionForFoundry(jobConstrainerEnablerReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job constrainer 
     *  enabler/foundry mappings for job constrainer enablers. 
     *
     *  @return a <code> JobConstrainerEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerFoundrySession getJobConstrainerEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  constrainer enablers to foundries. 
     *
     *  @return a <code> JobConstrainerEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerFoundryAssignmentSession getJobConstrainerEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job constrainer enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSmartFoundrySession getJobConstrainerEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> JobConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleLookupSession getJobConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler mapping lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleLookupSession getJobConstrainerEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerRuleLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> JobConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleApplicationSession getJobConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleApplicationSession getJobConstrainerEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobConstrainerEnablerRuleApplicationSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  lookup service. 
     *
     *  @return a <code> JobProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorLookupSession getJobProcessorLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorLookupSession getJobProcessorLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  query service. 
     *
     *  @return a <code> JobProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuerySession getJobProcessorQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuerySession getJobProcessorQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  search service. 
     *
     *  @return a <code> JobProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSearchSession getJobProcessorSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSearchSession getJobProcessorSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  administration service. 
     *
     *  @return a <code> JobProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorAdminSession getJobProcessorAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorAdminSession getJobProcessorAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  notification service. 
     *
     *  @param  jobProcessorReceiver the notification callback 
     *  @return a <code> JobProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorNotificationSession getJobProcessorNotificationSession(org.osid.resourcing.rules.JobProcessorReceiver jobProcessorReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorNotificationSession(jobProcessorReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  notification service for the given foundry. 
     *
     *  @param  jobProcessorReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobProcessorReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorNotificationSession getJobProcessorNotificationSessionForFoundry(org.osid.resourcing.rules.JobProcessorReceiver jobProcessorReceiver, 
                                                                                                                  org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorNotificationSessionForFoundry(jobProcessorReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job processor/foundry 
     *  mappings for job processors. 
     *
     *  @return a <code> JobProcessorFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorFoundrySession getJobProcessorFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  processor to foundries. 
     *
     *  @return a <code> JobProcessorFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorFoundryAssignmentSession getJobProcessorFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job processor smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSmartFoundrySession getJobProcessorSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  mapping lookup service for looking up the rules applied to the job. 
     *
     *  @return a <code> JobProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleLookupSession getJobProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  mapping lookup service for the given foundry for looking up rules 
     *  applied to a job. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleLookupSession getJobProcessorRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorRuleLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  assignment service. 
     *
     *  @return a <code> JobProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleApplicationSession getJobProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleApplicationSession getJobProcessorRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorRuleApplicationSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler lookup service. 
     *
     *  @return a <code> JobProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerLookupSession getJobProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerLookupSession getJobProcessorEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler query service. 
     *
     *  @return a <code> JobProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerQuerySession getJobProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerQuerySession getJobProcessorEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerQuerySessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler search service. 
     *
     *  @return a <code> JobProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSearchSession getJobProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enablers earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSearchSession getJobProcessorEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerSearchSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler administration service. 
     *
     *  @return a <code> JobProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerAdminSession getJobProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerAdminSession getJobProcessorEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerAdminSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler notification service. 
     *
     *  @param  jobProcessorEnablerReceiver the notification callback 
     *  @return a <code> JobProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerNotificationSession getJobProcessorEnablerNotificationSession(org.osid.resourcing.rules.JobProcessorEnablerReceiver jobProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerNotificationSession(jobProcessorEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler notification service for the given foundry. 
     *
     *  @param  jobProcessorEnablerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerNotificationSession getJobProcessorEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.JobProcessorEnablerReceiver jobProcessorEnablerReceiver, 
                                                                                                                                org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerNotificationSessionForFoundry(jobProcessorEnablerReceiver, foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job processor 
     *  enabler/foundry mappings for job processor enablers. 
     *
     *  @return a <code> JobProcessorEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerFoundrySession getJobProcessorEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerFoundrySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  processor enablers to foundries. 
     *
     *  @return a <code> JobProcessorEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerFoundryAssignmentSession getJobProcessorEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerFoundryAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job processor enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSmartFoundrySession getJobProcessorEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerSmartFoundrySession(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> JobProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleLookupSession getJobProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler mapping lookup service. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleLookupSession getJobProcessorEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerRuleLookupSessionForFoundry(foundryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler assignment service. 
     *
     *  @return a <code> JobProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleApplicationSession getJobProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleApplicationSession getJobProcessorEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJobProcessorEnablerRuleApplicationSessionForFoundry(foundryId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
