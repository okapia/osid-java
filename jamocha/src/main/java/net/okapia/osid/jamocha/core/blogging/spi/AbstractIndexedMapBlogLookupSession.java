//
// AbstractIndexedMapBlogLookupSession.java
//
//    A simple framework for providing a Blog lookup service
//    backed by a fixed collection of blogs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Blog lookup service backed by a
 *  fixed collection of blogs. The blogs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some blogs may be compatible
 *  with more types than are indicated through these blog
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Blogs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBlogLookupSession
    extends AbstractMapBlogLookupSession
    implements org.osid.blogging.BlogLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.blogging.Blog> blogsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.blogging.Blog>());
    private final MultiMap<org.osid.type.Type, org.osid.blogging.Blog> blogsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.blogging.Blog>());


    /**
     *  Makes a <code>Blog</code> available in this session.
     *
     *  @param  blog a blog
     *  @throws org.osid.NullArgumentException <code>blog<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBlog(org.osid.blogging.Blog blog) {
        super.putBlog(blog);

        this.blogsByGenus.put(blog.getGenusType(), blog);
        
        try (org.osid.type.TypeList types = blog.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.blogsByRecord.put(types.getNextType(), blog);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a blog from this session.
     *
     *  @param blogId the <code>Id</code> of the blog
     *  @throws org.osid.NullArgumentException <code>blogId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBlog(org.osid.id.Id blogId) {
        org.osid.blogging.Blog blog;
        try {
            blog = getBlog(blogId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.blogsByGenus.remove(blog.getGenusType());

        try (org.osid.type.TypeList types = blog.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.blogsByRecord.remove(types.getNextType(), blog);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBlog(blogId);
        return;
    }


    /**
     *  Gets a <code>BlogList</code> corresponding to the given
     *  blog genus <code>Type</code> which does not include
     *  blogs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known blogs or an error results. Otherwise,
     *  the returned list may contain only those blogs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  blogGenusType a blog genus type 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByGenusType(org.osid.type.Type blogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.blogging.blog.ArrayBlogList(this.blogsByGenus.get(blogGenusType)));
    }


    /**
     *  Gets a <code>BlogList</code> containing the given
     *  blog record <code>Type</code>. In plenary mode, the
     *  returned list contains all known blogs or an error
     *  results. Otherwise, the returned list may contain only those
     *  blogs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  blogRecordType a blog record type 
     *  @return the returned <code>blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByRecordType(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.blogging.blog.ArrayBlogList(this.blogsByRecord.get(blogRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.blogsByGenus.clear();
        this.blogsByRecord.clear();

        super.close();

        return;
    }
}
