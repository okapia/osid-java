//
// AbstractResourcingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractResourcingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.resourcing.batch.ResourcingBatchManager,
               org.osid.resourcing.batch.ResourcingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractResourcingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractResourcingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of jobs is available. 
     *
     *  @return <code> true </code> if a job bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of works is available. 
     *
     *  @return <code> true </code> if a work bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of competencies is available. 
     *
     *  @return <code> true </code> if a competency bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of availabilities is available. 
     *
     *  @return <code> true </code> if an availability bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of commissions is available. 
     *
     *  @return <code> true </code> if a commission bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of efforts is available. 
     *
     *  @return <code> true </code> if an effort bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of foundries is available. 
     *
     *  @return <code> true </code> if a foundry bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk job 
     *  administration service. 
     *
     *  @return a <code> JobBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.JobBatchAdminSession getJobBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getJobBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk job 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.JobBatchAdminSession getJobBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getJobBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk job 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobBatchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.JobBatchAdminSession getJobBatchAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getJobBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk job 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJobBatchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.JobBatchAdminSession getJobBatchAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getJobBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk work 
     *  administration service. 
     *
     *  @return a <code> WorkBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.WorkBatchAdminSession getWorkBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getWorkBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk work 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> WorkBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.WorkBatchAdminSession getWorkBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getWorkBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk work 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> WorkBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.WorkBatchAdminSession getWorkBatchAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getWorkBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk work 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> WorkBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.WorkBatchAdminSession getWorkBatchAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getWorkBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  competency administration service. 
     *
     *  @return a <code> CompetencyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CompetencyBatchAdminSession getCompetencyBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getCompetencyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  competency administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CompetencyBatchAdminSession getCompetencyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getCompetencyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  competency administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CompetencyBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CompetencyBatchAdminSession getCompetencyBatchAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getCompetencyBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  competency administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompetencyBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CompetencyBatchAdminSession getCompetencyBatchAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getCompetencyBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  availability administration service. 
     *
     *  @return an <code> AvailabilityBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.AvailabilityBatchAdminSession getAvailabilityBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getAvailabilityBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  availability administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.AvailabilityBatchAdminSession getAvailabilityBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getAvailabilityBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  availability administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.AvailabilityBatchAdminSession getAvailabilityBatchAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getAvailabilityBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  availability administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.AvailabilityBatchAdminSession getAvailabilityBatchAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getAvailabilityBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  commission administration service. 
     *
     *  @return a <code> CommissionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CommissionBatchAdminSession getCommissionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getCommissionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  commission administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CommissionBatchAdminSession getCommissionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getCommissionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  commission administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CommissionBatchAdminSession getCommissionBatchAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getCommissionBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  commission administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.CommissionBatchAdminSession getCommissionBatchAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getCommissionBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk effort 
     *  administration service. 
     *
     *  @return an <code> EffortBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.EffortBatchAdminSession getEffortBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getEffortBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk effort 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EffortBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.EffortBatchAdminSession getEffortBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getEffortBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk effort 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> EffortBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.EffortBatchAdminSession getEffortBatchAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getEffortBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk effort 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EffortBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEffortBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.EffortBatchAdminSession getEffortBatchAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getEffortBatchAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk foundry 
     *  administration service. 
     *
     *  @return a <code> FoundryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.FoundryBatchAdminSession getFoundryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchManager.getFoundryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk foundry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FoundryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFoundryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.batch.FoundryBatchAdminSession getFoundryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.batch.ResourcingBatchProxyManager.getFoundryBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
