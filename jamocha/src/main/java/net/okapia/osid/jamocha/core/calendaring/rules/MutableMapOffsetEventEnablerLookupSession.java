//
// MutableMapOffsetEventEnablerLookupSession
//
//    Implements an OffsetEventEnabler lookup service backed by a collection of
//    offsetEventEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements an OffsetEventEnabler lookup service backed by a collection of
 *  offset event enablers. The offset event enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of offset event enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapOffsetEventEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractMapOffsetEventEnablerLookupSession
    implements org.osid.calendaring.rules.OffsetEventEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapOffsetEventEnablerLookupSession}
     *  with no offset event enablers.
     *
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumentException {@code calendar} is
     *          {@code null}
     */

      public MutableMapOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOffsetEventEnablerLookupSession} with a
     *  single offsetEventEnabler.
     *
     *  @param calendar the calendar  
     *  @param offsetEventEnabler an offset event enabler
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code offsetEventEnabler} is {@code null}
     */

    public MutableMapOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                           org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler) {
        this(calendar);
        putOffsetEventEnabler(offsetEventEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOffsetEventEnablerLookupSession}
     *  using an array of offset event enablers.
     *
     *  @param calendar the calendar
     *  @param offsetEventEnablers an array of offset event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code offsetEventEnablers} is {@code null}
     */

    public MutableMapOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                           org.osid.calendaring.rules.OffsetEventEnabler[] offsetEventEnablers) {
        this(calendar);
        putOffsetEventEnablers(offsetEventEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOffsetEventEnablerLookupSession}
     *  using a collection of offset event enablers.
     *
     *  @param calendar the calendar
     *  @param offsetEventEnablers a collection of offset event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code offsetEventEnablers} is {@code null}
     */

    public MutableMapOffsetEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                           java.util.Collection<? extends org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablers) {

        this(calendar);
        putOffsetEventEnablers(offsetEventEnablers);
        return;
    }

    
    /**
     *  Makes an {@code OffsetEventEnabler} available in this session.
     *
     *  @param offsetEventEnabler an offset event enabler
     *  @throws org.osid.NullArgumentException {@code offsetEventEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putOffsetEventEnabler(org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler) {
        super.putOffsetEventEnabler(offsetEventEnabler);
        return;
    }


    /**
     *  Makes an array of offset event enablers available in this session.
     *
     *  @param offsetEventEnablers an array of offset event enablers
     *  @throws org.osid.NullArgumentException {@code offsetEventEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putOffsetEventEnablers(org.osid.calendaring.rules.OffsetEventEnabler[] offsetEventEnablers) {
        super.putOffsetEventEnablers(offsetEventEnablers);
        return;
    }


    /**
     *  Makes collection of offset event enablers available in this session.
     *
     *  @param offsetEventEnablers a collection of offset event enablers
     *  @throws org.osid.NullArgumentException {@code offsetEventEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putOffsetEventEnablers(java.util.Collection<? extends org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablers) {
        super.putOffsetEventEnablers(offsetEventEnablers);
        return;
    }


    /**
     *  Removes an OffsetEventEnabler from this session.
     *
     *  @param offsetEventEnablerId the {@code Id} of the offset event enabler
     *  @throws org.osid.NullArgumentException {@code offsetEventEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeOffsetEventEnabler(org.osid.id.Id offsetEventEnablerId) {
        super.removeOffsetEventEnabler(offsetEventEnablerId);
        return;
    }    
}
