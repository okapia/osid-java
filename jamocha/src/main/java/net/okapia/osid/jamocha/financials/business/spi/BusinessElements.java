//
// BusinessElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.business.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BusinessElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the BusinessElement Id.
     *
     *  @return the business element Id
     */

    public static org.osid.id.Id getBusinessEntityId() {
        return (makeEntityId("osid.financials.Business"));
    }


    /**
     *  Gets the AccountId element Id.
     *
     *  @return the AccountId element Id
     */

    public static org.osid.id.Id getAccountId() {
        return (makeQueryElementId("osid.financials.business.AccountId"));
    }


    /**
     *  Gets the Account element Id.
     *
     *  @return the Account element Id
     */

    public static org.osid.id.Id getAccount() {
        return (makeQueryElementId("osid.financials.business.Account"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeQueryElementId("osid.financials.business.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeQueryElementId("osid.financials.business.Activity"));
    }


    /**
     *  Gets the FiscalPeriodId element Id.
     *
     *  @return the FiscalPeriodId element Id
     */

    public static org.osid.id.Id getFiscalPeriodId() {
        return (makeQueryElementId("osid.financials.business.FiscalPeriodId"));
    }


    /**
     *  Gets the FiscalPeriod element Id.
     *
     *  @return the FiscalPeriod element Id
     */

    public static org.osid.id.Id getFiscalPeriod() {
        return (makeQueryElementId("osid.financials.business.FiscalPeriod"));
    }


    /**
     *  Gets the AncestorBusinessId element Id.
     *
     *  @return the AncestorBusinessId element Id
     */

    public static org.osid.id.Id getAncestorBusinessId() {
        return (makeQueryElementId("osid.financials.business.AncestorBusinessId"));
    }


    /**
     *  Gets the AncestorBusiness element Id.
     *
     *  @return the AncestorBusiness element Id
     */

    public static org.osid.id.Id getAncestorBusiness() {
        return (makeQueryElementId("osid.financials.business.AncestorBusiness"));
    }


    /**
     *  Gets the DescendantBusinessId element Id.
     *
     *  @return the DescendantBusinessId element Id
     */

    public static org.osid.id.Id getDescendantBusinessId() {
        return (makeQueryElementId("osid.financials.business.DescendantBusinessId"));
    }


    /**
     *  Gets the DescendantBusiness element Id.
     *
     *  @return the DescendantBusiness element Id
     */

    public static org.osid.id.Id getDescendantBusiness() {
        return (makeQueryElementId("osid.financials.business.DescendantBusiness"));
    }
}
