//
// AbstractAdapterLogEntryLookupSession.java
//
//    A LogEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A LogEntry lookup session adapter.
 */

public abstract class AbstractAdapterLogEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.tracking.LogEntryLookupSession {

    private final org.osid.tracking.LogEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterLogEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterLogEntryLookupSession(org.osid.tracking.LogEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code FrontOffice/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code FrontOffice Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the {@code FrontOffice} associated with this session.
     *
     *  @return the {@code FrontOffice} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform {@code LogEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupLogEntries() {
        return (this.session.canLookupLogEntries());
    }


    /**
     *  A complete view of the {@code LogEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLogEntryView() {
        this.session.useComparativeLogEntryView();
        return;
    }


    /**
     *  A complete view of the {@code LogEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLogEntryView() {
        this.session.usePlenaryLogEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include log entries in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    
     
    /**
     *  Gets the {@code LogEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code LogEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code LogEntry} and
     *  retained for compatibility.
     *
     *  @param logEntryId {@code Id} of the {@code LogEntry}
     *  @return the log entry
     *  @throws org.osid.NotFoundException {@code logEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code logEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntry getLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntry(logEntryId));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  logEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code LogEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  logEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByIds(org.osid.id.IdList logEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByIds(logEntryIds));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given
     *  log entry genus {@code Type} which does not include
     *  log entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByGenusType(logEntryGenusType));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given
     *  log entry genus {@code Type} and include any additional
     *  log entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByParentGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByParentGenusType(logEntryGenusType));
    }


    /**
     *  Gets a {@code LogEntryList} containing the given
     *  log entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryRecordType a logEntry record type 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByRecordType(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByRecordType(logEntryRecordType));
    }


    /**
     *  Gets a list of log entries logged in the date range. In
     *  plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those log entries that are accessible
     *  through this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getLogEntriesByDate(from, to));
    }


    /**
     *  Gets a {@code LogEntryList} in the given {@code Queue.}  In
     *  plenary mode, the returned list contains all of the log
     *  entries, or an error results if a log entry connected to the
     *  issue is not found or inaccessible. Otherwise, inaccessible
     *  {@code LogEntries} may be omitted from the list.
     *
     *  @param queueId a queue {@code Id}
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.NullArgumentException {@code issueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesForQueue(queueId));
    }


    /**
     *  Gets a list of log entries logged in the date range in the
     *  given queue. In plenary mode, the returned list contains all
     *  known availabilities or an error results. Otherwise, the
     *  returned list may contain only those log entries that are
     *  accessible through this session.
     *
     *  @param  queueId a queue {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code queueId, from}
     *          or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDateForQueue(org.osid.id.Id queueId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByDateForQueue(queueId, from, to));
    }


    /**
     *  Gets a {@code LogEntryList} connected to the given {@code
     *  Issue}. In plenary mode, the returned list contains all of the
     *  log entries, or an error results if a log entry connected to
     *  the issue is not found or inaccessible. Otherwise,
     *  inaccessible {@code LogEntries} may be omitted from the list.
     *
     *  @param  issueId an issue {@code Id} 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesForIssue(issueId));
    }


    /**
     *  Gets a list of log entries logged in the date range in the
     *  given issue. In plenary mode, the returned list contains all
     *  known availabilities or an error results. Otherwise, the
     *  returned list may contain only those log entries that are
     *  accessible through this session.
     *
     *  @param  issueId an issue {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code issueId, from} or 
     *          {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntriesByDateForIssue(org.osid.id.Id issueId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByDateForIssue(issueId, from, to));
    }


    /**
     *  Gets all {@code LogEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code LogEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryList getLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntries());
    }
}
