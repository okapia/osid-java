//
// AbstractDeviceEnablerSearch.java
//
//     A template for making a DeviceEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.deviceenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing device enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDeviceEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.rules.DeviceEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.DeviceEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.rules.DeviceEnablerSearchOrder deviceEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of device enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  deviceEnablerIds list of device enablers
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDeviceEnablers(org.osid.id.IdList deviceEnablerIds) {
        while (deviceEnablerIds.hasNext()) {
            try {
                this.ids.add(deviceEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDeviceEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of device enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDeviceEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  deviceEnablerSearchOrder device enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>deviceEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDeviceEnablerResults(org.osid.control.rules.DeviceEnablerSearchOrder deviceEnablerSearchOrder) {
	this.deviceEnablerSearchOrder = deviceEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.rules.DeviceEnablerSearchOrder getDeviceEnablerSearchOrder() {
	return (this.deviceEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given device enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a device enabler implementing the requested record.
     *
     *  @param deviceEnablerSearchRecordType a device enabler search record
     *         type
     *  @return the device enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.DeviceEnablerSearchRecord getDeviceEnablerSearchRecord(org.osid.type.Type deviceEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.rules.records.DeviceEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(deviceEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this device enabler search. 
     *
     *  @param deviceEnablerSearchRecord device enabler search record
     *  @param deviceEnablerSearchRecordType deviceEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDeviceEnablerSearchRecord(org.osid.control.rules.records.DeviceEnablerSearchRecord deviceEnablerSearchRecord, 
                                           org.osid.type.Type deviceEnablerSearchRecordType) {

        addRecordType(deviceEnablerSearchRecordType);
        this.records.add(deviceEnablerSearchRecord);        
        return;
    }
}
