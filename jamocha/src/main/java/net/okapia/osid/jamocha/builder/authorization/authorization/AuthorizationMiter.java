//
// AuthorizationMiter.java
//
//     Defines an Authorization miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.authorization;


/**
 *  Defines an <code>Authorization</code> miter for use with the builders.
 */

public interface AuthorizationMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.authorization.Authorization {


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this authorization is
     *         implicit, <code> false </code> otherwise
     */

    public void setImplicit(boolean implicit);


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the trust.
     *
     *  @param trust a trust
     *  @throws org.osid.NullArgumentException <code>trust</code> is
     *          <code>null</code>
     */

    public void setTrust(org.osid.authentication.process.Trust trust);


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the function.
     *
     *  @param function a function
     *  @throws org.osid.NullArgumentException <code>function</code>
     *          is <code>null</code>
     */

    public void setFunction(org.osid.authorization.Function function);


    /**
     *  Sets the qualifier.
     *
     *  @param qualifier a qualifier
     *  @throws org.osid.NullArgumentException <code>qualifier</code>
     *          is <code>null</code>
     */

    public void setQualifier(org.osid.authorization.Qualifier qualifier);


    /**
     *  Adds an Authorization record.
     *
     *  @param record an authorization record
     *  @param recordType the type of authorization record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAuthorizationRecord(org.osid.authorization.records.AuthorizationRecord record, org.osid.type.Type recordType);
}       


