//
// BinElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.bin.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BinElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the BinElement Id.
     *
     *  @return the bin element Id
     */

    public static org.osid.id.Id getBinEntityId() {
        return (makeEntityId("osid.resource.Bin"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeQueryElementId("osid.resource.bin.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeQueryElementId("osid.resource.bin.Resource"));
    }


    /**
     *  Gets the AncestorBinId element Id.
     *
     *  @return the AncestorBinId element Id
     */

    public static org.osid.id.Id getAncestorBinId() {
        return (makeQueryElementId("osid.resource.bin.AncestorBinId"));
    }


    /**
     *  Gets the AncestorBin element Id.
     *
     *  @return the AncestorBin element Id
     */

    public static org.osid.id.Id getAncestorBin() {
        return (makeQueryElementId("osid.resource.bin.AncestorBin"));
    }


    /**
     *  Gets the DescendantBinId element Id.
     *
     *  @return the DescendantBinId element Id
     */

    public static org.osid.id.Id getDescendantBinId() {
        return (makeQueryElementId("osid.resource.bin.DescendantBinId"));
    }


    /**
     *  Gets the DescendantBin element Id.
     *
     *  @return the DescendantBin element Id
     */

    public static org.osid.id.Id getDescendantBin() {
        return (makeQueryElementId("osid.resource.bin.DescendantBin"));
    }
}
