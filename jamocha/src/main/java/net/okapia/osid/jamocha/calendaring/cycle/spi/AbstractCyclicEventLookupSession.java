//
// AbstractCyclicEventLookupSession.java
//
//    A starter implementation framework for providing a CyclicEvent
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CyclicEvent
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCyclicEvents(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCyclicEventLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>CyclicEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCyclicEvents() {
        return (true);
    }


    /**
     *  A complete view of the <code>CyclicEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCyclicEventView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CyclicEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCyclicEventView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic events in calendars which are
     *  children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>CyclicEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CyclicEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CyclicEvent</code> and
     *  retained for compatibility.
     *
     *  @param  cyclicEventId <code>Id</code> of the
     *          <code>CyclicEvent</code>
     *  @return the cyclic event
     *  @throws org.osid.NotFoundException <code>cyclicEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>cyclicEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent(org.osid.id.Id cyclicEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.cycle.CyclicEventList cyclicEvents = getCyclicEvents()) {
            while (cyclicEvents.hasNext()) {
                org.osid.calendaring.cycle.CyclicEvent cyclicEvent = cyclicEvents.getNextCyclicEvent();
                if (cyclicEvent.getId().equals(cyclicEventId)) {
                    return (cyclicEvent);
                }
            }
        } 

        throw new org.osid.NotFoundException(cyclicEventId + " not found");
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CyclicEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCyclicEvents()</code>.
     *
     *  @param  cyclicEventIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByIds(org.osid.id.IdList cyclicEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.cycle.CyclicEvent> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = cyclicEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCyclicEvent(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("cyclic event " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.LinkedCyclicEventList(ret));
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  cyclic event genus <code>Type</code> which does not include
     *  cyclic events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCyclicEvents()</code>.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.cycle.cyclicevent.CyclicEventGenusFilterList(getCyclicEvents(), cyclicEventGenusType));
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  cyclic event genus <code>Type</code> and include any additional
     *  cyclic events with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCyclicEvents()</code>.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByParentGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCyclicEventsByGenusType(cyclicEventGenusType));
    }


    /**
     *  Gets a <code>CyclicEventList</code> containing the given
     *  cyclic event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCyclicEvents()</code>.
     *
     *  @param  cyclicEventRecordType a cyclicEvent record type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByRecordType(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.cycle.cyclicevent.CyclicEventRecordFilterList(getCyclicEvents(), cyclicEventRecordType));
    }


    /**
     *  Gets all <code>CyclicEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>CyclicEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.cycle.CyclicEventList getCyclicEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the cyclic event list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of cyclic events
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.cycle.CyclicEventList filterCyclicEventsOnViews(org.osid.calendaring.cycle.CyclicEventList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
