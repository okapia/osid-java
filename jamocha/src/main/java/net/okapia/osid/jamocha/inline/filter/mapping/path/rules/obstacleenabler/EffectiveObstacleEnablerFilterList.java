//
// EffectiveObstacleEnablerFilterList.java
//
//     Implements a filter for effectiveness.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.mapping.path.rules.obstacleenabler;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for effective temporals.
 */

public final class EffectiveObstacleEnablerFilterList
    extends net.okapia.osid.jamocha.inline.filter.mapping.path.rules.obstacleenabler.spi.AbstractObstacleEnablerFilterList
    implements org.osid.mapping.path.rules.ObstacleEnablerList,
               ObstacleEnablerFilter {


    /**
     *  Creates a new <code>EffectiveObstacleEnablerFilterList</code>.
     *
     *  @param list an <code>ObstacleEnablerList</code>
     *  @throws org.osid.NullArgumentException <code>list</code> is
     *          <code>null</code>
     */

    public EffectiveObstacleEnablerFilterList(org.osid.mapping.path.rules.ObstacleEnablerList list) {
        super(list);
        return;
    }    

    
    /**
     *  Filters ObstacleEnablers.
     *
     *  @param obstacleEnabler the obstacle enabler to filter
     *  @return <code>true</code> if the obstacle enabler passes the filter,
     *          <code>false</code> if the obstacle enabler should be filtered
     */

    @Override
    public boolean pass(org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler) {
        return (obstacleEnabler.isEffective());
    }
}
