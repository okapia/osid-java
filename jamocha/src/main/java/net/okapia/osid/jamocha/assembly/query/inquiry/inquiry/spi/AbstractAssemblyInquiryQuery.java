//
// AbstractAssemblyInquiryQuery.java
//
//     An InquiryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InquiryQuery that stores terms.
 */

public abstract class AbstractAssemblyInquiryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.inquiry.InquiryQuery,
               org.osid.inquiry.InquiryQueryInspector,
               org.osid.inquiry.InquirySearchOrder {

    private final java.util.Collection<org.osid.inquiry.records.InquiryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.InquiryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.InquirySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyInquiryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInquiryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the audit <code> Id </code> for this query. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuditId(org.osid.id.Id auditId, boolean match) {
        getAssembler().addIdTerm(getAuditIdColumn(), auditId, match);
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuditIdTerms() {
        getAssembler().clearTerms(getAuditIdColumn());
        return;
    }


    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuditIdTerms() {
        return (getAssembler().getIdTerms(getAuditIdColumn()));
    }


    /**
     *  Orders the results by the audit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAudit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAuditColumn(), style);
        return;
    }


    /**
     *  Gets the AuditId column name.
     *
     * @return the column name
     */

    protected String getAuditIdColumn() {
        return ("audit_id");
    }


    /**
     *  Tests if an <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an <code> Audit, </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getAuditQuery() {
        throw new org.osid.UnimplementedException("supportsAuditQuery() is false");
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearAuditTerms() {
        getAssembler().clearTerms(getAuditColumn());
        return;
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Tests if an audit search order is available. 
     *
     *  @return <code> true </code> if an audit search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditSearchOrder() {
        return (false);
    }


    /**
     *  Gets the audit search order. 
     *
     *  @return the audit search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAuditSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchOrder getAuditSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAuditSearchOrder() is false");
    }


    /**
     *  Gets the Audit column name.
     *
     * @return the column name
     */

    protected String getAuditColumn() {
        return ("audit");
    }


    /**
     *  Sets the question for this query. 
     *
     *  @param  question the question 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> question </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchQuestion(String question, 
                              org.osid.type.Type stringMatchType, 
                              boolean match) {
        getAssembler().addStringTerm(getQuestionColumn(), question, stringMatchType, match);
        return;
    }


    /**
     *  Matches inquiries with any question. 
     *
     *  @param  match <code> true </code> to match inquiries with a question, 
     *          <code> false </code> to match inquiries with no question 
     */

    @OSID @Override
    public void matchAnyQuestion(boolean match) {
        getAssembler().addStringWildcardTerm(getQuestionColumn(), match);
        return;
    }


    /**
     *  Clears the question query terms. 
     */

    @OSID @Override
    public void clearQuestionTerms() {
        getAssembler().clearTerms(getQuestionColumn());
        return;
    }


    /**
     *  Gets the question query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getQuestionTerms() {
        return (getAssembler().getStringTerms(getQuestionColumn()));
    }


    /**
     *  Orders the results by the question. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuestion(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQuestionColumn(), style);
        return;
    }


    /**
     *  Gets the Question column name.
     *
     * @return the column name
     */

    protected String getQuestionColumn() {
        return ("question");
    }


    /**
     *  Matches inquiries that are required. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRequired(boolean match) {
        getAssembler().addBooleanTerm(getRequiredColumn(), match);
        return;
    }


    /**
     *  Matches inquiries with any required flag set. 
     *
     *  @param  match <code> true </code> to match inquiries with a required 
     *          flag set, <code> false </code> to match inquiries with no 
     *          required flag set 
     */

    @OSID @Override
    public void matchAnyRequired(boolean match) {
        getAssembler().addBooleanWildcardTerm(getRequiredColumn(), match);
        return;
    }


    /**
     *  Clears the required query terms. 
     */

    @OSID @Override
    public void clearRequiredTerms() {
        getAssembler().clearTerms(getRequiredColumn());
        return;
    }


    /**
     *  Gets the required query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRequiredTerms() {
        return (getAssembler().getBooleanTerms(getRequiredColumn()));
    }


    /**
     *  Orders the results by the required flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequired(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequiredColumn(), style);
        return;
    }


    /**
     *  Gets the Required column name.
     *
     * @return the column name
     */

    protected String getRequiredColumn() {
        return ("required");
    }


    /**
     *  Matches inquiries that require a positive response. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAffirmationRequired(boolean match) {
        getAssembler().addBooleanTerm(getAffirmationRequiredColumn(), match);
        return;
    }


    /**
     *  Matches inquiries with any affirmation required flag set. 
     *
     *  @param  match <code> true </code> to match inquiries with an 
     *          affirmation required flag set, <code> false </code> to match 
     *          inquiries with no affirmation required flag set 
     */

    @OSID @Override
    public void matchAnyAffirmationRequired(boolean match) {
        getAssembler().addBooleanWildcardTerm(getAffirmationRequiredColumn(), match);
        return;
    }


    /**
     *  Clears the affirmation required query terms. 
     */

    @OSID @Override
    public void clearAffirmationRequiredTerms() {
        getAssembler().clearTerms(getAffirmationRequiredColumn());
        return;
    }


    /**
     *  Gets the affirmation required query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAffirmationRequiredTerms() {
        return (getAssembler().getBooleanTerms(getAffirmationRequiredColumn()));
    }


    /**
     *  Orders the results by the affirmation required flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAffirmationRequired(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAffirmationRequiredColumn(), style);
        return;
    }


    /**
     *  Gets the AffirmationRequired column name.
     *
     * @return the column name
     */

    protected String getAffirmationRequiredColumn() {
        return ("affirmation_required");
    }


    /**
     *  Matches inquiries that require a single response. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchNeedsOneResponse(boolean match) {
        getAssembler().addBooleanTerm(getNeedsOneResponseColumn(), match);
        return;
    }


    /**
     *  Matches inquiries with any needs one response set. 
     *
     *  @param  match <code> true </code> to match inquiries with a need one 
     *          response flag set, <code> false </code> to match inquiries 
     *          with no needs one response flag set 
     */

    @OSID @Override
    public void matchAnyNeedsOneResponse(boolean match) {
        getAssembler().addBooleanWildcardTerm(getNeedsOneResponseColumn(), match);
        return;
    }


    /**
     *  Clears the needs one response query terms. 
     */

    @OSID @Override
    public void clearNeedsOneResponseTerms() {
        getAssembler().clearTerms(getNeedsOneResponseColumn());
        return;
    }


    /**
     *  Gets the needs one response query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getNeedsOneResponseTerms() {
        return (getAssembler().getBooleanTerms(getNeedsOneResponseColumn()));
    }


    /**
     *  Orders the results by the needs one response flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNeedsOneResponse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNeedsOneResponseColumn(), style);
        return;
    }


    /**
     *  Gets the NeedsOneResponse column name.
     *
     * @return the column name
     */

    protected String getNeedsOneResponseColumn() {
        return ("needs_one_response");
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match inquiries 
     *  assigned to inquests. 
     *
     *  @param  inquestId the inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquestId(org.osid.id.Id inquestId, boolean match) {
        getAssembler().addIdTerm(getInquestIdColumn(), inquestId, match);
        return;
    }


    /**
     *  Clears the inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquestIdTerms() {
        getAssembler().clearTerms(getInquestIdColumn());
        return;
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (getAssembler().getIdTerms(getInquestIdColumn()));
    }


    /**
     *  Gets the InquestId column name.
     *
     * @return the column name
     */

    protected String getInquestIdColumn() {
        return ("inquest_id");
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getInquestQuery() {
        throw new org.osid.UnimplementedException("supportsInquestQuery() is false");
    }


    /**
     *  Clears the inquest query terms. 
     */

    @OSID @Override
    public void clearInquestTerms() {
        getAssembler().clearTerms(getInquestColumn());
        return;
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }


    /**
     *  Gets the Inquest column name.
     *
     * @return the column name
     */

    protected String getInquestColumn() {
        return ("inquest");
    }


    /**
     *  Tests if this inquiry supports the given record
     *  <code>Type</code>.
     *
     *  @param  inquiryRecordType an inquiry record type 
     *  @return <code>true</code> if the inquiryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inquiryRecordType) {
        for (org.osid.inquiry.records.InquiryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  inquiryRecordType the inquiry record type 
     *  @return the inquiry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquiryQueryRecord getInquiryQueryRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquiryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inquiryRecordType the inquiry record type 
     *  @return the inquiry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquiryQueryInspectorRecord getInquiryQueryInspectorRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquiryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param inquiryRecordType the inquiry record type
     *  @return the inquiry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquirySearchOrderRecord getInquirySearchOrderRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquirySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this inquiry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inquiryQueryRecord the inquiry query record
     *  @param inquiryQueryInspectorRecord the inquiry query inspector
     *         record
     *  @param inquirySearchOrderRecord the inquiry search order record
     *  @param inquiryRecordType inquiry record type
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryQueryRecord</code>,
     *          <code>inquiryQueryInspectorRecord</code>,
     *          <code>inquirySearchOrderRecord</code> or
     *          <code>inquiryRecordTypeinquiry</code> is
     *          <code>null</code>
     */
            
    protected void addInquiryRecords(org.osid.inquiry.records.InquiryQueryRecord inquiryQueryRecord, 
                                      org.osid.inquiry.records.InquiryQueryInspectorRecord inquiryQueryInspectorRecord, 
                                      org.osid.inquiry.records.InquirySearchOrderRecord inquirySearchOrderRecord, 
                                      org.osid.type.Type inquiryRecordType) {

        addRecordType(inquiryRecordType);

        nullarg(inquiryQueryRecord, "inquiry query record");
        nullarg(inquiryQueryInspectorRecord, "inquiry query inspector record");
        nullarg(inquirySearchOrderRecord, "inquiry search odrer record");

        this.queryRecords.add(inquiryQueryRecord);
        this.queryInspectorRecords.add(inquiryQueryInspectorRecord);
        this.searchOrderRecords.add(inquirySearchOrderRecord);
        
        return;
    }
}
