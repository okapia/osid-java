//
// AbstractAdapterParticipantLookupSession.java
//
//    A Participant lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Participant lookup session adapter.
 */

public abstract class AbstractAdapterParticipantLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.ParticipantLookupSession {

    private final org.osid.offering.ParticipantLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterParticipantLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterParticipantLookupSession(org.osid.offering.ParticipantLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code Participant} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupParticipants() {
        return (this.session.canLookupParticipants());
    }


    /**
     *  A complete view of the {@code Participant} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParticipantView() {
        this.session.useComparativeParticipantView();
        return;
    }


    /**
     *  A complete view of the {@code Participant} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParticipantView() {
        this.session.usePlenaryParticipantView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include participants in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only participants whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveParticipantView() {
        this.session.useEffectiveParticipantView();
        return;
    }
    

    /**
     *  All participants of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveParticipantView() {
        this.session.useAnyEffectiveParticipantView();
        return;
    }

     
    /**
     *  Gets the {@code Participant} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Participant} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Participant} and
     *  retained for compatibility.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param participantId {@code Id} of the {@code Participant}
     *  @return the participant
     *  @throws org.osid.NotFoundException {@code participantId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code participantId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Participant getParticipant(org.osid.id.Id participantId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipant(participantId));
    }


    /**
     *  Gets a {@code ParticipantList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  participants specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Participants} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Participant} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code participantIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByIds(org.osid.id.IdList participantIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByIds(participantIds));
    }


    /**
     *  Gets a {@code ParticipantList} corresponding to the given
     *  participant genus {@code Type} which does not include
     *  participants of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code Participant} list
     *  @throws org.osid.NullArgumentException
     *          {@code participantGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByGenusType(participantGenusType));
    }


    /**
     *  Gets a {@code ParticipantList} corresponding to the given
     *  participant genus {@code Type} and include any additional
     *  participants with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code Participant} list
     *  @throws org.osid.NullArgumentException
     *          {@code participantGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByParentGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByParentGenusType(participantGenusType));
    }


    /**
     *  Gets a {@code ParticipantList} containing the given
     *  participant record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantRecordType a participant record type 
     *  @return the returned {@code Participant} list
     *  @throws org.osid.NullArgumentException
     *          {@code participantRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByRecordType(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByRecordType(participantRecordType));
    }


    /**
     *  Gets a {@code ParticipantList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In active mode, participants are returned that are currently
     *  active. In any status mode, active and inactive participants
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Participant} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsOnDate(from, to));
    }
        

    /**
     *  Gets a {@code ParticipantList} by genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code participantGenusType, 
     *          from,} or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeOnDate(org.osid.type.Type participantGenusType, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByGenusTypeOnDate(participantGenusType, from, to));
    }


    /**
     *  Gets a list of participants corresponding to an offering
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the {@code Id} of the offering
     *  @return the returned {@code ParticipantList}
     *  @throws org.osid.NullArgumentException {@code offeringId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOffering(org.osid.id.Id offeringId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsForOffering(offeringId));
    }

    
    /**
     *  Gets a {@code ParticipantList} by genus type for the given
     *  offering.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code
     *         participantGenusType} or {@code offeringId} is {@code
     *         null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOffering(org.osid.id.Id offeringId, 
                                                                                   org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByGenusTypeForOffering(offeringId, participantGenusType));
    }


    /**
     *  Gets a list of participants corresponding to an offering
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the {@code Id} of the offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ParticipantList}
     *  @throws org.osid.NullArgumentException {@code offeringId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingOnDate(org.osid.id.Id offeringId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsForOfferingOnDate(offeringId, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for an offering
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code offeringId,
     *          participantGenusType, from}, or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingOnDate(org.osid.id.Id offeringId, 
                                                                                         org.osid.type.Type participantGenusType, 
                                                                                         org.osid.calendaring.DateTime from, 
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByGenusTypeForOfferingOnDate(offeringId, participantGenusType, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} for the given offering in a
     *  time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code offeringId} or 
     *          {@code timePeriodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOffering(org.osid.id.Id offeringId,
                                                                                    org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getParticipantsByTimePeriodForOffering(offeringId, timePeriodId));
    }

    
    /**
     *  Gets a {@code ParticipantList} by genus type for the given
     *  offering in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code offeringId, 
     *          timePeriodId,} or {@code participantGenusType} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOffering(org.osid.id.Id offeringId, 
                                                                                                org.osid.id.Id timePeriodId, 
                                                                                                org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodAndGenusTypeForOffering(offeringId, timePeriodId, participantGenusType));
    }


    /**
     *  Gets a {@code ParticipantList} for an offering in a time
     *  period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code offeringId, 
     *          timePeriodId, from,} or {@code to} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingOnDate(org.osid.id.Id offeringId, 
                                                                                          org.osid.id.Id timePeriodId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodForOfferingOnDate(offeringId, timePeriodId, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for an offering
     *  in a time period and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code offeringId,
     *          timePeriodId, participantGenusType, from}, or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingOnDate(org.osid.id.Id offeringId, 
                                                                                                      org.osid.id.Id timePeriodId, 
                                                                                                      org.osid.type.Type participantGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodAndGenusTypeForOfferingOnDate(offeringId, timePeriodId, participantGenusType, from, to));
    }


    /**
     *  Gets a list of participants corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code ParticipantList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsForResource(resourceId));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for the given
     *  resource.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code participantGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                   org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByGenusTypeForResource(resourceId, participantGenusType));
    }


    /**
     *  Gets a list of participants corresponding to a resource
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ParticipantList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForResourceOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for a resource
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          participantGenusType, from,} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                         org.osid.type.Type participantGenusType, 
                                                                                         org.osid.calendaring.DateTime from, 
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByGenusTypeForResourceOnDate(resourceId, participantGenusType, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} for the given resource in a
     *  time period {@code}.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code timePeriodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForResource(org.osid.id.Id resourceId, 
                                                                                    org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodForResource(resourceId, timePeriodId));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for the given
     *  resource in a time period {@code}.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          timePeriodId,} or {@code participantGenusType} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                                org.osid.id.Id timePeriodId, 
                                                                                                org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodAndGenusTypeForResource(resourceId, timePeriodId, participantGenusType));
    }


    /**
     *  Gets a {@code ParticipantList} for a resource in a time period
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          timePeriodId, from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                          org.osid.id.Id timePeriodId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodForResourceOnDate(resourceId, timePeriodId, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for a resource in
     *  a time period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          timePeriodId, participantGenusType, from}, or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                                      org.osid.id.Id timePeriodId, 
                                                                                                      org.osid.type.Type participantGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodAndGenusTypeForResourceOnDate(resourceId, timePeriodId, participantGenusType, from, to));
    }


    /**
     *  Gets a list of participants corresponding to offering and
     *  resource {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the {@code Id} of the offering
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code ParticipantList}
     *  @throws org.osid.NullArgumentException {@code offeringId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResource(org.osid.id.Id offeringId,
                                                                                   org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsForOfferingAndResource(offeringId, resourceId));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for the given
     *  offering and resource.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code offeringId,
     *          resourceId} or {@code participantGenusType} is {@code
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingAndResource(org.osid.id.Id offeringId, 
                                                                                              org.osid.id.Id resourceId, 
                                                                                              org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByGenusTypeForOfferingAndResource(offeringId, resourceId, participantGenusType));
    }


    /**
     *  Gets a list of participants corresponding to offering and
     *  resource {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are currently
     *  effective. In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ParticipantList}
     *  @throws org.osid.NullArgumentException {@code offeringId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResourceOnDate(org.osid.id.Id offeringId,
                                                                                         org.osid.id.Id resourceId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsForOfferingAndResourceOnDate(offeringId, resourceId, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for an offering,
     *  resource, and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          participantGenusType, from}, or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusTypeForOfferingAndResourceOnDate(org.osid.id.Id offeringId, 
                                                                                                    org.osid.id.Id resourceId, 
                                                                                                    org.osid.type.Type participantGenusType, 
                                                                                                    org.osid.calendaring.DateTime from, 
                                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getParticipantsByGenusTypeForOfferingAndResourceOnDate(offeringId, resourceId, participantGenusType, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} for the given offering and
     *  resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code offeringId,
     *          resourceId}, or {@code timePeriodId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingAndResource(org.osid.id.Id offeringId, 
                                                                                               org.osid.id.Id resourceId, 
                                                                                               org.osid.id.Id timePeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodForOfferingAndResource(offeringId, resourceId, timePeriodId));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for the given
     *  offering and resource in a time period.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @return the returned {@code ParticipantList} 
     *  @throws org.osid.NullArgumentException {@code offeringId,
     *          resourceId}, {@code timePeriodId} or {@code
     *          participantGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingAndResource(org.osid.id.Id offeringId, 
                                                                                                           org.osid.id.Id resourceId, 
                                                                                                           org.osid.id.Id timePeriodId, 
                                                                                                           org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodAndGenusTypeForOfferingAndResource(offeringId, resourceId, timePeriodId, participantGenusType));
    }


    /**
     *  Gets a {@code ParticipantList} for an offering and resource in
     *  a time period and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are currently 
     *  effective. In any effective mode, effective participants and those 
     *  currently expired are returned. 
     *
     *  @param offeringId an offering {@code Id}
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code offeringId,
     *          resourceId, timePeriodId, from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodForOfferingAndResourceOnDate(org.osid.id.Id offeringId, 
                                                                                                     org.osid.id.Id resourceId, 
                                                                                                     org.osid.id.Id timePeriodId, 
                                                                                                     org.osid.calendaring.DateTime from, 
                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipantsByTimePeriodForOfferingAndResourceOnDate(offeringId, resourceId, timePeriodId, from, to));
    }


    /**
     *  Gets a {@code ParticipantList} by genus type for an offering
     *  and resource in a time period and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are
     *  currently effective. In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId an offering {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  timePeriodId a time period {@code Id} 
     *  @param  participantGenusType a participant genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of participants 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code offeringId,
     *          resourceId, timePeriodId,participantGenusType, from},
     *          or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByTimePeriodAndGenusTypeForOfferingAndResourceOnDate(org.osid.id.Id offeringId, 
                                                                                                                 org.osid.id.Id resourceId, 
                                                                                                                 org.osid.id.Id timePeriodId, 
                                                                                                                 org.osid.type.Type participantGenusType, 
                                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getParticipantsByTimePeriodAndGenusTypeForOfferingAndResourceOnDate(offeringId, resourceId, timePeriodId, participantGenusType, from, to));
    }


    /**
     *  Gets all {@code Participants}. 
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Participants} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipants()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParticipants());
    }
}
