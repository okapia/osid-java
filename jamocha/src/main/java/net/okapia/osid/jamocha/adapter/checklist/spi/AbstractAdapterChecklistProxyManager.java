//
// AbstractChecklistProxyManager.java
//
//     An adapter for a ChecklistProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ChecklistProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterChecklistProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.checklist.ChecklistProxyManager>
    implements org.osid.checklist.ChecklistProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterChecklistProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterChecklistProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterChecklistProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterChecklistProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any checklist federation is exposed. Federation is exposed 
     *  when a specific checklist may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of checklists appears as a single checklist. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a todo lookup service. 
     *
     *  @return <code> true </code> if todo lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoLookup() {
        return (getAdapteeManager().supportsTodoLookup());
    }


    /**
     *  Tests if querying todos is available. 
     *
     *  @return <code> true </code> if todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoQuery() {
        return (getAdapteeManager().supportsTodoQuery());
    }


    /**
     *  Tests if searching for todos is available. 
     *
     *  @return <code> true </code> if todo search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoSearch() {
        return (getAdapteeManager().supportsTodoSearch());
    }


    /**
     *  Tests if managing todos is available. 
     *
     *  @return <code> true </code> if todo admin is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoAdmin() {
        return (getAdapteeManager().supportsTodoAdmin());
    }


    /**
     *  Tests if todo notification is available. 
     *
     *  @return <code> true </code> if todo notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoNotification() {
        return (getAdapteeManager().supportsTodoNotification());
    }


    /**
     *  Tests if todo <code> </code> hierarchy traversal service is supported. 
     *
     *  @return <code> true </code> if todo hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoHierarchy() {
        return (getAdapteeManager().supportsTodoHierarchy());
    }


    /**
     *  Tests if a todo <code> </code> hierarchy design service is supported. 
     *
     *  @return <code> true </code> if todo hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoHierarchyDesign() {
        return (getAdapteeManager().supportsTodoHierarchyDesign());
    }


    /**
     *  Tests if a todo to checklist lookup session is available. 
     *
     *  @return <code> true </code> if todo checklist lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoChecklist() {
        return (getAdapteeManager().supportsTodoChecklist());
    }


    /**
     *  Tests if a todo to checklist assignment session is available. 
     *
     *  @return <code> true </code> if todo checklist assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoChecklistAssignment() {
        return (getAdapteeManager().supportsTodoChecklistAssignment());
    }


    /**
     *  Tests if a todo smart checklisting session is available. 
     *
     *  @return <code> true </code> if todo smart checklisting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoSmartChecklist() {
        return (getAdapteeManager().supportsTodoSmartChecklist());
    }


    /**
     *  Tests for the availability of an checklist lookup service. 
     *
     *  @return <code> true </code> if checklist lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistLookup() {
        return (getAdapteeManager().supportsChecklistLookup());
    }


    /**
     *  Tests if querying checklists is available. 
     *
     *  @return <code> true </code> if checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistQuery() {
        return (getAdapteeManager().supportsChecklistQuery());
    }


    /**
     *  Tests if searching for checklists is available. 
     *
     *  @return <code> true </code> if checklist search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistSearch() {
        return (getAdapteeManager().supportsChecklistSearch());
    }


    /**
     *  Tests for the availability of a checklist administrative service for 
     *  creating and deleting checklists. 
     *
     *  @return <code> true </code> if checklist administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistAdmin() {
        return (getAdapteeManager().supportsChecklistAdmin());
    }


    /**
     *  Tests for the availability of a checklist notification service. 
     *
     *  @return <code> true </code> if checklist notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistNotification() {
        return (getAdapteeManager().supportsChecklistNotification());
    }


    /**
     *  Tests for the availability of a checklist hierarchy traversal service. 
     *
     *  @return <code> true </code> if checklist hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistHierarchy() {
        return (getAdapteeManager().supportsChecklistHierarchy());
    }


    /**
     *  Tests for the availability of a checklist hierarchy design service. 
     *
     *  @return <code> true </code> if checklist hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistHierarchyDesign() {
        return (getAdapteeManager().supportsChecklistHierarchyDesign());
    }


    /**
     *  Tests for the availability of a checklist batch service. 
     *
     *  @return <code> true </code> if checklist batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistBatch() {
        return (getAdapteeManager().supportsChecklistBatch());
    }


    /**
     *  Tests for the availability of a checklist mason service. 
     *
     *  @return <code> true </code> if checklist mason service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistMason() {
        return (getAdapteeManager().supportsChecklistMason());
    }


    /**
     *  Gets the supported <code> Todo </code> record types. 
     *
     *  @return a list containing the supported todo record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoRecordTypes() {
        return (getAdapteeManager().getTodoRecordTypes());
    }


    /**
     *  Tests if the given <code> Todo </code> record type is supported. 
     *
     *  @param  todoRecordType a <code> Type </code> indicating a <code> Todo 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> todoRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoRecordType(org.osid.type.Type todoRecordType) {
        return (getAdapteeManager().supportsTodoRecordType(todoRecordType));
    }


    /**
     *  Gets the supported todo search record types. 
     *
     *  @return a list containing the supported todo search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoSearchRecordTypes() {
        return (getAdapteeManager().getTodoSearchRecordTypes());
    }


    /**
     *  Tests if the given todo search record type is supported. 
     *
     *  @param  todoSearchRecordType a <code> Type </code> indicating a todo 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> todoSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoSearchRecordType(org.osid.type.Type todoSearchRecordType) {
        return (getAdapteeManager().supportsTodoSearchRecordType(todoSearchRecordType));
    }


    /**
     *  Gets the supported <code> Checklist </code> record types. 
     *
     *  @return a list containing the supported checklist record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChecklistRecordTypes() {
        return (getAdapteeManager().getChecklistRecordTypes());
    }


    /**
     *  Tests if the given <code> Checklist </code> record type is supported. 
     *
     *  @param  checklistRecordType a <code> Type </code> indicating a <code> 
     *          Checklist </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> checklistRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChecklistRecordType(org.osid.type.Type checklistRecordType) {
        return (getAdapteeManager().supportsChecklistRecordType(checklistRecordType));
    }


    /**
     *  Gets the supported checklist search record types. 
     *
     *  @return a list containing the supported checklist search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChecklistSearchRecordTypes() {
        return (getAdapteeManager().getChecklistSearchRecordTypes());
    }


    /**
     *  Tests if the given checklist search record type is supported. 
     *
     *  @param  checklistSearchRecordType a <code> Type </code> indicating a 
     *          checklist record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          checklistSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChecklistSearchRecordType(org.osid.type.Type checklistSearchRecordType) {
        return (getAdapteeManager().supportsChecklistSearchRecordType(checklistSearchRecordType));
    }


    /**
     *  Gets the supported priority types. 
     *
     *  @return a list containing the supported priority types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriorityTypes() {
        return (getAdapteeManager().getPriorityTypes());
    }


    /**
     *  Tests if the given priority type is supported. 
     *
     *  @param  priorityType a <code> Type </code> indicating a priority type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriorityType(org.osid.type.Type priorityType) {
        return (getAdapteeManager().supportsPriorityType(priorityType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoLookupSession getTodoLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo lookup 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoLookupSession getTodoLookupSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoLookupSessionForChecklist(checklistId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuerySession getTodoQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo query 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuerySession getTodoQuerySessionForChecklist(org.osid.id.Id checklistId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoQuerySessionForChecklist(checklistId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSearchSession getTodoSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo search 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSearchSession getTodoSearchSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoSearchSessionForChecklist(checklistId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoAdminSession getTodoAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  administration service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoAdminSession getTodoAdminSessionForChecklist(org.osid.id.Id checklistId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoAdminSessionForChecklist(checklistId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  notification service. 
     *
     *  @param  todoReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> TodoNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> todoReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoNotificationSession getTodoNotificationSession(org.osid.checklist.TodoReceiver todoReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoNotificationSession(todoReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  notification service for the given checklist. 
     *
     *  @param  todoReceiver the receiver 
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> todoReceiver, 
     *          checklistId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoNotificationSession getTodoNotificationSessionForChecklist(org.osid.checklist.TodoReceiver todoReceiver, 
                                                                                             org.osid.id.Id checklistId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoNotificationSessionForChecklist(todoReceiver, checklistId, proxy));
    }


    /**
     *  Gets the todo hierarchy traversal session. 
     *
     *  @param  proxy proxy 
     *  @return <code> a TodoHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchySession getTodoHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoHierarchySession(proxy));
    }


    /**
     *  Gets the todo hierarchy traversal session for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy proxy 
     *  @return <code> a TodoHierarchySession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoHierarchy() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchySession getTodoHierarchySessionForChecklist(org.osid.id.Id checklistId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoHierarchySessionForChecklist(checklistId, proxy));
    }


    /**
     *  Gets the todo hierarchy design session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TodoHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchyDesignSession getTodoHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the todo hierarchy design session for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> TodoHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchyDesignSession getTodoHierarchyDesignSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoHierarchyDesignSessionForChecklist(checklistId, proxy));
    }


    /**
     *  Gets the session for retrieving todo to checklist mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoChecklistSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoChecklist() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoChecklistSession getTodoChecklistSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoChecklistSession(proxy));
    }


    /**
     *  Gets the session for assigning todo to checklist mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoChecklistAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoChecklistAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoChecklistAssignmentSession getTodoChecklistAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoChecklistAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic todo checklists for the given 
     *  checklist. 
     *
     *  @param  checklistId the <code> Id </code> of a checklist 
     *  @param  proxy a proxy 
     *  @return <code> checklistId </code> not found 
     *  @throws org.osid.NotFoundException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoSmartChecklist() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSmartChecklistSession getTodoSmartChecklistSession(org.osid.id.Id checklistId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoSmartChecklistSession(checklistId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistLookupSession getChecklistLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistQueryh() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuerySession getChecklistQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistSearchSession getChecklistSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistAdminSession getChecklistAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  notification service. 
     *
     *  @param  checklistReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> checklistReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistNotificationSession getChecklistNotificationSession(org.osid.checklist.ChecklistReceiver checklistReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistNotificationSession(checklistReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistHierarchySession getChecklistHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistHierarchyDesignSession getChecklistHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> ChecklistBatchProxyManager. </code> 
     *
     *  @return a <code> ChecklistBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.batch.ChecklistBatchProxyManager getChecklistBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistBatchProxyManager());
    }


    /**
     *  Gets a <code> ChecklistMasonProxyManager. </code> 
     *
     *  @return a <code> ChecklistMasonProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistMason() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.ChecklistMasonProxyManager getChecklistMasonProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistMasonProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
