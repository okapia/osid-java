//
// OfficeElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.office.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OfficeElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the OfficeElement Id.
     *
     *  @return the office element Id
     */

    public static org.osid.id.Id getOfficeEntityId() {
        return (makeEntityId("osid.workflow.Office"));
    }


    /**
     *  Gets the ProcessId element Id.
     *
     *  @return the ProcessId element Id
     */

    public static org.osid.id.Id getProcessId() {
        return (makeQueryElementId("osid.workflow.office.ProcessId"));
    }


    /**
     *  Gets the Process element Id.
     *
     *  @return the Process element Id
     */

    public static org.osid.id.Id getProcess() {
        return (makeQueryElementId("osid.workflow.office.Process"));
    }


    /**
     *  Gets the StepId element Id.
     *
     *  @return the StepId element Id
     */

    public static org.osid.id.Id getStepId() {
        return (makeQueryElementId("osid.workflow.office.StepId"));
    }


    /**
     *  Gets the Step element Id.
     *
     *  @return the Step element Id
     */

    public static org.osid.id.Id getStep() {
        return (makeQueryElementId("osid.workflow.office.Step"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeQueryElementId("osid.workflow.office.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeQueryElementId("osid.workflow.office.Work"));
    }


    /**
     *  Gets the AncestorOfficeId element Id.
     *
     *  @return the AncestorOfficeId element Id
     */

    public static org.osid.id.Id getAncestorOfficeId() {
        return (makeQueryElementId("osid.workflow.office.AncestorOfficeId"));
    }


    /**
     *  Gets the AncestorOffice element Id.
     *
     *  @return the AncestorOffice element Id
     */

    public static org.osid.id.Id getAncestorOffice() {
        return (makeQueryElementId("osid.workflow.office.AncestorOffice"));
    }


    /**
     *  Gets the DescendantOfficeId element Id.
     *
     *  @return the DescendantOfficeId element Id
     */

    public static org.osid.id.Id getDescendantOfficeId() {
        return (makeQueryElementId("osid.workflow.office.DescendantOfficeId"));
    }


    /**
     *  Gets the DescendantOffice element Id.
     *
     *  @return the DescendantOffice element Id
     */

    public static org.osid.id.Id getDescendantOffice() {
        return (makeQueryElementId("osid.workflow.office.DescendantOffice"));
    }
}
