//
// AbstractAgencySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAgencySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.authentication.AgencySearchResults {

    private org.osid.authentication.AgencyList agencies;
    private final org.osid.authentication.AgencyQueryInspector inspector;
    private final java.util.Collection<org.osid.authentication.records.AgencySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAgencySearchResults.
     *
     *  @param agencies the result set
     *  @param agencyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>agencies</code>
     *          or <code>agencyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAgencySearchResults(org.osid.authentication.AgencyList agencies,
                                            org.osid.authentication.AgencyQueryInspector agencyQueryInspector) {
        nullarg(agencies, "agencies");
        nullarg(agencyQueryInspector, "agency query inspectpr");

        this.agencies = agencies;
        this.inspector = agencyQueryInspector;

        return;
    }


    /**
     *  Gets the agency list resulting from a search.
     *
     *  @return an agency list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgencies() {
        if (this.agencies == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.authentication.AgencyList agencies = this.agencies;
        this.agencies = null;
	return (agencies);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.authentication.AgencyQueryInspector getAgencyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  agency search record <code> Type. </code> This method must
     *  be used to retrieve an agency implementing the requested
     *  record.
     *
     *  @param agencySearchRecordType an agency search 
     *         record type 
     *  @return the agency search
     *  @throws org.osid.NullArgumentException
     *          <code>agencySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(agencySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgencySearchResultsRecord getAgencySearchResultsRecord(org.osid.type.Type agencySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.authentication.records.AgencySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(agencySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(agencySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record agency search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAgencyRecord(org.osid.authentication.records.AgencySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "agency record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
