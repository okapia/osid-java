//
// MutableMapProxySignalEnablerLookupSession
//
//    Implements a SignalEnabler lookup service backed by a collection of
//    signalEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules;


/**
 *  Implements a SignalEnabler lookup service backed by a collection of
 *  signalEnablers. The signalEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of signal enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxySignalEnablerLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.rules.spi.AbstractMapSignalEnablerLookupSession
    implements org.osid.mapping.path.rules.SignalEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxySignalEnablerLookupSession}
     *  with no signal enablers.
     *
     *  @param map the map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxySignalEnablerLookupSession(org.osid.mapping.Map map,
                                                  org.osid.proxy.Proxy proxy) {
        setMap(map);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxySignalEnablerLookupSession} with a
     *  single signal enabler.
     *
     *  @param map the map
     *  @param signalEnabler a signal enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signalEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySignalEnablerLookupSession(org.osid.mapping.Map map,
                                                org.osid.mapping.path.rules.SignalEnabler signalEnabler, org.osid.proxy.Proxy proxy) {
        this(map, proxy);
        putSignalEnabler(signalEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySignalEnablerLookupSession} using an
     *  array of signal enablers.
     *
     *  @param map the map
     *  @param signalEnablers an array of signal enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signalEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySignalEnablerLookupSession(org.osid.mapping.Map map,
                                                org.osid.mapping.path.rules.SignalEnabler[] signalEnablers, org.osid.proxy.Proxy proxy) {
        this(map, proxy);
        putSignalEnablers(signalEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySignalEnablerLookupSession} using a
     *  collection of signal enablers.
     *
     *  @param map the map
     *  @param signalEnablers a collection of signal enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signalEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySignalEnablerLookupSession(org.osid.mapping.Map map,
                                                java.util.Collection<? extends org.osid.mapping.path.rules.SignalEnabler> signalEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(map, proxy);
        setSessionProxy(proxy);
        putSignalEnablers(signalEnablers);
        return;
    }

    
    /**
     *  Makes a {@code SignalEnabler} available in this session.
     *
     *  @param signalEnabler an signal enabler
     *  @throws org.osid.NullArgumentException {@code signalEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putSignalEnabler(org.osid.mapping.path.rules.SignalEnabler signalEnabler) {
        super.putSignalEnabler(signalEnabler);
        return;
    }


    /**
     *  Makes an array of signalEnablers available in this session.
     *
     *  @param signalEnablers an array of signal enablers
     *  @throws org.osid.NullArgumentException {@code signalEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putSignalEnablers(org.osid.mapping.path.rules.SignalEnabler[] signalEnablers) {
        super.putSignalEnablers(signalEnablers);
        return;
    }


    /**
     *  Makes collection of signal enablers available in this session.
     *
     *  @param signalEnablers
     *  @throws org.osid.NullArgumentException {@code signalEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putSignalEnablers(java.util.Collection<? extends org.osid.mapping.path.rules.SignalEnabler> signalEnablers) {
        super.putSignalEnablers(signalEnablers);
        return;
    }


    /**
     *  Removes a SignalEnabler from this session.
     *
     *  @param signalEnablerId the {@code Id} of the signal enabler
     *  @throws org.osid.NullArgumentException {@code signalEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSignalEnabler(org.osid.id.Id signalEnablerId) {
        super.removeSignalEnabler(signalEnablerId);
        return;
    }    
}
