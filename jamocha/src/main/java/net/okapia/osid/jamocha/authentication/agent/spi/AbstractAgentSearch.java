//
// AbstractAgentSearch.java
//
//     A template for making an Agent Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing agent searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAgentSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.authentication.AgentSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.authentication.records.AgentSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.authentication.AgentSearchOrder agentSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of agents. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  agentIds list of agents
     *  @throws org.osid.NullArgumentException
     *          <code>agentIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAgents(org.osid.id.IdList agentIds) {
        while (agentIds.hasNext()) {
            try {
                this.ids.add(agentIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAgents</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of agent Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAgentIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  agentSearchOrder agent search order 
     *  @throws org.osid.NullArgumentException
     *          <code>agentSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>agentSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAgentResults(org.osid.authentication.AgentSearchOrder agentSearchOrder) {
	this.agentSearchOrder = agentSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
	return (this.agentSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given agent search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an agent implementing the requested record.
     *
     *  @param agentSearchRecordType an agent search record
     *         type
     *  @return the agent search record
     *  @throws org.osid.NullArgumentException
     *          <code>agentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agentSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgentSearchRecord getAgentSearchRecord(org.osid.type.Type agentSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.authentication.records.AgentSearchRecord record : this.records) {
            if (record.implementsRecordType(agentSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agent search. 
     *
     *  @param agentSearchRecord agent search record
     *  @param agentSearchRecordType agent search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgentSearchRecord(org.osid.authentication.records.AgentSearchRecord agentSearchRecord, 
                                           org.osid.type.Type agentSearchRecordType) {

        addRecordType(agentSearchRecordType);
        this.records.add(agentSearchRecord);        
        return;
    }
}
