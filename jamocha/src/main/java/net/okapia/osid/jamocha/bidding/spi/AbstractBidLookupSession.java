//
// AbstractBidLookupSession.java
//
//    A starter implementation framework for providing a Bid
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Bid
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBids(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBidLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.BidLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();
    

    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }

    /**
     *  Tests if this user can perform <code>Bid</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBids() {
        return (true);
    }


    /**
     *  A complete view of the <code>Bid</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBidView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Bid</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBidView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include bids in auction houses which are children of
     *  this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only bids whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveBidView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All bids of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveBidView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Bid</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Bid</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Bid</code> and
     *  retained for compatibility.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidId <code>Id</code> of the
     *          <code>Bid</code>
     *  @return the bid
     *  @throws org.osid.NotFoundException <code>bidId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>bidId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Bid getBid(org.osid.id.Id bidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.bidding.BidList bids = getBids()) {
            while (bids.hasNext()) {
                org.osid.bidding.Bid bid = bids.getNextBid();
                if (bid.getId().equals(bidId)) {
                    return (bid);
                }
            }
        } 

        throw new org.osid.NotFoundException(bidId + " not found");
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  bids specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Bids</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, bids are returned that are currently effective.
     *  In any effective mode, effective bids and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBids()</code>.
     *
     *  @param  bidIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>bidIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByIds(org.osid.id.IdList bidIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.bidding.Bid> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = bidIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBid(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("bid " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.bidding.bid.LinkedBidList(ret));
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  bid genus <code>Type</code> which does not include
     *  bids of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently effective.
     *  In any effective mode, effective bids and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBids()</code>.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.BidGenusFilterList(getBids(), bidGenusType));
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  bid genus <code>Type</code> and include any additional
     *  bids with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBids()</code>.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByParentGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBidsByGenusType(bidGenusType));
    }


    /**
     *  Gets a <code>BidList</code> containing the given
     *  bid record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, [hobjects are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBids()</code>.
     *
     *  @param  bidRecordType a bid record type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByRecordType(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.BidRecordFilterList(getBids(), bidRecordType));
    }


    /**
     *  Gets a <code> BidList </code> effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known bids or an error
     *  results. Otherwise, the returned list may contain only those bids that
     *  are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently effective. In
     *  any effective mode, effective bids and those currently expired are
     *  returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsOnDate(org.osid.calendaring.DateTime from,
                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.TemporalBidFilterList(getBids(), from, to));
    }


    /**
     *  Gets a list of bids corresponding to an auction
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auction</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.bidding.BidList getBidsForAuction(org.osid.id.Id auctionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.BidFilterList(new AuctionFilter(auctionId), getBids()));
    }


    /**
     *  Gets a list of bids corresponding to an auction
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auction</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionOnDate(org.osid.id.Id auctionId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.TemporalBidFilterList(getBidsForAuction(auctionId), from, to));
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.bidding.BidList getBidsForBidder(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.BidFilterList(new BidderFilter(resourceId), getBids()));
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForBidderOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.TemporalBidFilterList(getBidsForBidder(resourceId), from, to));
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auction</code>,
     *          <code>resource</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidder(org.osid.id.Id auctionId,
                                                               org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.BidFilterList(new BidderFilter(resourceId), getBidsForAuction(auctionId)));
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the bidder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidderOnDate(org.osid.id.Id auctionId,
                                                                     org.osid.id.Id resourceId,
                                                                     org.osid.calendaring.DateTime from,
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.TemporalBidFilterList(getBidsForAuctionAndBidder(auctionId, resourceId), from, to));
    }


    /**
     *  Gets all winning <code>Bids</code>.
     *
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session. In
     *  both cases, the order of the set is by start effective date.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @return a list of <code> Bids </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.WinningBidFilterList(getBids()));
    }


    /**
     *  Gets a list of all winning bids effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known bids or an error
     *  results. Otherwise, the returned list may contain only those bids that
     *  are accessible through this session. In both cases, the order of the
     *  set is by the start of the effective date.
     *
     *  In effective mode, bids are returned that are currently effective. In
     *  any effective mode, effective bids and those currently expired are
     *  returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsOnDate(org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.TemporalBidFilterList(getWinningBids(), from, to));
    }


    /**
     *  Gets a list of all winning bids for an auction.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  auctionId an auction <code> Id </code>
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.BidFilterList(new AuctionFilter(auctionId), getWinningBids()));        
    }


    /**
     *  Gets a list of winning bids for an auction and effectiveduring the
     *  entire given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known bids or an error
     *  results. Otherwise, the returned list may contain only those bids that
     *  are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently effective. In
     *  any effective mode, effective bids and those currently expired are
     *  returned.
     *
     *  @param  auctionId an auction <code> Id </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> auctionId, from, </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuctionOnDate(org.osid.id.Id auctionId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.TemporalBidFilterList(getWinningBidsForAuction(auctionId), from, to));
    }


    /**
     *  Gets a list of all winning bids for a bidder.
     *
     *  In plenary mode, the returned list contains all known auctions or an
     *  error results. Otherwise, the returned list may contain only those
     *  auctions that are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently effective. In
     *  any effective mode, effective bids and those currently expired are
     *  returned.
     *
     *  @param  resourceId a resourceId <code> Id </code>
     *  @return the returned <code> Bid </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.BidFilterList(new BidderFilter(resourceId), getWinningBidsForBidder(resourceId)));        
    }


    /**
     *  Gets a list of winning bids for a bidder and effective during the
     *  entire given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known bids or an error
     *  results. Otherwise, the returned list may contain only those bids that
     *  are accessible through this session.
     *
     *  In effective mode, bids are returned that are currently effective. In
     *  any effective mode, effective bids and those currently expired are
     *  returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned C <code> ommission </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidderOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.bid.TemporalBidFilterList(getWinningBidsForBidder(resourceId), from, to));
    }


    /**
     *  Gets all <code>Bids</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Bids</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.bidding.BidList getBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the bid list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of bids
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.bidding.BidList filterBidsOnViews(org.osid.bidding.BidList list)
        throws org.osid.OperationFailedException {
            
        org.osid.bidding.BidList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.bidding.bid.EffectiveBidFilterList(ret);
        }

        return (ret);
    }

       
    public static class AuctionFilter
        implements net.okapia.osid.jamocha.inline.filter.bidding.bid.BidFilter {
        
        private final org.osid.id.Id auctionId;
        
        
        /**
         *  Constructs a new <code>AuctionFilter</code>.
         *
         *  @param auctionId the auction to filter
         *  @throws org.osid.NullArgumentException
         *          <code>auctionId</code> is <code>null</code>
         */
        
        public AuctionFilter(org.osid.id.Id auctionId) {
            nullarg(auctionId, "auction Id");
            this.auctionId = auctionId;
            return;
        }
        
        
        /**
         *  Used by the BidFilterList to filter the 
         *  bid list based on auction.
         *
         *  @param bid the bid
         *  @return <code>true</code> to pass the bid,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.bidding.Bid bid) {
            return (bid.getAuctionId().equals(this.auctionId));
        }
    }
    
    
    public static class BidderFilter
        implements net.okapia.osid.jamocha.inline.filter.bidding.bid.BidFilter {
        
        private final org.osid.id.Id resourceId;
        
        
        /**
         *  Constructs a new <code>BidderFilter</code>.
         *
         *  @param resourceId the bidder to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public BidderFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "bidder Id");
            this.resourceId = resourceId;
            return;
        }
        
        
        /**
         *  Used by the BidFilterList to filter the 
         *  bid list based on bidder.
         *
         *  @param bid the bid
         *  @return <code>true</code> to pass the bid,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.bidding.Bid bid) {
            return (bid.getBidderId().equals(this.resourceId));
        }
    }
}
