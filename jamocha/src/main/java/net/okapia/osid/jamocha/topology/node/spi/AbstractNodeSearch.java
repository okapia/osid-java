//
// AbstractNodeSearch.java
//
//     A template for making a Node Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.node.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing node searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractNodeSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.topology.NodeSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.topology.records.NodeSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.topology.NodeSearchOrder nodeSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of nodes. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  nodeIds list of nodes
     *  @throws org.osid.NullArgumentException
     *          <code>nodeIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongNodes(org.osid.id.IdList nodeIds) {
        while (nodeIds.hasNext()) {
            try {
                this.ids.add(nodeIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongNodes</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of node Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getNodeIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  nodeSearchOrder node search order 
     *  @throws org.osid.NullArgumentException
     *          <code>nodeSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>nodeSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderNodeResults(org.osid.topology.NodeSearchOrder nodeSearchOrder) {
	this.nodeSearchOrder = nodeSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.topology.NodeSearchOrder getNodeSearchOrder() {
	return (this.nodeSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given node search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a node implementing the requested record.
     *
     *  @param nodeSearchRecordType a node search record
     *         type
     *  @return the node search record
     *  @throws org.osid.NullArgumentException
     *          <code>nodeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(nodeSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.NodeSearchRecord getNodeSearchRecord(org.osid.type.Type nodeSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.topology.records.NodeSearchRecord record : this.records) {
            if (record.implementsRecordType(nodeSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(nodeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this node search. 
     *
     *  @param nodeSearchRecord node search record
     *  @param nodeSearchRecordType node search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addNodeSearchRecord(org.osid.topology.records.NodeSearchRecord nodeSearchRecord, 
                                           org.osid.type.Type nodeSearchRecordType) {

        addRecordType(nodeSearchRecordType);
        this.records.add(nodeSearchRecord);        
        return;
    }
}
