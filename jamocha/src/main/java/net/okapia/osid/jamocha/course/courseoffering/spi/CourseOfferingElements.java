//
// CourseOfferingElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.courseoffering.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CourseOfferingElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the CourseOfferingElement Id.
     *
     *  @return the course offering element Id
     */

    public static org.osid.id.Id getCourseOfferingEntityId() {
        return (makeEntityId("osid.course.CourseOffering"));
    }


    /**
     *  Gets the CourseId element Id.
     *
     *  @return the CourseId element Id
     */

    public static org.osid.id.Id getCourseId() {
        return (makeElementId("osid.course.courseoffering.CourseId"));
    }


    /**
     *  Gets the Course element Id.
     *
     *  @return the Course element Id
     */

    public static org.osid.id.Id getCourse() {
        return (makeElementId("osid.course.courseoffering.Course"));
    }


    /**
     *  Gets the TermId element Id.
     *
     *  @return the TermId element Id
     */

    public static org.osid.id.Id getTermId() {
        return (makeElementId("osid.course.courseoffering.TermId"));
    }


    /**
     *  Gets the Term element Id.
     *
     *  @return the Term element Id
     */

    public static org.osid.id.Id getTerm() {
        return (makeElementId("osid.course.courseoffering.Term"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.course.courseoffering.Title"));
    }


    /**
     *  Gets the Number element Id.
     *
     *  @return the Number element Id
     */

    public static org.osid.id.Id getNumber() {
        return (makeElementId("osid.course.courseoffering.Number"));
    }


    /**
     *  Gets the InstructorIds element Id.
     *
     *  @return the InstructorIds element Id
     */

    public static org.osid.id.Id getInstructorIds() {
        return (makeElementId("osid.course.courseoffering.InstructorIds"));
    }


    /**
     *  Gets the Instructors element Id.
     *
     *  @return the Instructors element Id
     */

    public static org.osid.id.Id getInstructors() {
        return (makeElementId("osid.course.courseoffering.Instructors"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.course.courseoffering.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.course.courseoffering.Sponsors"));
    }


    /**
     *  Gets the Credits element Id.
     *
     *  @return the Credits element Id
     */

    public static org.osid.id.Id getCredits() {
        return (makeElementId("osid.course.courseoffering.Credits"));
    }


    /**
     *  Gets the GradingOptionIds element Id.
     *
     *  @return the GradingOptionIds element Id
     */

    public static org.osid.id.Id getGradingOptionIds() {
        return (makeElementId("osid.course.courseoffering.GradingOptionIds"));
    }


    /**
     *  Gets the GradingOptions element Id.
     *
     *  @return the GradingOptions element Id
     */

    public static org.osid.id.Id getGradingOptions() {
        return (makeElementId("osid.course.courseoffering.GradingOptions"));
    }


    /**
     *  Gets the MinimumSeats element Id.
     *
     *  @return the MinimumSeats element Id
     */

    public static org.osid.id.Id getMinimumSeats() {
        return (makeElementId("osid.course.courseoffering.MinimumSeats"));
    }


    /**
     *  Gets the MaximumSeats element Id.
     *
     *  @return the MaximumSeats element Id
     */

    public static org.osid.id.Id getMaximumSeats() {
        return (makeElementId("osid.course.courseoffering.MaximumSeats"));
    }


    /**
     *  Gets the URL element Id.
     *
     *  @return the URL element Id
     */

    public static org.osid.id.Id getURL() {
        return (makeElementId("osid.course.courseoffering.URL"));
    }


    /**
     *  Gets the ScheduleInfo element Id.
     *
     *  @return the ScheduleInfo element Id
     */

    public static org.osid.id.Id getScheduleInfo() {
        return (makeElementId("osid.course.courseoffering.ScheduleInfo"));
    }


    /**
     *  Gets the EventId element Id.
     *
     *  @return the EventId element Id
     */

    public static org.osid.id.Id getEventId() {
        return (makeElementId("osid.course.courseoffering.EventId"));
    }


    /**
     *  Gets the Event element Id.
     *
     *  @return the Event element Id
     */

    public static org.osid.id.Id getEvent() {
        return (makeElementId("osid.course.courseoffering.Event"));
    }


    /**
     *  Gets the RequiresRegistration element Id.
     *
     *  @return the RequiresRegistration element Id
     */

    public static org.osid.id.Id getRequiresRegistration() {
        return (makeElementId("osid.course.courseoffering.RequiresRegistration"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeQueryElementId("osid.course.courseoffering.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeQueryElementId("osid.course.courseoffering.Activity"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.courseoffering.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.courseoffering.CourseCatalog"));
    }
}
