//
// AbstractAuctionProcessor.java
//
//     Defines an AuctionProcessor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.rules.auctionprocessor.spi;


/**
 *  Defines an <code>AuctionProcessor</code> builder.
 */

public abstract class AbstractAuctionProcessorBuilder<T extends AbstractAuctionProcessorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidProcessorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.bidding.rules.auctionprocessor.AuctionProcessorMiter auctionProcessor;


    /**
     *  Constructs a new <code>AbstractAuctionProcessorBuilder</code>.
     *
     *  @param auctionProcessor the auction processor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAuctionProcessorBuilder(net.okapia.osid.jamocha.builder.bidding.rules.auctionprocessor.AuctionProcessorMiter auctionProcessor) {
        super(auctionProcessor);
        this.auctionProcessor = auctionProcessor;
        return;
    }


    /**
     *  Builds the auction processor.
     *
     *  @return the new auction processor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.bidding.rules.AuctionProcessor build() {
        (new net.okapia.osid.jamocha.builder.validator.bidding.rules.auctionprocessor.AuctionProcessorValidator(getValidations())).validate(this.auctionProcessor);
        return (new net.okapia.osid.jamocha.builder.bidding.rules.auctionprocessor.ImmutableAuctionProcessor(this.auctionProcessor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the auction processor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.bidding.rules.auctionprocessor.AuctionProcessorMiter getMiter() {
        return (this.auctionProcessor);
    }


    /**
     *  Adds an AuctionProcessor record.
     *
     *  @param record an auction processor record
     *  @param recordType the type of auction processor record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.bidding.rules.records.AuctionProcessorRecord record, org.osid.type.Type recordType) {
        getMiter().addAuctionProcessorRecord(record, recordType);
        return (self());
    }
}       


