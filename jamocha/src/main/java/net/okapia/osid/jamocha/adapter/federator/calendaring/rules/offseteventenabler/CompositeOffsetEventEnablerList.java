//
// CompositeOffsetEventEnablerList.java
//
//     Implements an OffsetEventEnablerList. This list allows 
//     OffsetEventEnablerLists to be added in serial. This is not as 
//     efficient as the ParallelOffsetEventEnablerList but 
//     preserves the ordering.
//
//
// Tom Coppeto
// Okapia
// 17 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.rules.offseteventenabler;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements an <code>OffsetEventEnablerList</code>. This list allows
 *  OffsetEventEnablerLists to be added a bit at a time and are read off in
 *  serial. <code>noMore</code> must be invoked when all the lists
 *  have been added.
 *
 *  Two error handlers are offered. A hard error throws an exception
 *  for any error received by an underlying list. A soft error ignores
 *  errors in underlying lists and proceeds to the next list.
 */

public class CompositeOffsetEventEnablerList
    extends net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.spi.AbstractOffsetEventEnablerList
    implements org.osid.calendaring.rules.OffsetEventEnablerList,
               net.okapia.osid.jamocha.adapter.federator.calendaring.rules.offseteventenabler.FederatingOffsetEventEnablerList {

    private final java.util.List<org.osid.calendaring.rules.OffsetEventEnablerList> lists = java.util.Collections.synchronizedList(new java.util.LinkedList<org.osid.calendaring.rules.OffsetEventEnablerList>());
    private org.osid.calendaring.rules.OffsetEventEnablerList active;

    private boolean soft = false;
    private boolean eol = false;


    /**
     *  Creates a new empty <code>CompositeOffsetEventEnablerList</code>. The
     *  underlying errors fail hard.
     */

    public CompositeOffsetEventEnablerList() {
        return;
    }


    /**
     *  Creates a new empty <code>CompositeOffsetEventEnablerList</code>.
     *
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     */

    public CompositeOffsetEventEnablerList(boolean soft) {
        this.soft = soft;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        primeList();
        if (this.active == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        primeList();
        if (this.active == null) {
            return (0);
        } else {
            return (this.active.available());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *  
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        long skipped = 0;

        while (skipped < n) {
            primeList();
            if (this.active == null) {
                return;
            }
            
            long s = this.active.available();
            if ((s + skipped) > n) {
                s = n - skipped;
            }
                        
            this.active.skip(s);
            skipped += s;
        }

        return;
    }


    /**
     *  Gets the next <code> OffsetEventEnabler </code> in this list.
     *
     *  @return the next <code> OffsetEventEnabler </code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code> OffsetEventEnabler </code> is available
     *          before calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnabler getNextOffsetEventEnabler()
        throws org.osid.OperationFailedException {

        while (true) {
            primeList();
            if (this.active != null) {
                try {
                    return (this.active.getNextOffsetEventEnabler());
                } catch (org.osid.OperationFailedException ofe) {
                    if (!this.soft) {
                        throw ofe;
                    }
                }
            } else {
                throw new org.osid.OperationFailedException("unable to get a next element");
            }
        }
    }

        
    /**
     *  Gets the next set of <code> OffsetEventEnabler </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param n the number of <code> OffsetEventEnabler </code> elements
     *          requested which must be less than or equal to <code>
     *          available() </code>
     *  @return an array of <code> OffsetEventEnabler </code> elements. <code>
     *          </code> The length of the array is less than or equal
     *          to the number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnabler[] getNextOffsetEventEnablers(long n)
        throws org.osid.OperationFailedException {
        
        java.util.List<org.osid.calendaring.rules.OffsetEventEnabler> ret = new java.util.ArrayList<>((int) n);
        long count = 0;

        while (count < n) {
            primeList();
            if (this.active == null) {
                break;
            }

            try {
                for (org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler : this.active.getNextOffsetEventEnablers(this.active.available())) {
                    ret.add(offsetEventEnabler);
                    ++count;
                }
            } catch (org.osid.OperationFailedException ofe) {
                if (!this.soft) {
                    throw ofe;
                }
            }
        }

        return (ret.toArray(new org.osid.calendaring.rules.OffsetEventEnabler[ret.size()]));      
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {

        for (org.osid.calendaring.rules.OffsetEventEnablerList list : this.lists) {
            try {
                list.close();
            } catch (Exception e) {}
        }

        this.active  = null;
        this.eol     = true;
        this.lists.clear();

        return;
    }

        
    /**
     *  Adds an <code>OffsetEventEnablerList</code> to this
     *  <code>OffsetEventEnablerList</code>.
     *
     *  @param offsetEventEnablerList the <code>OffsetEventEnablerList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>offsetEventEnablerList</code>
     *          is <code>null</code>
     */

    @Override
    public void addOffsetEventEnablerList(org.osid.calendaring.rules.OffsetEventEnablerList offsetEventEnablerList) {
        nullarg(offsetEventEnablerList, "offseteventenabler list");

        synchronized (this.lists) {
            this.lists.add(offsetEventEnablerList);
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Adds a collection of <code>OffsetEventEnablerLists</code> to this
     *  <code>OffsetEventEnablerList</code>.
     *
     *  @param offsetEventEnablerLists the <code>OffsetEventEnablerList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerLists</code> is <code>null</code>
     */

    @Override
    public void addOffsetEventEnablerLists(java.util.Collection<org.osid.calendaring.rules.OffsetEventEnablerList> offsetEventEnablerLists) {
        nullarg(offsetEventEnablerLists, "offseteventenabler lists");

        for (org.osid.calendaring.rules.OffsetEventEnablerList list : offsetEventEnablerLists) {
            addOffsetEventEnablerList(list);
        }

        return;
    }


    /**
     *  No more. Invoked when no more lists are to be added.
     */

    @Override
    public void noMore() {
        this.eol = true;
        synchronized (this.lists) {
            this.lists.notifyAll();
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    @Override
    public boolean isOperational() {
        if ((this.lists.size() == 0) && this.eol) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        if (this.lists.size() == 0) {
            return (true);
        }

        return (false);
    }


    private void primeList() {
        while (true) {
            if (this.active == null) {
                if ((this.lists.size() == 0) && (this.eol)) {
                    return;
                }
                
                this.active = this.lists.get(0);
            }
            
            if (this.active.hasNext()) {
                return;
            }
            
            this.lists.remove(0);
            this.active = null;
            
            synchronized (this.lists) {                 
                if (!this.eol && (this.lists.size() == 0)) {
                    try {
                        this.lists.wait();
                    } catch (InterruptedException ie) { }
                }
            }
        }
    }
}
