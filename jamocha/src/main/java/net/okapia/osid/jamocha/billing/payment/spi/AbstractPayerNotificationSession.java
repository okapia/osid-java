//
// AbstractPayerNotificationSession.java
//
//     A template for making PayerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Payer} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Payer} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for payer entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractPayerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.billing.payment.PayerNotificationSession {

    private boolean federated = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();


    /**
     *  Gets the {@code Business/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }

    
    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the {@code Business}.
     *
     *  @param business the business for this session
     *  @throws org.osid.NullArgumentException {@code business}
     *          is {@code null}
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can register for {@code Payer}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForPayerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgePayerNotification() </code>.
     */

    @OSID @Override
    public void reliablePayerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliablePayerNotifications() {
        return;
    }


    /**
     *  Acknowledge a payer notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgePayerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for payers in businesss which
     *  are children of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new payers. {@code
     *  PayerReceiver.newPayer()} is invoked when a new {@code Payer}
     *  is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewPayers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated payers. {@code
     *  PayerReceiver.changedPayer()} is invoked when a payer is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPayers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of new payers for the given
     *  resource.  {@code PayerReceiver.newPayer()} is invoked when a
     *  new {@code Payer} appears in this business.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewPayersForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated payers for the given
     *  resource.  {@code PayerReceiver.changedPayer()} is invoked
     *  when the specified payer in this business is changed.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedPayersForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated payer. {@code
     *  PayerReceiver.changedPayer()} is invoked when the specified
     *  payer is changed.
     *
     *  @param payerId the {@code Id} of the {@code Payer} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code payerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPayer(org.osid.id.Id payerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted payers by the given
     *  resource.  {@code PayerReceiver.deletedPayer()} is invoked
     *  when the specified payer is deleted or removed from this
     *  business.
     *
     *  @param resourceId the {@code Id} of the {@code Resource} to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedPayersForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted payers. {@code
     *  PayerReceiver.deletedPayer()} is invoked when a payer is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPayers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted payer. {@code
     *  PayerReceiver.deletedPayer()} is invoked when the specified
     *  payer is deleted.
     *
     *  @param payerId the {@code Id} of the
     *          {@code Payer} to monitor
     *  @throws org.osid.NullArgumentException {@code payerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPayer(org.osid.id.Id payerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
