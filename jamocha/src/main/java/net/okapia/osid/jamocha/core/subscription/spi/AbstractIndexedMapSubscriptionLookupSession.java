//
// AbstractIndexedMapSubscriptionLookupSession.java
//
//    A simple framework for providing a Subscription lookup service
//    backed by a fixed collection of subscriptions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Subscription lookup service backed by a
 *  fixed collection of subscriptions. The subscriptions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some subscriptions may be compatible
 *  with more types than are indicated through these subscription
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Subscriptions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSubscriptionLookupSession
    extends AbstractMapSubscriptionLookupSession
    implements org.osid.subscription.SubscriptionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.subscription.Subscription> subscriptionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.Subscription>());
    private final MultiMap<org.osid.type.Type, org.osid.subscription.Subscription> subscriptionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.Subscription>());


    /**
     *  Makes a <code>Subscription</code> available in this session.
     *
     *  @param  subscription a subscription
     *  @throws org.osid.NullArgumentException <code>subscription<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSubscription(org.osid.subscription.Subscription subscription) {
        super.putSubscription(subscription);

        this.subscriptionsByGenus.put(subscription.getGenusType(), subscription);
        
        try (org.osid.type.TypeList types = subscription.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.subscriptionsByRecord.put(types.getNextType(), subscription);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a subscription from this session.
     *
     *  @param subscriptionId the <code>Id</code> of the subscription
     *  @throws org.osid.NullArgumentException <code>subscriptionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSubscription(org.osid.id.Id subscriptionId) {
        org.osid.subscription.Subscription subscription;
        try {
            subscription = getSubscription(subscriptionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.subscriptionsByGenus.remove(subscription.getGenusType());

        try (org.osid.type.TypeList types = subscription.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.subscriptionsByRecord.remove(types.getNextType(), subscription);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSubscription(subscriptionId);
        return;
    }


    /**
     *  Gets a <code>SubscriptionList</code> corresponding to the given
     *  subscription genus <code>Type</code> which does not include
     *  subscriptions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known subscriptions or an error results. Otherwise,
     *  the returned list may contain only those subscriptions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned <code>Subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.subscription.ArraySubscriptionList(this.subscriptionsByGenus.get(subscriptionGenusType)));
    }


    /**
     *  Gets a <code>SubscriptionList</code> containing the given
     *  subscription record <code>Type</code>. In plenary mode, the
     *  returned list contains all known subscriptions or an error
     *  results. Otherwise, the returned list may contain only those
     *  subscriptions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  subscriptionRecordType a subscription record type 
     *  @return the returned <code>subscription</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByRecordType(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.subscription.ArraySubscriptionList(this.subscriptionsByRecord.get(subscriptionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.subscriptionsByGenus.clear();
        this.subscriptionsByRecord.clear();

        super.close();

        return;
    }
}
