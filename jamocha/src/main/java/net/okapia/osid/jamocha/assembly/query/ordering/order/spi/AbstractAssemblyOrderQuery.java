//
// AbstractAssemblyOrderQuery.java
//
//     An OrderQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OrderQuery that stores terms.
 */

public abstract class AbstractAssemblyOrderQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.ordering.OrderQuery,
               org.osid.ordering.OrderQueryInspector,
               org.osid.ordering.OrderSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.OrderQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.OrderQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.OrderSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOrderQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOrderQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the item <code> Id </code> for this query to match orders by 
     *  customers. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        getAssembler().addIdTerm(getCustomerIdColumn(), customerId, match);
        return;
    }


    /**
     *  Clears the customer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        getAssembler().clearTerms(getCustomerIdColumn());
        return;
    }


    /**
     *  Gets the customer <code> Id </code> terms. 
     *
     *  @return the customer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (getAssembler().getIdTerms(getCustomerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCustomerColumn(), style);
        return;
    }


    /**
     *  Gets the CustomerId column name.
     *
     * @return the column name
     */

    protected String getCustomerIdColumn() {
        return ("customer_id");
    }


    /**
     *  Tests if a <code> CustomerQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Clears the customer terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        getAssembler().clearTerms(getCustomerColumn());
        return;
    }


    /**
     *  Gets the customer terms. 
     *
     *  @return the customer terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCustomerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a customer search order is available. 
     *
     *  @return <code> true </code> if a customer order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the customer search order. 
     *
     *  @return the customer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Gets the Customer column name.
     *
     * @return the column name
     */

    protected String getCustomerColumn() {
        return ("customer");
    }


    /**
     *  Sets the item <code> Id </code> for this query to match orders 
     *  assigned to items. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches orders with any item. 
     *
     *  @param  match <code> true </code> for a to matc h odrers with any 
     *          items, <code> false </code> to match orders with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.ordering.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.ordering.ItemQueryInspector[0]);
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Matches total costs between the given range inclusive. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchTotalCost(org.osid.financials.Currency low, 
                               org.osid.financials.Currency high, 
                               boolean match) {
        getAssembler().addCurrencyRangeTerm(getTotalCostColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the total cost terms. 
     */

    @OSID @Override
    public void clearTotalCostTerms() {
        getAssembler().clearTerms(getTotalCostColumn());
        return;
    }


    /**
     *  Gets the total cost terms. 
     *
     *  @return the total cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getTotalCostTerms() {
        return (getAssembler().getCurrencyRangeTerms(getTotalCostColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the total cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalCostColumn(), style);
        return;
    }


    /**
     *  Gets the TotalCost column name.
     *
     * @return the column name
     */

    protected String getTotalCostColumn() {
        return ("total_cost");
    }


    /**
     *  Matches total costs greter than or equal to the given cost. 
     *
     *  @param  cost a cost 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumTotalCost(org.osid.financials.Currency cost, 
                                      boolean match) {
        getAssembler().addCurrencyTerm(getMinimumTotalCostColumn(), cost, match);
        return;
    }


    /**
     *  Clears the minimum total cost terms. 
     */

    @OSID @Override
    public void clearMinimumTotalCostTerms() {
        getAssembler().clearTerms(getMinimumTotalCostColumn());
        return;
    }


    /**
     *  Gets the minimum total cost terms. 
     *
     *  @return the minimum total cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getMinimumTotalCostTerms() {
        return (getAssembler().getCurrencyTerms(getMinimumTotalCostColumn()));
    }


    /**
     *  Gets the MinimumTotalCost column name.
     *
     * @return the column name
     */

    protected String getMinimumTotalCostColumn() {
        return ("minimum_total_cost");
    }


    /**
     *  Matches atomic orders. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAtomic(boolean match) {
        getAssembler().addBooleanTerm(getAtomicColumn(), match);
        return;
    }


    /**
     *  Clears the atomic terms. 
     */

    @OSID @Override
    public void clearAtomicTerms() {
        getAssembler().clearTerms(getAtomicColumn());
        return;
    }


    /**
     *  Gets the atomic terms. 
     *
     *  @return the atomic terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAtomicTerms() {
        return (getAssembler().getBooleanTerms(getAtomicColumn()));
    }


    /**
     *  Gets the Atomic column name.
     *
     * @return the column name
     */

    protected String getAtomicColumn() {
        return ("atomic");
    }


    /**
     *  Matches orders submitted between the given time range inclusive. 
     *
     *  @param  from starting time range 
     *  @param  to ending time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSubmitDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getSubmitDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the submit date terms. 
     */

    @OSID @Override
    public void clearSubmitDateTerms() {
        getAssembler().clearTerms(getSubmitDateColumn());
        return;
    }


    /**
     *  Gets the submit date terms. 
     *
     *  @return the date range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getSubmitDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getSubmitDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the submitted 
     *  date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmitDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubmitDateColumn(), style);
        return;
    }


    /**
     *  Gets the SubmitDate column name.
     *
     * @return the column name
     */

    protected String getSubmitDateColumn() {
        return ("submit_date");
    }


    /**
     *  Sets a submitting resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmitterId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSubmitterIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the submitter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubmitterIdTerms() {
        getAssembler().clearTerms(getSubmitterIdColumn());
        return;
    }


    /**
     *  Gets the submitter resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubmitterIdTerms() {
        return (getAssembler().getIdTerms(getSubmitterIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the submitter. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmitter(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubmitterColumn(), style);
        return;
    }


    /**
     *  Gets the SubmitterId column name.
     *
     * @return the column name
     */

    protected String getSubmitterIdColumn() {
        return ("submitter_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmitterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitting agent query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmitterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSubmitterQuery() {
        throw new org.osid.UnimplementedException("supportsSubmitterQuery() is false");
    }


    /**
     *  Matches any submitted order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySubmitter(boolean match) {
        getAssembler().addIdWildcardTerm(getSubmitterColumn(), match);
        return;
    }


    /**
     *  Clears the submitter terms. 
     */

    @OSID @Override
    public void clearSubmitterTerms() {
        getAssembler().clearTerms(getSubmitterColumn());
        return;
    }


    /**
     *  Gets the submitter terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSubmitterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a submitter search order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmitterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the submitter search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmitterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSubmitterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubmitterSearchOrder() is false");
    }


    /**
     *  Gets the Submitter column name.
     *
     * @return the column name
     */

    protected String getSubmitterColumn() {
        return ("submitter");
    }


    /**
     *  Sets a submitting agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmittingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getSubmittingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the submitter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentIdTerms() {
        getAssembler().clearTerms(getSubmittingAgentIdColumn());
        return;
    }


    /**
     *  Gets the submitting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubmittingAgentIdTerms() {
        return (getAssembler().getIdTerms(getSubmittingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the submitting 
     *  agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmittingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubmittingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the SubmittingAgentId column name.
     *
     * @return the column name
     */

    protected String getSubmittingAgentIdColumn() {
        return ("submitting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmittingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitting agent query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmittingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getSubmittingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsSubmittingAgentQuery() is false");
    }


    /**
     *  Matches any submitted order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySubmittingAgent(boolean match) {
        getAssembler().addIdWildcardTerm(getSubmittingAgentColumn(), match);
        return;
    }


    /**
     *  Clears the submitter terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentTerms() {
        getAssembler().clearTerms(getSubmittingAgentColumn());
        return;
    }


    /**
     *  Gets the submitting agent terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getSubmittingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if a submitting agent search order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmittingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the submitting agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmittingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getSubmittingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubmittingAgentSearchOrder() is false");
    }


    /**
     *  Gets the SubmittingAgent column name.
     *
     * @return the column name
     */

    protected String getSubmittingAgentColumn() {
        return ("submitting_agent");
    }


    /**
     *  Matches orders closed between the given time range inclusive. 
     *
     *  @param  from starting time range 
     *  @param  to ending time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchClosedDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getClosedDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the closed date terms. 
     */

    @OSID @Override
    public void clearClosedDateTerms() {
        getAssembler().clearTerms(getClosedDateColumn());
        return;
    }


    /**
     *  Gets the closed date terms. 
     *
     *  @return the date range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClosedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getClosedDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the closed date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClosedDateColumn(), style);
        return;
    }


    /**
     *  Gets the ClosedDate column name.
     *
     * @return the column name
     */

    protected String getClosedDateColumn() {
        return ("closed_date");
    }


    /**
     *  Sets a closer resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCloserId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getCloserIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the closer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCloserIdTerms() {
        getAssembler().clearTerms(getCloserIdColumn());
        return;
    }


    /**
     *  Gets the closer resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCloserIdTerms() {
        return (getAssembler().getIdTerms(getCloserIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCloser(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCloserColumn(), style);
        return;
    }


    /**
     *  Gets the CloserId column name.
     *
     * @return the column name
     */

    protected String getCloserIdColumn() {
        return ("closer_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserQuery() {
        return (false);
    }


    /**
     *  Gets the query for a closer resource query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCloserQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCloserQuery() {
        throw new org.osid.UnimplementedException("supportsCloserQuery() is false");
    }


    /**
     *  Matches any closed order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyCloser(boolean match) {
        getAssembler().addIdWildcardTerm(getCloserColumn(), match);
        return;
    }


    /**
     *  Clears the closer terms. 
     */

    @OSID @Override
    public void clearCloserTerms() {
        getAssembler().clearTerms(getCloserColumn());
        return;
    }


    /**
     *  Gets the closer terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCloserTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a closer search order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserSearchOrder() {
        return (false);
    }


    /**
     *  Gets the closer search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCloserSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getCloserSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCloserSearchOrder() is false");
    }


    /**
     *  Gets the Closer column name.
     *
     * @return the column name
     */

    protected String getCloserColumn() {
        return ("closer");
    }


    /**
     *  Sets a closing agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchClosingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getClosingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the closer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearClosingAgentIdTerms() {
        getAssembler().clearTerms(getClosingAgentIdColumn());
        return;
    }


    /**
     *  Gets the submitting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getClosingAgentIdTerms() {
        return (getAssembler().getIdTerms(getClosingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClosingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the ClosingAgentId column name.
     *
     * @return the column name
     */

    protected String getClosingAgentIdColumn() {
        return ("closing_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a closing agent query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsClosingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getClosingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsClosingAgentQuery() is false");
    }


    /**
     *  Matches any closed order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyClosingAgent(boolean match) {
        getAssembler().addIdWildcardTerm(getClosingAgentColumn(), match);
        return;
    }


    /**
     *  Clears the closer terms. 
     */

    @OSID @Override
    public void clearClosingAgentTerms() {
        getAssembler().clearTerms(getClosingAgentColumn());
        return;
    }


    /**
     *  Gets the closing agent terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getClosingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if a closing agent search order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the closing agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsClosingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getClosingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsClosingAgentSearchOrder() is false");
    }


    /**
     *  Gets the ClosingAgent column name.
     *
     * @return the column name
     */

    protected String getClosingAgentColumn() {
        return ("closing_agent");
    }


    /**
     *  Sets the item <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        getAssembler().clearTerms(getStoreIdColumn());
        return;
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (getAssembler().getIdTerms(getStoreIdColumn()));
    }


    /**
     *  Gets the StoreId column name.
     *
     * @return the column name
     */

    protected String getStoreIdColumn() {
        return ("store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        getAssembler().clearTerms(getStoreColumn());
        return;
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the Store column name.
     *
     * @return the column name
     */

    protected String getStoreColumn() {
        return ("store");
    }


    /**
     *  Tests if this order supports the given record
     *  <code>Type</code>.
     *
     *  @param  orderRecordType an order record type 
     *  @return <code>true</code> if the orderRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type orderRecordType) {
        for (org.osid.ordering.records.OrderQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(orderRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  orderRecordType the order record type 
     *  @return the order query record 
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(orderRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderQueryRecord getOrderQueryRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.OrderQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(orderRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  orderRecordType the order record type 
     *  @return the order query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(orderRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderQueryInspectorRecord getOrderQueryInspectorRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.OrderQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(orderRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param orderRecordType the order record type
     *  @return the order search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(orderRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderSearchOrderRecord getOrderSearchOrderRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.OrderSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(orderRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this order. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param orderQueryRecord the order query record
     *  @param orderQueryInspectorRecord the order query inspector
     *         record
     *  @param orderSearchOrderRecord the order search order record
     *  @param orderRecordType order record type
     *  @throws org.osid.NullArgumentException
     *          <code>orderQueryRecord</code>,
     *          <code>orderQueryInspectorRecord</code>,
     *          <code>orderSearchOrderRecord</code> or
     *          <code>orderRecordTypeorder</code> is
     *          <code>null</code>
     */
            
    protected void addOrderRecords(org.osid.ordering.records.OrderQueryRecord orderQueryRecord, 
                                      org.osid.ordering.records.OrderQueryInspectorRecord orderQueryInspectorRecord, 
                                      org.osid.ordering.records.OrderSearchOrderRecord orderSearchOrderRecord, 
                                      org.osid.type.Type orderRecordType) {

        addRecordType(orderRecordType);

        nullarg(orderQueryRecord, "order query record");
        nullarg(orderQueryInspectorRecord, "order query inspector record");
        nullarg(orderSearchOrderRecord, "order search odrer record");

        this.queryRecords.add(orderQueryRecord);
        this.queryInspectorRecords.add(orderQueryInspectorRecord);
        this.searchOrderRecords.add(orderSearchOrderRecord);
        
        return;
    }
}
