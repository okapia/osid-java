//
// AbstractMutableCourseEntry.java
//
//     Defines a mutable CourseEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.courseentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>CourseEntry</code>.
 */

public abstract class AbstractMutableCourseEntry
    extends net.okapia.osid.jamocha.course.chronicle.courseentry.spi.AbstractCourseEntry
    implements org.osid.course.chronicle.CourseEntry,
               net.okapia.osid.jamocha.builder.course.chronicle.courseentry.CourseEntryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this course entry. 
     *
     *  @param record course entry record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addCourseEntryRecord(org.osid.course.chronicle.records.CourseEntryRecord record, org.osid.type.Type recordType) {
        super.addCourseEntryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this course entry is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this course entry ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this course entry.
     *
     *  @param displayName the name for this course entry
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this course entry.
     *
     *  @param description the description of this course entry
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this course entry
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    @Override
    public void setStudent(org.osid.resource.Resource student) {
        super.setStudent(student);
        return;
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    @Override
    public void setCourse(org.osid.course.Course course) {
        super.setCourse(course);
        return;
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    @Override
    public void setTerm(org.osid.course.Term term) {
        super.setTerm(term);
        return;
    }

    
    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if the course has been
     *          completed, <code> false </code> otherwise
     */

    @Override
    public void setComplete(boolean complete) {
        super.setComplete(complete);
        return;
    }


    /**
     *  Sets the credit scale.
     *
     *  @param scale a credit scale
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreditScale(org.osid.grading.GradeSystem scale) {
        super.setCreditScale(scale);
        return;
    }


    /**
     *  Sets the credits earned.
     *
     *  @param credits a credits earned
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreditsEarned(java.math.BigDecimal credits) {
        super.setCreditsEarned(credits);
        return;
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    @Override
    public void setGrade(org.osid.grading.Grade grade) {
        super.setGrade(grade);
        return;
    }


    /**
     *  Sets the score scale.
     *
     *  @param scale a score scale
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    @Override
    public void setScoreScale(org.osid.grading.GradeSystem scale) {
        super.setScoreScale(scale);
        return;
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    @Override
    public void setScore(java.math.BigDecimal score) {
        super.setScore(score);
        return;
    }


    /**
     *  Adds a registration.
     *
     *  @param registration a registration
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    @Override
    public void addRegistration(org.osid.course.registration.Registration registration) {
        super.addRegistration(registration);
        return;
    }


    /**
     *  Sets all the registrations.
     *
     *  @param registrations a collection of registrations
     *  @throws org.osid.NullArgumentException
     *          <code>registrations</code> is <code>null</code>
     */

    @Override
    public void setRegistrations(java.util.Collection<org.osid.course.registration.Registration> registrations) {
        super.setRegistrations(registrations);
        return;
    }
}

