//
// AbstractOntologyRulesProxyManager.java
//
//     An adapter for a OntologyRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OntologyRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOntologyRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.ontology.rules.OntologyRulesProxyManager>
    implements org.osid.ontology.rules.OntologyRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterOntologyRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOntologyRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOntologyRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOntologyRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up relevancy enablers is supported. 
     *
     *  @return <code> true </code> if relevancy enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerLookup() {
        return (getAdapteeManager().supportsRelevancyEnablerLookup());
    }


    /**
     *  Tests if querying relevancy enablers is supported. 
     *
     *  @return <code> true </code> if relevancy enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerQuery() {
        return (getAdapteeManager().supportsRelevancyEnablerQuery());
    }


    /**
     *  Tests if searching relevancy enablers is supported. 
     *
     *  @return <code> true </code> if relevancy enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerSearch() {
        return (getAdapteeManager().supportsRelevancyEnablerSearch());
    }


    /**
     *  Tests if a relevancy enabler administrative service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerAdmin() {
        return (getAdapteeManager().supportsRelevancyEnablerAdmin());
    }


    /**
     *  Tests if a relevancy enabler notification service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerNotification() {
        return (getAdapteeManager().supportsRelevancyEnablerNotification());
    }


    /**
     *  Tests if a relevancy enabler ontology lookup service is supported. 
     *
     *  @return <code> true </code> if a relevancy enabler ontology lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerOntology() {
        return (getAdapteeManager().supportsRelevancyEnablerOntology());
    }


    /**
     *  Tests if a relevancy enabler ontology service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler ontology assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerOntologyAssignment() {
        return (getAdapteeManager().supportsRelevancyEnablerOntologyAssignment());
    }


    /**
     *  Tests if a relevancy enabler ontology lookup service is supported. 
     *
     *  @return <code> true </code> if a relevancy enabler ontology service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerSmartOntology() {
        return (getAdapteeManager().supportsRelevancyEnablerSmartOntology());
    }


    /**
     *  Tests if a relevancy enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a enabler relevancy rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerRuleLookup() {
        return (getAdapteeManager().supportsRelevancyEnablerRuleLookup());
    }


    /**
     *  Tests if a relevancy enabler rule application service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerRuleApplication() {
        return (getAdapteeManager().supportsRelevancyEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> RelevancyEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> RelevancyEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancyEnablerRecordTypes() {
        return (getAdapteeManager().getRelevancyEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> RelevancyEnabler </code> record type is 
     *  supported. 
     *
     *  @param  relevancyEnablerRecordType a <code> Type </code> indicating a 
     *          <code> RelevancyEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancyEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerRecordType(org.osid.type.Type relevancyEnablerRecordType) {
        return (getAdapteeManager().supportsRelevancyEnablerRecordType(relevancyEnablerRecordType));
    }


    /**
     *  Gets the supported <code> RelevancyEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> RelevancyEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancyEnablerSearchRecordTypes() {
        return (getAdapteeManager().getRelevancyEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> RelevancyEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  relevancyEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RelevancyEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancyEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerSearchRecordType(org.osid.type.Type relevancyEnablerSearchRecordType) {
        return (getAdapteeManager().supportsRelevancyEnablerSearchRecordType(relevancyEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerLookupSession getRelevancyEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler lookup service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerLookupSession getRelevancyEnablerLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerLookupSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerQuerySession getRelevancyEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler query service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerQuerySession getRelevancyEnablerQuerySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerQuerySessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSearchSession getRelevancyEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enablers earch service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSearchSession getRelevancyEnablerSearchSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerSearchSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerAdminSession getRelevancyEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerAdminSession getRelevancyEnablerAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerAdminSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler notification service. 
     *
     *  @param  relevancyEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relevancyEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerNotificationSession getRelevancyEnablerNotificationSession(org.osid.ontology.rules.RelevancyEnablerReceiver relevancyEnablerReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerNotificationSession(relevancyEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler notification service for the given ontology. 
     *
     *  @param  relevancyEnablerReceiver the notification callback 
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no ontology found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancyEnablerReceiver, ontologyId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerNotificationSession getRelevancyEnablerNotificationSessionForOntology(org.osid.ontology.rules.RelevancyEnablerReceiver relevancyEnablerReceiver, 
                                                                                                                         org.osid.id.Id ontologyId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerNotificationSessionForOntology(relevancyEnablerReceiver, ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relevancy 
     *  enabler/ontology mappings for relevancy enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerOntologySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerOntology() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerOntologySession getRelevancyEnablerOntologySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerOntologySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relevancy enablers to ontologies for relevancy. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerOntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerOntologyAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerOntologyAssignmentSession getRelevancyEnablerOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerOntologyAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage relevancy enabler smart 
     *  ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerSmartOntologySession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSmartOntology() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSmartOntologySession getRelevancyEnablerSmartOntologySession(org.osid.id.Id ontologyId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerSmartOntologySession(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler relevancy mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleLookupSession getRelevancyEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler relevancy mapping lookup service. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleLookupSession getRelevancyEnablerRuleLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerRuleLookupSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleApplicationSession getRelevancyEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler assignment service. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleApplicationSession getRelevancyEnablerRuleApplicationSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyEnablerRuleApplicationSessionForOntology(ontologyId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
