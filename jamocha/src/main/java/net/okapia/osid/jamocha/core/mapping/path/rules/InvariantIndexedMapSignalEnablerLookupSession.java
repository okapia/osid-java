//
// InvariantIndexedMapSignalEnablerLookupSession
//
//    Implements a SignalEnabler lookup service backed by a fixed
//    collection of signalEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules;


/**
 *  Implements a SignalEnabler lookup service backed by a fixed
 *  collection of signal enablers. The signal enablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some signal enablers may be compatible
 *  with more types than are indicated through these signal enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapSignalEnablerLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.rules.spi.AbstractIndexedMapSignalEnablerLookupSession
    implements org.osid.mapping.path.rules.SignalEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapSignalEnablerLookupSession} using an
     *  array of signalEnablers.
     *
     *  @param map the map
     *  @param signalEnablers an array of signal enablers
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signalEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapSignalEnablerLookupSession(org.osid.mapping.Map map,
                                                    org.osid.mapping.path.rules.SignalEnabler[] signalEnablers) {

        setMap(map);
        putSignalEnablers(signalEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapSignalEnablerLookupSession} using a
     *  collection of signal enablers.
     *
     *  @param map the map
     *  @param signalEnablers a collection of signal enablers
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signalEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapSignalEnablerLookupSession(org.osid.mapping.Map map,
                                                    java.util.Collection<? extends org.osid.mapping.path.rules.SignalEnabler> signalEnablers) {

        setMap(map);
        putSignalEnablers(signalEnablers);
        return;
    }
}
