//
// AbstractAssemblyItemQuery.java
//
//     An ItemQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ItemQuery that stores terms.
 */

public abstract class AbstractAssemblyItemQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.billing.ItemQuery,
               org.osid.billing.ItemQueryInspector,
               org.osid.billing.ItemSearchOrder {

    private final java.util.Collection<org.osid.billing.records.ItemQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.ItemQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.ItemSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyItemQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyItemQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the category <code> Id </code> for this query. 
     *
     *  @param  categoryId a category <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> categoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCategoryId(org.osid.id.Id categoryId, boolean match) {
        getAssembler().addIdTerm(getCategoryIdColumn(), categoryId, match);
        return;
    }


    /**
     *  Clears the category <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCategoryIdTerms() {
        getAssembler().clearTerms(getCategoryIdColumn());
        return;
    }


    /**
     *  Gets the category <code> Id </code> query terms. 
     *
     *  @return the category <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCategoryIdTerms() {
        return (getAssembler().getIdTerms(getCategoryIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the category. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCategory(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCategoryColumn(), style);
        return;
    }


    /**
     *  Gets the CategoryId column name.
     *
     * @return the column name
     */

    protected String getCategoryIdColumn() {
        return ("category_id");
    }


    /**
     *  Tests if a <code> CategoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a category query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a category. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a category query 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryQuery getCategoryQuery() {
        throw new org.osid.UnimplementedException("supportsCategoryQuery() is false");
    }


    /**
     *  Clears the category terms. 
     */

    @OSID @Override
    public void clearCategoryTerms() {
        getAssembler().clearTerms(getCategoryColumn());
        return;
    }


    /**
     *  Gets the category query terms. 
     *
     *  @return the category query terms 
     */

    @OSID @Override
    public org.osid.billing.CategoryQueryInspector[] getCategoryTerms() {
        return (new org.osid.billing.CategoryQueryInspector[0]);
    }


    /**
     *  Tests if a category search order is available. 
     *
     *  @return <code> true </code> if a category search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategorySearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the category. 
     *
     *  @return the category search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCategorySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategorySearchOrder getCategorySearchOrder() {
        throw new org.osid.UnimplementedException("supportsCategorySearchOrder() is false");
    }


    /**
     *  Gets the Category column name.
     *
     * @return the column name
     */

    protected String getCategoryColumn() {
        return ("category");
    }


    /**
     *  Sets the account <code> Id </code> for this query to match items that 
     *  have a related financial account. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        getAssembler().addIdTerm(getAccountIdColumn(), accountId, match);
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        getAssembler().clearTerms(getAccountIdColumn());
        return;
    }


    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (getAssembler().getIdTerms(getAccountIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the account. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAccount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAccountColumn(), style);
        return;
    }


    /**
     *  Gets the AccountId column name.
     *
     * @return the column name
     */

    protected String getAccountIdColumn() {
        return ("account_id");
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Matches items that have any account. 
     *
     *  @param  match <code> true </code> to match items with any account, 
     *          <code> false </code> to match items with no account 
     */

    @OSID @Override
    public void matchAnyAccount(boolean match) {
        getAssembler().addIdWildcardTerm(getAccountColumn(), match);
        return;
    }


    /**
     *  Clears the account terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        getAssembler().clearTerms(getAccountColumn());
        return;
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account query terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Tests if an account search order is available. 
     *
     *  @return <code> true </code> if an account search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the account. 
     *
     *  @return the account search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchOrder getAccountSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAccountSearchOrder() is false");
    }


    /**
     *  Gets the Account column name.
     *
     * @return the column name
     */

    protected String getAccountColumn() {
        return ("account");
    }


    /**
     *  Sets the product <code> Id </code> for this query to match items that 
     *  have a related product. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        getAssembler().addIdTerm(getProductIdColumn(), productId, match);
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        getAssembler().clearTerms(getProductIdColumn());
        return;
    }


    /**
     *  Gets the product <code> Id </code> query terms. 
     *
     *  @return the product <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProductIdTerms() {
        return (getAssembler().getIdTerms(getProductIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the product. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProduct(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProductColumn(), style);
        return;
    }


    /**
     *  Gets the ProductId column name.
     *
     * @return the column name
     */

    protected String getProductIdColumn() {
        return ("product_id");
    }


    /**
     *  Tests if a <code> ProductQuery </code> is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Matches items that have any product. 
     *
     *  @param  match <code> true </code> to match items with any product, 
     *          <code> false </code> to match items with no product 
     */

    @OSID @Override
    public void matchAnyProduct(boolean match) {
        getAssembler().addIdWildcardTerm(getProductColumn(), match);
        return;
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        getAssembler().clearTerms(getProductColumn());
        return;
    }


    /**
     *  Gets the product query terms. 
     *
     *  @return the product query terms 
     */

    @OSID @Override
    public org.osid.ordering.ProductQueryInspector[] getProductTerms() {
        return (new org.osid.ordering.ProductQueryInspector[0]);
    }


    /**
     *  Tests if a product search order is available. 
     *
     *  @return <code> true </code> if a product search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the product. 
     *
     *  @return the product search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductSearchOrder getProductSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProductSearchOrder() is false");
    }


    /**
     *  Gets the Product column name.
     *
     * @return the column name
     */

    protected String getProductColumn() {
        return ("product");
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getAmountColumn(), low, high, match);
        return;
    }


    /**
     *  Matches items that have any amount set. 
     *
     *  @param  match <code> true </code> to match items with any amount, 
     *          <code> false </code> to match items with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getAmountColumn(), match);
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        getAssembler().clearTerms(getAmountColumn());
        return;
    }


    /**
     *  Gets the amount terms. 
     *
     *  @return the amount terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getAmountColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAmountColumn(), style);
        return;
    }


    /**
     *  Gets the Amount column name.
     *
     * @return the column name
     */

    protected String getAmountColumn() {
        return ("amount");
    }


    /**
     *  Matches items that have debit amounts. 
     *
     *  @param  match <code> true </code> to match items with a debit amount, 
     *          <code> false </code> to match items with a credit amount 
     */

    @OSID @Override
    public void matchDebit(boolean match) {
        getAssembler().addBooleanTerm(getDebitColumn(), match);
        return;
    }


    /**
     *  Clears the debit terms. 
     */

    @OSID @Override
    public void clearDebitTerms() {
        getAssembler().clearTerms(getDebitColumn());
        return;
    }


    /**
     *  Gets the debit terms. 
     *
     *  @return the debit terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDebitTerms() {
        return (getAssembler().getBooleanTerms(getDebitColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by debit. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDebit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDebitColumn(), style);
        return;
    }


    /**
     *  Gets the Debit column name.
     *
     * @return the column name
     */

    protected String getDebitColumn() {
        return ("debit");
    }


    /**
     *  Matches the recurring interval between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRecurringInterval(org.osid.calendaring.Duration low, 
                                       org.osid.calendaring.Duration high, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getRecurringIntervalColumn(), low, high, match);
        return;
    }


    /**
     *  Matches items that have any recurring interval. 
     *
     *  @param  match <code> true </code> to match items with any recurring 
     *          interval, <code> false </code> to match items with a one-time 
     *          charge 
     */

    @OSID @Override
    public void matchAnyRecurringInterval(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getRecurringIntervalColumn(), match);
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearRecurringIntervalTerms() {
        getAssembler().clearTerms(getRecurringIntervalColumn());
        return;
    }


    /**
     *  Gets the recurring interval terms. 
     *
     *  @return the recurring interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRecurringIntervalTerms() {
        return (getAssembler().getDurationRangeTerms(getRecurringIntervalColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by interval. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecurringInterval(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecurringIntervalColumn(), style);
        return;
    }


    /**
     *  Gets the RecurringInterval column name.
     *
     * @return the column name
     */

    protected String getRecurringIntervalColumn() {
        return ("recurring_interval");
    }


    /**
     *  Sets the entry <code> Id </code> for this query to match items that 
     *  have a related entry. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        getAssembler().addIdTerm(getEntryIdColumn(), entryId, match);
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        getAssembler().clearTerms(getEntryIdColumn());
        return;
    }


    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (getAssembler().getIdTerms(getEntryIdColumn()));
    }


    /**
     *  Gets the EntryId column name.
     *
     * @return the column name
     */

    protected String getEntryIdColumn() {
        return ("entry_id");
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches items that have any entry. 
     *
     *  @param  match <code> true </code> to match items with any entry. 
     *          <code> false </code> to match items with no entry 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getEntryColumn(), match);
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        getAssembler().clearTerms(getEntryColumn());
        return;
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the entry query terms 
     */

    @OSID @Override
    public org.osid.billing.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.billing.EntryQueryInspector[0]);
    }


    /**
     *  Gets the Entry column name.
     *
     * @return the column name
     */

    protected String getEntryColumn() {
        return ("entry");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match customers 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.billing.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.ItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.ItemQueryInspectorRecord getItemQueryInspectorRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.ItemQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param itemRecordType the item record type
     *  @return the item search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.ItemSearchOrderRecord getItemSearchOrderRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.ItemSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this item. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemQueryRecord the item query record
     *  @param itemQueryInspectorRecord the item query inspector
     *         record
     *  @param itemSearchOrderRecord the item search order record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          <code>itemQueryRecord</code>,
     *          <code>itemQueryInspectorRecord</code>,
     *          <code>itemSearchOrderRecord</code> or
     *          <code>itemRecordTypeitem</code> is
     *          <code>null</code>
     */
            
    protected void addItemRecords(org.osid.billing.records.ItemQueryRecord itemQueryRecord, 
                                      org.osid.billing.records.ItemQueryInspectorRecord itemQueryInspectorRecord, 
                                      org.osid.billing.records.ItemSearchOrderRecord itemSearchOrderRecord, 
                                      org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);

        nullarg(itemQueryRecord, "item query record");
        nullarg(itemQueryInspectorRecord, "item query inspector record");
        nullarg(itemSearchOrderRecord, "item search odrer record");

        this.queryRecords.add(itemQueryRecord);
        this.queryInspectorRecords.add(itemQueryInspectorRecord);
        this.searchOrderRecords.add(itemSearchOrderRecord);
        
        return;
    }
}
