//
// AbstractJournalEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journalentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractJournalEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.journaling.JournalEntrySearchResults {

    private org.osid.journaling.JournalEntryList journalEntries;
    private final org.osid.journaling.JournalEntryQueryInspector inspector;
    private final java.util.Collection<org.osid.journaling.records.JournalEntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractJournalEntrySearchResults.
     *
     *  @param journalEntries the result set
     *  @param journalEntryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>journalEntries</code>
     *          or <code>journalEntryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractJournalEntrySearchResults(org.osid.journaling.JournalEntryList journalEntries,
                                            org.osid.journaling.JournalEntryQueryInspector journalEntryQueryInspector) {
        nullarg(journalEntries, "journal entries");
        nullarg(journalEntryQueryInspector, "journal entry query inspectpr");

        this.journalEntries = journalEntries;
        this.inspector = journalEntryQueryInspector;

        return;
    }


    /**
     *  Gets the journal entry list resulting from a search.
     *
     *  @return a journal entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntries() {
        if (this.journalEntries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.journaling.JournalEntryList journalEntries = this.journalEntries;
        this.journalEntries = null;
	return (journalEntries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.journaling.JournalEntryQueryInspector getJournalEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  journal entry search record <code> Type. </code> This method must
     *  be used to retrieve a journalEntry implementing the requested
     *  record.
     *
     *  @param journalEntrySearchRecordType a journalEntry search 
     *         record type 
     *  @return the journal entry search
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(journalEntrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntrySearchResultsRecord getJournalEntrySearchResultsRecord(org.osid.type.Type journalEntrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.journaling.records.JournalEntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(journalEntrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(journalEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record journal entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addJournalEntryRecord(org.osid.journaling.records.JournalEntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "journal entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
