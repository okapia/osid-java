//
// AbstractMapEndpointLookupSession
//
//    A simple framework for providing an Endpoint lookup service
//    backed by a fixed collection of endpoints.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.transport.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Endpoint lookup service backed by a
 *  fixed collection of endpoints. The endpoints are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Endpoints</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEndpointLookupSession
    extends net.okapia.osid.jamocha.transport.spi.AbstractEndpointLookupSession
    implements org.osid.transport.EndpointLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.transport.Endpoint> endpoints = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.transport.Endpoint>());


    /**
     *  Makes an <code>Endpoint</code> available in this session.
     *
     *  @param  endpoint an endpoint
     *  @throws org.osid.NullArgumentException <code>endpoint<code>
     *          is <code>null</code>
     */

    protected void putEndpoint(org.osid.transport.Endpoint endpoint) {
        this.endpoints.put(endpoint.getId(), endpoint);
        return;
    }


    /**
     *  Makes an array of endpoints available in this session.
     *
     *  @param  endpoints an array of endpoints
     *  @throws org.osid.NullArgumentException <code>endpoints<code>
     *          is <code>null</code>
     */

    protected void putEndpoints(org.osid.transport.Endpoint[] endpoints) {
        putEndpoints(java.util.Arrays.asList(endpoints));
        return;
    }


    /**
     *  Makes a collection of endpoints available in this session.
     *
     *  @param  endpoints a collection of endpoints
     *  @throws org.osid.NullArgumentException <code>endpoints<code>
     *          is <code>null</code>
     */

    protected void putEndpoints(java.util.Collection<? extends org.osid.transport.Endpoint> endpoints) {
        for (org.osid.transport.Endpoint endpoint : endpoints) {
            this.endpoints.put(endpoint.getId(), endpoint);
        }

        return;
    }


    /**
     *  Removes an Endpoint from this session.
     *
     *  @param  endpointId the <code>Id</code> of the endpoint
     *  @throws org.osid.NullArgumentException <code>endpointId<code> is
     *          <code>null</code>
     */

    protected void removeEndpoint(org.osid.id.Id endpointId) {
        this.endpoints.remove(endpointId);
        return;
    }


    /**
     *  Gets the <code>Endpoint</code> specified by its <code>Id</code>.
     *
     *  @param  endpointId <code>Id</code> of the <code>Endpoint</code>
     *  @return the endpoint
     *  @throws org.osid.NotFoundException <code>endpointId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>endpointId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.Endpoint getEndpoint(org.osid.id.Id endpointId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.transport.Endpoint endpoint = this.endpoints.get(endpointId);
        if (endpoint == null) {
            throw new org.osid.NotFoundException("endpoint not found: " + endpointId);
        }

        return (endpoint);
    }


    /**
     *  Gets all <code>Endpoints</code>. In plenary mode, the returned
     *  list contains all known endpoints or an error
     *  results. Otherwise, the returned list may contain only those
     *  endpoints that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Endpoints</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpoints()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.transport.endpoint.ArrayEndpointList(this.endpoints.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.endpoints.clear();
        super.close();
        return;
    }
}
