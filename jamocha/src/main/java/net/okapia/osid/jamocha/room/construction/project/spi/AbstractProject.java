//
// AbstractProject.java
//
//     Defines a Project.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Project</code>.
 */

public abstract class AbstractProject
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.room.construction.Project {

    private org.osid.room.Building building;
    private org.osid.financials.Currency cost;

    private final java.util.Collection<org.osid.room.construction.records.ProjectRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.building.getId());
    }


    /**
     *  Gets the building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.building);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @throws org.osid.NullArgumentException
     *          <code>building</code> is <code>null</code>
     */

    protected void setBuilding(org.osid.room.Building building) {
        nullarg(building, "building");
        this.building = building;
        return;
    }


    /**
     *  Tests if this renovation has a cost. 
     *
     *  @return <code> true </code> if a cost is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCost() {
        return (this.cost != null);
    }


    /**
     *  Gets the cost for this renovation. 
     *
     *  @return the cost 
     *  @throws org.osid.IllegalStateException <code> hasCost() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCost() {
        if (!hasCost()) {
            throw new org.osid.IllegalStateException("hasCost() is false");
        }

        return (this.cost);
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    protected void setCost(org.osid.financials.Currency cost) {
        nullarg(cost, "cost");
        this.cost = cost;
        return;
    }


    /**
     *  Tests if this project supports the given record
     *  <code>Type</code>.
     *
     *  @param  projectRecordType a project record type 
     *  @return <code>true</code> if the projectRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type projectRecordType) {
        for (org.osid.room.construction.records.ProjectRecord record : this.records) {
            if (record.implementsRecordType(projectRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Project</code> record <code>Type</code>.
     *
     *  @param  projectRecordType the project record type 
     *  @return the project record 
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(projectRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectRecord getProjectRecord(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.ProjectRecord record : this.records) {
            if (record.implementsRecordType(projectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(projectRecordType + " is not supported");
    }


    /**
     *  Adds a record to this project. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param projectRecord the project record
     *  @param projectRecordType project record type
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecord</code> or
     *          <code>projectRecordTypeproject</code> is
     *          <code>null</code>
     */
            
    protected void addProjectRecord(org.osid.room.construction.records.ProjectRecord projectRecord, 
                                    org.osid.type.Type projectRecordType) {

        nullarg(projectRecord, "project record");
        addRecordType(projectRecordType);
        this.records.add(projectRecord);
        
        return;
    }
}
