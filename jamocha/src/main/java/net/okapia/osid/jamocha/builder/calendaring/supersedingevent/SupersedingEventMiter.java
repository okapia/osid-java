//
// SupersedingEventMiter.java
//
//     Defines a SupersedingEvent miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.supersedingevent;


/**
 *  Defines a <code>SupersedingEvent</code> miter for use with the builders.
 */

public interface SupersedingEventMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.calendaring.SupersedingEvent {


    /**
     *  Sets the superseded event.
     *
     *  @param event a superseded event
     *  @throws org.osid.NullArgumentException
     *          <code>event</code> is <code>null</code>
     */

    public void setSupersededEvent(org.osid.calendaring.Event event);


    /**
     *  Sets the superseding event.
     *
     *  @param event a superseding event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public void setSupersedingEvent(org.osid.calendaring.Event event);


    /**
     *  Sets the superseded date.
     *
     *  @param date a superseded date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setSupersededDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the superseded event position.
     *
     *  @param position a superseded event position
     *  @throws org.osid.InvalidArgumentException
     *          <code>position</code> is negative
     */

    public void setSupersededEventPosition(long position);


    /**
     *  Adds a SupersedingEvent record.
     *
     *  @param record a supersedingEvent record
     *  @param recordType the type of supersedingEvent record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSupersedingEventRecord(org.osid.calendaring.records.SupersedingEventRecord record, org.osid.type.Type recordType);
}       


