//
// ControllerMiter.java
//
//     Defines a Controller miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.controller;


/**
 *  Defines a <code>Controller</code> miter for use with the builders.
 */

public interface ControllerMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            org.osid.control.Controller {


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException <code>address</code> is
     *          <code>null</code>
     */

    public void setAddress(String address);


    /**
     *  Sets the model.
     *
     *  @param model a model
     *  @throws org.osid.NullArgumentException <code>model</code> is
     *          <code>null</code>
     */

    public void setModel(org.osid.inventory.Model model);

    
    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @throws org.osid.NullArgumentException <code>version</code> is
     *          <code>null</code>
     */

    public void setVersion(org.osid.installation.Version version);


    /**
     *  Sets the toggleable.
     *
     *  @param toggleable {@code true} if a toggle, {@code false}
     *         otherwise
     */

    public void setToggleable(boolean toggleable);


    /**
     *  Sets the variable.
     *
     *  @param variable {@code true} if a variable dial, {@code false}
     *         otherwise
     */

    public void setVariable(boolean variable);


    /**
     *  Sets the variable by percentage.
     *
     *  @param variableByPercentage {@code true} if a variable percentage dial,
     *         {@code false} otherwise
     */

    public void setVariableByPercentage(boolean variableByPercentage);


    /**
     *  Sets the variable minimum.
     *
     *  @param value a variable minimum
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public void setVariableMinimum(java.math.BigDecimal value);


    /**
     *  Sets the variable maximum.
     *
     *  @param value a variable maximum
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public void setVariableMaximum(java.math.BigDecimal value);


    /**
     *  Sets the variable increment.
     *
     *  @param increment a variable increment
     *  @throws org.osid.NullArgumentException
     *          <code>increment</code> is <code>null</code>
     */

    public void setVariableIncrement(java.math.BigDecimal increment);


    /**
     *  Adds a discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public void addDiscreetState(org.osid.process.State state);


    /**
     *  Sets all the discreet states.
     *
     *  @param states a collection of discreet states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    public void setDiscreetStates(java.util.Collection<org.osid.process.State> states);


    /**
     *  Adds a Controller record.
     *
     *  @param record a controller record
     *  @param recordType the type of controller record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addControllerRecord(org.osid.control.records.ControllerRecord record, org.osid.type.Type recordType);
}       


