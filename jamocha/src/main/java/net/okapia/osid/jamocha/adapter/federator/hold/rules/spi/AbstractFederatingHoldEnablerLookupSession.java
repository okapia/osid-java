//
// AbstractFederatingHoldEnablerLookupSession.java
//
//     An abstract federating adapter for a HoldEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.hold.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  HoldEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingHoldEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.hold.rules.HoldEnablerLookupSession>
    implements org.osid.hold.rules.HoldEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.hold.Oubliette oubliette = new net.okapia.osid.jamocha.nil.hold.oubliette.UnknownOubliette();


    /**
     *  Constructs a new <code>AbstractFederatingHoldEnablerLookupSession</code>.
     */

    protected AbstractFederatingHoldEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.hold.rules.HoldEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Oubliette/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Oubliette Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.oubliette.getId());
    }


    /**
     *  Gets the <code>Oubliette</code> associated with this 
     *  session.
     *
     *  @return the <code>Oubliette</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.oubliette);
    }


    /**
     *  Sets the <code>Oubliette</code>.
     *
     *  @param  oubliette the oubliette for this session
     *  @throws org.osid.NullArgumentException <code>oubliette</code>
     *          is <code>null</code>
     */

    protected void setOubliette(org.osid.hold.Oubliette oubliette) {
        nullarg(oubliette, "oubliette");
        this.oubliette = oubliette;
        return;
    }


    /**
     *  Tests if this user can perform <code>HoldEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupHoldEnablers() {
        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            if (session.canLookupHoldEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>HoldEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeHoldEnablerView() {
        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            session.useComparativeHoldEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>HoldEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryHoldEnablerView() {
        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            session.usePlenaryHoldEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include hold enablers in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            session.useFederatedOublietteView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            session.useIsolatedOublietteView();
        }

        return;
    }


    /**
     *  Only active hold enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveHoldEnablerView() {
        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            session.useActiveHoldEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive hold enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusHoldEnablerView() {
        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            session.useAnyStatusHoldEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>HoldEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>HoldEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>HoldEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerId <code>Id</code> of the
     *          <code>HoldEnabler</code>
     *  @return the hold enabler
     *  @throws org.osid.NotFoundException <code>holdEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>holdEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnabler getHoldEnabler(org.osid.id.Id holdEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            try {
                return (session.getHoldEnabler(holdEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(holdEnablerId + " not found");
    }


    /**
     *  Gets a <code>HoldEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  holdEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>HoldEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>HoldEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByIds(org.osid.id.IdList holdEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.hold.rules.holdenabler.MutableHoldEnablerList ret = new net.okapia.osid.jamocha.hold.rules.holdenabler.MutableHoldEnablerList();

        try (org.osid.id.IdList ids = holdEnablerIds) {
            while (ids.hasNext()) {
                ret.addHoldEnabler(getHoldEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>HoldEnablerList</code> corresponding to the given
     *  hold enabler genus <code>Type</code> which does not include
     *  hold enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerGenusType a holdEnabler genus type 
     *  @return the returned <code>HoldEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByGenusType(org.osid.type.Type holdEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.FederatingHoldEnablerList ret = getHoldEnablerList();

        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            ret.addHoldEnablerList(session.getHoldEnablersByGenusType(holdEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>HoldEnablerList</code> corresponding to the given
     *  hold enabler genus <code>Type</code> and include any additional
     *  hold enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerGenusType a holdEnabler genus type 
     *  @return the returned <code>HoldEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByParentGenusType(org.osid.type.Type holdEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.FederatingHoldEnablerList ret = getHoldEnablerList();

        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            ret.addHoldEnablerList(session.getHoldEnablersByParentGenusType(holdEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>HoldEnablerList</code> containing the given
     *  hold enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  holdEnablerRecordType a holdEnabler record type 
     *  @return the returned <code>HoldEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByRecordType(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.FederatingHoldEnablerList ret = getHoldEnablerList();

        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            ret.addHoldEnablerList(session.getHoldEnablersByRecordType(holdEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>HoldEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible
     *  through this session.
     *  
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>HoldEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.FederatingHoldEnablerList ret = getHoldEnablerList();

        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            ret.addHoldEnablerList(session.getHoldEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>HoldEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible
     *  through this session.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>HoldEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.FederatingHoldEnablerList ret = getHoldEnablerList();

        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            ret.addHoldEnablerList(session.getHoldEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>HoldEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  hold enablers or an error results. Otherwise, the returned list
     *  may contain only those hold enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, hold enablers are returned that are currently
     *  active. In any status mode, active and inactive hold enablers
     *  are returned.
     *
     *  @return a list of <code>HoldEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.FederatingHoldEnablerList ret = getHoldEnablerList();

        for (org.osid.hold.rules.HoldEnablerLookupSession session : getSessions()) {
            ret.addHoldEnablerList(session.getHoldEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.FederatingHoldEnablerList getHoldEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.ParallelHoldEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.hold.rules.holdenabler.CompositeHoldEnablerList());
        }
    }
}
