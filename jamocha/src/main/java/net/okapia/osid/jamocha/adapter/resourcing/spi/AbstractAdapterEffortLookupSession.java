//
// AbstractAdapterEffortLookupSession.java
//
//    An Effort lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Effort lookup session adapter.
 */

public abstract class AbstractAdapterEffortLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.EffortLookupSession {

    private final org.osid.resourcing.EffortLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEffortLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEffortLookupSession(org.osid.resourcing.EffortLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code Effort} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEfforts() {
        return (this.session.canLookupEfforts());
    }


    /**
     *  A complete view of the {@code Effort} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEffortView() {
        this.session.useComparativeEffortView();
        return;
    }


    /**
     *  A complete view of the {@code Effort} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEffortView() {
        this.session.usePlenaryEffortView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include efforts in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only efforts whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEffortView() {
        this.session.useEffectiveEffortView();
        return;
    }
    

    /**
     *  All efforts of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEffortView() {
        this.session.useAnyEffectiveEffortView();
        return;
    }

     
    /**
     *  Gets the {@code Effort} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Effort} may
     *  have a different {@code Id} than requested, such as the case
     *  where a duplicate {@code Id} was assigned to a {@code Effort}
     *  and retained for compatibility.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param effortId {@code Id} of the {@code Effort}
     *  @return the effort
     *  @throws org.osid.NotFoundException {@code effortId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code effortId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Effort getEffort(org.osid.id.Id effortId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffort(effortId));
    }


    /**
     *  Gets an {@code EffortList} corresponding to the given {@code
     *  IdList}.
     *
     *  In plenary mode, the returned list contains all of the efforts
     *  specified in the {@code Id} list, in the order of the list,
     *  including duplicates, or an error results if an {@code Id} in
     *  the supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Efforts} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Effort} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code effortIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByIds(org.osid.id.IdList effortIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsByIds(effortIds));
    }


    /**
     *  Gets an {@code EffortList} corresponding to the given effort
     *  genus {@code Type} which does not include efforts of types
     *  derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortGenusType an effort genus type 
     *  @return the returned {@code Effort} list
     *  @throws org.osid.NullArgumentException
     *          {@code effortGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByGenusType(org.osid.type.Type effortGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsByGenusType(effortGenusType));
    }


    /**
     *  Gets an {@code EffortList} corresponding to the given effort
     *  genus {@code Type} and include any additional efforts with
     *  genus types derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortGenusType an effort genus type 
     *  @return the returned {@code Effort} list
     *  @throws org.osid.NullArgumentException
     *          {@code effortGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByParentGenusType(org.osid.type.Type effortGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsByParentGenusType(effortGenusType));
    }


    /**
     *  Gets an {@code EffortList} containing the given effort record
     *  {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortRecordType an effort record type 
     *  @return the returned {@code Effort} list
     *  @throws org.osid.NullArgumentException
     *          {@code effortRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByRecordType(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsByRecordType(effortRecordType));
    }


    /**
     *  Gets an {@code EffortList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *  
     *  In active mode, efforts are returned that are currently
     *  active. In any status mode, active and inactive efforts are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Effort} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsOnDate(org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsOnDate(from, to));
    }
        

    /**
     *  Gets a list of efforts corresponding to a resource {@code Id}.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForResource(resourceId));
    }


    /**
     *  Gets a list of efforts corresponding to a resource {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of efforts corresponding to a commission {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  commissionId the {@code Id} of the commission
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code commissionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForCommission(org.osid.id.Id commissionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForCommission(commissionId));
    }


    /**
     *  Gets a list of efforts corresponding to a commission {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  commissionId the {@code Id} of the commission
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code commissionId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForCommissionOnDate(org.osid.id.Id commissionId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForCommissionOnDate(commissionId, from, to));
    }


    /**
     *  Gets a list of efforts corresponding to resource and
     *  commission {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  commissionId the {@code Id} of the commission
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code commissionId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndCommission(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id commissionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForResourceAndCommission(resourceId, commissionId));
    }


    /**
     *  Gets a list of efforts corresponding to resource and
     *  commission {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective. In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param commissionId the {@code Id} of the commission
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code commissionId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndCommissionOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id commissionId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForResourceAndCommissionOnDate(resourceId, commissionId, from, to));
    }


    /**
     *  Gets a list of efforts corresponding to a work {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  workId the {@code Id} of the work
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code workId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForWork(org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForWork(workId));
    }


    /**
     *  Gets a list of efforts corresponding to a work {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  workId the {@code Id} of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code workId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForWorkOnDate(org.osid.id.Id workId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForWorkOnDate(workId, from, to));
    }


    /**
     *  Gets a list of efforts corresponding to resource and work
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  workId the {@code Id} of the work
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code workId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndWork(org.osid.id.Id resourceId,
                                                                       org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForResourceAndWork(resourceId, workId));
    }


    /**
     *  Gets a list of efforts corresponding to resource and work
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known efforts
     *  or an error results. Otherwise, the returned list may contain
     *  only those efforts that are accessible through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective. In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param workId the {@code Id} of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EffortList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code workId}, {@code from} or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndWorkOnDate(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id workId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEffortsForResourceAndWorkOnDate(resourceId, workId, from, to));
    }


    /**
     *  Gets all {@code Efforts}. 
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Efforts} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEfforts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEfforts());
    }
}
