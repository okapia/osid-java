//
// AbstractOsidGovernator.java
//
//     Defines a simple OSID rule to draw from.
//
//
// Tom Coppeto
// Okapia
// 22 January 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OSID rule to draw from. using this
 *  abstract class requires that <code>setId()</code> must be done
 *  before passing this interface to a consumer in order to maintain
 *  compliance with the OSID specification.
 */

public abstract class AbstractOsidGovernator
    extends AbstractSourceableOsidObject
    implements org.osid.OsidGovernator {

    private final Operable operable = new Operable();


    /**
     *  Tests if this governator is active. <code> isActive() </code>
     *  is <code> true </code> if <code> isEnabled() </code> and
     *  <code> isOperational() </code> are <code> true </code> and
     *  <code> isDisabled() </code> is <code> false. </code>
     *
     *  @return <code> true </code> if this governator is active,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isActive() {
        return (this.operable.isActive());
    }


    /**
     *  Tests if this governator is administravely
     *  enabled. Administratively enabling overrides any enabling rule
     *  which may exist. If this method returns <code> true </code>
     *  then <code> isDisabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this governator is enabled,
     *          <code> false </code> is the active status is
     *          determined by other rules
     */

    @OSID @Override
    public boolean isEnabled() {
        return (this.operable.isEnabled());
    }


    /**
     *  Sets the enabled flag.
     *
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if disabled
     */

    protected void setEnabled(boolean enabled) {
        this.operable.setEnabled(enabled);
        return;
    }


    /**
     *  Tests if this governator is administravely
     *  disabled. Administratively disabling overrides any disabling
     *  rule which may exist. If this method returns <code> true
     *  </code> then <code> isEnabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this governator is disabled,
     *          <code> false </code> is the active status is
     *          determined by other rules
     */

    @OSID @Override
    public boolean isDisabled() {
        return (this.operable.isDisabled());
    }


    /**
     *  Sets the disabled flag.
     *
     *  @param disabled <code>true</code> if disabled,
     *         <code>false<code> if disabled
     */

    protected void setDisabled(boolean disabled) {
        this.operable.setDisabled(disabled);
        return;
    }


    /**
     *  Tests if this governator is operational in that all rules
     *  pertaining to this operation except for an administrative
     *  disable are <code> true.  </code>
     *
     *  @return <code> true </code> if this governator is operational,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isOperational() {
        return (this.operable.isOperational());
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational the operational flag
     */

    protected void setOperational(boolean operational) {
        this.operable.setOperational(operational);
        return;
    }


    protected class Operable
        extends AbstractOperable
        implements org.osid.Operable {


        /**
         *  Sets the enabled flag.
         *
         *  @param enabled <code>true</code> if enabled,
         *         <code>false<code> if disabled
         */

        @Override
        protected void setEnabled(boolean enabled) {
            super.setEnabled(enabled);
            return;
        }


        /**
         *  Sets the disabled flag.
         *
         *  @param disabled <code>true</code> if disabled,
         *         <code>false<code> if disabled
         */

        @Override
        protected void setDisabled(boolean disabled) {
            super.setDisabled(disabled);
            return;
        }


        /**
         *  Sets the operational flag.
         *
         *  @param operational the operational flag
         */
        
        @Override
        protected void setOperational(boolean operational) {
            super.setOperational(operational);
            return;
        }
    }    
}
