//
// AbstractPollsQuery.java
//
//     A template for making a Polls Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.polls.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for pollses.
 */

public abstract class AbstractPollsQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.voting.PollsQuery {

    private final java.util.Collection<org.osid.voting.records.PollsQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the candidate <code> Id </code> for this query. 
     *
     *  @param  candidateId a candidate <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> candidateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCandidateId(org.osid.id.Id candidateId, boolean match) {
        return;
    }


    /**
     *  Clears the candidate <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCandidateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CandidateQuery </code> is available. 
     *
     *  @return <code> true </code> if a candidate query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a candidate. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the candidate query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuery getCandidateQuery() {
        throw new org.osid.UnimplementedException("supportsCandidateQuery() is false");
    }


    /**
     *  Matches polls with any candidate. 
     *
     *  @param  match <code> true </code> to match polls with any candidate, 
     *          <code> false </code> to match polls with no candidates 
     */

    @OSID @Override
    public void matchAnyCandidate(boolean match) {
        return;
    }


    /**
     *  Clears the candidate terms. 
     */

    @OSID @Override
    public void clearCandidateTerms() {
        return;
    }


    /**
     *  Sets the polls <code> Id </code> for this query to match polls that 
     *  have the specified polls as an ancestor. 
     *
     *  @param  pollsid a polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorPollsId(org.osid.id.Id pollsid, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorPollsQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getAncestorPollsQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorPollsQuery() is false");
    }


    /**
     *  Matches polls with any ancestor. 
     *
     *  @param  match <code> true </code> to match polls with any ancestor, 
     *          <code> false </code> to match root polls 
     */

    @OSID @Override
    public void matchAnyAncestorPolls(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor polls terms. 
     */

    @OSID @Override
    public void clearAncestorPollsTerms() {
        return;
    }


    /**
     *  Sets the polls <code> Id </code> for this query to match polls that 
     *  have the specified polls as a descendant. 
     *
     *  @param  pollsid a polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantPollsId(org.osid.id.Id pollsid, boolean match) {
        return;
    }


    /**
     *  Clears the descendant polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantPollsQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getDescendantPollsQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantPollsQuery() is false");
    }


    /**
     *  Matches polls with any descendant. 
     *
     *  @param  match <code> true </code> to match polls with any descendant, 
     *          <code> false </code> to match leaf polls 
     */

    @OSID @Override
    public void matchAnyDescendantPolls(boolean match) {
        return;
    }


    /**
     *  Clears the descendant polls terms. 
     */

    @OSID @Override
    public void clearDescendantPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given polls query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a polls implementing the requested record.
     *
     *  @param pollsRecordType a polls record type
     *  @return the polls query record
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pollsRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsQueryRecord getPollsQueryRecord(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.PollsQueryRecord record : this.records) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pollsRecordType + " is not supported");
    }


    /**
     *  Adds a record to this polls query. 
     *
     *  @param pollsQueryRecord polls query record
     *  @param pollsRecordType polls record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPollsQueryRecord(org.osid.voting.records.PollsQueryRecord pollsQueryRecord, 
                                          org.osid.type.Type pollsRecordType) {

        addRecordType(pollsRecordType);
        nullarg(pollsQueryRecord, "polls query record");
        this.records.add(pollsQueryRecord);        
        return;
    }
}
