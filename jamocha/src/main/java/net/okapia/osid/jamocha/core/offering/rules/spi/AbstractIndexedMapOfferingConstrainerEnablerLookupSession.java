//
// AbstractIndexedMapOfferingConstrainerEnablerLookupSession.java
//
//    A simple framework for providing an OfferingConstrainerEnabler lookup service
//    backed by a fixed collection of offering constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an OfferingConstrainerEnabler lookup service backed by a
 *  fixed collection of offering constrainer enablers. The offering constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some offering constrainer enablers may be compatible
 *  with more types than are indicated through these offering constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OfferingConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOfferingConstrainerEnablerLookupSession
    extends AbstractMapOfferingConstrainerEnablerLookupSession
    implements org.osid.offering.rules.OfferingConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.OfferingConstrainerEnabler> offeringConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.OfferingConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.OfferingConstrainerEnabler> offeringConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.OfferingConstrainerEnabler>());


    /**
     *  Makes an <code>OfferingConstrainerEnabler</code> available in this session.
     *
     *  @param  offeringConstrainerEnabler an offering constrainer enabler
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOfferingConstrainerEnabler(org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler) {
        super.putOfferingConstrainerEnabler(offeringConstrainerEnabler);

        this.offeringConstrainerEnablersByGenus.put(offeringConstrainerEnabler.getGenusType(), offeringConstrainerEnabler);
        
        try (org.osid.type.TypeList types = offeringConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offeringConstrainerEnablersByRecord.put(types.getNextType(), offeringConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of offering constrainer enablers available in this session.
     *
     *  @param  offeringConstrainerEnablers an array of offering constrainer enablers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putOfferingConstrainerEnablers(org.osid.offering.rules.OfferingConstrainerEnabler[] offeringConstrainerEnablers) {
        for (org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler : offeringConstrainerEnablers) {
            putOfferingConstrainerEnabler(offeringConstrainerEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of offering constrainer enablers available in this session.
     *
     *  @param  offeringConstrainerEnablers a collection of offering constrainer enablers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putOfferingConstrainerEnablers(java.util.Collection<? extends org.osid.offering.rules.OfferingConstrainerEnabler> offeringConstrainerEnablers) {
        for (org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler : offeringConstrainerEnablers) {
            putOfferingConstrainerEnabler(offeringConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes an offering constrainer enabler from this session.
     *
     *  @param offeringConstrainerEnablerId the <code>Id</code> of the offering constrainer enabler
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOfferingConstrainerEnabler(org.osid.id.Id offeringConstrainerEnablerId) {
        org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler;
        try {
            offeringConstrainerEnabler = getOfferingConstrainerEnabler(offeringConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.offeringConstrainerEnablersByGenus.remove(offeringConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = offeringConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offeringConstrainerEnablersByRecord.remove(types.getNextType(), offeringConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOfferingConstrainerEnabler(offeringConstrainerEnablerId);
        return;
    }


    /**
     *  Gets an <code>OfferingConstrainerEnablerList</code> corresponding to the given
     *  offering constrainer enabler genus <code>Type</code> which does not include
     *  offering constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known offering constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those offering constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  offeringConstrainerEnablerGenusType an offering constrainer enabler genus type 
     *  @return the returned <code>OfferingConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByGenusType(org.osid.type.Type offeringConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.offeringconstrainerenabler.ArrayOfferingConstrainerEnablerList(this.offeringConstrainerEnablersByGenus.get(offeringConstrainerEnablerGenusType)));
    }


    /**
     *  Gets an <code>OfferingConstrainerEnablerList</code> containing the given
     *  offering constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known offering constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  offering constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  offeringConstrainerEnablerRecordType an offering constrainer enabler record type 
     *  @return the returned <code>offeringConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.offeringconstrainerenabler.ArrayOfferingConstrainerEnablerList(this.offeringConstrainerEnablersByRecord.get(offeringConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offeringConstrainerEnablersByGenus.clear();
        this.offeringConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
