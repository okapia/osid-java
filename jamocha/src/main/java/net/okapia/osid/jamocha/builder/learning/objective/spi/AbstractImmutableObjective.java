//
// AbstractImmutableObjective.java
//
//     Wraps a mutable Objective to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Objective</code> to hide modifiers. This
 *  wrapper provides an immutized Objective from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying objective whose state changes are visible.
 */

public abstract class AbstractImmutableObjective
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.learning.Objective {

    private final org.osid.learning.Objective objective;


    /**
     *  Constructs a new <code>AbstractImmutableObjective</code>.
     *
     *  @param objective the objective to immutablize
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableObjective(org.osid.learning.Objective objective) {
        super(objective);
        this.objective = objective;
        return;
    }


    /**
     *  Tests if an assessment is associated with this objective. 
     *
     *  @return <code> true </code> if an assessment exists, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasAssessment() {
        return (this.objective.hasAssessment());
    }


    /**
     *  Gets the assessment <code> Id </code> associated with this learning 
     *  objective. 
     *
     *  @return the assessment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.objective.getAssessmentId());
    }


    /**
     *  Gets the assessment associated with this learning objective. 
     *
     *  @return the assessment 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.objective.getAssessment());
    }


    /**
     *  Tests if this objective has a knowledge dimension 
     *
     *  @return <code> true </code> if a knowledge category exists, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasKnowledgeCategory() {
        return (this.objective.hasKnowledgeCategory());
    }


    /**
     *  Gets the grade <code> Id </code> associated with the knowledge 
     *  dimension. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasKnowledgeCategory() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getKnowledgeCategoryId() {
        return (this.objective.getKnowledgeCategoryId());
    }


    /**
     *  Gets the grade associated with the knowledge dimension. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasKnowledgeCategory() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getKnowledgeCategory()
        throws org.osid.OperationFailedException {

        return (this.objective.getKnowledgeCategory());
    }


    /**
     *  Tests if this objective has a cognitive process type. 
     *
     *  @return <code> true </code> if a cognitive process exists, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCognitiveProcess() {
        return (this.objective.hasCognitiveProcess());
    }


    /**
     *  Gets the grade <code> Id </code> associated with the cognitive 
     *  process. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCognitiveProcess() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCognitiveProcessId() {
        return (this.objective.getCognitiveProcessId());
    }


    /**
     *  Gets the grade associated with the cognitive process. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasCognitiveProcess() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getCognitiveProcess()
        throws org.osid.OperationFailedException {

        return (this.objective.getCognitiveProcess());
    }


    /**
     *  Gets the objective bank record corresponding to the given <code> 
     *  Objective </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  objectiveRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(objectiveRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return the objective record 
     *  @throws org.osid.NullArgumentException <code> objectiveRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(objectiveRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveRecord getObjectiveRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        return (this.objective.getObjectiveRecord(objectiveRecordType));
    }
}

