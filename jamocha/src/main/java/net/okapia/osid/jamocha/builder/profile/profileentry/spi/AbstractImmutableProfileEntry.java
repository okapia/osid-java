//
// AbstractImmutableProfileEntry.java
//
//     Wraps a mutable ProfileEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ProfileEntry</code> to hide modifiers. This
 *  wrapper provides an immutized ProfileEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying profileEntry whose state changes are visible.
 */

public abstract class AbstractImmutableProfileEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.profile.ProfileEntry {

    private final org.osid.profile.ProfileEntry profileEntry;


    /**
     *  Constructs a new <code>AbstractImmutableProfileEntry</code>.
     *
     *  @param profileEntry the profile entry to immutablize
     *  @throws org.osid.NullArgumentException <code>profileEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProfileEntry(org.osid.profile.ProfileEntry profileEntry) {
        super(profileEntry);
        this.profileEntry = profileEntry;
        return;
    }


    /**
     *  Tests if this profile entry is implicitly generated. 
     *
     *  @return <code> true </code> if this profile entry is implicit, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.profileEntry.isImplicit());
    }


    /**
     *  Tests if this profile entry has a <code> Resource. </code> 
     *
     *  @return <code> true </code> if this profile entry has a <code> 
     *          Resource, </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasResource() {
        return (this.profileEntry.hasResource());
    }


    /**
     *  Gets the <code> resource Id </code> for this profile entry. 
     *
     *  @return the <code> Resource Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.profileEntry.getResourceId());
    }


    /**
     *  Gets the <code> Resource </code> for this profile ebtry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.profileEntry.getResource());
    }


    /**
     *  Tests if this profile entry has an <code> Agent. </code> An implied 
     *  profile entry may have an <code> Agent </code> in addition to a 
     *  specified <code> Resource. </code> 
     *
     *  @return <code> true </code> if this profile entry has an <code> Agent, 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAgent() {
        return (this.profileEntry.hasAgent());
    }


    /**
     *  Gets the <code> Agent Id </code> for this profile entry. 
     *
     *  @return the <code> Agent Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.profileEntry.getAgentId());
    }


    /**
     *  Gets the <code> Agent </code> for this profile entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.profileEntry.getAgent());
    }


    /**
     *  Gets the <code> ProfileItem </code> <code> Id </code> for this profile 
     *  entry. 
     *
     *  @return the profile item <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProfileItemId() {
        return (this.profileEntry.getProfileItemId());
    }


    /**
     *  Gets the <code> ProfileItem </code> for this profile entry. 
     *
     *  @return the profile item 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.profile.ProfileItem getProfileItem()
        throws org.osid.OperationFailedException {

        return (this.profileEntry.getProfileItem());
    }


    /**
     *  Gets the proile entry record corresponding to the given <code> 
     *  ProfileEntry </code> record <code> Type. </code> This method ie used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  profileEntryRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(profileEntryRecordType) </code> is <code> true </code> . 
     *
     *  @param  profileEntryRecordType the type of the record to retrieve 
     *  @return the profile entry record 
     *  @throws org.osid.NullArgumentException <code> profileEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(profileEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntryRecord getProfileEntryRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.profileEntry.getProfileEntryRecord(profileEntryRecordType));
    }
}

