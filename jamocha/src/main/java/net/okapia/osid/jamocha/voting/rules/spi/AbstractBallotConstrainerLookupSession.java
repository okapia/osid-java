//
// AbstractBallotConstrainerLookupSession.java
//
//    A starter implementation framework for providing a BallotConstrainer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a BallotConstrainer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBallotConstrainers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBallotConstrainerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.rules.BallotConstrainerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();
    

    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>BallotConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBallotConstrainers() {
        return (true);
    }


    /**
     *  A complete view of the <code>BallotConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBallotConstrainerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>BallotConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBallotConstrainerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active ballot constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBallotConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive ballot constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BallotConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BallotConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>BallotConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @param  ballotConstrainerId <code>Id</code> of the
     *          <code>BallotConstrainer</code>
     *  @return the ballot constrainer
     *  @throws org.osid.NotFoundException <code>ballotConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainer getBallotConstrainer(org.osid.id.Id ballotConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.rules.BallotConstrainerList ballotConstrainers = getBallotConstrainers()) {
            while (ballotConstrainers.hasNext()) {
                org.osid.voting.rules.BallotConstrainer ballotConstrainer = ballotConstrainers.getNextBallotConstrainer();
                if (ballotConstrainer.getId().equals(ballotConstrainerId)) {
                    return (ballotConstrainer);
                }
            }
        } 

        throw new org.osid.NotFoundException(ballotConstrainerId + " not found");
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ballotConstrainers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>BallotConstrainers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBallotConstrainers()</code>.
     *
     *  @param  ballotConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByIds(org.osid.id.IdList ballotConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.rules.BallotConstrainer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = ballotConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBallotConstrainer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("ballot constrainer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.rules.ballotconstrainer.LinkedBallotConstrainerList(ret));
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the given
     *  ballot constrainer genus <code>Type</code> which does not include
     *  ballot constrainers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBallotConstrainers()</code>.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.ballotconstrainer.BallotConstrainerGenusFilterList(getBallotConstrainers(), ballotConstrainerGenusType));
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> corresponding to the given
     *  ballot constrainer genus <code>Type</code> and include any additional
     *  ballot constrainers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBallotConstrainers()</code>.
     *
     *  @param  ballotConstrainerGenusType a ballotConstrainer genus type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByParentGenusType(org.osid.type.Type ballotConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBallotConstrainersByGenusType(ballotConstrainerGenusType));
    }


    /**
     *  Gets a <code>BallotConstrainerList</code> containing the given
     *  ballot constrainer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBallotConstrainers()</code>.
     *
     *  @param  ballotConstrainerRecordType a ballotConstrainer record type 
     *  @return the returned <code>BallotConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerList getBallotConstrainersByRecordType(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.ballotconstrainer.BallotConstrainerRecordFilterList(getBallotConstrainers(), ballotConstrainerRecordType));
    }


    /**
     *  Gets all <code>BallotConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  ballot constrainers or an error results. Otherwise, the returned list
     *  may contain only those ballot constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, ballot constrainers are returned that are currently
     *  active. In any status mode, active and inactive ballot constrainers
     *  are returned.
     *
     *  @return a list of <code>BallotConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.voting.rules.BallotConstrainerList getBallotConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the ballot constrainer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of ballot constrainers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.voting.rules.BallotConstrainerList filterBallotConstrainersOnViews(org.osid.voting.rules.BallotConstrainerList list)
        throws org.osid.OperationFailedException {

        org.osid.voting.rules.BallotConstrainerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.voting.rules.ballotconstrainer.ActiveBallotConstrainerFilterList(ret);
        }

        return (ret);
    }
}
