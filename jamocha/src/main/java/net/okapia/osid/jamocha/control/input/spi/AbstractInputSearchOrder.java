//
// AbstractInputSearchOdrer.java
//
//     Defines an InputSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.input.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code InputSearchOrder}.
 */

public abstract class AbstractInputSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.control.InputSearchOrder {

    private final java.util.Collection<org.osid.control.records.InputSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by device. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDevice(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a device search order is available. 
     *
     *  @return <code> true </code> if a device search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the device search order. 
     *
     *  @return the device search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsDeviceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchOrder getDeviceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDeviceSearchOrder() is false");
    }


    /**
     *  Orders the results by controller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByController(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a controller search order is available. 
     *
     *  @return <code> true </code> if a controller search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the controller search order. 
     *
     *  @return the controller search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsControllerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchOrder getControllerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsControllerSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  inputRecordType an input record type 
     *  @return {@code true} if the inputRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code inputRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inputRecordType) {
        for (org.osid.control.records.InputSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(inputRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  inputRecordType the input record type 
     *  @return the input search order record
     *  @throws org.osid.NullArgumentException
     *          {@code inputRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(inputRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.control.records.InputSearchOrderRecord getInputSearchOrderRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.InputSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(inputRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this input. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inputRecord the input search odrer record
     *  @param inputRecordType input record type
     *  @throws org.osid.NullArgumentException
     *          {@code inputRecord} or
     *          {@code inputRecordTypeinput} is
     *          {@code null}
     */
            
    protected void addInputRecord(org.osid.control.records.InputSearchOrderRecord inputSearchOrderRecord, 
                                     org.osid.type.Type inputRecordType) {

        addRecordType(inputRecordType);
        this.records.add(inputSearchOrderRecord);
        
        return;
    }
}
