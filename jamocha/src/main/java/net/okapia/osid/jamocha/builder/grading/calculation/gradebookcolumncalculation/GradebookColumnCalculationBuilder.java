//
// GradebookColumnCalculation.java
//
//     Defines a GradebookColumnCalculation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation;


/**
 *  Defines a <code>GradebookColumnCalculation</code> builder.
 */

public final class GradebookColumnCalculationBuilder
    extends net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.spi.AbstractGradebookColumnCalculationBuilder<GradebookColumnCalculationBuilder> {
    

    /**
     *  Constructs a new <code>GradebookColumnCalculationBuilder</code> using a
     *  <code>MutableGradebookColumnCalculation</code>.
     */

    public GradebookColumnCalculationBuilder() {
        super(new MutableGradebookColumnCalculation());
        return;
    }


    /**
     *  Constructs a new <code>GradebookColumnCalculationBuilder</code> using the given
     *  mutable gradebookColumnCalculation.
     * 
     *  @param gradebookColumnCalculation
     */

    public GradebookColumnCalculationBuilder(GradebookColumnCalculationMiter gradebookColumnCalculation) {
        super(gradebookColumnCalculation);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return GradebookColumnCalculationBuilder
     */

    @Override
    protected GradebookColumnCalculationBuilder self() {
        return (this);
    }
}       


