//
// AbstractAwardLookupSession.java
//
//    A starter implementation framework for providing an Award
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Award lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAwards(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAwardLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.recognition.AwardLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.recognition.Academy academy = new net.okapia.osid.jamocha.nil.recognition.academy.UnknownAcademy();
    

    /**
     *  Gets the <code>Academy/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.academy.getId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this session.
     *
     *  @return the <code>Academy</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.academy);
    }


    /**
     *  Sets the <code>Academy</code>.
     *
     *  @param  academy the academy for this session
     *  @throws org.osid.NullArgumentException <code>academy</code>
     *          is <code>null</code>
     */

    protected void setAcademy(org.osid.recognition.Academy academy) {
        nullarg(academy, "academy");
        this.academy = academy;
        return;
    }


    /**
     *  Tests if this user can perform <code>Award</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAwards() {
        return (true);
    }


    /**
     *  A complete view of the <code>Award</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAwardView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Award</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAwardView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include awards in academies which are children of
     *  this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Award</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Award</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Award</code> and retained for
     *  compatibility.
     *
     *  @param  awardId <code>Id</code> of the
     *          <code>Award</code>
     *  @return the award
     *  @throws org.osid.NotFoundException <code>awardId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward(org.osid.id.Id awardId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.recognition.AwardList awards = getAwards()) {
            while (awards.hasNext()) {
                org.osid.recognition.Award award = awards.getNextAward();
                if (award.getId().equals(awardId)) {
                    return (award);
                }
            }
        } 

        throw new org.osid.NotFoundException(awardId + " not found");
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the awards
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Awards</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAwards()</code>.
     *
     *  @param  awardIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>awardIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByIds(org.osid.id.IdList awardIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.recognition.Award> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = awardIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAward(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("award " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.recognition.award.LinkedAwardList(ret));
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  award genus <code>Type</code> which does not include awards of
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known awards
     *  or an error results. Otherwise, the returned list may contain
     *  only those awards that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAwards()</code>.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.award.AwardGenusFilterList(getAwards(), awardGenusType));
    }


    /**
     *  Gets an <code>AwardList</code> corresponding to the given
     *  award genus <code>Type</code> and include any additional
     *  awards with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known awards
     *  or an error results. Otherwise, the returned list may contain
     *  only those awards that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAwards()</code>.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByParentGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAwardsByGenusType(awardGenusType));
    }


    /**
     *  Gets an <code>AwardList</code> containing the given award
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known awards
     *  or an error results. Otherwise, the returned list may contain
     *  only those awards that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAwards()</code>.
     *
     *  @param  awardRecordType an award record type 
     *  @return the returned <code>Award</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByRecordType(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.recognition.award.AwardRecordFilterList(getAwards(), awardRecordType));
    }


    /**
     *  Gets all <code>Awards</code>.
     *
     *  In plenary mode, the returned list contains all known awards
     *  or an error results. Otherwise, the returned list may contain
     *  only those awards that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Awards</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.recognition.AwardList getAwards()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the award list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of awards
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.recognition.AwardList filterAwardsOnViews(org.osid.recognition.AwardList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
