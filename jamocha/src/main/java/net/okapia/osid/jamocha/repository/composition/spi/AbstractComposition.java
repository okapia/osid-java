//
// AbstractComposition.java
//
//     Defines a Composition.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Composition</code>.
 */

public abstract class AbstractComposition
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObject
    implements org.osid.repository.Composition {

    private final java.util.Collection<org.osid.repository.Composition> children = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.CompositionRecord> records = new java.util.LinkedHashSet<>();

    private final Operable operable = new Operable();
    private final Containable containable = new Containable();


    /**
     *  Tests if this composition is active. <code> isActive() </code>
     *  is <code> true </code> if <code> isEnabled() </code> and
     *  <code> isOperational() </code> are <code> true </code> and
     *  <code> isDisabled() </code> is <code> false. </code>
     *
     *  @return <code> true </code> if this composition is active,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isActive() {
        return (this.operable.isActive());
    }


    /**
     *  Tests if this composition is administravely
     *  enabled. Administratively enabling overrides any enabling rule
     *  which may exist. If this method returns <code> true </code>
     *  then <code> isDisabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this composition is enabled,
     *          <code> false </code> is the active status is
     *          determined by other rules
     */

    @OSID @Override
    public boolean isEnabled() {
        return (this.operable.isEnabled());
    }


    /**
     *  Sets the enabled flag.
     *
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if disabled
     */

    protected void setEnabled(boolean enabled) {
        this.operable.setEnabled(enabled);
        return;
    }


    /**
     *  Tests if this composition is administravely
     *  disabled. Administratively disabling overrides any disabling
     *  rule which may exist. If this method returns <code> true
     *  </code> then <code> isEnabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this composition is disabled,
     *          <code> false </code> is the active status is
     *          determined by other rules
     */

    @OSID @Override
    public boolean isDisabled() {
        return (this.operable.isDisabled());
    }


    /**
     *  Sets the disabled flag.
     *
     *  @param disabled <code>true</code> if disabled,
     *         <code>false<code> if disabled
     */

    protected void setDisabled(boolean disabled) {
        this.operable.setDisabled(disabled);
        return;
    }


    /**
     *  Tests if this composition is operational in that all rules
     *  pertaining to this operation except for an administrative
     *  disable are <code> true.  </code>
     *
     *  @return <code> true </code> if this composition is operational,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isOperational() {
        return (this.operable.isOperational());
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational the operational flag
     */

    protected void setOperational(boolean operational) {
        this.operable.setOperational(operational);
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    protected void setSequestered(boolean sequestered) {
        this.containable.setSequestered(sequestered);
        return;
    }


    /**
     *  Gets the child <code> Ids </code> of this composition. 
     *
     *  @return the composition child <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getChildrenIds() {
        try {
            org.osid.repository.CompositionList children = getChildren();
            return (new net.okapia.osid.jamocha.adapter.converter.repository.composition.CompositionToIdList(children));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the children of this composition. 
     *
     *  @return the composition children 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getChildren()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.repository.composition.ArrayCompositionList(this.children));
    }


    /**
     *  Adds a child.
     *
     *  @param child a child
     *  @throws org.osid.NullArgumentException
     *          <code>child</code> is <code>null</code>
     */

    protected void addChild(org.osid.repository.Composition child) {
        nullarg(child, "child composition");
        this.children.add(child);
        return;
    }


    /**
     *  Sets all the children.
     *
     *  @param children a collection of children
     *  @throws org.osid.NullArgumentException
     *          <code>children</code> is <code>null</code>
     */

    protected void setChildren(java.util.Collection<org.osid.repository.Composition> children) {
        nullarg(children, "child compositions");
        this.children.clear();
        this.children.addAll(children);
        return;
    }


    /**
     *  Tests if this composition supports the given record
     *  <code>Type</code>.
     *
     *  @param  compositionRecordType a composition record type 
     *  @return <code>true</code> if the compositionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type compositionRecordType) {
        for (org.osid.repository.records.CompositionRecord record : this.records) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Composition</code> record <code>Type</code>.
     *
     *  @param  compositionRecordType the composition record type 
     *  @return the composition record 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionRecord getCompositionRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.CompositionRecord record : this.records) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this composition. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param compositionRecord the composition record
     *  @param compositionRecordType composition record type
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecord</code> or
     *          <code>compositionRecordTypecomposition</code> is
     *          <code>null</code>
     */
            
    protected void addCompositionRecord(org.osid.repository.records.CompositionRecord compositionRecord, 
                                        org.osid.type.Type compositionRecordType) {
        
        nullarg(compositionRecordType, "composition record type");
        addRecordType(compositionRecordType);
        this.records.add(compositionRecord);
        
        return;
    }


    protected class Operable
        extends net.okapia.osid.jamocha.spi.AbstractOperable
        implements org.osid.Operable {


        /**
         *  Sets the enabled flag.
         *
         *  @param enabled <code>true</code> if enabled,
         *         <code>false<code> if disabled
         */

        @Override
        protected void setEnabled(boolean enabled) {
            super.setEnabled(enabled);
            return;
        }


        /**
         *  Sets the disabled flag.
         *
         *  @param disabled <code>true</code> if disabled,
         *         <code>false<code> if disabled
         */

        @Override
        protected void setDisabled(boolean disabled) {
            super.setDisabled(disabled);
            return;
        }


        /**
         *  Sets the operational flag.
         *
         *  @param operational the operational flag
         */
        
        @Override
        protected void setOperational(boolean operational) {
            super.setOperational(operational);
            return;
        }
    }    


    protected class Containable
        extends net.okapia.osid.jamocha.spi.AbstractContainable
        implements org.osid.Containable {


        /**
         *  Sets the sequestered flag.
         *
         *  @param sequestered <code> true </code> if this containable is
         *         sequestered, <code> false </code> if this containable
         *         may appear outside its aggregate
         */

        @Override
        protected void setSequestered(boolean sequestered) {
            super.setSequestered(sequestered);
            return;
        }
    }
}
