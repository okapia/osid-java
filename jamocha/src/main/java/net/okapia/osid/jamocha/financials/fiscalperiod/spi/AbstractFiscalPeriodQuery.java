//
// AbstractFiscalPeriodQuery.java
//
//     A template for making a FiscalPeriod Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for fiscal periods.
 */

public abstract class AbstractFiscalPeriodQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.financials.FiscalPeriodQuery {

    private final java.util.Collection<org.osid.financials.records.FiscalPeriodQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a display label for this query. 
     *
     *  @param  label label string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches a display label that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          display label, <code> false </code> to match fiscal periods 
     *          with no display label 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        return;
    }


    /**
     *  Clears the display label terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        return;
    }


    /**
     *  Adds a fiscal year for this query to match periods in the given fiscal 
     *  years inclusive. 
     *
     *  @param  from start of year range 
     *  @param  to end of year range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchFiscalYear(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches a fiscal year that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          fiscal year, <code> false </code> to match fiscal periods with 
     *          no fiscal year 
     */

    @OSID @Override
    public void matchAnyFiscalYear(boolean match) {
        return;
    }


    /**
     *  Clears the fiscal year terms. 
     */

    @OSID @Override
    public void clearFiscalYearTerms() {
        return;
    }


    /**
     *  Matches a start date within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches a start date that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          start date, <code> false </code> to match fiscal periods with 
     *          no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        return;
    }


    /**
     *  Clears the start date terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        return;
    }


    /**
     *  Matches an end date within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches an end date that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any end 
     *          date, <code> false </code> to match fiscal periods with no end 
     *          date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        return;
    }


    /**
     *  Clears the end date terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        return;
    }


    /**
     *  Matches a fiscal period duratione within the given date range 
     *  inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration from, 
                              org.osid.calendaring.Duration to, boolean match) {
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        return;
    }


    /**
     *  Matches a budget deadline within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBudgetDeadline(org.osid.calendaring.DateTime from, 
                                    org.osid.calendaring.DateTime to, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches a budget deadline that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          budget deadline, <code> false </code> to match fiscal periods 
     *          with no budget deadline 
     */

    @OSID @Override
    public void matchAnyBudgetDeadline(boolean match) {
        return;
    }


    /**
     *  Clears the budget deadline terms. 
     */

    @OSID @Override
    public void clearBudgetDeadlineTerms() {
        return;
    }


    /**
     *  Matches a posting deadline within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPostingDeadline(org.osid.calendaring.DateTime from, 
                                     org.osid.calendaring.DateTime to, 
                                     boolean match) {
        return;
    }


    /**
     *  Matches a posting deadline that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          posting deadline, <code> false </code> to match fiscal periods 
     *          with no posting deadline 
     */

    @OSID @Override
    public void matchAnyPostingDeadline(boolean match) {
        return;
    }


    /**
     *  Clears the posting deadline terms. 
     */

    @OSID @Override
    public void clearPostingDeadlineTerms() {
        return;
    }


    /**
     *  Matches a closing date within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchClosing(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches a closing date that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          closing date, <code> false </code> to match fiscal periods 
     *          with no closing date 
     */

    @OSID @Override
    public void matchAnyClosing(boolean match) {
        return;
    }


    /**
     *  Clears the closing terms. 
     */

    @OSID @Override
    public void clearClosingTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match fiscal 
     *  periods assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given fiscal period query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a fiscal period implementing the requested record.
     *
     *  @param fiscalPeriodRecordType a fiscal period record type
     *  @return the fiscal period query record
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fiscalPeriodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodQueryRecord getFiscalPeriodQueryRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.FiscalPeriodQueryRecord record : this.records) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fiscalPeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this fiscal period query. 
     *
     *  @param fiscalPeriodQueryRecord fiscal period query record
     *  @param fiscalPeriodRecordType fiscalPeriod record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFiscalPeriodQueryRecord(org.osid.financials.records.FiscalPeriodQueryRecord fiscalPeriodQueryRecord, 
                                          org.osid.type.Type fiscalPeriodRecordType) {

        addRecordType(fiscalPeriodRecordType);
        nullarg(fiscalPeriodQueryRecord, "fiscal period query record");
        this.records.add(fiscalPeriodQueryRecord);        
        return;
    }
}
