//
// UnknownCredentialEntry.java
//
//     Defines an unknown CredentialEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.chronicle.credentialentry;


/**
 *  Defines an unknown <code>CredentialEntry</code>.
 */

public final class UnknownCredentialEntry
    extends net.okapia.osid.jamocha.nil.course.chronicle.credentialentry.spi.AbstractUnknownCredentialEntry
    implements org.osid.course.chronicle.CredentialEntry {


    /**
     *  Constructs a new <code>UnknownCredentialEntry</code>.
     */

    public UnknownCredentialEntry() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownCredentialEntry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownCredentialEntry(boolean optional) {
        super(optional);
        addCredentialEntryRecord(new CredentialEntryRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown CredentialEntry.
     *
     *  @return an unknown CredentialEntry
     */

    public static org.osid.course.chronicle.CredentialEntry create() {
        return (net.okapia.osid.jamocha.builder.validator.course.chronicle.credentialentry.CredentialEntryValidator.validateCredentialEntry(new UnknownCredentialEntry()));
    }


    public class CredentialEntryRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.course.chronicle.records.CredentialEntryRecord {

        
        protected CredentialEntryRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
