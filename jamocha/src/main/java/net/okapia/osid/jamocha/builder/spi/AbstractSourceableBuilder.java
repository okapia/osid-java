//
// AbstractSourceableBuilder.java
//
//     Defines a builder for a Sourceable.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Defines the Sourceable builder.
 */

public abstract class AbstractSourceableBuilder<T extends AbstractSourceableBuilder<T>>
    extends AbstractBuilder<T> {

    private final SourceableMiter sourceable;


    /**
     *  Creates a new <code>AbstractSourceableBuilder</code>.
     *
     *  @param sourceable a sourceable miter interface
     *  @throws org.osid.NullArgumentException <code>sourceable</code> is
     *          <code>null</code>
     */

    protected AbstractSourceableBuilder(SourceableMiter sourceable) {
        this.sourceable = sourceable;
        return;
    }


    /**
     *  Sets the provider for this sourceable.
     *
     *  @param provider the name for this sourceable
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>provider</code> is
     *          <code>null</code>
     */
    
    public T provider(net.okapia.osid.provider.Provider provider) {
        this.sourceable.setProvider(provider);
        return (self());
    }


    /**
     *  Sets the provider for this sourceable.
     *
     *  @param provider the name for this sourceable
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>provider</code> is
     *          <code>null</code>
     */
    
    public T provider(org.osid.resource.Resource provider) {
        this.sourceable.setProvider(provider);
        return (self());
    }
    

    /**
     *  Adds an asset to the service branding.
     *
     *  @param asset an asset to add
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */
    
    public T branding(org.osid.repository.Asset asset) {
        this.sourceable.addAssetToBranding(asset);
        return (self());
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>license</code>
     *          is <code>null</code>
     */
    
    public T license(org.osid.locale.DisplayText license) {
        this.sourceable.setLicense(license);
        return (self());
    }
}
