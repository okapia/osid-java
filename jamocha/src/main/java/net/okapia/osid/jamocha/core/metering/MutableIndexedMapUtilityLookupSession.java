//
// MutableIndexedMapUtilityLookupSession
//
//    Implements an Utility lookup service backed by a collection of
//    utilities indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering;


/**
 *  Implements an Utility lookup service backed by a collection of
 *  utilities. The utilities are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some utilities may be compatible
 *  with more types than are indicated through these utility
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of utilities can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapUtilityLookupSession
    extends net.okapia.osid.jamocha.core.metering.spi.AbstractIndexedMapUtilityLookupSession
    implements org.osid.metering.UtilityLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapUtilityLookupSession} with no
     *  utilities.
     */

    public MutableIndexedMapUtilityLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapUtilityLookupSession} with a
     *  single utility.
     *  
     *  @param  utility an single utility
     *  @throws org.osid.NullArgumentException {@code utility}
     *          is {@code null}
     */

    public MutableIndexedMapUtilityLookupSession(org.osid.metering.Utility utility) {
        putUtility(utility);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapUtilityLookupSession} using an
     *  array of utilities.
     *
     *  @param  utilities an array of utilities
     *  @throws org.osid.NullArgumentException {@code utilities}
     *          is {@code null}
     */

    public MutableIndexedMapUtilityLookupSession(org.osid.metering.Utility[] utilities) {
        putUtilities(utilities);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapUtilityLookupSession} using a
     *  collection of utilities.
     *
     *  @param  utilities a collection of utilities
     *  @throws org.osid.NullArgumentException {@code utilities} is
     *          {@code null}
     */

    public MutableIndexedMapUtilityLookupSession(java.util.Collection<? extends org.osid.metering.Utility> utilities) {
        putUtilities(utilities);
        return;
    }
    

    /**
     *  Makes an {@code Utility} available in this session.
     *
     *  @param  utility an utility
     *  @throws org.osid.NullArgumentException {@code utility} is
     *          {@code null}
     */

    @Override
    public void putUtility(org.osid.metering.Utility utility) {
        super.putUtility(utility);
        return;
    }


    /**
     *  Makes an array of utilities available in this session.
     *
     *  @param  utilities an array of utilities
     *  @throws org.osid.NullArgumentException {@code utilities} 
     *          is {@code null}
     */

    @Override
    public void putUtilities(org.osid.metering.Utility[] utilities) {
        super.putUtilities(utilities);
        return;
    }


    /**
     *  Makes collection of utilities available in this session.
     *
     *  @param  utilities a collection of utilities
     *  @throws org.osid.NullArgumentException {@code utility}  is
     *          {@code null}
     */

    @Override
    public void putUtilities(java.util.Collection<? extends org.osid.metering.Utility> utilities) {
        super.putUtilities(utilities);
        return;
    }


    /**
     *  Removes an Utility from this session.
     *
     *  @param utilityId the {@code Id} of the utility
     *  @throws org.osid.NullArgumentException {@code utilityId} is
     *          {@code null}
     */

    @Override
    public void removeUtility(org.osid.id.Id utilityId) {
        super.removeUtility(utilityId);
        return;
    }    
}
