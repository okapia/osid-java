//
// InvariantMapParameterProcessorEnablerLookupSession
//
//    Implements a ParameterProcessorEnabler lookup service backed by a fixed collection of
//    parameterProcessorEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules;


/**
 *  Implements a ParameterProcessorEnabler lookup service backed by a fixed
 *  collection of parameter processor enablers. The parameter processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapParameterProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.configuration.rules.spi.AbstractMapParameterProcessorEnablerLookupSession
    implements org.osid.configuration.rules.ParameterProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorEnablerLookupSession</code> with no
     *  parameter processor enablers.
     *  
     *  @param configuration the configuration
     *  @throws org.osid.NullArgumnetException {@code configuration} is
     *          {@code null}
     */

    public InvariantMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration) {
        setConfiguration(configuration);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorEnablerLookupSession</code> with a single
     *  parameter processor enabler.
     *  
     *  @param configuration the configuration
     *  @param parameterProcessorEnabler a single parameter processor enabler
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessorEnabler} is <code>null</code>
     */

      public InvariantMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                               org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler) {
        this(configuration);
        putParameterProcessorEnabler(parameterProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorEnablerLookupSession</code> using an array
     *  of parameter processor enablers.
     *  
     *  @param configuration the configuration
     *  @param parameterProcessorEnablers an array of parameter processor enablers
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessorEnablers} is <code>null</code>
     */

      public InvariantMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                               org.osid.configuration.rules.ParameterProcessorEnabler[] parameterProcessorEnablers) {
        this(configuration);
        putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorEnablerLookupSession</code> using a
     *  collection of parameter processor enablers.
     *
     *  @param configuration the configuration
     *  @param parameterProcessorEnablers a collection of parameter processor enablers
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessorEnablers} is <code>null</code>
     */

      public InvariantMapParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                               java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablers) {
        this(configuration);
        putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }
}
