//
// AbstractTermLookupSession.java
//
//    A starter implementation framework for providing a Term
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Term
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getTerms(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractTermLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.TermLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Term</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTerms() {
        return (true);
    }


    /**
     *  A complete view of the <code>Term</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTermView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Term</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTermView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include terms in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Term</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Term</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Term</code> and
     *  retained for compatibility.
     *
     *  @param  termId <code>Id</code> of the
     *          <code>Term</code>
     *  @return the term
     *  @throws org.osid.NotFoundException <code>termId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>termId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Term getTerm(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.TermList terms = getTerms()) {
            while (terms.hasNext()) {
                org.osid.course.Term term = terms.getNextTerm();
                if (term.getId().equals(termId)) {
                    return (term);
                }
            }
        } 

        throw new org.osid.NotFoundException(termId + " not found");
    }


    /**
     *  Gets a <code>TermList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  terms specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Terms</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getTerms()</code>.
     *
     *  @param  termIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>termIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByIds(org.osid.id.IdList termIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.Term> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = termIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getTerm(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("term " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.term.LinkedTermList(ret));
    }


    /**
     *  Gets a <code>TermList</code> corresponding to the given
     *  term genus <code>Type</code> which does not include
     *  terms of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getTerms()</code>.
     *
     *  @param  termGenusType a term genus type 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByGenusType(org.osid.type.Type termGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.term.TermGenusFilterList(getTerms(), termGenusType));
    }


    /**
     *  Gets a <code>TermList</code> corresponding to the given
     *  term genus <code>Type</code> and include any additional
     *  terms with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTerms()</code>.
     *
     *  @param  termGenusType a term genus type 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByParentGenusType(org.osid.type.Type termGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTermsByGenusType(termGenusType));
    }


    /**
     *  Gets a <code>TermList</code> containing the given
     *  term record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTerms()</code>.
     *
     *  @param  termRecordType a term record type 
     *  @return the returned <code>Term</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByRecordType(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.term.TermRecordFilterList(getTerms(), termRecordType));
    }


    /**
     *  Gets a <code> TermList </code> where to the given <code> DateTime 
     *  </code> falls within the classes date range inclusive. Terms 
     *  containing the given date are matched. In plenary mode, the returned 
     *  list contains all of the terms specified in the <code> Id </code> 
     *  list, in the order of the list, including duplicates, or an error 
     *  results if an <code> Id </code> in the supplied list is not found or 
     *  inaccessible. Otherwise, inaccessible <code> Terms </code> may be 
     *  omitted from the list including returning a unique set. 
     *
     *  @param  datetime a date 
     *  @return the returned <code> Term </code> list 
     *  @throws org.osid.NullArgumentException <code> datetime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.TermList getTermsByClassesDate(org.osid.calendaring.DateTime datetime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.term.TermFilterList(new ClassesDateFilter(datetime), getTerms()));        
    }


    /**
     *  Gets all <code>Terms</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  terms or an error results. Otherwise, the returned list
     *  may contain only those terms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Terms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.TermList getTerms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the term list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of terms
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.TermList filterTermsOnViews(org.osid.course.TermList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class ClassesDateFilter
        implements net.okapia.osid.jamocha.inline.filter.course.term.TermFilter {

        private final org.osid.calendaring.DateTime datetime;

        
        /**
         *  Constructs a new <code>ClassesDateFilter</code>.
         *
         *  @param datetime the date to filter
         *  @throws org.osid.NullArgumentException <code>date</code>
         *          is <code>null</code>
         */

        public ClassesDateFilter(org.osid.calendaring.DateTime datetime) {
            nullarg(datetime, "datetime");
            this.datetime = datetime;
            return;
        }


        /**
         *  Used by the TermFilterList to filter the term list based
         *  on dates of classes.
         *
         *  @param term the term
         *  @return <code>true</code> to pass the term,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.Term term) {
            if (term.getClassesStart().isGreater(this.datetime)) {
                return (false);
            }

            if (term.getClassesEnd().isLess(this.datetime)) {
                return (false);
            }

            return (true);
        }
    }            
}
