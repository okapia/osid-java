//
// AbstractUnknownJournalEntry.java
//
//     Defines an unknown JournalEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.journaling.journalentry.spi;


/**
 *  Defines an unknown <code>JournalEntry</code>.
 */

public abstract class AbstractUnknownJournalEntry
    extends net.okapia.osid.jamocha.journaling.journalentry.spi.AbstractJournalEntry
    implements org.osid.journaling.JournalEntry {

    protected static final String OBJECT = "osid.journaling.JournalEntry";


    /**
     *  Constructs a new <code>AbstractUnknownJournalEntry</code>.
     */
    
    public AbstractUnknownJournalEntry() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));
        
        setSourceId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(""));
        setVersionId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(""));
        setTimestamp(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setResource(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setAgent(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());

        setBranch(new UnknownBranch(this));

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownJournalEntry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownJournalEntry(boolean optional) {
        this();
        return;
    }


    protected class UnknownBranch
        extends net.okapia.osid.jamocha.journaling.branch.spi.AbstractBranch
        implements org.osid.journaling.Branch {

        protected static final String OBJECT = "osid.journaling.Branch";


        /**
         *  Constructs a new <code>UnknownBranch</code>.
         */

        protected UnknownBranch(org.osid.journaling.JournalEntry entry) {
            setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
            setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
            setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));
            
            setOriginJournalEntry(entry);
            setLatestJournalEntry(entry);
            
            return;
        }
    }
}
