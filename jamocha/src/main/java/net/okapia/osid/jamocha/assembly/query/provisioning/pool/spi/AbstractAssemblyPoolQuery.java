//
// AbstractAssemblyPoolQuery.java
//
//     A PoolQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.pool.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PoolQuery that stores terms.
 */

public abstract class AbstractAssemblyPoolQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.provisioning.PoolQuery,
               org.osid.provisioning.PoolQueryInspector,
               org.osid.provisioning.PoolSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.PoolQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.PoolQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.PoolSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPoolQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPoolQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getBrokerIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        getAssembler().clearTerms(getBrokerIdColumn());
        return;
    }


    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (getAssembler().getIdTerms(getBrokerIdColumn()));
    }


    /**
     *  Orders the results by broker. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBroker(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBrokerColumn(), style);
        return;
    }


    /**
     *  Gets the BrokerId column name.
     *
     * @return the column name
     */

    protected String getBrokerIdColumn() {
        return ("broker_id");
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        getAssembler().clearTerms(getBrokerColumn());
        return;
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Tests if a broker search order is available. 
     *
     *  @return <code> true </code> if a broker search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the broker search order. 
     *
     *  @return the broker search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBrokerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchOrder getBrokerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBrokerSearchOrder() is false");
    }


    /**
     *  Gets the Broker column name.
     *
     * @return the column name
     */

    protected String getBrokerColumn() {
        return ("broker");
    }


    /**
     *  Sets the provisionable <code> Id </code> for this query. 
     *
     *  @param  provisionableId the provisionable <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionableId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionableId(org.osid.id.Id provisionableId, 
                                     boolean match) {
        getAssembler().addIdTerm(getProvisionableIdColumn(), provisionableId, match);
        return;
    }


    /**
     *  Clears the provisionable <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProvisionableIdTerms() {
        getAssembler().clearTerms(getProvisionableIdColumn());
        return;
    }


    /**
     *  Gets the provisionable <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProvisionableIdTerms() {
        return (getAssembler().getIdTerms(getProvisionableIdColumn()));
    }


    /**
     *  Gets the ProvisionableId column name.
     *
     * @return the column name
     */

    protected String getProvisionableIdColumn() {
        return ("provisionable_id");
    }


    /**
     *  Tests if a <code> ProvisionableQuery </code> is available. 
     *
     *  @return <code> true </code> if a provisionable query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> PoolQntry. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the provisionable query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQuery getProvisionableQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionableQuery() is false");
    }


    /**
     *  Matches pools that have any provisionable. 
     *
     *  @param  match <code> true </code> to match pools with any 
     *          provisionable, <code> false </code> to match pools with no 
     *          provisionable 
     */

    @OSID @Override
    public void matchAnyProvisionable(boolean match) {
        getAssembler().addIdWildcardTerm(getProvisionableColumn(), match);
        return;
    }


    /**
     *  Clears the provisionable query terms. 
     */

    @OSID @Override
    public void clearProvisionableTerms() {
        getAssembler().clearTerms(getProvisionableColumn());
        return;
    }


    /**
     *  Gets the provisionable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQueryInspector[] getProvisionableTerms() {
        return (new org.osid.provisioning.ProvisionableQueryInspector[0]);
    }


    /**
     *  Gets the Provisionable column name.
     *
     * @return the column name
     */

    protected String getProvisionableColumn() {
        return ("provisionable");
    }


    /**
     *  Matches pools of the given size inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchSize(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getSizeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches pools with any known size. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySize(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getSizeColumn(), match);
        return;
    }


    /**
     *  Clears the size query terms. 
     */

    @OSID @Override
    public void clearSizeTerms() {
        getAssembler().clearTerms(getSizeColumn());
        return;
    }


    /**
     *  Gets the size query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getSizeTerms() {
        return (getAssembler().getCardinalRangeTerms(getSizeColumn()));
    }


    /**
     *  Orders the results by pool size. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySize(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSizeColumn(), style);
        return;
    }


    /**
     *  Gets the Size column name.
     *
     * @return the column name
     */

    protected String getSizeColumn() {
        return ("size");
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this pool supports the given record
     *  <code>Type</code>.
     *
     *  @param  poolRecordType a pool record type 
     *  @return <code>true</code> if the poolRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolRecordType) {
        for (org.osid.provisioning.records.PoolQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  poolRecordType the pool record type 
     *  @return the pool query record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolQueryRecord getPoolQueryRecord(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.PoolQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(poolRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  poolRecordType the pool record type 
     *  @return the pool query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolQueryInspectorRecord getPoolQueryInspectorRecord(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.PoolQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(poolRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param poolRecordType the pool record type
     *  @return the pool search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolSearchOrderRecord getPoolSearchOrderRecord(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.PoolSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(poolRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this pool. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolQueryRecord the pool query record
     *  @param poolQueryInspectorRecord the pool query inspector
     *         record
     *  @param poolSearchOrderRecord the pool search order record
     *  @param poolRecordType pool record type
     *  @throws org.osid.NullArgumentException
     *          <code>poolQueryRecord</code>,
     *          <code>poolQueryInspectorRecord</code>,
     *          <code>poolSearchOrderRecord</code> or
     *          <code>poolRecordTypepool</code> is
     *          <code>null</code>
     */
            
    protected void addPoolRecords(org.osid.provisioning.records.PoolQueryRecord poolQueryRecord, 
                                      org.osid.provisioning.records.PoolQueryInspectorRecord poolQueryInspectorRecord, 
                                      org.osid.provisioning.records.PoolSearchOrderRecord poolSearchOrderRecord, 
                                      org.osid.type.Type poolRecordType) {

        addRecordType(poolRecordType);

        nullarg(poolQueryRecord, "pool query record");
        nullarg(poolQueryInspectorRecord, "pool query inspector record");
        nullarg(poolSearchOrderRecord, "pool search odrer record");

        this.queryRecords.add(poolQueryRecord);
        this.queryInspectorRecords.add(poolQueryInspectorRecord);
        this.searchOrderRecords.add(poolSearchOrderRecord);
        
        return;
    }
}
