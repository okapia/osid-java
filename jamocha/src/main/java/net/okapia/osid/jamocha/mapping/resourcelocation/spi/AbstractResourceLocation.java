//
// AbstractResourceLocation.java
//
//     Defines a ResourceLocation.
//
//
// Tom Coppeto
// Okapia
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.resourcelocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ResourceLocation</code>.
 */

public abstract class AbstractResourceLocation
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendium
    implements org.osid.mapping.ResourceLocation {

    private org.osid.resource.Resource resource;
    private org.osid.mapping.Location location;
    private org.osid.mapping.Coordinate coordinate;

    private final java.util.Collection<org.osid.mapping.records.ResourceLocationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the resource on the route. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource on the route. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {
        
        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource the resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this resource has a known location. 
     *
     *  @return <code> true </code> if a location is known, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        if (this.location == null) {
            return (false);
        } else { 
            return (true);
        }
    }


    /**
     *  Gets the location <code> Id </code> of the resource. 
     *
     *  @return the location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location.getId());
    }


    /**
     *  Gets the location of the resource. 
     *
     *  @return the location of the resource 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location);
    }


    /**
     *  Sets the location.
     *
     *  @param location the location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    protected void setLocation(org.osid.mapping.Location location) {
        nullarg(location, "location");
        this.location = location;
        return;
    }


    /**
     *  Tests if this resource has a known coordinate. 
     *
     *  @return <code> true </code> if a coordinate is known, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCoordinate() {
        if (this.coordinate == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the coordinate of the resource. 
     *
     *  @return the coordinate of the resource 
     *  @throws org.osid.IllegalStateException <code> hasCoordinate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinate() {
        if (!hasCoordinate()) {
            throw new org.osid.IllegalStateException("hasCoordinate() is false");
        }

        return (this.coordinate);
    }


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate the coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    protected void setCoordinate(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "coordinate");
        this.coordinate = coordinate;
        return;
    }

    
    /**
     *  Tests if this location supports the given record
     *  <code>Type</code>.
     *
     *  @param  resourceLocationRecordType a location record type 
     *  @return <code>true</code> if the resourceLocationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resourceLocationRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceLocationRecordType) {
        for (org.osid.mapping.records.ResourceLocationRecord record : this.records) {
            if (record.implementsRecordType(resourceLocationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the map record corresponding to the given <code>
     *  ResourceLocation </code> record <code> Type. </code> This
     *  method is used to retrieve an object implementing the
     *  requested record. The <code> resourceLocationRecordType
     *  </code> may be the <code> Type </code> returned in <code>
     *  getRecordTypes() </code> or any of its parents in a <code>
     *  Type </code> hierarchy where <code>
     *  hasRecordType(resourceLocationRecordType) </code> is <code>
     *  true </code> .
     *
     *  @param  resourceLocationRecordType the type of resource location 
     *          record to retrieve 
     *  @return the resource location record 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceLocationRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(resourceLocationRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.records.ResourceLocationRecord getResourceLocationRecord(org.osid.type.Type resourceLocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.ResourceLocationRecord record : this.records) {
            if (record.implementsRecordType(resourceLocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceLocationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource location. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceLocationRecord the resource location record
     *  @param resourceLocationRecordType resource location record type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceLocationRecord</code> or
     *          <code>resourceLocationRecordTypelocation</code> is
     *          <code>null</code>
     */
            
    protected void addResourceLocationRecord(org.osid.mapping.records.ResourceLocationRecord resourceLocationRecord, 
                                             org.osid.type.Type resourceLocationRecordType) {

        nullarg(resourceLocationRecord, "resource location record");
        addRecordType(resourceLocationRecordType);
        this.records.add(resourceLocationRecord);
        
        return;
    }    
}
