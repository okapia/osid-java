//
// AbstractIndexedMapParticipantLookupSession.java
//
//    A simple framework for providing a Participant lookup service
//    backed by a fixed collection of participants with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Participant lookup service backed by a
 *  fixed collection of participants. The participants are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some participants may be compatible
 *  with more types than are indicated through these participant
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Participants</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapParticipantLookupSession
    extends AbstractMapParticipantLookupSession
    implements org.osid.offering.ParticipantLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.Participant> participantsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Participant>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.Participant> participantsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Participant>());


    /**
     *  Makes a <code>Participant</code> available in this session.
     *
     *  @param  participant a participant
     *  @throws org.osid.NullArgumentException <code>participant<code> is
     *          <code>null</code>
     */

    @Override
    protected void putParticipant(org.osid.offering.Participant participant) {
        super.putParticipant(participant);

        this.participantsByGenus.put(participant.getGenusType(), participant);
        
        try (org.osid.type.TypeList types = participant.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.participantsByRecord.put(types.getNextType(), participant);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of participants available in this session.
     *
     *  @param  participants an array of participants
     *  @throws org.osid.NullArgumentException <code>participants<code>
     *          is <code>null</code>
     */

    @Override
    protected void putParticipants(org.osid.offering.Participant[] participants) {
        for (org.osid.offering.Participant participant : participants) {
            putParticipant(participant);
        }

        return;
    }


    /**
     *  Makes a collection of participants available in this session.
     *
     *  @param  participants a collection of participants
     *  @throws org.osid.NullArgumentException <code>participants<code>
     *          is <code>null</code>
     */

    @Override
    protected void putParticipants(java.util.Collection<? extends org.osid.offering.Participant> participants) {
        for (org.osid.offering.Participant participant : participants) {
            putParticipant(participant);
        }

        return;
    }


    /**
     *  Removes a participant from this session.
     *
     *  @param participantId the <code>Id</code> of the participant
     *  @throws org.osid.NullArgumentException <code>participantId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeParticipant(org.osid.id.Id participantId) {
        org.osid.offering.Participant participant;
        try {
            participant = getParticipant(participantId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.participantsByGenus.remove(participant.getGenusType());

        try (org.osid.type.TypeList types = participant.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.participantsByRecord.remove(types.getNextType(), participant);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeParticipant(participantId);
        return;
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  participant genus <code>Type</code> which does not include
     *  participants of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known participants or an error results. Otherwise,
     *  the returned list may contain only those participants that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.participant.ArrayParticipantList(this.participantsByGenus.get(participantGenusType)));
    }


    /**
     *  Gets a <code>ParticipantList</code> containing the given
     *  participant record <code>Type</code>. In plenary mode, the
     *  returned list contains all known participants or an error
     *  results. Otherwise, the returned list may contain only those
     *  participants that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  participantRecordType a participant record type 
     *  @return the returned <code>participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByRecordType(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.participant.ArrayParticipantList(this.participantsByRecord.get(participantRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.participantsByGenus.clear();
        this.participantsByRecord.clear();

        super.close();

        return;
    }
}
