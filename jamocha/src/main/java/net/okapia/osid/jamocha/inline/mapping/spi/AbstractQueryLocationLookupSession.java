//
// AbstractQueryLocationLookupSession.java
//
//    An inline adapter that maps a LocationLookupSession to
//    a LocationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a LocationLookupSession to
 *  a LocationQuerySession.
 */

public abstract class AbstractQueryLocationLookupSession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {

    private final org.osid.mapping.LocationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryLocationLookupSession.
     *
     *  @param querySession the underlying location query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryLocationLookupSession(org.osid.mapping.LocationQuerySession querySession) {
        nullarg(querySession, "location query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Map</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform <code>Location</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLocations() {
        return (this.session.canSearchLocations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include locations in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    
     
    /**
     *  Gets the <code>Location</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Location</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Location</code> and
     *  retained for compatibility.
     *
     *  @param  locationId <code>Id</code> of the
     *          <code>Location</code>
     *  @return the location
     *  @throws org.osid.NotFoundException <code>locationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>locationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.LocationQuery query = getQuery();
        query.matchId(locationId, true);
        org.osid.mapping.LocationList locations = this.session.getLocationsByQuery(query);
        if (locations.hasNext()) {
            return (locations.getNextLocation());
        } 
        
        throw new org.osid.NotFoundException(locationId + " not found");
    }


    /**
     *  Gets a <code>LocationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  locations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Locations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  locationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>locationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByIds(org.osid.id.IdList locationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.LocationQuery query = getQuery();

        try (org.osid.id.IdList ids = locationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getLocationsByQuery(query));
    }


    /**
     *  Gets a <code>LocationList</code> corresponding to the given
     *  location genus <code>Type</code> which does not include
     *  locations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationGenusType a location genus type 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByGenusType(org.osid.type.Type locationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.LocationQuery query = getQuery();
        query.matchGenusType(locationGenusType, true);
        return (this.session.getLocationsByQuery(query));
    }


    /**
     *  Gets a <code>LocationList</code> corresponding to the given
     *  location genus <code>Type</code> and include any additional
     *  locations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationGenusType a location genus type 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByParentGenusType(org.osid.type.Type locationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.LocationQuery query = getQuery();
        query.matchParentGenusType(locationGenusType, true);
        return (this.session.getLocationsByQuery(query));
    }


    /**
     *  Gets a <code>LocationList</code> containing the given
     *  location record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationRecordType a location record type 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByRecordType(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.LocationQuery query = getQuery();
        query.matchRecordType(locationRecordType, true);
        return (this.session.getLocationsByQuery(query));
    }

    
    /**
     *  Gets all <code>Locations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Locations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.LocationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getLocationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.mapping.LocationQuery getQuery() {
        org.osid.mapping.LocationQuery query = this.session.getLocationQuery();
        return (query);
    }
}
