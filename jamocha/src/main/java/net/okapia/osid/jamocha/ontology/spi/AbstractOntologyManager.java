//
// AbstractOntologyManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOntologyManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.ontology.OntologyManager,
               org.osid.ontology.OntologyProxyManager {

    private final Types subjectRecordTypes                 = new TypeRefSet();
    private final Types subjectSearchRecordTypes           = new TypeRefSet();

    private final Types relevancyRecordTypes               = new TypeRefSet();
    private final Types relevancySearchRecordTypes         = new TypeRefSet();

    private final Types ontologyRecordTypes                = new TypeRefSet();
    private final Types ontologySearchRecordTypes          = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractOntologyManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOntologyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a subject lookup service is supported. a subject lookup 
     *  service defines methods to access subjects. 
     *
     *  @return true if subject lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectLookup() {
        return (false);
    }


    /**
     *  Tests if a subject query service is supported. 
     *
     *  @return <code> true </code> if subject query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (false);
    }


    /**
     *  Tests if a subject search service is supported. 
     *
     *  @return <code> true </code> if subject search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectSearch() {
        return (false);
    }


    /**
     *  Tests if a subject administrative service is supported. 
     *
     *  @return <code> true </code> if subject admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectAdmin() {
        return (false);
    }


    /**
     *  Tests if subject notification is supported. Messages may be sent when 
     *  subjects are created, modified, or deleted. 
     *
     *  @return <code> true </code> if subject notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectNotification() {
        return (false);
    }


    /**
     *  Tests if a subject hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a subject hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectHierarchy() {
        return (false);
    }


    /**
     *  Tests if subject hierarchy design is supported. 
     *
     *  @return <code> true </code> if a subject hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a subject to ontology lookup session is available. 
     *
     *  @return <code> true </code> if subject ontology lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectOntology() {
        return (false);
    }


    /**
     *  Tests if a subject to ontology assignment session is available. 
     *
     *  @return <code> true </code> if subject ontology assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectOntologyAssignment() {
        return (false);
    }


    /**
     *  Tests if a subject smart ontology session is available. 
     *
     *  @return <code> true </code> if subject smart ontology session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectSmartOntology() {
        return (false);
    }


    /**
     *  Tests if a subject relevancy lookup service is supported. 
     *
     *  @return <code> true </code> if relevancy lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyLookup() {
        return (false);
    }


    /**
     *  Tests if a relevancy query service is supported. 
     *
     *  @return <code> true </code> if relevancy query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyQuery() {
        return (false);
    }


    /**
     *  Tests if a relevancy search service is supported. 
     *
     *  @return <code> true </code> if relevancy search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancySearch() {
        return (false);
    }


    /**
     *  Tests if a relevancy administrative service is supported. 
     *
     *  @return <code> true </code> if relevancy admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyAdmin() {
        return (false);
    }


    /**
     *  Tests if relevancy notification is supported. Messages may be sent 
     *  when subject relevancies are created, modified, or deleted. 
     *
     *  @return <code> true </code> if relevancy notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyNotification() {
        return (false);
    }


    /**
     *  Tests if an ontology lookup service is supported. 
     *
     *  @return <code> true </code> if ontology lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyLookup() {
        return (false);
    }


    /**
     *  Tests if a relevancy to ontology lookup session is available. 
     *
     *  @return <code> true </code> if relevancy ontology lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyOntology() {
        return (false);
    }


    /**
     *  Tests if a relevancy to ontology assignment session is available. 
     *
     *  @return <code> true </code> if relevancy ontology assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyOntologyAssignment() {
        return (false);
    }


    /**
     *  Tests if a relevancy smart ontology session is available. 
     *
     *  @return <code> true </code> if relevancy smart ontology session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancySmartOntology() {
        return (false);
    }


    /**
     *  Tests if an ontology query service is supported. 
     *
     *  @return <code> true </code> if ontology query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyQuery() {
        return (false);
    }


    /**
     *  Tests if an ontology search service is supported. 
     *
     *  @return <code> true </code> if ontology search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologySearch() {
        return (false);
    }


    /**
     *  Tests if an ontology administrative service is supported. 
     *
     *  @return <code> true </code> if ontology admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyAdmin() {
        return (false);
    }


    /**
     *  Tests if ontology notification is supported. Messages may be sent when 
     *  ontologies are created, modified, or deleted. 
     *
     *  @return <code> true </code> if ontology notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyNotification() {
        return (false);
    }


    /**
     *  Tests if an ontology hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an ontology hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyHierarchy() {
        return (false);
    }


    /**
     *  Tests if ontology hierarchy design is supported. 
     *
     *  @return <code> true </code> if an ontology hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if <code> Ids </code> can be asssigned to ontologies. 
     *
     *  @return <code> true </code> if an ontology hassignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyAssignment() {
        return (false);
    }


    /**
     *  Tests if an ontology batch service is supported. 
     *
     *  @return <code> true </code> if ontology batch is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyBatch() {
        return (false);
    }


    /**
     *  Tests if an ontology rules service is supported. 
     *
     *  @return <code> true </code> if ontology rules is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Subject </code> record types. 
     *
     *  @return a list containing the supported <code> Subject </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubjectRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.subjectRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Subject </code> record type is supported. 
     *
     *  @param  subjectRecordType a <code> Type </code> indicating a <code> 
     *          Subject </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> subjectRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubjectRecordType(org.osid.type.Type subjectRecordType) {
        return (this.subjectRecordTypes.contains(subjectRecordType));
    }


    /**
     *  Adds support for a subject record type.
     *
     *  @param subjectRecordType a subject record type
     *  @throws org.osid.NullArgumentException
     *  <code>subjectRecordType</code> is <code>null</code>
     */

    protected void addSubjectRecordType(org.osid.type.Type subjectRecordType) {
        this.subjectRecordTypes.add(subjectRecordType);
        return;
    }


    /**
     *  Removes support for a subject record type.
     *
     *  @param subjectRecordType a subject record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>subjectRecordType</code> is <code>null</code>
     */

    protected void removeSubjectRecordType(org.osid.type.Type subjectRecordType) {
        this.subjectRecordTypes.remove(subjectRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Subject </code> search record types. 
     *
     *  @return a list containing the supported <code> Subject </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubjectSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.subjectSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Subject </code> search record type is 
     *  supported. 
     *
     *  @param  subjectSearchRecordType a <code> Type </code> indicating a 
     *          <code> Subject </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> subjectSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubjectSearchRecordType(org.osid.type.Type subjectSearchRecordType) {
        return (this.subjectSearchRecordTypes.contains(subjectSearchRecordType));
    }


    /**
     *  Adds support for a subject search record type.
     *
     *  @param subjectSearchRecordType a subject search record type
     *  @throws org.osid.NullArgumentException
     *  <code>subjectSearchRecordType</code> is <code>null</code>
     */

    protected void addSubjectSearchRecordType(org.osid.type.Type subjectSearchRecordType) {
        this.subjectSearchRecordTypes.add(subjectSearchRecordType);
        return;
    }


    /**
     *  Removes support for a subject search record type.
     *
     *  @param subjectSearchRecordType a subject search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>subjectSearchRecordType</code> is <code>null</code>
     */

    protected void removeSubjectSearchRecordType(org.osid.type.Type subjectSearchRecordType) {
        this.subjectSearchRecordTypes.remove(subjectSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Relevancy </code> record types. 
     *
     *  @return a list containing the supported <code> Relevancy </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relevancyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Relevancy </code> record type is supported. 
     *
     *  @param  relevancyRecordType a <code> Type </code> indicating a <code> 
     *          Relevnacy </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> relevancyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelevancyRecordType(org.osid.type.Type relevancyRecordType) {
        return (this.relevancyRecordTypes.contains(relevancyRecordType));
    }


    /**
     *  Adds support for a relevancy record type.
     *
     *  @param relevancyRecordType a relevancy record type
     *  @throws org.osid.NullArgumentException
     *  <code>relevancyRecordType</code> is <code>null</code>
     */

    protected void addRelevancyRecordType(org.osid.type.Type relevancyRecordType) {
        this.relevancyRecordTypes.add(relevancyRecordType);
        return;
    }


    /**
     *  Removes support for a relevancy record type.
     *
     *  @param relevancyRecordType a relevancy record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relevancyRecordType</code> is <code>null</code>
     */

    protected void removeRelevancyRecordType(org.osid.type.Type relevancyRecordType) {
        this.relevancyRecordTypes.remove(relevancyRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Relevancy </code> search record types. 
     *
     *  @return a list containing the supported <code> Relevancy </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relevancySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Relevancy </code> search record type is 
     *  supported. 
     *
     *  @param  relevancySearchRecordType a <code> Type </code> indicating a 
     *          <code> Relevancy </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelevancySearchRecordType(org.osid.type.Type relevancySearchRecordType) {
        return (this.relevancySearchRecordTypes.contains(relevancySearchRecordType));
    }


    /**
     *  Adds support for a relevancy search record type.
     *
     *  @param relevancySearchRecordType a relevancy search record type
     *  @throws org.osid.NullArgumentException
     *  <code>relevancySearchRecordType</code> is <code>null</code>
     */

    protected void addRelevancySearchRecordType(org.osid.type.Type relevancySearchRecordType) {
        this.relevancySearchRecordTypes.add(relevancySearchRecordType);
        return;
    }


    /**
     *  Removes support for a relevancy search record type.
     *
     *  @param relevancySearchRecordType a relevancy search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relevancySearchRecordType</code> is <code>null</code>
     */

    protected void removeRelevancySearchRecordType(org.osid.type.Type relevancySearchRecordType) {
        this.relevancySearchRecordTypes.remove(relevancySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Ontology </code> record types. 
     *
     *  @return a list containing the supported <code> Ontology </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOntologyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ontologyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Ontology </code> record type is supported. 
     *
     *  @param  ontologyRecordType a <code> Type </code> indicating an <code> 
     *          Ontology </code> type 
     *  @return <code> true </code> if the given ontology record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ontologyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOntologyRecordType(org.osid.type.Type ontologyRecordType) {
        return (this.ontologyRecordTypes.contains(ontologyRecordType));
    }


    /**
     *  Adds support for an ontology record type.
     *
     *  @param ontologyRecordType an ontology record type
     *  @throws org.osid.NullArgumentException
     *  <code>ontologyRecordType</code> is <code>null</code>
     */

    protected void addOntologyRecordType(org.osid.type.Type ontologyRecordType) {
        this.ontologyRecordTypes.add(ontologyRecordType);
        return;
    }


    /**
     *  Removes support for an ontology record type.
     *
     *  @param ontologyRecordType an ontology record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ontologyRecordType</code> is <code>null</code>
     */

    protected void removeOntologyRecordType(org.osid.type.Type ontologyRecordType) {
        this.ontologyRecordTypes.remove(ontologyRecordType);
        return;
    }


    /**
     *  Gets the supported ontology search record types. 
     *
     *  @return a list containing the supported <code> Ontology </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOntologySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ontologySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given ontology search record type is supported. 
     *
     *  @param  ontologySearchRecordType a <code> Type </code> indicating an 
     *          <code> Ontology </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ontologySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOntologySearchRecordType(org.osid.type.Type ontologySearchRecordType) {
        return (this.ontologySearchRecordTypes.contains(ontologySearchRecordType));
    }


    /**
     *  Adds support for an ontology search record type.
     *
     *  @param ontologySearchRecordType an ontology search record type
     *  @throws org.osid.NullArgumentException
     *  <code>ontologySearchRecordType</code> is <code>null</code>
     */

    protected void addOntologySearchRecordType(org.osid.type.Type ontologySearchRecordType) {
        this.ontologySearchRecordTypes.add(ontologySearchRecordType);
        return;
    }


    /**
     *  Removes support for an ontology search record type.
     *
     *  @param ontologySearchRecordType an ontology search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ontologySearchRecordType</code> is <code>null</code>
     */

    protected void removeOntologySearchRecordType(org.osid.type.Type ontologySearchRecordType) {
        this.ontologySearchRecordTypes.remove(ontologySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service. 
     *
     *  @return a <code> SubjectLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a SubjectLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service for the given <code> Id </code> assigned using the <code> 
     *  OntologyAssignmentSession. </code> 
     *
     *  @param  id an <code> Id </code> 
     *  @return <code> a SubjectLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> id </code> not found 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForId(org.osid.id.Id id)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectLookupSessionForId not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service for the given <code> Id </code> assigned using the <code> 
     *  OntologyAssignmentSession. </code> 
     *
     *  @param  id an <code> Id </code> 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> id </code> not found 
     *  @throws org.osid.NullArgumentException <code> id </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForId(org.osid.id.Id id, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectLookupSessionForId not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject query 
     *  service. 
     *
     *  @return a <code> SubjectQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuerySession getSubjectQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjecQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuerySession getSubjectQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject query 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return a <code> SubjectQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuerySession getSubjectQuerySessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectQuerySessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject query 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuerySession getSubjectQuerySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectQuerySessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject search 
     *  service. 
     *
     *  @return a <code> SubjectSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchSession getSubjectSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchSession getSubjectSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject search 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a SubjectSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchSession getSubjectSearchSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectSearchSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject search 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchSession getSubjectSearchSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectSearchSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  administration service. 
     *
     *  @return a <code> SubjectAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectAdminSession getSubjectAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectAdminSession getSubjectAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject admin 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a SubjectAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectAdminSession getSubjectAdminSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject admin 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectAdminSession getSubjectAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to subject 
     *  changes. 
     *
     *  @param  subjectReceiver the subject receiver 
     *  @return a <code> SubjectNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> subjectReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectNotificationSession getSubjectNotificationSession(org.osid.ontology.SubjectReceiver subjectReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to subject 
     *  changes. 
     *
     *  @param  subjectReceiver the subject receiver 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> subjectReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectNotificationSession getSubjectNotificationSession(org.osid.ontology.SubjectReceiver subjectReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  notification service for the given ontology. 
     *
     *  @param  subjectReceiver the subject receiver 
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a SubjectNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> subjectReceiver </code> 
     *          or <code> ontologyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectNotificationSession getSubjectNotificationSessionForOntology(org.osid.ontology.SubjectReceiver subjectReceiver, 
                                                                                                 org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectNotificationSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  notification service for the given ontology. 
     *
     *  @param  subjectReceiver the subject receiver 
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> subjectReceiver, 
     *          ontologyId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectNotificationSession getSubjectNotificationSessionForOntology(org.osid.ontology.SubjectReceiver subjectReceiver, 
                                                                                                 org.osid.id.Id ontologyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectNotificationSessionForOntology not implemented");
    }


    /**
     *  Gets the session traversing subject hierarchies. 
     *
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing subject hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  heirarchy traversal service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectHierarchySessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  heirarchy traversal service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectHierarchySessionForOntology not implemented");
    }


    /**
     *  Gets the hierarchy session for the given <code> Id </code> assigned 
     *  using the <code> OntologyAssignmentSession. </code> 
     *
     *  @param  id an <code> Id </code> 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> id </code> not found 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySessionForId(org.osid.id.Id id)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectHierarchySessionForId not implemented");
    }


    /**
     *  Gets the hierarchy session for the given <code> Id </code> assigned 
     *  using the <code> OntologyAssignmentSession. </code> 
     *
     *  @param  id an <code> Id </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> id </code> not found 
     *  @throws org.osid.NullArgumentException <code> id </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySessionForId(org.osid.id.Id id, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectHierarchySessionForId not implemented");
    }


    /**
     *  Gets the session designing subject hierarchies. 
     *
     *  @return a <code> SubjectHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchyDesignSession getSubjectHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing subject hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchyDesignSession getSubjectHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  heirarchy design service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return a <code> SubjectHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchyDesignSession getSubjectHierarchyDesignSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectHierarchyDesignSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  heirarchy design service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchyDesignSession getSubjectHierarchyDesignSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectHierarchyDesignSessionForOntology not implemented");
    }


    /**
     *  Gets the session retrieving subject ontology mappings. 
     *
     *  @return a <code> SubjectOntologySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologySession getSubjectOntologySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectOntologySession not implemented");
    }


    /**
     *  Gets the session retrieving subject ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectOntologySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologySession getSubjectOntologySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectOntologySession not implemented");
    }


    /**
     *  Gets the session managing subject ontology mappings. 
     *
     *  @return a <code> SubjectOntologyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologyAssignmentSession getSubjectOntologyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the session managing subject ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectOntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologyAssignmentSession getSubjectOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the session managing subject smart ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return a <code> SubjectSmartOntologySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectSmartOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSmartOntologySession getSubjectSmartOntologySession(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getSubjectSmartOntologySession not implemented");
    }


    /**
     *  Gets the session managing subject smart ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectSmartOntologySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectSmartOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologySession getSubjectSmartOntologySession(org.osid.id.Id ontologyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getSubjectSmartOntologySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  lookup service. 
     *
     *  @return a <code> RelevancyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  lookup service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a RelevancyLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  lookup service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  query service. 
     *
     *  @return a <code> RelevancyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  query service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a RelevancyQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyQuerySessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  query service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyQuerySessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  search service. 
     *
     *  @return a <code> RelevancySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySearchSession getRelevancySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevanctSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySearchSession getRelevancySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  search service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a RelevancySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySearchSession getRelevancySearchSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancySearchSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  search service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySearchSession getRelevancySearchSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancySearchSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  administration service. 
     *
     *  @return a <code> RelvancyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyAdminSession getRelevancyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelvancyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyAdminSession getRelevancyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  admin service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a RelevancyAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyAdminSession getRelevancyAdminSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  admin service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyAdminSession getRelevancyAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  relevancy changes. 
     *
     *  @param  relevancyReceiver the relevancy receiver 
     *  @return <code> a RelevancyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relevancyReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyNotificationSession getRelevancyNotificationSession(org.osid.ontology.RelevancyReceiver relevancyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  relevancy changes. 
     *
     *  @param  relevancyReceiver the relevancy receiver 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relevancyReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyNotificationSession getRelevancyNotificationSession(org.osid.ontology.RelevancyReceiver relevancyReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  notification service for the given ontology. 
     *
     *  @param  relevancyReceiver the subject receiver 
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return <code> a RelevancyNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> relevancyReceiver 
     *          </code> or <code> ontologyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyNotificationSession getRelevancyNotificationSessionForOntology(org.osid.ontology.RelevancyReceiver relevancyReceiver, 
                                                                                                     org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyNotificationSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  notification service for the given ontology. 
     *
     *  @param  relevancyReceiver the subject receiver 
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> relevancyReceiver, 
     *          ontologyId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyNotificationSession getRelevancyNotificationSessionForOntology(org.osid.ontology.RelevancyReceiver relevancyReceiver, 
                                                                                                     org.osid.id.Id ontologyId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyNotificationSessionForOntology not implemented");
    }


    /**
     *  Gets the session retrieving relevancy ontology mappings. 
     *
     *  @return a <code> RelevancyOntologySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyOntologySession getRelevancyOntologySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyOntologySession not implemented");
    }


    /**
     *  Gets the session retrieving relevancy ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyOntologySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyOntologySession getRelevancyOntologySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyOntologySession not implemented");
    }


    /**
     *  Gets the session managing relevancy ontology mappings. 
     *
     *  @return a <code> RelevancyOntologyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyOntologyAssignmentSession getRelevancyOntologyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancyOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the session managing relevancy ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyOntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyOntologyAssignmentSession getRelevancyOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancyOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the session managing relevancy smart ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @return a <code> RelevancySmartOntologySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySmartOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySmartOntologySession getRelevancySmartOntologySession(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getRelevancySmartOntologySession not implemented");
    }


    /**
     *  Gets the session managing relevancy smart ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancySmartOntologySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySmartOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySmartOntologySession getRelevancySmartOntologySession(org.osid.id.Id ontologyId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getRelevancySmartOntologySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology lookup service. 
     *
     *  @return an <code> OntologyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyLookupSession getOntologyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyLookupSession getOntologyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology query service. 
     *
     *  @return an <code> OntologyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuerySession getOntologyQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuerySession getOntologyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology search service. 
     *
     *  @return an <code> OntologySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologySearch() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologySearchSession getOntologySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologySearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologySearch() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologySearchSession getOntologySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologySearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology administration 
     *  service. 
     *
     *  @return an <code> OntologyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyAdminSession getOntologyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyAdminSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the ontology administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyAdminSession getOntologyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to ontology 
     *  service changes. 
     *
     *  @param  ontologyReceiver the ontology receiver 
     *  @return an <code> OntologyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyNotificationSession getOntologyNotificationSession(org.osid.ontology.OntologyReceiver ontologyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to ontology 
     *  service changes. 
     *
     *  @param  ontologyReceiver the ontology receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OntologyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyNotificationSession getOntologyNotificationSession(org.osid.ontology.OntologyReceiver ontologyReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing ontology hierarchies. 
     *
     *  @return an <code> OntologyHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyHierarchySession getOntologyHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing ontology hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyHierarchySession getOntologyHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyHierarchySession not implemented");
    }


    /**
     *  Gets the session designing ontology hierarchies. 
     *
     *  @return an <code> OntologyHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyHierarchyDesignSession getOntologyHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing ontology hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyHierarchyDesignSession getOntologyHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session to assign <code> Ids </code> to ontologies. 
     *
     *  @return an <code> OntologyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyAssignmentSession getOntologyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the session to assign <code> Ids </code> to ontologies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyAssignmentSession getOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the ontology batch service. 
     *
     *  @return an <code> OntologyBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.OntologyBatchManager getOntologyBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyBatchManager not implemented");
    }


    /**
     *  Gets the ontology batch service. 
     *
     *  @return an <code> OntologyBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.OntologyBatchProxyManager getOntologyBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyBatchProxyManager not implemented");
    }


    /**
     *  Gets the ontology rules service. 
     *
     *  @return an <code> OntologyRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyRules() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.OntologyRulesManager getOntologyRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyManager.getOntologyRulesManager not implemented");
    }


    /**
     *  Gets the ontology rules service. 
     *
     *  @return an <code> OntologyRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyRules() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.OntologyRulesProxyManager getOntologyRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.OntologyProxyManager.getOntologyRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.subjectRecordTypes.clear();
        this.subjectRecordTypes.clear();

        this.subjectSearchRecordTypes.clear();
        this.subjectSearchRecordTypes.clear();

        this.relevancyRecordTypes.clear();
        this.relevancyRecordTypes.clear();

        this.relevancySearchRecordTypes.clear();
        this.relevancySearchRecordTypes.clear();

        this.ontologyRecordTypes.clear();
        this.ontologyRecordTypes.clear();

        this.ontologySearchRecordTypes.clear();
        this.ontologySearchRecordTypes.clear();

        return;
    }
}
