//
// MutableIndexedMapBallotConstrainerEnablerLookupSession
//
//    Implements a BallotConstrainerEnabler lookup service backed by a collection of
//    ballotConstrainerEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a BallotConstrainerEnabler lookup service backed by a collection of
 *  ballot constrainer enablers. The ballot constrainer enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some ballot constrainer enablers may be compatible
 *  with more types than are indicated through these ballot constrainer enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of ballot constrainer enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapBallotConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractIndexedMapBallotConstrainerEnablerLookupSession
    implements org.osid.voting.rules.BallotConstrainerEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapBallotConstrainerEnablerLookupSession} with no ballot constrainer enablers.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotConstrainerEnablerLookupSession} with a
     *  single ballot constrainer enabler.
     *  
     *  @param polls the polls
     *  @param  ballotConstrainerEnabler a single ballotConstrainerEnabler
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballotConstrainerEnabler} is {@code null}
     */

    public MutableIndexedMapBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.BallotConstrainerEnabler ballotConstrainerEnabler) {
        this(polls);
        putBallotConstrainerEnabler(ballotConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotConstrainerEnablerLookupSession} using an
     *  array of ballot constrainer enablers.
     *
     *  @param polls the polls
     *  @param  ballotConstrainerEnablers an array of ballot constrainer enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballotConstrainerEnablers} is {@code null}
     */

    public MutableIndexedMapBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.BallotConstrainerEnabler[] ballotConstrainerEnablers) {
        this(polls);
        putBallotConstrainerEnablers(ballotConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotConstrainerEnablerLookupSession} using a
     *  collection of ballot constrainer enablers.
     *
     *  @param polls the polls
     *  @param  ballotConstrainerEnablers a collection of ballot constrainer enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballotConstrainerEnablers} is {@code null}
     */

    public MutableIndexedMapBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.rules.BallotConstrainerEnabler> ballotConstrainerEnablers) {

        this(polls);
        putBallotConstrainerEnablers(ballotConstrainerEnablers);
        return;
    }
    

    /**
     *  Makes a {@code BallotConstrainerEnabler} available in this session.
     *
     *  @param  ballotConstrainerEnabler a ballot constrainer enabler
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putBallotConstrainerEnabler(org.osid.voting.rules.BallotConstrainerEnabler ballotConstrainerEnabler) {
        super.putBallotConstrainerEnabler(ballotConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of ballot constrainer enablers available in this session.
     *
     *  @param  ballotConstrainerEnablers an array of ballot constrainer enablers
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putBallotConstrainerEnablers(org.osid.voting.rules.BallotConstrainerEnabler[] ballotConstrainerEnablers) {
        super.putBallotConstrainerEnablers(ballotConstrainerEnablers);
        return;
    }


    /**
     *  Makes collection of ballot constrainer enablers available in this session.
     *
     *  @param  ballotConstrainerEnablers a collection of ballot constrainer enablers
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putBallotConstrainerEnablers(java.util.Collection<? extends org.osid.voting.rules.BallotConstrainerEnabler> ballotConstrainerEnablers) {
        super.putBallotConstrainerEnablers(ballotConstrainerEnablers);
        return;
    }


    /**
     *  Removes a BallotConstrainerEnabler from this session.
     *
     *  @param ballotConstrainerEnablerId the {@code Id} of the ballot constrainer enabler
     *  @throws org.osid.NullArgumentException {@code ballotConstrainerEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBallotConstrainerEnabler(org.osid.id.Id ballotConstrainerEnablerId) {
        super.removeBallotConstrainerEnabler(ballotConstrainerEnablerId);
        return;
    }    
}
