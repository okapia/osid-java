//
// MutableMapGradebookColumnLookupSession
//
//    Implements a GradebookColumn lookup service backed by a collection of
//    gradebookColumns that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading;


/**
 *  Implements a GradebookColumn lookup service backed by a collection of
 *  gradebook columns. The gradebook columns are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of gradebook columns can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapGradebookColumnLookupSession
    extends net.okapia.osid.jamocha.core.grading.spi.AbstractMapGradebookColumnLookupSession
    implements org.osid.grading.GradebookColumnLookupSession {


    /**
     *  Constructs a new {@code MutableMapGradebookColumnLookupSession}
     *  with no gradebook columns.
     *
     *  @param gradebook the gradebook
     *  @throws org.osid.NullArgumentException {@code gradebook} is
     *          {@code null}
     */

      public MutableMapGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook) {
        setGradebook(gradebook);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapGradebookColumnLookupSession} with a
     *  single gradebookColumn.
     *
     *  @param gradebook the gradebook  
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumn} is {@code null}
     */

    public MutableMapGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook,
                                           org.osid.grading.GradebookColumn gradebookColumn) {
        this(gradebook);
        putGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapGradebookColumnLookupSession}
     *  using an array of gradebook columns.
     *
     *  @param gradebook the gradebook
     *  @param gradebookColumns an array of gradebook columns
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumns} is {@code null}
     */

    public MutableMapGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook,
                                           org.osid.grading.GradebookColumn[] gradebookColumns) {
        this(gradebook);
        putGradebookColumns(gradebookColumns);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapGradebookColumnLookupSession}
     *  using a collection of gradebook columns.
     *
     *  @param gradebook the gradebook
     *  @param gradebookColumns a collection of gradebook columns
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumns} is {@code null}
     */

    public MutableMapGradebookColumnLookupSession(org.osid.grading.Gradebook gradebook,
                                           java.util.Collection<? extends org.osid.grading.GradebookColumn> gradebookColumns) {

        this(gradebook);
        putGradebookColumns(gradebookColumns);
        return;
    }

    
    /**
     *  Makes a {@code GradebookColumn} available in this session.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException {@code gradebookColumn{@code  is
     *          {@code null}
     */

    @Override
    public void putGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super.putGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Makes an array of gradebook columns available in this session.
     *
     *  @param gradebookColumns an array of gradebook columns
     *  @throws org.osid.NullArgumentException {@code gradebookColumns{@code 
     *          is {@code null}
     */

    @Override
    public void putGradebookColumns(org.osid.grading.GradebookColumn[] gradebookColumns) {
        super.putGradebookColumns(gradebookColumns);
        return;
    }


    /**
     *  Makes collection of gradebook columns available in this session.
     *
     *  @param gradebookColumns a collection of gradebook columns
     *  @throws org.osid.NullArgumentException {@code gradebookColumns{@code  is
     *          {@code null}
     */

    @Override
    public void putGradebookColumns(java.util.Collection<? extends org.osid.grading.GradebookColumn> gradebookColumns) {
        super.putGradebookColumns(gradebookColumns);
        return;
    }


    /**
     *  Removes a GradebookColumn from this session.
     *
     *  @param gradebookColumnId the {@code Id} of the gradebook column
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId{@code 
     *          is {@code null}
     */

    @Override
    public void removeGradebookColumn(org.osid.id.Id gradebookColumnId) {
        super.removeGradebookColumn(gradebookColumnId);
        return;
    }    
}
