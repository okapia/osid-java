//
// AbstractIndexedMapPackageLookupSession.java
//
//    A simple framework for providing a Package lookup service
//    backed by a fixed collection of packages with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Package lookup service backed by a
 *  fixed collection of packages. The packages are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some packages may be compatible
 *  with more types than are indicated through these package
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Packages</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPackageLookupSession
    extends AbstractMapPackageLookupSession
    implements org.osid.installation.PackageLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.installation.Package> packagesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.installation.Package>());
    private final MultiMap<org.osid.type.Type, org.osid.installation.Package> packagesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.installation.Package>());


    /**
     *  Makes a <code>Package</code> available in this session.
     *
     *  @param  pkg a package
     *  @throws org.osid.NullArgumentException <code>pkg<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPackage(org.osid.installation.Package pkg) {
        super.putPackage(pkg);

        this.packagesByGenus.put(pkg.getGenusType(), pkg);
        
        try (org.osid.type.TypeList types = pkg.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.packagesByRecord.put(types.getNextType(), pkg);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a package from this session.
     *
     *  @param pkgId the <code>Id</code> of the package
     *  @throws org.osid.NullArgumentException <code>pkgId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePackage(org.osid.id.Id pkgId) {
        org.osid.installation.Package pkg;
        try {
            pkg = getPackage(pkgId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.packagesByGenus.remove(pkg.getGenusType());

        try (org.osid.type.TypeList types = pkg.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.packagesByRecord.remove(types.getNextType(), pkg);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePackage(pkgId);
        return;
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  package genus <code>Type</code> which does not include
     *  packages of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known packages or an error results. Otherwise,
     *  the returned list may contain only those packages that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  pkgGenusType a package genus type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.pkg.ArrayPackageList(this.packagesByGenus.get(pkgGenusType)));
    }


    /**
     *  Gets a <code>PackageList</code> containing the given
     *  package record <code>Type</code>. In plenary mode, the
     *  returned list contains all known packages or an error
     *  results. Otherwise, the returned list may contain only those
     *  packages that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  pkgRecordType a package record type 
     *  @return the returned <code>pkg</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByRecordType(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.pkg.ArrayPackageList(this.packagesByRecord.get(pkgRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.packagesByGenus.clear();
        this.packagesByRecord.clear();

        super.close();

        return;
    }
}
