//
// AbstractBloggingProxyManager.java
//
//     An adapter for a BloggingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a BloggingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterBloggingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.blogging.BloggingProxyManager>
    implements org.osid.blogging.BloggingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterBloggingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterBloggingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterBloggingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterBloggingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if sending entries is supported. 
     *
     *  @return <code> true </code> if entry sending is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogging() {
        return (getAdapteeManager().supportsBlogging());
    }


    /**
     *  Tests if entry lookup is supported. 
     *
     *  @return <code> true </code> if entry lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryLookup() {
        return (getAdapteeManager().supportsEntryLookup());
    }


    /**
     *  Tests if entry query is supported. 
     *
     *  @return <code> true </code> if entry query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (getAdapteeManager().supportsEntryQuery());
    }


    /**
     *  Tests if entry search is supported. 
     *
     *  @return <code> true </code> if entry search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySearch() {
        return (getAdapteeManager().supportsEntrySearch());
    }


    /**
     *  Tests if creating, updating and deleting entries is supported. 
     *
     *  @return <code> true </code> if entry administration is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryAdmin() {
        return (getAdapteeManager().supportsEntryAdmin());
    }


    /**
     *  Tests if entry notification is supported. Entries may be sent when 
     *  entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if entry notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryNotification() {
        return (getAdapteeManager().supportsEntryNotification());
    }


    /**
     *  Tests if retrieving mappings of entry and blogs is supported. 
     *
     *  @return <code> true </code> if entry blog mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBlog() {
        return (getAdapteeManager().supportsEntryBlog());
    }


    /**
     *  Tests if managing mappings of entries and blogs is supported. 
     *
     *  @return <code> true </code> if entry blog assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBlogAssignment() {
        return (getAdapteeManager().supportsEntryBlogAssignment());
    }


    /**
     *  Tests if entry smart blogging is available. 
     *
     *  @return <code> true </code> if entry smart blog is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySmartBlog() {
        return (getAdapteeManager().supportsEntrySmartBlog());
    }


    /**
     *  Tests if blog lookup is supported. 
     *
     *  @return <code> true </code> if blog lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogLookup() {
        return (getAdapteeManager().supportsBlogLookup());
    }


    /**
     *  Tests if blog query is supported. 
     *
     *  @return <code> true </code> if blog query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogQuery() {
        return (getAdapteeManager().supportsBlogQuery());
    }


    /**
     *  Tests if blog search is supported. 
     *
     *  @return <code> true </code> if blog search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogSearch() {
        return (getAdapteeManager().supportsBlogSearch());
    }


    /**
     *  Tests if blog administration is supported. 
     *
     *  @return <code> true </code> if blog administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogAdmin() {
        return (getAdapteeManager().supportsBlogAdmin());
    }


    /**
     *  Tests if blog notification is supported. Entries may be sent when 
     *  <code> Blog </code> objects are created, deleted or updated. 
     *  Notifications for entries within blogs are sent via the entry 
     *  notification session. 
     *
     *  @return <code> true </code> if blog notification is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogNotification() {
        return (getAdapteeManager().supportsBlogNotification());
    }


    /**
     *  Tests if a blog hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a blog hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogHierarchy() {
        return (getAdapteeManager().supportsBlogHierarchy());
    }


    /**
     *  Tests if a blog hierarchy design is supported. 
     *
     *  @return <code> true </code> if a blog hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogHierarchyDesign() {
        return (getAdapteeManager().supportsBlogHierarchyDesign());
    }


    /**
     *  Tests if a blogging batch service is supported. 
     *
     *  @return <code> true </code> if a blogging batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBloggingBatch() {
        return (getAdapteeManager().supportsBloggingBatch());
    }


    /**
     *  Gets all the entry record types supported. 
     *
     *  @return the list of supported entry record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (getAdapteeManager().getEntryRecordTypes());
    }


    /**
     *  Tests if a given entry record type is supported. 
     *
     *  @param  entryRecordType the entry type 
     *  @return <code> true </code> if the entry record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (getAdapteeManager().supportsEntryRecordType(entryRecordType));
    }


    /**
     *  Gets all the entry search record types supported. 
     *
     *  @return the list of supported entry search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (getAdapteeManager().getEntrySearchRecordTypes());
    }


    /**
     *  Tests if a given entry search type is supported. 
     *
     *  @param  entrySearchRecordType the entry search type 
     *  @return <code> true </code> if the entry search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (getAdapteeManager().supportsEntrySearchRecordType(entrySearchRecordType));
    }


    /**
     *  Gets all the blog record types supported. 
     *
     *  @return the list of supported blog record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlogRecordTypes() {
        return (getAdapteeManager().getBlogRecordTypes());
    }


    /**
     *  Tests if a given blog record type is supported. 
     *
     *  @param  blogRecordType the blog record type 
     *  @return <code> true </code> if the blog record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blogRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlogRecordType(org.osid.type.Type blogRecordType) {
        return (getAdapteeManager().supportsBlogRecordType(blogRecordType));
    }


    /**
     *  Gets all the blog search record types supported. 
     *
     *  @return the list of supported blog search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlogSearchRecordTypes() {
        return (getAdapteeManager().getBlogSearchRecordTypes());
    }


    /**
     *  Tests if a given blog search record type is supported. 
     *
     *  @param  blogSearchRecordType the blog search record type 
     *  @return <code> true </code> if the blog search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blogSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlogSearchRecordType(org.osid.type.Type blogSearchRecordType) {
        return (getAdapteeManager().supportsBlogSearchRecordType(blogSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryLookupSession getEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryLookupSession getEntryLookupSessionForBlog(org.osid.id.Id blogId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryLookupSessionForBlog(blogId, proxy));
    }


    /**
     *  Gets an entry query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuerySession getEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryQuerySession(proxy));
    }


    /**
     *  Gets an entry query session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuerySession getEntryQuerySessionForBlog(org.osid.id.Id blogId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryQuerySessionForBlog(blogId, proxy));
    }


    /**
     *  Gets an entry search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySearchSession getEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySearchSession(proxy));
    }


    /**
     *  Gets an entry search session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySearchSession getEntrySearchSessionForBlog(org.osid.id.Id blogId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySearchSessionForBlog(blogId, proxy));
    }


    /**
     *  Gets an entry administration session for creating, updating and 
     *  deleting entries. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryAdminSession getEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryAdminSession(proxy));
    }


    /**
     *  Gets an entry administration session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryAdminSession getEntryAdminSessionForBlog(org.osid.id.Id blogId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryAdminSessionForBlog(blogId, proxy));
    }


    /**
     *  Gets the entry notification session for the given blog. 
     *
     *  @param  entryReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> an EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryNotificationSession getEntryNotificationSession(org.osid.blogging.EntryReceiver entryReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryNotificationSession(entryReceiver, proxy));
    }


    /**
     *  Gets the entry notification session for the given blog. 
     *
     *  @param  entryReceiver notification callback 
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> entryReceiver, blogId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryNotificationSession getEntryNotificationSessionForBlog(org.osid.blogging.EntryReceiver entryReceiver, 
                                                                                         org.osid.id.Id blogId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryNotificationSessionForBlog(entryReceiver, blogId, proxy));
    }


    /**
     *  Gets the session for retrieving entry to blog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryBlogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryBlog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryBlogSession getEntryBlogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBlogSession(proxy));
    }


    /**
     *  Gets the session for assigning entry to blog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryBlogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBlogAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryBlogAssignmentSession getEntryBlogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEntryBlogAssignmentSession(proxy));
    }


    /**
     *  Gets the session managing smart blogs. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return a <code> EntrySmartBlogSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> is not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartBlog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySmartBlogSession getEntrySmartBlogSession(org.osid.id.Id blogId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEntrySmartBlogSession(blogId, proxy));
    }


    /**
     *  Gets the blog lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogLookupSession getBlogLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogLookupSession(proxy));
    }


    /**
     *  Gets the blog query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuerySession getBlogQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogQuerySession(proxy));
    }


    /**
     *  Gets the blog search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogSearchSession getBlogSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogSearchSession(proxy));
    }


    /**
     *  Gets the blog administrative session for creating, updating and 
     *  deleteing blogs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogAdminSession getBlogAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogAdminSession(proxy));
    }


    /**
     *  Gets the notification session for subscriblogg to changes to a blog. 
     *
     *  @param  blogReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BlogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> blogReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogNotificationSession getBlogNotificationSession(org.osid.blogging.BlogReceiver blogReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogNotificationSession(blogReceiver, proxy));
    }


    /**
     *  Gets the blog hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BlogHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogHierarchySession getBlogHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogHierarchySession(proxy));
    }


    /**
     *  Gets the blog hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogHierarchyDesignSession getBlogHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlogHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> BloggingBatchProxyManager. </code> 
     *
     *  @return a <code> BloggingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBloggingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.BloggingBatchProxyManager getBloggingBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBloggingBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
