//
// MutableMapPoolProcessorLookupSession
//
//    Implements a PoolProcessor lookup service backed by a collection of
//    poolProcessors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolProcessor lookup service backed by a collection of
 *  pool processors. The pool processors are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of pool processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapPoolProcessorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolProcessorLookupSession
    implements org.osid.provisioning.rules.PoolProcessorLookupSession {


    /**
     *  Constructs a new {@code MutableMapPoolProcessorLookupSession}
     *  with no pool processors.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolProcessorLookupSession} with a
     *  single poolProcessor.
     *
     *  @param distributor the distributor  
     *  @param poolProcessor a pool processor
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessor} is {@code null}
     */

    public MutableMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.PoolProcessor poolProcessor) {
        this(distributor);
        putPoolProcessor(poolProcessor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolProcessorLookupSession}
     *  using an array of pool processors.
     *
     *  @param distributor the distributor
     *  @param poolProcessors an array of pool processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessors} is {@code null}
     */

    public MutableMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.PoolProcessor[] poolProcessors) {
        this(distributor);
        putPoolProcessors(poolProcessors);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapPoolProcessorLookupSession}
     *  using a collection of pool processors.
     *
     *  @param distributor the distributor
     *  @param poolProcessors a collection of pool processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessors} is {@code null}
     */

    public MutableMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessor> poolProcessors) {

        this(distributor);
        putPoolProcessors(poolProcessors);
        return;
    }

    
    /**
     *  Makes a {@code PoolProcessor} available in this session.
     *
     *  @param poolProcessor a pool processor
     *  @throws org.osid.NullArgumentException {@code poolProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putPoolProcessor(org.osid.provisioning.rules.PoolProcessor poolProcessor) {
        super.putPoolProcessor(poolProcessor);
        return;
    }


    /**
     *  Makes an array of pool processors available in this session.
     *
     *  @param poolProcessors an array of pool processors
     *  @throws org.osid.NullArgumentException {@code poolProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putPoolProcessors(org.osid.provisioning.rules.PoolProcessor[] poolProcessors) {
        super.putPoolProcessors(poolProcessors);
        return;
    }


    /**
     *  Makes collection of pool processors available in this session.
     *
     *  @param poolProcessors a collection of pool processors
     *  @throws org.osid.NullArgumentException {@code poolProcessors{@code  is
     *          {@code null}
     */

    @Override
    public void putPoolProcessors(java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessor> poolProcessors) {
        super.putPoolProcessors(poolProcessors);
        return;
    }


    /**
     *  Removes a PoolProcessor from this session.
     *
     *  @param poolProcessorId the {@code Id} of the pool processor
     *  @throws org.osid.NullArgumentException {@code poolProcessorId{@code 
     *          is {@code null}
     */

    @Override
    public void removePoolProcessor(org.osid.id.Id poolProcessorId) {
        super.removePoolProcessor(poolProcessorId);
        return;
    }    
}
