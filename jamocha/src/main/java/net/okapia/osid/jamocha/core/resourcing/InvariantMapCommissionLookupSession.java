//
// InvariantMapCommissionLookupSession
//
//    Implements a Commission lookup service backed by a fixed collection of
//    commissions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Commission lookup service backed by a fixed
 *  collection of commissions. The commissions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCommissionLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapCommissionLookupSession
    implements org.osid.resourcing.CommissionLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCommissionLookupSession</code> with no
     *  commissions.
     *  
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumnetException {@code foundry} is
     *          {@code null}
     */

    public InvariantMapCommissionLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCommissionLookupSession</code> with a single
     *  commission.
     *  
     *  @param foundry the foundry
     *  @param commission a single commission
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code commission} is <code>null</code>
     */

      public InvariantMapCommissionLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.Commission commission) {
        this(foundry);
        putCommission(commission);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCommissionLookupSession</code> using an array
     *  of commissions.
     *  
     *  @param foundry the foundry
     *  @param commissions an array of commissions
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code commissions} is <code>null</code>
     */

      public InvariantMapCommissionLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.Commission[] commissions) {
        this(foundry);
        putCommissions(commissions);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCommissionLookupSession</code> using a
     *  collection of commissions.
     *
     *  @param foundry the foundry
     *  @param commissions a collection of commissions
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code commissions} is <code>null</code>
     */

      public InvariantMapCommissionLookupSession(org.osid.resourcing.Foundry foundry,
                                               java.util.Collection<? extends org.osid.resourcing.Commission> commissions) {
        this(foundry);
        putCommissions(commissions);
        return;
    }
}
