//
// RoomElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.room.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RoomElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the RoomElement Id.
     *
     *  @return the room element Id
     */

    public static org.osid.id.Id getRoomEntityId() {
        return (makeEntityId("osid.room.Room"));
    }


    /**
     *  Gets the BuildingId element Id.
     *
     *  @return the BuildingId element Id
     */

    public static org.osid.id.Id getBuildingId() {
        return (makeElementId("osid.room.room.BuildingId"));
    }


    /**
     *  Gets the Building element Id.
     *
     *  @return the Building element Id
     */

    public static org.osid.id.Id getBuilding() {
        return (makeElementId("osid.room.room.Building"));
    }


    /**
     *  Gets the FloorId element Id.
     *
     *  @return the FloorId element Id
     */

    public static org.osid.id.Id getFloorId() {
        return (makeElementId("osid.room.room.FloorId"));
    }


    /**
     *  Gets the Floor element Id.
     *
     *  @return the Floor element Id
     */

    public static org.osid.id.Id getFloor() {
        return (makeElementId("osid.room.room.Floor"));
    }


    /**
     *  Gets the EnclosingRoomId element Id.
     *
     *  @return the EnclosingRoomId element Id
     */

    public static org.osid.id.Id getEnclosingRoomId() {
        return (makeElementId("osid.room.room.EnclosingRoomId"));
    }


    /**
     *  Gets the EnclosingRoom element Id.
     *
     *  @return the EnclosingRoom element Id
     */

    public static org.osid.id.Id getEnclosingRoom() {
        return (makeElementId("osid.room.room.EnclosingRoom"));
    }


    /**
     *  Gets the SubdivisionIds element Id.
     *
     *  @return the SubdivisionIds element Id
     */

    public static org.osid.id.Id getSubdivisionIds() {
        return (makeElementId("osid.room.room.SubdivisionIds"));
    }


    /**
     *  Gets the Subdivisions element Id.
     *
     *  @return the Subdivisions element Id
     */

    public static org.osid.id.Id getSubdivisions() {
        return (makeElementId("osid.room.room.Subdivisions"));
    }


    /**
     *  Gets the DesignatedName element Id.
     *
     *  @return the DesignatedName element Id
     */

    public static org.osid.id.Id getDesignatedName() {
        return (makeElementId("osid.room.room.DesignatedName"));
    }


    /**
     *  Gets the RoomNumber element Id.
     *
     *  @return the RoomNumber element Id
     */

    public static org.osid.id.Id getRoomNumber() {
        return (makeElementId("osid.room.room.RoomNumber"));
    }


    /**
     *  Gets the Code element Id.
     *
     *  @return the Code element Id
     */

    public static org.osid.id.Id getCode() {
        return (makeElementId("osid.room.room.Code"));
    }


    /**
     *  Gets the Area element Id.
     *
     *  @return the Area element Id
     */

    public static org.osid.id.Id getArea() {
        return (makeElementId("osid.room.room.Area"));
    }


    /**
     *  Gets the OccupancyLimit element Id.
     *
     *  @return the OccupancyLimit element Id
     */

    public static org.osid.id.Id getOccupancyLimit() {
        return (makeElementId("osid.room.room.OccupancyLimit"));
    }


    /**
     *  Gets the ResourceIds element Id.
     *
     *  @return the ResourceIds element Id
     */

    public static org.osid.id.Id getResourceIds() {
        return (makeElementId("osid.room.room.ResourceIds"));
    }


    /**
     *  Gets the Resources element Id.
     *
     *  @return the Resources element Id
     */

    public static org.osid.id.Id getResources() {
        return (makeElementId("osid.room.room.Resources"));
    }


    /**
     *  Gets the CampusId element Id.
     *
     *  @return the CampusId element Id
     */

    public static org.osid.id.Id getCampusId() {
        return (makeQueryElementId("osid.room.room.CampusId"));
    }


    /**
     *  Gets the Campus element Id.
     *
     *  @return the Campus element Id
     */

    public static org.osid.id.Id getCampus() {
        return (makeQueryElementId("osid.room.room.Campus"));
    }
}
