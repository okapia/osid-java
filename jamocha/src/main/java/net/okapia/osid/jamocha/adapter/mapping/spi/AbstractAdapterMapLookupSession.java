//
// AbstractAdapterMapLookupSession.java
//
//    A Map lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Map lookup session adapter.
 */

public abstract class AbstractAdapterMapLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.MapLookupSession {

    private final org.osid.mapping.MapLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterMapLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterMapLookupSession(org.osid.mapping.MapLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Map} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupMaps() {
        return (this.session.canLookupMaps());
    }


    /**
     *  A complete view of the {@code Map} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMapView() {
        this.session.useComparativeMapView();
        return;
    }


    /**
     *  A complete view of the {@code Map} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMapView() {
        this.session.usePlenaryMapView();
        return;
    }

     
    /**
     *  Gets the {@code Map} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Map} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Map} and
     *  retained for compatibility.
     *
     *  @param mapId {@code Id} of the {@code Map}
     *  @return the map
     *  @throws org.osid.NotFoundException {@code mapId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code mapId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMap(mapId));
    }


    /**
     *  Gets a {@code MapList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  maps specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Maps} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  mapIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Map} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code mapIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByIds(org.osid.id.IdList mapIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMapsByIds(mapIds));
    }


    /**
     *  Gets a {@code MapList} corresponding to the given
     *  map genus {@code Type} which does not include
     *  maps of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapGenusType a map genus type 
     *  @return the returned {@code Map} list
     *  @throws org.osid.NullArgumentException
     *          {@code mapGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByGenusType(org.osid.type.Type mapGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMapsByGenusType(mapGenusType));
    }


    /**
     *  Gets a {@code MapList} corresponding to the given
     *  map genus {@code Type} and include any additional
     *  maps with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapGenusType a map genus type 
     *  @return the returned {@code Map} list
     *  @throws org.osid.NullArgumentException
     *          {@code mapGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByParentGenusType(org.osid.type.Type mapGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMapsByParentGenusType(mapGenusType));
    }


    /**
     *  Gets a {@code MapList} containing the given
     *  map record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapRecordType a map record type 
     *  @return the returned {@code Map} list
     *  @throws org.osid.NullArgumentException
     *          {@code mapRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByRecordType(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMapsByRecordType(mapRecordType));
    }


    /**
     *  Gets a {@code MapList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Map} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMapsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Maps}. 
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Maps} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMaps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMaps());
    }
}
