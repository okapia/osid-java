//
// AbstractAdapterTypeLookupSession.java
//
//    A Type lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.type.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Type lookup session adapter.
 */

public abstract class AbstractAdapterTypeLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.type.TypeLookupSession {

    private final org.osid.type.TypeLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTypeLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTypeLookupSession(org.osid.type.TypeLookupSession session) {
        super(session);
        this.session = session;
        return;
    }

    
    /**
     *  Tests if this user can perform {@code Type} lookups. A return
     *  of true does not guarantee successful authorization. A return
     *  of false indicates that it is known all methods in this
     *  session will result in a {@code PERMISSION_DENIED}.  This is
     *  intended as a hint to an application that may opt not to offer
     *  lookup operations.
     *
     *  @return {@code false} if lookup methods are not authorized, 
     *          {@code true} otherwise 
     */

    @OSID @Override
    public boolean canLookupTypes() {
        return (this.session.canLookupTypes());
    }


    /**
     *  Gets a {@code Type} by its string representation which is a
     *  combination of the authority and identifier. This method only
     *  returns the {@code Type} if it is known by the given
     *  identification components.
     *
     *  @param  namespace the identifier namespace 
     *  @param  identifier the identifier 
     *  @param  authority the authority 
     *  @return the {@code Type} 
     *  @throws org.osid.NotFoundException the type is not found 
     *  @throws org.osid.NullArgumentException {@code null} argument 
     *          provided 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.Type getType(String namespace, String identifier, 
                                      String authority)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getType(namespace, identifier, authority));
    }


    /**
     *  Tests if the given {@code Type} is known. 
     *
     *  @param  type the {@code Type} to look for 
     *  @return {@code true} if the given {@code Type} is known, 
     *          {@code false} otherwise 
     *  @throws org.osid.NullArgumentException {@code type} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public boolean hasType(org.osid.type.Type type)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.hasType(type));
    }


    /**
     *  Gets all the known Types by domain. 
     *
     *  @param  domain the domain 
     *  @return the list of {@code Types} with the given domain 
     *  @throws org.osid.NullArgumentException {@code domain} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getTypesByDomain(String domain)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTypesByDomain(domain));
    }


    /**
     *  Gets all the known Types by authority. 
     *
     *  @param  authority the authority 
     *  @return the list of {@code Types} with the given authority 
     *  @throws org.osid.NullArgumentException {@code authority} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException respect my authoritay 
     */

    @OSID @Override
    public org.osid.type.TypeList getTypesByAuthority(String authority)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTypesByAuthority(authority));
    }


    /**
     *  Gets all the known Types by domain and authority.
     *
     *  @param  domain the domain 
     *  @param  authority the authority 
     *  @return the list of {@code Types} with the given domain and 
     *          authority 
     *  @throws org.osid.NullArgumentException {@code domain} or {@code 
     *          authority} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getTypesByDomainAndAuthority(String domain, 
                                                               String authority)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTypesByDomainAndAuthority(domain, authority));
    }


    /**
     *  Gets all the known Types. 
     *
     *  @return the list of all known {@code Types} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getTypes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTypes());
    }


    /**
     *  Tests if the given types are equivalent. 
     *
     *  @param  type a type 
     *  @param  equivalentType another type 
     *  @return {@code true} if both types are equivalent, {@code false 
     *         } otherwise 
     *  @throws org.osid.NullArgumentException {@code type} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public boolean isEquivalent(org.osid.type.Type type, 
                                org.osid.type.Type equivalentType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.isEquivalent(type, equivalentType));
    }


    /**
     *  Tests if the given type is implies support of a base type. 
     *
     *  @param  type a type 
     *  @param  baseType another type 
     *  @return {@code true} if {@code baseType} if supported by 
     *          {@code type,} {@code false} otherwise 
     *  @throws org.osid.NullArgumentException {@code type} or {@code 
     *          baseType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public boolean impliesSupport(org.osid.type.Type type, 
                                  org.osid.type.Type baseType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.impliesSupport(type, baseType));
    }


    /**
     *  Tests if the given type is derived from a base type. 
     *
     *  @param  type a type 
     *  @return {@code true} is the given type is derived from a base 
     *          type, {@code false} otherwise 
     *  @throws org.osid.NullArgumentException {@code type} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public boolean hasBaseType(org.osid.type.Type type)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.hasBaseType(type));
    }


    /**
     *  Gets the immediate base types of this type. 
     *
     *  @param  type a type 
     *  @return the base types 
     *  @throws org.osid.NullArgumentException {@code type} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getBaseTypes(org.osid.type.Type type)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBaseTypes(type));
    }


    /**
     *  Gets all known relation {@code Types.} A relation Types
     *  relates two {@code Types}.
     *
     *  @return known relation types 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelationTypes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationTypes());
    }


    /**
     *  Gets all source {@code Types} related by the given type.
     *
     *  @param  relationType a relation type 
     *  @return the source types 
     *  @throws org.osid.NullArgumentException {@code relationType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceTypesByRelationType(org.osid.type.Type relationType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSourceTypesByRelationType(relationType));
    }


    /**
     *  Gets all destination Types related to the given source {@code
     *  Type}.
     *
     *  @param  sourceType a source type 
     *  @return the related types 
     *  @throws org.osid.NullArgumentException {@code sourceType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getDestinationTypesBySource(org.osid.type.Type sourceType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDestinationTypesBySource(sourceType));
    }


    /**
     *  Gets all destination Types related to the given source {@code
     *  Type} and relation {@code Type.}
     *
     *  @param  sourceType a source type 
     *  @param  relationType a relation type 
     *  @return the related types 
     *  @throws org.osid.NullArgumentException {@code sourceType} or 
     *          {@code relationType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getDestinationTypesBySourceAndRelationType(org.osid.type.Type sourceType, 
                                                                             org.osid.type.Type relationType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDestinationTypesBySourceAndRelationType(sourceType, relationType));
    }


    /**
     *  Gets all destination {@code Types} related by the given
     *  type.
     *
     *  @param  relationType a relation type 
     *  @return the destination types 
     *  @throws org.osid.NullArgumentException {@code relationType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getDestinationTypesByRelationType(org.osid.type.Type relationType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDestinationTypesByRelationType(relationType));
    }


    /**
     *  Gets all source Types related to the given destination {@code
     *  Type}.
     *
     *  @param  destinationType a destination type 
     *  @return the source types 
     *  @throws org.osid.NullArgumentException {@code destinationType} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceTypesByDestination(org.osid.type.Type destinationType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSourceTypesByDestination(destinationType));
    }


    /**
     *  Gets all source Types related to the given destination {@code
     * Type } and relation {@code Type.}
     *
     *  @param  destinationType a destination type 
     *  @param  relationType a relation type 
     *  @return the related types 
     *  @throws org.osid.NullArgumentException {@code destinationType}
     *          or {@code relationType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceTypesByDestinationAndRelationType(org.osid.type.Type destinationType, 
                                                                             org.osid.type.Type relationType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSourceTypesByDestinationAndRelationType(destinationType, relationType));
    }
}

