//
// AbstractOrderSearch.java
//
//     A template for making an Order Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing order searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOrderSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ordering.OrderSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ordering.records.OrderSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ordering.OrderSearchOrder orderSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of orders. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  orderIds list of orders
     *  @throws org.osid.NullArgumentException
     *          <code>orderIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOrders(org.osid.id.IdList orderIds) {
        while (orderIds.hasNext()) {
            try {
                this.ids.add(orderIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOrders</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of order Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOrderIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  orderSearchOrder order search order 
     *  @throws org.osid.NullArgumentException
     *          <code>orderSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>orderSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOrderResults(org.osid.ordering.OrderSearchOrder orderSearchOrder) {
	this.orderSearchOrder = orderSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ordering.OrderSearchOrder getOrderSearchOrder() {
	return (this.orderSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given order search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an order implementing the requested record.
     *
     *  @param orderSearchRecordType an order search record
     *         type
     *  @return the order search record
     *  @throws org.osid.NullArgumentException
     *          <code>orderSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(orderSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderSearchRecord getOrderSearchRecord(org.osid.type.Type orderSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ordering.records.OrderSearchRecord record : this.records) {
            if (record.implementsRecordType(orderSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this order search. 
     *
     *  @param orderSearchRecord order search record
     *  @param orderSearchRecordType order search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOrderSearchRecord(org.osid.ordering.records.OrderSearchRecord orderSearchRecord, 
                                           org.osid.type.Type orderSearchRecordType) {

        addRecordType(orderSearchRecordType);
        this.records.add(orderSearchRecord);        
        return;
    }
}
