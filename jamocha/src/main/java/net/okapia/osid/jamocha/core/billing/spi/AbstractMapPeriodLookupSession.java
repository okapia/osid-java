//
// AbstractMapPeriodLookupSession
//
//    A simple framework for providing a Period lookup service
//    backed by a fixed collection of periods.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Period lookup service backed by a
 *  fixed collection of periods. The periods are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Periods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPeriodLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractPeriodLookupSession
    implements org.osid.billing.PeriodLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.billing.Period> periods = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.billing.Period>());


    /**
     *  Makes a <code>Period</code> available in this session.
     *
     *  @param  period a period
     *  @throws org.osid.NullArgumentException <code>period<code>
     *          is <code>null</code>
     */

    protected void putPeriod(org.osid.billing.Period period) {
        this.periods.put(period.getId(), period);
        return;
    }


    /**
     *  Makes an array of periods available in this session.
     *
     *  @param  periods an array of periods
     *  @throws org.osid.NullArgumentException <code>periods<code>
     *          is <code>null</code>
     */

    protected void putPeriods(org.osid.billing.Period[] periods) {
        putPeriods(java.util.Arrays.asList(periods));
        return;
    }


    /**
     *  Makes a collection of periods available in this session.
     *
     *  @param  periods a collection of periods
     *  @throws org.osid.NullArgumentException <code>periods<code>
     *          is <code>null</code>
     */

    protected void putPeriods(java.util.Collection<? extends org.osid.billing.Period> periods) {
        for (org.osid.billing.Period period : periods) {
            this.periods.put(period.getId(), period);
        }

        return;
    }


    /**
     *  Removes a Period from this session.
     *
     *  @param  periodId the <code>Id</code> of the period
     *  @throws org.osid.NullArgumentException <code>periodId<code> is
     *          <code>null</code>
     */

    protected void removePeriod(org.osid.id.Id periodId) {
        this.periods.remove(periodId);
        return;
    }


    /**
     *  Gets the <code>Period</code> specified by its <code>Id</code>.
     *
     *  @param  periodId <code>Id</code> of the <code>Period</code>
     *  @return the period
     *  @throws org.osid.NotFoundException <code>periodId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>periodId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod(org.osid.id.Id periodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.billing.Period period = this.periods.get(periodId);
        if (period == null) {
            throw new org.osid.NotFoundException("period not found: " + periodId);
        }

        return (period);
    }


    /**
     *  Gets all <code>Periods</code>. In plenary mode, the returned
     *  list contains all known periods or an error
     *  results. Otherwise, the returned list may contain only those
     *  periods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Periods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.period.ArrayPeriodList(this.periods.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.periods.clear();
        super.close();
        return;
    }
}
