//
// MutableIndexedMapProxyActivityUnitLookupSession
//
//    Implements an ActivityUnit lookup service backed by a collection of
//    activityUnits indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements an ActivityUnit lookup service backed by a collection of
 *  activityUnits. The activity units are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some activityUnits may be compatible
 *  with more types than are indicated through these activityUnit
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of activity units can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyActivityUnitLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractIndexedMapActivityUnitLookupSession
    implements org.osid.course.ActivityUnitLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityUnitLookupSession} with
     *  no activity unit.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityUnitLookupSession} with
     *  a single activity unit.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityUnit an activity unit
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityUnit}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.ActivityUnit activityUnit, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putActivityUnit(activityUnit);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityUnitLookupSession} using
     *  an array of activity units.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityUnits an array of activity units
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityUnits}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.ActivityUnit[] activityUnits, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putActivityUnits(activityUnits);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityUnitLookupSession} using
     *  a collection of activity units.
     *
     *  @param courseCatalog the course catalog
     *  @param  activityUnits a collection of activity units
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activityUnits}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityUnitLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       java.util.Collection<? extends org.osid.course.ActivityUnit> activityUnits,
                                                       org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putActivityUnits(activityUnits);
        return;
    }

    
    /**
     *  Makes an {@code ActivityUnit} available in this session.
     *
     *  @param  activityUnit an activity unit
     *  @throws org.osid.NullArgumentException {@code activityUnit{@code 
     *          is {@code null}
     */

    @Override
    public void putActivityUnit(org.osid.course.ActivityUnit activityUnit) {
        super.putActivityUnit(activityUnit);
        return;
    }


    /**
     *  Makes an array of activity units available in this session.
     *
     *  @param  activityUnits an array of activity units
     *  @throws org.osid.NullArgumentException {@code activityUnits{@code 
     *          is {@code null}
     */

    @Override
    public void putActivityUnits(org.osid.course.ActivityUnit[] activityUnits) {
        super.putActivityUnits(activityUnits);
        return;
    }


    /**
     *  Makes collection of activity units available in this session.
     *
     *  @param  activityUnits a collection of activity units
     *  @throws org.osid.NullArgumentException {@code activityUnit{@code 
     *          is {@code null}
     */

    @Override
    public void putActivityUnits(java.util.Collection<? extends org.osid.course.ActivityUnit> activityUnits) {
        super.putActivityUnits(activityUnits);
        return;
    }


    /**
     *  Removes an ActivityUnit from this session.
     *
     *  @param activityUnitId the {@code Id} of the activity unit
     *  @throws org.osid.NullArgumentException {@code activityUnitId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActivityUnit(org.osid.id.Id activityUnitId) {
        super.removeActivityUnit(activityUnitId);
        return;
    }    
}
