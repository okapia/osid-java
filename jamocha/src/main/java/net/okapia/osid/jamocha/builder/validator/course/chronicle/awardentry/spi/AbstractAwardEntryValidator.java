//
// AbstractAwardEntryValidator.java
//
//     Validates an AwardEntry.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.chronicle.awardentry.spi;


/**
 *  Validates an AwardEntry.
 */

public abstract class AbstractAwardEntryValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractAwardEntryValidator</code>.
     */

    protected AbstractAwardEntryValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractAwardEntryValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractAwardEntryValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an AwardEntry.
     *
     *  @param awardEntry an award entry to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>awardEntry</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.chronicle.AwardEntry awardEntry) {
        super.validate(awardEntry);

        testNestedObject(awardEntry, "getStudent");
        testNestedObject(awardEntry, "getAward");
        test(awardEntry.getDateAwarded(), "getDateAwarded()");

        testConditionalMethod(awardEntry, "getProgramId", awardEntry.hasProgram(), "hasProgram()");
        testConditionalMethod(awardEntry, "getProgram", awardEntry.hasProgram(), "hasProgram()");
        if (awardEntry.hasProgram()) {
            testNestedObject(awardEntry, "getProgram");
        }

        testConditionalMethod(awardEntry, "getCourseId", awardEntry.hasCourse(), "hasCourse()");
        testConditionalMethod(awardEntry, "getCourse", awardEntry.hasCourse(), "hasCourse()");
        if (awardEntry.hasCourse()) {
            testNestedObject(awardEntry, "getCourse");
        }

        testConditionalMethod(awardEntry, "getAssessmentId", awardEntry.hasAssessment(), "hasAssessment()");
        testConditionalMethod(awardEntry, "getAssessment", awardEntry.hasAssessment(), "hasAssessment()");
        if (awardEntry.hasAssessment()) {
            testNestedObject(awardEntry, "getAssessment");
        }

        if (awardEntry.hasProgram() && (awardEntry.hasCourse() || awardEntry.hasAssessment())) {
            throw new org.osid.BadLogicException("hasCourse() or hasAssessment() is true");
        }

        if (awardEntry.hasCourse() && (awardEntry.hasProgram() || awardEntry.hasAssessment())) {
            throw new org.osid.BadLogicException("hasProgram() or hasAssessment() is true");
        }

        if (awardEntry.hasAssessment() && (awardEntry.hasCourse() || awardEntry.hasProgram())) {
            throw new org.osid.BadLogicException("hasCourse() or hasProgram() is true");
        }

        return;
    }
}
