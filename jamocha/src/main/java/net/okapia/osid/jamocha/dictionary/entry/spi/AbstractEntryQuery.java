//
// AbstractEntryQuery.java
//
//     A template for making an Entry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for entries.
 */

public abstract class AbstractEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.dictionary.EntryQuery {

    private final java.util.Collection<org.osid.dictionary.records.EntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the <code> Type </code> for querying keys of a given type. 
     *
     *  @param  keyType the key <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> keyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchKeyType(org.osid.type.Type keyType, boolean match) {
        return;
    }


    /**
     *  Clears the key type terms. 
     */

    @OSID @Override
    public void clearKeyTypeTerms() {
        return;
    }


    /**
     *  Matches entries of this key. 
     *
     *  @param  key the key 
     *  @param  keyType the key <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> key </code> or <code> 
     *          keyType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchKey(java.lang.Object key, org.osid.type.Type keyType, 
                         boolean match) {
        return;
    }


    /**
     *  Clears the key terms. 
     */

    @OSID @Override
    public void clearKeyTerms() {
        return;
    }


    /**
     *  Sets the <code> Type </code> of this entry value. 
     *
     *  @param  valueType the value <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> valueType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueType(org.osid.type.Type valueType, boolean match) {
        return;
    }


    /**
     *  Clears the value type terms. 
     */

    @OSID @Override
    public void clearValueTypeTerms() {
        return;
    }


    /**
     *  Sets the value in this entry. 
     *
     *  @param  value the value 
     *  @param  valueType the value <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> or <code> 
     *          valueType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchValue(java.lang.Object value, 
                           org.osid.type.Type valueType, boolean match) {
        return;
    }


    /**
     *  Clears the value terms. 
     */

    @OSID @Override
    public void clearValueTerms() {
        return;
    }


    /**
     *  Sets the dictionary <code> Id </code> for this query. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDictionaryId(org.osid.id.Id dictionaryId, boolean match) {
        return;
    }


    /**
     *  Clears the dictionary <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDictionaryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DictionaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a dictionary query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dictionary. Multiple retrievals produce a nested 
     *  boolean <code> OR </code> term. 
     *
     *  @return the dictionary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuery getDictionaryQuery() {
        throw new org.osid.UnimplementedException("supportsDictionaryQuery() is false");
    }


    /**
     *  Clears the dictionary terms. 
     */

    @OSID @Override
    public void clearDictionaryTerms() {
        return;
    }


    /**
     *  Gets the record corresponding to the given entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an entry implementing the requested record.
     *
     *  @param entryRecordType an entry record type
     *  @return the entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.EntryQueryRecord getEntryQueryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.EntryQueryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry query. 
     *
     *  @param entryQueryRecord entry query record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEntryQueryRecord(org.osid.dictionary.records.EntryQueryRecord entryQueryRecord, 
                                          org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);
        nullarg(entryQueryRecord, "entry query record");
        this.records.add(entryQueryRecord);        
        return;
    }
}
