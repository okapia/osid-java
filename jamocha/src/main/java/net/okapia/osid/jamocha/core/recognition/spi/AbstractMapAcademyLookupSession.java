//
// AbstractMapAcademyLookupSession
//
//    A simple framework for providing an Academy lookup service
//    backed by a fixed collection of academies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Academy lookup service backed by a
 *  fixed collection of academies. The academies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Academies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAcademyLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractAcademyLookupSession
    implements org.osid.recognition.AcademyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.recognition.Academy> academies = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.recognition.Academy>());


    /**
     *  Makes an <code>Academy</code> available in this session.
     *
     *  @param  academy an academy
     *  @throws org.osid.NullArgumentException <code>academy<code>
     *          is <code>null</code>
     */

    protected void putAcademy(org.osid.recognition.Academy academy) {
        this.academies.put(academy.getId(), academy);
        return;
    }


    /**
     *  Makes an array of academies available in this session.
     *
     *  @param  academies an array of academies
     *  @throws org.osid.NullArgumentException <code>academies<code>
     *          is <code>null</code>
     */

    protected void putAcademies(org.osid.recognition.Academy[] academies) {
        putAcademies(java.util.Arrays.asList(academies));
        return;
    }


    /**
     *  Makes a collection of academies available in this session.
     *
     *  @param  academies a collection of academies
     *  @throws org.osid.NullArgumentException <code>academies<code>
     *          is <code>null</code>
     */

    protected void putAcademies(java.util.Collection<? extends org.osid.recognition.Academy> academies) {
        for (org.osid.recognition.Academy academy : academies) {
            this.academies.put(academy.getId(), academy);
        }

        return;
    }


    /**
     *  Removes an Academy from this session.
     *
     *  @param  academyId the <code>Id</code> of the academy
     *  @throws org.osid.NullArgumentException <code>academyId<code> is
     *          <code>null</code>
     */

    protected void removeAcademy(org.osid.id.Id academyId) {
        this.academies.remove(academyId);
        return;
    }


    /**
     *  Gets the <code>Academy</code> specified by its <code>Id</code>.
     *
     *  @param  academyId <code>Id</code> of the <code>Academy</code>
     *  @return the academy
     *  @throws org.osid.NotFoundException <code>academyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>academyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recognition.Academy academy = this.academies.get(academyId);
        if (academy == null) {
            throw new org.osid.NotFoundException("academy not found: " + academyId);
        }

        return (academy);
    }


    /**
     *  Gets all <code>Academies</code>. In plenary mode, the returned
     *  list contains all known academies or an error
     *  results. Otherwise, the returned list may contain only those
     *  academies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Academies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.academy.ArrayAcademyList(this.academies.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.academies.clear();
        super.close();
        return;
    }
}
