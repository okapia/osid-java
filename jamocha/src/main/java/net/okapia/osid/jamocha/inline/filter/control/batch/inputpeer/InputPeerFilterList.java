//
// InputPeerFilterList.java
//
//     Implements a filtering InputPeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.control.batch.inputpeer;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering InputPeerList.
 */

public final class InputPeerFilterList
    extends net.okapia.osid.jamocha.inline.filter.control.batch.inputpeer.spi.AbstractInputPeerFilterList
    implements org.osid.control.batch.InputPeerList,
               InputPeerFilter {

    private final InputPeerFilter filter;


    /**
     *  Creates a new <code>InputPeerFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list an <code>InputPeerList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public InputPeerFilterList(InputPeerFilter filter, org.osid.control.batch.InputPeerList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters InputPeers.
     *
     *  @param inputPeer the input peer to filter
     *  @return <code>true</code> if the input peer passes the filter,
     *          <code>false</code> if the input peer should be filtered
     */

    @Override
    public boolean pass(org.osid.control.batch.InputPeer inputPeer) {
        return (this.filter.pass(inputPeer));
    }
}
