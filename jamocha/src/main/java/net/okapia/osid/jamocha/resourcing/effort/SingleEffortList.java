//
// SingleEffortList.java
//
//     Implements an EffortList based on a single Effort.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.effort;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements an ObjectList based on a single Effort.
 */

public final class SingleEffortList
    extends net.okapia.osid.jamocha.resourcing.effort.spi.AbstractEffortList
    implements org.osid.resourcing.EffortList {

    private final org.osid.resourcing.Effort effort;
    private boolean read = false;


    /**
     *  Creates a new <code>SingleEffortList</code>.
     *
     *  @param effort an effort
     *  @throws org.osid.NullArgumentException <code>effort</code>
     *          is <code>null</code>
     */

    public SingleEffortList(org.osid.resourcing.Effort effort) {
        nullarg(effort, "effort");
        this.effort = effort;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>true</code> if more elements are available in
     *          this list, <code>false</code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.read) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code>Effort</code> in this list. 
     *
     *  @return the next <code>Effort</code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code>Effort</code> is available before
     *          calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Effort getNextEffort()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.read = true;
            return (this.effort);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in effort list");
        }
    }
}
