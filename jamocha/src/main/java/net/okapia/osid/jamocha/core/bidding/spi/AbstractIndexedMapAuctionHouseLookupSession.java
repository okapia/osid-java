//
// AbstractIndexedMapAuctionHouseLookupSession.java
//
//    A simple framework for providing an AuctionHouse lookup service
//    backed by a fixed collection of auction houses with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AuctionHouse lookup service backed by a
 *  fixed collection of auction houses. The auction houses are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some auction houses may be compatible
 *  with more types than are indicated through these auction house
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionHouses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuctionHouseLookupSession
    extends AbstractMapAuctionHouseLookupSession
    implements org.osid.bidding.AuctionHouseLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.bidding.AuctionHouse> auctionHousesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.AuctionHouse>());
    private final MultiMap<org.osid.type.Type, org.osid.bidding.AuctionHouse> auctionHousesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.AuctionHouse>());


    /**
     *  Makes an <code>AuctionHouse</code> available in this session.
     *
     *  @param  auctionHouse an auction house
     *  @throws org.osid.NullArgumentException <code>auctionHouse<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        super.putAuctionHouse(auctionHouse);

        this.auctionHousesByGenus.put(auctionHouse.getGenusType(), auctionHouse);
        
        try (org.osid.type.TypeList types = auctionHouse.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionHousesByRecord.put(types.getNextType(), auctionHouse);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an auction house from this session.
     *
     *  @param auctionHouseId the <code>Id</code> of the auction house
     *  @throws org.osid.NullArgumentException <code>auctionHouseId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuctionHouse(org.osid.id.Id auctionHouseId) {
        org.osid.bidding.AuctionHouse auctionHouse;
        try {
            auctionHouse = getAuctionHouse(auctionHouseId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auctionHousesByGenus.remove(auctionHouse.getGenusType());

        try (org.osid.type.TypeList types = auctionHouse.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionHousesByRecord.remove(types.getNextType(), auctionHouse);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuctionHouse(auctionHouseId);
        return;
    }


    /**
     *  Gets an <code>AuctionHouseList</code> corresponding to the given
     *  auction house genus <code>Type</code> which does not include
     *  auction houses of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known auction houses or an error results. Otherwise,
     *  the returned list may contain only those auction houses that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auctionHouseGenusType an auction house genus type 
     *  @return the returned <code>AuctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.auctionhouse.ArrayAuctionHouseList(this.auctionHousesByGenus.get(auctionHouseGenusType)));
    }


    /**
     *  Gets an <code>AuctionHouseList</code> containing the given
     *  auction house record <code>Type</code>. In plenary mode, the
     *  returned list contains all known auction houses or an error
     *  results. Otherwise, the returned list may contain only those
     *  auction houses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auctionHouseRecordType an auction house record type 
     *  @return the returned <code>auctionHouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByRecordType(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.auctionhouse.ArrayAuctionHouseList(this.auctionHousesByRecord.get(auctionHouseRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionHousesByGenus.clear();
        this.auctionHousesByRecord.clear();

        super.close();

        return;
    }
}
