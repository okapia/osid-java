//
// AbstractAdapterOrganizationLookupSession.java
//
//    An Organization lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Organization lookup session adapter.
 */

public abstract class AbstractAdapterOrganizationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.personnel.OrganizationLookupSession {

    private final org.osid.personnel.OrganizationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOrganizationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOrganizationLookupSession(org.osid.personnel.OrganizationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Realm/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Realm Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the {@code Realm} associated with this session.
     *
     *  @return the {@code Realm} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform {@code Organization} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOrganizations() {
        return (this.session.canLookupOrganizations());
    }


    /**
     *  A complete view of the {@code Organization} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOrganizationView() {
        this.session.useComparativeOrganizationView();
        return;
    }


    /**
     *  A complete view of the {@code Organization} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOrganizationView() {
        this.session.usePlenaryOrganizationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include organizations in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    

    /**
     *  Only organizations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveOrganizationView() {
        this.session.useEffectiveOrganizationView();
        return;
    }
    

    /**
     *  All organizations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveOrganizationView() {
        this.session.useAnyEffectiveOrganizationView();
        return;
    }

     
    /**
     *  Gets the {@code Organization} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Organization} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Organization} and
     *  retained for compatibility.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @param organizationId {@code Id} of the {@code Organization}
     *  @return the organization
     *  @throws org.osid.NotFoundException {@code organizationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code organizationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Organization getOrganization(org.osid.id.Id organizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrganization(organizationId));
    }


    /**
     *  Gets an {@code OrganizationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  organizations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Organizations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @param  organizationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Organization} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code organizationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByIds(org.osid.id.IdList organizationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrganizationsByIds(organizationIds));
    }


    /**
     *  Gets an {@code OrganizationList} corresponding to the given
     *  organization genus {@code Type} which does not include
     *  organizations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @param  organizationGenusType an organization genus type 
     *  @return the returned {@code Organization} list
     *  @throws org.osid.NullArgumentException
     *          {@code organizationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByGenusType(org.osid.type.Type organizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrganizationsByGenusType(organizationGenusType));
    }


    /**
     *  Gets an {@code OrganizationList} corresponding to the given
     *  organization genus {@code Type} and include any additional
     *  organizations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @param  organizationGenusType an organization genus type 
     *  @return the returned {@code Organization} list
     *  @throws org.osid.NullArgumentException
     *          {@code organizationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByParentGenusType(org.osid.type.Type organizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrganizationsByParentGenusType(organizationGenusType));
    }


    /**
     *  Gets an {@code OrganizationList} containing the given
     *  organization record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @param  organizationRecordType an organization record type 
     *  @return the returned {@code Organization} list
     *  @throws org.osid.NullArgumentException
     *          {@code organizationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByRecordType(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrganizationsByRecordType(organizationRecordType));
    }


    /**
     *  Gets an {@code OrganizationList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible
     *  through this session.
     *  
     *  In active mode, organizations are returned that are currently
     *  active. In any status mode, active and inactive organizations
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Organization} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrganizationsOnDate(from, to));
    }
        

    /**
     *  Gets all {@code Organizations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  organizations or an error results. Otherwise, the returned list
     *  may contain only those organizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, organizations are returned that are currently
     *  effective.  In any effective mode, effective organizations and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Organizations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOrganizations());
    }
}
