//
// AbstractOsidForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidForm to extend. 
 *
 *  The form Id is generated from {@code makeId()} using UUID. This
 *  method can be overidden to generate different kinds of Ids.
 *
 *  Validation is performed at the time any field is set. Additional
 *  side-effect validations mey be performed by overriding isValid().
 */

public abstract class AbstractOsidForm
    extends AbstractIdentifiable
    implements org.osid.OsidForm {
    
    private final boolean update;
    private final org.osid.locale.Locale defaultLocale;
    private final java.util.Collection<org.osid.locale.Locale> locales = new java.util.ArrayList<>();

    private org.osid.locale.Locale locale;

    private org.osid.Metadata journalCommentMetadata = getDefaultStringMetadata(getJournalCommentId(), "journal comment");
    private String journalComment;


    /** 
     *  Constructs a new {@code AbstractOsidForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidForm(org.osid.locale.Locale locale, boolean update) {
        nullarg(locale, "default locale");

        setId(makeId());

        this.defaultLocale = locale;
        this.locale = locale;
        this.update = update;

        return;
    }


    /**
     *  Tests if this form is for an update operation. 
     *
     *  @return <code> true </code> if this form is for an update operation, 
     *          <code> false </code> if for a create operation 
     */
                                                
    @OSID @Override
    public boolean isForUpdate() {
        return (this.update);
    }


    /**
     *  Gets a default locale for <code> DisplayTexts </code> when a
     *  locale is not specified.
     *
     *  @return the default locale 
     */

    @OSID @Override
    public org.osid.locale.Locale getDefaultLocale() {
        return (this.defaultLocale);
    }


    /**
     *  Gets a list of locales for available <code> DisplayText
     *  </code> translations that can be performed using this form.
     *
     *  @return a list of available locales or an empty list if no
     *          translation operations are available
     */

    @OSID @Override
    public org.osid.locale.LocaleList getLocales() {
        return (new net.okapia.osid.jamocha.locale.locale.ArrayLocaleList(this.locales));
    }


    /**
     *  Gets the current locale.
     *
     *  @return the current locale
     */

    protected org.osid.locale.Locale getLocale() {
        return (this.locale);
    }


    /**
     *  Adds an available locale.
     *
     *  @param locale a locale
     *  @throws org.osid.NullArgumentException {@code locale} is
     *          {@code null}
     */

    protected void addLocale(org.osid.locale.Locale locale) {
        nullarg(locale, "available locale");
        this.locales.add(locale);
        return;
    }


    /**
     *  Adds a collection of available locales.
     *
     *  @param locales a collection of locales
     *  @throws org.osid.NullArgumentException {@code locales} is
     *          {@code null}
     */

    protected void addLocales(java.util.Collection<org.osid.locale.Locale> locales) {
        nullarg(locales, "available locales");
        this.locales.addAll(locales);
        return;
    }


    /**
     *  Clears the available locales.
     */

    protected void clearLocales() {
        this.locales.clear();
        return;
    }


    /**
     *  Specifies a language and script type for <code> DisplayText
     *  </code> fields in this form. Setting a locale to something
     *  other than the default locale may affect the <code> Metadata
     *  </code> in this form.
     *  
     *  If multiple locales are available for managing translations,
     *  the <code> Metadata </code> indicates the fields are unset as
     *  they may be returning a defeult value based on the default
     *  locale.
     *
     *  @param  languageType the language type 
     *  @param  scriptType the script type 
     *  @throws org.osid.NullArgumentException <code> languageType
     *          </code> or <code> scriptType </code> is null
     *  @throws org.osid.UnsupportedException <code> languageType
     *          </code> and <code> scriptType </code> not available
     *          from <code> getLocales() </code>
     */

    @OSID @Override
    public void setLocale(org.osid.type.Type languageType, 
                          org.osid.type.Type scriptType) {

        try (org.osid.locale.LocaleList locales  = getLocales()) {
            while (locales.hasNext()) {
                try {
                    org.osid.locale.Locale locale = locales.getNextLocale();
                    if (locale.getLanguageType().equals(languageType) &&
                        locale.getScriptType().equals(scriptType)) {
                        this.locale = locale;
                        return;
                    }
                } catch (org.osid.OperationFailedException ofe) {}
            }
        }

        throw new org.osid.UnsupportedException("locale not supported");
    }
        

    /**
     *  Gets the metadata for the comment corresponding to this form
     *  submission. The comment is used for describing the nature of
     *  the change to the corresponding object for the purposes of
     *  logging and auditing.
     *
     *  @return metadata for the comment 
     */

    @OSID @Override
    public org.osid.Metadata getJournalCommentMetadata() {
        return (this.journalCommentMetadata);
    }


    /**
     *  Sets the comment metadata.
     *
     *  @param metadata the comment metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setJournalCommentMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "journal comment metadata");
        this.journalCommentMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the comment field.
     *
     *  @return the comment field Id
     */

    protected org.osid.id.Id getJournalCommentId() {
        return (makeElementId("osid.OsidForm.JournalComment"));
    }

    
    /**
     *  Sets a comment. 
     *
     *  @param  comment the new comment 
     *  @throws org.osid.InvalidArgumentException <code> comment </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> comment </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setJournalComment(String comment) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(comment, getJournalCommentMetadata());
        this.journalComment = comment;
        return;
    }


    /**
     *  Tests if a comment has been set in this form.
     *
     *  @return {@code true} if the comment has been set, {@code
     *          false} otherwise
     */

    protected boolean isJournalCommentSet() {
        return (this.journalComment != null);
    }


    /**
     *  Returns the current comment value.
     *
     *  @return the comment value or {@code null} if {@code
     *          isCommentSet()} is {@code false}
     *  @throws org.osid.IllegalStateException {@code isCommentSet()} is
     *          {@code false}
     */

    protected String getJournalComment() {
        if (!isJournalCommentSet()) {
            throw new org.osid.IllegalStateException("isCommentSet is false");
        }

        return (this.journalComment);
    }


    /**
     *  Tests if ths form is in a valid state for submission. A form
     *  is valid if all required data has been supplied compliant with
     *  any constraints.
     *
     *  This implementation validates data with corresponding metadata
     *  on the way in. Override this method to introduce any
     *  side-effect validation not captured by metadata.
     *
     *  @return <code> false </code> if there is a known error in this
     *          form, <code> true </code> otherwise
     *  @throws org.osid.OperationFailedException attempt to perform 
     *          validation failed 
     */

    @OSID @Override
    public boolean isValid()
        throws org.osid.OperationFailedException {

        return (true);
    }


    /**
     *  Gets text messages corresponding to additional instructions to
     *  pass form validation.
     *
     *  This implementation validates data with corresponding metadata
     *  on the way in. Override this method to introduce any
     *  side-effect validation not captured by metadata.
     *
     *  @return a message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText[] getValidationMessages() {
        return (new org.osid.locale.DisplayText[0]);
    }


    /**
     *  Gets a list of metadata for the elements in this form which
     *  are not valid.
     *
     *  This implementation validates data with corresponding metadata
     *  on the way in. Override this method to introduce any
     *  side-effect validation not captured by metadata.
     *
     *  @return invalid metadata 
     */

    @OSID @Override
    public org.osid.Metadata[] getInvalidMetadata() {
        return (new org.osid.Metadata[0]);
    }


    /**
     *  Generates a unique Id for this form.
     *
     *  @return a unique Id
     */

    protected org.osid.id.Id makeId() {
        return (new net.okapia.osid.primordium.id.BasicId("okapia.net", "uuid", 
                                                          java.util.UUID.randomUUID().toString()));
    }


    /**
     *  Makes an Id for a form element.
     *
     *  @param identifier the form element
     *  @return the Id
     */

    protected org.osid.id.Id makeElementId(String identifier) {
        return (new net.okapia.osid.primordium.id.BasicId("okapia.net", "form element", identifier));
    }


    /**
     *  Gets a default read-only metadata for booleans.
     *
     *  @param elementId the Id of the element
     *  @param field the name of the field
     *  @return metadata
     *  @throws org.osid.NullArgumentException {@code elementId} or
     *          {@code field} is {@code null}
     */

    protected org.osid.Metadata getDefaultBooleanMetadata(org.osid.id.Id elementId, String field) {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyBooleanMetadata(elementId,
                                                                             Plain.valueOf(field),
                                                                             Plain.valueOf("unmanaged field"),
                                                                             false));
    }


    /**
     *  Gets a default read-only metadata for date times.
     *
     *  @param elementId the Id of the element
     *  @param field the name of the field
     *  @return metadata
     *  @throws org.osid.NullArgumentException {@code elementId} or
     *          {@code field} is {@code null}
     */

    protected org.osid.Metadata getDefaultDateTimeMetadata(org.osid.id.Id elementId, String field) {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyDateTimeMetadata(elementId,
                                                                              Plain.valueOf(field),
                                                                              Plain.valueOf("unmanaged field"),
                                                                              false));
    }


    /**
     *  Gets a default read-only metadata for Ids.
     *
     *  @param elementId the Id of the element
     *  @param field the name of the field
     *  @return metadata
     *  @throws org.osid.NullArgumentException {@code elementId} or
     *          {@code field} is {@code null}
     */

    protected org.osid.Metadata getDefaultIdMetadata(org.osid.id.Id elementId, String field) {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyIdMetadata(elementId, 
                                                                        Plain.valueOf(field),
                                                                        Plain.valueOf("unmanaged field"),
                                                                        false));
    }


    /**
     *  Gets a default read-only metadata for Ids.
     *
     *  @param elementId the Id of the element
     *  @param field the name of the field
     *  @return metadata
     *  @throws org.osid.NullArgumentException {@code elementId} or
     *          {@code field} is {@code null}
     */

    protected org.osid.Metadata getDefaultArrayIdMetadata(org.osid.id.Id elementId, String field) {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyArrayIdMetadata(elementId, 
                                                                             Plain.valueOf(field),
                                                                             Plain.valueOf("unmanaged field"),
                                                                             false));
    }


    /**
     *  Gets a default read-only metadata for strings.
     *
     *  @param elementId the Id of the element
     *  @param field the name of the field
     *  @return metadata
     *  @throws org.osid.NullArgumentException {@code elementId} or
     *          {@code field} is {@code null}
     */

    protected org.osid.Metadata getDefaultStringMetadata(org.osid.id.Id elementId, String field) {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyStringMetadata(elementId, 
                                                                            Plain.valueOf(field),
                                                                            Plain.valueOf("unmanaged field"),
                                                                            false));
    }


    /**
     *  Gets a default read-only metadata for types.
     *
     *  @param elementId the Id of the element
     *  @param field the name of the field
     *  @return metadata
     *  @throws org.osid.NullArgumentException {@code elementId} or
     *          {@code field} is {@code null}
     */

    protected org.osid.Metadata getDefaultTypeMetadata(org.osid.id.Id elementId, String field) {
        return (new net.okapia.osid.jamocha.metadata.ReadOnlyTypeMetadata(elementId, 
                                                                          Plain.valueOf(field),
                                                                          Plain.valueOf("unmanaged field"),
                                                                          false));
    }
}
