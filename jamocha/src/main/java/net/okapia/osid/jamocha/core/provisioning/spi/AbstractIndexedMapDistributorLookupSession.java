//
// AbstractIndexedMapDistributorLookupSession.java
//
//    A simple framework for providing a Distributor lookup service
//    backed by a fixed collection of distributors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Distributor lookup service backed by a
 *  fixed collection of distributors. The distributors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some distributors may be compatible
 *  with more types than are indicated through these distributor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Distributors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDistributorLookupSession
    extends AbstractMapDistributorLookupSession
    implements org.osid.provisioning.DistributorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Distributor> distributorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Distributor>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Distributor> distributorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Distributor>());


    /**
     *  Makes a <code>Distributor</code> available in this session.
     *
     *  @param  distributor a distributor
     *  @throws org.osid.NullArgumentException <code>distributor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDistributor(org.osid.provisioning.Distributor distributor) {
        super.putDistributor(distributor);

        this.distributorsByGenus.put(distributor.getGenusType(), distributor);
        
        try (org.osid.type.TypeList types = distributor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.distributorsByRecord.put(types.getNextType(), distributor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a distributor from this session.
     *
     *  @param distributorId the <code>Id</code> of the distributor
     *  @throws org.osid.NullArgumentException <code>distributorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDistributor(org.osid.id.Id distributorId) {
        org.osid.provisioning.Distributor distributor;
        try {
            distributor = getDistributor(distributorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.distributorsByGenus.remove(distributor.getGenusType());

        try (org.osid.type.TypeList types = distributor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.distributorsByRecord.remove(types.getNextType(), distributor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDistributor(distributorId);
        return;
    }


    /**
     *  Gets a <code>DistributorList</code> corresponding to the given
     *  distributor genus <code>Type</code> which does not include
     *  distributors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known distributors or an error results. Otherwise,
     *  the returned list may contain only those distributors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  distributorGenusType a distributor genus type 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByGenusType(org.osid.type.Type distributorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.distributor.ArrayDistributorList(this.distributorsByGenus.get(distributorGenusType)));
    }


    /**
     *  Gets a <code>DistributorList</code> containing the given
     *  distributor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known distributors or an error
     *  results. Otherwise, the returned list may contain only those
     *  distributors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  distributorRecordType a distributor record type 
     *  @return the returned <code>distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByRecordType(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.distributor.ArrayDistributorList(this.distributorsByRecord.get(distributorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.distributorsByGenus.clear();
        this.distributorsByRecord.clear();

        super.close();

        return;
    }
}
