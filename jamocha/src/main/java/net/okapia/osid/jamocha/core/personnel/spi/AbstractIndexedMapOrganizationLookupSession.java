//
// AbstractIndexedMapOrganizationLookupSession.java
//
//    A simple framework for providing an Organization lookup service
//    backed by a fixed collection of organizations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Organization lookup service backed by a
 *  fixed collection of organizations. The organizations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some organizations may be compatible
 *  with more types than are indicated through these organization
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Organizations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOrganizationLookupSession
    extends AbstractMapOrganizationLookupSession
    implements org.osid.personnel.OrganizationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.personnel.Organization> organizationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Organization>());
    private final MultiMap<org.osid.type.Type, org.osid.personnel.Organization> organizationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Organization>());


    /**
     *  Makes an <code>Organization</code> available in this session.
     *
     *  @param  organization an organization
     *  @throws org.osid.NullArgumentException <code>organization<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOrganization(org.osid.personnel.Organization organization) {
        super.putOrganization(organization);

        this.organizationsByGenus.put(organization.getGenusType(), organization);
        
        try (org.osid.type.TypeList types = organization.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.organizationsByRecord.put(types.getNextType(), organization);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an organization from this session.
     *
     *  @param organizationId the <code>Id</code> of the organization
     *  @throws org.osid.NullArgumentException <code>organizationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOrganization(org.osid.id.Id organizationId) {
        org.osid.personnel.Organization organization;
        try {
            organization = getOrganization(organizationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.organizationsByGenus.remove(organization.getGenusType());

        try (org.osid.type.TypeList types = organization.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.organizationsByRecord.remove(types.getNextType(), organization);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOrganization(organizationId);
        return;
    }


    /**
     *  Gets an <code>OrganizationList</code> corresponding to the given
     *  organization genus <code>Type</code> which does not include
     *  organizations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known organizations or an error results. Otherwise,
     *  the returned list may contain only those organizations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  organizationGenusType an organization genus type 
     *  @return the returned <code>Organization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>organizationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByGenusType(org.osid.type.Type organizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.organization.ArrayOrganizationList(this.organizationsByGenus.get(organizationGenusType)));
    }


    /**
     *  Gets an <code>OrganizationList</code> containing the given
     *  organization record <code>Type</code>. In plenary mode, the
     *  returned list contains all known organizations or an error
     *  results. Otherwise, the returned list may contain only those
     *  organizations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  organizationRecordType an organization record type 
     *  @return the returned <code>organization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationList getOrganizationsByRecordType(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.organization.ArrayOrganizationList(this.organizationsByRecord.get(organizationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.organizationsByGenus.clear();
        this.organizationsByRecord.clear();

        super.close();

        return;
    }
}
