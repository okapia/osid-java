//
// AbstractCommission.java
//
//     Defines a Commission builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.commission.spi;


/**
 *  Defines a <code>Commission</code> builder.
 */

public abstract class AbstractCommissionBuilder<T extends AbstractCommissionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resourcing.commission.CommissionMiter commission;


    /**
     *  Constructs a new <code>AbstractCommissionBuilder</code>.
     *
     *  @param commission the commission to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCommissionBuilder(net.okapia.osid.jamocha.builder.resourcing.commission.CommissionMiter commission) {
        super(commission);
        this.commission = commission;
        return;
    }


    /**
     *  Builds the commission.
     *
     *  @return the new commission
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resourcing.Commission build() {
        (new net.okapia.osid.jamocha.builder.validator.resourcing.commission.CommissionValidator(getValidations())).validate(this.commission);
        return (new net.okapia.osid.jamocha.builder.resourcing.commission.ImmutableCommission(this.commission));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the commission miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resourcing.commission.CommissionMiter getMiter() {
        return (this.commission);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the work.
     *
     *  @param work a work
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>work</code> is
     *          <code>null</code>
     */

    public T work(org.osid.resourcing.Work work) {
        getMiter().setWork(work);
        return (self());
    }


    /**
     *  Sets the competency.
     *
     *  @param competency a competency
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    public T competency(org.osid.resourcing.Competency competency) {
        getMiter().setCompetency(competency);
        return (self());
    }


    /**
     *  Sets the percentage.
     *
     *  @param percentage a percentage
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    public T percentage(long percentage) {
        getMiter().setPercentage(percentage);
        return (self());
    }


    /**
     *  Adds a Commission record.
     *
     *  @param record a commission record
     *  @param recordType the type of commission record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resourcing.records.CommissionRecord record, org.osid.type.Type recordType) {
        getMiter().addCommissionRecord(record, recordType);
        return (self());
    }
}       


