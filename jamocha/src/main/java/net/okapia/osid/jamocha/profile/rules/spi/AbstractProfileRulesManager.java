//
// AbstractProfileRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractProfileRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.profile.rules.ProfileRulesManager,
               org.osid.profile.rules.ProfileRulesProxyManager {

    private final Types profileEntryEnablerRecordTypes     = new TypeRefSet();
    private final Types profileEntryEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractProfileRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractProfileRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up profile entry enablers is supported. 
     *
     *  @return <code> true </code> if profile entry enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying profile entry enablers is supported. 
     *
     *  @return <code> true </code> if profile entry enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching profile entry enablers is supported. 
     *
     *  @return <code> true </code> if profile entry enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a profile entry enabler administrative service is supported. 
     *
     *  @return <code> true </code> if profile entry enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a profile entry enabler notification service is supported. 
     *
     *  @return <code> true </code> if profile entry enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a profile entry enabler profile lookup service is supported. 
     *
     *  @return <code> true </code> if a profile entry enabler profile lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerProfile() {
        return (false);
    }


    /**
     *  Tests if a profile entry enabler profile service is supported. 
     *
     *  @return <code> true </code> if profile entry enabler profile 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerProfileAssignment() {
        return (false);
    }


    /**
     *  Tests if a profile entry enabler profile lookup service is supported. 
     *
     *  @return <code> true </code> if a profile entry enabler profile service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerSmartProfile() {
        return (false);
    }


    /**
     *  Tests if a profile entry enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a profile entry enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a profile entry enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if profile entry enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> ProfileEntryEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ProfileEntryEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntryEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileEntryEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProfileEntryEnabler </code> record type is 
     *  supported. 
     *
     *  @param  profileEntryEnablerRecordType a <code> Type </code> indicating 
     *          a <code> ProfileEntryEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerRecordType(org.osid.type.Type profileEntryEnablerRecordType) {
        return (this.profileEntryEnablerRecordTypes.contains(profileEntryEnablerRecordType));
    }


    /**
     *  Adds support for a profile entry enabler record type.
     *
     *  @param profileEntryEnablerRecordType a profile entry enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryEnablerRecordType</code> is <code>null</code>
     */

    protected void addProfileEntryEnablerRecordType(org.osid.type.Type profileEntryEnablerRecordType) {
        this.profileEntryEnablerRecordTypes.add(profileEntryEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a profile entry enabler record type.
     *
     *  @param profileEntryEnablerRecordType a profile entry enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryEnablerRecordType</code> is <code>null</code>
     */

    protected void removeProfileEntryEnablerRecordType(org.osid.type.Type profileEntryEnablerRecordType) {
        this.profileEntryEnablerRecordTypes.remove(profileEntryEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProfileEntryEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> ProfileEntryEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntryEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileEntryEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProfileEntryEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  profileEntryEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> ProfileEntryEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntryEnablerSearchRecordType(org.osid.type.Type profileEntryEnablerSearchRecordType) {
        return (this.profileEntryEnablerSearchRecordTypes.contains(profileEntryEnablerSearchRecordType));
    }


    /**
     *  Adds support for a profile entry enabler search record type.
     *
     *  @param profileEntryEnablerSearchRecordType a profile entry enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addProfileEntryEnablerSearchRecordType(org.osid.type.Type profileEntryEnablerSearchRecordType) {
        this.profileEntryEnablerSearchRecordTypes.add(profileEntryEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a profile entry enabler search record type.
     *
     *  @param profileEntryEnablerSearchRecordType a profile entry enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeProfileEntryEnablerSearchRecordType(org.osid.type.Type profileEntryEnablerSearchRecordType) {
        this.profileEntryEnablerSearchRecordTypes.remove(profileEntryEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler lookup service. 
     *
     *  @return a <code> ProfileEntryEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerLookupSession getProfileEntryEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerLookupSession getProfileEntryEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerLookupSession getProfileEntryEnablerLookupSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerLookupSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerLookupSession getProfileEntryEnablerLookupSessionForProfile(org.osid.id.Id profileId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerLookupSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler query service. 
     *
     *  @return a <code> ProfileEntryEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerQuerySession getProfileEntryEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerQuerySession getProfileEntryEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler query service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerQuerySession getProfileEntryEnablerQuerySessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerQuerySessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler query service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerQuerySession getProfileEntryEnablerQuerySessionForProfile(org.osid.id.Id profileId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerQuerySessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler search service. 
     *
     *  @return a <code> ProfileEntryEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerSearchSession getProfileEntryEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerSearchSession getProfileEntryEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enablers earch service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerSearchSession getProfileEntryEnablerSearchSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerSearchSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enablers earch service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerSearchSession getProfileEntryEnablerSearchSessionForProfile(org.osid.id.Id profileId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerSearchSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler administration service. 
     *
     *  @return a <code> ProfileEntryEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerAdminSession getProfileEntryEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerAdminSession getProfileEntryEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler administration service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerAdminSession getProfileEntryEnablerAdminSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerAdminSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler administration service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerAdminSession getProfileEntryEnablerAdminSessionForProfile(org.osid.id.Id profileId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerAdminSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler notification service. 
     *
     *  @param  profileEntryEnablerReceiver the notification callback 
     *  @return a <code> ProfileEntryEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerNotificationSession getProfileEntryEnablerNotificationSession(org.osid.profile.rules.ProfileEntryEnablerReceiver profileEntryEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler notification service. 
     *
     *  @param  profileEntryEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerNotificationSession getProfileEntryEnablerNotificationSession(org.osid.profile.rules.ProfileEntryEnablerReceiver profileEntryEnablerReceiver, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler notification service for the given profile. 
     *
     *  @param  profileEntryEnablerReceiver the notification callback 
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no profile found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryEnablerReceiver </code> or <code> profileId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerNotificationSession getProfileEntryEnablerNotificationSessionForProfile(org.osid.profile.rules.ProfileEntryEnablerReceiver profileEntryEnablerReceiver, 
                                                                                                                             org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerNotificationSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler notification service for the given profile. 
     *
     *  @param  profileEntryEnablerReceiver the notification callback 
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no profile found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryEnablerReceiver, profileId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerNotificationSession getProfileEntryEnablerNotificationSessionForProfile(org.osid.profile.rules.ProfileEntryEnablerReceiver profileEntryEnablerReceiver, 
                                                                                                                             org.osid.id.Id profileId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerNotificationSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup profile entry 
     *  enabler/profile mappings for profile entry enablers. 
     *
     *  @return a <code> ProfileEntryEnablerProfileSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerProfileSession getProfileEntryEnablerProfileSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerProfileSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup profile entry 
     *  enabler/profile mappings for profile entry enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerProfileSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerProfileSession getProfileEntryEnablerProfileSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerProfileSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning profile 
     *  entry enablers to profiles for profile entry. 
     *
     *  @return a <code> ProfileEntryEnablerProfileAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerProfileAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerProfileAssignmentSession getProfileEntryEnablerProfileAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerProfileAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning profile 
     *  entry enablers to profiles for profile entry. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerProfileAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerProfileAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerProfileAssignmentSession getProfileEntryEnablerProfileAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerProfileAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage profile entry enabler 
     *  smart profiles. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerSmartProfileSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerSmartProfile() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerSmartProfileSession getProfileEntryEnablerSmartProfileSession(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerSmartProfileSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage profile entry enabler 
     *  smart profiles. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerSmartProfileSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerSmartProfile() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerSmartProfileSession getProfileEntryEnablerSmartProfileSession(org.osid.id.Id profileId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerSmartProfileSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler mapping lookup service for looking up the rules applied to a 
     *  profile entry. 
     *
     *  @return a <code> ProfileEntryEnablerRuleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleLookupSession getProfileEntryEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  profile. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleLookupSession getProfileEntryEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler mapping lookup service for the given profile for looking up 
     *  rules applied to a profile entry. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleLookupSession getProfileEntryEnablerRuleLookupSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerRuleLookupSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler mapping lookup service for the given profile for looking up 
     *  rules applied to a profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnableRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleLookupSession getProfileEntryEnablerRuleLookupSessionForProfile(org.osid.id.Id profileId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerRuleLookupSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler assignment service to apply enablers to profile entries. 
     *
     *  @return a <code> ProfileEntryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleApplicationSession getProfileEntryEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler assignment service to apply enablers to profiles. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleApplicationSession getProfileEntryEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler assignment service for the given profile to apply enablers to 
     *  profile entries. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleApplicationSession getProfileEntryEnablerRuleApplicationSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesManager.getProfileEntryEnablerRuleApplicationSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  enabler assignment service for the given profile to apply enablers to 
     *  profiles. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryEnableryRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerRuleApplicationSession getProfileEntryEnablerRuleApplicationSessionForProfile(org.osid.id.Id profileId, 
                                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.rules.ProfileRulesProxyManager.getProfileEntryEnablerRuleApplicationSessionForProfile not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.profileEntryEnablerRecordTypes.clear();
        this.profileEntryEnablerRecordTypes.clear();

        this.profileEntryEnablerSearchRecordTypes.clear();
        this.profileEntryEnablerSearchRecordTypes.clear();

        return;
    }
}
