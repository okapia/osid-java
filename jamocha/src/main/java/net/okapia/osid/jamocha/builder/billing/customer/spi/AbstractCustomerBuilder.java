//
// AbstractCustomer.java
//
//     Defines a Customer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.customer.spi;


/**
 *  Defines a <code>Customer</code> builder.
 */

public abstract class AbstractCustomerBuilder<T extends AbstractCustomerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.billing.customer.CustomerMiter customer;


    /**
     *  Constructs a new <code>AbstractCustomerBuilder</code>.
     *
     *  @param customer the customer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCustomerBuilder(net.okapia.osid.jamocha.builder.billing.customer.CustomerMiter customer) {
        super(customer);
        this.customer = customer;
        return;
    }


    /**
     *  Builds the customer.
     *
     *  @return the new customer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.billing.Customer build() {
        (new net.okapia.osid.jamocha.builder.validator.billing.customer.CustomerValidator(getValidations())).validate(this.customer);
        return (new net.okapia.osid.jamocha.builder.billing.customer.ImmutableCustomer(this.customer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the customer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.billing.customer.CustomerMiter getMiter() {
        return (this.customer);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the customer number.
     *
     *  @param number a customer number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T customerNumber(String number) {
        getMiter().setCustomerNumber(number);
        return (self());
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>activity</code> is <code>null</code>
     */

    public T activity(org.osid.financials.Activity activity) {
        getMiter().setActivity(activity);
        return (self());
    }


    /**
     *  Adds a Customer record.
     *
     *  @param record a customer record
     *  @param recordType the type of customer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.billing.records.CustomerRecord record, org.osid.type.Type recordType) {
        getMiter().addCustomerRecord(record, recordType);
        return (self());
    }
}       


