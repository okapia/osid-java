//
// AbstractProficiencySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProficiencySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.learning.ProficiencySearchResults {

    private org.osid.learning.ProficiencyList proficiencies;
    private final org.osid.learning.ProficiencyQueryInspector inspector;
    private final java.util.Collection<org.osid.learning.records.ProficiencySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProficiencySearchResults.
     *
     *  @param proficiencies the result set
     *  @param proficiencyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>proficiencies</code>
     *          or <code>proficiencyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProficiencySearchResults(org.osid.learning.ProficiencyList proficiencies,
                                            org.osid.learning.ProficiencyQueryInspector proficiencyQueryInspector) {
        nullarg(proficiencies, "proficiencies");
        nullarg(proficiencyQueryInspector, "proficiency query inspectpr");

        this.proficiencies = proficiencies;
        this.inspector = proficiencyQueryInspector;

        return;
    }


    /**
     *  Gets the proficiency list resulting from a search.
     *
     *  @return a proficiency list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficiencies() {
        if (this.proficiencies == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.learning.ProficiencyList proficiencies = this.proficiencies;
        this.proficiencies = null;
	return (proficiencies);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.learning.ProficiencyQueryInspector getProficiencyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  proficiency search record <code> Type. </code> This method must
     *  be used to retrieve a proficiency implementing the requested
     *  record.
     *
     *  @param proficiencySearchRecordType a proficiency search 
     *         record type 
     *  @return the proficiency search
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(proficiencySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencySearchResultsRecord getProficiencySearchResultsRecord(org.osid.type.Type proficiencySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.learning.records.ProficiencySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(proficiencySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(proficiencySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record proficiency search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProficiencyRecord(org.osid.learning.records.ProficiencySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "proficiency record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
