//
// AbstractWorkflowBatchManager.java
//
//     An adapter for a WorkflowBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a WorkflowBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterWorkflowBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.workflow.batch.WorkflowBatchManager>
    implements org.osid.workflow.batch.WorkflowBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterWorkflowBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterWorkflowBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterWorkflowBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterWorkflowBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of processes is available. 
     *
     *  @return <code> true </code> if a process bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessBatchAdmin() {
        return (getAdapteeManager().supportsProcessBatchAdmin());
    }


    /**
     *  Tests if bulk administration of steps is available. 
     *
     *  @return <code> true </code> if a step bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepBatchAdmin() {
        return (getAdapteeManager().supportsStepBatchAdmin());
    }


    /**
     *  Tests if bulk administration of works is available. 
     *
     *  @return <code> true </code> if a work bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkBatchAdmin() {
        return (getAdapteeManager().supportsWorkBatchAdmin());
    }


    /**
     *  Tests if bulk administration of offices is available. 
     *
     *  @return <code> true </code> if an office bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeBatchAdmin() {
        return (getAdapteeManager().supportsOfficeBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk process 
     *  administration service. 
     *
     *  @return a <code> ProcessBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.ProcessBatchAdminSession getProcessBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk process 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.ProcessBatchAdminSession getProcessBatchAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessBatchAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk step 
     *  administration service. 
     *
     *  @return a <code> StepBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.StepBatchAdminSession getStepBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk step 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.StepBatchAdminSession getStepBatchAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepBatchAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk work 
     *  administration service. 
     *
     *  @return a <code> WorkBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.WorkBatchAdminSession getWorkBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk work 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> WorkBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.WorkBatchAdminSession getWorkBatchAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkBatchAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk office 
     *  administration service. 
     *
     *  @return a <code> OfficeBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfficeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.batch.OfficeBatchAdminSession getOfficeBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfficeBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
