//
// AbstractInstructionSearchOdrer.java
//
//     Defines an InstructionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.instruction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code InstructionSearchOrder}.
 */

public abstract class AbstractInstructionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerSearchOrder
    implements org.osid.rules.check.InstructionSearchOrder {

    private final java.util.Collection<org.osid.rules.check.records.InstructionSearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidRelationshipSearchOrder order = new OsidRelationshipSearchOrder();


    /**
     *  Specifies a preference for ordering the results by the end reason 
     *  state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByEndReason(org.osid.SearchOrderStyle style) {
        this.order.orderByEndReason(style);
        return;
    }


    /**
     *  Tests if a {@code StateSearchOrder} is available. 
     *
     *  @return {@code true} if a state search order is available, 
     *          {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsEndReasonSearchOrder() {
        return (this.order.supportsEndReasonSearchOrder());
    }


    /**
     *  Gets the search order for a state. 
     *
     *  @return the state search order 
     *  @throws org.osid.UnimplementedException {@code
     *          supportsEndReasonSearchOrder()} is {@code false}
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getEndReasonSearchOrder() {
        return (this.order.getEndReasonSearchOrder());
    }
    

    /**
     *  Orders the results by agenda. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgenda(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agenda search order is available. 
     *
     *  @return <code> true </code> if an agenda search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agenda search order. 
     *
     *  @return the agenda search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAgendaSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchOrder getAgendaSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgendaSearchOrder() is false");
    }


    /**
     *  Orders the results by check. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCheck(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a check search order is available. 
     *
     *  @return <code> true </code> if a check search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckSearchOrder() {
        return (false);
    }


    /**
     *  Gets the check search order. 
     *
     *  @return the check search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCheckSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchOrder getCheckSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCheckSearchOrder() is false");
    }


    /**
     *  Orders the results by message. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMessage(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the warning flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWarning(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the continue-on-fail flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByContinueOnFail(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  instructionRecordType an instruction record type 
     *  @return {@code true} if the instructionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code instructionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type instructionRecordType) {
        for (org.osid.rules.check.records.InstructionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  instructionRecordType the instruction record type 
     *  @return the instruction search order record
     *  @throws org.osid.NullArgumentException
     *          {@code instructionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(instructionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionSearchOrderRecord getInstructionSearchOrderRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.InstructionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this instruction. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param instructionRecord the instruction search odrer record
     *  @param instructionRecordType instruction record type
     *  @throws org.osid.NullArgumentException
     *          {@code instructionRecord} or
     *          {@code instructionRecordTypeinstruction} is
     *          {@code null}
     */
            
    protected void addInstructionRecord(org.osid.rules.check.records.InstructionSearchOrderRecord instructionSearchOrderRecord, 
                                     org.osid.type.Type instructionRecordType) {

        addRecordType(instructionRecordType);
        this.records.add(instructionSearchOrderRecord);
        
        return;
    }


    protected class OsidRelationshipSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
        implements org.osid.OsidRelationshipSearchOrder {
    }
}
