//
// AbstractProvisionReturn.java
//
//     Defines a ProvisionReturn builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.provisionreturn.spi;


/**
 *  Defines a <code>ProvisionReturn</code> builder.
 */

public abstract class AbstractProvisionReturnBuilder<T extends AbstractProvisionReturnBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.provisionreturn.ProvisionReturnMiter provisionReturn;


    /**
     *  Constructs a new <code>AbstractProvisionReturnBuilder</code>.
     *
     *  @param provisionReturn the provision return to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProvisionReturnBuilder(net.okapia.osid.jamocha.builder.provisioning.provisionreturn.ProvisionReturnMiter provisionReturn) {
        super(provisionReturn);
        this.provisionReturn = provisionReturn;
        return;
    }


    /**
     *  Builds the provision return.
     *
     *  @return the new provision return
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>provisionReturn</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.ProvisionReturn build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.provisionreturn.ProvisionReturnValidator(getValidations())).validate(this.provisionReturn);
        return (new net.okapia.osid.jamocha.builder.provisioning.provisionreturn.ImmutableProvisionReturn(this.provisionReturn));
    }


    /**
     *  Gets the provision return. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new provisionReturn
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.provisionreturn.ProvisionReturnMiter getMiter() {
        return (this.provisionReturn);
    }


    /**
     *  Sets the return date.
     *
     *  @param returnDate a return date
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>returnDate</code> is <code>null</code>
     */

    public T returnDate(org.osid.calendaring.DateTime returnDate) {
        getMiter().setReturnDate(returnDate);
        return (self());
    }


    /**
     *  Sets the returner.
     *
     *  @param returner a returner
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>returner</code> is <code>null</code>
     */

    public T returner(org.osid.resource.Resource returner) {
        getMiter().setReturner(returner);
        return (self());
    }


    /**
     *  Sets the returning agent.
     *
     *  @param returningAgent a returning agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>returningAgent</code> is <code>null</code>
     */

    public T returningAgent(org.osid.authentication.Agent returningAgent) {
        getMiter().setReturningAgent(returningAgent);
        return (self());
    }


    /**
     *  Adds a ProvisionReturn record.
     *
     *  @param record a provision return record
     *  @param recordType the type of provision return record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.records.ProvisionReturnRecord record, org.osid.type.Type recordType) {
        getMiter().addProvisionReturnRecord(record, recordType);
        return (self());
    }
}       


