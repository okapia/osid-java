//
// AbstractAdapterDepotLookupSession.java
//
//    A Depot lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.installation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Depot lookup session adapter.
 */

public abstract class AbstractAdapterDepotLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.installation.DepotLookupSession {

    private final org.osid.installation.DepotLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDepotLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDepotLookupSession(org.osid.installation.DepotLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Depot} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDepots() {
        return (this.session.canLookupDepots());
    }


    /**
     *  A complete view of the {@code Depot} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDepotView() {
        this.session.useComparativeDepotView();
        return;
    }


    /**
     *  A complete view of the {@code Depot} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDepotView() {
        this.session.usePlenaryDepotView();
        return;
    }

     
    /**
     *  Gets the {@code Depot} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Depot} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Depot} and
     *  retained for compatibility.
     *
     *  @param depotId {@code Id} of the {@code Depot}
     *  @return the depot
     *  @throws org.osid.NotFoundException {@code depotId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code depotId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDepot(depotId));
    }


    /**
     *  Gets a {@code DepotList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  depots specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Depots} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  depotIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Depot} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code depotIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByIds(org.osid.id.IdList depotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDepotsByIds(depotIds));
    }


    /**
     *  Gets a {@code DepotList} corresponding to the given
     *  depot genus {@code Type} which does not include
     *  depots of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  depotGenusType a depot genus type 
     *  @return the returned {@code Depot} list
     *  @throws org.osid.NullArgumentException
     *          {@code depotGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByGenusType(org.osid.type.Type depotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDepotsByGenusType(depotGenusType));
    }


    /**
     *  Gets a {@code DepotList} corresponding to the given
     *  depot genus {@code Type} and include any additional
     *  depots with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  depotGenusType a depot genus type 
     *  @return the returned {@code Depot} list
     *  @throws org.osid.NullArgumentException
     *          {@code depotGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByParentGenusType(org.osid.type.Type depotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDepotsByParentGenusType(depotGenusType));
    }


    /**
     *  Gets a {@code DepotList} containing the given
     *  depot record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  depotRecordType a depot record type 
     *  @return the returned {@code Depot} list
     *  @throws org.osid.NullArgumentException
     *          {@code depotRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByRecordType(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDepotsByRecordType(depotRecordType));
    }


    /**
     *  Gets a {@code DepotList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Depot} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDepotsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Depots}. 
     *
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Depots} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDepots());
    }
}
