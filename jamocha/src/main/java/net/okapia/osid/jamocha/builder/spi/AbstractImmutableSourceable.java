//
// AbstractImmutableSourceable
//
//     Defines an immutable wrapper for an OSID Object.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an immutable wrapper for an OSID Object.
 */

public abstract class AbstractImmutableSourceable
    implements org.osid.Sourceable {

    private final org.osid.Sourceable sourceable;


    /**
     *  Constructs a new <code>AbstractImmutableSourceable</code>.
     *
     *  @param sourceable
     *  @throws org.osid.NullArgumentException <code>sourceable</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSourceable(org.osid.Sourceable sourceable) {
        nullarg(sourceable, "sourceable");
        this.sourceable = sourceable;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Provider </code> of
     *  this <code> Catalog. </code>
     *
     *  @return the <code> Provider Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProviderId() {
        return (this.sourceable.getProviderId());
    }


    /**
     *  Gets the <code> Resource </code> representing the provider of
     *  this catalog.
     *
     *  @return the provider 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getProvider()
        throws org.osid.OperationFailedException {
        
        return (this.sourceable.getProvider());
    }


    /**
     *  Gets the branding asset Ids, such as an image or logo,
     *  expressed using the <code> Asset </code> interface.
     *
     *  @return a list of asset Ids
     */

    @OSID @Override
    public org.osid.id.IdList getBrandingIds() {
        return (this.sourceable.getBrandingIds());
    }
    

    /**
     *  Gets a branding, such as an image or logo, expressed using the
     *  <code> Asset </code> interface.
     *
     *  @return a list of assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getBranding()
        throws org.osid.OperationFailedException {

        return (this.sourceable.getBranding());
    }


    /**
     *  Gets the terms of usage. An empty license means the terms are
     *  unknown.
     *
     *  @return the license 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLicense() {
        return (this.sourceable.getLicense());
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an sourceable to compare
     *  @return <code> true </code> if the given sourceable is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public boolean equals(Object obj) {
        return (this.sourceable.equals(obj));
    }


    /**
     *  Returns a hash code value for this <code>Sourceable</code>
     *  based on the <code>Id</code>.
     *
     *  @return a hash code value for this sourceable
     */

    @Override
    public int hashCode() {
        return (this.sourceable.hashCode());
    }


    /**
     *  Returns a string representation of this Sourceable.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        return (this.sourceable.toString());
    }
}
