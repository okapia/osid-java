//
// AbstractAssemblyRelevancyQuery.java
//
//     A RelevancyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ontology.relevancy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RelevancyQuery that stores terms.
 */

public abstract class AbstractAssemblyRelevancyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.ontology.RelevancyQuery,
               org.osid.ontology.RelevancyQueryInspector,
               org.osid.ontology.RelevancySearchOrder {

    private final java.util.Collection<org.osid.ontology.records.RelevancyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.records.RelevancyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.records.RelevancySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRelevancyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRelevancyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the subject <code> Id </code> for this query. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubjectId(org.osid.id.Id subjectId, boolean match) {
        getAssembler().addIdTerm(getSubjectIdColumn(), subjectId, match);
        return;
    }


    /**
     *  Clears the subject <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearSubjectIdTerms() {
        getAssembler().clearTerms(getSubjectIdColumn());
        return;
    }


    /**
     *  Gets the subject <code> Id </code> terms. 
     *
     *  @return the subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubjectIdTerms() {
        return (getAssembler().getIdTerms(getSubjectIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the subject. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubject(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubjectColumn(), style);
        return;
    }


    /**
     *  Gets the SubjectId column name.
     *
     * @return the column name
     */

    protected String getSubjectIdColumn() {
        return ("subject_id");
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for an subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsSubjectQuery() is false");
    }


    /**
     *  Clears the subject terms for this query. 
     */

    @OSID @Override
    public void clearSubjectTerms() {
        getAssembler().clearTerms(getSubjectColumn());
        return;
    }


    /**
     *  Gets the subject terms. 
     *
     *  @return the subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Tests if a <code> SubjectSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a subject search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a subject. 
     *
     *  @return the subject search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchOrder getSubjectSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubjectSearchOrder() is false");
    }


    /**
     *  Gets the Subject column name.
     *
     * @return the column name
     */

    protected String getSubjectColumn() {
        return ("subject");
    }


    /**
     *  Sets the mapped <code> Id </code> for this query. 
     *
     *  @param  id an <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMappedId(org.osid.id.Id id, boolean match) {
        getAssembler().addIdTerm(getMappedIdColumn(), id, match);
        return;
    }


    /**
     *  Clears the mapped <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearMappedIdTerms() {
        getAssembler().clearTerms(getMappedIdColumn());
        return;
    }


    /**
     *  Gets the mapped <code> Id </code> terms. 
     *
     *  @return the mapped <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMappedIdTerms() {
        return (getAssembler().getIdTerms(getMappedIdColumn()));
    }


    /**
     *  Gets the MappedId column name.
     *
     * @return the column name
     */

    protected String getMappedIdColumn() {
        return ("mapped_id");
    }


    /**
     *  Sets the ontology <code> Id </code> for this query. 
     *
     *  @param  ontologyId a ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOntologyId(org.osid.id.Id ontologyId, boolean match) {
        getAssembler().addIdTerm(getOntologyIdColumn(), ontologyId, match);
        return;
    }


    /**
     *  Clears the ontology <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearOntologyIdTerms() {
        getAssembler().clearTerms(getOntologyIdColumn());
        return;
    }


    /**
     *  Gets the ontology <code> Id </code> terms. 
     *
     *  @return the ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOntologyIdTerms() {
        return (getAssembler().getIdTerms(getOntologyIdColumn()));
    }


    /**
     *  Gets the OntologyId column name.
     *
     * @return the column name
     */

    protected String getOntologyIdColumn() {
        return ("ontology_id");
    }


    /**
     *  Tests if a <code> OntologyQuery </code> is available for querying 
     *  ontologies. 
     *
     *  @return <code> true </code> if a ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsOntologyQuery() is false");
    }


    /**
     *  Clears the ontology terms for this query. 
     */

    @OSID @Override
    public void clearOntologyTerms() {
        getAssembler().clearTerms(getOntologyColumn());
        return;
    }


    /**
     *  Gets the ontology terms. 
     *
     *  @return the ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }


    /**
     *  Gets the Ontology column name.
     *
     * @return the column name
     */

    protected String getOntologyColumn() {
        return ("ontology");
    }


    /**
     *  Tests if this relevancy supports the given record
     *  <code>Type</code>.
     *
     *  @param  relevancyRecordType a relevancy record type 
     *  @return <code>true</code> if the relevancyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type relevancyRecordType) {
        for (org.osid.ontology.records.RelevancyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relevancyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  relevancyRecordType the relevancy record type 
     *  @return the relevancy query record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancyQueryRecord getRelevancyQueryRecord(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.RelevancyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relevancyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  relevancyRecordType the relevancy record type 
     *  @return the relevancy query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancyQueryInspectorRecord getRelevancyQueryInspectorRecord(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.RelevancyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(relevancyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param relevancyRecordType the relevancy record type
     *  @return the relevancy search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancySearchOrderRecord getRelevancySearchOrderRecord(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.RelevancySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(relevancyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this relevancy. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param relevancyQueryRecord the relevancy query record
     *  @param relevancyQueryInspectorRecord the relevancy query inspector
     *         record
     *  @param relevancySearchOrderRecord the relevancy search order record
     *  @param relevancyRecordType relevancy record type
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyQueryRecord</code>,
     *          <code>relevancyQueryInspectorRecord</code>,
     *          <code>relevancySearchOrderRecord</code> or
     *          <code>relevancyRecordTyperelevancy</code> is
     *          <code>null</code>
     */
            
    protected void addRelevancyRecords(org.osid.ontology.records.RelevancyQueryRecord relevancyQueryRecord, 
                                      org.osid.ontology.records.RelevancyQueryInspectorRecord relevancyQueryInspectorRecord, 
                                      org.osid.ontology.records.RelevancySearchOrderRecord relevancySearchOrderRecord, 
                                      org.osid.type.Type relevancyRecordType) {

        addRecordType(relevancyRecordType);

        nullarg(relevancyQueryRecord, "relevancy query record");
        nullarg(relevancyQueryInspectorRecord, "relevancy query inspector record");
        nullarg(relevancySearchOrderRecord, "relevancy search odrer record");

        this.queryRecords.add(relevancyQueryRecord);
        this.queryInspectorRecords.add(relevancyQueryInspectorRecord);
        this.searchOrderRecords.add(relevancySearchOrderRecord);
        
        return;
    }
}
