//
// AbstractQueryCompetencyLookupSession.java
//
//    An inline adapter that maps a CompetencyLookupSession to
//    a CompetencyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CompetencyLookupSession to
 *  a CompetencyQuerySession.
 */

public abstract class AbstractQueryCompetencyLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractCompetencyLookupSession
    implements org.osid.resourcing.CompetencyLookupSession {

    private final org.osid.resourcing.CompetencyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCompetencyLookupSession.
     *
     *  @param querySession the underlying competency query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCompetencyLookupSession(org.osid.resourcing.CompetencyQuerySession querySession) {
        nullarg(querySession, "competency query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>Competency</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCompetencies() {
        return (this.session.canSearchCompetencies());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include competencies in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    
     
    /**
     *  Gets the <code>Competency</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Competency</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Competency</code> and
     *  retained for compatibility.
     *
     *  @param  competencyId <code>Id</code> of the
     *          <code>Competency</code>
     *  @return the competency
     *  @throws org.osid.NotFoundException <code>competencyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>competencyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency(org.osid.id.Id competencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CompetencyQuery query = getQuery();
        query.matchId(competencyId, true);
        org.osid.resourcing.CompetencyList competencies = this.session.getCompetenciesByQuery(query);
        if (competencies.hasNext()) {
            return (competencies.getNextCompetency());
        } 
        
        throw new org.osid.NotFoundException(competencyId + " not found");
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  competencies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Competencies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  competencyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>competencyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByIds(org.osid.id.IdList competencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CompetencyQuery query = getQuery();

        try (org.osid.id.IdList ids = competencyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCompetenciesByQuery(query));
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  competency genus <code>Type</code> which does not include
     *  competencies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CompetencyQuery query = getQuery();
        query.matchGenusType(competencyGenusType, true);
        return (this.session.getCompetenciesByQuery(query));
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  competency genus <code>Type</code> and include any additional
     *  competencies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByParentGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CompetencyQuery query = getQuery();
        query.matchParentGenusType(competencyGenusType, true);
        return (this.session.getCompetenciesByQuery(query));
    }


    /**
     *  Gets a <code>CompetencyList</code> containing the given
     *  competency record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  competencyRecordType a competency record type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByRecordType(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CompetencyQuery query = getQuery();
        query.matchRecordType(competencyRecordType, true);
        return (this.session.getCompetenciesByQuery(query));
    }

    
    /**
     *  Gets all <code>Competencies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Competencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CompetencyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCompetenciesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.CompetencyQuery getQuery() {
        org.osid.resourcing.CompetencyQuery query = this.session.getCompetencyQuery();
        return (query);
    }
}
