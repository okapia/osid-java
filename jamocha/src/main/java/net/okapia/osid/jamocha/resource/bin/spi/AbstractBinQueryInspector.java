//
// AbstractBinQueryInspector.java
//
//     A template for making a BinQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.bin.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for bins.
 */

public abstract class AbstractBinQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.resource.BinQueryInspector {

    private final java.util.Collection<org.osid.resource.records.BinQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the ancestor bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBinIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getAncestorBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }


    /**
     *  Gets the descendant bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBinIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getDescendantBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given bin query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a bin implementing the requested record.
     *
     *  @param binRecordType a bin record type
     *  @return the bin query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(binRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.BinQueryInspectorRecord getBinQueryInspectorRecord(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.BinQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(binRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(binRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bin query. 
     *
     *  @param binQueryInspectorRecord bin query inspector
     *         record
     *  @param binRecordType bin record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBinQueryInspectorRecord(org.osid.resource.records.BinQueryInspectorRecord binQueryInspectorRecord, 
                                                   org.osid.type.Type binRecordType) {

        addRecordType(binRecordType);
        nullarg(binRecordType, "bin record type");
        this.records.add(binQueryInspectorRecord);        
        return;
    }
}
