//
// AbstractAdapterIdLookupSession.java
//
//    A Id lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.id.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Id lookup session adapter.
 */

public abstract class AbstractAdapterIdLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.id.IdLookupSession {

    private final org.osid.id.IdLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterIdLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterIdLookupSession(org.osid.id.IdLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Id} lookups. A return of
     *  true does not guarantee successful authorization. A return of
     *  false indicates that it is known all methods in this session
     *  will result in a {@code PERMISSION_DENIED.}  This is intended
     *  as a hint to an application that may opt not to offer lookup
     *  operations.
     *
     *  @return {@code false} if lookup methods are not authorized, 
     *          {@code true} otherwise 
     */

    @OSID @Override
    public boolean canLookupIds() {
        return (this.session.canLookupIds());
    }


    /**
     *  Gets an {@code Id.} This method serves to get the principal
     *  {@code Id} if the given {@code Id} Is an alias.
     *
     *  @param  id an {@code Id} 
     *  @return the {@code Id} 
     *  @throws org.osid.NotFoundException {@code id} is not found 
     *  @throws org.osid.NullArgumentException {@code id} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.Id getId(org.osid.id.Id id)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getId(id));
    }


    /**
     *  Gets a list of {@code Ids.} This method serves to get the
     *  principal {@code Ids} if different from the given {@code Ids}.
     *
     *  @param  ids a list of {@code Ids} 
     *  @return a list of {@code Ids} 
     *  @throws org.osid.NotFoundException an {@code id} is not found 
     *  @throws org.osid.NullArgumentException {@code ids} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdsByIds(org.osid.id.IdList ids)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIdsByIds(ids));
    }


    /**
     *  Gets {@code Ids} by the given authority. 
     *
     *  @param  authority an authority 
     *  @return a list of {@code Ids} 
     *  @throws org.osid.NullArgumentException {@code authority} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdsByAuthority(String authority)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIdsByAuthority(authority));
    }


    /**
     *  Gets {@code Ids} by the given authority and namespace. 
     *
     *  @param  authority an authority 
     *  @param  namespace a namespace 
     *  @return a list of {@code Ids} 
     *  @throws org.osid.NullArgumentException {@code authority} or 
     *          {@code namespace} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdsByAuthorityAndNamespace(String authority, 
                                                            String namespace)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIdsByAuthorityAndNamespace(authority, namespace));
    }


    /**
     *  Gets all {@code Ids.} 
     *
     *  @return the list of all {@code Ids} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIds());
    }


    /**
     *  Tests if the two {@code Ids} are equivalent. Two {@code Ids}
     *  are equivalent if they identify the same object. If one of the
     *  {@code Ids} is not known, they are not equivalent.
     *
     *  @param  id an {@code Id} 
     *  @param  equivalentId an {@code Id} 
     *  @return {@code true} if the {@code Ids} are equivalent, 
     *          false otherwise 
     *  @throws org.osid.NullArgumentException {@code null} argument 
     *          provided 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public boolean isEquivalent(org.osid.id.Id id, org.osid.id.Id equivalentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.isEquivalent(id, equivalentId));
    }


    /**
     *  Gets a list of {@code Id} aliases of an {@code Id.}
     *
     *  @param  id an {@code Id} 
     *  @return a list of alias {@code Ids} 
     *  @throws org.osid.NullArgumentException {@code id} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdAliases(org.osid.id.Id id)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getIdAliases(id));
    }


    /**
     *  Gets a list of {@code Id} aliases in a authority for an {@code
     *  Id}.
     *
     *  @param  id an {@code Id} 
     *  @param  authority an authority 
     *  @return a list of alias {@code Ids} 
     *  @throws org.osid.NullArgumentException {@code id} or {@code 
     *          authority} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdAliasesByAuthority(org.osid.id.Id id, 
                                                      String authority)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIdAliasesByAuthority(id, authority));
    }


    /**
     *  Gets a list of {@code Id} aliases in a namespace for an {@code 
     *  Id.} 
     *
     *  @param  id an {@code Id} 
     *  @param  authority an authority 
     *  @param  namespace a namespace 
     *  @return a list of alias {@code Ids} 
     *  @throws org.osid.NullArgumentException {@code id, authority},
     *          or {@code namespace} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdAliasesByAuthorityAndNamespace(org.osid.id.Id id, 
                                                                  String authority, 
                                                                  String namespace)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIdAliasesByAuthorityAndNamespace(id, authority, namespace));
    }
}

