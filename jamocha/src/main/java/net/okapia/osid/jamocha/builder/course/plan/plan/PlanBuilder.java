//
// Plan.java
//
//     Defines a Plan builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.plan;


/**
 *  Defines a <code>Plan</code> builder.
 */

public final class PlanBuilder
    extends net.okapia.osid.jamocha.builder.course.plan.plan.spi.AbstractPlanBuilder<PlanBuilder> {
    

    /**
     *  Constructs a new <code>PlanBuilder</code> using a
     *  <code>MutablePlan</code>.
     */

    public PlanBuilder() {
        super(new MutablePlan());
        return;
    }


    /**
     *  Constructs a new <code>PlanBuilder</code> using the given
     *  mutable plan.
     * 
     *  @param plan
     */

    public PlanBuilder(PlanMiter plan) {
        super(plan);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return PlanBuilder
     */

    @Override
    protected PlanBuilder self() {
        return (this);
    }
}       


