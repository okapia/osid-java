//
// AbstractAntimatroidQueryInspector.java
//
//     A template for making an AntimatroidQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.antimatroid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for antimatroids.
 */

public abstract class AbstractAntimatroidQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.sequencing.AntimatroidQueryInspector {

    private final java.util.Collection<org.osid.sequencing.records.AntimatroidQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the chain <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChainIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the chain query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQueryInspector[] getChainTerms() {
        return (new org.osid.sequencing.ChainQueryInspector[0]);
    }


    /**
     *  Gets the ancestor antimatroid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAntimatroidIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor antimatroid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQueryInspector[] getAncestorAntimatroidTerms() {
        return (new org.osid.sequencing.AntimatroidQueryInspector[0]);
    }


    /**
     *  Gets the descendant antimatroid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAntimatroidIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant antimatroid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQueryInspector[] getDescendantAntimatroidTerms() {
        return (new org.osid.sequencing.AntimatroidQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given antimatroid query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an antimatroid implementing the requested record.
     *
     *  @param antimatroidRecordType an antimatroid record type
     *  @return the antimatroid query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(antimatroidRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidQueryInspectorRecord getAntimatroidQueryInspectorRecord(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.AntimatroidQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(antimatroidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(antimatroidRecordType + " is not supported");
    }


    /**
     *  Adds a record to this antimatroid query. 
     *
     *  @param antimatroidQueryInspectorRecord antimatroid query inspector
     *         record
     *  @param antimatroidRecordType antimatroid record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAntimatroidQueryInspectorRecord(org.osid.sequencing.records.AntimatroidQueryInspectorRecord antimatroidQueryInspectorRecord, 
                                                   org.osid.type.Type antimatroidRecordType) {

        addRecordType(antimatroidRecordType);
        nullarg(antimatroidRecordType, "antimatroid record type");
        this.records.add(antimatroidQueryInspectorRecord);        
        return;
    }
}
