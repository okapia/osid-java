//
// MutableMapDirectionLookupSession
//
//    Implements a Direction lookup service backed by a collection of
//    directions that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Direction lookup service backed by a collection of
 *  directions. The directions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of directions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapDirectionLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractMapDirectionLookupSession
    implements org.osid.recipe.DirectionLookupSession {


    /**
     *  Constructs a new {@code MutableMapDirectionLookupSession}
     *  with no directions.
     *
     *  @param cookbook the cookbook
     *  @throws org.osid.NullArgumentException {@code cookbook} is
     *          {@code null}
     */

      public MutableMapDirectionLookupSession(org.osid.recipe.Cookbook cookbook) {
        setCookbook(cookbook);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDirectionLookupSession} with a
     *  single direction.
     *
     *  @param cookbook the cookbook  
     *  @param direction a direction
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code direction} is {@code null}
     */

    public MutableMapDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                           org.osid.recipe.Direction direction) {
        this(cookbook);
        putDirection(direction);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDirectionLookupSession}
     *  using an array of directions.
     *
     *  @param cookbook the cookbook
     *  @param directions an array of directions
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code directions} is {@code null}
     */

    public MutableMapDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                           org.osid.recipe.Direction[] directions) {
        this(cookbook);
        putDirections(directions);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDirectionLookupSession}
     *  using a collection of directions.
     *
     *  @param cookbook the cookbook
     *  @param directions a collection of directions
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code directions} is {@code null}
     */

    public MutableMapDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                           java.util.Collection<? extends org.osid.recipe.Direction> directions) {

        this(cookbook);
        putDirections(directions);
        return;
    }

    
    /**
     *  Makes a {@code Direction} available in this session.
     *
     *  @param direction a direction
     *  @throws org.osid.NullArgumentException {@code direction{@code  is
     *          {@code null}
     */

    @Override
    public void putDirection(org.osid.recipe.Direction direction) {
        super.putDirection(direction);
        return;
    }


    /**
     *  Makes an array of directions available in this session.
     *
     *  @param directions an array of directions
     *  @throws org.osid.NullArgumentException {@code directions{@code 
     *          is {@code null}
     */

    @Override
    public void putDirections(org.osid.recipe.Direction[] directions) {
        super.putDirections(directions);
        return;
    }


    /**
     *  Makes collection of directions available in this session.
     *
     *  @param directions a collection of directions
     *  @throws org.osid.NullArgumentException {@code directions{@code  is
     *          {@code null}
     */

    @Override
    public void putDirections(java.util.Collection<? extends org.osid.recipe.Direction> directions) {
        super.putDirections(directions);
        return;
    }


    /**
     *  Removes a Direction from this session.
     *
     *  @param directionId the {@code Id} of the direction
     *  @throws org.osid.NullArgumentException {@code directionId{@code 
     *          is {@code null}
     */

    @Override
    public void removeDirection(org.osid.id.Id directionId) {
        super.removeDirection(directionId);
        return;
    }    
}
