//
// AbstractAssemblyAuctionProcessorEnablerQuery.java
//
//     An AuctionProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.bidding.rules.auctionprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuctionProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyAuctionProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.bidding.rules.AuctionProcessorEnablerQuery,
               org.osid.bidding.rules.AuctionProcessorEnablerQueryInspector,
               org.osid.bidding.rules.AuctionProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAuctionProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuctionProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the auction processor. 
     *
     *  @param  auctionProcessorId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuctionProcessorId(org.osid.id.Id auctionProcessorId, 
                                             boolean match) {
        getAssembler().addIdTerm(getRuledAuctionProcessorIdColumn(), auctionProcessorId, match);
        return;
    }


    /**
     *  Clears the auction processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionProcessorIdTerms() {
        getAssembler().clearTerms(getRuledAuctionProcessorIdColumn());
        return;
    }


    /**
     *  Gets the auction processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuctionProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledAuctionProcessorIdColumn()));
    }


    /**
     *  Gets the RuledAuctionProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledAuctionProcessorIdColumn() {
        return ("ruled_auction_processor_id");
    }


    /**
     *  Tests if an <code> AuctionProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction processor query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuctionProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuctionProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuery getRuledAuctionProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuctionProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any auction processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any auction 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          auction processors 
     */

    @OSID @Override
    public void matchAnyRuledAuctionProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledAuctionProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the auction processor query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionProcessorTerms() {
        getAssembler().clearTerms(getRuledAuctionProcessorColumn());
        return;
    }


    /**
     *  Gets the auction processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQueryInspector[] getRuledAuctionProcessorTerms() {
        return (new org.osid.bidding.rules.AuctionProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledAuctionProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledAuctionProcessorColumn() {
        return ("ruled_auction_processor");
    }


    /**
     *  Matches enablers mapped to the auction house. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAuctionHouseIdColumn(), auctionHouseId, match);
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        getAssembler().clearTerms(getAuctionHouseIdColumn());
        return;
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (getAssembler().getIdTerms(getAuctionHouseIdColumn()));
    }


    /**
     *  Gets the AuctionHouseId column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseIdColumn() {
        return ("auction_house_id");
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        getAssembler().clearTerms(getAuctionHouseColumn());
        return;
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the AuctionHouse column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseColumn() {
        return ("auction_house");
    }


    /**
     *  Tests if this auctionProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  auctionProcessorEnablerRecordType an auction processor enabler record type 
     *  @return <code>true</code> if the auctionProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auctionProcessorEnablerRecordType) {
        for (org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  auctionProcessorEnablerRecordType the auction processor enabler record type 
     *  @return the auction processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord getAuctionProcessorEnablerQueryRecord(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  auctionProcessorEnablerRecordType the auction processor enabler record type 
     *  @return the auction processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord getAuctionProcessorEnablerQueryInspectorRecord(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(auctionProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param auctionProcessorEnablerRecordType the auction processor enabler record type
     *  @return the auction processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorEnablerSearchOrderRecord getAuctionProcessorEnablerSearchOrderRecord(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(auctionProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this auction processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auctionProcessorEnablerQueryRecord the auction processor enabler query record
     *  @param auctionProcessorEnablerQueryInspectorRecord the auction processor enabler query inspector
     *         record
     *  @param auctionProcessorEnablerSearchOrderRecord the auction processor enabler search order record
     *  @param auctionProcessorEnablerRecordType auction processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerQueryRecord</code>,
     *          <code>auctionProcessorEnablerQueryInspectorRecord</code>,
     *          <code>auctionProcessorEnablerSearchOrderRecord</code> or
     *          <code>auctionProcessorEnablerRecordTypeauctionProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addAuctionProcessorEnablerRecords(org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord auctionProcessorEnablerQueryRecord, 
                                      org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord auctionProcessorEnablerQueryInspectorRecord, 
                                      org.osid.bidding.rules.records.AuctionProcessorEnablerSearchOrderRecord auctionProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type auctionProcessorEnablerRecordType) {

        addRecordType(auctionProcessorEnablerRecordType);

        nullarg(auctionProcessorEnablerQueryRecord, "auction processor enabler query record");
        nullarg(auctionProcessorEnablerQueryInspectorRecord, "auction processor enabler query inspector record");
        nullarg(auctionProcessorEnablerSearchOrderRecord, "auction processor enabler search odrer record");

        this.queryRecords.add(auctionProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(auctionProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(auctionProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
