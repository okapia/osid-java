//
// AbstractProxyManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.proxy.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractProxyManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.proxy.ProxyManager,
               org.osid.proxy.ProxyProxyManager {

    private final Types proxyRecordTypes                   = new TypeRefSet();
    private final Types proxyConditionRecordTypes          = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractProxyManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if a proxy session is supported. 
     *
     *  @return <code> true </code> if proxy is supported <code> , </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProxy() {
        return (false);
    }


    /**
     *  Gets the supported <code> Proxy </code> record interface types. 
     *
     *  @return a list containing the supported <code> Proxy </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProxyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.proxyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Proxy </code> record interface type is 
     *  supported. 
     *
     *  @param  proxyRecordType a <code> Type </code> indicating a <code> 
     *          Proxy </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proxyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProxyRecordType(org.osid.type.Type proxyRecordType) {
        return (this.proxyRecordTypes.contains(proxyRecordType));
    }


    /**
     *  Adds support for a proxy record type.
     *
     *  @param proxyRecordType a proxy record type
     *  @throws org.osid.NullArgumentException
     *  <code>proxyRecordType</code> is <code>null</code>
     */

    protected void addProxyRecordType(org.osid.type.Type proxyRecordType) {
        this.proxyRecordTypes.add(proxyRecordType);
        return;
    }


    /**
     *  Removes support for a proxy record type.
     *
     *  @param proxyRecordType a proxy record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>proxyRecordType</code> is <code>null</code>
     */

    protected void removeProxyRecordType(org.osid.type.Type proxyRecordType) {
        this.proxyRecordTypes.remove(proxyRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProxyCondition </code> record interface 
     *  types. 
     *
     *  @return a list containing the supported <code> ProxyCondition </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProxyConditionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.proxyConditionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProxyCondition </code> record interface type 
     *  is supported. 
     *
     *  @param  proxyConditionRecordType a <code> Type </code> indicating a 
     *          <code> ProxyCondition </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proxyConditionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProxyConditionRecordType(org.osid.type.Type proxyConditionRecordType) {
        return (this.proxyConditionRecordTypes.contains(proxyConditionRecordType));
    }


    /**
     *  Adds support for a proxy condition record type.
     *
     *  @param proxyConditionRecordType a proxy condition record type
     *  @throws org.osid.NullArgumentException
     *  <code>proxyConditionRecordType</code> is <code>null</code>
     */

    protected void addProxyConditionRecordType(org.osid.type.Type proxyConditionRecordType) {
        this.proxyConditionRecordTypes.add(proxyConditionRecordType);
        return;
    }


    /**
     *  Removes support for a proxy condition record type.
     *
     *  @param proxyConditionRecordType a proxy condition record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>proxyConditionRecordType</code> is <code>null</code>
     */

    protected void removeProxyConditionRecordType(org.osid.type.Type proxyConditionRecordType) {
        this.proxyConditionRecordTypes.remove(proxyConditionRecordType);
        return;
    }


    /**
     *  Gets a <code> ProxySession </code> which is responsible for acquiring 
     *  authentication credentials on behalf of a service client. 
     *
     *  @return a proxy session for this service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProxy() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.ProxySession getProxySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.proxy.ProxyManager.getProxySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the <code> 
     *  ProxySession </code> using the supplied <code> Proxy. </code> 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProxySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProxy() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.ProxySession getProxySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.proxy.ProxyProxyManager.getProxySession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.proxyRecordTypes.clear();
        this.proxyRecordTypes.clear();

        this.proxyConditionRecordTypes.clear();
        this.proxyConditionRecordTypes.clear();

        return;
    }
}
