//
// AbstractOrderQueryInspector.java
//
//     A template for making an OrderQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for orders.
 */

public abstract class AbstractOrderQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.ordering.OrderQueryInspector {

    private final java.util.Collection<org.osid.ordering.records.OrderQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the customer <code> Id </code> terms. 
     *
     *  @return the customer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the customer terms. 
     *
     *  @return the customer terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCustomerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the item <code> Id </code> terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the item terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.ordering.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.ordering.ItemQueryInspector[0]);
    }


    /**
     *  Gets the total cost terms. 
     *
     *  @return the total cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getTotalCostTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the minimum total cost terms. 
     *
     *  @return the minimum total cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getMinimumTotalCostTerms() {
        return (new org.osid.search.terms.CurrencyTerm[0]);
    }


    /**
     *  Gets the atomic terms. 
     *
     *  @return the atomic terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAtomicTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the submit date terms. 
     *
     *  @return the date range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getSubmitDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the submitter resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubmitterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the submitter terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSubmitterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the submitting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubmittingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the submitting agent terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getSubmittingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the closed date terms. 
     *
     *  @return the date range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClosedDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the closer resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCloserIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the closer terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCloserTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the submitting agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getClosingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the closing agent terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getClosingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given order query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an order implementing the requested record.
     *
     *  @param orderRecordType an order record type
     *  @return the order query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(orderRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderQueryInspectorRecord getOrderQueryInspectorRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.OrderQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(orderRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderRecordType + " is not supported");
    }


    /**
     *  Adds a record to this order query. 
     *
     *  @param orderQueryInspectorRecord order query inspector
     *         record
     *  @param orderRecordType order record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOrderQueryInspectorRecord(org.osid.ordering.records.OrderQueryInspectorRecord orderQueryInspectorRecord, 
                                                   org.osid.type.Type orderRecordType) {

        addRecordType(orderRecordType);
        nullarg(orderRecordType, "order record type");
        this.records.add(orderQueryInspectorRecord);        
        return;
    }
}
