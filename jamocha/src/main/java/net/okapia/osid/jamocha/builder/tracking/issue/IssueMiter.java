//
// IssueMiter.java
//
//     Defines an Issue miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.issue;


/**
 *  Defines an <code>Issue</code> miter for use with the builders.
 */

public interface IssueMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.tracking.Issue {


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException <code>queue</code> is
     *          <code>null</code>
     */

    public void setQueue(org.osid.tracking.Queue queue);


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public void setCustomer(org.osid.resource.Resource customer);


    /**
     *  Sets the topic.
     *
     *  @param topic a topic
     *  @throws org.osid.NullArgumentException <code>topic</code> is
     *          <code>null</code>
     */

    public void setTopic(org.osid.ontology.Subject topic);


    /**
     *  Sets the master issue.
     *
     *  @param issue a master issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public void setMasterIssue(org.osid.tracking.Issue issue);


    /**
     *  Adds a duplicate issue.
     *
     *  @param issue a duplicate issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public void addDuplicateIssue(org.osid.tracking.Issue issue);


    /**
     *  Sets all the duplicate issues.
     *
     *  @param issues a collection of duplicate issues
     *  @throws org.osid.NullArgumentException <code>issues</code> is
     *          <code>null</code>
     */

    public void setDuplicateIssues(java.util.Collection<org.osid.tracking.Issue> issues);


    /**
     *  Sets the branched issue.
     *
     *  @param issue a branched issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public void setBranchedIssue(org.osid.tracking.Issue issue);


    /**
     *  Sets the root issue.
     *
     *  @param issue a root issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public void setRootIssue(org.osid.tracking.Issue issue);


    /**
     *  Sets the priority type.
     *
     *  @param priorityType a priority type
     *  @throws org.osid.NullArgumentException
     *          <code>priorityType</code> is <code>null</code>
     */

    public void setPriorityType(org.osid.type.Type priorityType);


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @throws org.osid.NullArgumentException <code>creator</code> is
     *          <code>null</code>
     */

    public void setCreator(org.osid.resource.Resource creator);


    /**
     *  Sets the creating agent.
     *
     *  @param agent a creating agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setCreatingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setCreatedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the reopener.
     *
     *  @param reopener a reopener
     *  @throws org.osid.NullArgumentException <code>reopener</code>
     *          is <code>null</code>
     */

    public void setReopener(org.osid.resource.Resource reopener);


    /**
     *  Sets the reopening agent.
     *
     *  @param agent a reopening agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setReopeningAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the reopened date.
     *
     *  @param date a reopened date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setReopenedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDueDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a blocker.
     *
     *  @param blocker a blocker
     *  @throws org.osid.NullArgumentException <code>blocker</code> is
     *          <code>null</code>
     */

    public void addBlocker(org.osid.tracking.Issue blocker);


    /**
     *  Sets all the blockers.
     *
     *  @param blockers a collection of blockers
     *  @throws org.osid.NullArgumentException <code>blockers</code>
     *          is <code>null</code>
     */

    public void setBlockers(java.util.Collection<org.osid.tracking.Issue> blockers);


    /**
     *  Sets the resolver.
     *
     *  @param resolver a resolver
     *  @throws org.osid.NullArgumentException <code>resolver</code>
     *          is <code>null</code>
     */

    public void setResolver(org.osid.resource.Resource resolver);


    /**
     *  Sets the resolving agent.
     *
     *  @param agent a resolving agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setResolvingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the resolved date.
     *
     *  @param date a resolved date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setResolvedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the resolution type.
     *
     *  @param resolutionType a resolution type
     *  @throws org.osid.NullArgumentException
     *          <code>resolutionType</code> is <code>null</code>
     */

    public void setResolutionType(org.osid.type.Type resolutionType);


    /**
     *  Sets the closer.
     *
     *  @param closer a closer
     *  @throws org.osid.NullArgumentException <code>closer</code> is
     *          <code>null</code>
     */

    public void setCloser(org.osid.resource.Resource closer);


    /**
     *  Sets the closing agent.
     *
     *  @param agent a closing agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setClosingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setClosedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the assigned resource.
     *
     *  @param resource an assigned resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setAssignedResource(org.osid.resource.Resource resource);


    /**
     *  Adds an Issue record.
     *
     *  @param record an issue record
     *  @param recordType the type of issue record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addIssueRecord(org.osid.tracking.records.IssueRecord record, org.osid.type.Type recordType);
}       


