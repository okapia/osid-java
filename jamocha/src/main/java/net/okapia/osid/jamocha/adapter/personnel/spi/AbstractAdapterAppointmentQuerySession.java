//
// AbstractQueryAppointmentLookupSession.java
//
//    An AppointmentQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AppointmentQuerySession adapter.
 */

public abstract class AbstractAdapterAppointmentQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.personnel.AppointmentQuerySession {

    private final org.osid.personnel.AppointmentQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAppointmentQuerySession.
     *
     *  @param session the underlying appointment query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAppointmentQuerySession(org.osid.personnel.AppointmentQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeRealm</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeRealm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the {@codeRealm</code> associated with this 
     *  session.
     *
     *  @return the {@codeRealm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform {@codeAppointment</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAppointments() {
        return (this.session.canSearchAppointments());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include appointments in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this realm only.
     */
    
    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    
      
    /**
     *  Gets an appointment query. The returned query will not have an
     *  extension query.
     *
     *  @return the appointment query 
     */
      
    @OSID @Override
    public org.osid.personnel.AppointmentQuery getAppointmentQuery() {
        return (this.session.getAppointmentQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  appointmentQuery the appointment query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code appointmentQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code appointmentQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByQuery(org.osid.personnel.AppointmentQuery appointmentQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAppointmentsByQuery(appointmentQuery));
    }
}
