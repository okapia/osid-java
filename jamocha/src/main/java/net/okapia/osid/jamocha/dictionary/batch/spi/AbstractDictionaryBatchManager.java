//
// AbstractDictionaryBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractDictionaryBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.dictionary.batch.DictionaryBatchManager,
               org.osid.dictionary.batch.DictionaryBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractDictionaryBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractDictionaryBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of entries is available. 
     *
     *  @return <code> true </code> if an entry bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of dictionaries is available. 
     *
     *  @return <code> true </code> if a dictionary bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service. 
     *
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.EntryBatchAdminSession getEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.batch.DictionaryBatchManager.getEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.EntryBatchAdminSession getEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.batch.DictionaryBatchProxyManager.getEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service for the given dictionary. 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.EntryBatchAdminSession getEntryBatchAdminSessionForDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.batch.DictionaryBatchManager.getEntryBatchAdminSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service for the given dictionary. 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.EntryBatchAdminSession getEntryBatchAdminSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.batch.DictionaryBatchProxyManager.getEntryBatchAdminSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  dictionary administration service. 
     *
     *  @return a <code> DictionaryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.DictionaryBatchAdminSession getDictionaryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.batch.DictionaryBatchManager.getDictionaryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  dictionary administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.DictionaryBatchAdminSession getDictionaryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.batch.DictionaryBatchProxyManager.getDictionaryBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
