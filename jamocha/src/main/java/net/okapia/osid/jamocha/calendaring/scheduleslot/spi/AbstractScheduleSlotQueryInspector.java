//
// AbstractScheduleSlotQueryInspector.java
//
//     A template for making a ScheduleSlotQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for schedule slots.
 */

public abstract class AbstractScheduleSlotQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractContainableOsidObjectQueryInspector
    implements org.osid.calendaring.ScheduleSlotQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the schedule slot <code> Id </code> terms. 
     *
     *  @return the schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleSlotIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the schedule slot terms. 
     *
     *  @return the schedule slot terms 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQueryInspector[] getScheduleSlotTerms() {
        return (new org.osid.calendaring.ScheduleSlotQueryInspector[0]);
    }


    /**
     *  Gets the weekday terms. 
     *
     *  @return the weekday terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getWeekdayTerms() {
        return (new org.osid.search.terms.CardinalTerm[0]);
    }


    /**
     *  Gets the weekly interval terms. 
     *
     *  @return the weekly interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerTerm[] getWeeklyIntervalTerms() {
        return (new org.osid.search.terms.IntegerTerm[0]);
    }


    /**
     *  Gets the week of month terms. 
     *
     *  @return the week of month terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerTerm[] getWeekOfMonthTerms() {
        return (new org.osid.search.terms.IntegerTerm[0]);
    }


    /**
     *  Gets the weekday time terms. 
     *
     *  @return the fixed interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.TimeRangeTerm[] getWeekdayTimeTerms() {
        return (new org.osid.search.terms.TimeRangeTerm[0]);
    }


    /**
     *  Gets the fixed interval terms. 
     *
     *  @return the fixed interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getFixedIntervalTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (new org.osid.search.terms.DurationTerm[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given schedule slot query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a schedule slot implementing the requested record.
     *
     *  @param scheduleSlotRecordType a schedule slot record type
     *  @return the schedule slot query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSlotRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord getScheduleSlotQueryInspectorRecord(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSlotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule slot query. 
     *
     *  @param scheduleSlotQueryInspectorRecord schedule slot query inspector
     *         record
     *  @param scheduleSlotRecordType scheduleSlot record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addScheduleSlotQueryInspectorRecord(org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord scheduleSlotQueryInspectorRecord, 
                                                   org.osid.type.Type scheduleSlotRecordType) {

        addRecordType(scheduleSlotRecordType);
        nullarg(scheduleSlotRecordType, "schedule slot record type");
        this.records.add(scheduleSlotQueryInspectorRecord);        
        return;
    }
}
