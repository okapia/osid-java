//
// AssessmentRequirementElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.assessmentrequirement.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AssessmentRequirementElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the AssessmentRequirementElement Id.
     *
     *  @return the assessment requirement element Id
     */

    public static org.osid.id.Id getAssessmentRequirementEntityId() {
        return (makeEntityId("osid.course.requisite.AssessmentRequirement"));
    }


    /**
     *  Gets the AltRequisites element Id.
     *
     *  @return the AltRequisites element Id
     */

    public static org.osid.id.Id getAltRequisites() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.AltRequisites"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.Assessment"));
    }


    /**
     *  Gets the Timeframe element Id.
     *
     *  @return the Timeframe element Id
     */

    public static org.osid.id.Id getTimeframe() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.Timeframe"));
    }


    /**
     *  Gets the MinimumGradeId element Id.
     *
     *  @return the MinimumGradeId element Id
     */

    public static org.osid.id.Id getMinimumGradeId() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.MinimumGradeId"));
    }


    /**
     *  Gets the MinimumGrade element Id.
     *
     *  @return the MinimumGrade element Id
     */

    public static org.osid.id.Id getMinimumGrade() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.MinimumGrade"));
    }


    /**
     *  Gets the MinimumScoreSystemId element Id.
     *
     *  @return the MinimumScoreSystemId element Id
     */

    public static org.osid.id.Id getMinimumScoreSystemId() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.MinimumScoreSystemId"));
    }


    /**
     *  Gets the MinimumScoreSystem element Id.
     *
     *  @return the MinimumScoreSystem element Id
     */

    public static org.osid.id.Id getMinimumScoreSystem() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.MinimumScoreSystem"));
    }


    /**
     *  Gets the MinimumScore element Id.
     *
     *  @return the MinimumScore element Id
     */

    public static org.osid.id.Id getMinimumScore() {
        return (makeElementId("osid.course.requisite.assessmentrequirement.MinimumScore"));
    }
}
