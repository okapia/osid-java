//
// AbstractAssemblyAuditQuery.java
//
//     An AuditQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inquiry.audit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuditQuery that stores terms.
 */

public abstract class AbstractAssemblyAuditQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.inquiry.AuditQuery,
               org.osid.inquiry.AuditQueryInspector,
               org.osid.inquiry.AuditSearchOrder {

    private final java.util.Collection<org.osid.inquiry.records.AuditQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.AuditQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.AuditSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAuditQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuditQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the inquiry <code> Id </code> for this query. 
     *
     *  @param  inquiryId the inquiry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquiryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquiryId(org.osid.id.Id inquiryId, boolean match) {
        getAssembler().addIdTerm(getInquiryIdColumn(), inquiryId, match);
        return;
    }


    /**
     *  Clears the inquiry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquiryIdTerms() {
        getAssembler().clearTerms(getInquiryIdColumn());
        return;
    }


    /**
     *  Gets the inquiry Id query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryIdTerms() {
        return (getAssembler().getIdTerms(getInquiryIdColumn()));
    }


    /**
     *  Gets the InquiryId column name.
     *
     * @return the column name
     */

    protected String getInquiryIdColumn() {
        return ("inquiry_id");
    }


    /**
     *  Tests if an <code> InquiryQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquiry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquiry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquiry query 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuery getInquiryQuery() {
        throw new org.osid.UnimplementedException("supportsInquiryQuery() is false");
    }


    /**
     *  Matches audits with any inquiry. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchInquiry(boolean match) {
        getAssembler().addBooleanTerm(getInquiryColumn(), match);
        return;
    }


    /**
     *  Clears the inquiry query terms. 
     */

    @OSID @Override
    public void clearInquiryTerms() {
        getAssembler().clearTerms(getInquiryColumn());
        return;
    }


    /**
     *  Gets the inquiry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQueryInspector[] getInquiryTerms() {
        return (new org.osid.inquiry.InquiryQueryInspector[0]);
    }


    /**
     *  Gets the Inquiry column name.
     *
     * @return the column name
     */

    protected String getInquiryColumn() {
        return ("inquiry");
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match audits 
     *  assigned to inquests. 
     *
     *  @param  inquestId the inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquestId(org.osid.id.Id inquestId, boolean match) {
        getAssembler().addIdTerm(getInquestIdColumn(), inquestId, match);
        return;
    }


    /**
     *  Clears the inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquestIdTerms() {
        getAssembler().clearTerms(getInquestIdColumn());
        return;
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (getAssembler().getIdTerms(getInquestIdColumn()));
    }


    /**
     *  Gets the InquestId column name.
     *
     * @return the column name
     */

    protected String getInquestIdColumn() {
        return ("inquest_id");
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getInquestQuery() {
        throw new org.osid.UnimplementedException("supportsInquestQuery() is false");
    }


    /**
     *  Clears the inquest query terms. 
     */

    @OSID @Override
    public void clearInquestTerms() {
        getAssembler().clearTerms(getInquestColumn());
        return;
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }


    /**
     *  Gets the Inquest column name.
     *
     * @return the column name
     */

    protected String getInquestColumn() {
        return ("inquest");
    }


    /**
     *  Tests if this audit supports the given record
     *  <code>Type</code>.
     *
     *  @param  auditRecordType an audit record type 
     *  @return <code>true</code> if the auditRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auditRecordType) {
        for (org.osid.inquiry.records.AuditQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auditRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  auditRecordType the audit record type 
     *  @return the audit query record 
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.AuditQueryRecord getAuditQueryRecord(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.AuditQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  auditRecordType the audit record type 
     *  @return the audit query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.AuditQueryInspectorRecord getAuditQueryInspectorRecord(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.AuditQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(auditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param auditRecordType the audit record type
     *  @return the audit search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.AuditSearchOrderRecord getAuditSearchOrderRecord(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.AuditSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(auditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this audit. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auditQueryRecord the audit query record
     *  @param auditQueryInspectorRecord the audit query inspector
     *         record
     *  @param auditSearchOrderRecord the audit search order record
     *  @param auditRecordType audit record type
     *  @throws org.osid.NullArgumentException
     *          <code>auditQueryRecord</code>,
     *          <code>auditQueryInspectorRecord</code>,
     *          <code>auditSearchOrderRecord</code> or
     *          <code>auditRecordTypeaudit</code> is
     *          <code>null</code>
     */
            
    protected void addAuditRecords(org.osid.inquiry.records.AuditQueryRecord auditQueryRecord, 
                                      org.osid.inquiry.records.AuditQueryInspectorRecord auditQueryInspectorRecord, 
                                      org.osid.inquiry.records.AuditSearchOrderRecord auditSearchOrderRecord, 
                                      org.osid.type.Type auditRecordType) {

        addRecordType(auditRecordType);

        nullarg(auditQueryRecord, "audit query record");
        nullarg(auditQueryInspectorRecord, "audit query inspector record");
        nullarg(auditSearchOrderRecord, "audit search odrer record");

        this.queryRecords.add(auditQueryRecord);
        this.queryInspectorRecords.add(auditQueryInspectorRecord);
        this.searchOrderRecords.add(auditSearchOrderRecord);
        
        return;
    }
}
