//
// AbstractRenovation.java
//
//     Defines a Renovation.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.renovation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Renovation</code>.
 */

public abstract class AbstractRenovation
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.room.construction.Renovation {

    private final java.util.Collection<org.osid.room.Room> rooms = new java.util.LinkedHashSet<>();
    private org.osid.financials.Currency cost;

    private final java.util.Collection<org.osid.room.construction.records.RenovationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the affected rooms. 
     *
     *  @return the room <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRoomIds() {
        try {
            org.osid.room.RoomList rooms = getRooms();
            return (new net.okapia.osid.jamocha.adapter.converter.room.room.RoomToIdList(rooms));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the affected rooms. 
     *
     *  @return the rooms 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.RoomList getRooms()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.room.room.ArrayRoomList(this.rooms));
    }


    /**
     *  Adds a room.
     *
     *  @param room a room
     *  @throws org.osid.NullArgumentException
     *          <code>room</code> is <code>null</code>
     */

    protected void addRoom(org.osid.room.Room room) {
        nullarg(room, "room");
        this.rooms.add(room);
        return;
    }


    /**
     *  Sets all the rooms.
     *
     *  @param rooms a collection of rooms
     *  @throws org.osid.NullArgumentException
     *          <code>rooms</code> is <code>null</code>
     */

    protected void setRooms(java.util.Collection<org.osid.room.Room> rooms) {
        nullarg(rooms, "rooms");
        this.rooms.clear();
        this.rooms.addAll(rooms);
        return;
    }


    /**
     *  Tests if this renovation has a cost. 
     *
     *  @return <code> true </code> if a cost is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCost() {
        return (this.cost != null);
    }


    /**
     *  Gets the cost for this renovation. 
     *
     *  @return the cost 
     *  @throws org.osid.IllegalStateException <code> hasCost() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCost() {
        if (!hasCost()) {
            throw new org.osid.IllegalStateException("hasCost() is false");
        }

        return (this.cost);
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    protected void setCost(org.osid.financials.Currency cost) {
        nullarg(cost, "cost");
        this.cost = cost;
        return;
    }


    /**
     *  Tests if this renovation supports the given record
     *  <code>Type</code>.
     *
     *  @param  renovationRecordType a renovation record type 
     *  @return <code>true</code> if the renovationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type renovationRecordType) {
        for (org.osid.room.construction.records.RenovationRecord record : this.records) {
            if (record.implementsRecordType(renovationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Renovation</code> record <code>Type</code>.
     *
     *  @param  renovationRecordType the renovation record type 
     *  @return the renovation record 
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(renovationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationRecord getRenovationRecord(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.RenovationRecord record : this.records) {
            if (record.implementsRecordType(renovationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(renovationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this renovation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param renovationRecord the renovation record
     *  @param renovationRecordType renovation record type
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecord</code> or
     *          <code>renovationRecordTyperenovation</code> is
     *          <code>null</code>
     */
            
    protected void addRenovationRecord(org.osid.room.construction.records.RenovationRecord renovationRecord, 
                                       org.osid.type.Type renovationRecordType) {

        nullarg(renovationRecord, "renovation record");
        addRecordType(renovationRecordType);
        this.records.add(renovationRecord);
        
        return;
    }
}
