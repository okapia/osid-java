//
// AbstractSupersedingEventEnablerSearch.java
//
//     A template for making a SupersedingEventEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing superseding event enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSupersedingEventEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.rules.SupersedingEventEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.rules.SupersedingEventEnablerSearchOrder supersedingEventEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of superseding event enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  supersedingEventEnablerIds list of superseding event enablers
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSupersedingEventEnablers(org.osid.id.IdList supersedingEventEnablerIds) {
        while (supersedingEventEnablerIds.hasNext()) {
            try {
                this.ids.add(supersedingEventEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSupersedingEventEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of superseding event enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSupersedingEventEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  supersedingEventEnablerSearchOrder superseding event enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>supersedingEventEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSupersedingEventEnablerResults(org.osid.calendaring.rules.SupersedingEventEnablerSearchOrder supersedingEventEnablerSearchOrder) {
	this.supersedingEventEnablerSearchOrder = supersedingEventEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.rules.SupersedingEventEnablerSearchOrder getSupersedingEventEnablerSearchOrder() {
	return (this.supersedingEventEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given superseding event enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a superseding event enabler implementing the requested record.
     *
     *  @param supersedingEventEnablerSearchRecordType a superseding event enabler search record
     *         type
     *  @return the superseding event enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerSearchRecord getSupersedingEventEnablerSearchRecord(org.osid.type.Type supersedingEventEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.rules.records.SupersedingEventEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this superseding event enabler search. 
     *
     *  @param supersedingEventEnablerSearchRecord superseding event enabler search record
     *  @param supersedingEventEnablerSearchRecordType supersedingEventEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSupersedingEventEnablerSearchRecord(org.osid.calendaring.rules.records.SupersedingEventEnablerSearchRecord supersedingEventEnablerSearchRecord, 
                                           org.osid.type.Type supersedingEventEnablerSearchRecordType) {

        addRecordType(supersedingEventEnablerSearchRecordType);
        this.records.add(supersedingEventEnablerSearchRecord);        
        return;
    }
}
