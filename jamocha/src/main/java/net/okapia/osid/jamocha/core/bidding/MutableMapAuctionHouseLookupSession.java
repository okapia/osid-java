//
// MutableMapAuctionHouseLookupSession
//
//    Implements an AuctionHouse lookup service backed by a collection of
//    auctionHouses that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding;


/**
 *  Implements an AuctionHouse lookup service backed by a collection of
 *  auction houses. The auction houses are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of auction houses can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.core.bidding.spi.AbstractMapAuctionHouseLookupSession
    implements org.osid.bidding.AuctionHouseLookupSession {


    /**
     *  Constructs a new {@code MutableMapAuctionHouseLookupSession}
     *  with no auction houses.
     */

    public MutableMapAuctionHouseLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionHouseLookupSession} with a
     *  single auctionHouse.
     *  
     *  @param auctionHouse an auction house
     *  @throws org.osid.NullArgumentException {@code auctionHouse}
     *          is {@code null}
     */

    public MutableMapAuctionHouseLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        putAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionHouseLookupSession}
     *  using an array of auction houses.
     *
     *  @param auctionHouses an array of auction houses
     *  @throws org.osid.NullArgumentException {@code auctionHouses}
     *          is {@code null}
     */

    public MutableMapAuctionHouseLookupSession(org.osid.bidding.AuctionHouse[] auctionHouses) {
        putAuctionHouses(auctionHouses);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionHouseLookupSession}
     *  using a collection of auction houses.
     *
     *  @param auctionHouses a collection of auction houses
     *  @throws org.osid.NullArgumentException {@code auctionHouses}
     *          is {@code null}
     */

    public MutableMapAuctionHouseLookupSession(java.util.Collection<? extends org.osid.bidding.AuctionHouse> auctionHouses) {
        putAuctionHouses(auctionHouses);
        return;
    }

    
    /**
     *  Makes an {@code AuctionHouse} available in this session.
     *
     *  @param auctionHouse an auction house
     *  @throws org.osid.NullArgumentException {@code auctionHouse{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        super.putAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Makes an array of auction houses available in this session.
     *
     *  @param auctionHouses an array of auction houses
     *  @throws org.osid.NullArgumentException {@code auctionHouses{@code 
     *          is {@code null}
     */

    @Override
    public void putAuctionHouses(org.osid.bidding.AuctionHouse[] auctionHouses) {
        super.putAuctionHouses(auctionHouses);
        return;
    }


    /**
     *  Makes collection of auction houses available in this session.
     *
     *  @param auctionHouses a collection of auction houses
     *  @throws org.osid.NullArgumentException {@code auctionHouses{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionHouses(java.util.Collection<? extends org.osid.bidding.AuctionHouse> auctionHouses) {
        super.putAuctionHouses(auctionHouses);
        return;
    }


    /**
     *  Removes an AuctionHouse from this session.
     *
     *  @param auctionHouseId the {@code Id} of the auction house
     *  @throws org.osid.NullArgumentException {@code auctionHouseId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAuctionHouse(org.osid.id.Id auctionHouseId) {
        super.removeAuctionHouse(auctionHouseId);
        return;
    }    
}
