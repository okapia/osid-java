//
// AbstractAssemblyValueEnablerQuery.java
//
//     A ValueEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.configuration.rules.valueenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ValueEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyValueEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.configuration.rules.ValueEnablerQuery,
               org.osid.configuration.rules.ValueEnablerQueryInspector,
               org.osid.configuration.rules.ValueEnablerSearchOrder {

    private final java.util.Collection<org.osid.configuration.rules.records.ValueEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ValueEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyValueEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyValueEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the value. 
     *
     *  @param  valueId the value <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> valueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledValueId(org.osid.id.Id valueId, boolean match) {
        getAssembler().addIdTerm(getRuledValueIdColumn(), valueId, match);
        return;
    }


    /**
     *  Clears the value <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledValueIdTerms() {
        getAssembler().clearTerms(getRuledValueIdColumn());
        return;
    }


    /**
     *  Gets the value <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledValueIdTerms() {
        return (getAssembler().getIdTerms(getRuledValueIdColumn()));
    }


    /**
     *  Gets the RuledValueId column name.
     *
     * @return the column name
     */

    protected String getRuledValueIdColumn() {
        return ("ruled_value_id");
    }


    /**
     *  Tests if a <code> ValueQuery </code> is available. 
     *
     *  @return <code> true </code> if a value query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledValueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a value. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the value query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleValueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuery getRuledValueQuery() {
        throw new org.osid.UnimplementedException("supportsRuledValueQuery() is false");
    }


    /**
     *  Matches enablers mapped to any value. 
     *
     *  @param  match <code> true </code> for enablers mapped to any value, 
     *          <code> false </code> to match enablers mapped to no value 
     */

    @OSID @Override
    public void matchAnyRuledValue(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledValueColumn(), match);
        return;
    }


    /**
     *  Clears the value query terms. 
     */

    @OSID @Override
    public void clearRuledValueTerms() {
        getAssembler().clearTerms(getRuledValueColumn());
        return;
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ValueQueryInspector[] getRuledValueTerms() {
        return (new org.osid.configuration.ValueQueryInspector[0]);
    }


    /**
     *  Gets the RuledValue column name.
     *
     * @return the column name
     */

    protected String getRuledValueColumn() {
        return ("ruled_value");
    }


    /**
     *  Matches enablers mapped to the configuration. 
     *
     *  @param  configurationId the configuration <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getConfigurationIdColumn(), configurationId, match);
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        getAssembler().clearTerms(getConfigurationIdColumn());
        return;
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (getAssembler().getIdTerms(getConfigurationIdColumn()));
    }


    /**
     *  Gets the ConfigurationId column name.
     *
     * @return the column name
     */

    protected String getConfigurationIdColumn() {
        return ("configuration_id");
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration query terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        getAssembler().clearTerms(getConfigurationColumn());
        return;
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the Configuration column name.
     *
     * @return the column name
     */

    protected String getConfigurationColumn() {
        return ("configuration");
    }


    /**
     *  Tests if this valueEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  valueEnablerRecordType a value enabler record type 
     *  @return <code>true</code> if the valueEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type valueEnablerRecordType) {
        for (org.osid.configuration.rules.records.ValueEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(valueEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  valueEnablerRecordType the value enabler record type 
     *  @return the value enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ValueEnablerQueryRecord getValueEnablerQueryRecord(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ValueEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(valueEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  valueEnablerRecordType the value enabler record type 
     *  @return the value enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord getValueEnablerQueryInspectorRecord(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(valueEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param valueEnablerRecordType the value enabler record type
     *  @return the value enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ValueEnablerSearchOrderRecord getValueEnablerSearchOrderRecord(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ValueEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(valueEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this value enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param valueEnablerQueryRecord the value enabler query record
     *  @param valueEnablerQueryInspectorRecord the value enabler query inspector
     *         record
     *  @param valueEnablerSearchOrderRecord the value enabler search order record
     *  @param valueEnablerRecordType value enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerQueryRecord</code>,
     *          <code>valueEnablerQueryInspectorRecord</code>,
     *          <code>valueEnablerSearchOrderRecord</code> or
     *          <code>valueEnablerRecordTypevalueEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addValueEnablerRecords(org.osid.configuration.rules.records.ValueEnablerQueryRecord valueEnablerQueryRecord, 
                                      org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord valueEnablerQueryInspectorRecord, 
                                      org.osid.configuration.rules.records.ValueEnablerSearchOrderRecord valueEnablerSearchOrderRecord, 
                                      org.osid.type.Type valueEnablerRecordType) {

        addRecordType(valueEnablerRecordType);

        nullarg(valueEnablerQueryRecord, "value enabler query record");
        nullarg(valueEnablerQueryInspectorRecord, "value enabler query inspector record");
        nullarg(valueEnablerSearchOrderRecord, "value enabler search odrer record");

        this.queryRecords.add(valueEnablerQueryRecord);
        this.queryInspectorRecords.add(valueEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(valueEnablerSearchOrderRecord);
        
        return;
    }
}
