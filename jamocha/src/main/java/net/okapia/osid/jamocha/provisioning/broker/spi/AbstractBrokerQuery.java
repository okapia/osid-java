//
// AbstractBrokerQuery.java
//
//     A template for making a Broker Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.broker.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for brokers.
 */

public abstract class AbstractBrokerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.provisioning.BrokerQuery {

    private final java.util.Collection<org.osid.provisioning.records.BrokerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Matches brokers with any queue. 
     *
     *  @param  match <code> true </code> to match brokers with any queue, 
     *          <code> false </code> to match brokers with no queue 
     */

    @OSID @Override
    public void matchAnyQueue(boolean match) {
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        return;
    }


    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Matches brokers with any pool. 
     *
     *  @param  match <code> true </code> to match brokers with any pool, 
     *          <code> false </code> to match brokers with no pool 
     */

    @OSID @Override
    public void matchAnyPool(boolean match) {
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        return;
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given broker query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker implementing the requested record.
     *
     *  @param brokerRecordType a broker record type
     *  @return the broker query record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.BrokerQueryRecord getBrokerQueryRecord(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.BrokerQueryRecord record : this.records) {
            if (record.implementsRecordType(brokerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker query. 
     *
     *  @param brokerQueryRecord broker query record
     *  @param brokerRecordType broker record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerQueryRecord(org.osid.provisioning.records.BrokerQueryRecord brokerQueryRecord, 
                                          org.osid.type.Type brokerRecordType) {

        addRecordType(brokerRecordType);
        nullarg(brokerQueryRecord, "broker query record");
        this.records.add(brokerQueryRecord);        
        return;
    }
}
