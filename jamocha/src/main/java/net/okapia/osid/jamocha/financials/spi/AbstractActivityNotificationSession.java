//
// AbstractActivityNotificationSession.java
//
//     A template for making ActivityNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Activity} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Activity} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for activity entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractActivityNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.ActivityNotificationSession {

    private boolean federated = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();


    /**
     *  Gets the {@code Business/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }

    
    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the {@code Business}.
     *
     *  @param business the business for this session
     *  @throws org.osid.NullArgumentException {@code business}
     *          is {@code null}
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can register for {@code Activity}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForActivityNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for activities in businesses
     *  which are children of this business in the business
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new activities. {@code
     *  ActivityReceiver.newActivity()} is invoked when an new {@code
     *  Activity} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  activity. {@code ActivityReceiver.newAncestorActivity()} is
     *  invoked when the specified activity node gets a new ancestor.
     *
     *  @param activityId the {@code Id} of the
     *         {@code Activity} node to monitor
     *  @throws org.osid.NullArgumentException {@code activityId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewActivityAncestors(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  activity. {@code ActivityReceiver.newDescendantActivity()} is
     *  invoked when the specified activity node gets a new
     *  descendant.
     *
     *  @param activityId the {@code Id} of the
     *         {@code Activity} node to monitor
     *  @throws org.osid.NullArgumentException {@code activityId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewActivityDescendants(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated activities. {@code
     *  ActivityReceiver.changedActivity()} is invoked when an
     *  activity is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated activity. {@code
     *  ActivityReceiver.changedActivity()} is invoked when the
     *  specified activity is changed.
     *
     *  @param activityId the {@code Id} of the {@code Activity} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code activityId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted activities. {@code
     *  ActivityReceiver.deletedActivity()} is invoked when an
     *  activity is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted activity. {@code
     *  ActivityReceiver.deletedActivity()} is invoked when the
     *  specified activity is deleted.
     *
     *  @param activityId the {@code Id} of the
     *          {@code Activity} to monitor
     *  @throws org.osid.NullArgumentException {@code activityId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified activity. {@code
     *  ActivityReceiver.deletedAncestor()} is invoked when the
     *  specified activity node loses an ancestor.
     *
     *  @param activityId the {@code Id} of the
     *         {@code Activity} node to monitor
     *  @throws org.osid.NullArgumentException {@code activityId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedActivityAncestors(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified activity. {@code
     *  ActivityReceiver.deletedDescendant()} is invoked when the
     *  specified activity node loses a descendant.
     *
     *  @param activityId the {@code Id} of the
     *          {@code Activity} node to monitor
     *  @throws org.osid.NullArgumentException {@code activityId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedActivityDescendants(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
