//
// InvariantMapCommitmentEnablerLookupSession
//
//    Implements a CommitmentEnabler lookup service backed by a fixed collection of
//    commitmentEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements a CommitmentEnabler lookup service backed by a fixed
 *  collection of commitment enablers. The commitment enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCommitmentEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractMapCommitmentEnablerLookupSession
    implements org.osid.calendaring.rules.CommitmentEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCommitmentEnablerLookupSession</code> with no
     *  commitment enablers.
     *  
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumnetException {@code calendar} is
     *          {@code null}
     */

    public InvariantMapCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCommitmentEnablerLookupSession</code> with a single
     *  commitment enabler.
     *  
     *  @param calendar the calendar
     *  @param commitmentEnabler a single commitment enabler
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code commitmentEnabler} is <code>null</code>
     */

      public InvariantMapCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler) {
        this(calendar);
        putCommitmentEnabler(commitmentEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCommitmentEnablerLookupSession</code> using an array
     *  of commitment enablers.
     *  
     *  @param calendar the calendar
     *  @param commitmentEnablers an array of commitment enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code commitmentEnablers} is <code>null</code>
     */

      public InvariantMapCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.rules.CommitmentEnabler[] commitmentEnablers) {
        this(calendar);
        putCommitmentEnablers(commitmentEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCommitmentEnablerLookupSession</code> using a
     *  collection of commitment enablers.
     *
     *  @param calendar the calendar
     *  @param commitmentEnablers a collection of commitment enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code commitmentEnablers} is <code>null</code>
     */

      public InvariantMapCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               java.util.Collection<? extends org.osid.calendaring.rules.CommitmentEnabler> commitmentEnablers) {
        this(calendar);
        putCommitmentEnablers(commitmentEnablers);
        return;
    }
}
