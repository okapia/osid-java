//
// AbstractAssemblyActivityQuery.java
//
//     An ActivityQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActivityQuery that stores terms.
 */

public abstract class AbstractAssemblyActivityQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.ActivityQuery,
               org.osid.course.ActivityQueryInspector,
               org.osid.course.ActivitySearchOrder {

    private final java.util.Collection<org.osid.course.records.ActivityQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.ActivityQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.ActivitySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActivityQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActivityQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches an activity that is implicitly generated. 
     *
     *  @param  match <code> true </code> to match activities implicitly 
     *          generated, <code> false </code> to match activities explicitly 
     *          defined 
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        getAssembler().addBooleanTerm(getImplicitColumn(), match);
        return;
    }


    /**
     *  Clears the implcit terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        getAssembler().clearTerms(getImplicitColumn());
        return;
    }


    /**
     *  Gets the implicit terms. 
     *
     *  @return the implicit terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getImplicitTerms() {
        return (getAssembler().getBooleanTerms(getImplicitColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  implicit flag.
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByImplicit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getImplicitColumn(), style);
        return;
    }


    /**
     *  Gets the Implicit column name.
     *
     * @return the column name
     */

    protected String getImplicitColumn() {
        return ("implicit");
    }


    /**
     *  Sets the activity unit <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id activityUnitId, 
                                    boolean match) {
        getAssembler().addIdTerm(getActivityUnitIdColumn(), activityUnitId, match);
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        getAssembler().clearTerms(getActivityUnitIdColumn());
        return;
    }


    /**
     *  Gets the activity unit <code> Id </code> query terms. 
     *
     *  @return the activity unit <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityUnitIdTerms() {
        return (getAssembler().getIdTerms(getActivityUnitIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by activity unit. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivityUnit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActivityUnitColumn(), style);
        return;
    }


    /**
     *  Gets the ActivityUnitId column name.
     *
     * @return the column name
     */

    protected String getActivityUnitIdColumn() {
        return ("activity_unit_id");
    }


    /**
     *  Tests if an <code> ActivityUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Clears the activity unit terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        getAssembler().clearTerms(getActivityUnitColumn());
        return;
    }


    /**
     *  Gets the activity unit query terms. 
     *
     *  @return the activity unit query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQueryInspector[] getActivityUnitTerms() {
        return (new org.osid.course.ActivityUnitQueryInspector[0]);
    }


    /**
     *  Tests if an activity unit search order is available. 
     *
     *  @return <code> true </code> if an activity unit search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitSearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity unit search order. 
     *
     *  @return the activity unit search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getActivityUnitSearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivityUnitSearchOrder() is false");
    }


    /**
     *  Gets the ActivityUnit column name.
     *
     * @return the column name
     */

    protected String getActivityUnitColumn() {
        return ("activity_unit");
    }


    /**
     *  Sets the course offering <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        getAssembler().addIdTerm(getCourseOfferingIdColumn(), courseOfferingId, match);
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        getAssembler().clearTerms(getCourseOfferingIdColumn());
        return;
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (getAssembler().getIdTerms(getCourseOfferingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course offering. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourseOffering(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseOfferingColumn(), style);
        return;
    }


    /**
     *  Gets the CourseOfferingId column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingIdColumn() {
        return ("course_offering_id");
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a coure offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Clears the course offering terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        getAssembler().clearTerms(getCourseOfferingColumn());
        return;
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Tests if a course offering search is available. 
     *
     *  @return <code> true </code> if a course offering search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course offering search order. 
     *
     *  @return the course offering search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchOrder getCourseOfferingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingSearchOrder() is false");
    }


    /**
     *  Gets the CourseOffering column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingColumn() {
        return ("course_offering");
    }


    /**
     *  Sets the term <code> Id </code> for this query to match course 
     *  offerings that have a related term. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        getAssembler().clearTerms(getTermIdColumn());
        return;
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (getAssembler().getIdTerms(getTermIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by term. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerm(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTermColumn(), style);
        return;
    }


    /**
     *  Gets the TermId column name.
     *
     * @return the column name
     */

    protected String getTermIdColumn() {
        return ("term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a reporting term. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        getAssembler().clearTerms(getTermColumn());
        return;
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term query terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Tests if a term search order is available. 
     *
     *  @return <code> true </code> if a term search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearchOrder() {
        return (false);
    }


    /**
     *  Gets the term order. 
     *
     *  @return the term search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchOrder getTermSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTermSearchOrder() is false");
    }


    /**
     *  Gets the Term column name.
     *
     * @return the column name
     */

    protected String getTermColumn() {
        return ("term");
    }


    /**
     *  Matches activities where the given time falls within a denormalized 
     *  meeting time inclusive. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMeetingTime(org.osid.calendaring.DateTime date, 
                                 boolean match) {
        getAssembler().addDateTimeTerm(getMeetingTimeColumn(), date, match);
        return;
    }


    /**
     *  Clears the meeting time terms. 
     */

    @OSID @Override
    public void clearMeetingTimeTerms() {
        getAssembler().clearTerms(getMeetingTimeColumn());
        return;
    }


    /**
     *  Gets the meeting time query terms. 
     *
     *  @return the date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getMeetingTimeTerms() {
        return (getAssembler().getDateTimeTerms(getMeetingTimeColumn()));
    }


    /**
     *  Gets the MeetingTime column name.
     *
     * @return the column name
     */

    protected String getMeetingTimeColumn() {
        return ("meeting_time");
    }


    /**
     *  Matches activities with any denormalized meeting time within the given 
     *  date range inclusive. 
     *
     *  @param  start a start date 
     *  @param  end an end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchMeetingTimeInclusive(org.osid.calendaring.DateTime start, 
                                          org.osid.calendaring.DateTime end, 
                                          boolean match) {
        getAssembler().addDateTimeRangeTerm(getMeetingTimeInclusiveColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the meeting time inclusive terms. 
     */

    @OSID @Override
    public void clearMeetingTimeInclusiveTerms() {
        getAssembler().clearTerms(getMeetingTimeInclusiveColumn());
        return;
    }


    /**
     *  Gets the meeting time query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getMeetingTimeInclusiveTerms() {
        return (getAssembler().getDateTimeRangeTerms(getMeetingTimeInclusiveColumn()));
    }


    /**
     *  Gets the MeetingTimeInclusive column name.
     *
     * @return the column name
     */

    protected String getMeetingTimeInclusiveColumn() {
        return ("meeting_time_inclusive");
    }


    /**
     *  Matches activities with any meeting location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeetingLocationId(org.osid.id.Id locationId, 
                                       boolean match) {
        getAssembler().addIdTerm(getMeetingLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMeetingLocationIdTerms() {
        getAssembler().clearTerms(getMeetingLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the location <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMeetingLocationIdTerms() {
        return (getAssembler().getIdTerms(getMeetingLocationIdColumn()));
    }


    /**
     *  Gets the MeetingLocationId column name.
     *
     * @return the column name
     */

    protected String getMeetingLocationIdColumn() {
        return ("meeting_location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeetingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeetingLocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getMeetingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsMeetingLocationQuery() is false");
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMeetingLocationTerms() {
        getAssembler().clearTerms(getMeetingLocationColumn());
        return;
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the location query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getMeetingLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the MeetingLocation column name.
     *
     * @return the column name
     */

    protected String getMeetingLocationColumn() {
        return ("meeting_location");
    }


    /**
     *  Sets the schedule <code> Id </code> for this query. 
     *
     *  @param  scheduleId a schedule <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleId(org.osid.id.Id scheduleId, boolean match) {
        getAssembler().addIdTerm(getScheduleIdColumn(), scheduleId, match);
        return;
    }


    /**
     *  Clears the schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleIdTerms() {
        getAssembler().clearTerms(getScheduleIdColumn());
        return;
    }


    /**
     *  Gets the schedule <code> Id </code> query terms. 
     *
     *  @return the schedule <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleIdTerms() {
        return (getAssembler().getIdTerms(getScheduleIdColumn()));
    }


    /**
     *  Gets the ScheduleId column name.
     *
     * @return the column name
     */

    protected String getScheduleIdColumn() {
        return ("schedule_id");
    }


    /**
     *  Tests if a <code> ScheduleQuery </code> is available. 
     *
     *  @return <code> true </code> if a schedule query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedule. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the schedule query 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuery getScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleQuery() is false");
    }


    /**
     *  Matches an activity that has any schedule. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          schedule, <code> false </code> to match activitiies with no 
     *          schedule 
     */

    @OSID @Override
    public void matchAnySchedule(boolean match) {
        getAssembler().addIdWildcardTerm(getScheduleColumn(), match);
        return;
    }


    /**
     *  Clears the schedule terms. 
     */

    @OSID @Override
    public void clearScheduleTerms() {
        getAssembler().clearTerms(getScheduleColumn());
        return;
    }


    /**
     *  Gets the schedule query terms. 
     *
     *  @return the schedule query terms 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQueryInspector[] getScheduleTerms() {
        return (new org.osid.calendaring.ScheduleQueryInspector[0]);
    }


    /**
     *  Gets the Schedule column name.
     *
     * @return the column name
     */

    protected String getScheduleColumn() {
        return ("schedule");
    }


    /**
     *  Sets the superseding activity <code> Id </code> for this query. 
     *
     *  @param  activityId a superseding activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSupersedingActivityId(org.osid.id.Id activityId, 
                                           boolean match) {
        getAssembler().addIdTerm(getSupersedingActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the superseding activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersedingActivityIdTerms() {
        getAssembler().clearTerms(getSupersedingActivityIdColumn());
        return;
    }


    /**
     *  Gets the superseding activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersedingActivityIdTerms() {
        return (getAssembler().getIdTerms(getSupersedingActivityIdColumn()));
    }


    /**
     *  Gets the SupersedingActivityId column name.
     *
     * @return the column name
     */

    protected String getSupersedingActivityIdColumn() {
        return ("superseding_activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if a superseding activity query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding activity. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the superseding activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getSupersedingActivityQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingActivityQuery() is false");
    }


    /**
     *  Matches an activity that has any superseding activity. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          superseding activity, <code> false </code> to match 
     *          activitiies with no superseding activities 
     */

    @OSID @Override
    public void matchAnySupersedingActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getSupersedingActivityColumn(), match);
        return;
    }


    /**
     *  Clears the superseding activity terms. 
     */

    @OSID @Override
    public void clearSupersedingActivityTerms() {
        getAssembler().clearTerms(getSupersedingActivityColumn());
        return;
    }


    /**
     *  Gets the superseding activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getSupersedingActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the SupersedingActivity column name.
     *
     * @return the column name
     */

    protected String getSupersedingActivityColumn() {
        return ("superseding_activity");
    }


    /**
     *  Matches activities with specific dates between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchSpecificMeetingTime(org.osid.calendaring.DateTime start, 
                                         org.osid.calendaring.DateTime end, 
                                         boolean match) {
        getAssembler().addDateTimeRangeTerm(getSpecificMeetingTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches an activity that has any specific date. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          specific date, <code> false </code> to match activitiies with 
     *          no specific dates 
     */

    @OSID @Override
    public void matchAnySpecificMeetingTime(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getSpecificMeetingTimeColumn(), match);
        return;
    }


    /**
     *  Clears the specific date terms. 
     */

    @OSID @Override
    public void clearSpecificMeetingTimeTerms() {
        getAssembler().clearTerms(getSpecificMeetingTimeColumn());
        return;
    }


    /**
     *  Gets the specific meeting time query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getSpecificMeetingTimeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getSpecificMeetingTimeColumn()));
    }


    /**
     *  Gets the SpecificMeetingTime column name.
     *
     * @return the column name
     */

    protected String getSpecificMeetingTimeColumn() {
        return ("specific_meeting_time");
    }


    /**
     *  Matches activities containing the given blackout date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBlackout(org.osid.calendaring.DateTime date, 
                              boolean match) {
        getAssembler().addDateTimeTerm(getBlackoutColumn(), date, match);
        return;
    }


    /**
     *  Matches an activity that has any blackout. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          blackout, <code> false </code> to match activitiies with no 
     *          blackouts 
     */

    @OSID @Override
    public void matchAnyBlackout(boolean match) {
        getAssembler().addDateTimeWildcardTerm(getBlackoutColumn(), match);
        return;
    }


    /**
     *  Clears all blackout terms. 
     */

    @OSID @Override
    public void clearBlackoutTerms() {
        getAssembler().clearTerms(getBlackoutColumn());
        return;
    }


    /**
     *  Gets the blackout query terms. 
     *
     *  @return the blackout query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getBlackoutTerms() {
        return (getAssembler().getDateTimeTerms(getBlackoutColumn()));
    }


    /**
     *  Gets the Blackout column name.
     *
     * @return the column name
     */

    protected String getBlackoutColumn() {
        return ("blackout");
    }


    /**
     *  Matches activities with blackouts within the given date range 
     *  inclousive. 
     *
     *  @param  start a start date 
     *  @param  end an end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> star </code> t or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBlackoutInclusive(org.osid.calendaring.DateTime start, 
                                       org.osid.calendaring.DateTime end, 
                                       boolean match) {
        getAssembler().addDateTimeRangeTerm(getBlackoutInclusiveColumn(), start, end, match);
        return;
    }


    /**
     *  Clears all blackout inclusive terms. 
     */

    @OSID @Override
    public void clearBlackoutInclusiveTerms() {
        getAssembler().clearTerms(getBlackoutInclusiveColumn());
        return;
    }


    /**
     *  Gets the inclusive blackout query terms. 
     *
     *  @return the blackout query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBlackoutInclusiveTerms() {
        return (getAssembler().getDateTimeRangeTerms(getBlackoutInclusiveColumn()));
    }


    /**
     *  Gets the BlackoutInclusive column name.
     *
     * @return the column name
     */

    protected String getBlackoutInclusiveColumn() {
        return ("blackout_inclusive");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match course 
     *  offerings that have an instructor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getInstructorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the instructor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearInstructorIdTerms() {
        getAssembler().clearTerms(getInstructorIdColumn());
        return;
    }


    /**
     *  Gets the instructor <code> Id </code> query terms. 
     *
     *  @return the instructor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstructorIdTerms() {
        return (getAssembler().getIdTerms(getInstructorIdColumn()));
    }


    /**
     *  Gets the InstructorId column name.
     *
     * @return the column name
     */

    protected String getInstructorIdColumn() {
        return ("instructor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instructor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getInstructorQuery() {
        throw new org.osid.UnimplementedException("supportsInstructorQuery() is false");
    }


    /**
     *  Matches course offerings that have any instructor. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          instructor, <code> false </code> to match course offerings 
     *          with no instructors 
     */

    @OSID @Override
    public void matchAnyInstructor(boolean match) {
        getAssembler().addIdWildcardTerm(getInstructorColumn(), match);
        return;
    }


    /**
     *  Clears the instructor terms. 
     */

    @OSID @Override
    public void clearInstructorTerms() {
        getAssembler().clearTerms(getInstructorColumn());
        return;
    }


    /**
     *  Gets the instructor query terms. 
     *
     *  @return the instructor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getInstructorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Instructor column name.
     *
     * @return the column name
     */

    protected String getInstructorColumn() {
        return ("instructor");
    }


    /**
     *  Matches activities with a minimum seating between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMinimumSeats(long min, long max, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumSeatsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches activities with any minimum seating assigned. 
     *
     *  @param  match <code> true </code> to match activities with any minimum 
     *          seating, <code> false </code> to match activities with no 
     *          minimum seating 
     */

    @OSID @Override
    public void matchAnyMinimumSeats(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMinimumSeatsColumn(), match);
        return;
    }


    /**
     *  Clears the minimum seating terms. 
     */

    @OSID @Override
    public void clearMinimumSeatsTerms() {
        getAssembler().clearTerms(getMinimumSeatsColumn());
        return;
    }


    /**
     *  Gets the minimum seating terms. 
     *
     *  @return the minimum seat query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumSeatsTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumSeatsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumSeats(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumSeatsColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumSeats column name.
     *
     * @return the column name
     */

    protected String getMinimumSeatsColumn() {
        return ("minimum_seats");
    }


    /**
     *  Matches activities with a maximum seating between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMaximumSeats(long min, long max, boolean match) {
        getAssembler().addCardinalRangeTerm(getMaximumSeatsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches activities with any maximum seating assigned. 
     *
     *  @param  match <code> true </code> to match activities with any maximum 
     *          seating, <code> false </code> to match activities with no 
     *          maximum seating 
     */

    @OSID @Override
    public void matchAnyMaximumSeats(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMaximumSeatsColumn(), match);
        return;
    }


    /**
     *  Clears the maximum seating terms. 
     */

    @OSID @Override
    public void clearMaximumSeatsTerms() {
        getAssembler().clearTerms(getMaximumSeatsColumn());
        return;
    }


    /**
     *  Gets the maximum seating terms. 
     *
     *  @return the maximum seat query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumSeatsTerms() {
        return (getAssembler().getCardinalRangeTerms(getMaximumSeatsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumSeats(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMaximumSeatsColumn(), style);
        return;
    }


    /**
     *  Gets the MaximumSeats column name.
     *
     * @return the column name
     */

    protected String getMaximumSeatsColumn() {
        return ("maximum_seats");
    }


    /**
     *  Matches activities with a total effort between the given durations 
     *  inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetEffort(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getTotalTargetEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity that has any total effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any total 
     *          effort, <code> false </code> to match activities with no total 
     *          effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getTotalTargetEffortColumn(), match);
        return;
    }


    /**
     *  Clears the total effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetEffortTerms() {
        getAssembler().clearTerms(getTotalTargetEffortColumn());
        return;
    }


    /**
     *  Gets the total effort query terms. 
     *
     *  @return the total effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalTargetEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the total 
     *  effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalTargetEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalTargetEffortColumn(), style);
        return;
    }


    /**
     *  Gets the TotalTargetEffort column name.
     *
     * @return the column name
     */

    protected String getTotalTargetEffortColumn() {
        return ("total_target_effort");
    }


    /**
     *  Matches activities that are contact activities. 
     *
     *  @param  match <code> true </code> to match activities that have 
     *          contact, <code> false </code> to match activities with no 
     *          contact 
     */

    @OSID @Override
    public void matchContact(boolean match) {
        getAssembler().addBooleanTerm(getContactColumn(), match);
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        getAssembler().clearTerms(getContactColumn());
        return;
    }


    /**
     *  Gets the contact query terms. 
     *
     *  @return the contact query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getContactTerms() {
        return (getAssembler().getBooleanTerms(getContactColumn()));
    }


    /**
     *  Gets the Contact column name.
     *
     * @return the column name
     */

    protected String getContactColumn() {
        return ("contact");
    }


    /**
     *  Matches activities with a total contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetContactTime(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        getAssembler().addDurationRangeTerm(getTotalTargetContactTimeColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity that has any total contact assigned. 
     *
     *  @param  match <code> true </code> to match activities with any total 
     *          contatc, <code> false </code> to match activities with no 
     *          total contact 
     */

    @OSID @Override
    public void matchAnyTotalTargetContactTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getTotalTargetContactTimeColumn(), match);
        return;
    }


    /**
     *  Clears the total contact terms. 
     */

    @OSID @Override
    public void clearTotalTargetContactTimeTerms() {
        getAssembler().clearTerms(getTotalTargetContactTimeColumn());
        return;
    }


    /**
     *  Gets the total contact time query terms. 
     *
     *  @return the total contact time query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetContactTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalTargetContactTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the total 
     *  contact. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalTargetContactTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalTargetContactTimeColumn(), style);
        return;
    }


    /**
     *  Gets the TotalTargetContactTime column name.
     *
     * @return the column name
     */

    protected String getTotalTargetContactTimeColumn() {
        return ("total_target_contact_time");
    }


    /**
     *  Matches activities with a individual effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetIndividualEffort(org.osid.calendaring.Duration min, 
                                                 org.osid.calendaring.Duration max, 
                                                 boolean match) {
        getAssembler().addDurationRangeTerm(getTotalTargetIndividualEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity that has any individual effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          individual effort, <code> false </code> to match activities 
     *          with no individual effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetIndividualEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getTotalTargetIndividualEffortColumn(), match);
        return;
    }


    /**
     *  Clears the individual effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetIndividualEffortTerms() {
        getAssembler().clearTerms(getTotalTargetIndividualEffortColumn());
        return;
    }


    /**
     *  Gets the total individual effort query terms. 
     *
     *  @return the total individual effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetIndividualEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getTotalTargetIndividualEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by total individual 
     *  effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalTargetIndividualEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTotalTargetIndividualEffortColumn(), style);
        return;
    }


    /**
     *  Gets the TotalTargetIndividualEffort column name.
     *
     * @return the column name
     */

    protected String getTotalTargetIndividualEffortColumn() {
        return ("total_target_individual_effort");
    }


    /**
     *  Matches activities that recur weekly. 
     *
     *  @param  match <code> true </code> to match activities that recur 
     *          weekly, <code> false </code> to match activities with no 
     *          weekly recurrance 
     */

    @OSID @Override
    public void matchRecurringWeekly(boolean match) {
        getAssembler().addBooleanTerm(getRecurringWeeklyColumn(), match);
        return;
    }


    /**
     *  Clears the recurring weekly terms. 
     */

    @OSID @Override
    public void clearRecurringWeeklyTerms() {
        getAssembler().clearTerms(getRecurringWeeklyColumn());
        return;
    }


    /**
     *  Gets the recurring weekly query terms. 
     *
     *  @return the recurring weekly query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRecurringWeeklyTerms() {
        return (getAssembler().getBooleanTerms(getRecurringWeeklyColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by weekly 
     *  recurring. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecurringWeekly(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecurringWeeklyColumn(), style);
        return;
    }


    /**
     *  Gets the RecurringWeekly column name.
     *
     * @return the column name
     */

    protected String getRecurringWeeklyColumn() {
        return ("recurring_weekly");
    }


    /**
     *  Matches activities with a weekly effort between the given durations 
     *  inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyEffort(org.osid.calendaring.Duration min, 
                                  org.osid.calendaring.Duration max, 
                                  boolean match) {
        getAssembler().addDurationRangeTerm(getWeeklyEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity that has any weekly effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any weekly 
     *          effort, <code> false </code> to match activities with no 
     *          weekly effort 
     */

    @OSID @Override
    public void matchAnyWeeklyEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getWeeklyEffortColumn(), match);
        return;
    }


    /**
     *  Clears the weekly effort terms. 
     */

    @OSID @Override
    public void clearWeeklyEffortTerms() {
        getAssembler().clearTerms(getWeeklyEffortColumn());
        return;
    }


    /**
     *  Gets the weekly effort query terms. 
     *
     *  @return the weekly effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getWeeklyEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the weekly 
     *  effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeeklyEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeeklyEffortColumn(), style);
        return;
    }


    /**
     *  Gets the WeeklyEffort column name.
     *
     * @return the column name
     */

    protected String getWeeklyEffortColumn() {
        return ("weekly_effort");
    }


    /**
     *  Matches activities with a weekly contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyContactTime(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getWeeklyContactTimeColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity that has any weekly contact time assigned. 
     *
     *  @param  match <code> true </code> to match activities with any weekly 
     *          contact time, <code> false </code> to match activities with no 
     *          weekly contact time 
     */

    @OSID @Override
    public void matchAnyWeeklyContactTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getWeeklyContactTimeColumn(), match);
        return;
    }


    /**
     *  Clears the weekly contact time terms. 
     */

    @OSID @Override
    public void clearWeeklyContactTimeTerms() {
        getAssembler().clearTerms(getWeeklyContactTimeColumn());
        return;
    }


    /**
     *  Gets the weekly contact time query terms. 
     *
     *  @return the weekly contact time query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyContactTimeTerms() {
        return (getAssembler().getDurationRangeTerms(getWeeklyContactTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the weekly 
     *  contact. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeeklyContactTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeeklyContactTimeColumn(), style);
        return;
    }


    /**
     *  Gets the WeeklyContactTime column name.
     *
     * @return the column name
     */

    protected String getWeeklyContactTimeColumn() {
        return ("weekly_contact_time");
    }


    /**
     *  Matches activities with a weekly individual effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyIndividualEffort(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        getAssembler().addDurationRangeTerm(getWeeklyIndividualEffortColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity that has any weekly individual effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any weekly 
     *          individual effort, <code> false </code> to match activities 
     *          with no weekly individual effort 
     */

    @OSID @Override
    public void matchAnyWeeklyIndividualEffort(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getWeeklyIndividualEffortColumn(), match);
        return;
    }


    /**
     *  Clears the weekly individual effort terms. 
     */

    @OSID @Override
    public void clearWeeklyIndividualEffortTerms() {
        getAssembler().clearTerms(getWeeklyIndividualEffortColumn());
        return;
    }


    /**
     *  Gets the weekly individual effort query terms. 
     *
     *  @return the weekly individual effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyIndividualEffortTerms() {
        return (getAssembler().getDurationRangeTerms(getWeeklyIndividualEffortColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by weekly 
     *  individual effort. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeeklyIndividualEffort(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeeklyIndividualEffortColumn(), style);
        return;
    }


    /**
     *  Gets the WeeklyIndividualEffort column name.
     *
     * @return the column name
     */

    protected String getWeeklyIndividualEffortColumn() {
        return ("weekly_individual_effort");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  activities assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this activity supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return <code>true</code> if the activityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRecordType) {
        for (org.osid.course.records.ActivityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity query record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityQueryRecord getActivityQueryRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityQueryInspectorRecord getActivityQueryInspectorRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param activityRecordType the activity record type
     *  @return the activity search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivitySearchOrderRecord getActivitySearchOrderRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivitySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this activity. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityQueryRecord the activity query record
     *  @param activityQueryInspectorRecord the activity query inspector
     *         record
     *  @param activitySearchOrderRecord the activity search order record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityQueryRecord</code>,
     *          <code>activityQueryInspectorRecord</code>,
     *          <code>activitySearchOrderRecord</code> or
     *          <code>activityRecordTypeactivity</code> is
     *          <code>null</code>
     */
            
    protected void addActivityRecords(org.osid.course.records.ActivityQueryRecord activityQueryRecord, 
                                      org.osid.course.records.ActivityQueryInspectorRecord activityQueryInspectorRecord, 
                                      org.osid.course.records.ActivitySearchOrderRecord activitySearchOrderRecord, 
                                      org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);

        nullarg(activityQueryRecord, "activity query record");
        nullarg(activityQueryInspectorRecord, "activity query inspector record");
        nullarg(activitySearchOrderRecord, "activity search odrer record");

        this.queryRecords.add(activityQueryRecord);
        this.queryInspectorRecords.add(activityQueryInspectorRecord);
        this.searchOrderRecords.add(activitySearchOrderRecord);
        
        return;
    }
}
