//
// AbstractQueryPriceEnablerLookupSession.java
//
//    An inline adapter that maps a PriceEnablerLookupSession to
//    a PriceEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PriceEnablerLookupSession to
 *  a PriceEnablerQuerySession.
 */

public abstract class AbstractQueryPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.ordering.rules.spi.AbstractPriceEnablerLookupSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;    
    private final org.osid.ordering.rules.PriceEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPriceEnablerLookupSession.
     *
     *  @param querySession the underlying price enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPriceEnablerLookupSession(org.osid.ordering.rules.PriceEnablerQuerySession querySession) {
        nullarg(querySession, "price enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Store</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform <code>PriceEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPriceEnablers() {
        return (this.session.canSearchPriceEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price enablers in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    

    /**
     *  Only active price enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePriceEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive price enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPriceEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>PriceEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PriceEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PriceEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerId <code>Id</code> of the
     *          <code>PriceEnabler</code>
     *  @return the price enabler
     *  @throws org.osid.NotFoundException <code>priceEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>priceEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnabler getPriceEnabler(org.osid.id.Id priceEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.rules.PriceEnablerQuery query = getQuery();
        query.matchId(priceEnablerId, true);
        org.osid.ordering.rules.PriceEnablerList priceEnablers = this.session.getPriceEnablersByQuery(query);
        if (priceEnablers.hasNext()) {
            return (priceEnablers.getNextPriceEnabler());
        } 
        
        throw new org.osid.NotFoundException(priceEnablerId + " not found");
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  priceEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>PriceEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByIds(org.osid.id.IdList priceEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.rules.PriceEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = priceEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPriceEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  price enabler genus <code>Type</code> which does not include
     *  price enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.rules.PriceEnablerQuery query = getQuery();
        query.matchGenusType(priceEnablerGenusType, true);
        return (this.session.getPriceEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  price enabler genus <code>Type</code> and include any additional
     *  price enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  price enablers or an error results. Otherwise, the returned list
     *  may contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByParentGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.rules.PriceEnablerQuery query = getQuery();
        query.matchParentGenusType(priceEnablerGenusType, true);
        return (this.session.getPriceEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> containing the given
     *  price enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerRecordType a priceEnabler record type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByRecordType(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.rules.PriceEnablerQuery query = getQuery();
        query.matchRecordType(priceEnablerRecordType, true);
        return (this.session.getPriceEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session.
     *  
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PriceEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.rules.PriceEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getPriceEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>PriceEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @return a list of <code>PriceEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.rules.PriceEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPriceEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.ordering.rules.PriceEnablerQuery getQuery() {
        org.osid.ordering.rules.PriceEnablerQuery query = this.session.getPriceEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
