//
// AbstractSignalEnablerQuery.java
//
//     A template for making a SignalEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.signalenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for signal enablers.
 */

public abstract class AbstractSignalEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.mapping.path.rules.SignalEnablerQuery {

    private final java.util.Collection<org.osid.mapping.path.rules.records.SignalEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to a signal. 
     *
     *  @param  signalId the signal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> signalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSignalId(org.osid.id.Id signalId, boolean match) {
        return;
    }


    /**
     *  Clears the signal <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSignalIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SignalQuery </code> is available. 
     *
     *  @return <code> true </code> if a signal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSignalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a signal. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the signal query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSignalQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuery getRuledSignalQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSignalQuery() is false");
    }


    /**
     *  Matches rules mapped to any signal. 
     *
     *  @param  match <code> true </code> for rules mapped to any signal, 
     *          <code> false </code> to match rules mapped to no signals 
     */

    @OSID @Override
    public void matchAnyRuledSignal(boolean match) {
        return;
    }


    /**
     *  Clears the signal query terms. 
     */

    @OSID @Override
    public void clearRuledSignalTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to an map. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if an map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for an map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given signal enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a signal enabler implementing the requested record.
     *
     *  @param signalEnablerRecordType a signal enabler record type
     *  @return the signal enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SignalEnablerQueryRecord getSignalEnablerQueryRecord(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SignalEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(signalEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this signal enabler query. 
     *
     *  @param signalEnablerQueryRecord signal enabler query record
     *  @param signalEnablerRecordType signalEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSignalEnablerQueryRecord(org.osid.mapping.path.rules.records.SignalEnablerQueryRecord signalEnablerQueryRecord, 
                                          org.osid.type.Type signalEnablerRecordType) {

        addRecordType(signalEnablerRecordType);
        nullarg(signalEnablerQueryRecord, "signal enabler query record");
        this.records.add(signalEnablerQueryRecord);        
        return;
    }
}
