//
// AbstractDeedLookupSession.java
//
//    A starter implementation framework for providing a Deed
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Deed lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getDeeds(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractDeedLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.squatting.DeedLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();
    

    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this session.
     *
     *  @return the <code>Campus</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Deed</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDeeds() {
        return (true);
    }


    /**
     *  A complete view of the <code>Deed</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDeedView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Deed</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDeedView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include deeds in campuses which are children of this
     *  campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only deeds whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveDeedView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All deeds of any effective dates are returned by all methods
     *  in this session.
     */

    @OSID @Override
    public void useAnyEffectiveDeedView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Deed</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Deed</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Deed</code> and retained for
     *  compatibility.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and
     *  those currently expired are returned.
     *
     *  @param  deedId <code>Id</code> of the
     *          <code>Deed</code>
     *  @return the deed
     *  @throws org.osid.NotFoundException <code>deedId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>deedId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Deed getDeed(org.osid.id.Id deedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.squatting.DeedList deeds = getDeeds()) {
            while (deeds.hasNext()) {
                org.osid.room.squatting.Deed deed = deeds.getNextDeed();
                if (deed.getId().equals(deedId)) {
                    return (deed);
                }
            }
        } 

        throw new org.osid.NotFoundException(deedId + " not found");
    }


    /**
     *  Gets a <code>DeedList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the deeds
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Deeds</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getDeeds()</code>.
     *
     *  @param deedIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>deedIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByIds(org.osid.id.IdList deedIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.squatting.Deed> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = deedIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getDeed(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("deed " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.squatting.deed.LinkedDeedList(ret));
    }


    /**
     *  Gets a <code>DeedList</code> corresponding to the given deed
     *  genus <code>Type</code> which does not include deeds of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getDeeds()</code>.
     *
     *  @param deedGenusType a deed genus type
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusType(org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeeds(), deedGenusType));
    }


    /**
     *  Gets a <code>DeedList</code> corresponding to the given deed
     *  genus <code>Type</code> and include any additional deeds with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDeeds()</code>.
     *
     *  @param deedGenusType a deed genus type
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByParentGenusType(org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDeedsByGenusType(deedGenusType));
    }


    /**
     *  Gets a <code>DeedList</code> containing the given deed record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDeeds()</code>.
     *
     *  @param  deedRecordType a deed record type 
     *  @return the returned <code>Deed</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByRecordType(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedRecordFilterList(getDeeds(), deedRecordType));
    }


    /**
     *  Gets a <code>DeedList</code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In active mode, deeds are returned that are currently
     *  active. In any status mode, active and inactive deeds are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsOnDate(org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.TemporalDeedFilterList(getDeeds(), from, to));
    }



    /**
     *  Gets a <code>DeedList</code> by genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In active mode, deeds are returned that are currently
     *  active. In any status mode, active and inactive deeds are
     *  returned.
     *
     *  @param deedGenusType a deed genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>deedGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeOnDate(org.osid.type.Type deedGenusType,
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeedsOnDate(from, to), deedGenusType));
    }
        

    /**
     *  Gets a list of deeds corresponding to a building
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.DeedList getDeedsForBuilding(org.osid.id.Id buildingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedFilterList(new BuildingFilter(buildingId), getDeeds()));
    }


    /**
     *  Gets a list of deeds corresponding to a building
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param buildingId the <code>Id</code> of the building
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingOnDate(org.osid.id.Id buildingId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.TemporalDeedFilterList(getDeedsForBuilding(buildingId), from, to));
    }


    /**
     *  Gets a <code>DeedList</code> containing the given building and
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.NullArgumentException <code>buildingId</code>
     *          or <code>deedGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                           org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeedsForBuilding(buildingId), deedGenusType));
    }        


    /**
     *  Gets a list of all deeds for a building with a genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingId</code>, <code>deedGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                 org.osid.type.Type deedGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeedsForBuildingOnDate(buildingId, from, to), deedGenusType));
    }

    
    /**
     *  Gets a list of deeds corresponding to a owner <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the owner
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.DeedList getDeedsForOwner(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedFilterList(new OwnerFilter(resourceId), getDeeds()));
    }


    /**
     *  Gets a list of deeds corresponding to a owner <code>Id</code>
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the owner
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForOwnerOnDate(org.osid.id.Id resourceId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.TemporalDeedFilterList(getDeedsForOwner(resourceId), from, to));
    }


    /**
     *  Gets a <code>DeedList</code> containing the given owner and
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>deedGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForOwner(org.osid.id.Id resourceId, 
                                                                        org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeedsForOwner(resourceId), deedGenusType));
    }


    /**
     *  Gets a list of all deeds for a resource owner with a genus
     *  type and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>deedGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForOwnerOnDate(org.osid.id.Id resourceId, 
                                                                              org.osid.type.Type deedGenusType, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeedsForOwnerOnDate(resourceId, from, to), deedGenusType));
    }


    /**
     *  Gets a list of deeds corresponding to building and owner
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @param  resourceId the <code>Id</code> of the owner
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingAndOwner(org.osid.id.Id buildingId,
                                                                        org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedFilterList(new OwnerFilter(resourceId), getDeedsForBuilding(buildingId)));
    }


    /**
     *  Gets a list of deeds corresponding to building and owner
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the owner
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DeedList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsForBuildingAndOwnerOnDate(org.osid.id.Id buildingId,
                                                                              org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.TemporalDeedFilterList(getDeedsForBuildingAndOwner(buildingId, resourceId), from, to));
    }


    /**
     *  Gets a <code>DeedList</code> containing the given building,
     *  owner, and genus type.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  deedGenusType a deed genus type 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingId</code>, <code>resourceId</code>, or
     *          <code>deedGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingAndOwner(org.osid.id.Id buildingId, 
                                                                                   org.osid.id.Id resourceId,
                                                                                   org.osid.type.Type deedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeedsForBuildingAndOwner(buildingId, resourceId), deedGenusType));
    }        


    /**
     *  Gets a list of all deeds for a building and owner with a genus
     *  type and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session.
     *  
     *  In effective mode, deeds are returned that are currently
     *  effective. In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code> 
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  deedGenusType a deed genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Deed</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingId</code>, <code>resourceId</code>,
     *          <code>deedGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeedsByGenusTypeForBuildingAndOwnerOnDate(org.osid.id.Id buildingId, 
                                                                                         org.osid.id.Id resourceId, 
                                                                                         org.osid.type.Type deedGenusType, 
                                                                                         org.osid.calendaring.DateTime from, 
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedGenusFilterList(getDeedsForBuildingAndOwnerOnDate(buildingId, resourceId, from, to), deedGenusType));
    }


    /**
     *  Gets all <code>Deeds</code>.
     *
     *  In plenary mode, the returned list contains all known deeds or
     *  an error results. Otherwise, the returned list may contain
     *  only those deeds that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, deeds are returned that are currently
     *  effective.  In any effective mode, effective deeds and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Deeds</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.room.squatting.DeedList getDeeds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the deed list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of deeds
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.room.squatting.DeedList filterDeedsOnViews(org.osid.room.squatting.DeedList list)
        throws org.osid.OperationFailedException {

        org.osid.room.squatting.DeedList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.room.squatting.deed.EffectiveDeedFilterList(ret);
        }

        return (ret);
    }


    public static class BuildingFilter
        implements net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedFilter {
         
        private final org.osid.id.Id buildingId;
         
         
        /**
         *  Constructs a new <code>BuildingFilter</code>.
         *
         *  @param buildingId the building to filter
         *  @throws org.osid.NullArgumentException
         *          <code>buildingId</code> is <code>null</code>
         */
        
        public BuildingFilter(org.osid.id.Id buildingId) {
            nullarg(buildingId, "building Id");
            this.buildingId = buildingId;
            return;
        }

         
        /**
         *  Used by the DeedFilterList to filter the deed list based
         *  on building.
         *
         *  @param deed the deed
         *  @return <code>true</code> to pass the deed,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.squatting.Deed deed) {
            return (deed.getBuildingId().equals(this.buildingId));
        }
    }


    public static class OwnerFilter
        implements net.okapia.osid.jamocha.inline.filter.room.squatting.deed.DeedFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>OwnerFilter</code>.
         *
         *  @param resourceId the owner to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public OwnerFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "owner Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the DeedFilterList to filter the 
         *  deed list based on owner.
         *
         *  @param deed the deed
         *  @return <code>true</code> to pass the deed,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.squatting.Deed deed) {
            return (deed.getOwnerId().equals(this.resourceId));
        }
    }
}
