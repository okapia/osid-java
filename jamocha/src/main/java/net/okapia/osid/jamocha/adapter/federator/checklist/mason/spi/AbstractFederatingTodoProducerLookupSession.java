//
// AbstractFederatingTodoProducerLookupSession.java
//
//     An abstract federating adapter for a TodoProducerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  TodoProducerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingTodoProducerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.checklist.mason.TodoProducerLookupSession>
    implements org.osid.checklist.mason.TodoProducerLookupSession {

    private boolean parallel = false;
    private org.osid.checklist.Checklist checklist = new net.okapia.osid.jamocha.nil.checklist.checklist.UnknownChecklist();


    /**
     *  Constructs a new <code>AbstractFederatingTodoProducerLookupSession</code>.
     */

    protected AbstractFederatingTodoProducerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.checklist.mason.TodoProducerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Checklist/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Checklist Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.checklist.getId());
    }


    /**
     *  Gets the <code>Checklist</code> associated with this 
     *  session.
     *
     *  @return the <code>Checklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.checklist);
    }


    /**
     *  Sets the <code>Checklist</code>.
     *
     *  @param  checklist the checklist for this session
     *  @throws org.osid.NullArgumentException <code>checklist</code>
     *          is <code>null</code>
     */

    protected void setChecklist(org.osid.checklist.Checklist checklist) {
        nullarg(checklist, "checklist");
        this.checklist = checklist;
        return;
    }


    /**
     *  Tests if this user can perform <code>TodoProducer</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTodoProducers() {
        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            if (session.canLookupTodoProducers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>TodoProducer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTodoProducerView() {
        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            session.useComparativeTodoProducerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>TodoProducer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTodoProducerView() {
        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            session.usePlenaryTodoProducerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todo producers in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            session.useFederatedChecklistView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            session.useIsolatedChecklistView();
        }

        return;
    }


    /**
     *  Only active todo producers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTodoProducerView() {
        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            session.useActiveTodoProducerView();
        }

        return;
    }


    /**
     *  Active and inactive todo producers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTodoProducerView() {
        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            session.useAnyStatusTodoProducerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>TodoProducer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>TodoProducer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>TodoProducer</code> and
     *  retained for compatibility.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerId <code>Id</code> of the
     *          <code>TodoProducer</code>
     *  @return the todo producer
     *  @throws org.osid.NotFoundException <code>todoProducerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>todoProducerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducer(org.osid.id.Id todoProducerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            try {
                return (session.getTodoProducer(todoProducerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(todoProducerId + " not found");
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  todoProducers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>TodoProducers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByIds(org.osid.id.IdList todoProducerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.checklist.mason.todoproducer.MutableTodoProducerList ret = new net.okapia.osid.jamocha.checklist.mason.todoproducer.MutableTodoProducerList();

        try (org.osid.id.IdList ids = todoProducerIds) {
            while (ids.hasNext()) {
                ret.addTodoProducer(getTodoProducer(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  todo producer genus <code>Type</code> which does not include
     *  todo producers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.mason.todoproducer.FederatingTodoProducerList ret = getTodoProducerList();

        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            ret.addTodoProducerList(session.getTodoProducersByGenusType(todoProducerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  todo producer genus <code>Type</code> and include any additional
     *  todo producers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByParentGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.mason.todoproducer.FederatingTodoProducerList ret = getTodoProducerList();

        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            ret.addTodoProducerList(session.getTodoProducersByParentGenusType(todoProducerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoProducerList</code> containing the given
     *  todo producer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerRecordType a todoProducer record type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByRecordType(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.mason.todoproducer.FederatingTodoProducerList ret = getTodoProducerList();

        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            ret.addTodoProducerList(session.getTodoProducersByRecordType(todoProducerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoProducer</code> by <code>Todo</code>. In
     *  plenary mode, the returned list contains all known todo
     *  producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible
     *  through this session.
     *
     *  @param  todoId a todo <code>Id</code>
     *  @return the returned <code>TodoProducer</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NotFoundException <code>todoId</code> is not
     *          found or has no producer
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducerByTodo(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            try {
                return (session.getTodoProducerByTodo(todoId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException("no todo producer for " + todoId);
    }


    /**
     *  Gets all <code>TodoProducers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @return a list of <code>TodoProducers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.mason.todoproducer.FederatingTodoProducerList ret = getTodoProducerList();

        for (org.osid.checklist.mason.TodoProducerLookupSession session : getSessions()) {
            ret.addTodoProducerList(session.getTodoProducers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.checklist.mason.todoproducer.FederatingTodoProducerList getTodoProducerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.checklist.mason.todoproducer.ParallelTodoProducerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.checklist.mason.todoproducer.CompositeTodoProducerList());
        }
    }
}
