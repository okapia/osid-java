//
// InvariantMapProxyProjectLookupSession
//
//    Implements a Project lookup service backed by a fixed
//    collection of projects. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.construction;


/**
 *  Implements a Project lookup service backed by a fixed
 *  collection of projects. The projects are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyProjectLookupSession
    extends net.okapia.osid.jamocha.core.room.construction.spi.AbstractMapProjectLookupSession
    implements org.osid.room.construction.ProjectLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProjectLookupSession} with no
     *  projects.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProjectLookupSession(org.osid.room.Campus campus,
                                                  org.osid.proxy.Proxy proxy) {
        setCampus(campus);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyProjectLookupSession} with a single
     *  project.
     *
     *  @param campus the campus
     *  @param project a single project
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code project} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProjectLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.construction.Project project, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putProject(project);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyProjectLookupSession} using
     *  an array of projects.
     *
     *  @param campus the campus
     *  @param projects an array of projects
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code projects} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProjectLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.construction.Project[] projects, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putProjects(projects);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProjectLookupSession} using a
     *  collection of projects.
     *
     *  @param campus the campus
     *  @param projects a collection of projects
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code projects} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProjectLookupSession(org.osid.room.Campus campus,
                                                  java.util.Collection<? extends org.osid.room.construction.Project> projects,
                                                  org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putProjects(projects);
        return;
    }
}
