//
// MutableMapProxyRecurringEventLookupSession
//
//    Implements a RecurringEvent lookup service backed by a collection of
//    recurringEvents that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a RecurringEvent lookup service backed by a collection of
 *  recurringEvents. The recurringEvents are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of recurring events can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyRecurringEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapRecurringEventLookupSession
    implements org.osid.calendaring.RecurringEventLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyRecurringEventLookupSession}
     *  with no recurring events.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyRecurringEventLookupSession} with a
     *  single recurring event.
     *
     *  @param calendar the calendar
     *  @param recurringEvent a recurring event
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code recurringEvent}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.RecurringEvent recurringEvent, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putRecurringEvent(recurringEvent);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRecurringEventLookupSession} using an
     *  array of recurring events.
     *
     *  @param calendar the calendar
     *  @param recurringEvents an array of recurring events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code recurringEvents}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.RecurringEvent[] recurringEvents, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putRecurringEvents(recurringEvents);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRecurringEventLookupSession} using a
     *  collection of recurring events.
     *
     *  @param calendar the calendar
     *  @param recurringEvents a collection of recurring events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code recurringEvents}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                java.util.Collection<? extends org.osid.calendaring.RecurringEvent> recurringEvents,
                                                org.osid.proxy.Proxy proxy) {
   
        this(calendar, proxy);
        setSessionProxy(proxy);
        putRecurringEvents(recurringEvents);
        return;
    }

    
    /**
     *  Makes a {@code RecurringEvent} available in this session.
     *
     *  @param recurringEvent an recurring event
     *  @throws org.osid.NullArgumentException {@code recurringEvent{@code 
     *          is {@code null}
     */

    @Override
    public void putRecurringEvent(org.osid.calendaring.RecurringEvent recurringEvent) {
        super.putRecurringEvent(recurringEvent);
        return;
    }


    /**
     *  Makes an array of recurringEvents available in this session.
     *
     *  @param recurringEvents an array of recurring events
     *  @throws org.osid.NullArgumentException {@code recurringEvents{@code 
     *          is {@code null}
     */

    @Override
    public void putRecurringEvents(org.osid.calendaring.RecurringEvent[] recurringEvents) {
        super.putRecurringEvents(recurringEvents);
        return;
    }


    /**
     *  Makes collection of recurring events available in this session.
     *
     *  @param recurringEvents
     *  @throws org.osid.NullArgumentException {@code recurringEvent{@code 
     *          is {@code null}
     */

    @Override
    public void putRecurringEvents(java.util.Collection<? extends org.osid.calendaring.RecurringEvent> recurringEvents) {
        super.putRecurringEvents(recurringEvents);
        return;
    }


    /**
     *  Removes a RecurringEvent from this session.
     *
     *  @param recurringEventId the {@code Id} of the recurring event
     *  @throws org.osid.NullArgumentException {@code recurringEventId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRecurringEvent(org.osid.id.Id recurringEventId) {
        super.removeRecurringEvent(recurringEventId);
        return;
    }    
}
