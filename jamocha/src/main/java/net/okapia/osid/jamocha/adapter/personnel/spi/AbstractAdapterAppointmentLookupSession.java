//
// AbstractAdapterAppointmentLookupSession.java
//
//    An Appointment lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Appointment lookup session adapter.
 */

public abstract class AbstractAdapterAppointmentLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.personnel.AppointmentLookupSession {

    private final org.osid.personnel.AppointmentLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAppointmentLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAppointmentLookupSession(org.osid.personnel.AppointmentLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Realm/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Realm Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the {@code Realm} associated with this session.
     *
     *  @return the {@code Realm} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform {@code Appointment} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAppointments() {
        return (this.session.canLookupAppointments());
    }


    /**
     *  A complete view of the {@code Appointment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAppointmentView() {
        this.session.useComparativeAppointmentView();
        return;
    }


    /**
     *  A complete view of the {@code Appointment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAppointmentView() {
        this.session.usePlenaryAppointmentView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include appointments in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    

    /**
     *  Only appointments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAppointmentView() {
        this.session.useEffectiveAppointmentView();
        return;
    }
    

    /**
     *  All appointments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAppointmentView() {
        this.session.useAnyEffectiveAppointmentView();
        return;
    }

     
    /**
     *  Gets the {@code Appointment} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Appointment} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Appointment} and
     *  retained for compatibility.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param appointmentId {@code Id} of the {@code Appointment}
     *  @return the appointment
     *  @throws org.osid.NotFoundException {@code appointmentId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code appointmentId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Appointment getAppointment(org.osid.id.Id appointmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointment(appointmentId));
    }


    /**
     *  Gets an {@code AppointmentList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  appointments specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Appointments} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Appointment} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code appointmentIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByIds(org.osid.id.IdList appointmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsByIds(appointmentIds));
    }


    /**
     *  Gets an {@code AppointmentList} corresponding to the given
     *  appointment genus {@code Type} which does not include
     *  appointments of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentGenusType an appointment genus type 
     *  @return the returned {@code Appointment} list
     *  @throws org.osid.NullArgumentException
     *          {@code appointmentGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByGenusType(org.osid.type.Type appointmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsByGenusType(appointmentGenusType));
    }


    /**
     *  Gets an {@code AppointmentList} corresponding to the given
     *  appointment genus {@code Type} and include any additional
     *  appointments with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentGenusType an appointment genus type 
     *  @return the returned {@code Appointment} list
     *  @throws org.osid.NullArgumentException
     *          {@code appointmentGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByParentGenusType(org.osid.type.Type appointmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsByParentGenusType(appointmentGenusType));
    }


    /**
     *  Gets an {@code AppointmentList} containing the given
     *  appointment record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentRecordType an appointment record type 
     *  @return the returned {@code Appointment} list
     *  @throws org.osid.NullArgumentException
     *          {@code appointmentRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByRecordType(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsByRecordType(appointmentRecordType));
    }


    /**
     *  Gets an {@code AppointmentList} effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *  
     *  In active mode, appointments are returned that are currently
     *  active. In any status mode, active and inactive appointments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Appointment} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsOnDate(from, to));
    }
        

    /**
     *  Gets a list of appointments corresponding to a person
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the {@code Id} of the person
     *  @return the returned {@code AppointmentList}
     *  @throws org.osid.NullArgumentException {@code personId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPerson(org.osid.id.Id personId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsForPerson(personId));
    }


    /**
     *  Gets a list of appointments corresponding to a person {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the {@code Id} of the person
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AppointmentList}
     *  @throws org.osid.NullArgumentException {@code personId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonOnDate(org.osid.id.Id personId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsForPersonOnDate(personId, from, to));
    }


    /**
     *  Gets a list of appointments corresponding to a position {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the {@code Id} of the position
     *  @return the returned {@code AppointmentList}
     *  @throws org.osid.NullArgumentException {@code positionId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPosition(org.osid.id.Id positionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsForPosition(positionId));
    }


    /**
     *  Gets a list of appointments corresponding to a position {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the {@code Id} of the position
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AppointmentList}
     *  @throws org.osid.NullArgumentException {@code positionId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPositionOnDate(org.osid.id.Id positionId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsForPositionOnDate(positionId, from, to));
    }


    /**
     *  Gets a list of appointments corresponding to person and
     *  position {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the {@code Id} of the person
     *  @param  positionId the {@code Id} of the position
     *  @return the returned {@code AppointmentList}
     *  @throws org.osid.NullArgumentException {@code personId},
     *          {@code positionId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonAndPosition(org.osid.id.Id personId,
                                                                                  org.osid.id.Id positionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsForPersonAndPosition(personId, positionId));
    }


    /**
     *  Gets a list of appointments corresponding to person and
     *  position {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective. In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the {@code Id} of the position
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AppointmentList}
     *  @throws org.osid.NullArgumentException {@code personId},
     *          {@code positionId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonAndPositionOnDate(org.osid.id.Id personId,
                                                                                        org.osid.id.Id positionId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointmentsForPersonAndPositionOnDate(personId, positionId, from, to));
    }


    /**
     *  Gets all {@code Appointments}. 
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @return a list of {@code Appointments} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAppointments());
    }
}
