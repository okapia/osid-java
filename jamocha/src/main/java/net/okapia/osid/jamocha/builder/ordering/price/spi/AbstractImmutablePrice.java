//
// AbstractImmutablePrice.java
//
//     Wraps a mutable Price to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.price.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Price</code> to hide modifiers. This
 *  wrapper provides an immutized Price from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying price whose state changes are visible.
 */

public abstract class AbstractImmutablePrice
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.ordering.Price {

    private final org.osid.ordering.Price price;


    /**
     *  Constructs a new <code>AbstractImmutablePrice</code>.
     *
     *  @param price the price to immutablize
     *  @throws org.osid.NullArgumentException <code>price</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePrice(org.osid.ordering.Price price) {
        super(price);
        this.price = price;
        return;
    }


    /**
     *  Gets the price schedule <code> Id </code> to which this price belongs. 
     *
     *  @return the price schedule <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPriceScheduleId() {
        return (this.price.getPriceScheduleId());
    }


    /**
     *  Gets the price schedule to which this price belongs. 
     *
     *  @return the price schedule 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceSchedule()
        throws org.osid.OperationFailedException {

        return (this.price.getPriceSchedule());
    }


    /**
     *  Tests if this price is restricted by quantity. 
     *
     *  @return <code> true </code> if this price has a quantity range, <code> 
     *          false </code> if this price is the same for any quantity 
     */

    @OSID @Override
    public boolean hasQuantityRange() {
        return (this.price.hasQuantityRange());
    }


    /**
     *  Gets the minium quantity for this price. 
     *
     *  @return the quantity 
     *  @throws org.osid.IllegalStateException <code> hasQuantityRange() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMinimumQuantity() {
        return (this.price.getMinimumQuantity());
    }


    /**
     *  Gets the maximum quantity for this price. 
     *
     *  @return the quantity 
     *  @throws org.osid.IllegalStateException <code> hasQuantityRange() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaximumQuantity() {
        return (this.price.getMaximumQuantity());
    }


    /**
     *  Tests if this price is restricted by demographic. 
     *
     *  @return <code> true </code> if this price has a demographic, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasDemographic() {
        return (this.price.hasDemographic());
    }


    /**
     *  Gets the resource <code> Id </code> representing the demographic. 
     *
     *  @return the resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasDemographic() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDemographicId() {
        return (this.price.getDemographicId());
    }


    /**
     *  Gets the resource representing the demographic. 
     *
     *  @return the resource 
     *  @throws org.osid.IllegalStateException <code> hasDemographic() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getDemographic()
        throws org.osid.OperationFailedException {

        return (this.price.getDemographic());
    }


    /**
     *  Gets the amount. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.price.getAmount());
    }


    /**
     *  Tests if this price has a recurring charge. 
     *
     *  @return <code> true </code> if this price is recurring, <code> false 
     *          </code> if one-time 
     */

    @OSID @Override
    public boolean isRecurring() {
        return (this.price.isRecurring());
    }


    /**
     *  Gets the recurring interval. 
     *
     *  @return the interval 
     *  @throws org.osid.IllegalStateException <code> isRecurring() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRecurringInterval() {
        return (this.price.getRecurringInterval());
    }


    /**
     *  Gets the price record corresponding to the given <code> Product 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> priceRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(priceRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  priceRecordType the type of price record to retrieve 
     *  @return the price record 
     *  @throws org.osid.NullArgumentException <code> priceRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(priceRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.records.PriceRecord getPriceRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        return (this.price.getPriceRecord(priceRecordType));
    }
}

