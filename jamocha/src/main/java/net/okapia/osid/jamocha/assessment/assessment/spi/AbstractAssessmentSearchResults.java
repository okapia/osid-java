//
// AbstractAssessmentSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAssessmentSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.assessment.AssessmentSearchResults {

    private org.osid.assessment.AssessmentList assessments;
    private final org.osid.assessment.AssessmentQueryInspector inspector;
    private final java.util.Collection<org.osid.assessment.records.AssessmentSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAssessmentSearchResults.
     *
     *  @param assessments the result set
     *  @param assessmentQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>assessments</code>
     *          or <code>assessmentQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAssessmentSearchResults(org.osid.assessment.AssessmentList assessments,
                                            org.osid.assessment.AssessmentQueryInspector assessmentQueryInspector) {
        nullarg(assessments, "assessments");
        nullarg(assessmentQueryInspector, "assessment query inspectpr");

        this.assessments = assessments;
        this.inspector = assessmentQueryInspector;

        return;
    }


    /**
     *  Gets the assessment list resulting from a search.
     *
     *  @return an assessment list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments() {
        if (this.assessments == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.assessment.AssessmentList assessments = this.assessments;
        this.assessments = null;
	return (assessments);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.assessment.AssessmentQueryInspector getAssessmentQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  assessment search record <code> Type. </code> This method must
     *  be used to retrieve an assessment implementing the requested
     *  record.
     *
     *  @param assessmentSearchRecordType an assessment search 
     *         record type 
     *  @return the assessment search
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(assessmentSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentSearchResultsRecord getAssessmentSearchResultsRecord(org.osid.type.Type assessmentSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.assessment.records.AssessmentSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(assessmentSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(assessmentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record assessment search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAssessmentRecord(org.osid.assessment.records.AssessmentSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "assessment record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
