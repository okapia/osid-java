//
// AbstractAssemblyPublisherQuery.java
//
//     A PublisherQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.subscription.publisher.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PublisherQuery that stores terms.
 */

public abstract class AbstractAssemblyPublisherQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.subscription.PublisherQuery,
               org.osid.subscription.PublisherQueryInspector,
               org.osid.subscription.PublisherSearchOrder {

    private final java.util.Collection<org.osid.subscription.records.PublisherQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.records.PublisherQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.records.PublisherSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPublisherQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPublisherQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the subscription <code> Id </code> for this query to match 
     *  subscriptions assigned to publishers. 
     *
     *  @param  subscriptionId a subscription <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subscriptionId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchSubscriptionId(org.osid.id.Id subscriptionId, 
                                    boolean match) {
        getAssembler().addIdTerm(getSubscriptionIdColumn(), subscriptionId, match);
        return;
    }


    /**
     *  Clears the subscription <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubscriptionIdTerms() {
        getAssembler().clearTerms(getSubscriptionIdColumn());
        return;
    }


    /**
     *  Gets the subscription <code> Id </code> terms. 
     *
     *  @return the subscription <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubscriptionIdTerms() {
        return (getAssembler().getIdTerms(getSubscriptionIdColumn()));
    }


    /**
     *  Gets the SubscriptionId column name.
     *
     * @return the column name
     */

    protected String getSubscriptionIdColumn() {
        return ("subscription_id");
    }


    /**
     *  Tests if a subscription query is available. 
     *
     *  @return <code> true </code> if a subscription query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. 
     *
     *  @return the subscription query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuery getSubscriptionQuery() {
        throw new org.osid.UnimplementedException("supportsSubscriptionQuery() is false");
    }


    /**
     *  Matches publishers with any subscription. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          subscription, <code> false </code> to match publishers with no 
     *          subscriptions 
     */

    @OSID @Override
    public void matchAnySubscription(boolean match) {
        getAssembler().addIdWildcardTerm(getSubscriptionColumn(), match);
        return;
    }


    /**
     *  Clears the subscription terms. 
     */

    @OSID @Override
    public void clearSubscriptionTerms() {
        getAssembler().clearTerms(getSubscriptionColumn());
        return;
    }


    /**
     *  Gets the subscription terms. 
     *
     *  @return the subscription terms 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQueryInspector[] getSubscriptionTerms() {
        return (new org.osid.subscription.SubscriptionQueryInspector[0]);
    }


    /**
     *  Gets the Subscription column name.
     *
     * @return the column name
     */

    protected String getSubscriptionColumn() {
        return ("subscription");
    }


    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to dispatches. 
     *
     *  @param  dispatchId a dispatch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dispatchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDispatchId(org.osid.id.Id dispatchId, boolean match) {
        getAssembler().addIdTerm(getDispatchIdColumn(), dispatchId, match);
        return;
    }


    /**
     *  Clears the dispatch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDispatchIdTerms() {
        getAssembler().clearTerms(getDispatchIdColumn());
        return;
    }


    /**
     *  Gets the dispatch <code> Id </code> terms. 
     *
     *  @return the dispatch <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDispatchIdTerms() {
        return (getAssembler().getIdTerms(getDispatchIdColumn()));
    }


    /**
     *  Gets the DispatchId column name.
     *
     * @return the column name
     */

    protected String getDispatchIdColumn() {
        return ("dispatch_id");
    }


    /**
     *  Tests if a <code> DispatchQuery </code> is available. 
     *
     *  @return <code> true </code> if a dispatch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dispatch query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the dispatch query 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuery getDispatchQuery() {
        throw new org.osid.UnimplementedException("supportsDispatchQuery() is false");
    }


    /**
     *  Matches publishers with any dispatch. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          dispatch, <code> false </code> to match publishers with no 
     *          dispatches 
     */

    @OSID @Override
    public void matchAnyDispatch(boolean match) {
        getAssembler().addIdWildcardTerm(getDispatchColumn(), match);
        return;
    }


    /**
     *  Clears the dispatch terms. 
     */

    @OSID @Override
    public void clearDispatchTerms() {
        getAssembler().clearTerms(getDispatchColumn());
        return;
    }


    /**
     *  Gets the dispatch terms. 
     *
     *  @return the dispatch terms 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQueryInspector[] getDispatchTerms() {
        return (new org.osid.subscription.DispatchQueryInspector[0]);
    }


    /**
     *  Gets the Dispatch column name.
     *
     * @return the column name
     */

    protected String getDispatchColumn() {
        return ("dispatch");
    }


    /**
     *  Sets the publisher <code> Id </code> for this query to match 
     *  publishers that have the specified publisher as an ancestor. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorPublisherId(org.osid.id.Id publisherId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorPublisherIdColumn(), publisherId, match);
        return;
    }


    /**
     *  Clears the ancestor publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorPublisherIdTerms() {
        getAssembler().clearTerms(getAncestorPublisherIdColumn());
        return;
    }


    /**
     *  Gets the ancestor publisher <code> Id </code> terms. 
     *
     *  @return the ancestor publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorPublisherIdTerms() {
        return (getAssembler().getIdTerms(getAncestorPublisherIdColumn()));
    }


    /**
     *  Gets the AncestorPublisherId column name.
     *
     * @return the column name
     */

    protected String getAncestorPublisherIdColumn() {
        return ("ancestor_publisher_id");
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorPublisherQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getAncestorPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorPublisherQuery() is false");
    }


    /**
     *  Matches publishers with any ancestor. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          ancestor, <code> false </code> to match root publishers 
     */

    @OSID @Override
    public void matchAnyAncestorPublisher(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorPublisherColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor publisher terms. 
     */

    @OSID @Override
    public void clearAncestorPublisherTerms() {
        getAssembler().clearTerms(getAncestorPublisherColumn());
        return;
    }


    /**
     *  Gets the ancestor publisher terms. 
     *
     *  @return the ancestor publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getAncestorPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }


    /**
     *  Gets the AncestorPublisher column name.
     *
     * @return the column name
     */

    protected String getAncestorPublisherColumn() {
        return ("ancestor_publisher");
    }


    /**
     *  Sets the publisher <code> Id </code> for this query to match 
     *  publishers that have the specified publisher as a descendant. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantPublisherId(org.osid.id.Id publisherId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantPublisherIdColumn(), publisherId, match);
        return;
    }


    /**
     *  Clears the descendant publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantPublisherIdTerms() {
        getAssembler().clearTerms(getDescendantPublisherIdColumn());
        return;
    }


    /**
     *  Gets the descendant publisher <code> Id </code> terms. 
     *
     *  @return the descendant publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantPublisherIdTerms() {
        return (getAssembler().getIdTerms(getDescendantPublisherIdColumn()));
    }


    /**
     *  Gets the DescendantPublisherId column name.
     *
     * @return the column name
     */

    protected String getDescendantPublisherIdColumn() {
        return ("descendant_publisher_id");
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantPublisherQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getDescendantPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantPublisherQuery() is false");
    }


    /**
     *  Matches publishers with any descendant. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          descendant, <code> false </code> to match leaf publishers 
     */

    @OSID @Override
    public void matchAnyDescendantPublisher(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantPublisherColumn(), match);
        return;
    }


    /**
     *  Clears the descendant publisher terms. 
     */

    @OSID @Override
    public void clearDescendantPublisherTerms() {
        getAssembler().clearTerms(getDescendantPublisherColumn());
        return;
    }


    /**
     *  Gets the descendant publisher terms. 
     *
     *  @return the descendant publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getDescendantPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }


    /**
     *  Gets the DescendantPublisher column name.
     *
     * @return the column name
     */

    protected String getDescendantPublisherColumn() {
        return ("descendant_publisher");
    }


    /**
     *  Tests if this publisher supports the given record
     *  <code>Type</code>.
     *
     *  @param  publisherRecordType a publisher record type 
     *  @return <code>true</code> if the publisherRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type publisherRecordType) {
        for (org.osid.subscription.records.PublisherQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(publisherRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  publisherRecordType the publisher record type 
     *  @return the publisher query record 
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(publisherRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.PublisherQueryRecord getPublisherQueryRecord(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.PublisherQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(publisherRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(publisherRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  publisherRecordType the publisher record type 
     *  @return the publisher query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(publisherRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.PublisherQueryInspectorRecord getPublisherQueryInspectorRecord(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.PublisherQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(publisherRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(publisherRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param publisherRecordType the publisher record type
     *  @return the publisher search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(publisherRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.PublisherSearchOrderRecord getPublisherSearchOrderRecord(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.PublisherSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(publisherRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(publisherRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this publisher. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param publisherQueryRecord the publisher query record
     *  @param publisherQueryInspectorRecord the publisher query inspector
     *         record
     *  @param publisherSearchOrderRecord the publisher search order record
     *  @param publisherRecordType publisher record type
     *  @throws org.osid.NullArgumentException
     *          <code>publisherQueryRecord</code>,
     *          <code>publisherQueryInspectorRecord</code>,
     *          <code>publisherSearchOrderRecord</code> or
     *          <code>publisherRecordTypepublisher</code> is
     *          <code>null</code>
     */
            
    protected void addPublisherRecords(org.osid.subscription.records.PublisherQueryRecord publisherQueryRecord, 
                                      org.osid.subscription.records.PublisherQueryInspectorRecord publisherQueryInspectorRecord, 
                                      org.osid.subscription.records.PublisherSearchOrderRecord publisherSearchOrderRecord, 
                                      org.osid.type.Type publisherRecordType) {

        addRecordType(publisherRecordType);

        nullarg(publisherQueryRecord, "publisher query record");
        nullarg(publisherQueryInspectorRecord, "publisher query inspector record");
        nullarg(publisherSearchOrderRecord, "publisher search odrer record");

        this.queryRecords.add(publisherQueryRecord);
        this.queryInspectorRecords.add(publisherQueryInspectorRecord);
        this.searchOrderRecords.add(publisherSearchOrderRecord);
        
        return;
    }
}
