//
// AbstractAssemblyProgramEntryQuery.java
//
//     A ProgramEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProgramEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyProgramEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.chronicle.ProgramEntryQuery,
               org.osid.course.chronicle.ProgramEntryQueryInspector,
               org.osid.course.chronicle.ProgramEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProgramEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProgramEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Sets the program <code> Id </code> for this query to match entries 
     *  that have an entry for the given course. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        getAssembler().addIdTerm(getProgramIdColumn(), programId, match);
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        getAssembler().clearTerms(getProgramIdColumn());
        return;
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (getAssembler().getIdTerms(getProgramIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the program. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProgramColumn(), style);
        return;
    }


    /**
     *  Gets the ProgramId column name.
     *
     * @return the column name
     */

    protected String getProgramIdColumn() {
        return ("program_id");
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        getAssembler().clearTerms(getProgramColumn());
        return;
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Tests if a program order is available. 
     *
     *  @return <code> true </code> if a program order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }


    /**
     *  Gets the Program column name.
     *
     * @return the column name
     */

    protected String getProgramColumn() {
        return ("program");
    }


    /**
     *  Matches admission dates between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAdmissionDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getAdmissionDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any admission date. 
     *
     *  @param  match <code> true </code> to match entries with any admission 
     *          date, <code> false </code> to match entries with no admission 
     *          date 
     */

    @OSID @Override
    public void matchAnyAdmissionDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getAdmissionDateColumn(), match);
        return;
    }


    /**
     *  Clears the admission date terms. 
     */

    @OSID @Override
    public void clearAdmissionDateTerms() {
        getAssembler().clearTerms(getAdmissionDateColumn());
        return;
    }


    /**
     *  Gets the admission date query terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getAdmissionDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getAdmissionDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the admission 
     *  date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAdmissionDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAdmissionDateColumn(), style);
        return;
    }


    /**
     *  Gets the AdmissionDate column name.
     *
     * @return the column name
     */

    protected String getAdmissionDateColumn() {
        return ("admission_date");
    }


    /**
     *  Matches completed programs. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        getAssembler().addBooleanTerm(getCompleteColumn(), match);
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        getAssembler().clearTerms(getCompleteColumn());
        return;
    }


    /**
     *  Gets the compledt program query terms. 
     *
     *  @return the completed course terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (getAssembler().getBooleanTerms(getCompleteColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by completed 
     *  programs. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompleteColumn(), style);
        return;
    }


    /**
     *  Gets the Complete column name.
     *
     * @return the column name
     */

    protected String getCompleteColumn() {
        return ("complete");
    }


    /**
     *  Sets the term <code> Id </code> for this query. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        getAssembler().clearTerms(getTermIdColumn());
        return;
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (getAssembler().getIdTerms(getTermIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the term. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerm(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTermColumn(), style);
        return;
    }


    /**
     *  Gets the TermId column name.
     *
     * @return the column name
     */

    protected String getTermIdColumn() {
        return ("term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Matches entries that have any term. 
     *
     *  @param  match <code> true </code> to match entries specific to a term, 
     *          <code> false </code> to match entries for the entire 
     *          enrollment 
     */

    @OSID @Override
    public void matchAnyTerm(boolean match) {
        getAssembler().addIdWildcardTerm(getTermColumn(), match);
        return;
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        getAssembler().clearTerms(getTermColumn());
        return;
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Tests if a term order is available. 
     *
     *  @return <code> true </code> if a term order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearchOrder() {
        return (false);
    }


    /**
     *  Gets the term order. 
     *
     *  @return the term search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchOrder getTermSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTermSearchOrder() is false");
    }


    /**
     *  Gets the Term column name.
     *
     * @return the column name
     */

    protected String getTermColumn() {
        return ("term");
    }


    /**
     *  Matches a credit scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getCreditScaleIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditScaleIdTerms() {
        getAssembler().clearTerms(getCreditScaleIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditScaleIdTerms() {
        return (getAssembler().getIdTerms(getCreditScaleIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for credits. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditScale(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditScaleColumn(), style);
        return;
    }


    /**
     *  Gets the CreditScaleId column name.
     *
     * @return the column name
     */

    protected String getCreditScaleIdColumn() {
        return ("credit_scale_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditScaleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getCreditScaleQuery() {
        throw new org.osid.UnimplementedException("supportsCreditScaleQuery() is false");
    }


    /**
     *  Matches entries that have any credit scale. 
     *
     *  @param  match <code> true </code> to match entries with any credit 
     *          scale, <code> false </code> to match entries with no credit 
     *          scale 
     */

    @OSID @Override
    public void matchAnyCreditScale(boolean match) {
        getAssembler().addIdWildcardTerm(getCreditScaleColumn(), match);
        return;
    }


    /**
     *  Clears the credit scale terms. 
     */

    @OSID @Override
    public void clearCreditScaleTerms() {
        getAssembler().clearTerms(getCreditScaleColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getCreditScaleTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system order. 
     *
     *  @return the credit scale search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditScaleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getCreditScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreditScaleSearchOrder() is false");
    }


    /**
     *  Gets the CreditScale column name.
     *
     * @return the column name
     */

    protected String getCreditScaleColumn() {
        return ("credit_scale");
    }


    /**
     *  Matches earned credits between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditsEarned(java.math.BigDecimal from, 
                                   java.math.BigDecimal to, boolean match) {
        getAssembler().addDecimalRangeTerm(getCreditsEarnedColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any earned credits. 
     *
     *  @param  match <code> true </code> to match entries with any earned 
     *          credits, <code> false </code> to match entries with no earned 
     *          credits 
     */

    @OSID @Override
    public void matchAnyCreditsEarned(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getCreditsEarnedColumn(), match);
        return;
    }


    /**
     *  Clears the earned credits terms. 
     */

    @OSID @Override
    public void clearCreditsEarnedTerms() {
        getAssembler().clearTerms(getCreditsEarnedColumn());
        return;
    }


    /**
     *  Gets the earned credits query terms. 
     *
     *  @return the earned credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCreditsEarnedTerms() {
        return (getAssembler().getDecimalRangeTerms(getCreditsEarnedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the earned 
     *  credits. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditsEarned(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditsEarnedColumn(), style);
        return;
    }


    /**
     *  Gets the CreditsEarned column name.
     *
     * @return the column name
     */

    protected String getCreditsEarnedColumn() {
        return ("credits_earned");
    }


    /**
     *  Matches a GPA scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGPAScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getGPAScaleIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGPAScaleIdTerms() {
        getAssembler().clearTerms(getGPAScaleIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGPAScaleIdTerms() {
        return (getAssembler().getIdTerms(getGPAScaleIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for GPAs. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGPAScale(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGPAScaleColumn(), style);
        return;
    }


    /**
     *  Gets the GPAScaleId column name.
     *
     * @return the column name
     */

    protected String getGPAScaleIdColumn() {
        return ("g_pascale_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGPAScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> supportsGPAScaleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGPAScaleQuery() {
        throw new org.osid.UnimplementedException("supportsGPAScaleQuery() is false");
    }


    /**
     *  Matches entries that have any GPA scale. 
     *
     *  @param  match <code> true </code> to match entries with any GPA scale, 
     *          <code> false </code> to match entries with no GPA scale 
     */

    @OSID @Override
    public void matchAnyGPAScale(boolean match) {
        getAssembler().addIdWildcardTerm(getGPAScaleColumn(), match);
        return;
    }


    /**
     *  Clears the credit scale terms. 
     */

    @OSID @Override
    public void clearGPAScaleTerms() {
        getAssembler().clearTerms(getGPAScaleColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGPAScaleTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGPAScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system order. 
     *
     *  @return the GPA scale search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGPAScaleSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGPAScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGPAScaleSearchOrder() is false");
    }


    /**
     *  Gets the GPAScale column name.
     *
     * @return the column name
     */

    protected String getGPAScaleColumn() {
        return ("g_pascale");
    }


    /**
     *  Matches GPA between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGPA(java.math.BigDecimal from, java.math.BigDecimal to, 
                         boolean match) {
        getAssembler().addDecimalRangeTerm(getGPAColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any GPA. 
     *
     *  @param  match <code> true </code> to match entries with any GPA, 
     *          <code> false </code> to match entries with no GPA 
     */

    @OSID @Override
    public void matchAnyGPA(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getGPAColumn(), match);
        return;
    }


    /**
     *  Clears the GPA terms. 
     */

    @OSID @Override
    public void clearGPATerms() {
        getAssembler().clearTerms(getGPAColumn());
        return;
    }


    /**
     *  Gets the GPA query terms. 
     *
     *  @return the GPA query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getGPATerms() {
        return (getAssembler().getDecimalRangeTerms(getGPAColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the gpa. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGPA(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGPAColumn(), style);
        return;
    }


    /**
     *  Gets the GPA column name.
     *
     * @return the column name
     */

    protected String getGPAColumn() {
        return ("g_pa");
    }


    /**
     *  Sets the enrollment <code> Id </code> for this query. 
     *
     *  @param  enrollmentId an enrollment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> enrollmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEnrollmentId(org.osid.id.Id enrollmentId, boolean match) {
        getAssembler().addIdTerm(getEnrollmentIdColumn(), enrollmentId, match);
        return;
    }


    /**
     *  Clears the enrollment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEnrollmentIdTerms() {
        getAssembler().clearTerms(getEnrollmentIdColumn());
        return;
    }


    /**
     *  Gets the enrollment <code> Id </code> query terms. 
     *
     *  @return the enrollment <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEnrollmentIdTerms() {
        return (getAssembler().getIdTerms(getEnrollmentIdColumn()));
    }


    /**
     *  Gets the EnrollmentId column name.
     *
     * @return the column name
     */

    protected String getEnrollmentIdColumn() {
        return ("enrollment_id");
    }


    /**
     *  Tests if an <code> EnrollmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an enrollment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an enrollment entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an enrollment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuery getEnrollmentQuery() {
        throw new org.osid.UnimplementedException("supportsEnrollmentQuery() is false");
    }


    /**
     *  Matches entries that have any enrollment. 
     *
     *  @param  match <code> true </code> to match enries with any enrollment, 
     *          <code> false </code> to match enries with no enrollments 
     */

    @OSID @Override
    public void matchAnyEnrollment(boolean match) {
        getAssembler().addIdWildcardTerm(getEnrollmentColumn(), match);
        return;
    }


    /**
     *  Clears the enrollment terms. 
     */

    @OSID @Override
    public void clearEnrollmentTerms() {
        getAssembler().clearTerms(getEnrollmentColumn());
        return;
    }


    /**
     *  Gets the enrollment query terms. 
     *
     *  @return the enrollment query terms 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQueryInspector[] getEnrollmentTerms() {
        return (new org.osid.course.program.EnrollmentQueryInspector[0]);
    }


    /**
     *  Gets the Enrollment column name.
     *
     * @return the column name
     */

    protected String getEnrollmentColumn() {
        return ("enrollment");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this programEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  programEntryRecordType a program entry record type 
     *  @return <code>true</code> if the programEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programEntryRecordType) {
        for (org.osid.course.chronicle.records.ProgramEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  programEntryRecordType the program entry record type 
     *  @return the program entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntryQueryRecord getProgramEntryQueryRecord(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.ProgramEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  programEntryRecordType the program entry record type 
     *  @return the program entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntryQueryInspectorRecord getProgramEntryQueryInspectorRecord(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.ProgramEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param programEntryRecordType the program entry record type
     *  @return the program entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord getProgramEntrySearchOrderRecord(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this program entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programEntryQueryRecord the program entry query record
     *  @param programEntryQueryInspectorRecord the program entry query inspector
     *         record
     *  @param programEntrySearchOrderRecord the program entry search order record
     *  @param programEntryRecordType program entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryQueryRecord</code>,
     *          <code>programEntryQueryInspectorRecord</code>,
     *          <code>programEntrySearchOrderRecord</code> or
     *          <code>programEntryRecordTypeprogramEntry</code> is
     *          <code>null</code>
     */
            
    protected void addProgramEntryRecords(org.osid.course.chronicle.records.ProgramEntryQueryRecord programEntryQueryRecord, 
                                      org.osid.course.chronicle.records.ProgramEntryQueryInspectorRecord programEntryQueryInspectorRecord, 
                                      org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord programEntrySearchOrderRecord, 
                                      org.osid.type.Type programEntryRecordType) {

        addRecordType(programEntryRecordType);

        nullarg(programEntryQueryRecord, "program entry query record");
        nullarg(programEntryQueryInspectorRecord, "program entry query inspector record");
        nullarg(programEntrySearchOrderRecord, "program entry search odrer record");

        this.queryRecords.add(programEntryQueryRecord);
        this.queryInspectorRecords.add(programEntryQueryInspectorRecord);
        this.searchOrderRecords.add(programEntrySearchOrderRecord);
        
        return;
    }
}
