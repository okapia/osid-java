//
// AbstractItemLookupSession.java
//
//    A starter implementation framework for providing an Item
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Item
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getItems(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractItemLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.billing.ItemLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }

    /**
     *  Tests if this user can perform <code>Item</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupItems() {
        return (true);
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeItemView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryItemView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in businesses which are
     *  children of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Item</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Item</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Item</code> and
     *  retained for compatibility.
     *
     *  @param  itemId <code>Id</code> of the
     *          <code>Item</code>
     *  @return the item
     *  @throws org.osid.NotFoundException <code>itemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>itemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.billing.ItemList items = getItems()) {
            while (items.hasNext()) {
                org.osid.billing.Item item = items.getNextItem();
                if (item.getId().equals(itemId)) {
                    return (item);
                }
            }
        } 

        throw new org.osid.NotFoundException(itemId + " not found");
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Items</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getItems()</code>.
     *
     *  @param  itemIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>itemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Item> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = itemIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getItem(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("item " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.billing.item.LinkedItemList(ret));
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> which does not include
     *  items of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getItems()</code>.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Item> ret = new java.util.ArrayList<>();

        try (org.osid.billing.ItemList items = getItems()) {
            while (items.hasNext()) {
                org.osid.billing.Item item = items.getNextItem();
                if (item.isOfGenusType(itemGenusType)) {
                    ret.add(item);
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.billing.item.LinkedItemList(ret));
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> and include any additional
     *  items with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getItems()</code>.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getItemsByGenusType(itemGenusType));
    }


    /**
     *  Gets a <code>ItemList</code> containing the given
     *  item record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getItems()</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Item> ret = new java.util.ArrayList<>();

        try (org.osid.billing.ItemList items = getItems()) {
            while (items.hasNext()) {
                org.osid.billing.Item item = items.getNextItem();
                if (item.hasRecordType(itemRecordType)) {
                    ret.add(item);
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.billing.item.LinkedItemList(ret));
    }


    /**
     *  Gets an <code> ItemList </code> of the given category. In
     *  plenary mode, the returned list contains all known items or an
     *  error results.  Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  categoryId an item category <code> Id </code>
     *  @return the returned <code> Item </code> list
     *  @throws org.osid.NullArgumentException <code> categoryId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.billing.ItemList getItemsByCategory(org.osid.id.Id categoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Item> ret = new java.util.ArrayList<>();

        try (org.osid.billing.ItemList items = getItems()) {
                while (items.hasNext()) {
                    org.osid.billing.Item item = items.getNextItem();
                    if (item.getCategoryId().equals(categoryId)) {
                        ret.add(item);
                    }
                }
            }

        return (new net.okapia.osid.jamocha.billing.item.LinkedItemList(ret));
    }


    /**
     *  Gets an <code> ItemList </code> of the given general ledger
     *  account.  In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through this
     *  session.
     *
     *  @param  accountId a category <code> Id </code>
     *  @return the returned <code> Item </code> list
     *  @throws org.osid.NullArgumentException <code> accountId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByAccount(org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Item> ret = new java.util.ArrayList<>();
        
        try (org.osid.billing.ItemList items = getItems()) {
                while (items.hasNext()) {
                    org.osid.billing.Item item = items.getNextItem();
                    if (item.hasAccount()) {
                        if (item.getAccountId().equals(accountId)) {
                            ret.add(item);
                        }
                    }
                }
            }
        
        return (new net.okapia.osid.jamocha.billing.item.LinkedItemList(ret));
    }


    /**
     *  Gets an <code> ItemList </code> of the given product. In
     *  plenary mode, the returned list contains all known items or an
     *  error results.  Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  productId a product <code> Id </code>
     *  @return the returned <code> Item </code> list
     *  @throws org.osid.NullArgumentException <code> productId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByProduct(org.osid.id.Id productId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Item> ret = new java.util.ArrayList<>();

        try (org.osid.billing.ItemList items = getItems()) {
                while (items.hasNext()) {
                    org.osid.billing.Item item = items.getNextItem();
                    if (item.hasProduct()) {
                        if (item.getProductId().equals(productId)) {
                            ret.add(item);
                        }
                    }
                }
            }

        return (new net.okapia.osid.jamocha.billing.item.LinkedItemList(ret));
    }


    /**
     *  Gets all <code>Items</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Items</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.billing.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the item list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of items
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.billing.ItemList filterItemsOnViews(org.osid.billing.ItemList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
