//
// AbstractNodeBusinessHierarchySession.java
//
//     Defines a Business hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a business hierarchy session for delivering a hierarchy
 *  of businesses using the BusinessNode interface.
 */

public abstract class AbstractNodeBusinessHierarchySession
    extends net.okapia.osid.jamocha.financials.spi.AbstractBusinessHierarchySession
    implements org.osid.financials.BusinessHierarchySession {

    private java.util.Collection<org.osid.financials.BusinessNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root business <code> Ids </code> in this hierarchy.
     *
     *  @return the root business <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootBusinessIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.financials.businessnode.BusinessNodeToIdList(this.roots));
    }


    /**
     *  Gets the root businesses in the business hierarchy. A node
     *  with no parents is an orphan. While all business <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root businesses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.BusinessList getRootBusinesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.businessnode.BusinessNodeToBusinessList(new net.okapia.osid.jamocha.financials.businessnode.ArrayBusinessNodeList(this.roots)));
    }


    /**
     *  Adds a root business node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootBusiness(org.osid.financials.BusinessNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root business nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootBusinesses(java.util.Collection<org.osid.financials.BusinessNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root business node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootBusiness(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.financials.BusinessNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Business </code> has any parents. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @return <code> true </code> if the business has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentBusinesses(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getBusinessNode(businessId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  business.
     *
     *  @param  id an <code> Id </code> 
     *  @param  businessId the <code> Id </code> of a business 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> businessId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> businessId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfBusiness(org.osid.id.Id id, org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.BusinessNodeList parents = getBusinessNode(businessId).getParentBusinessNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextBusinessNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given business. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @return the parent <code> Ids </code> of the business 
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentBusinessIds(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.business.BusinessToIdList(getParentBusinesses(businessId)));
    }


    /**
     *  Gets the parents of the given business. 
     *
     *  @param  businessId the <code> Id </code> to query 
     *  @return the parents of the business 
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.BusinessList getParentBusinesses(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.businessnode.BusinessNodeToBusinessList(getBusinessNode(businessId).getParentBusinessNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  business.
     *
     *  @param  id an <code> Id </code> 
     *  @param  businessId the Id of a business 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> businessId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfBusiness(org.osid.id.Id id, org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBusiness(id, businessId)) {
            return (true);
        }

        try (org.osid.financials.BusinessList parents = getParentBusinesses(businessId)) {
            while (parents.hasNext()) {
                if (isAncestorOfBusiness(id, parents.getNextBusiness().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a business has any children. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @return <code> true </code> if the <code> businessId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildBusinesses(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBusinessNode(businessId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  business.
     *
     *  @param  id an <code> Id </code> 
     *  @param businessId the <code> Id </code> of a 
     *         business
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> businessId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> businessId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfBusiness(org.osid.id.Id id, org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfBusiness(businessId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  business.
     *
     *  @param  businessId the <code> Id </code> to query 
     *  @return the children of the business 
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildBusinessIds(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.business.BusinessToIdList(getChildBusinesses(businessId)));
    }


    /**
     *  Gets the children of the given business. 
     *
     *  @param  businessId the <code> Id </code> to query 
     *  @return the children of the business 
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.BusinessList getChildBusinesses(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.businessnode.BusinessNodeToBusinessList(getBusinessNode(businessId).getChildBusinessNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  business.
     *
     *  @param  id an <code> Id </code> 
     *  @param businessId the <code> Id </code> of a 
     *         business
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> businessId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfBusiness(org.osid.id.Id id, org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBusiness(businessId, id)) {
            return (true);
        }

        try (org.osid.financials.BusinessList children = getChildBusinesses(businessId)) {
            while (children.hasNext()) {
                if (isDescendantOfBusiness(id, children.getNextBusiness().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  business.
     *
     *  @param  businessId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified business node 
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getBusinessNodeIds(org.osid.id.Id businessId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.financials.businessnode.BusinessNodeToNode(getBusinessNode(businessId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given business.
     *
     *  @param  businessId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified business node 
     *  @throws org.osid.NotFoundException <code> businessId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> businessId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.BusinessNode getBusinessNodes(org.osid.id.Id businessId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBusinessNode(businessId));
    }


    /**
     *  Closes this <code>BusinessHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a business node.
     *
     *  @param businessId the id of the business node
     *  @throws org.osid.NotFoundException <code>businessId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>businessId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.financials.BusinessNode getBusinessNode(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(businessId, "business Id");
        for (org.osid.financials.BusinessNode business : this.roots) {
            if (business.getId().equals(businessId)) {
                return (business);
            }

            org.osid.financials.BusinessNode r = findBusiness(business, businessId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(businessId + " is not found");
    }


    protected org.osid.financials.BusinessNode findBusiness(org.osid.financials.BusinessNode node, 
                                                            org.osid.id.Id businessId) 
	throws org.osid.OperationFailedException {

        try (org.osid.financials.BusinessNodeList children = node.getChildBusinessNodes()) {
            while (children.hasNext()) {
                org.osid.financials.BusinessNode business = children.getNextBusinessNode();
                if (business.getId().equals(businessId)) {
                    return (business);
                }
                
                business = findBusiness(business, businessId);
                if (business != null) {
                    return (business);
                }
            }
        }

        return (null);
    }
}
