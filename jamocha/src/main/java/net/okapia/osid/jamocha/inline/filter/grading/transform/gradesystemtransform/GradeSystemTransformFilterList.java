//
// GradeSystemTransformFilterList.java
//
//     Implements a filtering GradeSystemTransformList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.grading.transform.gradesystemtransform;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering GradeSystemTransformList.
 */

public final class GradeSystemTransformFilterList
    extends net.okapia.osid.jamocha.inline.filter.grading.transform.gradesystemtransform.spi.AbstractGradeSystemTransformFilterList
    implements org.osid.grading.transform.GradeSystemTransformList,
               GradeSystemTransformFilter {

    private final GradeSystemTransformFilter filter;


    /**
     *  Creates a new <code>GradeSystemTransformFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>GradeSystemTransformList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public GradeSystemTransformFilterList(GradeSystemTransformFilter filter, org.osid.grading.transform.GradeSystemTransformList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters GradeSystemTransforms.
     *
     *  @param gradeSystemTransform the grade system transform to filter
     *  @return <code>true</code> if the grade system transform passes the filter,
     *          <code>false</code> if the grade system transform should be filtered
     */

    @Override
    public boolean pass(org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        return (this.filter.pass(gradeSystemTransform));
    }
}
