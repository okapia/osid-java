//
// AbstractQueryCookbookLookupSession.java
//
//    An inline adapter that maps a CookbookLookupSession to
//    a CookbookQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CookbookLookupSession to
 *  a CookbookQuerySession.
 */

public abstract class AbstractQueryCookbookLookupSession
    extends net.okapia.osid.jamocha.recipe.spi.AbstractCookbookLookupSession
    implements org.osid.recipe.CookbookLookupSession {

    private final org.osid.recipe.CookbookQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCookbookLookupSession.
     *
     *  @param querySession the underlying cookbook query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCookbookLookupSession(org.osid.recipe.CookbookQuerySession querySession) {
        nullarg(querySession, "cookbook query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Cookbook</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCookbooks() {
        return (this.session.canSearchCookbooks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Cookbook</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Cookbook</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Cookbook</code> and
     *  retained for compatibility.
     *
     *  @param  cookbookId <code>Id</code> of the
     *          <code>Cookbook</code>
     *  @return the cookbook
     *  @throws org.osid.NotFoundException <code>cookbookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>cookbookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.CookbookQuery query = getQuery();
        query.matchId(cookbookId, true);
        org.osid.recipe.CookbookList cookbooks = this.session.getCookbooksByQuery(query);
        if (cookbooks.hasNext()) {
            return (cookbooks.getNextCookbook());
        } 
        
        throw new org.osid.NotFoundException(cookbookId + " not found");
    }


    /**
     *  Gets a <code>CookbookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cookbooks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Cookbooks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  cookbookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Cookbook</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByIds(org.osid.id.IdList cookbookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.CookbookQuery query = getQuery();

        try (org.osid.id.IdList ids = cookbookIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCookbooksByQuery(query));
    }


    /**
     *  Gets a <code>CookbookList</code> corresponding to the given
     *  cookbook genus <code>Type</code> which does not include
     *  cookbooks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cookbookGenusType a cookbook genus type 
     *  @return the returned <code>Cookbook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByGenusType(org.osid.type.Type cookbookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.CookbookQuery query = getQuery();
        query.matchGenusType(cookbookGenusType, true);
        return (this.session.getCookbooksByQuery(query));
    }


    /**
     *  Gets a <code>CookbookList</code> corresponding to the given
     *  cookbook genus <code>Type</code> and include any additional
     *  cookbooks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cookbookGenusType a cookbook genus type 
     *  @return the returned <code>Cookbook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByParentGenusType(org.osid.type.Type cookbookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.CookbookQuery query = getQuery();
        query.matchParentGenusType(cookbookGenusType, true);
        return (this.session.getCookbooksByQuery(query));
    }


    /**
     *  Gets a <code>CookbookList</code> containing the given
     *  cookbook record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cookbookRecordType a cookbook record type 
     *  @return the returned <code>Cookbook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByRecordType(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.CookbookQuery query = getQuery();
        query.matchRecordType(cookbookRecordType, true);
        return (this.session.getCookbooksByQuery(query));
    }


    /**
     *  Gets a <code>CookbookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known cookbooks or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  cookbooks that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Cookbook</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.CookbookQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getCookbooksByQuery(query));        
    }

    
    /**
     *  Gets all <code>Cookbooks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Cookbooks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recipe.CookbookQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCookbooksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.recipe.CookbookQuery getQuery() {
        org.osid.recipe.CookbookQuery query = this.session.getCookbookQuery();
        
        return (query);
    }
}
