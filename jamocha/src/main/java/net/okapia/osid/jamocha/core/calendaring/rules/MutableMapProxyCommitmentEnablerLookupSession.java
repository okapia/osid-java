//
// MutableMapProxyCommitmentEnablerLookupSession
//
//    Implements a CommitmentEnabler lookup service backed by a collection of
//    commitmentEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements a CommitmentEnabler lookup service backed by a collection of
 *  commitmentEnablers. The commitmentEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of commitment enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCommitmentEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractMapCommitmentEnablerLookupSession
    implements org.osid.calendaring.rules.CommitmentEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyCommitmentEnablerLookupSession}
     *  with no commitment enablers.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCommitmentEnablerLookupSession} with a
     *  single commitment enabler.
     *
     *  @param calendar the calendar
     *  @param commitmentEnabler a commitment enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code commitmentEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putCommitmentEnabler(commitmentEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCommitmentEnablerLookupSession} using an
     *  array of commitment enablers.
     *
     *  @param calendar the calendar
     *  @param commitmentEnablers an array of commitment enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code commitmentEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.rules.CommitmentEnabler[] commitmentEnablers, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putCommitmentEnablers(commitmentEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCommitmentEnablerLookupSession} using a
     *  collection of commitment enablers.
     *
     *  @param calendar the calendar
     *  @param commitmentEnablers a collection of commitment enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code commitmentEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCommitmentEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                java.util.Collection<? extends org.osid.calendaring.rules.CommitmentEnabler> commitmentEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(calendar, proxy);
        setSessionProxy(proxy);
        putCommitmentEnablers(commitmentEnablers);
        return;
    }

    
    /**
     *  Makes a {@code CommitmentEnabler} available in this session.
     *
     *  @param commitmentEnabler an commitment enabler
     *  @throws org.osid.NullArgumentException {@code commitmentEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putCommitmentEnabler(org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler) {
        super.putCommitmentEnabler(commitmentEnabler);
        return;
    }


    /**
     *  Makes an array of commitmentEnablers available in this session.
     *
     *  @param commitmentEnablers an array of commitment enablers
     *  @throws org.osid.NullArgumentException {@code commitmentEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putCommitmentEnablers(org.osid.calendaring.rules.CommitmentEnabler[] commitmentEnablers) {
        super.putCommitmentEnablers(commitmentEnablers);
        return;
    }


    /**
     *  Makes collection of commitment enablers available in this session.
     *
     *  @param commitmentEnablers
     *  @throws org.osid.NullArgumentException {@code commitmentEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putCommitmentEnablers(java.util.Collection<? extends org.osid.calendaring.rules.CommitmentEnabler> commitmentEnablers) {
        super.putCommitmentEnablers(commitmentEnablers);
        return;
    }


    /**
     *  Removes a CommitmentEnabler from this session.
     *
     *  @param commitmentEnablerId the {@code Id} of the commitment enabler
     *  @throws org.osid.NullArgumentException {@code commitmentEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCommitmentEnabler(org.osid.id.Id commitmentEnablerId) {
        super.removeCommitmentEnabler(commitmentEnablerId);
        return;
    }    
}
