//
// InstallationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installation.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class InstallationElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the InstallationElement Id.
     *
     *  @return the installation element Id
     */

    public static org.osid.id.Id getInstallationEntityId() {
        return (makeEntityId("osid.installation.Installation"));
    }


    /**
     *  Gets the SiteId element Id.
     *
     *  @return the SiteId element Id
     */

    public static org.osid.id.Id getSiteId() {
        return (makeElementId("osid.installation.installation.SiteId"));
    }


    /**
     *  Gets the Site element Id.
     *
     *  @return the Site element Id
     */

    public static org.osid.id.Id getSite() {
        return (makeElementId("osid.installation.installation.Site"));
    }


    /**
     *  Gets the PackageId element Id.
     *
     *  @return the PackageId element Id
     */

    public static org.osid.id.Id getPackageId() {
        return (makeElementId("osid.installation.installation.PackageId"));
    }


    /**
     *  Gets the Package element Id.
     *
     *  @return the Package element Id
     */

    public static org.osid.id.Id getPackage() {
        return (makeElementId("osid.installation.installation.Package"));
    }


    /**
     *  Gets the DepotId element Id.
     *
     *  @return the DepotId element Id
     */

    public static org.osid.id.Id getDepotId() {
        return (makeElementId("osid.installation.installation.DepotId"));
    }


    /**
     *  Gets the Depot element Id.
     *
     *  @return the Depot element Id
     */

    public static org.osid.id.Id getDepot() {
        return (makeElementId("osid.installation.installation.Depot"));
    }


    /**
     *  Gets the InstallDate element Id.
     *
     *  @return the InstallDate element Id
     */

    public static org.osid.id.Id getInstallDate() {
        return (makeElementId("osid.installation.installation.InstallDate"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeElementId("osid.installation.installation.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeElementId("osid.installation.installation.Agent"));
    }


    /**
     *  Gets the LastCheckDate element Id.
     *
     *  @return the LastCheckDate element Id
     */

    public static org.osid.id.Id getLastCheckDate() {
        return (makeElementId("osid.installation.installation.LastCheckDate"));
    }
}
