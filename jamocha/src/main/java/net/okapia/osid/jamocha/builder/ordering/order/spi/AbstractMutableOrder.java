//
// AbstractMutableOrder.java
//
//     Defines a mutable Order.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Order</code>.
 */

public abstract class AbstractMutableOrder
    extends net.okapia.osid.jamocha.ordering.order.spi.AbstractOrder
    implements org.osid.ordering.Order,
               net.okapia.osid.jamocha.builder.ordering.order.OrderMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this order. 
     *
     *  @param record order record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addOrderRecord(org.osid.ordering.records.OrderRecord record, org.osid.type.Type recordType) {
        super.addOrderRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this order.
     *
     *  @param displayName the name for this order
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this order.
     *
     *  @param description the description of this order
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    @Override
    public void setCustomer(org.osid.resource.Resource customer) {
        super.setCustomer(customer);
        return;
    }


    /**
     *  Adds an item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    @Override
    public void addItem(org.osid.ordering.Item item) {
        super.addItem(item);
        return;
    }


    /**
     *  Sets all the items.
     *
     *  @param items a collection of items
     *  @throws org.osid.NullArgumentException <code>items</code> is
     *          <code>null</code>
     */

    @Override
    public void setItems(java.util.Collection<org.osid.ordering.Item> items) {
        super.setItems(items);
        return;
    }


    /**
     *  Sets the total cost.
     *
     *  @param cost a total cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    @Override
    public void setTotalCost(org.osid.financials.Currency cost) {
        super.setTotalCost(cost);
        return;
    }


    /**
     *  Sets the atomic flag.
     *
     *  @param atomic <code> true </code> if the order is atomic,
     *          <code> false </code> otherwise
     */

    @Override
    public void setAtomic(boolean atomic) {
        super.setAtomic(atomic);
        return;
    }


    /**
     *  Sets the submit date.
     *
     *  @param submitDate a submit date
     *  @throws org.osid.NullArgumentException <code>submitDate</code>
     *          is <code>null</code>
     */

    @Override
    public void setSubmitDate(org.osid.calendaring.DateTime submitDate) {
        super.setSubmitDate(submitDate);
        return;
    }


    /**
     *  Sets the submitter.
     *
     *  @param resource the submitting resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void setSubmitter(org.osid.resource.Resource resource) {
        super.setSubmitter(resource);
        return;
    }


    /**
     *  Sets the submitting agent.
     *
     *  @param agent the submitting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setSubmittingAgent(org.osid.authentication.Agent agent) {
        super.setSubmittingAgent(agent);
        return;
    }


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setClosedDate(org.osid.calendaring.DateTime date) {
        super.setClosedDate(date);
        return;
    }


    /**
     *  Sets the closer.
     *
     *  @param resource the closing resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void setCloser(org.osid.resource.Resource resource) {
        super.setCloser(resource);
        return;
    }


    /**
     *  Sets the closing agent.
     *
     *  @param agent the closing agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setClosingAgent(org.osid.authentication.Agent agent) {
        super.setClosingAgent(agent);
        return;
    }
}

