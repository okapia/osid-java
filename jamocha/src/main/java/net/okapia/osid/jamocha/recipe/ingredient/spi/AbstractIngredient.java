//
// AbstractIngredient.java
//
//     Defines an Ingredient.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.ingredient.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Ingredient</code>.
 */

public abstract class AbstractIngredient
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.recipe.Ingredient {

    private java.math.BigDecimal quantity;
    private org.osid.type.Type unitType;
    private org.osid.inventory.Stock stock;

    private final java.util.Collection<org.osid.recipe.records.IngredientRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the quantity of items. 
     *
     *  @return the quantity 
     */

    @OSID @Override
    public java.math.BigDecimal getQuantity() {
        return (this.quantity);
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     *  @throws org.osid.NullArgumentException <code>quantity</code>
     *          is <code>null</code>
     */

    protected void setQuantity(java.math.BigDecimal quantity) {
        cardinalarg(quantity, "quantity");
        this.quantity = quantity;
        return;
    }


    /**
     *  Gets the units of the quantity. 
     *
     *  @return the unit type 
     */

    @OSID @Override
    public org.osid.type.Type getUnitType() {
        return (this.unitType);
    }


    /**
     *  Sets the unit type.
     *
     *  @param unitType an unit type
     *  @throws org.osid.NullArgumentException
     *          <code>unitType</code> is <code>null</code>
     */

    protected void setUnitType(org.osid.type.Type unitType) {
        nullarg(unitType, "unit type");
        this.unitType = unitType;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Stock. </code> 
     *
     *  @return the stock <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        return (this.stock.getId());
    }


    /**
     *  Get the stock. 
     *
     *  @return the stock 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        return (this.stock);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @throws org.osid.NullArgumentException
     *          <code>stock</code> is <code>null</code>
     */

    protected void setStock(org.osid.inventory.Stock stock) {
        nullarg(stock, "stock");
        this.stock = stock;
        return;
    }


    /**
     *  Tests if this ingredient supports the given record
     *  <code>Type</code>.
     *
     *  @param  ingredientRecordType an ingredient record type 
     *  @return <code>true</code> if the ingredientRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>ingredientRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ingredientRecordType) {
        for (org.osid.recipe.records.IngredientRecord record : this.records) {
            if (record.implementsRecordType(ingredientRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Ingredient</code> record <code>Type</code>.
     *
     *  @param  ingredientRecordType the ingredient record type 
     *  @return the ingredient record 
     *  @throws org.osid.NullArgumentException
     *          <code>ingredientRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ingredientRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.IngredientRecord getIngredientRecord(org.osid.type.Type ingredientRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.IngredientRecord record : this.records) {
            if (record.implementsRecordType(ingredientRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ingredientRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ingredient. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ingredientRecord the ingredient record
     *  @param ingredientRecordType ingredient record type
     *  @throws org.osid.NullArgumentException
     *          <code>ingredientRecord</code> or
     *          <code>ingredientRecordTypeingredient</code> is
     *          <code>null</code>
     */
            
    protected void addIngredientRecord(org.osid.recipe.records.IngredientRecord ingredientRecord, 
                                       org.osid.type.Type ingredientRecordType) {
        
        nullarg(ingredientRecord, "ingredient record");
        addRecordType(ingredientRecordType);
        this.records.add(ingredientRecord);
        
        return;
    }    
}
