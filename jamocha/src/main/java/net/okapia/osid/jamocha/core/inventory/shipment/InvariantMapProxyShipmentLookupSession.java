//
// InvariantMapProxyShipmentLookupSession
//
//    Implements a Shipment lookup service backed by a fixed
//    collection of shipments. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.shipment;


/**
 *  Implements a Shipment lookup service backed by a fixed
 *  collection of shipments. The shipments are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyShipmentLookupSession
    extends net.okapia.osid.jamocha.core.inventory.shipment.spi.AbstractMapShipmentLookupSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyShipmentLookupSession} with no
     *  shipments.
     *
     *  @param warehouse the warehouse
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.proxy.Proxy proxy) {
        setWarehouse(warehouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyShipmentLookupSession} with a single
     *  shipment.
     *
     *  @param warehouse the warehouse
     *  @param shipment a single shipment
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code shipment} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.shipment.Shipment shipment, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putShipment(shipment);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyShipmentLookupSession} using
     *  an array of shipments.
     *
     *  @param warehouse the warehouse
     *  @param shipments an array of shipments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code shipments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.shipment.Shipment[] shipments, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putShipments(shipments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyShipmentLookupSession} using a
     *  collection of shipments.
     *
     *  @param warehouse the warehouse
     *  @param shipments a collection of shipments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code shipments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  java.util.Collection<? extends org.osid.inventory.shipment.Shipment> shipments,
                                                  org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putShipments(shipments);
        return;
    }
}
