//
// AbstractAdapterOfferingConstrainerEnablerLookupSession.java
//
//    An OfferingConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OfferingConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterOfferingConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.rules.OfferingConstrainerEnablerLookupSession {

    private final org.osid.offering.rules.OfferingConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOfferingConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOfferingConstrainerEnablerLookupSession(org.osid.offering.rules.OfferingConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code OfferingConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOfferingConstrainerEnablers() {
        return (this.session.canLookupOfferingConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code OfferingConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOfferingConstrainerEnablerView() {
        this.session.useComparativeOfferingConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code OfferingConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOfferingConstrainerEnablerView() {
        this.session.usePlenaryOfferingConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offering constrainer enablers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active offering constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOfferingConstrainerEnablerView() {
        this.session.useActiveOfferingConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive offering constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOfferingConstrainerEnablerView() {
        this.session.useAnyStatusOfferingConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code OfferingConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code OfferingConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code OfferingConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param offeringConstrainerEnablerId {@code Id} of the {@code OfferingConstrainerEnabler}
     *  @return the offering constrainer enabler
     *  @throws org.osid.NotFoundException {@code offeringConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code offeringConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnabler getOfferingConstrainerEnabler(org.osid.id.Id offeringConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainerEnabler(offeringConstrainerEnablerId));
    }


    /**
     *  Gets an {@code OfferingConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offeringConstrainerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code OfferingConstrainerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code OfferingConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByIds(org.osid.id.IdList offeringConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainerEnablersByIds(offeringConstrainerEnablerIds));
    }


    /**
     *  Gets an {@code OfferingConstrainerEnablerList} corresponding to the given
     *  offering constrainer enabler genus {@code Type} which does not include
     *  offering constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerGenusType an offeringConstrainerEnabler genus type 
     *  @return the returned {@code OfferingConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByGenusType(org.osid.type.Type offeringConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainerEnablersByGenusType(offeringConstrainerEnablerGenusType));
    }


    /**
     *  Gets an {@code OfferingConstrainerEnablerList} corresponding to the given
     *  offering constrainer enabler genus {@code Type} and include any additional
     *  offering constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerGenusType an offeringConstrainerEnabler genus type 
     *  @return the returned {@code OfferingConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByParentGenusType(org.osid.type.Type offeringConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainerEnablersByParentGenusType(offeringConstrainerEnablerGenusType));
    }


    /**
     *  Gets an {@code OfferingConstrainerEnablerList} containing the given
     *  offering constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerRecordType an offeringConstrainerEnabler record type 
     *  @return the returned {@code OfferingConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainerEnablersByRecordType(offeringConstrainerEnablerRecordType));
    }


    /**
     *  Gets an {@code OfferingConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code OfferingConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code OfferingConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code OfferingConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getOfferingConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code OfferingConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @return a list of {@code OfferingConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainerEnablers());
    }
}
