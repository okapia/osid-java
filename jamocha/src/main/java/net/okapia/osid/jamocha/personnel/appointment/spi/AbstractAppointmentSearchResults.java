//
// AbstractAppointmentSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.appointment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAppointmentSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.personnel.AppointmentSearchResults {

    private org.osid.personnel.AppointmentList appointments;
    private final org.osid.personnel.AppointmentQueryInspector inspector;
    private final java.util.Collection<org.osid.personnel.records.AppointmentSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAppointmentSearchResults.
     *
     *  @param appointments the result set
     *  @param appointmentQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>appointments</code>
     *          or <code>appointmentQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAppointmentSearchResults(org.osid.personnel.AppointmentList appointments,
                                            org.osid.personnel.AppointmentQueryInspector appointmentQueryInspector) {
        nullarg(appointments, "appointments");
        nullarg(appointmentQueryInspector, "appointment query inspectpr");

        this.appointments = appointments;
        this.inspector = appointmentQueryInspector;

        return;
    }


    /**
     *  Gets the appointment list resulting from a search.
     *
     *  @return an appointment list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointments() {
        if (this.appointments == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.personnel.AppointmentList appointments = this.appointments;
        this.appointments = null;
	return (appointments);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.personnel.AppointmentQueryInspector getAppointmentQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  appointment search record <code> Type. </code> This method must
     *  be used to retrieve an appointment implementing the requested
     *  record.
     *
     *  @param appointmentSearchRecordType an appointment search 
     *         record type 
     *  @return the appointment search
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(appointmentSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentSearchResultsRecord getAppointmentSearchResultsRecord(org.osid.type.Type appointmentSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.personnel.records.AppointmentSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(appointmentSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(appointmentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record appointment search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAppointmentRecord(org.osid.personnel.records.AppointmentSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "appointment record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
