//
// AbstractAdapterInquestLookupSession.java
//
//    An Inquest lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Inquest lookup session adapter.
 */

public abstract class AbstractAdapterInquestLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inquiry.InquestLookupSession {

    private final org.osid.inquiry.InquestLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterInquestLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInquestLookupSession(org.osid.inquiry.InquestLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Inquest} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupInquests() {
        return (this.session.canLookupInquests());
    }


    /**
     *  A complete view of the {@code Inquest} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInquestView() {
        this.session.useComparativeInquestView();
        return;
    }


    /**
     *  A complete view of the {@code Inquest} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInquestView() {
        this.session.usePlenaryInquestView();
        return;
    }

     
    /**
     *  Gets the {@code Inquest} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Inquest} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Inquest} and
     *  retained for compatibility.
     *
     *  @param inquestId {@code Id} of the {@code Inquest}
     *  @return the inquest
     *  @throws org.osid.NotFoundException {@code inquestId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code inquestId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquest(inquestId));
    }


    /**
     *  Gets an {@code InquestList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inquests specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Inquests} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  inquestIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Inquest} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code inquestIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByIds(org.osid.id.IdList inquestIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquestsByIds(inquestIds));
    }


    /**
     *  Gets an {@code InquestList} corresponding to the given
     *  inquest genus {@code Type} which does not include
     *  inquests of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inquestGenusType an inquest genus type 
     *  @return the returned {@code Inquest} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquestGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByGenusType(org.osid.type.Type inquestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquestsByGenusType(inquestGenusType));
    }


    /**
     *  Gets an {@code InquestList} corresponding to the given
     *  inquest genus {@code Type} and include any additional
     *  inquests with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inquestGenusType an inquest genus type 
     *  @return the returned {@code Inquest} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquestGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByParentGenusType(org.osid.type.Type inquestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquestsByParentGenusType(inquestGenusType));
    }


    /**
     *  Gets an {@code InquestList} containing the given
     *  inquest record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inquestRecordType an inquest record type 
     *  @return the returned {@code Inquest} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquestRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByRecordType(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquestsByRecordType(inquestRecordType));
    }


    /**
     *  Gets an {@code InquestList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Inquest} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquestsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Inquests}. 
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Inquests} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquests());
    }
}
