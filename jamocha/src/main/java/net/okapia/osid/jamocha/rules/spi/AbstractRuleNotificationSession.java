//
// AbstractRuleNotificationSession.java
//
//     A template for making RuleNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Rule} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Rule} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for rule entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRuleNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.rules.RuleNotificationSession {

    private boolean federated = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();


    /**
     *  Gets the {@code Engine/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Engine Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }

    
    /**
     *  Gets the {@code Engine} associated with this session.
     *
     *  @return the {@code Engine} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the {@code Engine}.
     *
     *  @param engine the engine for this session
     *  @throws org.osid.NullArgumentException {@code engine}
     *          is {@code null}
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can register for {@code Rule}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRuleNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeRuleNotification() </code>.
     */

    @OSID @Override
    public void reliableRuleNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableRuleNotifications() {
        return;
    }


    /**
     *  Acknowledge a rule notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeRuleNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rules in engines which are children of
     *  this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new rules. {@code
     *  RuleReceiver.newRule()} is invoked when a new {@code Rule} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated rules. {@code
     *  RuleReceiver.changedRule()} is invoked when a rule is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated rule. {@code
     *  RuleReceiver.changedRule()} is invoked when the specified rule
     *  is changed.
     *
     *  @param ruleId the {@code Id} of the {@code Rule} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code ruleId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRule(org.osid.id.Id ruleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted rules. {@code
     *  RuleReceiver.deletedRule()} is invoked when a rule is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted rule. {@code
     *  RuleReceiver.deletedRule()} is invoked when the specified rule
     *  is deleted.
     *
     *  @param ruleId the {@code Id} of the
     *          {@code Rule} to monitor
     *  @throws org.osid.NullArgumentException {@code ruleId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRule(org.osid.id.Id ruleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
