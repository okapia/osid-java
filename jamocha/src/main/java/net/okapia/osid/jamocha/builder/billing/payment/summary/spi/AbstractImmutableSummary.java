//
// AbstractImmutableSummary.java
//
//     Wraps a mutable Summary to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.summary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Summary</code> to hide modifiers. This
 *  wrapper provides an immutized Summary from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying summary whose state changes are visible.
 */

public abstract class AbstractImmutableSummary
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCompendium
    implements org.osid.billing.payment.Summary {

    private final org.osid.billing.payment.Summary summary;


    /**
     *  Constructs a new <code>AbstractImmutableSummary</code>.
     *
     *  @param summary the summary to immutablize
     *  @throws org.osid.NullArgumentException <code>summary</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSummary(org.osid.billing.payment.Summary summary) {
        super(summary);
        this.summary = summary;
        return;
    }


    /**
     *  Gets the customer <code> Id. </code> 
     *
     *  @return the customer <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCustomer() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.summary.getCustomerId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        return (this.summary.getCustomer());
    }


    /**
     *  Gets the billing period <code> Id </code> To which this summary 
     *  pertains. 
     *
     *  @return the period <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPeriodId() {
        return (this.summary.getPeriodId());
    }


    /**
     *  Gets the billing period to which this summary pertains. 
     *
     *  @return the period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod()
        throws org.osid.OperationFailedException {

        return (this.summary.getPeriod());
    }


    /**
     *  Tests if the balance is a credit or customer owes money. 
     *
     *  @return <code> true </code> if a credit, <code> false </code> if a 
     *          debit 
     */

    @OSID @Override
    public boolean isCreditBalance() {
        return (this.summary.isCreditBalance());
    }


    /**
     *  Gets the balance for this customer. 
     *
     *  @return the balance 
     */

    @OSID @Override
    public org.osid.financials.Currency getBalance() {
        return (this.summary.getBalance());
    }


    /**
     *  Tests if the customer has paid before. 
     *
     *  @return <code> true </code> if there is a last payment, <code> false 
     *          </code> otehrwise 
     */

    @OSID @Override
    public boolean hasLastPayment() {
        return (this.summary.hasLastPayment());
    }


    /**
     *  Gets the date the last payment was made. 
     *
     *  @return the last payment date 
     *  @throws org.osid.IllegalStateException <code> hasLastPayment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastPaymentDate() {
        return (this.summary.getLastPaymentDate());
    }


    /**
     *  Gets the last payment for this customer. 
     *
     *  @return the payment 
     *  @throws org.osid.IllegalStateException <code> hasLastPayment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getLastPayment() {
        return (this.summary.getLastPayment());
    }


    /**
     *  Gets the date the next payment is due. 
     *
     *  @return the payment due date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPaymentDueDate() {
        return (this.summary.getPaymentDueDate());
    }


    /**
     *  Gets the summary record corresponding to the given <code>
     *  Summary </code> record <code> Type. </code> This method is
     *  used to retrieve an object implementing the requested
     *  record. The <code> summaryRecordType </code> may be the <code>
     *  Type </code> returned in <code> getRecordTypes() </code> or
     *  any of its parents in a <code> Type </code> hierarchy where
     *  <code> hasRecordType(summaryRecordType) </code> is <code> true
     *  </code>.
     *
     *  @param  summaryRecordType the type of summary record to retrieve 
     *  @return the summary record 
     *  @throws org.osid.NullArgumentException <code>
     *          summaryRecordType </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(summaryRecordType) </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.SummaryRecord getSummaryRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        return (this.summary.getSummaryRecord(summaryRecordType));
    }
}

