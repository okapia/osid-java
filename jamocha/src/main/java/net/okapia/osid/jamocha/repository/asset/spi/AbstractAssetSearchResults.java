//
// AbstractAssetSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAssetSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.repository.AssetSearchResults {

    private org.osid.repository.AssetList assets;
    private final org.osid.repository.AssetQueryInspector inspector;
    private final java.util.Collection<org.osid.repository.records.AssetSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAssetSearchResults.
     *
     *  @param assets the result set
     *  @param assetQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>assets</code>
     *          or <code>assetQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAssetSearchResults(org.osid.repository.AssetList assets,
                                            org.osid.repository.AssetQueryInspector assetQueryInspector) {
        nullarg(assets, "assets");
        nullarg(assetQueryInspector, "asset query inspectpr");

        this.assets = assets;
        this.inspector = assetQueryInspector;

        return;
    }


    /**
     *  Gets the asset list resulting from a search.
     *
     *  @return an asset list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets() {
        if (this.assets == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.repository.AssetList assets = this.assets;
        this.assets = null;
	return (assets);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.repository.AssetQueryInspector getAssetQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  asset search record <code> Type. </code> This method must
     *  be used to retrieve an asset implementing the requested
     *  record.
     *
     *  @param assetSearchRecordType an asset search 
     *         record type 
     *  @return the asset search
     *  @throws org.osid.NullArgumentException
     *          <code>assetSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(assetSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetSearchResultsRecord getAssetSearchResultsRecord(org.osid.type.Type assetSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.repository.records.AssetSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(assetSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(assetSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record asset search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAssetRecord(org.osid.repository.records.AssetSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "asset record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
