//
// AbstractPackageSearchOdrer.java
//
//     Defines a PackageSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PackageSearchOrder}.
 */

public abstract class AbstractPackageSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectSearchOrder
    implements org.osid.installation.PackageSearchOrder {

    private final java.util.Collection<org.osid.installation.records.PackageSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the version. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVersion(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the copyright. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCopyright(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the license 
     *  acknowledgement flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiresLicenseAcknowledgement(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the creator. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreator(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available for creator 
     *  resources. 
     *
     *  @return <code> true </code> if a creator resource search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a creator resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreatorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCreatorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreatorSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the release date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReleaseDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the url. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByURL(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  pkgRecordType a package record type 
     *  @return {@code true} if the pkgRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code pkgRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pkgRecordType) {
        for (org.osid.installation.records.PackageSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  pkgRecordType the package record type 
     *  @return the package search order record
     *  @throws org.osid.NullArgumentException
     *          {@code pkgRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(pkgRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.installation.records.PackageSearchOrderRecord getPackageSearchOrderRecord(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.PackageSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this package. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pkgRecord the package search odrer record
     *  @param pkgRecordType package record type
     *  @throws org.osid.NullArgumentException
     *          {@code pkgRecord} or
     *          {@code pkgRecordTypepkg} is
     *          {@code null}
     */
            
    protected void addPackageRecord(org.osid.installation.records.PackageSearchOrderRecord pkgSearchOrderRecord, 
                                     org.osid.type.Type pkgRecordType) {

        addRecordType(pkgRecordType);
        this.records.add(pkgSearchOrderRecord);
        
        return;
    }
}
