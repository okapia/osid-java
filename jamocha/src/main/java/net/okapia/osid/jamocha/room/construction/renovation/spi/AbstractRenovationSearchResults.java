//
// AbstractRenovationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.renovation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRenovationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.room.construction.RenovationSearchResults {

    private org.osid.room.construction.RenovationList renovations;
    private final org.osid.room.construction.RenovationQueryInspector inspector;
    private final java.util.Collection<org.osid.room.construction.records.RenovationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRenovationSearchResults.
     *
     *  @param renovations the result set
     *  @param renovationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>renovations</code>
     *          or <code>renovationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRenovationSearchResults(org.osid.room.construction.RenovationList renovations,
                                            org.osid.room.construction.RenovationQueryInspector renovationQueryInspector) {
        nullarg(renovations, "renovations");
        nullarg(renovationQueryInspector, "renovation query inspectpr");

        this.renovations = renovations;
        this.inspector = renovationQueryInspector;

        return;
    }


    /**
     *  Gets the renovation list resulting from a search.
     *
     *  @return a renovation list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovations() {
        if (this.renovations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.room.construction.RenovationList renovations = this.renovations;
        this.renovations = null;
	return (renovations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.room.construction.RenovationQueryInspector getRenovationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  renovation search record <code> Type. </code> This method must
     *  be used to retrieve a renovation implementing the requested
     *  record.
     *
     *  @param renovationSearchRecordType a renovation search 
     *         record type 
     *  @return the renovation search
     *  @throws org.osid.NullArgumentException
     *          <code>renovationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(renovationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationSearchResultsRecord getRenovationSearchResultsRecord(org.osid.type.Type renovationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.room.construction.records.RenovationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(renovationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(renovationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record renovation search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRenovationRecord(org.osid.room.construction.records.RenovationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "renovation record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
