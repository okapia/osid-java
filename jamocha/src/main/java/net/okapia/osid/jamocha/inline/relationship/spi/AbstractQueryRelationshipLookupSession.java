//
// AbstractQueryRelationshipLookupSession.java
//
//    An inline adapter that maps a RelationshipLookupSession to
//    a RelationshipQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RelationshipLookupSession to
 *  a RelationshipQuerySession.
 */

public abstract class AbstractQueryRelationshipLookupSession
    extends net.okapia.osid.jamocha.relationship.spi.AbstractRelationshipLookupSession
    implements org.osid.relationship.RelationshipLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.relationship.RelationshipQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRelationshipLookupSession.
     *
     *  @param querySession the underlying relationship query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRelationshipLookupSession(org.osid.relationship.RelationshipQuerySession querySession) {
        nullarg(querySession, "relationship query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Family</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Family Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.session.getFamilyId());
    }


    /**
     *  Gets the <code>Family</code> associated with this 
     *  session.
     *
     *  @return the <code>Family</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFamily());
    }


    /**
     *  Tests if this user can perform <code>Relationship</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelationships() {
        return (this.session.canSearchRelationships());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationships in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.session.useFederatedFamilyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        this.session.useIsolatedFamilyView();
        return;
    }
    

    /**
     *  Only relationships whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRelationshipView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All relationships of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRelationshipView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Relationship</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Relationship</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Relationship</code> and
     *  retained for compatibility.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipId <code>Id</code> of the
     *          <code>Relationship</code>
     *  @return the relationship
     *  @throws org.osid.NotFoundException <code>relationshipId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relationshipId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Relationship getRelationship(org.osid.id.Id relationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchId(relationshipId, true);
        org.osid.relationship.RelationshipList relationships = this.session.getRelationshipsByQuery(query);
        if (relationships.hasNext()) {
            return (relationships.getNextRelationship());
        } 
        
        throw new org.osid.NotFoundException(relationshipId + " not found");
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Relationships</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  relationshipIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByIds(org.osid.id.IdList relationshipIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();

        try (org.osid.id.IdList ids = relationshipIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the given
     *  relationship genus <code>Type</code> which does not include
     *  relationships of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchGenusType(relationshipGenusType, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given relationship genus <code>Type</code> and include any
     *  additional relationships with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByParentGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchParentGenusType(relationshipGenusType, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipList</code> containing the given
     *  relationship record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipRecordType a relationship record type 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByRecordType(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchRecordType(relationshipRecordType, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code>RelationshipList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Relationship</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsOnDate(org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRelationshipsByQuery(query));
    }
        

    /**
     *  Gets a list of relationships corresponding to a source
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.relationship.RelationshipList getRelationshipsForSource(org.osid.id.Id sourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of relationships corresponding to a source
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForSourceOnDate(org.osid.id.Id sourceId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code> RelationshipList </code> corresponding to the
     *  given peer <code> Id </code> and relationship genus <code>
     *  Type. Relationships </code> of any genus derived from the
     *  given genus are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible <code> Relationships
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer <code> Id </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> or 
     *          <code> relationshipGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSource(org.osid.id.Id sourceId, 
                                                                                       org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchGenusType(relationshipGenusType, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code> RelationshipList </code> corresponding to the given peer 
     *  <code> Id </code> and relationship genus <code> Type </code> and 
     *  effective during the entire given date range inclusive but not 
     *  confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all of the relationships 
     *  corresponding to the given peer or an error results if a relationship 
     *  is inaccessible. Otherwise, inaccessible <code> Relationships </code> 
     *  may be omitted from the list. 
     *  
     *  In effective mode, relationships are returned that are currently 
     *  effective in addition to being effective during the given dates. In 
     *  any effective mode, effective relationships and those currently 
     *  expired are returned. 
     *
     *  @param  sourceId a peer <code> Id </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from is greater than 
     *          to </code> 
     *  @throws org.osid.NullArgumentException <code> sourceId, 
     *          relationshipGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSourceOnDate(org.osid.id.Id sourceId, 
                                                                                             org.osid.type.Type relationshipGenusType, 
                                                                                             org.osid.calendaring.DateTime from, 
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchGenusType(relationshipGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of relationships corresponding to a destination
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the <code>Id</code> of the destination
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.relationship.RelationshipList getRelationshipsForDestination(org.osid.id.Id destinationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchDestinationId(destinationId, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of relationships corresponding to a destination
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the <code>Id</code> of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForDestinationOnDate(org.osid.id.Id destinationId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchDestinationId(destinationId, true);
        query.matchDate(from, to, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code> RelationshipList </code> corresponding to the
     *  given peer <code> Id </code> and relationship genus <code>
     *  Type. Relationships </code> of any genus derived from the
     *  given genus are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible <code> Relationships
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId a peer <code> Id </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code> destinationId
     *          </code> or <code> relationshipGenusType </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestination(org.osid.id.Id destinationId, 
                                                                                            org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchDestinationId(destinationId, true);
        query.matchGenusType(relationshipGenusType, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code> RelationshipList </code> corresponding to the
     *  given peer <code> Id </code> and relationship genus <code>
     *  Type </code> and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code> Relationships </code> may be omitted from
     *  the list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param destinationId a peer <code> Id </code>
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from is greater than 
     *          to </code> 
     *  @throws org.osid.NullArgumentException <code> destinationId, 
     *          relationshipGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestinationOnDate(org.osid.id.Id destinationId, 
                                                                                                  org.osid.type.Type relationshipGenusType, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchDestinationId(destinationId, true);
        query.matchGenusType(relationshipGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of relationships corresponding to source and
     *  destination <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  destinationId the <code>Id</code> of the destination
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeers(org.osid.id.Id sourceId,
                                                                           org.osid.id.Id destinationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchDestinationId(destinationId, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a list of relationships corresponding to source and
     *  destination <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  destinationId the <code>Id</code> of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeersOnDate(org.osid.id.Id sourceId,
                                                                                 org.osid.id.Id destinationId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchDestinationId(destinationId, true);
        query.matchDate(from, to, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code> RelationshipList </code> corresponding between
     *  the given peer <code> Ids </code> and relationship genus
     *  <code> Type.  Relationships </code> of any genus derived from
     *  the given genus are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code> Relationships </code> may be omitted from
     *  the list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer <code> Id </code> 
     *  @param  destinationId a related peer <code> Id </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException <code> sourceId,
     *          destinationId, </code> or <code> relationshipGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeers(org.osid.id.Id sourceId, 
                                                                                      org.osid.id.Id destinationId, 
                                                                                      org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchDestinationId(destinationId, true);
        query.matchGenusType(relationshipGenusType, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets a <code> RelationshipList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer set or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code> Relationships </code> may be omitted from
     *  the list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  sourceId a peer <code> Id </code> 
     *  @param  destinationId a related peer <code> Id </code> 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException <code> from is greater than 
     *          to </code> 
     *  @throws org.osid.NullArgumentException <code> sourceId, destinationId, 
     *          relationshipGenusType, from </code> or <code> to </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeersOnDate(org.osid.id.Id sourceId, 
                                                                                            org.osid.id.Id destinationId, 
                                                                                            org.osid.type.Type relationshipGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchSourceId(sourceId, true);
        query.matchDestinationId(destinationId, true);
        query.matchGenusType(relationshipGenusType, true);        
        query.matchDate(from, to, true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets all <code>Relationships</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Relationships</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.relationship.RelationshipQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRelationshipsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.relationship.RelationshipQuery getQuery() {
        org.osid.relationship.RelationshipQuery query = this.session.getRelationshipQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
