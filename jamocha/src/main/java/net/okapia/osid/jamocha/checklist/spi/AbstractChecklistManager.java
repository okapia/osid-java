//
// AbstractChecklistManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractChecklistManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.checklist.ChecklistManager,
               org.osid.checklist.ChecklistProxyManager {

    private final Types todoRecordTypes                    = new TypeRefSet();
    private final Types todoSearchRecordTypes              = new TypeRefSet();

    private final Types checklistRecordTypes               = new TypeRefSet();
    private final Types checklistSearchRecordTypes         = new TypeRefSet();

    private final Types priorityTypes                      = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractChecklistManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractChecklistManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any checklist federation is exposed. Federation is exposed 
     *  when a specific checklist may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of checklists appears as a single checklist. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a todo lookup service. 
     *
     *  @return <code> true </code> if todo lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoLookup() {
        return (false);
    }


    /**
     *  Tests if querying todos is available. 
     *
     *  @return <code> true </code> if todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoQuery() {
        return (false);
    }


    /**
     *  Tests if searching for todos is available. 
     *
     *  @return <code> true </code> if todo search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoSearch() {
        return (false);
    }


    /**
     *  Tests if managing todos is available. 
     *
     *  @return <code> true </code> if todo admin is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoAdmin() {
        return (false);
    }


    /**
     *  Tests if todo notification is available. 
     *
     *  @return <code> true </code> if todo notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoNotification() {
        return (false);
    }


    /**
     *  Tests if todo <code> </code> hierarchy traversal service is supported. 
     *
     *  @return <code> true </code> if todo hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoHierarchy() {
        return (false);
    }


    /**
     *  Tests if a todo <code> </code> hierarchy design service is supported. 
     *
     *  @return <code> true </code> if todo hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a todo to checklist lookup session is available. 
     *
     *  @return <code> true </code> if todo checklist lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoChecklist() {
        return (false);
    }


    /**
     *  Tests if a todo to checklist assignment session is available. 
     *
     *  @return <code> true </code> if todo checklist assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoChecklistAssignment() {
        return (false);
    }


    /**
     *  Tests if a todo smart checklisting session is available. 
     *
     *  @return <code> true </code> if todo smart checklisting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoSmartChecklist() {
        return (false);
    }


    /**
     *  Tests for the availability of an checklist lookup service. 
     *
     *  @return <code> true </code> if checklist lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistLookup() {
        return (false);
    }


    /**
     *  Tests if querying checklists is available. 
     *
     *  @return <code> true </code> if checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistQuery() {
        return (false);
    }


    /**
     *  Tests if searching for checklists is available. 
     *
     *  @return <code> true </code> if checklist search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a checklist administrative service for 
     *  creating and deleting checklists. 
     *
     *  @return <code> true </code> if checklist administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a checklist notification service. 
     *
     *  @return <code> true </code> if checklist notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a checklist hierarchy traversal service. 
     *
     *  @return <code> true </code> if checklist hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a checklist hierarchy design service. 
     *
     *  @return <code> true </code> if checklist hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a checklist batch service. 
     *
     *  @return <code> true </code> if checklist batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a checklist mason service. 
     *
     *  @return <code> true </code> if checklist mason service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistMason() {
        return (false);
    }


    /**
     *  Gets the supported <code> Todo </code> record types. 
     *
     *  @return a list containing the supported todo record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.todoRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Todo </code> record type is supported. 
     *
     *  @param  todoRecordType a <code> Type </code> indicating a <code> Todo 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> todoRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoRecordType(org.osid.type.Type todoRecordType) {
        return (this.todoRecordTypes.contains(todoRecordType));
    }


    /**
     *  Adds support for a todo record type.
     *
     *  @param todoRecordType a todo record type
     *  @throws org.osid.NullArgumentException
     *  <code>todoRecordType</code> is <code>null</code>
     */

    protected void addTodoRecordType(org.osid.type.Type todoRecordType) {
        this.todoRecordTypes.add(todoRecordType);
        return;
    }


    /**
     *  Removes support for a todo record type.
     *
     *  @param todoRecordType a todo record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>todoRecordType</code> is <code>null</code>
     */

    protected void removeTodoRecordType(org.osid.type.Type todoRecordType) {
        this.todoRecordTypes.remove(todoRecordType);
        return;
    }


    /**
     *  Gets the supported todo search record types. 
     *
     *  @return a list containing the supported todo search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.todoSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given todo search record type is supported. 
     *
     *  @param  todoSearchRecordType a <code> Type </code> indicating a todo 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> todoSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoSearchRecordType(org.osid.type.Type todoSearchRecordType) {
        return (this.todoSearchRecordTypes.contains(todoSearchRecordType));
    }


    /**
     *  Adds support for a todo search record type.
     *
     *  @param todoSearchRecordType a todo search record type
     *  @throws org.osid.NullArgumentException
     *  <code>todoSearchRecordType</code> is <code>null</code>
     */

    protected void addTodoSearchRecordType(org.osid.type.Type todoSearchRecordType) {
        this.todoSearchRecordTypes.add(todoSearchRecordType);
        return;
    }


    /**
     *  Removes support for a todo search record type.
     *
     *  @param todoSearchRecordType a todo search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>todoSearchRecordType</code> is <code>null</code>
     */

    protected void removeTodoSearchRecordType(org.osid.type.Type todoSearchRecordType) {
        this.todoSearchRecordTypes.remove(todoSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Checklist </code> record types. 
     *
     *  @return a list containing the supported checklist record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChecklistRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.checklistRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Checklist </code> record type is supported. 
     *
     *  @param  checklistRecordType a <code> Type </code> indicating a <code> 
     *          Checklist </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> checklistRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChecklistRecordType(org.osid.type.Type checklistRecordType) {
        return (this.checklistRecordTypes.contains(checklistRecordType));
    }


    /**
     *  Adds support for a checklist record type.
     *
     *  @param checklistRecordType a checklist record type
     *  @throws org.osid.NullArgumentException
     *  <code>checklistRecordType</code> is <code>null</code>
     */

    protected void addChecklistRecordType(org.osid.type.Type checklistRecordType) {
        this.checklistRecordTypes.add(checklistRecordType);
        return;
    }


    /**
     *  Removes support for a checklist record type.
     *
     *  @param checklistRecordType a checklist record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>checklistRecordType</code> is <code>null</code>
     */

    protected void removeChecklistRecordType(org.osid.type.Type checklistRecordType) {
        this.checklistRecordTypes.remove(checklistRecordType);
        return;
    }


    /**
     *  Gets the supported checklist search record types. 
     *
     *  @return a list containing the supported checklist search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChecklistSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.checklistSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given checklist search record type is supported. 
     *
     *  @param  checklistSearchRecordType a <code> Type </code> indicating a 
     *          checklist record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          checklistSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChecklistSearchRecordType(org.osid.type.Type checklistSearchRecordType) {
        return (this.checklistSearchRecordTypes.contains(checklistSearchRecordType));
    }


    /**
     *  Adds support for a checklist search record type.
     *
     *  @param checklistSearchRecordType a checklist search record type
     *  @throws org.osid.NullArgumentException
     *  <code>checklistSearchRecordType</code> is <code>null</code>
     */

    protected void addChecklistSearchRecordType(org.osid.type.Type checklistSearchRecordType) {
        this.checklistSearchRecordTypes.add(checklistSearchRecordType);
        return;
    }


    /**
     *  Removes support for a checklist search record type.
     *
     *  @param checklistSearchRecordType a checklist search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>checklistSearchRecordType</code> is <code>null</code>
     */

    protected void removeChecklistSearchRecordType(org.osid.type.Type checklistSearchRecordType) {
        this.checklistSearchRecordTypes.remove(checklistSearchRecordType);
        return;
    }


    /**
     *  Gets the supported priority types. 
     *
     *  @return a list containing the supported priority types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriorityTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.priorityTypes.toCollection()));
    }


    /**
     *  Tests if the given priority type is supported. 
     *
     *  @param  priorityType a <code> Type </code> indicating a priority type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriorityType(org.osid.type.Type priorityType) {
        return (this.priorityTypes.contains(priorityType));
    }


    /**
     *  Adds support for a priority type.
     *
     *  @param priorityType a priority type
     *  @throws org.osid.NullArgumentException
     *  <code>priorityType</code> is <code>null</code>
     */

    protected void addPriorityType(org.osid.type.Type priorityType) {
        this.priorityTypes.add(priorityType);
        return;
    }


    /**
     *  Removes support for a priority type.
     *
     *  @param priorityType a priority type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>priorityType</code> is <code>null</code>
     */

    protected void removePriorityType(org.osid.type.Type priorityType) {
        this.priorityTypes.remove(priorityType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo lookup 
     *  service. 
     *
     *  @return a <code> TodoLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoLookupSession getTodoLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoLookupSession getTodoLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo lookup 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoLookupSession getTodoLookupSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoLookupSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo lookup 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoLookupSession getTodoLookupSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoLookupSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo query 
     *  service. 
     *
     *  @return a <code> TodoQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuerySession getTodoQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuerySession getTodoQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo query 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuerySession getTodoQuerySessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoQuerySessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo query 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuerySession getTodoQuerySessionForChecklist(org.osid.id.Id checklistId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoQuerySessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo search 
     *  service. 
     *
     *  @return a <code> TodoSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSearchSession getTodoSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSearchSession getTodoSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo search 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSearchSession getTodoSearchSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoSearchSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo search 
     *  service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSearchSession getTodoSearchSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoSearchSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  administration service. 
     *
     *  @return a <code> TodoAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoAdminSession getTodoAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoAdminSession getTodoAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  administration service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoAdminSession getTodoAdminSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoAdminSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  administration service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoAdminSession getTodoAdminSessionForChecklist(org.osid.id.Id checklistId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoAdminSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  notification service. 
     *
     *  @param  todoReceiver the receiver 
     *  @return a <code> TodoNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> todoReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoNotificationSession getTodoNotificationSession(org.osid.checklist.TodoReceiver todoReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  notification service. 
     *
     *  @param  todoReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> TodoNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> todoReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoNotificationSession getTodoNotificationSession(org.osid.checklist.TodoReceiver todoReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  notification service for the given checklist. 
     *
     *  @param  todoReceiver the receiver 
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> todoReceiver </code> or 
     *          <code> checklistId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoNotificationSession getTodoNotificationSessionForChecklist(org.osid.checklist.TodoReceiver todoReceiver, 
                                                                                             org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoNotificationSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo 
     *  notification service for the given checklist. 
     *
     *  @param  todoReceiver the receiver 
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Todo </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> todoReceiver, 
     *          checklistId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoNotificationSession getTodoNotificationSessionForChecklist(org.osid.checklist.TodoReceiver todoReceiver, 
                                                                                             org.osid.id.Id checklistId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoNotificationSessionForChecklist not implemented");
    }


    /**
     *  Gets the todo hierarchy traversal session. 
     *
     *  @return <code> a TodoHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchySession getTodoHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoHierarchySession not implemented");
    }


    /**
     *  Gets the todo hierarchy traversal session. 
     *
     *  @param  proxy proxy 
     *  @return <code> a TodoHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchySession getTodoHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoHierarchySession not implemented");
    }


    /**
     *  Gets the todo hierarchy traversal session for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return <code> a TodoHierarchySession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoHierarchy() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchySession getTodoHierarchySessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoHierarchySessionForChecklist not implemented");
    }


    /**
     *  Gets the todo hierarchy traversal session for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy proxy 
     *  @return <code> a TodoHierarchySession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoHierarchy() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchySession getTodoHierarchySessionForChecklist(org.osid.id.Id checklistId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoHierarchySessionForChecklist not implemented");
    }


    /**
     *  Gets the todo hierarchy design session. 
     *
     *  @return a <code> TodoHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchyDesignSession getTodoHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the todo hierarchy design session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TodoHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchyDesignSession getTodoHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the todo hierarchy design session for the given checklist. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchyDesignSession getTodoHierarchyDesignSessionForChecklist(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoHierarchyDesignSessionForChecklist not implemented");
    }


    /**
     *  Gets the todo hierarchy design session for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> TodoHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoHierarchyDesignSession getTodoHierarchyDesignSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoHierarchyDesignSessionForChecklist not implemented");
    }


    /**
     *  Gets the session for retrieving todo to checklist mappings. 
     *
     *  @return a <code> TodoChecklistSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoChecklist() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoChecklistSession getTodoChecklistSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoChecklistSession not implemented");
    }


    /**
     *  Gets the session for retrieving todo to checklist mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoChecklistSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTodoChecklist() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoChecklistSession getTodoChecklistSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoChecklistSession not implemented");
    }


    /**
     *  Gets the session for assigning todo to checklist mappings. 
     *
     *  @return a <code> TodoChecklistAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoChecklistAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoChecklistAssignmentSession getTodoChecklistAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoChecklistAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning todo to checklist mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoChecklistAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoChecklistAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoChecklistAssignmentSession getTodoChecklistAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoChecklistAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the todo smart checklist for the 
     *  given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the checklist 
     *  @return a <code> TodoSmartChecklistSession </code> 
     *  @throws org.osid.NotFoundException <code> checklistId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoSmartChecklist() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSmartChecklistSession getTodoSmartChecklistSession(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getTodoSmartChecklistSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic todo checklists for the given 
     *  checklist. 
     *
     *  @param  checklistId the <code> Id </code> of a checklist 
     *  @param  proxy a proxy 
     *  @return <code> checklistId </code> not found 
     *  @throws org.osid.NotFoundException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoSmartChecklist() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoSmartChecklistSession getTodoSmartChecklistSession(org.osid.id.Id checklistId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getTodoSmartChecklistSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  lookup service. 
     *
     *  @return a <code> ChecklistLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistLookupSession getChecklistLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistLookupSession getChecklistLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  query service. 
     *
     *  @return a <code> ChecklistQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuerySession getChecklistQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistQueryh() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuerySession getChecklistQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  search service. 
     *
     *  @return a <code> ChecklistSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistSearchSession getChecklistSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistSearchSession getChecklistSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  administrative service. 
     *
     *  @return a <code> ChecklistAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistAdminSession getChecklistAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistAdminSession getChecklistAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  notification service. 
     *
     *  @param  checklistReceiver the receiver 
     *  @return a <code> ChecklistNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> checklistReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistNotificationSession getChecklistNotificationSession(org.osid.checklist.ChecklistReceiver checklistReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  notification service. 
     *
     *  @param  checklistReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> checklistReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistNotificationSession getChecklistNotificationSession(org.osid.checklist.ChecklistReceiver checklistReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  hierarchy service. 
     *
     *  @return a <code> ChecklistHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistHierarchySession getChecklistHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistHierarchySession getChecklistHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  hierarchy design service. 
     *
     *  @return a <code> ChecklistHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistHierarchyDesignSession getChecklistHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the checklist 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ChecklistHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistHierarchyDesignSession getChecklistHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> ChecklistBatchManager. </code> 
     *
     *  @return a <code> ChecklistBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.batch.ChecklistBatchManager getChecklistBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistBatchManager not implemented");
    }


    /**
     *  Gets a <code> ChecklistBatchProxyManager. </code> 
     *
     *  @return a <code> ChecklistBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.batch.ChecklistBatchProxyManager getChecklistBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> ChecklistMasonManager. </code> 
     *
     *  @return a <code> ChecklistMasonManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistMason() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.ChecklistMasonManager getChecklistMasonManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistManager.getChecklistMasonManager not implemented");
    }


    /**
     *  Gets a <code> ChecklistMasonProxyManager. </code> 
     *
     *  @return a <code> ChecklistMasonProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistMason() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.ChecklistMasonProxyManager getChecklistMasonProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.ChecklistProxyManager.getChecklistMasonProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.todoRecordTypes.clear();
        this.todoRecordTypes.clear();

        this.todoSearchRecordTypes.clear();
        this.todoSearchRecordTypes.clear();

        this.checklistRecordTypes.clear();
        this.checklistRecordTypes.clear();

        this.checklistSearchRecordTypes.clear();
        this.checklistSearchRecordTypes.clear();

        this.priorityTypes.clear();
        this.priorityTypes.clear();

        return;
    }
}
