//
// AbstractAdapterRepositoryLookupSession.java
//
//    A Repository lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.repository.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Repository lookup session adapter.
 */

public abstract class AbstractAdapterRepositoryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.repository.RepositoryLookupSession {

    private final org.osid.repository.RepositoryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRepositoryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRepositoryLookupSession(org.osid.repository.RepositoryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Repository} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRepositories() {
        return (this.session.canLookupRepositories());
    }


    /**
     *  A complete view of the {@code Repository} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRepositoryView() {
        this.session.useComparativeRepositoryView();
        return;
    }


    /**
     *  A complete view of the {@code Repository} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRepositoryView() {
        this.session.usePlenaryRepositoryView();
        return;
    }

     
    /**
     *  Gets the {@code Repository} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Repository} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Repository} and
     *  retained for compatibility.
     *
     *  @param repositoryId {@code Id} of the {@code Repository}
     *  @return the repository
     *  @throws org.osid.NotFoundException {@code repositoryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code repositoryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepository(repositoryId));
    }


    /**
     *  Gets a {@code RepositoryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  repositories specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Repositories} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  repositoryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Repository} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code repositoryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByIds(org.osid.id.IdList repositoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepositoriesByIds(repositoryIds));
    }


    /**
     *  Gets a {@code RepositoryList} corresponding to the given
     *  repository genus {@code Type} which does not include
     *  repositories of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned {@code Repository} list
     *  @throws org.osid.NullArgumentException
     *          {@code repositoryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepositoriesByGenusType(repositoryGenusType));
    }


    /**
     *  Gets a {@code RepositoryList} corresponding to the given
     *  repository genus {@code Type} and include any additional
     *  repositories with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned {@code Repository} list
     *  @throws org.osid.NullArgumentException
     *          {@code repositoryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByParentGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepositoriesByParentGenusType(repositoryGenusType));
    }


    /**
     *  Gets a {@code RepositoryList} containing the given
     *  repository record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryRecordType a repository record type 
     *  @return the returned {@code Repository} list
     *  @throws org.osid.NullArgumentException
     *          {@code repositoryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByRecordType(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepositoriesByRecordType(repositoryRecordType));
    }


    /**
     *  Gets a {@code RepositoryList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Repository} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepositoriesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Repositories}. 
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Repositories} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepositories());
    }
}
