//
// AbstractFederatingAcademyLookupSession.java
//
//     An abstract federating adapter for an AcademyLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AcademyLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAcademyLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.recognition.AcademyLookupSession>
    implements org.osid.recognition.AcademyLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingAcademyLookupSession</code>.
     */

    protected AbstractFederatingAcademyLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.recognition.AcademyLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Academy</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAcademies() {
        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            if (session.canLookupAcademies()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Academy</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAcademyView() {
        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            session.useComparativeAcademyView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Academy</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAcademyView() {
        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            session.usePlenaryAcademyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Academy</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Academy</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Academy</code> and
     *  retained for compatibility.
     *
     *  @param  academyId <code>Id</code> of the
     *          <code>Academy</code>
     *  @return the academy
     *  @throws org.osid.NotFoundException <code>academyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>academyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            try {
                return (session.getAcademy(academyId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(academyId + " not found");
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  academies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Academies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  academyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>academyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByIds(org.osid.id.IdList academyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.recognition.academy.MutableAcademyList ret = new net.okapia.osid.jamocha.recognition.academy.MutableAcademyList();

        try (org.osid.id.IdList ids = academyIds) {
            while (ids.hasNext()) {
                ret.addAcademy(getAcademy(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  academy genus <code>Type</code> which does not include
     *  academies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.academy.FederatingAcademyList ret = getAcademyList();

        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            ret.addAcademyList(session.getAcademiesByGenusType(academyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  academy genus <code>Type</code> and include any additional
     *  academies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByParentGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.academy.FederatingAcademyList ret = getAcademyList();

        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            ret.addAcademyList(session.getAcademiesByParentGenusType(academyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AcademyList</code> containing the given
     *  academy record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyRecordType an academy record type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByRecordType(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.academy.FederatingAcademyList ret = getAcademyList();

        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            ret.addAcademyList(session.getAcademiesByRecordType(academyRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AcademyList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known academies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  academies that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Academy</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.recognition.academy.FederatingAcademyList ret = getAcademyList();

        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            ret.addAcademyList(session.getAcademiesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Academies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Academies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.recognition.academy.FederatingAcademyList ret = getAcademyList();

        for (org.osid.recognition.AcademyLookupSession session : getSessions()) {
            ret.addAcademyList(session.getAcademies());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.recognition.academy.FederatingAcademyList getAcademyList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.academy.ParallelAcademyList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.recognition.academy.CompositeAcademyList());
        }
    }
}
