//
// AbstractAdapterPoolProcessorLookupSession.java
//
//    A PoolProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A PoolProcessor lookup session adapter.
 */

public abstract class AbstractAdapterPoolProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.PoolProcessorLookupSession {

    private final org.osid.provisioning.rules.PoolProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPoolProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPoolProcessorLookupSession(org.osid.provisioning.rules.PoolProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code PoolProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPoolProcessors() {
        return (this.session.canLookupPoolProcessors());
    }


    /**
     *  A complete view of the {@code PoolProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolProcessorView() {
        this.session.useComparativePoolProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code PoolProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolProcessorView() {
        this.session.usePlenaryPoolProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool processors in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pool processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolProcessorView() {
        this.session.useActivePoolProcessorView();
        return;
    }


    /**
     *  Active and inactive pool processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolProcessorView() {
        this.session.useAnyStatusPoolProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code PoolProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code PoolProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code PoolProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, pool processors are returned that are currently
     *  active. In any status mode, active and inactive pool processors
     *  are returned.
     *
     *  @param poolProcessorId {@code Id} of the {@code PoolProcessor}
     *  @return the pool processor
     *  @throws org.osid.NotFoundException {@code poolProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code poolProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessor getPoolProcessor(org.osid.id.Id poolProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessor(poolProcessorId));
    }


    /**
     *  Gets a {@code PoolProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  poolProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code PoolProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, pool processors are returned that are currently
     *  active. In any status mode, active and inactive pool processors
     *  are returned.
     *
     *  @param  poolProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code PoolProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessorsByIds(org.osid.id.IdList poolProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorsByIds(poolProcessorIds));
    }


    /**
     *  Gets a {@code PoolProcessorList} corresponding to the given
     *  pool processor genus {@code Type} which does not include
     *  pool processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  pool processors or an error results. Otherwise, the returned list
     *  may contain only those pool processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processors are returned that are currently
     *  active. In any status mode, active and inactive pool processors
     *  are returned.
     *
     *  @param  poolProcessorGenusType a poolProcessor genus type 
     *  @return the returned {@code PoolProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessorsByGenusType(org.osid.type.Type poolProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorsByGenusType(poolProcessorGenusType));
    }


    /**
     *  Gets a {@code PoolProcessorList} corresponding to the given
     *  pool processor genus {@code Type} and include any additional
     *  pool processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  pool processors or an error results. Otherwise, the returned list
     *  may contain only those pool processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processors are returned that are currently
     *  active. In any status mode, active and inactive pool processors
     *  are returned.
     *
     *  @param  poolProcessorGenusType a poolProcessor genus type 
     *  @return the returned {@code PoolProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessorsByParentGenusType(org.osid.type.Type poolProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorsByParentGenusType(poolProcessorGenusType));
    }


    /**
     *  Gets a {@code PoolProcessorList} containing the given
     *  pool processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pool processors or an error results. Otherwise, the returned list
     *  may contain only those pool processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processors are returned that are currently
     *  active. In any status mode, active and inactive pool processors
     *  are returned.
     *
     *  @param  poolProcessorRecordType a poolProcessor record type 
     *  @return the returned {@code PoolProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessorsByRecordType(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorsByRecordType(poolProcessorRecordType));
    }


    /**
     *  Gets all {@code PoolProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  pool processors or an error results. Otherwise, the returned list
     *  may contain only those pool processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processors are returned that are currently
     *  active. In any status mode, active and inactive pool processors
     *  are returned.
     *
     *  @return a list of {@code PoolProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorList getPoolProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessors());
    }
}
