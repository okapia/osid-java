//
// AbstractIssueQuery.java
//
//     A template for making an Issue Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.issue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for issues.
 */

public abstract class AbstractIssueQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.hold.IssueQuery {

    private final java.util.Collection<org.osid.hold.records.IssueQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBureauId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBureauIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBureauQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getBureauQuery() {
        throw new org.osid.UnimplementedException("supportsBureauQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearBureauTerms() {
        return;
    }


    /**
     *  Sets the block <code> Id </code> for this query. 
     *
     *  @param  blockId the block <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockId(org.osid.id.Id blockId, boolean match) {
        return;
    }


    /**
     *  Clears the block <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBlockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BlockQuery </code> is available. 
     *
     *  @return <code> true </code> if a block query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a block. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the block query 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuery getBlockQuery() {
        throw new org.osid.UnimplementedException("supportsBlockQuery() is false");
    }


    /**
     *  Matches issues used for any block. 
     *
     *  @param  match <code> true </code> to match issues with any blocks, 
     *          <code> false </code> to match issues with no blocks 
     */

    @OSID @Override
    public void matchAnyBlock(boolean match) {
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearBlockTerms() {
        return;
    }


    /**
     *  Sets the hold <code> Id </code> for this query. 
     *
     *  @param  holdId the hold <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> holdId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchHoldId(org.osid.id.Id holdId, boolean match) {
        return;
    }


    /**
     *  Clears the hold <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearHoldIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> HoldQuery </code> is available. 
     *
     *  @return <code> true </code> if a hold query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldQuery() {
        return (false);
    }


    /**
     *  Gets the query for a hold. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the hold query 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuery getHoldQuery() {
        throw new org.osid.UnimplementedException("supportsHoldQuery() is false");
    }


    /**
     *  Matches issues referenced by any hold. 
     *
     *  @param  match <code> true </code> to match issues with any holds, 
     *          <code> false </code> to match issues with no holds 
     */

    @OSID @Override
    public void matchAnyHold(boolean match) {
        return;
    }


    /**
     *  Clears the hold query terms. 
     */

    @OSID @Override
    public void clearHoldTerms() {
        return;
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match issues 
     *  assigned to foundries. 
     *
     *  @param  oublietteId the oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOublietteId(org.osid.id.Id oublietteId, boolean match) {
        return;
    }


    /**
     *  Clears the oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOublietteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsOublietteQuery() is false");
    }


    /**
     *  Clears the oubliette query terms. 
     */

    @OSID @Override
    public void clearOublietteTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given issue query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an issue implementing the requested record.
     *
     *  @param issueRecordType an issue record type
     *  @return the issue query record
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.IssueQueryRecord getIssueQueryRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.IssueQueryRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this issue query. 
     *
     *  @param issueQueryRecord issue query record
     *  @param issueRecordType issue record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addIssueQueryRecord(org.osid.hold.records.IssueQueryRecord issueQueryRecord, 
                                          org.osid.type.Type issueRecordType) {

        addRecordType(issueRecordType);
        nullarg(issueQueryRecord, "issue query record");
        this.records.add(issueQueryRecord);        
        return;
    }
}
