//
// AbstractOfferingConstrainerEnablerSearch.java
//
//     A template for making an OfferingConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing offering constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOfferingConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.rules.OfferingConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.rules.OfferingConstrainerEnablerSearchOrder offeringConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of offering constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  offeringConstrainerEnablerIds list of offering constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOfferingConstrainerEnablers(org.osid.id.IdList offeringConstrainerEnablerIds) {
        while (offeringConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(offeringConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOfferingConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of offering constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOfferingConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  offeringConstrainerEnablerSearchOrder offering constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>offeringConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOfferingConstrainerEnablerResults(org.osid.offering.rules.OfferingConstrainerEnablerSearchOrder offeringConstrainerEnablerSearchOrder) {
	this.offeringConstrainerEnablerSearchOrder = offeringConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.rules.OfferingConstrainerEnablerSearchOrder getOfferingConstrainerEnablerSearchOrder() {
	return (this.offeringConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given offering constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offering constrainer enabler implementing the requested record.
     *
     *  @param offeringConstrainerEnablerSearchRecordType an offering constrainer enabler search record
     *         type
     *  @return the offering constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerEnablerSearchRecord getOfferingConstrainerEnablerSearchRecord(org.osid.type.Type offeringConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.rules.records.OfferingConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering constrainer enabler search. 
     *
     *  @param offeringConstrainerEnablerSearchRecord offering constrainer enabler search record
     *  @param offeringConstrainerEnablerSearchRecordType offeringConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingConstrainerEnablerSearchRecord(org.osid.offering.rules.records.OfferingConstrainerEnablerSearchRecord offeringConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type offeringConstrainerEnablerSearchRecordType) {

        addRecordType(offeringConstrainerEnablerSearchRecordType);
        this.records.add(offeringConstrainerEnablerSearchRecord);        
        return;
    }
}
