//
// MutableIndexedMapProxyProfileEntryEnablerLookupSession
//
//    Implements a ProfileEntryEnabler lookup service backed by a collection of
//    profileEntryEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.rules;


/**
 *  Implements a ProfileEntryEnabler lookup service backed by a collection of
 *  profileEntryEnablers. The profile entry enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some profileEntryEnablers may be compatible
 *  with more types than are indicated through these profileEntryEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of profile entry enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyProfileEntryEnablerLookupSession
    extends net.okapia.osid.jamocha.core.profile.rules.spi.AbstractIndexedMapProfileEntryEnablerLookupSession
    implements org.osid.profile.rules.ProfileEntryEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProfileEntryEnablerLookupSession} with
     *  no profile entry enabler.
     *
     *  @param profile the profile
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProfileEntryEnablerLookupSession(org.osid.profile.Profile profile,
                                                       org.osid.proxy.Proxy proxy) {
        setProfile(profile);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProfileEntryEnablerLookupSession} with
     *  a single profile entry enabler.
     *
     *  @param profile the profile
     *  @param  profileEntryEnabler an profile entry enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile},
     *          {@code profileEntryEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProfileEntryEnablerLookupSession(org.osid.profile.Profile profile,
                                                       org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler, org.osid.proxy.Proxy proxy) {

        this(profile, proxy);
        putProfileEntryEnabler(profileEntryEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProfileEntryEnablerLookupSession} using
     *  an array of profile entry enablers.
     *
     *  @param profile the profile
     *  @param  profileEntryEnablers an array of profile entry enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile},
     *          {@code profileEntryEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProfileEntryEnablerLookupSession(org.osid.profile.Profile profile,
                                                       org.osid.profile.rules.ProfileEntryEnabler[] profileEntryEnablers, org.osid.proxy.Proxy proxy) {

        this(profile, proxy);
        putProfileEntryEnablers(profileEntryEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyProfileEntryEnablerLookupSession} using
     *  a collection of profile entry enablers.
     *
     *  @param profile the profile
     *  @param  profileEntryEnablers a collection of profile entry enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile},
     *          {@code profileEntryEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyProfileEntryEnablerLookupSession(org.osid.profile.Profile profile,
                                                       java.util.Collection<? extends org.osid.profile.rules.ProfileEntryEnabler> profileEntryEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(profile, proxy);
        putProfileEntryEnablers(profileEntryEnablers);
        return;
    }

    
    /**
     *  Makes a {@code ProfileEntryEnabler} available in this session.
     *
     *  @param  profileEntryEnabler a profile entry enabler
     *  @throws org.osid.NullArgumentException {@code profileEntryEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putProfileEntryEnabler(org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler) {
        super.putProfileEntryEnabler(profileEntryEnabler);
        return;
    }


    /**
     *  Makes an array of profile entry enablers available in this session.
     *
     *  @param  profileEntryEnablers an array of profile entry enablers
     *  @throws org.osid.NullArgumentException {@code profileEntryEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putProfileEntryEnablers(org.osid.profile.rules.ProfileEntryEnabler[] profileEntryEnablers) {
        super.putProfileEntryEnablers(profileEntryEnablers);
        return;
    }


    /**
     *  Makes collection of profile entry enablers available in this session.
     *
     *  @param  profileEntryEnablers a collection of profile entry enablers
     *  @throws org.osid.NullArgumentException {@code profileEntryEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putProfileEntryEnablers(java.util.Collection<? extends org.osid.profile.rules.ProfileEntryEnabler> profileEntryEnablers) {
        super.putProfileEntryEnablers(profileEntryEnablers);
        return;
    }


    /**
     *  Removes a ProfileEntryEnabler from this session.
     *
     *  @param profileEntryEnablerId the {@code Id} of the profile entry enabler
     *  @throws org.osid.NullArgumentException {@code profileEntryEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProfileEntryEnabler(org.osid.id.Id profileEntryEnablerId) {
        super.removeProfileEntryEnabler(profileEntryEnablerId);
        return;
    }    
}
