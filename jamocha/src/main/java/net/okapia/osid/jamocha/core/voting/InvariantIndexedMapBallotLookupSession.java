//
// InvariantIndexedMapBallotLookupSession
//
//    Implements a Ballot lookup service backed by a fixed
//    collection of ballots indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Ballot lookup service backed by a fixed
 *  collection of ballots. The ballots are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some ballots may be compatible
 *  with more types than are indicated through these ballot
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapBallotLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractIndexedMapBallotLookupSession
    implements org.osid.voting.BallotLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapBallotLookupSession} using an
     *  array of ballots.
     *
     *  @param polls the polls
     *  @param ballots an array of ballots
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballots} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapBallotLookupSession(org.osid.voting.Polls polls,
                                                    org.osid.voting.Ballot[] ballots) {

        setPolls(polls);
        putBallots(ballots);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapBallotLookupSession} using a
     *  collection of ballots.
     *
     *  @param polls the polls
     *  @param ballots a collection of ballots
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballots} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapBallotLookupSession(org.osid.voting.Polls polls,
                                                    java.util.Collection<? extends org.osid.voting.Ballot> ballots) {

        setPolls(polls);
        putBallots(ballots);
        return;
    }
}
