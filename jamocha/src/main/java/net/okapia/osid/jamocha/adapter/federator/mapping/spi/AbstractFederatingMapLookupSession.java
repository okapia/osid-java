//
// AbstractFederatingMapLookupSession.java
//
//     An abstract federating adapter for a MapLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  MapLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingMapLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.MapLookupSession>
    implements org.osid.mapping.MapLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingMapLookupSession</code>.
     */

    protected AbstractFederatingMapLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.MapLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Map</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupMaps() {
        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            if (session.canLookupMaps()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Map</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMapView() {
        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            session.useComparativeMapView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Map</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMapView() {
        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            session.usePlenaryMapView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Map</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Map</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Map</code> and
     *  retained for compatibility.
     *
     *  @param  mapId <code>Id</code> of the
     *          <code>Map</code>
     *  @return the map
     *  @throws org.osid.NotFoundException <code>mapId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>mapId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            try {
                return (session.getMap(mapId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(mapId + " not found");
    }


    /**
     *  Gets a <code>MapList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  maps specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Maps</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  mapIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>mapIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByIds(org.osid.id.IdList mapIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.map.MutableMapList ret = new net.okapia.osid.jamocha.mapping.map.MutableMapList();

        try (org.osid.id.IdList ids = mapIds) {
            while (ids.hasNext()) {
                ret.addMap(getMap(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>MapList</code> corresponding to the given
     *  map genus <code>Type</code> which does not include
     *  maps of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapGenusType a map genus type 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByGenusType(org.osid.type.Type mapGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.map.FederatingMapList ret = getMapList();

        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            ret.addMapList(session.getMapsByGenusType(mapGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MapList</code> corresponding to the given
     *  map genus <code>Type</code> and include any additional
     *  maps with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapGenusType a map genus type 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByParentGenusType(org.osid.type.Type mapGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.map.FederatingMapList ret = getMapList();

        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            ret.addMapList(session.getMapsByParentGenusType(mapGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MapList</code> containing the given
     *  map record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  mapRecordType a map record type 
     *  @return the returned <code>Map</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByRecordType(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.map.FederatingMapList ret = getMapList();

        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            ret.addMapList(session.getMapsByRecordType(mapRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>MapList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known maps or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  maps that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Map</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMapsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.mapping.map.FederatingMapList ret = getMapList();

        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            ret.addMapList(session.getMapsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Maps</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  maps or an error results. Otherwise, the returned list
     *  may contain only those maps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Maps</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMaps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.map.FederatingMapList ret = getMapList();

        for (org.osid.mapping.MapLookupSession session : getSessions()) {
            ret.addMapList(session.getMaps());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.map.FederatingMapList getMapList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.map.ParallelMapList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.map.CompositeMapList());
        }
    }
}
