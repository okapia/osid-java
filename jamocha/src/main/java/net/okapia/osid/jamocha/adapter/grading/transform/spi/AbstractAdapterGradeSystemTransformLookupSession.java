//
// AbstractAdapterGradeSystemTransformLookupSession.java
//
//    A GradeSystemTransform lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.transform.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A GradeSystemTransform lookup session adapter.
 */

public abstract class AbstractAdapterGradeSystemTransformLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {

    private final org.osid.grading.transform.GradeSystemTransformLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterGradeSystemTransformLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradeSystemTransformLookupSession(org.osid.grading.transform.GradeSystemTransformLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Gradebook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Gradebook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the {@code Gradebook} associated with this session.
     *
     *  @return the {@code Gradebook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform {@code GradeSystemTransform} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupGradeSystemTransforms() {
        return (this.session.canLookupGradeSystemTransforms());
    }


    /**
     *  A complete view of the {@code GradeSystemTransform} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeSystemTransformView() {
        this.session.useComparativeGradeSystemTransformView();
        return;
    }


    /**
     *  A complete view of the {@code GradeSystemTransform} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeSystemTransformView() {
        this.session.usePlenaryGradeSystemTransformView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade system transforms in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    

    /**
     *  Only active grade system transforms are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveGradeSystemTransformView() {
        this.session.useActiveGradeSystemTransformView();
        return;
    }


    /**
     *  Active and inactive grade system transforms are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusGradeSystemTransformView() {
        this.session.useAnyStatusGradeSystemTransformView();
        return;
    }
    
     
    /**
     *  Gets the {@code GradeSystemTransform} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code GradeSystemTransform} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code GradeSystemTransform} and
     *  retained for compatibility.
     *
     *  In active mode, grade system transforms are returned that are currently
     *  active. In any status mode, active and inactive grade system transforms
     *  are returned.
     *
     *  @param gradeSystemTransformId {@code Id} of the {@code GradeSystemTransform}
     *  @return the grade system transform
     *  @throws org.osid.NotFoundException {@code gradeSystemTransformId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransformId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransform getGradeSystemTransform(org.osid.id.Id gradeSystemTransformId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransform(gradeSystemTransformId));
    }


    /**
     *  Gets a {@code GradeSystemTransformList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeSystemTransforms specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code GradeSystemTransforms} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, grade system transforms are returned that are currently
     *  active. In any status mode, active and inactive grade system transforms
     *  are returned.
     *
     *  @param  gradeSystemTransformIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code GradeSystemTransform} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemTransformIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByIds(org.osid.id.IdList gradeSystemTransformIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransformsByIds(gradeSystemTransformIds));
    }


    /**
     *  Gets a {@code GradeSystemTransformList} corresponding to the given
     *  grade system transform genus {@code Type} which does not include
     *  grade system transforms of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade system transforms or an error results. Otherwise, the returned list
     *  may contain only those grade system transforms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, grade system transforms are returned that are currently
     *  active. In any status mode, active and inactive grade system transforms
     *  are returned.
     *
     *  @param  gradeSystemTransformGenusType a gradeSystemTransform genus type 
     *  @return the returned {@code GradeSystemTransform} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemTransformGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByGenusType(org.osid.type.Type gradeSystemTransformGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransformsByGenusType(gradeSystemTransformGenusType));
    }


    /**
     *  Gets a {@code GradeSystemTransformList} corresponding to the given
     *  grade system transform genus {@code Type} and include any additional
     *  grade system transforms with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  grade system transforms or an error results. Otherwise, the returned list
     *  may contain only those grade system transforms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, grade system transforms are returned that are currently
     *  active. In any status mode, active and inactive grade system transforms
     *  are returned.
     *
     *  @param  gradeSystemTransformGenusType a gradeSystemTransform genus type 
     *  @return the returned {@code GradeSystemTransform} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemTransformGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByParentGenusType(org.osid.type.Type gradeSystemTransformGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransformsByParentGenusType(gradeSystemTransformGenusType));
    }


    /**
     *  Gets a {@code GradeSystemTransformList} containing the given
     *  grade system transform record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  grade system transforms or an error results. Otherwise, the returned list
     *  may contain only those grade system transforms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, grade system transforms are returned that are currently
     *  active. In any status mode, active and inactive grade system transforms
     *  are returned.
     *
     *  @param  gradeSystemTransformRecordType a gradeSystemTransform record type 
     *  @return the returned {@code GradeSystemTransform} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemTransformRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsByRecordType(org.osid.type.Type gradeSystemTransformRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransformsByRecordType(gradeSystemTransformRecordType));
    }


    /**
     *  Gets the grade system transforms from the source grade
     *  system. In plenary mode, the returned list contains all known
     *  grade system transforms or an error results. Otherwise, the
     *  returned list may contain only those grade system transforms
     *  that are accessible through this session.
     *
     *  @param  sourceGradeSystemId the source grade system 
     *  @return the returned {@code GradeSystemTransform} list 
     *  @throws org.osid.NotFoundException {@code sourceGradeSystemId}
     *          not found
     *  @throws org.osid.NullArgumentException {@code
     *         sourceGradeSystemId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransformsBySource(org.osid.id.Id sourceGradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransformsBySource(sourceGradeSystemId));
    }


    /**
     *  Gets a grade system transform by its source and target grade
     *  systems.  In plenary mode, the returned list contains all
     *  known grade system transforms or an error results. Otherwise,
     *  the returned list may contain only those grade system
     *  transforms that are accessible through this session.
     *
     *  @param  sourceGradeSystemId the source grade system 
     *  @param  targetGradeSystemId the target grade system 
     *  @return the returned {@code GradeSystemTransform} 
     *  @throws org.osid.NotFoundException transform not found 
     *  @throws org.osid.NullArgumentException {@code
     *         sourceGradeSystemId} or {@code targetGradeSystemId} is
     *         {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransform getGradeSystemTransformBySystems(org.osid.id.Id sourceGradeSystemId, 
                                                                                            org.osid.id.Id targetGradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransformBySystems(sourceGradeSystemId, targetGradeSystemId));
    }
    
    
    /**
     *  Gets all {@code GradeSystemTransforms}. 
     *
     *  In plenary mode, the returned list contains all known grade
     *  system transforms or an error results. Otherwise, the returned
     *  list may contain only those grade system transforms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, grade system transforms are returned that are
     *  currently active. In any status mode, active and inactive
     *  grade system transforms are returned.
     *
     *  @return a list of {@code GradeSystemTransforms} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformList getGradeSystemTransforms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemTransforms());
    }
}
