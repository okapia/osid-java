//
// InvariantIndexedMapOfferingConstrainerEnablerLookupSession
//
//    Implements an OfferingConstrainerEnabler lookup service backed by a fixed
//    collection of offeringConstrainerEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements an OfferingConstrainerEnabler lookup service backed by a fixed
 *  collection of offering constrainer enablers. The offering constrainer enablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some offering constrainer enablers may be compatible
 *  with more types than are indicated through these offering constrainer enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapOfferingConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractIndexedMapOfferingConstrainerEnablerLookupSession
    implements org.osid.offering.rules.OfferingConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapOfferingConstrainerEnablerLookupSession} using an
     *  array of offeringConstrainerEnablers.
     *
     *  @param catalogue the catalogue
     *  @param offeringConstrainerEnablers an array of offering constrainer enablers
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code offeringConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapOfferingConstrainerEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                    org.osid.offering.rules.OfferingConstrainerEnabler[] offeringConstrainerEnablers) {

        setCatalogue(catalogue);
        putOfferingConstrainerEnablers(offeringConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapOfferingConstrainerEnablerLookupSession} using a
     *  collection of offering constrainer enablers.
     *
     *  @param catalogue the catalogue
     *  @param offeringConstrainerEnablers a collection of offering constrainer enablers
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code offeringConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapOfferingConstrainerEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                    java.util.Collection<? extends org.osid.offering.rules.OfferingConstrainerEnabler> offeringConstrainerEnablers) {

        setCatalogue(catalogue);
        putOfferingConstrainerEnablers(offeringConstrainerEnablers);
        return;
    }
}
