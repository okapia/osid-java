//
// AbstractBankLookupSession.java
//
//    A starter implementation framework for providing a Bank
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Bank
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBanks(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBankLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.BankLookupSession {

    private boolean pedantic = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();
    

    /**
     *  Tests if this user can perform <code>Bank</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBanks() {
        return (true);
    }


    /**
     *  A complete view of the <code>Bank</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBankView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Bank</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBankView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Bank</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Contact</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Contact</code> and retained for
     *  compatibility.
     *
     *  @param bankId <code>Id</code> of the <code>Bank</code>
     *  @return the bank
     *  @throws org.osid.NotFoundException <code>bankId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>bankId</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.assessment.BankList banks = getBanks()) {
            while (banks.hasNext()) {
                org.osid.assessment.Bank bank = banks.getNextBank();
                if (bank.getId().equals(bankId)) {
                    return (bank);
                }
            }
        } 

        throw new org.osid.NotFoundException(bankId + " not found");
    }


    /**
     *  Gets a <code>BankList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  banks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Banks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBanks()</code>.
     *
     *  @param  bankIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Bank</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>bankIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanksByIds(org.osid.id.IdList bankIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.Bank> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = bankIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBank(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("bank " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.assessment.bank.LinkedBankList(ret));
    }


    /**
     *  Gets a <code>BankList</code> corresponding to the given
     *  bank genus <code>Type</code> which does not include
     *  banks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  banks or an error results. Otherwise, the returned list
     *  may contain only those banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBanks()</code>.
     *
     *  @param  bankGenusType a bank genus type 
     *  @return the returned <code>Bank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bankGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanksByGenusType(org.osid.type.Type bankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.bank.BankGenusFilterList(getBanks(), bankGenusType));
    }


    /**
     *  Gets a <code>BankList</code> corresponding to the given
     *  bank genus <code>Type</code> and include any additional
     *  banks with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  banks or an error results. Otherwise, the returned list
     *  may contain only those banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBanks()</code>.
     *
     *  @param  bankGenusType a bank genus type 
     *  @return the returned <code>Bank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bankGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanksByParentGenusType(org.osid.type.Type bankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBanksByGenusType(bankGenusType));
    }


    /**
     *  Gets a <code>BankList</code> containing the given
     *  bank record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  banks or an error results. Otherwise, the returned list
     *  may contain only those banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBanks()</code>.
     *
     *  @param  bankRecordType a bank record type 
     *  @return the returned <code>Bank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanksByRecordType(org.osid.type.Type bankRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.bank.BankRecordFilterList(getBanks(), bankRecordType));
    }


    /**
     *  Gets a <code>BankList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known banks or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  banks that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Bank</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.BankList getBanksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.assessment.bank.BankProviderFilterList(getBanks(), resourceId));
    }


    /**
     *  Gets all <code>Banks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  banks or an error results. Otherwise, the returned list
     *  may contain only those banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Banks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.assessment.BankList getBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the bank list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of banks
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.assessment.BankList filterBanksOnViews(org.osid.assessment.BankList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
