//
// AbstractIndexedMapPeriodLookupSession.java
//
//    A simple framework for providing a Period lookup service
//    backed by a fixed collection of periods with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Period lookup service backed by a
 *  fixed collection of periods. The periods are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some periods may be compatible
 *  with more types than are indicated through these period
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Periods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPeriodLookupSession
    extends AbstractMapPeriodLookupSession
    implements org.osid.billing.PeriodLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.billing.Period> periodsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Period>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.Period> periodsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Period>());


    /**
     *  Makes a <code>Period</code> available in this session.
     *
     *  @param  period a period
     *  @throws org.osid.NullArgumentException <code>period<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPeriod(org.osid.billing.Period period) {
        super.putPeriod(period);

        this.periodsByGenus.put(period.getGenusType(), period);
        
        try (org.osid.type.TypeList types = period.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.periodsByRecord.put(types.getNextType(), period);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a period from this session.
     *
     *  @param periodId the <code>Id</code> of the period
     *  @throws org.osid.NullArgumentException <code>periodId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePeriod(org.osid.id.Id periodId) {
        org.osid.billing.Period period;
        try {
            period = getPeriod(periodId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.periodsByGenus.remove(period.getGenusType());

        try (org.osid.type.TypeList types = period.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.periodsByRecord.remove(types.getNextType(), period);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePeriod(periodId);
        return;
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  period genus <code>Type</code> which does not include
     *  periods of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known periods or an error results. Otherwise,
     *  the returned list may contain only those periods that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.period.ArrayPeriodList(this.periodsByGenus.get(periodGenusType)));
    }


    /**
     *  Gets a <code>PeriodList</code> containing the given
     *  period record <code>Type</code>. In plenary mode, the
     *  returned list contains all known periods or an error
     *  results. Otherwise, the returned list may contain only those
     *  periods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  periodRecordType a period record type 
     *  @return the returned <code>period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByRecordType(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.period.ArrayPeriodList(this.periodsByRecord.get(periodRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.periodsByGenus.clear();
        this.periodsByRecord.clear();

        super.close();

        return;
    }
}
