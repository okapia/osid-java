//
// RequisiteElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.requisite.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RequisiteElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {

    private final net.okapia.osid.jamocha.spi.ContainableElements element = new net.okapia.osid.jamocha.spi.ContainableElements();


    /**
     *  Gets the RequisiteElement Id.
     *
     *  @return the requisite element Id
     */

    public static org.osid.id.Id getRequisiteEntityId() {
        return (makeEntityId("osid.course.requisite.Requisite"));
    }


    /**
     *  Gets the RequisiteOptions element Id.
     *
     *  @return the RequisiteOptions element Id
     */

    public static org.osid.id.Id getRequisiteOptions() {
        return (makeElementId("osid.course.requisite.requisite.RequisiteOptions"));
    }


    /**
     *  Gets the CourseRequirements element Id.
     *
     *  @return the CourseRequirements element Id
     */

    public static org.osid.id.Id getCourseRequirements() {
        return (makeElementId("osid.course.requisite.requisite.CourseRequirements"));
    }


    /**
     *  Gets the ProgramRequirements element Id.
     *
     *  @return the ProgramRequirements element Id
     */

    public static org.osid.id.Id getProgramRequirements() {
        return (makeElementId("osid.course.requisite.requisite.ProgramRequirements"));
    }


    /**
     *  Gets the CredentialRequirements element Id.
     *
     *  @return the CredentialRequirements element Id
     */

    public static org.osid.id.Id getCredentialRequirements() {
        return (makeElementId("osid.course.requisite.requisite.CredentialRequirements"));
    }


    /**
     *  Gets the LearningObjectiveRequirements element Id.
     *
     *  @return the LearningObjectiveRequirements element Id
     */

    public static org.osid.id.Id getLearningObjectiveRequirements() {
        return (makeElementId("osid.course.requisite.requisite.LearningObjectiveRequirements"));
    }


    /**
     *  Gets the AssessmentRequirements element Id.
     *
     *  @return the AssessmentRequirements element Id
     */

    public static org.osid.id.Id getAssessmentRequirements() {
        return (makeElementId("osid.course.requisite.requisite.AssessmentRequirements"));
    }


    /**
     *  Gets the AwardRequirements element Id.
     *
     *  @return the AwardRequirements element Id
     */

    public static org.osid.id.Id getAwardRequirements() {
        return (makeElementId("osid.course.requisite.requisite.AwardRequirements"));
    }


    /**
     *  Gets the RequisiteOptionId element Id.
     *
     *  @return the RequisiteOptionId element Id
     */

    public static org.osid.id.Id getRequisiteOptionId() {
        return (makeQueryElementId("osid.course.requisite.requisite.RequisiteOptionId"));
    }


    /**
     *  Gets the CourseId element Id.
     *
     *  @return the CourseId element Id
     */

    public static org.osid.id.Id getCourseId() {
        return (makeQueryElementId("osid.course.requisite.requisite.CourseId"));
    }


    /**
     *  Gets the Course element Id.
     *
     *  @return the Course element Id
     */

    public static org.osid.id.Id getCourse() {
        return (makeQueryElementId("osid.course.requisite.requisite.Course"));
    }


    /**
     *  Gets the ProgramId element Id.
     *
     *  @return the ProgramId element Id
     */

    public static org.osid.id.Id getProgramId() {
        return (makeQueryElementId("osid.course.requisite.requisite.ProgramId"));
    }


    /**
     *  Gets the Program element Id.
     *
     *  @return the Program element Id
     */

    public static org.osid.id.Id getProgram() {
        return (makeQueryElementId("osid.course.requisite.requisite.Program"));
    }


    /**
     *  Gets the CredentialId element Id.
     *
     *  @return the CredentialId element Id
     */

    public static org.osid.id.Id getCredentialId() {
        return (makeQueryElementId("osid.course.requisite.requisite.CredentialId"));
    }


    /**
     *  Gets the Credential element Id.
     *
     *  @return the Credential element Id
     */

    public static org.osid.id.Id getCredential() {
        return (makeQueryElementId("osid.course.requisite.requisite.Credential"));
    }


    /**
     *  Gets the LearningObjectiveId element Id.
     *
     *  @return the LearningObjectiveId element Id
     */

    public static org.osid.id.Id getLearningObjectiveId() {
        return (makeQueryElementId("osid.course.requisite.requisite.LearningObjectiveId"));
    }


    /**
     *  Gets the LearningObjective element Id.
     *
     *  @return the LearningObjective element Id
     */

    public static org.osid.id.Id getLearningObjective() {
        return (makeQueryElementId("osid.course.requisite.requisite.LearningObjective"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeQueryElementId("osid.course.requisite.requisite.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeQueryElementId("osid.course.requisite.requisite.Assessment"));
    }


    /**
     *  Gets the AwardId element Id.
     *
     *  @return the AwardId element Id
     */

    public static org.osid.id.Id getAwardId() {
        return (makeQueryElementId("osid.course.requisite.requisite.AwardId"));
    }


    /**
     *  Gets the Award element Id.
     *
     *  @return the Award element Id
     */

    public static org.osid.id.Id getAward() {
        return (makeQueryElementId("osid.course.requisite.requisite.Award"));
    }


    /**
     *  Gets the ContainingRequisiteId element Id.
     *
     *  @return the ContainingRequisiteId element Id
     */

    public static org.osid.id.Id getContainingRequisiteId() {
        return (makeQueryElementId("osid.course.requisite.requisite.ContainingRequisiteId"));
    }


    /**
     *  Gets the ContainingRequisite element Id.
     *
     *  @return the ContainingRequisite element Id
     */

    public static org.osid.id.Id getContainingRequisite() {
        return (makeQueryElementId("osid.course.requisite.requisite.ContainingRequisite"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.requisite.requisite.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.requisite.requisite.CourseCatalog"));
    }
}
