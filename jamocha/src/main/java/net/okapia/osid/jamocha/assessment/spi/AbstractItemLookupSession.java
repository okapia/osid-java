//
// AbstractItemLookupSession.java
//
//    A starter implementation framework for providing an Item
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Item
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getItems(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractItemLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.ItemLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();
    

    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }

    /**
     *  Tests if this user can perform <code>Item</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupItems() {
        return (true);
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeItemView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryItemView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in banks which are
     *  children of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Item</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code> Contact </code> may have a different <code> Id </code>
     *  than requested, such as the case where a duplicate <code> Id
     *  </code> was assigned to a <code> Contact </code> and retained
     *  for compatibility.
     *
     *  @param  itemId <code>Id</code> of the
     *          <code>Item</code>
     *  @return the item
     *  @throws org.osid.NotFoundException <code>itemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>itemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.assessment.ItemList items = getItems()) {
            while (items.hasNext()) {
                org.osid.assessment.Item item = items.getNextItem();
                if (item.getId().equals(itemId)) {
                    return (item);
                }
            }
        } 

        throw new org.osid.NotFoundException(itemId + " not found");
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Items</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getItems()</code>.
     *
     *  @param  itemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>itemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.Item> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = itemIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getItem(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("item " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.assessment.item.LinkedItemList(ret));
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> which does not include
     *  items of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getItems()</code>.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.item.ItemGenusFilterList(getItems(), itemGenusType));
    }


    /**
     *  Gets a <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> and include any additional
     *  items with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getItems()</code>.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getItemsByGenusType(itemGenusType));
    }


    /**
     *  Gets a <code>ItemList</code> containing the given
     *  item record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getItems()</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.item.ItemRecordFilterList(getItems(), itemRecordType));
    }


    /**
     *  Gets an <code> ItemList </code> containing the given
     *  question. <code> </code> In plenary mode, the returned list
     *  contains all known items or an error results. Otherwise, the
     *  returned list may contain only those assessment items that are
     *  accessible through this session.
     *
     *  @param  questionId a question <code> Id </code> 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> questionId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByQuestion(org.osid.id.Id questionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.item.ItemFilterList(new QuestionFilter(questionId), getItems()));
    }


    /**
     *  Gets an <code> ItemList </code> containing the given
     *  answer. <code> </code> In plenary mode, the returned list
     *  contains all known items or an error results. Otherwise, the
     *  returned list may contain only those assessment items that are
     *  accessible through this session.
     *
     *  @param  answerId an answer <code> Id </code> 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> answerId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByAnswer(org.osid.id.Id answerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.Item> ret = new java.util.ArrayList<>();

        try (org.osid.assessment.ItemList items = getItems()) {
            while (items.hasNext()) {
                org.osid.assessment.Item item = items.getNextItem();
                try (org.osid.id.IdList ids = item.getAnswerIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(answerId)) {
                            ret.add(item);
                        }
                    }
                }
            }
        }

        return (new net.okapia.osid.jamocha.assessment.item.LinkedItemList(ret));
    }


    /**
     *  Gets an <code>ItemList</code> containing the given learning
     *  objective. In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those assessment items that are accessible
     *  through this session.
     *
     *  @param  objectiveId a learning objective <code>Id</code>
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByLearningObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.Item> ret = new java.util.ArrayList<>();

        try (org.osid.assessment.ItemList items = getItems()) {
            while (items.hasNext()) {
                org.osid.assessment.Item item = items.getNextItem();
                try (org.osid.id.IdList ids = item.getLearningObjectiveIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(objectiveId)) {
                            ret.add(item);
                        }
                    }
                }
            }
        }

        return (new net.okapia.osid.jamocha.assessment.item.LinkedItemList(ret));
    }


    /**
     *  Gets an <code> ItemList </code> containing the given learning
     *  objectives. In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those assessment items that are accessible
     *  through this session.
     *
     *  Override this method to search learning objective parents.
     *
     *  @param  objectiveIds learning objective <code>Ids</code>
     *  @return the returned <code> Item </code> list
     *  @throws org.osid.NullArgumentException <code> objectiveIds
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByLearningObjectives(org.osid.id.IdList objectiveIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
                
        net.okapia.osid.jamocha.adapter.federator.assessment.item.FederatingItemList ret = new net.okapia.osid.jamocha.adapter.federator.assessment.item.ParallelItemList();

        try (org.osid.id.IdList ids = objectiveIds) {
            while (ids.hasNext()) {
                ret.addItemList(getItemsByLearningObjective(ids.getNextId()));
            }
        }
            
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Items</code>. 
     *
     *  In plenary mode, the returned list contains all known items or
     *  an error results. Otherwise, the returned list may contain
     *  only those items that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Items</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.assessment.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the item list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of items
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.assessment.ItemList filterItemsOnViews(org.osid.assessment.ItemList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }


    public static class QuestionFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.item.ItemFilter {

        private final org.osid.id.Id questionId;

        
        /**
         *  Constructs a new <code>QuestionFilter</code>.
         *
         *  @param questionId the question Id to filter
         *  @throws org.osid.NullArgumentException <code>questionId</code>
         *          is <code>null</code>
         */

        public QuestionFilter(org.osid.id.Id questionId) {
            nullarg(questionId, "question Id");
            this.questionId = questionId;
            return;
        }


        /**
         *  Used by the ItemFilterList to filter the course list
         *  based on number.
         *
         *  @param item the item
         *  @return <code>true</code> to pass the item,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.Item item) {
            return (item.getQuestionId().equals(this.questionId));
        }
    }            
}
