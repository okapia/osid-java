//
// SupersedingEventEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SupersedingEventEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the SupersedingEventEnablerElement Id.
     *
     *  @return the superseding event enabler element Id
     */

    public static org.osid.id.Id getSupersedingEventEnablerEntityId() {
        return (makeEntityId("osid.calendaring.rules.SupersedingEventEnabler"));
    }


    /**
     *  Gets the RuledSupersedingEventId element Id.
     *
     *  @return the RuledSupersedingEventId element Id
     */

    public static org.osid.id.Id getRuledSupersedingEventId() {
        return (makeQueryElementId("osid.calendaring.rules.supersedingeventenabler.RuledSupersedingEventId"));
    }


    /**
     *  Gets the RuledSupersedingEvent element Id.
     *
     *  @return the RuledSupersedingEvent element Id
     */

    public static org.osid.id.Id getRuledSupersedingEvent() {
        return (makeQueryElementId("osid.calendaring.rules.supersedingeventenabler.RuledSupersedingEvent"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.rules.supersedingeventenabler.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.rules.supersedingeventenabler.Calendar"));
    }
}
