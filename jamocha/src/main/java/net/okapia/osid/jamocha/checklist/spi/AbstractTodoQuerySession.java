//
// AbstractTodoQuerySession.java
//
//     A template for making TodoQuerySessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic template for query sessions.
 */

public abstract class AbstractTodoQuerySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.checklist.TodoQuerySession {

    private boolean federated = false;
    private org.osid.checklist.Checklist checklist = new net.okapia.osid.jamocha.nil.checklist.checklist.UnknownChecklist();


      /**
       *  Gets the <code>Checklist/code> <code>Id</code> associated
       *  with this session.
       *
       *  @return the <code>Checklist Id</code> associated with
       *          this session
       *  @throws org.osid.IllegalStateException this session has been
       *          closed
       */

    @OSID @Override
      public org.osid.id.Id getChecklistId() {
        return (this.checklist.getId());
    }


    /**
     *  Gets the <code>Checklist</code> associated with this 
     *  session.
     *
     *  @return the <code>Checklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.checklist);
    }


    /**
     *  Sets the <code>Checklist</code>.
     *
     *  @param  checklist the checklist for this session
     *  @throws org.osid.NullArgumentException <code>checklist</code>
     *          is <code>null</code>
     */

    protected void setChecklist(org.osid.checklist.Checklist checklist) {
        nullarg(checklist, "checklist");
        this.checklist = checklist;
        return;
    }

    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todos in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Tests if this user can perform <code> Todo </code>
     *  searches. A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer search operations to
     *  unauthorized users.
     *
     *  @return <code> false </code> if search methods are not authorized, 
     *          <code> true </code> otherwise 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canSearchTodos() {
        return (true);
    }
}
