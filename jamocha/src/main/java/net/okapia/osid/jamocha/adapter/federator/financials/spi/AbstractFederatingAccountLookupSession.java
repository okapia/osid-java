//
// AbstractFederatingAccountLookupSession.java
//
//     An abstract federating adapter for an AccountLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AccountLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAccountLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.financials.AccountLookupSession>
    implements org.osid.financials.AccountLookupSession {

    private boolean parallel = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingAccountLookupSession</code>.
     */

    protected AbstractFederatingAccountLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.financials.AccountLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Account</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAccounts() {
        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            if (session.canLookupAccounts()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Account</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAccountView() {
        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            session.useComparativeAccountView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Account</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAccountView() {
        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            session.usePlenaryAccountView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include accounts in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Account</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Account</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Account</code> and
     *  retained for compatibility.
     *
     *  @param  accountId <code>Id</code> of the
     *          <code>Account</code>
     *  @return the account
     *  @throws org.osid.NotFoundException <code>accountId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            try {
                return (session.getAccount(accountId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(accountId + " not found");
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  accounts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Accounts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  accountIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>accountIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByIds(org.osid.id.IdList accountIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.financials.account.MutableAccountList ret = new net.okapia.osid.jamocha.financials.account.MutableAccountList();

        try (org.osid.id.IdList ids = accountIds) {
            while (ids.hasNext()) {
                ret.addAccount(getAccount(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  account genus <code>Type</code> which does not include
     *  accounts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.account.FederatingAccountList ret = getAccountList();

        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            ret.addAccountList(session.getAccountsByGenusType(accountGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  account genus <code>Type</code> and include any additional
     *  accounts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByParentGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.account.FederatingAccountList ret = getAccountList();

        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            ret.addAccountList(session.getAccountsByParentGenusType(accountGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AccountList</code> containing the given
     *  account record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountRecordType an account record type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByRecordType(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.account.FederatingAccountList ret = getAccountList();

        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            ret.addAccountList(session.getAccountsByRecordType(accountRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets an <code> AccountList </code> associated with the given
     *  code. In plenary mode, the returned list contains all
     *  known accounts or an error results. Otherwise, the returned
     *  list may contain only those accounts that are accessible
     *  through this session.
     *
     *  @param  code an account code
     *  @return the returned <code> Account </code> list
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.account.FederatingAccountList ret = getAccountList();

        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            ret.addAccountList(session.getAccountsByCode(code));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Accounts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Accounts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccounts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.account.FederatingAccountList ret = getAccountList();

        for (org.osid.financials.AccountLookupSession session : getSessions()) {
            ret.addAccountList(session.getAccounts());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.financials.account.FederatingAccountList getAccountList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.account.ParallelAccountList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.account.CompositeAccountList());
        }
    }
}
