//
// MutableMapProxyContactEnablerLookupSession
//
//    Implements a ContactEnabler lookup service backed by a collection of
//    contactEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.rules;


/**
 *  Implements a ContactEnabler lookup service backed by a collection of
 *  contactEnablers. The contactEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of contact enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyContactEnablerLookupSession
    extends net.okapia.osid.jamocha.core.contact.rules.spi.AbstractMapContactEnablerLookupSession
    implements org.osid.contact.rules.ContactEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyContactEnablerLookupSession}
     *  with no contact enablers.
     *
     *  @param addressBook the address book
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyContactEnablerLookupSession(org.osid.contact.AddressBook addressBook,
                                                  org.osid.proxy.Proxy proxy) {
        setAddressBook(addressBook);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyContactEnablerLookupSession} with a
     *  single contact enabler.
     *
     *  @param addressBook the address book
     *  @param contactEnabler a contact enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook},
     *          {@code contactEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyContactEnablerLookupSession(org.osid.contact.AddressBook addressBook,
                                                org.osid.contact.rules.ContactEnabler contactEnabler, org.osid.proxy.Proxy proxy) {
        this(addressBook, proxy);
        putContactEnabler(contactEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyContactEnablerLookupSession} using an
     *  array of contact enablers.
     *
     *  @param addressBook the address book
     *  @param contactEnablers an array of contact enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook},
     *          {@code contactEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyContactEnablerLookupSession(org.osid.contact.AddressBook addressBook,
                                                org.osid.contact.rules.ContactEnabler[] contactEnablers, org.osid.proxy.Proxy proxy) {
        this(addressBook, proxy);
        putContactEnablers(contactEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyContactEnablerLookupSession} using a
     *  collection of contact enablers.
     *
     *  @param addressBook the address book
     *  @param contactEnablers a collection of contact enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code addressBook},
     *          {@code contactEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyContactEnablerLookupSession(org.osid.contact.AddressBook addressBook,
                                                java.util.Collection<? extends org.osid.contact.rules.ContactEnabler> contactEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(addressBook, proxy);
        setSessionProxy(proxy);
        putContactEnablers(contactEnablers);
        return;
    }

    
    /**
     *  Makes a {@code ContactEnabler} available in this session.
     *
     *  @param contactEnabler an contact enabler
     *  @throws org.osid.NullArgumentException {@code contactEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putContactEnabler(org.osid.contact.rules.ContactEnabler contactEnabler) {
        super.putContactEnabler(contactEnabler);
        return;
    }


    /**
     *  Makes an array of contactEnablers available in this session.
     *
     *  @param contactEnablers an array of contact enablers
     *  @throws org.osid.NullArgumentException {@code contactEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putContactEnablers(org.osid.contact.rules.ContactEnabler[] contactEnablers) {
        super.putContactEnablers(contactEnablers);
        return;
    }


    /**
     *  Makes collection of contact enablers available in this session.
     *
     *  @param contactEnablers
     *  @throws org.osid.NullArgumentException {@code contactEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putContactEnablers(java.util.Collection<? extends org.osid.contact.rules.ContactEnabler> contactEnablers) {
        super.putContactEnablers(contactEnablers);
        return;
    }


    /**
     *  Removes a ContactEnabler from this session.
     *
     *  @param contactEnablerId the {@code Id} of the contact enabler
     *  @throws org.osid.NullArgumentException {@code contactEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeContactEnabler(org.osid.id.Id contactEnablerId) {
        super.removeContactEnabler(contactEnablerId);
        return;
    }    
}
