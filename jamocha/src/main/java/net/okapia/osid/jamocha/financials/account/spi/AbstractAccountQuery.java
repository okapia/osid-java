//
// AbstractAccountQuery.java
//
//     A template for making an Account Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.account.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for accounts.
 */

public abstract class AbstractAccountQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.financials.AccountQuery {

    private final java.util.Collection<org.osid.financials.records.AccountQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches accounts that are credit accounts. 
     *
     *  @param  match <code> true </code> to match accounts that are credit 
     *          account, <code> false </code> to match accounts debit accounts 
     */

    @OSID @Override
    public void matchCreditBalance(boolean match) {
        return;
    }


    /**
     *  Clears the credit balance terms. 
     */

    @OSID @Override
    public void clearCreditBalanceTerms() {
        return;
    }


    /**
     *  Matches an account code. 
     *
     *  @param  code a code 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches an account that has any code assigned. 
     *
     *  @param  match <code> true </code> to match accounts with any code, 
     *          <code> false </code> to match accounts with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        return;
    }


    /**
     *  Clears the code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        return;
    }


    /**
     *  Tests if a <code> SummaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a summery query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSummaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a summary. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the summery query 
     *  @throws org.osid.UnimplementedException <code> supportsSummeryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.SummaryQuery getSummaryQuery() {
        throw new org.osid.UnimplementedException("supportsSummaryQuery() is false");
    }


    /**
     *  Clears the summary terms. 
     */

    @OSID @Override
    public void clearSummaryTerms() {
        return;
    }


    /**
     *  Sets the account <code> Id </code> for this query to match accounts 
     *  that have the specified account as an ancestor. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAccountId(org.osid.id.Id accountId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor account <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorAccountIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAccountQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAncestorAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAccountQuery() is false");
    }


    /**
     *  Matches accounts with any account ancestor. 
     *
     *  @param  match <code> true </code> to match accounts with any ancestor, 
     *          <code> false </code> to match root accounts 
     */

    @OSID @Override
    public void matchAnyAncestorAccount(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor account query terms. 
     */

    @OSID @Override
    public void clearAncestorAccountTerms() {
        return;
    }


    /**
     *  Sets the account <code> Id </code> for this query to match accounts 
     *  that have the specified account as a descendant. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAccountId(org.osid.id.Id accountId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant account <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantAccountIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAccountQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getDescendantAccountQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAccountQuery() is false");
    }


    /**
     *  Matches accounts with any account descendant. 
     *
     *  @param  match <code> true </code> to match accounts with any 
     *          descendant, <code> false </code> to match leaf accounts 
     */

    @OSID @Override
    public void matchAnyDescendantAccount(boolean match) {
        return;
    }


    /**
     *  Clears the descendant account query terms. 
     */

    @OSID @Override
    public void clearDescendantAccountTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match accounts 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given account query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an account implementing the requested record.
     *
     *  @param accountRecordType an account record type
     *  @return the account query record
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(accountRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountQueryRecord getAccountQueryRecord(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.AccountQueryRecord record : this.records) {
            if (record.implementsRecordType(accountRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(accountRecordType + " is not supported");
    }


    /**
     *  Adds a record to this account query. 
     *
     *  @param accountQueryRecord account query record
     *  @param accountRecordType account record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAccountQueryRecord(org.osid.financials.records.AccountQueryRecord accountQueryRecord, 
                                          org.osid.type.Type accountRecordType) {

        addRecordType(accountRecordType);
        nullarg(accountQueryRecord, "account query record");
        this.records.add(accountQueryRecord);        
        return;
    }
}
