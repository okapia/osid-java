//
// LinkedProvisionableList.java
//
//     Implements a ObjectList based on a linked list.
//
//
// Tom Coppeto
// OnTapSolutions
// 22 June 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionable;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a ProvisionableList based on a linked of Provisionables. This
 *  list removes elements as they are retrieved.
 */

public final class LinkedProvisionableList
    extends net.okapia.osid.jamocha.provisioning.provisionable.spi.AbstractProvisionableList
    implements org.osid.provisioning.ProvisionableList {

    private final java.util.LinkedList<org.osid.provisioning.Provisionable> provisionables;


    /**
     *  Creates a new <code>LinkedProvisionableList</code>.
     *
     *  @param c a collection of provisionables
     *  @throws org.osid.NullArgumentException <code>c</code>
     *          is <code>null</code>
     */

    public LinkedProvisionableList(java.util.Collection<org.osid.provisioning.Provisionable> c) {
        nullarg(c, "collection");
        this.provisionables = new java.util.LinkedList<>(c);
        return;
    }


    /**
     *  Creates a new <code>LinkedProvisionableList</code>.
     *
     *  @param provisionables an array of provisionables
     *  @throws org.osid.NullArgumentException <code>obects</code>
     *          is <code>null</code>
     */

    public LinkedProvisionableList(org.osid.provisioning.Provisionable[] provisionables) {
        nullarg(provisionables, "provisionables");
        this.provisionables = new java.util.LinkedList<org.osid.provisioning.Provisionable>();
        for (org.osid.provisioning.Provisionable o : provisionables) {
            this.provisionables.add(o);
        }

        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.provisionables.size() > 0) {
            return (true);
        } else {
            return (false);
        }
    }
    

    /**
     *  Gets the number of elements available for retrieval. The number 
     *  returned by this method may be less than or equal to the total number 
     *  of elements in this list. To determine if the end of the list has been 
     *  reached, the method <code> hasNext() </code> should be used. This 
     *  method conveys what is known about the number of remaining elements at 
     *  a point in time and can be used to determine a minimum size of the 
     *  remaining elements, if known. A valid return is zero even if <code> 
     *  hasNext() </code> is true. 
     *  
     *  This method does not imply asynchronous usage. All OSID methods may 
     *  block. 
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        return (this.provisionables.size());
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        if (n > this.provisionables.size()) {
            n = this.provisionables.size();
        }

        while (n-- > 0) {
            this.provisionables.removeFirst();
        }

        return;
    }


    /**
     *  Gets the next <code>Provisionable</code> in this list. 
     *
     *  @return the next <code>Provisionable</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>Provisionable</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Provisionable getNextProvisionable()
        throws org.osid.OperationFailedException {
 
        if (hasNext()) {
            return (this.provisionables.removeFirst());
        } else {
            throw new org.osid.IllegalStateException("no more elements available in provisionable list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.provisionables.clear();
        return;
    }
}
