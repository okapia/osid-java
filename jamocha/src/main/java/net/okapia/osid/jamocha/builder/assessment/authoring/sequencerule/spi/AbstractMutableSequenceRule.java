//
// AbstractMutableSequenceRule.java
//
//     Defines a mutable SequenceRule.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>SequenceRule</code>.
 */

public abstract class AbstractMutableSequenceRule
    extends net.okapia.osid.jamocha.assessment.authoring.sequencerule.spi.AbstractSequenceRule
    implements org.osid.assessment.authoring.SequenceRule,
               net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.SequenceRuleMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this sequence rule. 
     *
     *  @param record sequence rule record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addSequenceRuleRecord(org.osid.assessment.authoring.records.SequenceRuleRecord record, org.osid.type.Type recordType) {
        super.addSequenceRuleRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this sequence rule. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this sequence rule. Disabling an operable overrides
     *  any enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */

    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this sequence rule.
     *
     *  @param displayName the name for this sequence rule
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this sequence rule.
     *
     *  @param description the description of this sequence rule
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException
     *          <code>rule</code> is <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the assessment part.
     *
     *  @param assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPart</code> is <code>null</code>
     */

    @Override
    public void setAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        super.setAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Sets the next assessment part.
     *
     *  @param nextAssessmentPart a next assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>nextAssessmentPart</code> is <code>null</code>
     */

    @Override
    public void setNextAssessmentPart(org.osid.assessment.authoring.AssessmentPart nextAssessmentPart) {
        super.setNextAssessmentPart(nextAssessmentPart);
        return;
    }


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     */

    @Override
    public void setMinimumScore(long score) {
        super.setMinimumScore(score);
        return;
    }


    /**
     *  Sets the maximum score.
     *
     *  @param score a maximum score
     */

    @Override
    public void setMaximumScore(long score) {
        super.setMaximumScore(score);
        return;
    }


    /**
     *  Sets the cumulative flag.
     *
     *  @param cumulative <code> true </code> if the score is applied
     *         to all previous assessment parts, <code> false </code>
     *         otherwise
     */

    @Override
    public void setCumulative(boolean cumulative) {
        super.setCumulative(cumulative);
        return;
    }


    /**
     *  Adds an applied assessment part.
     *
     *  @param appliedAssessmentPart an applied assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>appliedAssessmentPart</code> is <code>null</code>
     */

    @Override
    public void addAppliedAssessmentPart(org.osid.assessment.authoring.AssessmentPart appliedAssessmentPart) {
        super.addAppliedAssessmentPart(appliedAssessmentPart);
        return;
    }


    /**
     *  Sets all the applied assessment parts.
     *
     *  @param appliedAssessmentParts a collection of applied assessment parts
     *  @throws org.osid.NullArgumentException
     *          <code>appliedAssessmentParts</code> is <code>null</code>
     */

    @Override
    public void setAppliedAssessmentParts(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> appliedAssessmentParts) {
        super.setAppliedAssessmentParts(appliedAssessmentParts);
        return;
    }
}

