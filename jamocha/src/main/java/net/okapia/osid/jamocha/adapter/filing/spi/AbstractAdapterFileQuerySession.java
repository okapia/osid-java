//
// AbstractQueryFileLookupSession.java
//
//    A FileQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.filing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FileQuerySession adapter.
 */

public abstract class AbstractAdapterFileQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.filing.FileQuerySession {

    private final org.osid.filing.FileQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterFileQuerySession.
     *
     *  @param session the underlying file query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFileQuerySession(org.osid.filing.FileQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeDirectory</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeDirectory Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDirectoryId() {
        return (this.session.getDirectoryId());
    }


    /**
     *  Gets the {@codeDirectory</code> associated with this 
     *  session.
     *
     *  @return the {@codeDirectory</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDirectory());
    }


    /**
     *  Tests if this user can perform {@codeFile</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchFiles() {
        return (this.session.canSearchFiles());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include files in directories which are children
     *  of this directory in the directory hierarchy.
     */

    @OSID @Override
    public void useFederatedDirectoryView() {
        this.session.useFederatedDirectoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this directory only.
     */
    
    @OSID @Override
    public void useIsolatedDirectoryView() {
        this.session.useIsolatedDirectoryView();
        return;
    }
    
      
    /**
     *  Gets a file query. The returned query will not have an
     *  extension query.
     *
     *  @return the file query 
     */
      
    @OSID @Override
    public org.osid.filing.FileQuery getFileQuery() {
        return (this.session.getFileQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  fileQuery the file query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code fileQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code fileQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByQuery(org.osid.filing.FileQuery fileQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getFilesByQuery(fileQuery));
    }
}
