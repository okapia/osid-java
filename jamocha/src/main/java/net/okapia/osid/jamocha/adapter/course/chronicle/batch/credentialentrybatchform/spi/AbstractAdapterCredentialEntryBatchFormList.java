//
// AbstractCredentialEntryBatchFormList.java
//
//     Implements an AbstractCredentialEntryBatchFormList adapter.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.chronicle.batch.credentialentrybatchform.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AbstractCredentialEntryBatchFormList adapter template.
 */

public abstract class AbstractAdapterCredentialEntryBatchFormList
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidList
    implements org.osid.course.chronicle.batch.CredentialEntryBatchFormList {


    /**
     *  Constructs a new {@code AbstractAdapterCredentialEntryBatchFormList}.
     *
     *  @param list
     *  @throws org.osid.NullArgumentException {@code list} is 
     *          {@code null}
     */

    protected AbstractAdapterCredentialEntryBatchFormList(org.osid.OsidList list) {
        super(list);
        return;
    }


    /**
     *  Gets the next {@code CredentialEntryBatchForm} in this list. 
     *
     *  @return the next {@code CredentialEntryBatchForm} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code CredentialEntryBatchForm} is available before calling this
     *          method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public abstract org.osid.course.chronicle.batch.CredentialEntryBatchForm getNextCredentialEntryBatchForm()
        throws org.osid.OperationFailedException;


    /**
     *  Gets the next set of {@code CredentialEntryBatchForm} elements in this list
     *  which must be less than or equal to the return from {@code
     *  available()}.
     *
     *  @param n the number of {@code CredentialEntryBatchForm} elements requested
     *          which must be less than or equal to {@code
     *          available()}
     *  @return an array of {@code CredentialEntryBatchForm} elements. The
     *          length of the array is less than or equal to the
     *          number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list has been closed
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.InvalidArgumentException cardinal value is negative
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CredentialEntryBatchForm[] getNextCredentialEntryBatchForms(long n)
        throws org.osid.OperationFailedException {
        
        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        org.osid.course.chronicle.batch.CredentialEntryBatchForm[] ret = new org.osid.course.chronicle.batch.CredentialEntryBatchForm[(int) n];

        for (int i = 0; i < n; i++) {
            ret[i] = getNextCredentialEntryBatchForm();
        }

        return (ret);
    }
}
