//
// AbstractAdapterBrokerProcessorLookupSession.java
//
//    A BrokerProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A BrokerProcessor lookup session adapter.
 */

public abstract class AbstractAdapterBrokerProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.BrokerProcessorLookupSession {

    private final org.osid.provisioning.rules.BrokerProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBrokerProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBrokerProcessorLookupSession(org.osid.provisioning.rules.BrokerProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code BrokerProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBrokerProcessors() {
        return (this.session.canLookupBrokerProcessors());
    }


    /**
     *  A complete view of the {@code BrokerProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerProcessorView() {
        this.session.useComparativeBrokerProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code BrokerProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerProcessorView() {
        this.session.usePlenaryBrokerProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker processors in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active broker processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerProcessorView() {
        this.session.useActiveBrokerProcessorView();
        return;
    }


    /**
     *  Active and inactive broker processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerProcessorView() {
        this.session.useAnyStatusBrokerProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code BrokerProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code BrokerProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code BrokerProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @param brokerProcessorId {@code Id} of the {@code BrokerProcessor}
     *  @return the broker processor
     *  @throws org.osid.NotFoundException {@code brokerProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code brokerProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessor getBrokerProcessor(org.osid.id.Id brokerProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerProcessor(brokerProcessorId));
    }


    /**
     *  Gets a {@code BrokerProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code BrokerProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @param  brokerProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code BrokerProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code brokerProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByIds(org.osid.id.IdList brokerProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerProcessorsByIds(brokerProcessorIds));
    }


    /**
     *  Gets a {@code BrokerProcessorList} corresponding to the given
     *  broker processor genus {@code Type} which does not include
     *  broker processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  broker processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @param  brokerProcessorGenusType a brokerProcessor genus type 
     *  @return the returned {@code BrokerProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByGenusType(org.osid.type.Type brokerProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerProcessorsByGenusType(brokerProcessorGenusType));
    }


    /**
     *  Gets a {@code BrokerProcessorList} corresponding to the given
     *  broker processor genus {@code Type} and include any additional
     *  broker processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  broker processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @param  brokerProcessorGenusType a brokerProcessor genus type 
     *  @return the returned {@code BrokerProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByParentGenusType(org.osid.type.Type brokerProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerProcessorsByParentGenusType(brokerProcessorGenusType));
    }


    /**
     *  Gets a {@code BrokerProcessorList} containing the given
     *  broker processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  broker processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @param  brokerProcessorRecordType a brokerProcessor record type 
     *  @return the returned {@code BrokerProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessorsByRecordType(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerProcessorsByRecordType(brokerProcessorRecordType));
    }


    /**
     *  Gets all {@code BrokerProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  broker processors or an error results. Otherwise, the returned list
     *  may contain only those broker processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker processors are returned that are currently
     *  active. In any status mode, active and inactive broker processors
     *  are returned.
     *
     *  @return a list of {@code BrokerProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerProcessors());
    }
}
