//
// JDBCInquiryEnablerList
//
//     Implements an InquiryEnablerList. This list creates inquiryenablers from
//     the return of a JDBC query.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.jdbc.inquiry.rules.inquiryenabler;

import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements an InquiryEnablerList. This list creates inquiryenablers from the
 *  return of a JDBC query. The query is processed in a separate
 *  thread.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more inquiryenablers to be added.</p>
 */

public final class JDBCInquiryEnablerList
    extends net.okapia.osid.jamocha.inquiry.rules.inquiryenabler.spi.AbstractMutableInquiryEnablerList
    implements org.osid.inquiry.rules.InquiryEnablerList,
               Runnable {

    private boolean running = false;
    private InquiryEnablerFetcher fetcher;


    /**
     *  Creates a new <code>JDBCInquiryEnablerList</code>.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator an inquiryEnabler to parse a result row and
     *         generate an InquiryEnabler
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of inquiryenablers in this list
     *         buffer such that when the number of inquiryenablers exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until inquiryenablers are retrieved. The value
     *         of this may depend on the amount of memory an InquiryEnabler
     *         consumes.
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>inquiryEnabler</code>
     *          is <code>null</code>
     */

    public JDBCInquiryEnablerList(String query, java.sql.Connection connection, JDBCInquiryEnablerGenerator generator,
                        boolean closeWhenDone, int bufferSize)
        throws org.osid.OperationFailedException {

        nullarg(query, "query");
        nullarg(connection, "connection");
        nullarg(generator, "generator");

        if (bufferSize <= 0) {
            throw new org.osid.InvalidArgumentException("buffer size too small");
        }

        this.fetcher = new InquiryEnablerFetcher(query, connection, generator, closeWhenDone, bufferSize);
        return;
    }


    /**
     *  Creates a new <code>JDBCInquiryEnablerList</code> and runs it upon
     *  instantiation.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator an InquiryEnabler to parse a result row and
     *         generate an InquiryEnabler
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of inquiryenablers in this list
     *         buffer such that when the number of inquiryenablers exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until inquiryenablers are retrieved. The value
     *         of this may depend on the amount of memory an InquiryEnabler
     *         consumes.
     *  @param run <code>true</code> to start the fetching thread
     *         immediately
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>inquiryEnabler</code>
     *          is <code>null</code>
     */

    public JDBCInquiryEnablerList(String query, java.sql.Connection connection, JDBCInquiryEnablerGenerator generator,
                        boolean closeWhenDone, int bufferSize, boolean run)
        throws org.osid.OperationFailedException {

        this(query, connection, generator, closeWhenDone, bufferSize);
        if (run) {
            run();
        }
        return;
    }


    /**
     *  Starts the JDBC process.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    public void run() {
        if (this.running || hasError()) {
            throw new org.osid.IllegalStateException("already started");
        }

        this.fetcher.start();
        return;
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.running = false;
        return;
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    class InquiryEnablerFetcher
        extends Thread {
        
        private String query;
        private java.sql.Connection connection;
        private JDBCInquiryEnablerGenerator generator;
        private boolean closeWhenDone;
        private int bufferSize;

        
        InquiryEnablerFetcher(String query, java.sql.Connection connection, JDBCInquiryEnablerGenerator generator,
                        boolean closeWhenDone, int bufferSize) {

            this.query         = query;
            this.connection    = connection;
            this.generator     = generator;
            this.closeWhenDone = closeWhenDone;
            this.bufferSize    = bufferSize;

            return;
        }
            
        
        public void run() {
            java.sql.Statement statement = null;
            java.sql.ResultSet resultset = null;
            JDBCInquiryEnablerList.this.running = true;

            try {
                statement = this.connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                                                            java.sql.ResultSet.CONCUR_READ_ONLY);
                resultset = statement.executeQuery(this.query);
                long length = 0;

                while (resultset.next() && JDBCInquiryEnablerList.this.running && !JDBCInquiryEnablerList.this.hasError()) {
                    JDBCInquiryEnablerList.this.addInquiryEnabler(generator.makeInquiryEnabler(resultset));
                    synchronized (JDBCInquiryEnablerList.this) {
                        JDBCInquiryEnablerList.this.notifyAll();
                    }

                    if (++length > this.bufferSize) {
                        length = JDBCInquiryEnablerList.this.available();
                        if (length > this.bufferSize) {
                            synchronized (JDBCInquiryEnablerList.this) {
                                try {
                                    JDBCInquiryEnablerList.this.wait();
                                } catch (InterruptedException ie) {}
                            }
                        }
                    } 
                }
            } catch (Exception e) {
                JDBCInquiryEnablerList.this.error(new org.osid.OperationFailedException("cannot generate inquiryenabler", e));
                return;
            } finally {
                try {
                    if (this.closeWhenDone) {
                        this.connection.close();
                    }
                 
                    JDBCInquiryEnablerList.this.running = false;

                    if (statement != null) {
                        statement.close();
                    }

                    if (resultset != null) {
                        resultset.close();
                    }
                } catch (Exception e) {}
            }

            JDBCInquiryEnablerList.this.eol();
            return;
        }
    }
}
