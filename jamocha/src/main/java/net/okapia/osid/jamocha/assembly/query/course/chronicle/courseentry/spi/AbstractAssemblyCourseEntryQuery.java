//
// AbstractAssemblyCourseEntryQuery.java
//
//     A CourseEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.chronicle.courseentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CourseEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyCourseEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.chronicle.CourseEntryQuery,
               org.osid.course.chronicle.CourseEntryQueryInspector,
               org.osid.course.chronicle.CourseEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.CourseEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.CourseEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCourseEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCourseEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Sets the course <code> Id </code> for this query to match entries that 
     *  have an entry for the given course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the course. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseColumn(), style);
        return;
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Sets the term <code> Id </code> for this query. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        getAssembler().clearTerms(getTermIdColumn());
        return;
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (getAssembler().getIdTerms(getTermIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the term. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerm(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTermColumn(), style);
        return;
    }


    /**
     *  Gets the TermId column name.
     *
     * @return the column name
     */

    protected String getTermIdColumn() {
        return ("term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        getAssembler().clearTerms(getTermColumn());
        return;
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Tests if a term order is available. 
     *
     *  @return <code> true </code> if a term order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearchOrder() {
        return (false);
    }


    /**
     *  Gets the term order. 
     *
     *  @return the term search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchOrder getTermSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTermSearchOrder() is false");
    }


    /**
     *  Gets the Term column name.
     *
     * @return the column name
     */

    protected String getTermColumn() {
        return ("term");
    }


    /**
     *  Matches completed courses. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        getAssembler().addBooleanTerm(getCompleteColumn(), match);
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        getAssembler().clearTerms(getCompleteColumn());
        return;
    }


    /**
     *  Gets the complete query terms. 
     *
     *  @return the complete terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (getAssembler().getBooleanTerms(getCompleteColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by completed 
     *  courses. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompleteColumn(), style);
        return;
    }


    /**
     *  Gets the Complete column name.
     *
     * @return the column name
     */

    protected String getCompleteColumn() {
        return ("complete");
    }


    /**
     *  Matches a credit scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getCreditScaleIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditScaleIdTerms() {
        getAssembler().clearTerms(getCreditScaleIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditScaleIdTerms() {
        return (getAssembler().getIdTerms(getCreditScaleIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for credits. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditScale(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditScaleColumn(), style);
        return;
    }


    /**
     *  Gets the CreditScaleId column name.
     *
     * @return the column name
     */

    protected String getCreditScaleIdColumn() {
        return ("credit_scale_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditScaleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getCreditScaleQuery() {
        throw new org.osid.UnimplementedException("supportsCreditScaleQuery() is false");
    }


    /**
     *  Matches entries that have any credit scale. 
     *
     *  @param  match <code> true </code> to match entries with any credit 
     *          scale, <code> false </code> to match entries with no credit 
     *          scale 
     */

    @OSID @Override
    public void matchAnyCreditScale(boolean match) {
        getAssembler().addIdWildcardTerm(getCreditScaleColumn(), match);
        return;
    }


    /**
     *  Clears the credit scale terms. 
     */

    @OSID @Override
    public void clearCreditScaleTerms() {
        getAssembler().clearTerms(getCreditScaleColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getCreditScaleTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system search order. 
     *
     *  @return the credit scale search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditScaleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getCreditScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreditScaleSearchOrder() is false");
    }


    /**
     *  Gets the CreditScale column name.
     *
     * @return the column name
     */

    protected String getCreditScaleColumn() {
        return ("credit_scale");
    }


    /**
     *  Matches earned credits between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditsEarned(java.math.BigDecimal from, 
                                   java.math.BigDecimal to, boolean match) {
        getAssembler().addDecimalRangeTerm(getCreditsEarnedColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any earned credits. 
     *
     *  @param  match <code> true </code> to match entries with any earned 
     *          credits, <code> false </code> to match entries with no earned 
     *          credits 
     */

    @OSID @Override
    public void matchAnyCreditsEarned(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getCreditsEarnedColumn(), match);
        return;
    }


    /**
     *  Clears the earned credits terms. 
     */

    @OSID @Override
    public void clearCreditsEarnedTerms() {
        getAssembler().clearTerms(getCreditsEarnedColumn());
        return;
    }


    /**
     *  Gets the earned credits query terms. 
     *
     *  @return the earned credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCreditsEarnedTerms() {
        return (getAssembler().getDecimalRangeTerms(getCreditsEarnedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the earned 
     *  credits. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditsEarned(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditsEarnedColumn(), style);
        return;
    }


    /**
     *  Gets the CreditsEarned column name.
     *
     * @return the column name
     */

    protected String getCreditsEarnedColumn() {
        return ("credits_earned");
    }


    /**
     *  Matches a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getGradeIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        getAssembler().clearTerms(getGradeIdColumn());
        return;
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (getAssembler().getIdTerms(getGradeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeColumn(), style);
        return;
    }


    /**
     *  Gets the GradeId column name.
     *
     * @return the column name
     */

    protected String getGradeIdColumn() {
        return ("grade_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches entries that have any grade. 
     *
     *  @param  match <code> true </code> to match entries with any grade, 
     *          <code> false </code> to match entries with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeColumn(), match);
        return;
    }


    /**
     *  Clears the grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        getAssembler().clearTerms(getGradeColumn());
        return;
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system search order. 
     *
     *  @return the grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Gets the Grade column name.
     *
     * @return the column name
     */

    protected String getGradeColumn() {
        return ("grade");
    }


    /**
     *  Matches a score scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScoreScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getScoreScaleIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScoreScaleIdTerms() {
        getAssembler().clearTerms(getScoreScaleIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScoreScaleIdTerms() {
        return (getAssembler().getIdTerms(getScoreScaleIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for scores. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScoreScale(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScoreScaleColumn(), style);
        return;
    }


    /**
     *  Gets the ScoreScaleId column name.
     *
     * @return the column name
     */

    protected String getScoreScaleIdColumn() {
        return ("score_scale_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreScaleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getScoreScaleQuery() {
        throw new org.osid.UnimplementedException("supportsScoreScaleQuery() is false");
    }


    /**
     *  Matches entries that have any score scale. 
     *
     *  @param  match <code> true </code> to match entries with any score 
     *          scale, <code> false </code> to match entries with no score 
     *          scale 
     */

    @OSID @Override
    public void matchAnyScoreScale(boolean match) {
        getAssembler().addIdWildcardTerm(getScoreScaleColumn(), match);
        return;
    }


    /**
     *  Clears the score scale terms. 
     */

    @OSID @Override
    public void clearScoreScaleTerms() {
        getAssembler().clearTerms(getScoreScaleColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getScoreScaleTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system order. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreScaleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getScoreScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScoreScaleSearchOrder() is false");
    }


    /**
     *  Gets the ScoreScale column name.
     *
     * @return the column name
     */

    protected String getScoreScaleColumn() {
        return ("score_scale");
    }


    /**
     *  Matches scores between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchScore(java.math.BigDecimal from, java.math.BigDecimal to, 
                           boolean match) {
        getAssembler().addDecimalRangeTerm(getScoreColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any score. 
     *
     *  @param  match <code> true </code> to match entries with any score, 
     *          <code> false </code> to match entries with no score 
     */

    @OSID @Override
    public void matchAnyScore(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getScoreColumn(), match);
        return;
    }


    /**
     *  Clears the score terms. 
     */

    @OSID @Override
    public void clearScoreTerms() {
        getAssembler().clearTerms(getScoreColumn());
        return;
    }


    /**
     *  Gets the score query terms. 
     *
     *  @return the score query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getScoreColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScoreColumn(), style);
        return;
    }


    /**
     *  Gets the Score column name.
     *
     * @return the column name
     */

    protected String getScoreColumn() {
        return ("score");
    }


    /**
     *  Sets the enrollment <code> Id </code> for this query. 
     *
     *  @param  enrollmentId an enrollment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> enrollmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRegistrationId(org.osid.id.Id enrollmentId, boolean match) {
        getAssembler().addIdTerm(getRegistrationIdColumn(), enrollmentId, match);
        return;
    }


    /**
     *  Clears the registration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRegistrationIdTerms() {
        getAssembler().clearTerms(getRegistrationIdColumn());
        return;
    }


    /**
     *  Gets the registration <code> Id </code> query terms. 
     *
     *  @return the registration <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRegistrationIdTerms() {
        return (getAssembler().getIdTerms(getRegistrationIdColumn()));
    }


    /**
     *  Gets the RegistrationId column name.
     *
     * @return the column name
     */

    protected String getRegistrationIdColumn() {
        return ("registration_id");
    }


    /**
     *  Tests if a <code> RegistrationQuery </code> is available. 
     *
     *  @return <code> true </code> if a registration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a registration entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a registration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQuery getRegistrationQuery() {
        throw new org.osid.UnimplementedException("supportsRegistrationQuery() is false");
    }


    /**
     *  Matches entries that have any registration. 
     *
     *  @param  match <code> true </code> to match enries with any 
     *          registration, <code> false </code> to match enries with no 
     *          registration 
     */

    @OSID @Override
    public void matchAnyRegistration(boolean match) {
        getAssembler().addIdWildcardTerm(getRegistrationColumn(), match);
        return;
    }


    /**
     *  Clears the registration terms. 
     */

    @OSID @Override
    public void clearRegistrationTerms() {
        getAssembler().clearTerms(getRegistrationColumn());
        return;
    }


    /**
     *  Gets the registration query terms. 
     *
     *  @return the registration query terms 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQueryInspector[] getRegistrationTerms() {
        return (new org.osid.course.registration.RegistrationQueryInspector[0]);
    }


    /**
     *  Gets the Registration column name.
     *
     * @return the column name
     */

    protected String getRegistrationColumn() {
        return ("registration");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this courseEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  courseEntryRecordType a course entry record type 
     *  @return <code>true</code> if the courseEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseEntryRecordType) {
        for (org.osid.course.chronicle.records.CourseEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  courseEntryRecordType the course entry record type 
     *  @return the course entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntryQueryRecord getCourseEntryQueryRecord(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CourseEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  courseEntryRecordType the course entry record type 
     *  @return the course entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord getCourseEntryQueryInspectorRecord(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param courseEntryRecordType the course entry record type
     *  @return the course entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntrySearchOrderRecord getCourseEntrySearchOrderRecord(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CourseEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this course entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseEntryQueryRecord the course entry query record
     *  @param courseEntryQueryInspectorRecord the course entry query inspector
     *         record
     *  @param courseEntrySearchOrderRecord the course entry search order record
     *  @param courseEntryRecordType course entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryQueryRecord</code>,
     *          <code>courseEntryQueryInspectorRecord</code>,
     *          <code>courseEntrySearchOrderRecord</code> or
     *          <code>courseEntryRecordTypecourseEntry</code> is
     *          <code>null</code>
     */
            
    protected void addCourseEntryRecords(org.osid.course.chronicle.records.CourseEntryQueryRecord courseEntryQueryRecord, 
                                      org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord courseEntryQueryInspectorRecord, 
                                      org.osid.course.chronicle.records.CourseEntrySearchOrderRecord courseEntrySearchOrderRecord, 
                                      org.osid.type.Type courseEntryRecordType) {

        addRecordType(courseEntryRecordType);

        nullarg(courseEntryQueryRecord, "course entry query record");
        nullarg(courseEntryQueryInspectorRecord, "course entry query inspector record");
        nullarg(courseEntrySearchOrderRecord, "course entry search odrer record");

        this.queryRecords.add(courseEntryQueryRecord);
        this.queryInspectorRecords.add(courseEntryQueryInspectorRecord);
        this.searchOrderRecords.add(courseEntrySearchOrderRecord);
        
        return;
    }
}
