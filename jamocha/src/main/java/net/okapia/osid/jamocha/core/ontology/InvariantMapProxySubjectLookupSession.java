//
// InvariantMapProxySubjectLookupSession
//
//    Implements a Subject lookup service backed by a fixed
//    collection of subjects. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology;


/**
 *  Implements a Subject lookup service backed by a fixed
 *  collection of subjects. The subjects are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySubjectLookupSession
    extends net.okapia.osid.jamocha.core.ontology.spi.AbstractMapSubjectLookupSession
    implements org.osid.ontology.SubjectLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySubjectLookupSession} with no
     *  subjects.
     *
     *  @param ontology the ontology
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                  org.osid.proxy.Proxy proxy) {
        setOntology(ontology);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxySubjectLookupSession} with a single
     *  subject.
     *
     *  @param ontology the ontology
     *  @param subject a single subject
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology},
     *          {@code subject} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                  org.osid.ontology.Subject subject, org.osid.proxy.Proxy proxy) {

        this(ontology, proxy);
        putSubject(subject);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySubjectLookupSession} using
     *  an array of subjects.
     *
     *  @param ontology the ontology
     *  @param subjects an array of subjects
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology},
     *          {@code subjects} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                  org.osid.ontology.Subject[] subjects, org.osid.proxy.Proxy proxy) {

        this(ontology, proxy);
        putSubjects(subjects);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySubjectLookupSession} using a
     *  collection of subjects.
     *
     *  @param ontology the ontology
     *  @param subjects a collection of subjects
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology},
     *          {@code subjects} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                  java.util.Collection<? extends org.osid.ontology.Subject> subjects,
                                                  org.osid.proxy.Proxy proxy) {

        this(ontology, proxy);
        putSubjects(subjects);
        return;
    }
}
