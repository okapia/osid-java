//
// AbstractCheckSearchOdrer.java
//
//     Defines a CheckSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CheckSearchOrder}.
 */

public abstract class AbstractCheckSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.rules.check.CheckSearchOrder {

    private final java.util.Collection<org.osid.rules.check.records.CheckSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by the time check start date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckStartDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the time end check date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckEndDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the time check event. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckEvent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an event search order is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the event search order. 
     *
     *  @return the event search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsTimeCheckEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getTimeCheckEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimeCheckEventSearchOrder() is false");
    }


    /**
     *  Orders the results by cyclic event. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckCyclicEvent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a cyclic event search order is available. 
     *
     *  @return <code> true </code> if a cyclic event search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckCyclicEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the cyclic event search order. 
     *
     *  @return the cyclic event search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsTimeCheckCyclicEventSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchOrder getTimeCheckCyclicEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimeCheckCyclicEventSearchOrder() is false");
    }


    /**
     *  Orders the results by block. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHoldCheckBlock(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a block search order is available. 
     *
     *  @return <code> true </code> if a block search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldCheckBlockSearchOrder() {
        return (false);
    }


    /**
     *  Gets the block search order. 
     *
     *  @return the block search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsHoldCheckBlockSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchOrder getHoldCheckBlockSearchOrder() {
        throw new org.osid.UnimplementedException("supportsHoldCheckBlockSearchOrder() is false");
    }


    /**
     *  Orders the results by audit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInquiryCheckAudit(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an audit search order is available. 
     *
     *  @return <code> true </code> if an audit search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryCheckAuditSearchOrder() {
        return (false);
    }


    /**
     *  Gets the audit search order. 
     *
     *  @return the audit search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInquiryCheckAuditSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchOrder getInquiryCheckAuditSearchOrder() {
        throw new org.osid.UnimplementedException("supportsInquiryCheckAuditSearchOrder() is false");
    }


    /**
     *  Orders the results by agenda. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProcessCheckAgenda(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agenda search order is available. 
     *
     *  @return <code> true </code> if an agenda search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessCheckAgendaSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agenda search order. 
     *
     *  @return the agenda search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProcessCheckAgendaSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchOrder getProcessCheckAgendaSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProcessCheckAgendaSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  checkRecordType a check record type 
     *  @return {@code true} if the checkRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code checkRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type checkRecordType) {
        for (org.osid.rules.check.records.CheckSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(checkRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  checkRecordType the check record type 
     *  @return the check search order record
     *  @throws org.osid.NullArgumentException
     *          {@code checkRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(checkRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckSearchOrderRecord getCheckSearchOrderRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(checkRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this check. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param checkRecord the check search odrer record
     *  @param checkRecordType check record type
     *  @throws org.osid.NullArgumentException
     *          {@code checkRecord} or
     *          {@code checkRecordTypecheck} is
     *          {@code null}
     */
            
    protected void addCheckRecord(org.osid.rules.check.records.CheckSearchOrderRecord checkSearchOrderRecord, 
                                     org.osid.type.Type checkRecordType) {

        addRecordType(checkRecordType);
        this.records.add(checkSearchOrderRecord);
        
        return;
    }
}
