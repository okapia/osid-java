//
// AbstractAssemblyParameterQuery.java
//
//     A ParameterQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ParameterQuery that stores terms.
 */

public abstract class AbstractAssemblyParameterQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.configuration.ParameterQuery,
               org.osid.configuration.ParameterQueryInspector,
               org.osid.configuration.ParameterSearchOrder {

    private final java.util.Collection<org.osid.configuration.records.ParameterQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ParameterQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ParameterSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyParameterQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyParameterQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a match for parameters of a given value syntax. Multiple matches 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  syntax the parameter value syntax 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syntax </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchValueSyntax(org.osid.Syntax syntax, boolean match) {
        getAssembler().addSyntaxTerm(getValueSyntaxColumn(), syntax, match);
        return;
    }


    /**
     *  Clears the value syntax terms. 
     */

    @OSID @Override
    public void clearValueSyntaxTerms() {
        getAssembler().clearTerms(getValueSyntaxColumn());
        return;
    }


    /**
     *  Gets the value syntax query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SyntaxTerm[] getValueSyntaxTerms() {
        return (getAssembler().getSyntaxTerms(getValueSyntaxColumn()));
    }


    /**
     *  Specifies a preference for ordering the results by the value syntax 
     *  and object type. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValueSyntax(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getValueSyntaxColumn(), style);
        return;
    }


    /**
     *  Gets the ValueSyntax column name.
     *
     * @return the column name
     */

    protected String getValueSyntaxColumn() {
        return ("value_syntax");
    }


    /**
     *  Adds a match for parameters with a given coordinate type for a 
     *  coordinate value. Multiple matches can be added to perform a boolean 
     *  <code> OR </code> among them. 
     *
     *  @param  coordinateType the coordinate type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchValueCoordinateType(org.osid.type.Type coordinateType, 
                                         boolean match) {
        getAssembler().addTypeTerm(getValueCoordinateTypeColumn(), coordinateType, match);
        return;
    }


    /**
     *  Clears the coordinate type terms. 
     */

    @OSID @Override
    public void clearValueCoordinateTypeTerms() {
        getAssembler().clearTerms(getValueCoordinateTypeColumn());
        return;
    }


    /**
     *  Gets the coordinate record type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueCoordinateTypeTerms() {
        return (getAssembler().getTypeTerms(getValueCoordinateTypeColumn()));
    }


    /**
     *  Gets the ValueCoordinateType column name.
     *
     * @return the column name
     */

    protected String getValueCoordinateTypeColumn() {
        return ("value_coordinate_type");
    }


    /**
     *  Adds a match for parameters with a given heading type for a coordinate 
     *  value. Multiple matches can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  headingType the heading type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueHeadingType(org.osid.type.Type headingType, 
                                      boolean match) {
        getAssembler().addTypeTerm(getValueHeadingTypeColumn(), headingType, match);
        return;
    }


    /**
     *  Clears the coorheadingdinate record type terms. 
     */

    @OSID @Override
    public void clearValueHeadingTypeTerms() {
        getAssembler().clearTerms(getValueHeadingTypeColumn());
        return;
    }


    /**
     *  Gets the heading record type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueHeadingTypeTerms() {
        return (getAssembler().getTypeTerms(getValueHeadingTypeColumn()));
    }


    /**
     *  Gets the ValueHeadingType column name.
     *
     * @return the column name
     */

    protected String getValueHeadingTypeColumn() {
        return ("value_heading_type");
    }


    /**
     *  Adds a match for parameters with a given object type for an object 
     *  value. Multiple matches can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  objectType the object type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueObjectType(org.osid.type.Type objectType, 
                                     boolean match) {
        getAssembler().addTypeTerm(getValueObjectTypeColumn(), objectType, match);
        return;
    }


    /**
     *  Clears the object value type terms. 
     */

    @OSID @Override
    public void clearValueObjectTypeTerms() {
        getAssembler().clearTerms(getValueObjectTypeColumn());
        return;
    }


    /**
     *  Gets the object value type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueObjectTypeTerms() {
        return (getAssembler().getTypeTerms(getValueObjectTypeColumn()));
    }


    /**
     *  Gets the ValueObjectType column name.
     *
     * @return the column name
     */

    protected String getValueObjectTypeColumn() {
        return ("value_object_type");
    }


    /**
     *  Adds a match for parameters with a given spatial unit record type for 
     *  a coordinate value. Multiple matches can be added to perform a boolean 
     *  <code> OR </code> among them. 
     *
     *  @param  spatialUnitRecordType the spatial unit record type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchValueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType, 
                                                boolean match) {
        getAssembler().addTypeTerm(getValueSpatialUnitRecordTypeColumn(), spatialUnitRecordType, match);
        return;
    }


    /**
     *  Clears the spatial unit record type terms. 
     */

    @OSID @Override
    public void clearValueSpatialUnitRecordTypeTerms() {
        getAssembler().clearTerms(getValueSpatialUnitRecordTypeColumn());
        return;
    }


    /**
     *  Gets the spatial unit record type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueSpatialUnitRecordTypeTerms() {
        return (getAssembler().getTypeTerms(getValueSpatialUnitRecordTypeColumn()));
    }


    /**
     *  Gets the ValueSpatialUnitRecordType column name.
     *
     * @return the column name
     */

    protected String getValueSpatialUnitRecordTypeColumn() {
        return ("value_spatial_unit_record_type");
    }


    /**
     *  Adds a match for parameters with a given version type for a version 
     *  value. Multiple matches can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  versionType the version type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> versionType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchValueVersionScheme(org.osid.type.Type versionType, 
                                        boolean match) {
        getAssembler().addTypeTerm(getValueVersionSchemeColumn(), versionType, match);
        return;
    }


    /**
     *  Clears the value type terms. 
     */

    @OSID @Override
    public void clearValueVersionSchemeTerms() {
        getAssembler().clearTerms(getValueVersionSchemeColumn());
        return;
    }


    /**
     *  Gets the version type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getValueVersionSchemeTerms() {
        return (getAssembler().getTypeTerms(getValueVersionSchemeColumn()));
    }


    /**
     *  Gets the ValueVersionScheme column name.
     *
     * @return the column name
     */

    protected String getValueVersionSchemeColumn() {
        return ("value_version_scheme");
    }


    /**
     *  Tests if a <code> ValueQuery </code> is available. 
     *
     *  @return <code> true </code> if a value query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a value. 
     *
     *  @return the value query 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuery getValueQuery() {
        throw new org.osid.UnimplementedException("supportsValueQuery() is false");
    }


    /**
     *  Matches parameters that have any value. 
     *
     *  @param  match <code> true </code> to match parameters with any value, 
     *          <code> false </code> to match parameters with no value 
     */

    @OSID @Override
    public void matchAnyValue(boolean match) {
        getAssembler().addIdWildcardTerm(getValueColumn(), match);
        return;
    }


    /**
     *  Clears the value terms. 
     */

    @OSID @Override
    public void clearValueTerms() {
        getAssembler().clearTerms(getValueColumn());
        return;
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ValueQueryInspector[] getValueTerms() {
        return (new org.osid.configuration.ValueQueryInspector[0]);
    }


    /**
     *  Gets the Value column name.
     *
     * @return the column name
     */

    protected String getValueColumn() {
        return ("value");
    }


    /**
     *  Matches shuffle order. 
     *
     *  @param  shuffle <code> true </code> to match shuffle by weight, false 
     *          to match order by index 
     */

    @OSID @Override
    public void matchValuesShuffled(boolean shuffle) {
        getAssembler().addBooleanTerm(getValuesShuffledColumn(), shuffle);
        return;
    }


    /**
     *  Matches parameters that have any shuffled value. 
     *
     *  @param  match <code> true </code> to match parameters with any shuffle 
     *          value, <code> false </code> to match parameters with no 
     *          shuffle value 
     */

    @OSID @Override
    public void matchAnyValuesShuffled(boolean match) {
        getAssembler().addBooleanWildcardTerm(getValuesShuffledColumn(), match);
        return;
    }


    /**
     *  Clears the shuffle terms. 
     */

    @OSID @Override
    public void clearValuesShuffledTerms() {
        getAssembler().clearTerms(getValuesShuffledColumn());
        return;
    }


    /**
     *  Gets the shuffle query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getValuesShuffledTerms() {
        return (getAssembler().getBooleanTerms(getValuesShuffledColumn()));
    }


    /**
     *  Specifies a preference for ordering the results by the shuffle flag. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValuesShuffled(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getValuesShuffledColumn(), style);
        return;
    }


    /**
     *  Gets the ValuesShuffled column name.
     *
     * @return the column name
     */

    protected String getValuesShuffledColumn() {
        return ("values_shuffled");
    }


    /**
     *  Sets the configuration <code> Id </code> for this query. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getConfigurationIdColumn(), configurationId, match);
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        getAssembler().clearTerms(getConfigurationIdColumn());
        return;
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (getAssembler().getIdTerms(getConfigurationIdColumn()));
    }


    /**
     *  Gets the ConfigurationId column name.
     *
     * @return the column name
     */

    protected String getConfigurationIdColumn() {
        return ("configuration_id");
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        getAssembler().clearTerms(getConfigurationColumn());
        return;
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the Configuration column name.
     *
     * @return the column name
     */

    protected String getConfigurationColumn() {
        return ("configuration");
    }


    /**
     *  Tests if this parameter supports the given record
     *  <code>Type</code>.
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return <code>true</code> if the parameterRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type parameterRecordType) {
        for (org.osid.configuration.records.ParameterQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  parameterRecordType the parameter record type 
     *  @return the parameter query record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterQueryRecord getParameterQueryRecord(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ParameterQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  parameterRecordType the parameter record type 
     *  @return the parameter query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterQueryInspectorRecord getParameterQueryInspectorRecord(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ParameterQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param parameterRecordType the parameter record type
     *  @return the parameter search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterSearchOrderRecord getParameterSearchOrderRecord(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ParameterSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this parameter. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param parameterQueryRecord the parameter query record
     *  @param parameterQueryInspectorRecord the parameter query inspector
     *         record
     *  @param parameterSearchOrderRecord the parameter search order record
     *  @param parameterRecordType parameter record type
     *  @throws org.osid.NullArgumentException
     *          <code>parameterQueryRecord</code>,
     *          <code>parameterQueryInspectorRecord</code>,
     *          <code>parameterSearchOrderRecord</code> or
     *          <code>parameterRecordTypeparameter</code> is
     *          <code>null</code>
     */
            
    protected void addParameterRecords(org.osid.configuration.records.ParameterQueryRecord parameterQueryRecord, 
                                      org.osid.configuration.records.ParameterQueryInspectorRecord parameterQueryInspectorRecord, 
                                      org.osid.configuration.records.ParameterSearchOrderRecord parameterSearchOrderRecord, 
                                      org.osid.type.Type parameterRecordType) {

        addRecordType(parameterRecordType);

        nullarg(parameterQueryRecord, "parameter query record");
        nullarg(parameterQueryInspectorRecord, "parameter query inspector record");
        nullarg(parameterSearchOrderRecord, "parameter search odrer record");

        this.queryRecords.add(parameterQueryRecord);
        this.queryInspectorRecords.add(parameterQueryInspectorRecord);
        this.searchOrderRecords.add(parameterSearchOrderRecord);
        
        return;
    }
}
