//
// AbstractIndexedMapJobLookupSession.java
//
//    A simple framework for providing a Job lookup service
//    backed by a fixed collection of jobs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Job lookup service backed by a
 *  fixed collection of jobs. The jobs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some jobs may be compatible
 *  with more types than are indicated through these job
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Jobs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapJobLookupSession
    extends AbstractMapJobLookupSession
    implements org.osid.resourcing.JobLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Job> jobsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Job>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Job> jobsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Job>());


    /**
     *  Makes a <code>Job</code> available in this session.
     *
     *  @param  job a job
     *  @throws org.osid.NullArgumentException <code>job<code> is
     *          <code>null</code>
     */

    @Override
    protected void putJob(org.osid.resourcing.Job job) {
        super.putJob(job);

        this.jobsByGenus.put(job.getGenusType(), job);
        
        try (org.osid.type.TypeList types = job.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobsByRecord.put(types.getNextType(), job);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a job from this session.
     *
     *  @param jobId the <code>Id</code> of the job
     *  @throws org.osid.NullArgumentException <code>jobId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeJob(org.osid.id.Id jobId) {
        org.osid.resourcing.Job job;
        try {
            job = getJob(jobId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.jobsByGenus.remove(job.getGenusType());

        try (org.osid.type.TypeList types = job.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobsByRecord.remove(types.getNextType(), job);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeJob(jobId);
        return;
    }


    /**
     *  Gets a <code>JobList</code> corresponding to the given
     *  job genus <code>Type</code> which does not include
     *  jobs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known jobs or an error results. Otherwise,
     *  the returned list may contain only those jobs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  jobGenusType a job genus type 
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByGenusType(org.osid.type.Type jobGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.job.ArrayJobList(this.jobsByGenus.get(jobGenusType)));
    }


    /**
     *  Gets a <code>JobList</code> containing the given
     *  job record <code>Type</code>. In plenary mode, the
     *  returned list contains all known jobs or an error
     *  results. Otherwise, the returned list may contain only those
     *  jobs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  jobRecordType a job record type 
     *  @return the returned <code>job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByRecordType(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.job.ArrayJobList(this.jobsByRecord.get(jobRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobsByGenus.clear();
        this.jobsByRecord.clear();

        super.close();

        return;
    }
}
