//
// AbstractIndexedMapSignalEnablerLookupSession.java
//
//    A simple framework for providing a SignalEnabler lookup service
//    backed by a fixed collection of signal enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SignalEnabler lookup service backed by a
 *  fixed collection of signal enablers. The signal enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some signal enablers may be compatible
 *  with more types than are indicated through these signal enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SignalEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSignalEnablerLookupSession
    extends AbstractMapSignalEnablerLookupSession
    implements org.osid.mapping.path.rules.SignalEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.rules.SignalEnabler> signalEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.rules.SignalEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.rules.SignalEnabler> signalEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.rules.SignalEnabler>());


    /**
     *  Makes a <code>SignalEnabler</code> available in this session.
     *
     *  @param  signalEnabler a signal enabler
     *  @throws org.osid.NullArgumentException <code>signalEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSignalEnabler(org.osid.mapping.path.rules.SignalEnabler signalEnabler) {
        super.putSignalEnabler(signalEnabler);

        this.signalEnablersByGenus.put(signalEnabler.getGenusType(), signalEnabler);
        
        try (org.osid.type.TypeList types = signalEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.signalEnablersByRecord.put(types.getNextType(), signalEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a signal enabler from this session.
     *
     *  @param signalEnablerId the <code>Id</code> of the signal enabler
     *  @throws org.osid.NullArgumentException <code>signalEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSignalEnabler(org.osid.id.Id signalEnablerId) {
        org.osid.mapping.path.rules.SignalEnabler signalEnabler;
        try {
            signalEnabler = getSignalEnabler(signalEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.signalEnablersByGenus.remove(signalEnabler.getGenusType());

        try (org.osid.type.TypeList types = signalEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.signalEnablersByRecord.remove(types.getNextType(), signalEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSignalEnabler(signalEnablerId);
        return;
    }


    /**
     *  Gets a <code>SignalEnablerList</code> corresponding to the given
     *  signal enabler genus <code>Type</code> which does not include
     *  signal enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known signal enablers or an error results. Otherwise,
     *  the returned list may contain only those signal enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  signalEnablerGenusType a signal enabler genus type 
     *  @return the returned <code>SignalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByGenusType(org.osid.type.Type signalEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.signalenabler.ArraySignalEnablerList(this.signalEnablersByGenus.get(signalEnablerGenusType)));
    }


    /**
     *  Gets a <code>SignalEnablerList</code> containing the given
     *  signal enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known signal enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  signal enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  signalEnablerRecordType a signal enabler record type 
     *  @return the returned <code>signalEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByRecordType(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.signalenabler.ArraySignalEnablerList(this.signalEnablersByRecord.get(signalEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.signalEnablersByGenus.clear();
        this.signalEnablersByRecord.clear();

        super.close();

        return;
    }
}
