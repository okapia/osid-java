//
// InvariantMapProxyAccountLookupSession
//
//    Implements an Account lookup service backed by a fixed
//    collection of accounts. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials;


/**
 *  Implements an Account lookup service backed by a fixed
 *  collection of accounts. The accounts are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAccountLookupSession
    extends net.okapia.osid.jamocha.core.financials.spi.AbstractMapAccountLookupSession
    implements org.osid.financials.AccountLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAccountLookupSession} with no
     *  accounts.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAccountLookupSession(org.osid.financials.Business business,
                                                  org.osid.proxy.Proxy proxy) {
        setBusiness(business);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAccountLookupSession} with a single
     *  account.
     *
     *  @param business the business
     *  @param account an single account
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code account} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAccountLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.Account account, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putAccount(account);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAccountLookupSession} using
     *  an array of accounts.
     *
     *  @param business the business
     *  @param accounts an array of accounts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code accounts} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAccountLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.Account[] accounts, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putAccounts(accounts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAccountLookupSession} using a
     *  collection of accounts.
     *
     *  @param business the business
     *  @param accounts a collection of accounts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code accounts} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAccountLookupSession(org.osid.financials.Business business,
                                                  java.util.Collection<? extends org.osid.financials.Account> accounts,
                                                  org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putAccounts(accounts);
        return;
    }
}
