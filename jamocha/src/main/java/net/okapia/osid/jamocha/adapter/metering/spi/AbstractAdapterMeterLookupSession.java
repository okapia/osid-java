//
// AbstractAdapterMeterLookupSession.java
//
//    A Meter lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.metering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Meter lookup session adapter.
 */

public abstract class AbstractAdapterMeterLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.metering.MeterLookupSession {

    private final org.osid.metering.MeterLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterMeterLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterMeterLookupSession(org.osid.metering.MeterLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Utility/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Utility Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getUtilityId() {
        return (this.session.getUtilityId());
    }


    /**
     *  Gets the {@code Utility} associated with this session.
     *
     *  @return the {@code Utility} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getUtility());
    }


    /**
     *  Tests if this user can perform {@code Meter} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupMeters() {
        return (this.session.canLookupMeters());
    }


    /**
     *  A complete view of the {@code Meter} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMeterView() {
        this.session.useComparativeMeterView();
        return;
    }


    /**
     *  A complete view of the {@code Meter} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMeterView() {
        this.session.usePlenaryMeterView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include meters in utilities which are children
     *  of this utility in the utility hierarchy.
     */

    @OSID @Override
    public void useFederatedUtilityView() {
        this.session.useFederatedUtilityView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this utility only.
     */

    @OSID @Override
    public void useIsolatedUtilityView() {
        this.session.useIsolatedUtilityView();
        return;
    }
    
     
    /**
     *  Gets the {@code Meter} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Meter} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Meter} and
     *  retained for compatibility.
     *
     *  @param meterId {@code Id} of the {@code Meter}
     *  @return the meter
     *  @throws org.osid.NotFoundException {@code meterId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code meterId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Meter getMeter(org.osid.id.Id meterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMeter(meterId));
    }


    /**
     *  Gets a {@code MeterList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  meters specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Meters} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  meterIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Meter} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code meterIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByIds(org.osid.id.IdList meterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMetersByIds(meterIds));
    }


    /**
     *  Gets a {@code MeterList} corresponding to the given
     *  meter genus {@code Type} which does not include
     *  meters of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterGenusType a meter genus type 
     *  @return the returned {@code Meter} list
     *  @throws org.osid.NullArgumentException
     *          {@code meterGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByGenusType(org.osid.type.Type meterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMetersByGenusType(meterGenusType));
    }


    /**
     *  Gets a {@code MeterList} corresponding to the given
     *  meter genus {@code Type} and include any additional
     *  meters with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterGenusType a meter genus type 
     *  @return the returned {@code Meter} list
     *  @throws org.osid.NullArgumentException
     *          {@code meterGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByParentGenusType(org.osid.type.Type meterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMetersByParentGenusType(meterGenusType));
    }


    /**
     *  Gets a {@code MeterList} containing the given
     *  meter record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  meterRecordType a meter record type 
     *  @return the returned {@code Meter} list
     *  @throws org.osid.NullArgumentException
     *          {@code meterRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByRecordType(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMetersByRecordType(meterRecordType));
    }


    /**
     *  Gets all {@code Meters}. 
     *
     *  In plenary mode, the returned list contains all known
     *  meters or an error results. Otherwise, the returned list
     *  may contain only those meters that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Meters} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.MeterList getMeters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getMeters());
    }
}
