//
// AbstractAssemblyAssetQuery.java
//
//     An AssetQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssetQuery that stores terms.
 */

public abstract class AbstractAssemblyAssetQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblySourceableOsidObjectQuery
    implements org.osid.repository.AssetQuery,
               org.osid.repository.AssetQueryInspector,
               org.osid.repository.AssetSearchOrder {

    private final java.util.Collection<org.osid.repository.records.AssetQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.AssetQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.AssetSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAssetQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAssetQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match assets with any title, 
     *          <code> false </code> to match assets with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by asset title. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Matches assets marked as public domain. 
     *
     *  @param  publicDomain public domain flag 
     */

    @OSID @Override
    public void matchPublicDomain(boolean publicDomain) {
        getAssembler().addBooleanTerm(getPublicDomainColumn(), publicDomain);
        return;
    }


    /**
     *  Matches assets with any public domain value. 
     *
     *  @param  match <code> true </code> to match assets with any public 
     *          domain value, <code> false </code> to match assets with no 
     *          public domain value 
     */

    @OSID @Override
    public void matchAnyPublicDomain(boolean match) {
        getAssembler().addBooleanWildcardTerm(getPublicDomainColumn(), match);
        return;
    }


    /**
     *  Clears the public domain terms. 
     */

    @OSID @Override
    public void clearPublicDomainTerms() {
        getAssembler().clearTerms(getPublicDomainColumn());
        return;
    }


    /**
     *  Gets the public domain query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getPublicDomainTerms() {
        return (getAssembler().getBooleanTerms(getPublicDomainColumn()));
    }


    /**
     *  Specifies a preference for grouping the result set by published 
     *  domain. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPublicDomain(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPublicDomainColumn(), style);
        return;
    }


    /**
     *  Gets the PublicDomain column name.
     *
     * @return the column name
     */

    protected String getPublicDomainColumn() {
        return ("public_domain");
    }


    /**
     *  Adds a copyright for this query. 
     *
     *  @param  copyright copyright string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> copyright </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> copyright </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCopyright(String copyright, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        getAssembler().addStringTerm(getCopyrightColumn(), copyright, stringMatchType, match);
        return;
    }


    /**
     *  Matches assets with any copyright statement. 
     *
     *  @param  match <code> true </code> to match assets with any copyright 
     *          value, <code> false </code> to match assets with no copyright 
     *          value 
     */

    @OSID @Override
    public void matchAnyCopyright(boolean match) {
        getAssembler().addStringWildcardTerm(getCopyrightColumn(), match);
        return;
    }


    /**
     *  Clears the copyright terms. 
     */

    @OSID @Override
    public void clearCopyrightTerms() {
        getAssembler().clearTerms(getCopyrightColumn());
        return;
    }


    /**
     *  Gets the copyright query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCopyrightTerms() {
        return (getAssembler().getStringTerms(getCopyrightColumn()));
    }


    /**
     *  Specifies a preference for grouping the result set by
     *  copyright.
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCopyright(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCopyrightColumn(), style);
        return;
    }


    /**
     *  Gets the Copyright column name.
     *
     * @return the column name
     */

    protected String getCopyrightColumn() {
        return ("copyright");
    }


    /**
     *  Adds a copyright registration for this query. 
     *
     *  @param  registration copyright registration string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> registration </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> registration </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCopyrightRegistration(String registration, 
                                           org.osid.type.Type stringMatchType, 
                                           boolean match) {
        getAssembler().addStringTerm(getCopyrightRegistrationColumn(), registration, stringMatchType, match);
        return;
    }


    /**
     *  Matches assets with any copyright registration. 
     *
     *  @param  match <code> true </code> to match assets with any copyright 
     *          registration value, <code> false </code> to match assets with 
     *          no copyright registration value 
     */

    @OSID @Override
    public void matchAnyCopyrightRegistration(boolean match) {
        getAssembler().addStringWildcardTerm(getCopyrightRegistrationColumn(), match);
        return;
    }


    /**
     *  Clears the copyright registration terms. 
     */

    @OSID @Override
    public void clearCopyrightRegistrationTerms() {
        getAssembler().clearTerms(getCopyrightRegistrationColumn());
        return;
    }


    /**
     *  Gets the copyright registration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCopyrightRegistrationTerms() {
        return (getAssembler().getStringTerms(getCopyrightRegistrationColumn()));
    }


    /**
     *  Specifies a preference for grouping the result set by
     *  copyright registration.
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCopyrightRegistration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCopyrightRegistrationColumn(), style);
        return;
    }


    /**
     *  Gets the CopyrightRegistration column name.
     *
     * @return the column name
     */

    protected String getCopyrightRegistrationColumn() {
        return ("copyright_registration");
    }


    /**
     *  Matches assets marked as distributable. 
     *
     *  @param  distributable distribute verbatim rights flag 
     */

    @OSID @Override
    public void matchDistributeVerbatim(boolean distributable) {
        getAssembler().addBooleanTerm(getDistributeVerbatimColumn(), distributable);
        return;
    }


    /**
     *  Clears the distribute verbatim terms. 
     */

    @OSID @Override
    public void clearDistributeVerbatimTerms() {
        getAssembler().clearTerms(getDistributeVerbatimColumn());
        return;
    }


    /**
     *  Gets the verbatim distribution query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDistributeVerbatimTerms() {
        return (getAssembler().getBooleanTerms(getDistributeVerbatimColumn()));
    }


    /**
     *  Specifies a preference for grouping the result set by the ability to 
     *  distribute copies. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistributeVerbatim(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistributeVerbatimColumn(), style);
        return;
    }


    /**
     *  Gets the DistributeVerbatim column name.
     *
     * @return the column name
     */

    protected String getDistributeVerbatimColumn() {
        return ("distribute_verbatim");
    }


    /**
     *  Matches assets that whose alterations can be distributed. 
     *
     *  @param  alterable distribute alterations rights flag 
     */

    @OSID @Override
    public void matchDistributeAlterations(boolean alterable) {
        getAssembler().addBooleanTerm(getDistributeAlterationsColumn(), alterable);
        return;
    }


    /**
     *  Clears the distribute alterations terms. 
     */

    @OSID @Override
    public void clearDistributeAlterationsTerms() {
        getAssembler().clearTerms(getDistributeAlterationsColumn());
        return;
    }


    /**
     *  Gets the alteration distribution query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDistributeAlterationsTerms() {
        return (getAssembler().getBooleanTerms(getDistributeAlterationsColumn()));
    }


    /**
     *  Specifies a preference for grouping the result set by the ability to 
     *  distribute alterations. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistributeAlterations(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistributeAlterationsColumn(), style);
        return;
    }


    /**
     *  Gets the DistributeAlterations column name.
     *
     * @return the column name
     */

    protected String getDistributeAlterationsColumn() {
        return ("distribute_alterations");
    }


    /**
     *  Matches assets that can be distributed as part of other compositions. 
     *
     *  @param  composable distribute compositions rights flag 
     */

    @OSID @Override
    public void matchDistributeCompositions(boolean composable) {
        getAssembler().addBooleanTerm(getDistributeCompositionsColumn(), composable);
        return;
    }


    /**
     *  Clears the distribute compositions terms. 
     */

    @OSID @Override
    public void clearDistributeCompositionsTerms() {
        getAssembler().clearTerms(getDistributeCompositionsColumn());
        return;
    }


    /**
     *  Gets the composition distribution query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDistributeCompositionsTerms() {
        return (getAssembler().getBooleanTerms(getDistributeCompositionsColumn()));
    }


    /**
     *  Specifies a preference for grouping the result set by the ability to 
     *  distribute compositions. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistributeCompositions(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDistributeCompositionsColumn(), style);
        return;
    }


    /**
     *  Gets the DistributeCompositions column name.
     *
     * @return the column name
     */

    protected String getDistributeCompositionsColumn() {
        return ("distribute_compositions");
    }


    /**
     *  Sets the source <code> Id </code> for this query. 
     *
     *  @param  sourceId the source <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id sourceId, boolean match) {
        getAssembler().addIdTerm(getSourceIdColumn(), sourceId, match);
        return;
    }


    /**
     *  Clears the source <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        getAssembler().clearTerms(getSourceIdColumn());
        return;
    }


    /**
     *  Gets the source <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (getAssembler().getIdTerms(getSourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by asset source. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSourceColumn(), style);
        return;
    }


    /**
     *  Gets the SourceId column name.
     *
     * @return the column name
     */

    protected String getSourceIdColumn() {
        return ("source_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for the source. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for the source. Multiple queries can be retrieved for a 
     *  nested <code> OR </code> term. 
     *
     *  @return the source query 
     *  @throws org.osid.UnimplementedException <code> supportsSourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSourceQuery() {
        throw new org.osid.UnimplementedException("supportsSourceQuery() is false");
    }


    /**
     *  Matches assets with any source. 
     *
     *  @param  match <code> true </code> to match assets with any source, 
     *          <code> false </code> to match assets with no sources 
     */

    @OSID @Override
    public void matchAnySource(boolean match) {
        getAssembler().addIdWildcardTerm(getSourceColumn(), match);
        return;
    }


    /**
     *  Clears the source terms. 
     */

    @OSID @Override
    public void clearSourceTerms() {
        getAssembler().clearTerms(getSourceColumn());
        return;
    }


    /**
     *  Gets the source query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a source order interface is available. 
     *
     *  @return <code> true </code> if a source search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the source order. 
     *
     *  @return the resource search order for the source 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSourceSearchOrder() is false");
    }


    /**
     *  Gets the Source column name.
     *
     * @return the column name
     */

    protected String getSourceColumn() {
        return ("source");
    }


    /**
     *  Match assets that are created between the specified time period. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedDate(org.osid.calendaring.DateTime start, 
                                 org.osid.calendaring.DateTime end, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getCreatedDateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches assets with any creation time. 
     *
     *  @param  match <code> true </code> to match assets with any created 
     *          time, <code> false </code> to match assets with no cerated 
     *          time 
     */

    @OSID @Override
    public void matchAnyCreatedDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getCreatedDateColumn(), match);
        return;
    }


    /**
     *  Clears the created time terms. 
     */

    @OSID @Override
    public void clearCreatedDateTerms() {
        getAssembler().clearTerms(getCreatedDateColumn());
        return;
    }


    /**
     *  Gets the created time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCreatedDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by created date. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreatedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreatedDateColumn(), style);
        return;
    }


    /**
     *  Gets the CreatedDate column name.
     *
     * @return the column name
     */

    protected String getCreatedDateColumn() {
        return ("created_date");
    }


    /**
     *  Marks assets that are marked as published. 
     *
     *  @param  published published flag 
     */

    @OSID @Override
    public void matchPublished(boolean published) {
        getAssembler().addBooleanTerm(getPublishedColumn(), published);
        return;
    }


    /**
     *  Clears the published terms. 
     */

    @OSID @Override
    public void clearPublishedTerms() {
        getAssembler().clearTerms(getPublishedColumn());
        return;
    }


    /**
     *  Gets the published query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getPublishedTerms() {
        return (getAssembler().getBooleanTerms(getPublishedColumn()));
    }


    /**
     *  Specifies a preference for grouping the result set by published 
     *  status. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPublished(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPublishedColumn(), style);
        return;
    }


    /**
     *  Gets the Published column name.
     *
     * @return the column name
     */

    protected String getPublishedColumn() {
        return ("published");
    }


    /**
     *  Match assets that are published between the specified time period. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPublishedDate(org.osid.calendaring.DateTime start, 
                                   org.osid.calendaring.DateTime end, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getPublishedDateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches assets with any published time. 
     *
     *  @param  match <code> true </code> to match assets with any published 
     *          time, <code> false </code> to match assets with no published 
     *          time 
     */

    @OSID @Override
    public void matchAnyPublishedDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getPublishedDateColumn(), match);
        return;
    }


    /**
     *  Clears the published time terms. 
     */

    @OSID @Override
    public void clearPublishedDateTerms() {
        getAssembler().clearTerms(getPublishedDateColumn());
        return;
    }


    /**
     *  Gets the published time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getPublishedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getPublishedDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by published date. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPublishedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPublishedDateColumn(), style);
        return;
    }


    /**
     *  Gets the PublishedDate column name.
     *
     * @return the column name
     */

    protected String getPublishedDateColumn() {
        return ("published_date");
    }


    /**
     *  Adds a principal credit string for this query. 
     *
     *  @param  credit credit string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> credit </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> credit </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPrincipalCreditString(String credit, 
                                           org.osid.type.Type stringMatchType, 
                                           boolean match) {
        getAssembler().addStringTerm(getPrincipalCreditStringColumn(), credit, stringMatchType, match);
        return;
    }


    /**
     *  Matches a principal credit string that has any value. 
     *
     *  @param  match <code> true </code> to match assets with any principal 
     *          credit string, <code> false </code> to match assets with no 
     *          principal credit string 
     */

    @OSID @Override
    public void matchAnyPrincipalCreditString(boolean match) {
        getAssembler().addStringWildcardTerm(getPrincipalCreditStringColumn(), match);
        return;
    }


    /**
     *  Clears the principal credit string terms. 
     */

    @OSID @Override
    public void clearPrincipalCreditStringTerms() {
        getAssembler().clearTerms(getPrincipalCreditStringColumn());
        return;
    }


    /**
     *  Gets the principal credit string query terms. 
     *
     *  @return the principal credit string terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPrincipalCreditStringTerms() {
        return (getAssembler().getStringTerms(getPrincipalCreditStringColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the principal 
     *  credit string. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPrincipalCreditString(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPrincipalCreditStringColumn(), style);
        return;
    }


    /**
     *  Gets the PrincipalCreditString column name.
     *
     * @return the column name
     */

    protected String getPrincipalCreditStringColumn() {
        return ("principal_credit_string");
    }


    /**
     *  Match assets that whose coverage falls between the specified time 
     *  period inclusive. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTemporalCoverage(org.osid.calendaring.DateTime start, 
                                      org.osid.calendaring.DateTime end, 
                                      boolean match) {
        getAssembler().addDateTimeRangeTerm(getTemporalCoverageColumn(), start, end, match);
        return;
    }


    /**
     *  Matches assets with any temporal coverage. 
     *
     *  @param  match <code> true </code> to match assets with any temporal 
     *          coverage, <code> false </code> to match assets with no 
     *          temporal coverage 
     */

    @OSID @Override
    public void matchAnyTemporalCoverage(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getTemporalCoverageColumn(), match);
        return;
    }


    /**
     *  Clears the temporal coverage terms. 
     */

    @OSID @Override
    public void clearTemporalCoverageTerms() {
        getAssembler().clearTerms(getTemporalCoverageColumn());
        return;
    }


    /**
     *  Gets the temporal coverage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTemporalCoverageTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTemporalCoverageColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by temporal 
     *  coverage. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTemporalCoverage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTemporalCoverageColumn(), style);
        return;
    }


    /**
     *  Gets the TemporalCoverage column name.
     *
     * @return the column name
     */

    protected String getTemporalCoverageColumn() {
        return ("temporal_coverage");
    }


    /**
     *  Sets the location <code> Id </code> for this query of spatial 
     *  coverage. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        getAssembler().addIdTerm(getLocationIdColumn(), locationId, match);
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        getAssembler().clearTerms(getLocationIdColumn());
        return;
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (getAssembler().getIdTerms(getLocationIdColumn()));
    }


    /**
     *  Gets the LocationId column name.
     *
     * @return the column name
     */

    protected String getLocationIdColumn() {
        return ("location_id");
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for the provider. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple queries can be retrieved for a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches assets with any provider. 
     *
     *  @param  match <code> true </code> to match assets with any location, 
     *          <code> false </code> to match assets with no locations 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        getAssembler().addIdWildcardTerm(getLocationColumn(), match);
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        getAssembler().clearTerms(getLocationColumn());
        return;
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the Location column name.
     *
     * @return the column name
     */

    protected String getLocationColumn() {
        return ("location");
    }


    /**
     *  Matches assets that are contained within the given spatial unit. 
     *
     *  @param  spatialUnit the spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> spatialUnit </code> is 
     *          not suppoted 
     */

    @OSID @Override
    public void matchSpatialCoverage(org.osid.mapping.SpatialUnit spatialUnit, 
                                     boolean match) {
        getAssembler().addSpatialUnitTerm(getSpatialCoverageColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Matches assets with no spatial coverage. 
     *
     *  @param  match <code> true </code> to match assets with any spatial 
     *          coverage, <code> false </code> to match assets with no spatial 
     *          coverage 
     */

    @OSID @Override
    public void matchAnySpatialCoverage(boolean match) {
        getAssembler().addSpatialUnitWildcardTerm(getSpatialCoverageColumn(), match);
        return;
    }


    /**
     *  Clears the spatial coverage terms. 
     */

    @OSID @Override
    public void clearSpatialCoverageTerms() {
        getAssembler().clearTerms(getSpatialCoverageColumn());
        return;
    }


    /**
     *  Gets the spatial coverage query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialCoverageTerms() {
        return (getAssembler().getSpatialUnitTerms(getSpatialCoverageColumn()));
    }


    /**
     *  Gets the SpatialCoverage column name.
     *
     * @return the column name
     */

    protected String getSpatialCoverageColumn() {
        return ("spatial_coverage");
    }


    /**
     *  Matches assets that overlap or touch the given spatial unit. 
     *
     *  @param  spatialUnit the spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> spatialUnit </code> is 
     *          not suppoted 
     */

    @OSID @Override
    public void matchSpatialCoverageOverlap(org.osid.mapping.SpatialUnit spatialUnit, 
                                            boolean match) {
        getAssembler().addSpatialUnitTerm(getSpatialCoverageOverlapColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Clears the spatial coverage overlap terms. 
     */

    @OSID @Override
    public void clearSpatialCoverageOverlapTerms() {
        getAssembler().clearTerms(getSpatialCoverageOverlapColumn());
        return;
    }


    /**
     *  Gets the spatial coverage overlap query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialCoverageOverlapTerms() {
        return (getAssembler().getSpatialUnitTerms(getSpatialCoverageOverlapColumn()));
    }


    /**
     *  Gets the SpatialCoverageOverlap column name.
     *
     * @return the column name
     */

    protected String getSpatialCoverageOverlapColumn() {
        return ("spatial_coverage_overlap");
    }


    /**
     *  Sets the asset content <code> Id </code> for this query. 
     *
     *  @param  assetContentId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetContentId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAssetContentId(org.osid.id.Id assetContentId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAssetContentIdColumn(), assetContentId, match);
        return;
    }


    /**
     *  Clears the asset content <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetContentIdTerms() {
        getAssembler().clearTerms(getAssetContentIdColumn());
        return;
    }


    /**
     *  Gets the asset content <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetContentIdTerms() {
        return (getAssembler().getIdTerms(getAssetContentIdColumn()));
    }


    /**
     *  Gets the AssetContentId column name.
     *
     * @return the column name
     */

    protected String getAssetContentIdColumn() {
        return ("asset_content_id");
    }


    /**
     *  Tests if an <code> AssetContentQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset content query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetContentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the asset content. Multiple queries can be 
     *  retrieved for a nested <code> OR </code> term. 
     *
     *  @return the asset contents query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetContentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetContentQuery getAssetContentQuery() {
        throw new org.osid.UnimplementedException("supportsAssetContentQuery() is false");
    }


    /**
     *  Matches assets with any content. 
     *
     *  @param  match <code> true </code> to match assets with any content, 
     *          <code> false </code> to match assets with no content 
     */

    @OSID @Override
    public void matchAnyAssetContent(boolean match) {
        getAssembler().addIdWildcardTerm(getAssetContentColumn(), match);
        return;
    }


    /**
     *  Clears the asset content terms. 
     */

    @OSID @Override
    public void clearAssetContentTerms() {
        getAssembler().clearTerms(getAssetContentColumn());
        return;
    }


    /**
     *  Gets the asset content query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetContentQueryInspector[] getAssetContentTerms() {
        return (new org.osid.repository.AssetContentQueryInspector[0]);
    }


    /**
     *  Gets the AssetContent column name.
     *
     * @return the column name
     */

    protected String getAssetContentColumn() {
        return ("asset_content");
    }


    /**
     *  Sets the composition <code> Id </code> for this query to match assets 
     *  that are a part of the composition. 
     *
     *  @param  compositionId the composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompositionId(org.osid.id.Id compositionId, boolean match) {
        getAssembler().addIdTerm(getCompositionIdColumn(), compositionId, match);
        return;
    }


    /**
     *  Clears the composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompositionIdTerms() {
        getAssembler().clearTerms(getCompositionIdColumn());
        return;
    }


    /**
     *  Gets the composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompositionIdTerms() {
        return (getAssembler().getIdTerms(getCompositionIdColumn()));
    }


    /**
     *  Gets the CompositionId column name.
     *
     * @return the column name
     */

    protected String getCompositionIdColumn() {
        return ("composition_id");
    }


    /**
     *  Tests if a <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple queries can be retrieved 
     *  for a nested <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsCompositionQuery() is false");
    }


    /**
     *  Matches assets with any composition mappings. 
     *
     *  @param  match <code> true </code> to match assets with any 
     *          composition, <code> false </code> to match assets with no 
     *          composition mappings 
     */

    @OSID @Override
    public void matchAnyComposition(boolean match) {
        getAssembler().addIdWildcardTerm(getCompositionColumn(), match);
        return;
    }


    /**
     *  Clears the composition terms. 
     */

    @OSID @Override
    public void clearCompositionTerms() {
        getAssembler().clearTerms(getCompositionColumn());
        return;
    }


    /**
     *  Gets the composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the Composition column name.
     *
     * @return the column name
     */

    protected String getCompositionColumn() {
        return ("composition");
    }


    /**
     *  Sets the repository <code> Id </code> for this query. 
     *
     *  @param  repositoryId the repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRepositoryId(org.osid.id.Id repositoryId, boolean match) {
        getAssembler().addIdTerm(getRepositoryIdColumn(), repositoryId, match);
        return;
    }


    /**
     *  Clears the repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRepositoryIdTerms() {
        getAssembler().clearTerms(getRepositoryIdColumn());
        return;
    }


    /**
     *  Gets the repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRepositoryIdTerms() {
        return (getAssembler().getIdTerms(getRepositoryIdColumn()));
    }


    /**
     *  Gets the RepositoryId column name.
     *
     * @return the column name
     */

    protected String getRepositoryIdColumn() {
        return ("repository_id");
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple queries can be retrieved for 
     *  a nested <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsRepositoryQuery() is false");
    }


    /**
     *  Clears the repository terms. 
     */

    @OSID @Override
    public void clearRepositoryTerms() {
        getAssembler().clearTerms(getRepositoryColumn());
        return;
    }


    /**
     *  Gets the repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }


    /**
     *  Gets the Repository column name.
     *
     * @return the column name
     */

    protected String getRepositoryColumn() {
        return ("repository");
    }


    /**
     *  Tests if this asset supports the given record
     *  <code>Type</code>.
     *
     *  @param  assetRecordType an asset record type 
     *  @return <code>true</code> if the assetRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assetRecordType) {
        for (org.osid.repository.records.AssetQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  assetRecordType the asset record type 
     *  @return the asset query record 
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetQueryRecord getAssetQueryRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  assetRecordType the asset record type 
     *  @return the asset query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetQueryInspectorRecord getAssetQueryInspectorRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(assetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param assetRecordType the asset record type
     *  @return the asset search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>assetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetSearchOrderRecord getAssetSearchOrderRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(assetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this asset. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assetQueryRecord the asset query record
     *  @param assetQueryInspectorRecord the asset query inspector
     *         record
     *  @param assetSearchOrderRecord the asset search order record
     *  @param assetRecordType asset record type
     *  @throws org.osid.NullArgumentException
     *          <code>assetQueryRecord</code>,
     *          <code>assetQueryInspectorRecord</code>,
     *          <code>assetSearchOrderRecord</code> or
     *          <code>assetRecordTypeasset</code> is
     *          <code>null</code>
     */
            
    protected void addAssetRecords(org.osid.repository.records.AssetQueryRecord assetQueryRecord, 
                                      org.osid.repository.records.AssetQueryInspectorRecord assetQueryInspectorRecord, 
                                      org.osid.repository.records.AssetSearchOrderRecord assetSearchOrderRecord, 
                                      org.osid.type.Type assetRecordType) {

        addRecordType(assetRecordType);

        nullarg(assetQueryRecord, "asset query record");
        nullarg(assetQueryInspectorRecord, "asset query inspector record");
        nullarg(assetSearchOrderRecord, "asset search odrer record");

        this.queryRecords.add(assetQueryRecord);
        this.queryInspectorRecords.add(assetQueryInspectorRecord);
        this.searchOrderRecords.add(assetSearchOrderRecord);
        
        return;
    }
}
