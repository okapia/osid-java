//
// AbstractMapActionGroupLookupSession
//
//    A simple framework for providing an ActionGroup lookup service
//    backed by a fixed collection of action groups.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an ActionGroup lookup service backed by a
 *  fixed collection of action groups. The action groups are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActionGroups</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapActionGroupLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractActionGroupLookupSession
    implements org.osid.control.ActionGroupLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.ActionGroup> actionGroups = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.ActionGroup>());


    /**
     *  Makes an <code>ActionGroup</code> available in this session.
     *
     *  @param  actionGroup an action group
     *  @throws org.osid.NullArgumentException <code>actionGroup<code>
     *          is <code>null</code>
     */

    protected void putActionGroup(org.osid.control.ActionGroup actionGroup) {
        this.actionGroups.put(actionGroup.getId(), actionGroup);
        return;
    }


    /**
     *  Makes an array of action groups available in this session.
     *
     *  @param  actionGroups an array of action groups
     *  @throws org.osid.NullArgumentException <code>actionGroups<code>
     *          is <code>null</code>
     */

    protected void putActionGroups(org.osid.control.ActionGroup[] actionGroups) {
        putActionGroups(java.util.Arrays.asList(actionGroups));
        return;
    }


    /**
     *  Makes a collection of action groups available in this session.
     *
     *  @param  actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException <code>actionGroups<code>
     *          is <code>null</code>
     */

    protected void putActionGroups(java.util.Collection<? extends org.osid.control.ActionGroup> actionGroups) {
        for (org.osid.control.ActionGroup actionGroup : actionGroups) {
            this.actionGroups.put(actionGroup.getId(), actionGroup);
        }

        return;
    }


    /**
     *  Removes an ActionGroup from this session.
     *
     *  @param  actionGroupId the <code>Id</code> of the action group
     *  @throws org.osid.NullArgumentException <code>actionGroupId<code> is
     *          <code>null</code>
     */

    protected void removeActionGroup(org.osid.id.Id actionGroupId) {
        this.actionGroups.remove(actionGroupId);
        return;
    }


    /**
     *  Gets the <code>ActionGroup</code> specified by its <code>Id</code>.
     *
     *  @param  actionGroupId <code>Id</code> of the <code>ActionGroup</code>
     *  @return the actionGroup
     *  @throws org.osid.NotFoundException <code>actionGroupId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>actionGroupId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getActionGroup(org.osid.id.Id actionGroupId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.ActionGroup actionGroup = this.actionGroups.get(actionGroupId);
        if (actionGroup == null) {
            throw new org.osid.NotFoundException("actionGroup not found: " + actionGroupId);
        }

        return (actionGroup);
    }


    /**
     *  Gets all <code>ActionGroups</code>. In plenary mode, the returned
     *  list contains all known actionGroups or an error
     *  results. Otherwise, the returned list may contain only those
     *  actionGroups that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ActionGroups</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroups()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.actiongroup.ArrayActionGroupList(this.actionGroups.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.actionGroups.clear();
        super.close();
        return;
    }
}
