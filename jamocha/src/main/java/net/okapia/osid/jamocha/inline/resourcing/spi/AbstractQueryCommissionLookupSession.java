//
// AbstractQueryCommissionLookupSession.java
//
//    An inline adapter that maps a CommissionLookupSession to
//    a CommissionQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CommissionLookupSession to
 *  a CommissionQuerySession.
 */

public abstract class AbstractQueryCommissionLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractCommissionLookupSession
    implements org.osid.resourcing.CommissionLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.resourcing.CommissionQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCommissionLookupSession.
     *
     *  @param querySession the underlying commission query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCommissionLookupSession(org.osid.resourcing.CommissionQuerySession querySession) {
        nullarg(querySession, "commission query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>Commission</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommissions() {
        return (this.session.canSearchCommissions());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commissions in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only commissions whose effective dates are current are
     *  returned by methods in this session.
     */

    public void useEffectiveCommissionView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All commissions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCommissionView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Commission</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Commission</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Commission</code> and
     *  retained for compatibility.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionId <code>Id</code> of the
     *          <code>Commission</code>
     *  @return the commission
     *  @throws org.osid.NotFoundException <code>commissionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commissionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Commission getCommission(org.osid.id.Id commissionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchId(commissionId, true);
        org.osid.resourcing.CommissionList commissions = this.session.getCommissionsByQuery(query);
        if (commissions.hasNext()) {
            return (commissions.getNextCommission());
        } 
        
        throw new org.osid.NotFoundException(commissionId + " not found");
    }


    /**
     *  Gets a <code>CommissionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commissions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Commissions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>commissionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByIds(org.osid.id.IdList commissionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();

        try (org.osid.id.IdList ids = commissionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a <code>CommissionList</code> corresponding to the given
     *  commission genus <code>Type</code> which does not include
     *  commissions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionGenusType a commission genus type 
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByGenusType(org.osid.type.Type commissionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchGenusType(commissionGenusType, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a <code>CommissionList</code> corresponding to the given
     *  commission genus <code>Type</code> and include any additional
     *  commissions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionGenusType a commission genus type 
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByParentGenusType(org.osid.type.Type commissionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchParentGenusType(commissionGenusType, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a <code>CommissionList</code> containing the given
     *  commission record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionRecordType a commission record type 
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByRecordType(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchRecordType(commissionRecordType, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a <code>CommissionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible
     *  through this session.
     *  
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Commission</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCommissionsByQuery(query));
    }
        

    /**
     *  Gets a list of commissions corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible
     *  through this session.
     *
     *  In effective mode, commissions are returned that are
     *  currently effective.  In any effective mode, effective
     *  commissions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.CommissionList getCommissionsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a list of commissions corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible
     *  through this session.
     *
     *  In effective mode, commissions are returned that are
     *  currently effective.  In any effective mode, effective
     *  commissions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a list of commissions corresponding to a work
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible
     *  through this session.
     *
     *  In effective mode, commissions are returned that are
     *  currently effective.  In any effective mode, effective
     *  commissions and those currently expired are returned.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>workId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.CommissionList getCommissionsForWork(org.osid.id.Id workId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchWorkId(workId, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a list of commissions corresponding to a work
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible
     *  through this session.
     *
     *  In effective mode, commissions are returned that are
     *  currently effective.  In any effective mode, effective
     *  commissions and those currently expired are returned.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForWorkOnDate(org.osid.id.Id workId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchWorkId(workId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a list of commissions corresponding to resource and work
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible
     *  through this session.
     *
     *  In effective mode, commissions are returned that are
     *  currently effective.  In any effective mode, effective
     *  commissions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  workId the <code>Id</code> of the work
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>workId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceAndWork(org.osid.id.Id resourceId,
                                                                               org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchWorkId(workId, true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets a list of commissions corresponding to resource and work
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible
     *  through this session.
     *
     *  In effective mode, commissions are returned that are
     *  currently effective.  In any effective mode, effective
     *  commissions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>workId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceAndWorkOnDate(org.osid.id.Id resourceId,
                                                                                     org.osid.id.Id workId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchWorkId(workId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommissionsByQuery(query));
    }

    
    /**
     *  Gets all <code>Commissions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Commissions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.CommissionQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCommissionsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.CommissionQuery getQuery() {
        org.osid.resourcing.CommissionQuery query = this.session.getCommissionQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
