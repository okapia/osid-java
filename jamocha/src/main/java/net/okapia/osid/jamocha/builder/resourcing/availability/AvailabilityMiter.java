//
// AvailabilityMiter.java
//
//     Defines an Availability miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.availability;


/**
 *  Defines an <code>Availability</code> miter for use with the builders.
 */

public interface AvailabilityMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.resourcing.Availability {


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the job.
     *
     *  @param job a job
     *  @throws org.osid.NullArgumentException <code>job</code> is
     *          <code>null</code>
     */

    public void setJob(org.osid.resourcing.Job job);


    /**
     *  Sets the competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    public void setCompetency(org.osid.resourcing.Competency competency);


    /**
     *  Sets the percentage availability.
     *
     *  @param percentage a percentage (0-100)
     *  @throws org.osid.InvalidArgumentException <code>percentage</code>
     *          is out of range
     */

    public void setPercentage(long percentage);


    /**
     *  Adds an Availability record.
     *
     *  @param record an availability record
     *  @param recordType the type of availability record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAvailabilityRecord(org.osid.resourcing.records.AvailabilityRecord record, org.osid.type.Type recordType);
}       


