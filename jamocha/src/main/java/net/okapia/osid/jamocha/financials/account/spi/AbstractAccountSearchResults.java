//
// AbstractAccountSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.account.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAccountSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.financials.AccountSearchResults {

    private org.osid.financials.AccountList accounts;
    private final org.osid.financials.AccountQueryInspector inspector;
    private final java.util.Collection<org.osid.financials.records.AccountSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAccountSearchResults.
     *
     *  @param accounts the result set
     *  @param accountQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>accounts</code>
     *          or <code>accountQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAccountSearchResults(org.osid.financials.AccountList accounts,
                                            org.osid.financials.AccountQueryInspector accountQueryInspector) {
        nullarg(accounts, "accounts");
        nullarg(accountQueryInspector, "account query inspectpr");

        this.accounts = accounts;
        this.inspector = accountQueryInspector;

        return;
    }


    /**
     *  Gets the account list resulting from a search.
     *
     *  @return an account list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccounts() {
        if (this.accounts == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.financials.AccountList accounts = this.accounts;
        this.accounts = null;
	return (accounts);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.financials.AccountQueryInspector getAccountQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  account search record <code> Type. </code> This method must
     *  be used to retrieve an account implementing the requested
     *  record.
     *
     *  @param accountSearchRecordType an account search 
     *         record type 
     *  @return the account search
     *  @throws org.osid.NullArgumentException
     *          <code>accountSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(accountSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountSearchResultsRecord getAccountSearchResultsRecord(org.osid.type.Type accountSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.financials.records.AccountSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(accountSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(accountSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record account search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAccountRecord(org.osid.financials.records.AccountSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "account record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
