//
// GradebookColumnSummaryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumnsummary.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradebookColumnSummaryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the GradebookColumnSummaryElement Id.
     *
     *  @return the gradebook column summary element Id
     */

    public static org.osid.id.Id getGradebookColumnSummaryEntityId() {
        return (makeEntityId("osid.grading.GradebookColumnSummary"));
    }


    /**
     *  Gets the GradebookColumnId element Id.
     *
     *  @return the GradebookColumnId element Id
     */

    public static org.osid.id.Id getGradebookColumnId() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.GradebookColumnId"));
    }


    /**
     *  Gets the GradebookColumn element Id.
     *
     *  @return the GradebookColumn element Id
     */

    public static org.osid.id.Id getGradebookColumn() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.GradebookColumn"));
    }


    /**
     *  Gets the Mean element Id.
     *
     *  @return the Mean element Id
     */

    public static org.osid.id.Id getMean() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.Mean"));
    }


    /**
     *  Gets the Median element Id.
     *
     *  @return the Median element Id
     */

    public static org.osid.id.Id getMedian() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.Median"));
    }


    /**
     *  Gets the Mode element Id.
     *
     *  @return the Mode element Id
     */

    public static org.osid.id.Id getMode() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.Mode"));
    }


    /**
     *  Gets the RMS element Id.
     *
     *  @return the RMS element Id
     */

    public static org.osid.id.Id getRMS() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.RMS"));
    }


    /**
     *  Gets the StandardDeviation element Id.
     *
     *  @return the StandardDeviation element Id
     */

    public static org.osid.id.Id getStandardDeviation() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.StandardDeviation"));
    }


    /**
     *  Gets the Sum element Id.
     *
     *  @return the Sum element Id
     */

    public static org.osid.id.Id getSum() {
        return (makeElementId("osid.grading.gradebookcolumnsummary.Sum"));
    }


    /**
     *  Gets the MinimumMean element Id.
     *
     *  @return the MinimumMean element Id
     */

    public static org.osid.id.Id getMinimumMean() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.MinimumMean"));
    }


    /**
     *  Gets the MinimumMedian element Id.
     *
     *  @return the MinimumMedian element Id
     */

    public static org.osid.id.Id getMinimumMedian() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.MinimumMedian"));
    }


    /**
     *  Gets the MinimumMode element Id.
     *
     *  @return the MinimumMode element Id
     */

    public static org.osid.id.Id getMinimumMode() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.MinimumMode"));
    }


    /**
     *  Gets the MinimumRMS element Id.
     *
     *  @return the MinimumRMS element Id
     */

    public static org.osid.id.Id getMinimumRMS() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.MinimumRMS"));
    }


    /**
     *  Gets the MinimumStandardDeviation element Id.
     *
     *  @return the MinimumStandardDeviation element Id
     */

    public static org.osid.id.Id getMinimumStandardDeviation() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.MinimumStandardDeviation"));
    }


    /**
     *  Gets the MinimumSum element Id.
     *
     *  @return the MinimumSum element Id
     */

    public static org.osid.id.Id getMinimumSum() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.MinimumSum"));
    }


    /**
     *  Gets the GradebookId element Id.
     *
     *  @return the GradebookId element Id
     */

    public static org.osid.id.Id getGradebookId() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.GradebookId"));
    }


    /**
     *  Gets the Gradebook element Id.
     *
     *  @return the Gradebook element Id
     */

    public static org.osid.id.Id getGradebook() {
        return (makeQueryElementId("osid.grading.gradebookcolumnsummary.Gradebook"));
    }
}
