//
// AbstractMutableController.java
//
//     Defines a mutable Controller.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.controller.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Controller</code>.
 */

public abstract class AbstractMutableController
    extends net.okapia.osid.jamocha.control.controller.spi.AbstractController
    implements org.osid.control.Controller,
               net.okapia.osid.jamocha.builder.control.controller.ControllerMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this controller. 
     *
     *  @param record controller record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addControllerRecord(org.osid.control.records.ControllerRecord record, org.osid.type.Type recordType) {
        super.addControllerRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this controller. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if disabled
     */
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (isDisabled()) {
            setDisabled(false);
        }

        return;
    }


    /**
     *  Disables this controller. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code>true</code> if disabled, <code>false<code>
     *         if enabled
     */
    
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);

        if (isEnabled()) {
            setEnabled(false);
        }

        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this controller.
     *
     *  @param displayName the name for this controller
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this controller.
     *
     *  @param description the description of this controller
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    @Override
    public void setAddress(String address) {
        super.setAddress(address);
        return;
    }


    /**
     *  Sets the model.
     *
     *  @param model a model
     *  @throws org.osid.NullArgumentException
     *          <code>model</code> is <code>null</code>
     */

    @Override
    public void setModel(org.osid.inventory.Model model) {
        super.setModel(model);
        return;
    }


    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @throws org.osid.NullArgumentException
     *          <code>version</code> is <code>null</code>
     */

    @Override
    public void setVersion(org.osid.installation.Version version) {
        super.setVersion(version);
        return;
    }


    /**
     *  Sets the toggleable.
     *
     *  @param toggleable {@code true} if a toggle, {@code false}
     *         otherwise
     */

    @Override
    public void setToggleable(boolean toggleable) {
        super.setToggleable(toggleable);
        return;
    }


    /**
     *  Sets the variable.
     *
     *  @param variable {@code true} if a variable dial, {@code
     *         false} otherwise
     */

    @Override
    public void setVariable(boolean variable) {
        super.setVariable(variable);
        return;
    }


    /**
     *  Sets the variable by percentage.
     *
     *  @param variableByPercentage {@code true} if a variable
     *         percentage dial, {@code false} otherwise
     */

    @Override
    public void setVariableByPercentage(boolean variableByPercentage) {
        super.setVariableByPercentage(variableByPercentage);
        return;
    }

    
    /**
     *  Sets the variable minimum.
     *
     *  @param value a variable minimum
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setVariableMinimum(java.math.BigDecimal value) {
        super.setVariableMinimum(value);
        return;
    }


    /**
     *  Sets the variable maximum.
     *
     *  @param value a variable maximum
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setVariableMaximum(java.math.BigDecimal value) {
        super.setVariableMaximum(value);
        return;
    }


    /**
     *  Sets the variable increment.
     *
     *  @param increment a variable increment
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    @Override
    public void setVariableIncrement(java.math.BigDecimal increment) {
        super.setVariableIncrement(increment);
        return;
    }


    /**
     *  Adds a discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void addDiscreetState(org.osid.process.State state) {
        super.addDiscreetState(state);
        return;
    }


    /**
     *  Sets all the discreet states.
     *
     *  @param states a collection of discreet states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    @Override
    public void setDiscreetStates(java.util.Collection<org.osid.process.State> states) {
        super.setDiscreetStates(states);
        return;
    }
}

