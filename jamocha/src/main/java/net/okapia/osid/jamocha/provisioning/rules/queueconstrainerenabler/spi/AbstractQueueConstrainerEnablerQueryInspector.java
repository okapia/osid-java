//
// AbstractQueueConstrainerEnablerQueryInspector.java
//
//     A template for making a QueueConstrainerEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for queue constrainer enablers.
 */

public abstract class AbstractQueueConstrainerEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.provisioning.rules.QueueConstrainerEnablerQueryInspector {

    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the queue constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueConstrainerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the queue constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerQueryInspector[] getRuledQueueConstrainerTerms() {
        return (new org.osid.provisioning.rules.QueueConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given queue constrainer enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a queue constrainer enabler implementing the requested record.
     *
     *  @param queueConstrainerEnablerRecordType a queue constrainer enabler record type
     *  @return the queue constrainer enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryInspectorRecord getQueueConstrainerEnablerQueryInspectorRecord(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue constrainer enabler query. 
     *
     *  @param queueConstrainerEnablerQueryInspectorRecord queue constrainer enabler query inspector
     *         record
     *  @param queueConstrainerEnablerRecordType queueConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueConstrainerEnablerQueryInspectorRecord(org.osid.provisioning.rules.records.QueueConstrainerEnablerQueryInspectorRecord queueConstrainerEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type queueConstrainerEnablerRecordType) {

        addRecordType(queueConstrainerEnablerRecordType);
        nullarg(queueConstrainerEnablerRecordType, "queue constrainer enabler record type");
        this.records.add(queueConstrainerEnablerQueryInspectorRecord);        
        return;
    }
}
