//
// AbstractQueryResourceRelationshipLookupSession.java
//
//    A ResourceRelationshipQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ResourceRelationshipQuerySession adapter.
 */

public abstract class AbstractAdapterResourceRelationshipQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.ResourceRelationshipQuerySession {

    private final org.osid.resource.ResourceRelationshipQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterResourceRelationshipQuerySession.
     *
     *  @param session the underlying resource relationship query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterResourceRelationshipQuerySession(org.osid.resource.ResourceRelationshipQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBin</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the {@codeBin</code> associated with this 
     *  session.
     *
     *  @return the {@codeBin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform {@codeResourceRelationship</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchResourceRelationships() {
        return (this.session.canSearchResourceRelationships());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resource relationships in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this bin only.
     */
    
    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    
      
    /**
     *  Gets a resource relationship query. The returned query will not have an
     *  extension query.
     *
     *  @return the resource relationship query 
     */
      
    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuery getResourceRelationshipQuery() {
        return (this.session.getResourceRelationshipQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  resourceRelationshipQuery the resource relationship query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code resourceRelationshipQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code resourceRelationshipQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByQuery(org.osid.resource.ResourceRelationshipQuery resourceRelationshipQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getResourceRelationshipsByQuery(resourceRelationshipQuery));
    }
}
