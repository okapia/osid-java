//
// AbstractAdapterCustomerLookupSession.java
//
//    A Customer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Customer lookup session adapter.
 */

public abstract class AbstractAdapterCustomerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.billing.CustomerLookupSession {

    private final org.osid.billing.CustomerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCustomerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCustomerLookupSession(org.osid.billing.CustomerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Customer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCustomers() {
        return (this.session.canLookupCustomers());
    }


    /**
     *  A complete view of the {@code Customer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCustomerView() {
        this.session.useComparativeCustomerView();
        return;
    }


    /**
     *  A complete view of the {@code Customer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCustomerView() {
        this.session.usePlenaryCustomerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include customers in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only customers whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCustomerView() {
        this.session.useEffectiveCustomerView();
        return;
    }
    

    /**
     *  All customers of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCustomerView() {
        this.session.useAnyEffectiveCustomerView();
        return;
    }

     
    /**
     *  Gets the {@code Customer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Customer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Customer} and
     *  retained for compatibility.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param customerId {@code Id} of the {@code Customer}
     *  @return the customer
     *  @throws org.osid.NotFoundException {@code customerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code customerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer(org.osid.id.Id customerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomer(customerId));
    }


    /**
     *  Gets a {@code CustomerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  customers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Customers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  customerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Customer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code customerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByIds(org.osid.id.IdList customerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByIds(customerIds));
    }


    /**
     *  Gets a {@code CustomerList} corresponding to the given
     *  customer genus {@code Type} which does not include
     *  customers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  customerGenusType a customer genus type 
     *  @return the returned {@code Customer} list
     *  @throws org.osid.NullArgumentException
     *          {@code customerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByGenusType(org.osid.type.Type customerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByGenusType(customerGenusType));
    }


    /**
     *  Gets a {@code CustomerList} corresponding to the given
     *  customer genus {@code Type} and include any additional
     *  customers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  customerGenusType a customer genus type 
     *  @return the returned {@code Customer} list
     *  @throws org.osid.NullArgumentException
     *          {@code customerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByParentGenusType(org.osid.type.Type customerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByParentGenusType(customerGenusType));
    }


    /**
     *  Gets a {@code CustomerList} containing the given
     *  customer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  customerRecordType a customer record type 
     *  @return the returned {@code Customer} list
     *  @throws org.osid.NullArgumentException
     *          {@code customerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByRecordType(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByRecordType(customerRecordType));
    }


    /**
     *  Gets a {@code CustomerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible
     *  through this session.
     *  
     *  In active mode, customers are returned that are currently
     *  active. In any status mode, active and inactive customers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Customer} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.billing.CustomerList getCustomersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersOnDate(from, to));
    }
        

    /**
     *  Gets a <code> CustomerList </code> related to the given
     *  customer number.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  number a customer number 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByNumber(number));
    }

    
    /**
     *  Gets a <code> CustomerList </code> related to the given
     *  resource.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByResource(resourceId));
    }

    
    /**
     *  Gets a <code> CustomerList </code> of the given resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective customers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByResourceOnDate(org.osid.id.Id resourceId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a <code> CustomerList </code> having the given activity.
     *  
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session.
     *  
     *  In effective mode, customers are returned that are currently
     *  effective. In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return the returned <code> CustomerList </code> list 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomersByActivity(activityId));
    }


    /**
     *  Gets all {@code Customers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  customers or an error results. Otherwise, the returned list
     *  may contain only those customers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, customers are returned that are currently
     *  effective.  In any effective mode, effective customers and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Customers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCustomers());
    }
}
