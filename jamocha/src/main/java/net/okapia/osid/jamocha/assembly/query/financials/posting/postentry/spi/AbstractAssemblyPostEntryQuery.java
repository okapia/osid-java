//
// AbstractAssemblyPostEntryQuery.java
//
//     A PostEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PostEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyPostEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.financials.posting.PostEntryQuery,
               org.osid.financials.posting.PostEntryQueryInspector,
               org.osid.financials.posting.PostEntrySearchOrder {

    private final java.util.Collection<org.osid.financials.posting.records.PostEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.posting.records.PostEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.posting.records.PostEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPostEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPostEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the post <code> Id </code> for this query. 
     *
     *  @param  postId a post <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPostId(org.osid.id.Id postId, boolean match) {
        getAssembler().addIdTerm(getPostIdColumn(), postId, match);
        return;
    }


    /**
     *  Clears the post <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostIdTerms() {
        getAssembler().clearTerms(getPostIdColumn());
        return;
    }


    /**
     *  Gets the post <code> Id </code> query terms. 
     *
     *  @return the post <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostIdTerms() {
        return (getAssembler().getIdTerms(getPostIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by post. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPostColumn(), style);
        return;
    }


    /**
     *  Gets the PostId column name.
     *
     * @return the column name
     */

    protected String getPostIdColumn() {
        return ("post_id");
    }


    /**
     *  Tests if a <code> PostQuery </code> is available. 
     *
     *  @return <code> true </code> if a post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the post query 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuery getPostQuery() {
        throw new org.osid.UnimplementedException("supportsPostQuery() is false");
    }


    /**
     *  Clears the post terms. 
     */

    @OSID @Override
    public void clearPostTerms() {
        getAssembler().clearTerms(getPostColumn());
        return;
    }


    /**
     *  Gets the post query terms. 
     *
     *  @return the post query terms 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQueryInspector[] getPostTerms() {
        return (new org.osid.financials.posting.PostQueryInspector[0]);
    }


    /**
     *  Tests if a post search order is available. 
     *
     *  @return <code> true </code> if a post search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSearchOrder() {
        return (false);
    }


    /**
     *  Gets the post order. 
     *
     *  @return the post search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchOrder getPostSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPostSearchOrder() is false");
    }


    /**
     *  Gets the Post column name.
     *
     * @return the column name
     */

    protected String getPostColumn() {
        return ("post");
    }


    /**
     *  Sets the account <code> Id </code> for this query. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        getAssembler().addIdTerm(getAccountIdColumn(), accountId, match);
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        getAssembler().clearTerms(getAccountIdColumn());
        return;
    }


    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (getAssembler().getIdTerms(getAccountIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by account. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAccount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAccountColumn(), style);
        return;
    }


    /**
     *  Gets the AccountId column name.
     *
     * @return the column name
     */

    protected String getAccountIdColumn() {
        return ("account_id");
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Matches entries that have any account set. 
     *
     *  @param  match <code> true </code> to match entries with any account, 
     *          <code> false </code> to match entries with no account 
     */

    @OSID @Override
    public void matchAnyAccount(boolean match) {
        getAssembler().addIdWildcardTerm(getAccountColumn(), match);
        return;
    }


    /**
     *  Clears the account terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        getAssembler().clearTerms(getAccountColumn());
        return;
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account query terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Tests if an account search order is available. 
     *
     *  @return <code> true </code> if an search account order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountSearchOrder() {
        return (false);
    }


    /**
     *  Gets the account order. 
     *
     *  @return the account search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchOrder getAccountSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAccountSearchOrder() is false");
    }


    /**
     *  Gets the Account column name.
     *
     * @return the column name
     */

    protected String getAccountColumn() {
        return ("account");
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by activity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActivityColumn(), style);
        return;
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches entries that have any activity set. 
     *
     *  @param  match <code> true </code> to match entries with any activity, 
     *          <code> false </code> to match entries with no activity 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Tests if an activity search order is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity search order. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchOrder getActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivitySearchOrder() is false");
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getAmountColumn(), low, high, match);
        return;
    }


    /**
     *  Matches entries that have any amount set. 
     *
     *  @param  match <code> true </code> to match entries with any amount, 
     *          <code> false </code> to match entries with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getAmountColumn(), match);
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        getAssembler().clearTerms(getAmountColumn());
        return;
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getAmountColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAmountColumn(), style);
        return;
    }


    /**
     *  Gets the Amount column name.
     *
     * @return the column name
     */

    protected String getAmountColumn() {
        return ("amount");
    }


    /**
     *  Matches entries that have debit amounts. 
     *
     *  @param  match <code> true </code> to match entries with a debit 
     *          amount, <code> false </code> to match entries with a credit 
     *          amount 
     */

    @OSID @Override
    public void matchDebit(boolean match) {
        getAssembler().addBooleanTerm(getDebitColumn(), match);
        return;
    }


    /**
     *  Clears the debit terms. 
     */

    @OSID @Override
    public void clearDebitTerms() {
        getAssembler().clearTerms(getDebitColumn());
        return;
    }


    /**
     *  Gets the debit query terms. 
     *
     *  @return the debit query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDebitTerms() {
        return (getAssembler().getBooleanTerms(getDebitColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the debit flag. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDebit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDebitColumn(), style);
        return;
    }


    /**
     *  Gets the Debit column name.
     *
     * @return the column name
     */

    protected String getDebitColumn() {
        return ("debit");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match post 
     *  entries assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this postEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  postEntryRecordType a post entry record type 
     *  @return <code>true</code> if the postEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type postEntryRecordType) {
        for (org.osid.financials.posting.records.PostEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  postEntryRecordType the post entry record type 
     *  @return the post entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntryQueryRecord getPostEntryQueryRecord(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  postEntryRecordType the post entry record type 
     *  @return the post entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntryQueryInspectorRecord getPostEntryQueryInspectorRecord(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param postEntryRecordType the post entry record type
     *  @return the post entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntrySearchOrderRecord getPostEntrySearchOrderRecord(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this post entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param postEntryQueryRecord the post entry query record
     *  @param postEntryQueryInspectorRecord the post entry query inspector
     *         record
     *  @param postEntrySearchOrderRecord the post entry search order record
     *  @param postEntryRecordType post entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryQueryRecord</code>,
     *          <code>postEntryQueryInspectorRecord</code>,
     *          <code>postEntrySearchOrderRecord</code> or
     *          <code>postEntryRecordTypepostEntry</code> is
     *          <code>null</code>
     */
            
    protected void addPostEntryRecords(org.osid.financials.posting.records.PostEntryQueryRecord postEntryQueryRecord, 
                                      org.osid.financials.posting.records.PostEntryQueryInspectorRecord postEntryQueryInspectorRecord, 
                                      org.osid.financials.posting.records.PostEntrySearchOrderRecord postEntrySearchOrderRecord, 
                                      org.osid.type.Type postEntryRecordType) {

        addRecordType(postEntryRecordType);

        nullarg(postEntryQueryRecord, "post entry query record");
        nullarg(postEntryQueryInspectorRecord, "post entry query inspector record");
        nullarg(postEntrySearchOrderRecord, "post entry search odrer record");

        this.queryRecords.add(postEntryQueryRecord);
        this.queryInspectorRecords.add(postEntryQueryInspectorRecord);
        this.searchOrderRecords.add(postEntrySearchOrderRecord);
        
        return;
    }
}
