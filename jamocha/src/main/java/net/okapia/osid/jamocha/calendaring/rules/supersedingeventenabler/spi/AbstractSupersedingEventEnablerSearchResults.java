//
// AbstractSupersedingEventEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSupersedingEventEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.rules.SupersedingEventEnablerSearchResults {

    private org.osid.calendaring.rules.SupersedingEventEnablerList supersedingEventEnablers;
    private final org.osid.calendaring.rules.SupersedingEventEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSupersedingEventEnablerSearchResults.
     *
     *  @param supersedingEventEnablers the result set
     *  @param supersedingEventEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnablers</code>
     *          or <code>supersedingEventEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSupersedingEventEnablerSearchResults(org.osid.calendaring.rules.SupersedingEventEnablerList supersedingEventEnablers,
                                            org.osid.calendaring.rules.SupersedingEventEnablerQueryInspector supersedingEventEnablerQueryInspector) {
        nullarg(supersedingEventEnablers, "superseding event enablers");
        nullarg(supersedingEventEnablerQueryInspector, "superseding event enabler query inspectpr");

        this.supersedingEventEnablers = supersedingEventEnablers;
        this.inspector = supersedingEventEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the superseding event enabler list resulting from a search.
     *
     *  @return a superseding event enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablers() {
        if (this.supersedingEventEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.rules.SupersedingEventEnablerList supersedingEventEnablers = this.supersedingEventEnablers;
        this.supersedingEventEnablers = null;
	return (supersedingEventEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.rules.SupersedingEventEnablerQueryInspector getSupersedingEventEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  superseding event enabler search record <code> Type. </code> This method must
     *  be used to retrieve a supersedingEventEnabler implementing the requested
     *  record.
     *
     *  @param supersedingEventEnablerSearchRecordType a supersedingEventEnabler search 
     *         record type 
     *  @return the superseding event enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(supersedingEventEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerSearchResultsRecord getSupersedingEventEnablerSearchResultsRecord(org.osid.type.Type supersedingEventEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.rules.records.SupersedingEventEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(supersedingEventEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record superseding event enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSupersedingEventEnablerRecord(org.osid.calendaring.rules.records.SupersedingEventEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "superseding event enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
