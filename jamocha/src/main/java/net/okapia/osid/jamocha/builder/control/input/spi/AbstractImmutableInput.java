//
// AbstractImmutableInput.java
//
//     Wraps a mutable Input to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.input.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Input</code> to hide modifiers. This
 *  wrapper provides an immutized Input from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying input whose state changes are visible.
 */

public abstract class AbstractImmutableInput
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.control.Input {

    private final org.osid.control.Input input;


    /**
     *  Constructs a new <code>AbstractImmutableInput</code>.
     *
     *  @param input the input to immutablize
     *  @throws org.osid.NullArgumentException <code>input</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableInput(org.osid.control.Input input) {
        super(input);
        this.input = input;
        return;
    }


    /**
     *  Gets the device <code> Id. </code> 
     *
     *  @return the device <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDeviceId() {
        return (this.input.getDeviceId());
    }


    /**
     *  Gets the device. 
     *
     *  @return the device 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Device getDevice()
        throws org.osid.OperationFailedException {

        return (this.input.getDevice());
    }


    /**
     *  Gets the controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getControllerId() {
        return (this.input.getControllerId());
    }


    /**
     *  Gets the controller. 
     *
     *  @return the controller 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getController()
        throws org.osid.OperationFailedException {

        return (this.input.getController());
    }


    /**
     *  Gets the input record corresponding to the given <code> Input </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> inputRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(inputRecordType) </code> is <code> true </code> . 
     *
     *  @param  inputRecordType the type of input record to retrieve 
     *  @return the input record 
     *  @throws org.osid.NullArgumentException <code> inputRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(inputRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.records.InputRecord getInputRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        return (this.input.getInputRecord(inputRecordType));
    }
}

