//
// ReadOnlyDateTimeMetadata.java
//
//     Defines a read-only datetime Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a read-only datetime Metadata.
 */

public final class ReadOnlyDateTimeMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new {@code ReadOnlyDateTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param label the element label
     *  @param instructions the instructions
     *  @param exists {@code true} is a value exists for this element,
     *         {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId},
     *          {@code label}, or {@code instructions} is {@code null}
     */

    public ReadOnlyDateTimeMetadata(org.osid.id.Id elementId, 
                                    org.osid.locale.DisplayText label, 
                                    org.osid.locale.DisplayText instructions, boolean exists) {
        
        super(org.osid.Syntax.DATETIME, elementId, false, false);

        setReadOnly(true);
        setLabel(label);
        setInstructions(instructions);
        setValueExists(exists);

        return;
    }
}