//
// AbstractConferral.java
//
//     Defines a Conferral.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Conferral</code>.
 */

public abstract class AbstractConferral
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.recognition.Conferral {

    private org.osid.recognition.Award award;
    private org.osid.resource.Resource recipient;
    private org.osid.id.Id referenceId;
    private org.osid.recognition.Convocation convocation;

    private final java.util.Collection<org.osid.recognition.records.ConferralRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the subscriber's award. 
     *
     *  @return the subscriber <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAwardId() {
        return (this.award.getId());
    }


    /**
     *  Gets the subscriber's award. 
     *
     *  @return the subscriber's award. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward()
        throws org.osid.OperationFailedException {

        return (this.award);
    }


    /**
     *  Sets the award.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException
     *          <code>award</code> is <code>null</code>
     */

    protected void setAward(org.osid.recognition.Award award) {
        nullarg(award, "award");
        this.award = award;
        return;
    }


    /**
     *  Gets the <code>Id</code> of the recipient.
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipientId() {
        return (this.recipient.getId());
    }


    /**
     *  Gets the recipient (e.g. Samuel Goldwyn).
     *
     *  @return the resource
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRecipient()
        throws org.osid.OperationFailedException {

        return (this.recipient);
    }


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException
     *          <code>recipient</code> is <code>null</code>
     */

    protected void setRecipient(org.osid.resource.Resource recipient) {
        nullarg(recipient, "recipient");
        this.recipient = recipient;
        return;
    }


    /**
     *  Tests if the was conferred for a reference object. 
     *
     *  @return <code> true </code> if a reference exists, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasReference() {
        return (this.referenceId != null);
    }


    /**
     *  Gets the <code> Id </code> of the reference (e.g. "Best Years of Our 
     *  Lives"). 
     *
     *  @return the reference <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasReference() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        if (!hasReference()) {
            throw new org.osid.IllegalStateException("hasReference() is false");
        }

        return (this.referenceId);
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    protected void setReferenceId(org.osid.id.Id referenceId) {
        nullarg(referenceId, "reference Id");
        this.referenceId = referenceId;
        return;
    }


    /**
     *  Tests if the award was conferred as part of a convocation. 
     *
     *  @return <code> true </code> if a convocation exists, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasConvocation() {
        return (this.convocation != null);
    }


    /**
     *  .Gets the <code> Id </code> of the convocation. 
     *
     *  @return the convocation <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasConvocation() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getConvocationId() {
        if (!hasConvocation()) {
            throw new org.osid.IllegalStateException("hasConvocation() is false");
        }

        return (this.convocation.getId());
    }


    /**
     *  Gets the convocation (e.g. 19th Academy Awards for the time period 
     *  1946). 
     *
     *  @return the convocation 
     *  @throws org.osid.IllegalStateException <code> hasConvocation() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Convocation getConvocation()
        throws org.osid.OperationFailedException {

        if (!hasConvocation()) {
            throw new org.osid.IllegalStateException("hasConvocation() is false");
        }

        return (this.convocation);
    }


    /**
     *  Sets the convocation.
     *
     *  @param convocation a convocation
     *  @throws org.osid.NullArgumentException
     *          <code>convocation</code> is <code>null</code>
     */

    protected void setConvocation(org.osid.recognition.Convocation convocation) {
        nullarg(convocation, "convocation");
        this.convocation = convocation;
        return;
    }


    /**
     *  Tests if this conferral supports the given record
     *  <code>Type</code>.
     *
     *  @param  conferralRecordType a conferral record type 
     *  @return <code>true</code> if the conferralRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type conferralRecordType) {
        for (org.osid.recognition.records.ConferralRecord record : this.records) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Conferral</code> record <code>Type</code>.
     *
     *  @param  conferralRecordType the conferral record type 
     *  @return the conferral record 
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(conferralRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralRecord getConferralRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConferralRecord record : this.records) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralRecordType + " is not supported");
    }


    /**
     *  Adds a record to this conferral. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param conferralRecord the conferral record
     *  @param conferralRecordType conferral record type
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecord</code> or
     *          <code>conferralRecordTypeconferral</code> is
     *          <code>null</code>
     */
            
    protected void addConferralRecord(org.osid.recognition.records.ConferralRecord conferralRecord, 
                                      org.osid.type.Type conferralRecordType) {

        nullarg(conferralRecord, "conferral record");
        addRecordType(conferralRecordType);
        this.records.add(conferralRecord);
        
        return;
    }
}
