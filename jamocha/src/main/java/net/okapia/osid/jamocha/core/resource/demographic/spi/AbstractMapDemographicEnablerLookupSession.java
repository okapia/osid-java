//
// AbstractMapDemographicEnablerLookupSession
//
//    A simple framework for providing a DemographicEnabler lookup service
//    backed by a fixed collection of demographic enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a DemographicEnabler lookup service backed by a
 *  fixed collection of demographic enablers. The demographic enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>DemographicEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDemographicEnablerLookupSession
    extends net.okapia.osid.jamocha.resource.demographic.spi.AbstractDemographicEnablerLookupSession
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resource.demographic.DemographicEnabler> demographicEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resource.demographic.DemographicEnabler>());


    /**
     *  Makes a <code>DemographicEnabler</code> available in this session.
     *
     *  @param  demographicEnabler a demographic enabler
     *  @throws org.osid.NullArgumentException <code>demographicEnabler<code>
     *          is <code>null</code>
     */

    protected void putDemographicEnabler(org.osid.resource.demographic.DemographicEnabler demographicEnabler) {
        this.demographicEnablers.put(demographicEnabler.getId(), demographicEnabler);
        return;
    }


    /**
     *  Makes an array of demographic enablers available in this session.
     *
     *  @param  demographicEnablers an array of demographic enablers
     *  @throws org.osid.NullArgumentException <code>demographicEnablers<code>
     *          is <code>null</code>
     */

    protected void putDemographicEnablers(org.osid.resource.demographic.DemographicEnabler[] demographicEnablers) {
        putDemographicEnablers(java.util.Arrays.asList(demographicEnablers));
        return;
    }


    /**
     *  Makes a collection of demographic enablers available in this session.
     *
     *  @param  demographicEnablers a collection of demographic enablers
     *  @throws org.osid.NullArgumentException <code>demographicEnablers<code>
     *          is <code>null</code>
     */

    protected void putDemographicEnablers(java.util.Collection<? extends org.osid.resource.demographic.DemographicEnabler> demographicEnablers) {
        for (org.osid.resource.demographic.DemographicEnabler demographicEnabler : demographicEnablers) {
            this.demographicEnablers.put(demographicEnabler.getId(), demographicEnabler);
        }

        return;
    }


    /**
     *  Removes a DemographicEnabler from this session.
     *
     *  @param  demographicEnablerId the <code>Id</code> of the demographic enabler
     *  @throws org.osid.NullArgumentException <code>demographicEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeDemographicEnabler(org.osid.id.Id demographicEnablerId) {
        this.demographicEnablers.remove(demographicEnablerId);
        return;
    }


    /**
     *  Gets the <code>DemographicEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  demographicEnablerId <code>Id</code> of the <code>DemographicEnabler</code>
     *  @return the demographicEnabler
     *  @throws org.osid.NotFoundException <code>demographicEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>demographicEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnabler getDemographicEnabler(org.osid.id.Id demographicEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resource.demographic.DemographicEnabler demographicEnabler = this.demographicEnablers.get(demographicEnablerId);
        if (demographicEnabler == null) {
            throw new org.osid.NotFoundException("demographicEnabler not found: " + demographicEnablerId);
        }

        return (demographicEnabler);
    }


    /**
     *  Gets all <code>DemographicEnablers</code>. In plenary mode, the returned
     *  list contains all known demographicEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  demographicEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>DemographicEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographicenabler.ArrayDemographicEnablerList(this.demographicEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.demographicEnablers.clear();
        super.close();
        return;
    }
}
