//
// InvariantMapSpeedZoneLookupSession
//
//    Implements a SpeedZone lookup service backed by a fixed collection of
//    speedZones.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a SpeedZone lookup service backed by a fixed
 *  collection of speed zones. The speed zones are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapSpeedZoneLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapSpeedZoneLookupSession
    implements org.osid.mapping.path.SpeedZoneLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneLookupSession</code> with no
     *  speed zones.
     */

    public InvariantMapSpeedZoneLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneLookupSession</code> with a single
     *  speed zone.
     *  
     *  @param  speedZone a single speed zone
     *  @throws org.osid.NullArgumentException <code>speedZone</code>
     *          is <code>null</code>
     */

    public InvariantMapSpeedZoneLookupSession(org.osid.mapping.path.SpeedZone speedZone) {
        putSpeedZone(speedZone);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneLookupSession</code> using an
     *  array of speed zones.
     *
     *  @param  speedZones an array of speed zones
     *  @throws org.osid.NullArgumentException <code>speedZones</code>
     *          is <code>null</code>
     */

    public InvariantMapSpeedZoneLookupSession(org.osid.mapping.path.SpeedZone[] speedZones) {
        putSpeedZones(speedZones);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSpeedZoneLookupSession</code> using a
     *  collection of speed zones.
     *
     *  @param  speedZones a collection of speed zones
     *  @throws org.osid.NullArgumentException <code>speedZones</code>
     *          is <code>null</code>
     */

    public InvariantMapSpeedZoneLookupSession(java.util.Collection<? extends org.osid.mapping.path.SpeedZone> speedZones) {
        putSpeedZones(speedZones);
        return;
    }
}
