//
// AbstractProjectQueryInspector.java
//
//     A template for making a ProjectQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for projects.
 */

public abstract class AbstractProjectQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.room.construction.ProjectQueryInspector {

    private final java.util.Collection<org.osid.room.construction.records.ProjectQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given project query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a project implementing the requested record.
     *
     *  @param projectRecordType a project record type
     *  @return the project query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(projectRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectQueryInspectorRecord getProjectQueryInspectorRecord(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.ProjectQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(projectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(projectRecordType + " is not supported");
    }


    /**
     *  Adds a record to this project query. 
     *
     *  @param projectQueryInspectorRecord project query inspector
     *         record
     *  @param projectRecordType project record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProjectQueryInspectorRecord(org.osid.room.construction.records.ProjectQueryInspectorRecord projectQueryInspectorRecord, 
                                                   org.osid.type.Type projectRecordType) {

        addRecordType(projectRecordType);
        nullarg(projectRecordType, "project record type");
        this.records.add(projectQueryInspectorRecord);        
        return;
    }
}
