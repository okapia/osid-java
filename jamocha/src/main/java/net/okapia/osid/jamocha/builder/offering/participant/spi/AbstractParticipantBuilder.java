//
// AbstractParticipant.java
//
//     Defines a Participant builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.participant.spi;


/**
 *  Defines a <code>Participant</code> builder.
 */

public abstract class AbstractParticipantBuilder<T extends AbstractParticipantBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.offering.participant.ParticipantMiter participant;


    /**
     *  Constructs a new <code>AbstractParticipantBuilder</code>.
     *
     *  @param participant the participant to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractParticipantBuilder(net.okapia.osid.jamocha.builder.offering.participant.ParticipantMiter participant) {
        super(participant);
        this.participant = participant;
        return;
    }


    /**
     *  Builds the participant.
     *
     *  @return the new participant
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>participant</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.offering.Participant build() {
        (new net.okapia.osid.jamocha.builder.validator.offering.participant.ParticipantValidator(getValidations())).validate(this.participant);
        return (new net.okapia.osid.jamocha.builder.offering.participant.ImmutableParticipant(this.participant));
    }


    /**
     *  Gets the participant. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new participant
     */

    @Override
    public net.okapia.osid.jamocha.builder.offering.participant.ParticipantMiter getMiter() {
        return (this.participant);
    }


    /**
     *  Sets the offering.
     *
     *  @param offering an offering
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>offering</code> is <code>null</code>
     */

    public T offering(org.osid.offering.Offering offering) {
        getMiter().setOffering(offering);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriod</code> is <code>null</code>
     */

    public T timePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        getMiter().setTimePeriod(timePeriod);
        return (self());
    }


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    public T resultOption(org.osid.grading.GradeSystem resultOption) {
        getMiter().addResultOption(resultOption);
        return (self());
    }


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    public T resultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions) {
        getMiter().setResultOptions(resultOptions);
        return (self());
    }


    /**
     *  Adds a Participant record.
     *
     *  @param record a participant record
     *  @param recordType the type of participant record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.offering.records.ParticipantRecord record, org.osid.type.Type recordType) {
        getMiter().addParticipantRecord(record, recordType);
        return (self());
    }
}       


