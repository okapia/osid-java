//
// AbstractRequestTransaction.java
//
//     Defines a RequestTransaction builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.requesttransaction.spi;


/**
 *  Defines a <code>RequestTransaction</code> builder.
 */

public abstract class AbstractRequestTransactionBuilder<T extends AbstractRequestTransactionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.requesttransaction.RequestTransactionMiter requestTransaction;


    /**
     *  Constructs a new <code>AbstractRequestTransactionBuilder</code>.
     *
     *  @param requestTransaction the request transaction to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRequestTransactionBuilder(net.okapia.osid.jamocha.builder.provisioning.requesttransaction.RequestTransactionMiter requestTransaction) {
        super(requestTransaction);
        this.requestTransaction = requestTransaction;
        return;
    }


    /**
     *  Builds the request transaction.
     *
     *  @return the new request transaction
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.RequestTransaction build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.requesttransaction.RequestTransactionValidator(getValidations())).validate(this.requestTransaction);
        return (new net.okapia.osid.jamocha.builder.provisioning.requesttransaction.ImmutableRequestTransaction(this.requestTransaction));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the request transaction miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.requesttransaction.RequestTransactionMiter getMiter() {
        return (this.requestTransaction);
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>broker</code> is
     *          <code>null</code>
     */

    public T broker(org.osid.provisioning.Broker broker) {
        getMiter().setBroker(broker);
        return (self());
    }


    /**
     *  Sets the submit date.
     *
     *  @param date a submit date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T submitDate(org.osid.calendaring.DateTime date) {
        getMiter().setSubmitDate(date);
        return (self());
    }


    /**
     *  Sets the submitter.
     *
     *  @param submitter a submitter
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>submitter</code>
     *          is <code>null</code>
     */

    public T submitter(org.osid.resource.Resource submitter) {
        getMiter().setSubmitter(submitter);
        return (self());
    }


    /**
     *  Sets the submitting agent.
     *
     *  @param agent a submitting agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T submittingAgent(org.osid.authentication.Agent agent) {
        getMiter().setSubmittingAgent(agent);
        return (self());
    }


    /**
     *  Adds a request.
     *
     *  @param request a request
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>request</code> is
     *          <code>null</code>
     */

    public T request(org.osid.provisioning.Request request) {
        getMiter().addRequest(request);
        return (self());
    }


    /**
     *  Sets all the requests.
     *
     *  @param requests a collection of requests
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>requests</code>
     *          is <code>null</code>
     */

    public T requests(java.util.Collection<org.osid.provisioning.Request> requests) {
        getMiter().setRequests(requests);
        return (self());
    }


    /**
     *  Adds a RequestTransaction record.
     *
     *  @param record a request transaction record
     *  @param recordType the type of request transaction record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.records.RequestTransactionRecord record, org.osid.type.Type recordType) {
        getMiter().addRequestTransactionRecord(record, recordType);
        return (self());
    }
}       


