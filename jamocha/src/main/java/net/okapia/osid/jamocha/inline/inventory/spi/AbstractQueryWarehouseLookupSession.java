//
// AbstractQueryWarehouseLookupSession.java
//
//    An inline adapter that maps a WarehouseLookupSession to
//    a WarehouseQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a WarehouseLookupSession to
 *  a WarehouseQuerySession.
 */

public abstract class AbstractQueryWarehouseLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractWarehouseLookupSession
    implements org.osid.inventory.WarehouseLookupSession {

    private final org.osid.inventory.WarehouseQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryWarehouseLookupSession.
     *
     *  @param querySession the underlying warehouse query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryWarehouseLookupSession(org.osid.inventory.WarehouseQuerySession querySession) {
        nullarg(querySession, "warehouse query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Warehouse</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWarehouses() {
        return (this.session.canSearchWarehouses());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Warehouse</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Warehouse</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Warehouse</code> and
     *  retained for compatibility.
     *
     *  @param  warehouseId <code>Id</code> of the
     *          <code>Warehouse</code>
     *  @return the warehouse
     *  @throws org.osid.NotFoundException <code>warehouseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>warehouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.WarehouseQuery query = getQuery();
        query.matchId(warehouseId, true);
        org.osid.inventory.WarehouseList warehouses = this.session.getWarehousesByQuery(query);
        if (warehouses.hasNext()) {
            return (warehouses.getNextWarehouse());
        } 
        
        throw new org.osid.NotFoundException(warehouseId + " not found");
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  warehouses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Warehouses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  warehouseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByIds(org.osid.id.IdList warehouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.WarehouseQuery query = getQuery();

        try (org.osid.id.IdList ids = warehouseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getWarehousesByQuery(query));
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  warehouse genus <code>Type</code> which does not include
     *  warehouses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.WarehouseQuery query = getQuery();
        query.matchGenusType(warehouseGenusType, true);
        return (this.session.getWarehousesByQuery(query));
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  warehouse genus <code>Type</code> and include any additional
     *  warehouses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByParentGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.WarehouseQuery query = getQuery();
        query.matchParentGenusType(warehouseGenusType, true);
        return (this.session.getWarehousesByQuery(query));
    }


    /**
     *  Gets a <code>WarehouseList</code> containing the given
     *  warehouse record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  warehouseRecordType a warehouse record type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByRecordType(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.WarehouseQuery query = getQuery();
        query.matchRecordType(warehouseRecordType, true);
        return (this.session.getWarehousesByQuery(query));
    }


    /**
     *  Gets a <code>WarehouseList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known warehouses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  warehouses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Warehouse</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.WarehouseQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getWarehousesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Warehouses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Warehouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.WarehouseQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getWarehousesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.inventory.WarehouseQuery getQuery() {
        org.osid.inventory.WarehouseQuery query = this.session.getWarehouseQuery();
        return (query);
    }
}
