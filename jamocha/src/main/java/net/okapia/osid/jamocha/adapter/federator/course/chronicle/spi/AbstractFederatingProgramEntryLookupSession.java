//
// AbstractFederatingProgramEntryLookupSession.java
//
//     An abstract federating adapter for a ProgramEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProgramEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProgramEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.chronicle.ProgramEntryLookupSession>
    implements org.osid.course.chronicle.ProgramEntryLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingProgramEntryLookupSession</code>.
     */

    protected AbstractFederatingProgramEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.chronicle.ProgramEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProgramEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProgramEntries() {
        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            if (session.canLookupProgramEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ProgramEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProgramEntryView() {
        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            session.useComparativeProgramEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ProgramEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProgramEntryView() {
        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            session.usePlenaryProgramEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include program entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only program entries whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveProgramEntryView() {
        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            session.useEffectiveProgramEntryView();
        }

        return;
    }


    /**
     *  All program entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProgramEntryView() {
        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            session.useAnyEffectiveProgramEntryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>ProgramEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProgramEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programEntryId <code>Id</code> of the
     *          <code>ProgramEntry</code>
     *  @return the program entry
     *  @throws org.osid.NotFoundException <code>programEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntry getProgramEntry(org.osid.id.Id programEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            try {
                return (session.getProgramEntry(programEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(programEntryId + " not found");
    }


    /**
     *  Gets a <code>ProgramEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programEntries specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>ProgramEntries</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByIds(org.osid.id.IdList programEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.chronicle.programentry.MutableProgramEntryList ret = new net.okapia.osid.jamocha.course.chronicle.programentry.MutableProgramEntryList();

        try (org.osid.id.IdList ids = programEntryIds) {
            while (ids.hasNext()) {
                ret.addProgramEntry(getProgramEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramEntryList</code> corresponding to the
     *  given program entry genus <code>Type</code> which does not
     *  include program entries of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programEntryGenusType a programEntry genus type 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByGenusType(org.osid.type.Type programEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesByGenusType(programEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramEntryList</code> corresponding to the given
     *  program entry genus <code>Type</code> and include any additional
     *  program entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programEntryGenusType a programEntry genus type 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByParentGenusType(org.osid.type.Type programEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesByParentGenusType(programEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramEntryList</code> containing the given
     *  program entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programEntryRecordType a programEntry record type 
     *  @return the returned <code>ProgramEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByRecordType(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesByRecordType(programEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session.
     *  
     *  In active mode, program entries are returned that are
     *  currently active. In any status mode, active and inactive
     *  program entries are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of program entries corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesForStudent(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of program entries corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesForStudentOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of program entries corresponding to a program
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForProgram(org.osid.id.Id programId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesForProgram(programId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of program entries corresponding to a program
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForProgramOnDate(org.osid.id.Id programId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesForProgramOnDate(programId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of program entries corresponding to student and
     *  program <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  programId the <code>Id</code> of the program
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>programId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentAndProgram(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id programId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesForStudentAndProgram(resourceId, programId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of program entries corresponding to student and
     *  program <code>Ids</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @param  programId the <code>Id</code> of the program
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProgramEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>programId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentAndProgramOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id programId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntriesForStudentAndProgramOnDate(resourceId, programId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ProgramEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known program
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective program
     *  entries and those currently expired are returned.
     *
     *  @return a list of <code>ProgramEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList ret = getProgramEntryList();

        for (org.osid.course.chronicle.ProgramEntryLookupSession session : getSessions()) {
            ret.addProgramEntryList(session.getProgramEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.FederatingProgramEntryList getProgramEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.ParallelProgramEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.chronicle.programentry.CompositeProgramEntryList());
        }
    }
}
