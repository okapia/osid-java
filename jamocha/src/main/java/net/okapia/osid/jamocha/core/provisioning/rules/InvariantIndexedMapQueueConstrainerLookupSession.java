//
// InvariantIndexedMapQueueConstrainerLookupSession
//
//    Implements a QueueConstrainer lookup service backed by a fixed
//    collection of queueConstrainers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a QueueConstrainer lookup service backed by a fixed
 *  collection of queue constrainers. The queue constrainers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some queue constrainers may be compatible
 *  with more types than are indicated through these queue constrainer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapQueueConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractIndexedMapQueueConstrainerLookupSession
    implements org.osid.provisioning.rules.QueueConstrainerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapQueueConstrainerLookupSession} using an
     *  array of queueConstrainers.
     *
     *  @param distributor the distributor
     *  @param queueConstrainers an array of queue constrainers
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code queueConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapQueueConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                    org.osid.provisioning.rules.QueueConstrainer[] queueConstrainers) {

        setDistributor(distributor);
        putQueueConstrainers(queueConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapQueueConstrainerLookupSession} using a
     *  collection of queue constrainers.
     *
     *  @param distributor the distributor
     *  @param queueConstrainers a collection of queue constrainers
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code queueConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapQueueConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                    java.util.Collection<? extends org.osid.provisioning.rules.QueueConstrainer> queueConstrainers) {

        setDistributor(distributor);
        putQueueConstrainers(queueConstrainers);
        return;
    }
}
