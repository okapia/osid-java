//
// UtilityElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.utility.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class UtilityElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the UtilityElement Id.
     *
     *  @return the utility element Id
     */

    public static org.osid.id.Id getUtilityEntityId() {
        return (makeEntityId("osid.metering.Utility"));
    }


    /**
     *  Gets the MeterId element Id.
     *
     *  @return the MeterId element Id
     */

    public static org.osid.id.Id getMeterId() {
        return (makeQueryElementId("osid.metering.utility.MeterId"));
    }


    /**
     *  Gets the Meter element Id.
     *
     *  @return the Meter element Id
     */

    public static org.osid.id.Id getMeter() {
        return (makeQueryElementId("osid.metering.utility.Meter"));
    }


    /**
     *  Gets the AncestorUtilityId element Id.
     *
     *  @return the AncestorUtilityId element Id
     */

    public static org.osid.id.Id getAncestorUtilityId() {
        return (makeQueryElementId("osid.metering.utility.AncestorUtilityId"));
    }


    /**
     *  Gets the AncestorUtility element Id.
     *
     *  @return the AncestorUtility element Id
     */

    public static org.osid.id.Id getAncestorUtility() {
        return (makeQueryElementId("osid.metering.utility.AncestorUtility"));
    }


    /**
     *  Gets the DescendantUtilityId element Id.
     *
     *  @return the DescendantUtilityId element Id
     */

    public static org.osid.id.Id getDescendantUtilityId() {
        return (makeQueryElementId("osid.metering.utility.DescendantUtilityId"));
    }


    /**
     *  Gets the DescendantUtility element Id.
     *
     *  @return the DescendantUtility element Id
     */

    public static org.osid.id.Id getDescendantUtility() {
        return (makeQueryElementId("osid.metering.utility.DescendantUtility"));
    }
}
