//
// AbstractObjectiveQuery.java
//
//     A template for making an Objective Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for objectives.
 */

public abstract class AbstractObjectiveQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.learning.ObjectiveQuery {

    private final java.util.Collection<org.osid.learning.records.ObjectiveQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available for querying 
     *  activities. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches an objective that has any assessment assigned. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          assessment, <code> false </code> to match objectives with no 
     *          assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the knowledge category <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchKnowledgeCategoryId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears the knowledge category <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearKnowledgeCategoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying 
     *  knowledge categories. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKnowledgeCategoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a knowledge category. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKnowledgeCategoryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getKnowledgeCategoryQuery() {
        throw new org.osid.UnimplementedException("supportsKnowledgeCategoryQuery() is false");
    }


    /**
     *  Matches an objective that has any knowledge category. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          knowledge category, <code> false </code> to match objectives 
     *          with no knowledge category 
     */

    @OSID @Override
    public void matchAnyKnowledgeCategory(boolean match) {
        return;
    }


    /**
     *  Clears the knowledge category terms. 
     */

    @OSID @Override
    public void clearKnowledgeCategoryTerms() {
        return;
    }


    /**
     *  Sets the cognitive process <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCognitiveProcessId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears the cognitive process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCognitiveProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying 
     *  cognitive processes. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCognitiveProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cognitive process. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCognitiveProcessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getCognitiveProcessQuery() {
        throw new org.osid.UnimplementedException("supportsCognitiveProcessQuery() is false");
    }


    /**
     *  Matches an objective that has any cognitive process. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          cognitive process, <code> false </code> to match objectives 
     *          with no cognitive process 
     */

    @OSID @Override
    public void matchAnyCognitiveProcess(boolean match) {
        return;
    }


    /**
     *  Clears the cognitive process terms. 
     */

    @OSID @Override
    public void clearCognitiveProcessTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available for querying 
     *  activities. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches an objective that has any related activity. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          activity, <code> false </code> to match objectives with no 
     *          activity 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the requisite objective <code> Id </code> for this query. 
     *
     *  @param  requisiteObjectiveId a requisite objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteObjectiveId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRequisiteObjectiveId(org.osid.id.Id requisiteObjectiveId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the requisite objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRequisiteObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  requisite objectives. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite objective. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getRequisiteObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsRequisiteObjectiveQuery() is false");
    }


    /**
     *  Matches an objective that has any related requisite. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          requisite, <code> false </code> to match objectives with no 
     *          requisite 
     */

    @OSID @Override
    public void matchAnyRequisiteObjective(boolean match) {
        return;
    }


    /**
     *  Clears the requisite objective terms. 
     */

    @OSID @Override
    public void clearRequisiteObjectiveTerms() {
        return;
    }


    /**
     *  Sets the dependent objective <code> Id </code> to query objectives 
     *  dependent on the given objective. 
     *
     *  @param  dependentObjectiveId a dependent objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dependentObjectiveId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDependentObjectiveId(org.osid.id.Id dependentObjectiveId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the dependent objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDependentObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  dependent objectives. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepndentObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dependent objective. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependentObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getDependentObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsDependentObjectiveQuery() is false");
    }


    /**
     *  Matches an objective that has any related dependents. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          dependent, <code> false </code> to match objectives with no 
     *          dependents 
     */

    @OSID @Override
    public void matchAnyDependentObjective(boolean match) {
        return;
    }


    /**
     *  Clears the dependent objective terms. 
     */

    @OSID @Override
    public void clearDependentObjectiveTerms() {
        return;
    }


    /**
     *  Sets the equivalent objective <code> Id </code> to query equivalents. 
     *
     *  @param  equivalentObjectiveId an equivalent objective <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> equivalentObjectiveId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEquivalentObjectiveId(org.osid.id.Id equivalentObjectiveId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the equivalent objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEquivalentObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  equivalent objectives. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEquivalentObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an equivalent objective. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEquivalentObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getEquivalentObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsEquivalentObjectiveQuery() is false");
    }


    /**
     *  Matches an objective that has any related equivalents. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          equivalent, <code> false </code> to match objectives with no 
     *          equivalents 
     */

    @OSID @Override
    public void matchAnyEquivalentObjective(boolean match) {
        return;
    }


    /**
     *  Clears the equivalent objective terms. 
     */

    @OSID @Override
    public void clearEquivalentObjectiveTerms() {
        return;
    }


    /**
     *  Sets the objective <code> Id </code> for this query to match 
     *  objectives that have the specified objective as an ancestor. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the ancestor objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getAncestorObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorObjectiveQuery() is false");
    }


    /**
     *  Matches objectives that have any ancestor. 
     *
     *  @param  match <code> true </code> to match objective with any 
     *          ancestor, <code> false </code> to match root objectives 
     */

    @OSID @Override
    public void matchAnyAncestorObjective(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor objective query terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveTerms() {
        return;
    }


    /**
     *  Sets the objective <code> Id </code> for this query to match 
     *  objectives that have the specified objective as a descendant. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantObjectiveId(org.osid.id.Id objectiveId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the descendant objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getDescendantObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantObjectiveQuery() is false");
    }


    /**
     *  Matches objectives that have any ancestor. 
     *
     *  @param  match <code> true </code> to match objectives with any 
     *          ancestor, <code> false </code> to match leaf objectives 
     */

    @OSID @Override
    public void matchAnyDescendantObjective(boolean match) {
        return;
    }


    /**
     *  Clears the descendant objective query terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveTerms() {
        return;
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  objective banks. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveBankQuery() is false");
    }


    /**
     *  Clears the objective bank terms. 
     */

    @OSID @Override
    public void clearObjectiveBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given objective query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an objective implementing the requested record.
     *
     *  @param objectiveRecordType an objective record type
     *  @return the objective query record
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveQueryRecord getObjectiveQueryRecord(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveQueryRecord record : this.records) {
            if (record.implementsRecordType(objectiveRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveRecordType + " is not supported");
    }


    /**
     *  Adds a record to this objective query. 
     *
     *  @param objectiveQueryRecord objective query record
     *  @param objectiveRecordType objective record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObjectiveQueryRecord(org.osid.learning.records.ObjectiveQueryRecord objectiveQueryRecord, 
                                          org.osid.type.Type objectiveRecordType) {

        addRecordType(objectiveRecordType);
        nullarg(objectiveQueryRecord, "objective query record");
        this.records.add(objectiveQueryRecord);        
        return;
    }
}
