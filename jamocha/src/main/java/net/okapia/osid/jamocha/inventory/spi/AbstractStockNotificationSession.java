//
// AbstractStockNotificationSession.java
//
//     A template for making StockNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Stock} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Stock} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for stock entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractStockNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.StockNotificationSession {

    private boolean federated = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();


    /**
     *  Gets the {@code Warehouse/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Warehouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }

    
    /**
     *  Gets the {@code Warehouse} associated with this session.
     *
     *  @return the {@code Warehouse} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the {@code Warehouse}.
     *
     *  @param warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException {@code warehouse}
     *          is {@code null}
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can register for {@code Stock}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForStockNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for models in warehouses which
     *  are children of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new stocks. {@code
     *  StockReceiver.newStock()} is invoked when a new {@code Stock}
     *  is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified stock. {@code
     *  StockReceiver.newAncestorStock()} is invoked when the
     *  specified stock node gets a new ancestor.
     *
     *  @param stockId the {@code Id} of the
     *         {@code Stock} node to monitor
     *  @throws org.osid.NullArgumentException {@code stockId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewStockAncestors(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  stock. {@code StockReceiver.newDescendantStock()} is invoked
     *  when the specified stock node gets a new descendant.
     *
     *  @param stockId the {@code Id} of the
     *         {@code Stock} node to monitor
     *  @throws org.osid.NullArgumentException {@code stockId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewStockDescendants(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated stocks. {@code
     *  StockReceiver.changedStock()} is invoked when a stock is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated stock. {@code
     *  StockReceiver.changedStock()} is invoked when the specified
     *  stock is changed.
     *
     *  @param stockId the {@code Id} of the {@code Stock} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code stockId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted stocks. {@code
     *  StockReceiver.deletedStock()} is invoked when a stock is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted stock. {@code
     *  StockReceiver.deletedStock()} is invoked when the specified
     *  stock is deleted.
     *
     *  @param stockId the {@code Id} of the
     *          {@code Stock} to monitor
     *  @throws org.osid.NullArgumentException {@code stockId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified stock. {@code
     *  StockReceiver.deletedAncestor()} is invoked when the specified
     *  stock node loses an ancestor.
     *
     *  @param stockId the {@code Id} of the
     *         {@code Stock} node to monitor
     *  @throws org.osid.NullArgumentException {@code stockId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedStockAncestors(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified stock. {@code
     *  StockReceiver.deletedDescendant()} is invoked when the
     *  specified stock node loses a descendant.
     *
     *  @param stockId the {@code Id} of the
     *          {@code Stock} node to monitor
     *  @throws org.osid.NullArgumentException {@code stockId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedStockDescendants(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
