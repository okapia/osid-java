//
// AbstractMutableStatistic.java
//
//     Defines a mutable Statistic.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.statistic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Statistic</code>.
 */

public abstract class AbstractMutableStatistic
    extends net.okapia.osid.jamocha.metering.statistic.spi.AbstractStatistic
    implements org.osid.metering.Statistic,
               net.okapia.osid.jamocha.builder.metering.statistic.StatisticMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this statistic. 
     *
     *  @param record statistic record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addStatisticRecord(org.osid.metering.records.StatisticRecord record, org.osid.type.Type recordType) {
        super.addStatisticRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }

     
    /**
     *  Sets the display name for this statistic.
     *
     *  @param displayName the name for this statistic
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this statistic.
     *
     *  @param description the description of this statistic
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }
   

    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the interpolated flag.
     *
     *  @param interpolated {@code true} is interpolated, {@code
     *         false} otherwise
     */

    @Override
    public void setInterpolated(boolean interpolated) {
        super.setInterpolated(interpolated);
        return;
    }
        

    /**
     *  Sets the extrapolated flag.
     *
     *  @param extrapolated {@code true} is extrapolated, {@code
     *         false} otherwise
     */

    @Override
    public void setExtrapolated(boolean extrapolated) {
        super.setExtrapolated(extrapolated);
        return;
    }


    /**
     *  Sets the meter.
     *
     *  @param meter a meter
     *  @throws org.osid.NullArgumentException <code>meter</code> is
     *          <code>null</code>
     */

    @Override
    public void setMeter(org.osid.metering.Meter meter) {
        super.setMeter(meter);
        return;
    }


    /**
     *  Sets the metered object id.
     *
     *  @param meteredObjectId a metered object id
     *  @throws org.osid.NullArgumentException
     *          <code>meteredObjectId</code> is <code>null</code>
     */

    @Override
    public void setMeteredObjectId(org.osid.id.Id meteredObjectId) {
        super.setMeteredObjectId(meteredObjectId);
        return;
    }


    /**
     *  Sets the sum.
     *
     *  @param sum the sum
     *  @throws org.osid.NullArgumentException <code>sum</code> is
     *          <code>null</code>
     */

    @Override
    public void setSum(java.math.BigDecimal sum) {
        super.setSum(sum);
        return;
    }


    /**
     *  Sets the mean.
     *
     *  @param mean the mean
     *  @throws org.osid.NullArgumentException <code>mean</code> is
     *          <code>null</code>
     */

    @Override
    public void setMean(java.math.BigDecimal mean) {
        super.setMean(mean);
        return;
    }


    /**
     *  Sets the median.
     *
     *  @param median the median
     *  @throws org.osid.NullArgumentException <code>median</code> is
     *          <code>null</code>
     */

    @Override
    public void setMedian(java.math.BigDecimal median) {
        super.setMedian(median);
        return;
    }


    /**
     *  Sets the mode.
     *
     *  @param mode the mode
     *  @throws org.osid.NullArgumentException <code>mode</code> is
     *          <code>null</code>
     */

    @Override
    public void setMode(java.math.BigDecimal mode) {
        super.setMode(mode);
        return;
    }


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation the standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    @Override
    public void setStandardDeviation(java.math.BigDecimal standardDeviation) {
        super.setStandardDeviation(standardDeviation);
        return;
    }


    /**
     *  Sets the root mean square.
     *
     *  @param rms the rms
     *  @throws org.osid.NullArgumentException <code>rms</code> is
     *          <code>null</code>
     */

    @Override
    public void setRMS(java.math.BigDecimal rms) {
        super.setRMS(rms);
        return;
    }


    /**
     *  Sets the delta.
     *
     *  @param delta the delta
     *  @throws org.osid.NullArgumentException <code>delta</code> is
     *          <code>null</code>
     */

    @Override
    public void setDelta(java.math.BigDecimal delta) {
        super.setDelta(delta);
        return;
    }


    /**
     *  Sets the percent change.
     *
     *  @param percent the percent change
     *  @throws org.osid.NullArgumentException <code>percent</code> is
     *          <code>null</code>
     */

    @Override
    public void setPercentChange(java.math.BigDecimal percent) {
        super.setPercentChange(percent);
        return;
    }


    /**
     *  Sets the average rate.
     *
     *  @param rate the average rate
     *  @param units the time units
     *  @throws org.osid.NullArgumentException <code>rate</code> or
     *          <code>units</code> is <code>null</code>
     */

    @Override
    public void setAverageRate(java.math.BigDecimal rate, org.osid.calendaring.DateTimeResolution units) {
        super.setAverageRate(rate, units);
        return;
    }
}

