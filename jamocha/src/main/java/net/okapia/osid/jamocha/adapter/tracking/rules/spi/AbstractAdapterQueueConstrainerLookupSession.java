//
// AbstractAdapterQueueConstrainerLookupSession.java
//
//    A QueueConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A QueueConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterQueueConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.tracking.rules.QueueConstrainerLookupSession {

    private final org.osid.tracking.rules.QueueConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterQueueConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterQueueConstrainerLookupSession(org.osid.tracking.rules.QueueConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code FrontOffice/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code FrontOffice Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the {@code FrontOffice} associated with this session.
     *
     *  @return the {@code FrontOffice} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform {@code QueueConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupQueueConstrainers() {
        return (this.session.canLookupQueueConstrainers());
    }


    /**
     *  A complete view of the {@code QueueConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueConstrainerView() {
        this.session.useComparativeQueueConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code QueueConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueConstrainerView() {
        this.session.usePlenaryQueueConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainers in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    

    /**
     *  Only active queue constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueConstrainerView() {
        this.session.useActiveQueueConstrainerView();
        return;
    }


    /**
     *  Active and inactive queue constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueConstrainerView() {
        this.session.useAnyStatusQueueConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code QueueConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code QueueConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code QueueConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @param queueConstrainerId {@code Id} of the {@code QueueConstrainer}
     *  @return the queue constrainer
     *  @throws org.osid.NotFoundException {@code queueConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code queueConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainer getQueueConstrainer(org.osid.id.Id queueConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainer(queueConstrainerId));
    }


    /**
     *  Gets a {@code QueueConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queueConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code QueueConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @param  queueConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code QueueConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByIds(org.osid.id.IdList queueConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainersByIds(queueConstrainerIds));
    }


    /**
     *  Gets a {@code QueueConstrainerList} corresponding to the given
     *  queue constrainer genus {@code Type} which does not include
     *  queue constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned {@code QueueConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainersByGenusType(queueConstrainerGenusType));
    }


    /**
     *  Gets a {@code QueueConstrainerList} corresponding to the given
     *  queue constrainer genus {@code Type} and include any additional
     *  queue constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned {@code QueueConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByParentGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainersByParentGenusType(queueConstrainerGenusType));
    }


    /**
     *  Gets a {@code QueueConstrainerList} containing the given
     *  queue constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @param  queueConstrainerRecordType a queueConstrainer record type 
     *  @return the returned {@code QueueConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByRecordType(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainersByRecordType(queueConstrainerRecordType));
    }


    /**
     *  Gets all {@code QueueConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @return a list of {@code QueueConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueueConstrainers());
    }
}
