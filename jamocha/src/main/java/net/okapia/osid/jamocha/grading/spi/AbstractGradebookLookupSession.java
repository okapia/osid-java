//
// AbstractGradebookLookupSession.java
//
//    A starter implementation framework for providing a Gradebook
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Gradebook
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getGradebooks(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractGradebookLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.grading.GradebookLookupSession {

    private boolean pedantic = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();
    

    /**
     *  Tests if this user can perform <code>Gradebook</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradebooks() {
        return (true);
    }


    /**
     *  A complete view of the <code>Gradebook</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradebookView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Gradebook</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradebookView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Gradebook</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Gradebook</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Gradebook</code> and
     *  retained for compatibility.
     *
     *  @param  gradebookId <code>Id</code> of the
     *          <code>Gradebook</code>
     *  @return the gradebook
     *  @throws org.osid.NotFoundException <code>gradebookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradebookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.grading.GradebookList gradebooks = getGradebooks()) {
            while (gradebooks.hasNext()) {
                org.osid.grading.Gradebook gradebook = gradebooks.getNextGradebook();
                if (gradebook.getId().equals(gradebookId)) {
                    return (gradebook);
                }
            }
        } 

        throw new org.osid.NotFoundException(gradebookId + " not found");
    }


    /**
     *  Gets a <code>GradebookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradebooks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Gradebooks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getGradebooks()</code>.
     *
     *  @param  gradebookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByIds(org.osid.id.IdList gradebookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.grading.Gradebook> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = gradebookIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getGradebook(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("gradebook " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.grading.gradebook.LinkedGradebookList(ret));
    }


    /**
     *  Gets a <code>GradebookList</code> corresponding to the given
     *  gradebook genus <code>Type</code> which does not include
     *  gradebooks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getGradebooks()</code>.
     *
     *  @param  gradebookGenusType a gradebook genus type 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByGenusType(org.osid.type.Type gradebookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradebook.GradebookGenusFilterList(getGradebooks(), gradebookGenusType));
    }


    /**
     *  Gets a <code>GradebookList</code> corresponding to the given
     *  gradebook genus <code>Type</code> and include any additional
     *  gradebooks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradebooks()</code>.
     *
     *  @param  gradebookGenusType a gradebook genus type 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByParentGenusType(org.osid.type.Type gradebookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGradebooksByGenusType(gradebookGenusType));
    }


    /**
     *  Gets a <code>GradebookList</code> containing the given
     *  gradebook record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGradebooks()</code>.
     *
     *  @param  gradebookRecordType a gradebook record type 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByRecordType(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.grading.gradebook.GradebookRecordFilterList(getGradebooks(), gradebookRecordType));
    }


    /**
     *  Gets a <code>GradebookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known gradebooks or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  gradebooks that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Gradebook</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.grading.gradebook.GradebookProviderFilterList(getGradebooks(), resourceId));
    }


    /**
     *  Gets all <code>Gradebooks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Gradebooks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.grading.GradebookList getGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the gradebook list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of gradebooks
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.grading.GradebookList filterGradebooksOnViews(org.osid.grading.GradebookList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
