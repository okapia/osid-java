//
// AbstractFederatingRelationshipEnablerLookupSession.java
//
//     An abstract federating adapter for a RelationshipEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.relationship.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RelationshipEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRelationshipEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.relationship.rules.RelationshipEnablerLookupSession>
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.relationship.Family family = new net.okapia.osid.jamocha.nil.relationship.family.UnknownFamily();


    /**
     *  Constructs a new <code>AbstractFederatingRelationshipEnablerLookupSession</code>.
     */

    protected AbstractFederatingRelationshipEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.relationship.rules.RelationshipEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Family/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Family Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.family.getId());
    }


    /**
     *  Gets the <code>Family</code> associated with this 
     *  session.
     *
     *  @return the <code>Family</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.family);
    }


    /**
     *  Sets the <code>Family</code>.
     *
     *  @param  family the family for this session
     *  @throws org.osid.NullArgumentException <code>family</code>
     *          is <code>null</code>
     */

    protected void setFamily(org.osid.relationship.Family family) {
        nullarg(family, "family");
        this.family = family;
        return;
    }


    /**
     *  Tests if this user can perform <code>RelationshipEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelationshipEnablers() {
        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            if (session.canLookupRelationshipEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>RelationshipEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelationshipEnablerView() {
        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            session.useComparativeRelationshipEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>RelationshipEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelationshipEnablerView() {
        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            session.usePlenaryRelationshipEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationship enablers in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            session.useFederatedFamilyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            session.useIsolatedFamilyView();
        }

        return;
    }


    /**
     *  Only active relationship enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRelationshipEnablerView() {
        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            session.useActiveRelationshipEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive relationship enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRelationshipEnablerView() {
        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            session.useAnyStatusRelationshipEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>RelationshipEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RelationshipEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>RelationshipEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerId <code>Id</code> of the
     *          <code>RelationshipEnabler</code>
     *  @return the relationship enabler
     *  @throws org.osid.NotFoundException <code>relationshipEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relationshipEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnabler getRelationshipEnabler(org.osid.id.Id relationshipEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            try {
                return (session.getRelationshipEnabler(relationshipEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(relationshipEnablerId + " not found");
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relationshipEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>RelationshipEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByIds(org.osid.id.IdList relationshipEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.relationship.rules.relationshipenabler.MutableRelationshipEnablerList ret = new net.okapia.osid.jamocha.relationship.rules.relationshipenabler.MutableRelationshipEnablerList();

        try (org.osid.id.IdList ids = relationshipEnablerIds) {
            while (ids.hasNext()) {
                ret.addRelationshipEnabler(getRelationshipEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> corresponding to the given
     *  relationship enabler genus <code>Type</code> which does not include
     *  relationship enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerGenusType a relationshipEnabler genus type 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByGenusType(org.osid.type.Type relationshipEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.FederatingRelationshipEnablerList ret = getRelationshipEnablerList();

        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            ret.addRelationshipEnablerList(session.getRelationshipEnablersByGenusType(relationshipEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> corresponding to the given
     *  relationship enabler genus <code>Type</code> and include any additional
     *  relationship enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerGenusType a relationshipEnabler genus type 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByParentGenusType(org.osid.type.Type relationshipEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.FederatingRelationshipEnablerList ret = getRelationshipEnablerList();

        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            ret.addRelationshipEnablerList(session.getRelationshipEnablersByParentGenusType(relationshipEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> containing the given
     *  relationship enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  relationshipEnablerRecordType a relationshipEnabler record type 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByRecordType(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.FederatingRelationshipEnablerList ret = getRelationshipEnablerList();

        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            ret.addRelationshipEnablerList(session.getRelationshipEnablersByRecordType(relationshipEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session.
     *  
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RelationshipEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.FederatingRelationshipEnablerList ret = getRelationshipEnablerList();

        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            ret.addRelationshipEnablerList(session.getRelationshipEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>RelationshipEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.FederatingRelationshipEnablerList ret = getRelationshipEnablerList();

        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            ret.addRelationshipEnablerList(session.getRelationshipEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>RelationshipEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  relationship enablers or an error results. Otherwise, the
     *  returned list may contain only those relationship enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, relationship enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  relationship enablers are returned.
     *
     *  @return a list of <code>RelationshipEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.FederatingRelationshipEnablerList ret = getRelationshipEnablerList();

        for (org.osid.relationship.rules.RelationshipEnablerLookupSession session : getSessions()) {
            ret.addRelationshipEnablerList(session.getRelationshipEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.FederatingRelationshipEnablerList getRelationshipEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.ParallelRelationshipEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.relationship.rules.relationshipenabler.CompositeRelationshipEnablerList());
        }
    }
}
