//
// AbstractOrganization.java
//
//     Defines an Organization.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.organization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Organization</code>.
 */

public abstract class AbstractOrganization
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.personnel.Organization {

    private org.osid.locale.DisplayText displayLabel;

    private final java.util.Collection<org.osid.personnel.records.OrganizationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the display label or code for this organization. 
     *
     *  @return the display label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.displayLabel);
    }


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @throws org.osid.NullArgumentException
     *          <code>label</code> is <code>null</code>
     */

    protected void setDisplayLabel(org.osid.locale.DisplayText label) {
        nullarg(label, "display label");
        this.displayLabel = label;
        return;
    }


    /**
     *  Tests if this organization supports the given record
     *  <code>Type</code>.
     *
     *  @param  organizationRecordType an organization record type 
     *  @return <code>true</code> if the organizationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type organizationRecordType) {
        for (org.osid.personnel.records.OrganizationRecord record : this.records) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Organization</code> record <code>Type</code>.
     *
     *  @param  organizationRecordType the organization record type 
     *  @return the organization record 
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(organizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationRecord getOrganizationRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.OrganizationRecord record : this.records) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this organization. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param organizationRecord the organization record
     *  @param organizationRecordType organization record type
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecord</code> or
     *          <code>organizationRecordTypeorganization</code> is
     *          <code>null</code>
     */
            
    protected void addOrganizationRecord(org.osid.personnel.records.OrganizationRecord organizationRecord, 
                                         org.osid.type.Type organizationRecordType) {

        nullarg(organizationRecord, "organization record");
        addRecordType(organizationRecordType);
        this.records.add(organizationRecord);
        
        return;
    }
}
