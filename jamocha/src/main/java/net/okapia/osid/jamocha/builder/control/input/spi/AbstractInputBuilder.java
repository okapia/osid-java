//
// AbstractInput.java
//
//     Defines an Input builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.input.spi;


/**
 *  Defines an <code>Input</code> builder.
 */

public abstract class AbstractInputBuilder<T extends AbstractInputBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.control.input.InputMiter input;


    /**
     *  Constructs a new <code>AbstractInputBuilder</code>.
     *
     *  @param input the input to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractInputBuilder(net.okapia.osid.jamocha.builder.control.input.InputMiter input) {
        super(input);
        this.input = input;
        return;
    }


    /**
     *  Builds the input.
     *
     *  @return the new input
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>input</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.control.Input build() {
        (new net.okapia.osid.jamocha.builder.validator.control.input.InputValidator(getValidations())).validate(this.input);
        return (new net.okapia.osid.jamocha.builder.control.input.ImmutableInput(this.input));
    }


    /**
     *  Gets the input. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new input
     */

    @Override
    public net.okapia.osid.jamocha.builder.control.input.InputMiter getMiter() {
        return (this.input);
    }


    /**
     *  Sets the device.
     *
     *  @param device a device
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>device</code> is <code>null</code>
     */

    public T device(org.osid.control.Device device) {
        getMiter().setDevice(device);
        return (self());
    }


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>controller</code> is <code>null</code>
     */

    public T controller(org.osid.control.Controller controller) {
        getMiter().setController(controller);
        return (self());
    }


    /**
     *  Adds an Input record.
     *
     *  @param record an input record
     *  @param recordType the type of input record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.control.records.InputRecord record, org.osid.type.Type recordType) {
        getMiter().addInputRecord(record, recordType);
        return (self());
    }
}       


