//
// Reading.java
//
//     Defines a Reading builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.reading;


/**
 *  Defines a <code>Reading</code> builder.
 */

public final class ReadingBuilder
    extends net.okapia.osid.jamocha.builder.metering.reading.spi.AbstractReadingBuilder<ReadingBuilder> {
    

    /**
     *  Constructs a new <code>ReadingBuilder</code> using a
     *  <code>MutableReading</code>.
     */

    public ReadingBuilder() {
        super(new MutableReading());
        return;
    }


    /**
     *  Constructs a new <code>ReadingBuilder</code> using the given
     *  mutable reading.
     * 
     *  @param reading
     */

    public ReadingBuilder(ReadingMiter reading) {
        super(reading);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return ReadingBuilder
     */

    @Override
    protected ReadingBuilder self() {
        return (this);
    }
}       


