//
// RelevancyMiter.java
//
//     Defines a Relevancy miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ontology.relevancy;


/**
 *  Defines a <code>Relevancy</code> miter for use with the builders.
 */

public interface RelevancyMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.ontology.Relevancy {


    /**
     *  Sets the subject.
     *
     *  @param subject a subject
     *  @throws org.osid.NullArgumentException <code>subject</code> is
     *          <code>null</code>
     */

    public void setSubject(org.osid.ontology.Subject subject);


    /**
     *  Sets the mapped id.
     *
     *  @param mappedId a mapped id
     *  @throws org.osid.NullArgumentException <code>mappedId</code>
     *          is <code>null</code>
     */

    public void setMappedId(org.osid.id.Id mappedId);


    /**
     *  Adds a Relevancy record.
     *
     *  @param record a relevancy record
     *  @param recordType the type of relevancy record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRelevancyRecord(org.osid.ontology.records.RelevancyRecord record, org.osid.type.Type recordType);
}       


