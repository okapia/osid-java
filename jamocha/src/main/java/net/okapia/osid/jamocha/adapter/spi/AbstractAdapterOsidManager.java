//
// AbstractAdapterOsidManager.java
//
//     A basic adapter for OsidManagers.
//
//
// Tom Coppeto
// Okapia
// 27 January 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This is a abstract class for implementing a basic OsidManager
 *  adapter. The underlying manager is already initialized and set
 *  using setAdapteeManager().
 */

public abstract class AbstractAdapterOsidManager<T extends org.osid.OsidManager>
    extends AbstractAdapterOsidProfile<T>
    implements org.osid.OsidManager {

    private org.osid.OsidRuntimeManager runtime;
    private net.okapia.osid.jamocha.spi.ManagerConfiguration configuration;
    private net.okapia.osid.jamocha.spi.ManagerLoader loader;


    /**
     *  Constructs a new {@code AbstractAdapterOsidManager} using the
     *  underlying provider.
     */

    protected AbstractAdapterOsidManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOsidManager}.
     *
     *  @param provider the provider identity for the adapter
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOsidManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with
     *          implementation configuration
     *  @throws org.osid.IllegalStateException this manager has
     *          already been initialized by the {@code OsidRuntime}
     *  @throws org.osid.NullArgumentException {@code runtime} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        this.runtime = runtime;
        this.configuration = new net.okapia.osid.jamocha.spi.ManagerConfiguration(runtime);
        this.loader = new net.okapia.osid.jamocha.spi.ManagerLoader(runtime);

        return;
    }


    /**
     *  Gets the runtime environment.
     *
     *  @return the runtime
     */

    protected org.osid.OsidRuntimeManager getRuntime() {
        return (this.runtime);
    }


    /**
     *  Sets the runtime environment.
     *
     *  @param runtime
     *  @throws org.osid.NullArgumentException <code>runtime</code> is null
     */

    protected void setRuntime(org.osid.OsidRuntimeManager runtime) {
        nullarg(runtime, "runtime");
        this.runtime = runtime;
        return;
    }


    /**
     *  Rolls back this service to a point in time. 
     *
     *  @param  rollbackTime the requested time 
     *  @return the journal entry corresponding to the actual state of this 
     *          service 
     *  @throws org.osid.NullArgumentException {@code rollbackTime} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     *  @throws org.osid.UnimplementedException {@code 
     *          supportsJournalRollback()} is {@code false} 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry rollbackService(java.util.Date rollbackTime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getAdapteeManager().rollbackService(rollbackTime));
    }


    /**
     *  Changes the service branch. 
     *
     *  @param  branchId the new service branch 
     *  @throws org.osid.NotFoundException {@code branchId} not found 
     *  @throws org.osid.NullArgumentException {@code branchId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     *  @throws org.osid.UnimplementedException {@code 
     *          supportsJournalBranching()} is {@code false} 
     */

    @OSID @Override
    public void changeBranch(org.osid.id.Id branchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        getAdapteeManager().changeBranch(branchId);
        return;
    }


    /**
     *  Instantiates an OsidManager.
     *
     *  @param osid the OSID to load
     *  @param  implClassName the name of the implementation 
     *  @return the OsidManager
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.NullArgumentException <code> implClassName </code> or 
     *          <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    protected org.osid.OsidManager getOsidManager(org.osid.OSID osid, String implClassName)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.loader == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.loader.getOsidManager(osid, implClassName));
    }


    /**
     *  Instantiates an OsidProxyManager.
     *
     *  @param osid the OSID to load
     *  @param  implClassName the name of the implementation 
     *  @return the OsidManager
     *  @throws org.osid.ConfigurationErrorException an error in configuring 
     *          the implementation
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.NullArgumentException <code> implClassName </code> or 
     *          <code> version </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete 
     *  @throws org.osid.UnsupportedException <code> implClassName </code> 
     *          does not support the requested OSID 
     */

    protected org.osid.OsidProxyManager getOsidProxyManager(org.osid.OSID osid, String implClassName)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.loader == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.loader.getOsidProxyManager(osid, implClassName));
    }


    /**
     *  Gets a <code> Value </code> for the given parameter <code>
     *  Id. </code> If more than one value exists for the given
     *  parameter, the most preferred value is returned. This method
     *  can be used as a convenience when only one value is
     *  expected. <code> getValuesByParameters() </code> should be
     *  used for getting all the active values.
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value 
     *  @throws org.osid.ConfigurationErrorException the <code>
     *          parameterId </code> not found or no value available
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.configuration.Value getConfigurationValue(org.osid.id.Id parameterId)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.configuration == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.configuration.getConfigurationValue(parameterId));
    }


    /**
     *  Gets all the <code> Values </code> for the given parameter
     *  <code> Id.  </code>
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.ConfigurationErrorException the <code>
     *          parameterId </code> not found
     *  @throws org.osid.IllegalStateException manager not initialized
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.configuration.ValueList getValuesByParameter(org.osid.id.Id parameterId)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.configuration == null) {
            throw new org.osid.IllegalStateException("manager not initialized");
        }

        return (this.configuration.getValuesByParameter(parameterId));
    }


    /**
     *  Closes this <code>osid.OsidManager</code>
     *
     *  @throws org.osid.IllegalStateException This manager has been closed.
     */

    @OSIDBinding @Override
    public void close() {

        /*
         *  If this is a runtime manager, then don't shutdown.
         */

        if (!(this instanceof org.osid.OsidRuntimeManager)) {
            getRuntime().close();
        }

        if (this.configuration != null) {
            try {
                this.configuration.close();
            } catch (Exception e) {}
        }

        return;
    }
}
