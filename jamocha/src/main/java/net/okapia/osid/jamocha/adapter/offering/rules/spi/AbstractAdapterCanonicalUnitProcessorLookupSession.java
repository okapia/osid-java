//
// AbstractAdapterCanonicalUnitProcessorLookupSession.java
//
//    A CanonicalUnitProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CanonicalUnitProcessor lookup session adapter.
 */

public abstract class AbstractAdapterCanonicalUnitProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {

    private final org.osid.offering.rules.CanonicalUnitProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCanonicalUnitProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCanonicalUnitProcessorLookupSession(org.osid.offering.rules.CanonicalUnitProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code CanonicalUnitProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitProcessors() {
        return (this.session.canLookupCanonicalUnitProcessors());
    }


    /**
     *  A complete view of the {@code CanonicalUnitProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitProcessorView() {
        this.session.useComparativeCanonicalUnitProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code CanonicalUnitProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitProcessorView() {
        this.session.usePlenaryCanonicalUnitProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit processors in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active canonical unit processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitProcessorView() {
        this.session.useActiveCanonicalUnitProcessorView();
        return;
    }


    /**
     *  Active and inactive canonical unit processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitProcessorView() {
        this.session.useAnyStatusCanonicalUnitProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code CanonicalUnitProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CanonicalUnitProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CanonicalUnitProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param canonicalUnitProcessorId {@code Id} of the {@code CanonicalUnitProcessor}
     *  @return the canonical unit processor
     *  @throws org.osid.NotFoundException {@code canonicalUnitProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessor getCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitProcessor(canonicalUnitProcessorId));
    }


    /**
     *  Gets a {@code CanonicalUnitProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CanonicalUnitProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CanonicalUnitProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByIds(org.osid.id.IdList canonicalUnitProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitProcessorsByIds(canonicalUnitProcessorIds));
    }


    /**
     *  Gets a {@code CanonicalUnitProcessorList} corresponding to the given
     *  canonical unit processor genus {@code Type} which does not include
     *  canonical unit processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorGenusType a canonicalUnitProcessor genus type 
     *  @return the returned {@code CanonicalUnitProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByGenusType(org.osid.type.Type canonicalUnitProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitProcessorsByGenusType(canonicalUnitProcessorGenusType));
    }


    /**
     *  Gets a {@code CanonicalUnitProcessorList} corresponding to the given
     *  canonical unit processor genus {@code Type} and include any additional
     *  canonical unit processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorGenusType a canonicalUnitProcessor genus type 
     *  @return the returned {@code CanonicalUnitProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByParentGenusType(org.osid.type.Type canonicalUnitProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitProcessorsByParentGenusType(canonicalUnitProcessorGenusType));
    }


    /**
     *  Gets a {@code CanonicalUnitProcessorList} containing the given
     *  canonical unit processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @param  canonicalUnitProcessorRecordType a canonicalUnitProcessor record type 
     *  @return the returned {@code CanonicalUnitProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessorsByRecordType(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitProcessorsByRecordType(canonicalUnitProcessorRecordType));
    }


    /**
     *  Gets all {@code CanonicalUnitProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit processors or an error results. Otherwise, the returned list
     *  may contain only those canonical unit processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit processors are returned that are currently
     *  active. In any status mode, active and inactive canonical unit processors
     *  are returned.
     *
     *  @return a list of {@code CanonicalUnitProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorList getCanonicalUnitProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitProcessors());
    }
}
