//
// AbstractIndexedMapProfileEntryEnablerLookupSession.java
//
//    A simple framework for providing a ProfileEntryEnabler lookup service
//    backed by a fixed collection of profile entry enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ProfileEntryEnabler lookup service backed by a
 *  fixed collection of profile entry enablers. The profile entry enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some profile entry enablers may be compatible
 *  with more types than are indicated through these profile entry enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProfileEntryEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProfileEntryEnablerLookupSession
    extends AbstractMapProfileEntryEnablerLookupSession
    implements org.osid.profile.rules.ProfileEntryEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.profile.rules.ProfileEntryEnabler> profileEntryEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.rules.ProfileEntryEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.profile.rules.ProfileEntryEnabler> profileEntryEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.rules.ProfileEntryEnabler>());


    /**
     *  Makes a <code>ProfileEntryEnabler</code> available in this session.
     *
     *  @param  profileEntryEnabler a profile entry enabler
     *  @throws org.osid.NullArgumentException <code>profileEntryEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProfileEntryEnabler(org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler) {
        super.putProfileEntryEnabler(profileEntryEnabler);

        this.profileEntryEnablersByGenus.put(profileEntryEnabler.getGenusType(), profileEntryEnabler);
        
        try (org.osid.type.TypeList types = profileEntryEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profileEntryEnablersByRecord.put(types.getNextType(), profileEntryEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a profile entry enabler from this session.
     *
     *  @param profileEntryEnablerId the <code>Id</code> of the profile entry enabler
     *  @throws org.osid.NullArgumentException <code>profileEntryEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProfileEntryEnabler(org.osid.id.Id profileEntryEnablerId) {
        org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler;
        try {
            profileEntryEnabler = getProfileEntryEnabler(profileEntryEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.profileEntryEnablersByGenus.remove(profileEntryEnabler.getGenusType());

        try (org.osid.type.TypeList types = profileEntryEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profileEntryEnablersByRecord.remove(types.getNextType(), profileEntryEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProfileEntryEnabler(profileEntryEnablerId);
        return;
    }


    /**
     *  Gets a <code>ProfileEntryEnablerList</code> corresponding to the given
     *  profile entry enabler genus <code>Type</code> which does not include
     *  profile entry enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known profile entry enablers or an error results. Otherwise,
     *  the returned list may contain only those profile entry enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  profileEntryEnablerGenusType a profile entry enabler genus type 
     *  @return the returned <code>ProfileEntryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersByGenusType(org.osid.type.Type profileEntryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.rules.profileentryenabler.ArrayProfileEntryEnablerList(this.profileEntryEnablersByGenus.get(profileEntryEnablerGenusType)));
    }


    /**
     *  Gets a <code>ProfileEntryEnablerList</code> containing the given
     *  profile entry enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known profile entry enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  profile entry enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  profileEntryEnablerRecordType a profile entry enabler record type 
     *  @return the returned <code>profileEntryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileEntryEnablerList getProfileEntryEnablersByRecordType(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.rules.profileentryenabler.ArrayProfileEntryEnablerList(this.profileEntryEnablersByRecord.get(profileEntryEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profileEntryEnablersByGenus.clear();
        this.profileEntryEnablersByRecord.clear();

        super.close();

        return;
    }
}
