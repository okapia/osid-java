//
// AbstractAssemblyInventoryQuery.java
//
//     An InventoryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inventory.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InventoryQuery that stores terms.
 */

public abstract class AbstractAssemblyInventoryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.inventory.InventoryQuery,
               org.osid.inventory.InventoryQueryInspector,
               org.osid.inventory.InventorySearchOrder {

    private final java.util.Collection<org.osid.inventory.records.InventoryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.InventoryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.InventorySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyInventoryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInventoryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the stock <code> Id </code> for this query. 
     *
     *  @param  stockId the stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        getAssembler().addIdTerm(getStockIdColumn(), stockId, match);
        return;
    }


    /**
     *  Clears the stock <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        getAssembler().clearTerms(getStockIdColumn());
        return;
    }


    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the stock <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (getAssembler().getIdTerms(getStockIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the stock. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStock(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStockColumn(), style);
        return;
    }


    /**
     *  Gets the StockId column name.
     *
     * @return the column name
     */

    protected String getStockIdColumn() {
        return ("stock_id");
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stocjk. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Clears the stock terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        getAssembler().clearTerms(getStockColumn());
        return;
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the stock query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Tests if a <code> StockSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a stock search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a stock. 
     *
     *  @return the stock search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchOrder getStockSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStockSearchOrder() is false");
    }


    /**
     *  Gets the Stock column name.
     *
     * @return the column name
     */

    protected String getStockColumn() {
        return ("stock");
    }


    /**
     *  Matches inventories where the given time falls within a denormalized 
     *  inventory date inclusive. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime date, boolean match) {
        getAssembler().addDateTimeTerm(getDateColumn(), date, match);
        return;
    }


    /**
     *  Clears the meeting time terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        getAssembler().clearTerms(getDateColumn());
        return;
    }


    /**
     *  Gets the date query terms. 
     *
     *  @return the date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getDateTerms() {
        return (getAssembler().getDateTimeTerms(getDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateColumn(), style);
        return;
    }


    /**
     *  Gets the Date column name.
     *
     * @return the column name
     */

    protected String getDateColumn() {
        return ("date");
    }


    /**
     *  Matches inventories with any denormalized date within the given date 
     *  range inclusive. 
     *
     *  @param  start a start date 
     *  @param  end an end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDateInclusive(org.osid.calendaring.DateTime start, 
                                   org.osid.calendaring.DateTime end, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateInclusiveColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the date inclusive terms. 
     */

    @OSID @Override
    public void clearDateInclusiveTerms() {
        getAssembler().clearTerms(getDateInclusiveColumn());
        return;
    }


    /**
     *  Gets the date query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateInclusiveTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateInclusiveColumn()));
    }


    /**
     *  Gets the DateInclusive column name.
     *
     * @return the column name
     */

    protected String getDateInclusiveColumn() {
        return ("date_inclusive");
    }


    /**
     *  Matches inventories with a quantity within the given range inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchQuantity(java.math.BigDecimal start, 
                              java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getQuantityColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the quantity terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        getAssembler().clearTerms(getQuantityColumn());
        return;
    }


    /**
     *  Gets the quantity terms. 
     *
     *  @return the quantity query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getQuantityTerms() {
        return (getAssembler().getDecimalRangeTerms(getQuantityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the quantity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuantity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQuantityColumn(), style);
        return;
    }


    /**
     *  Gets the Quantity column name.
     *
     * @return the column name
     */

    protected String getQuantityColumn() {
        return ("quantity");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match 
     *  inventories assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        getAssembler().addIdTerm(getWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        getAssembler().clearTerms(getWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getWarehouseIdColumn()));
    }


    /**
     *  Gets the WarehouseId column name.
     *
     * @return the column name
     */

    protected String getWarehouseIdColumn() {
        return ("warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        getAssembler().clearTerms(getWarehouseColumn());
        return;
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the Warehouse column name.
     *
     * @return the column name
     */

    protected String getWarehouseColumn() {
        return ("warehouse");
    }


    /**
     *  Tests if this inventory supports the given record
     *  <code>Type</code>.
     *
     *  @param  inventoryRecordType an inventory record type 
     *  @return <code>true</code> if the inventoryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inventoryRecordType) {
        for (org.osid.inventory.records.InventoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inventoryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  inventoryRecordType the inventory record type 
     *  @return the inventory query record 
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inventoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.InventoryQueryRecord getInventoryQueryRecord(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.InventoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inventoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inventoryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inventoryRecordType the inventory record type 
     *  @return the inventory query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inventoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.InventoryQueryInspectorRecord getInventoryQueryInspectorRecord(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.InventoryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(inventoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inventoryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param inventoryRecordType the inventory record type
     *  @return the inventory search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inventoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.InventorySearchOrderRecord getInventorySearchOrderRecord(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.InventorySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(inventoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inventoryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this inventory. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inventoryQueryRecord the inventory query record
     *  @param inventoryQueryInspectorRecord the inventory query inspector
     *         record
     *  @param inventorySearchOrderRecord the inventory search order record
     *  @param inventoryRecordType inventory record type
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryQueryRecord</code>,
     *          <code>inventoryQueryInspectorRecord</code>,
     *          <code>inventorySearchOrderRecord</code> or
     *          <code>inventoryRecordTypeinventory</code> is
     *          <code>null</code>
     */
            
    protected void addInventoryRecords(org.osid.inventory.records.InventoryQueryRecord inventoryQueryRecord, 
                                      org.osid.inventory.records.InventoryQueryInspectorRecord inventoryQueryInspectorRecord, 
                                      org.osid.inventory.records.InventorySearchOrderRecord inventorySearchOrderRecord, 
                                      org.osid.type.Type inventoryRecordType) {

        addRecordType(inventoryRecordType);

        nullarg(inventoryQueryRecord, "inventory query record");
        nullarg(inventoryQueryInspectorRecord, "inventory query inspector record");
        nullarg(inventorySearchOrderRecord, "inventory search odrer record");

        this.queryRecords.add(inventoryQueryRecord);
        this.queryInspectorRecords.add(inventoryQueryInspectorRecord);
        this.searchOrderRecords.add(inventorySearchOrderRecord);
        
        return;
    }
}
