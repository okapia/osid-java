//
// AbstractRace.java
//
//     Defines a Race.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.race.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Race</code>.
 */

public abstract class AbstractRace
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.voting.Race {

    private org.osid.voting.Ballot ballot;
    private final java.util.Collection<org.osid.voting.records.RaceRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the ballot <code> Id </code>. 
     *
     *  @return the ballot <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBallotId() {
        return (this.ballot.getId());
    }


    /**
     *  Gets the ballot. 
     *
     *  @return the ballot 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Ballot getBallot()
        throws org.osid.OperationFailedException {

        return (this.ballot);
    }


    /**
     *  Sets the ballot.
     *
     *  @param ballot a ballot
     *  @throws org.osid.NullArgumentException
     *          <code>ballot</code> is <code>null</code>
     */

    protected void setBallot(org.osid.voting.Ballot ballot) {
        nullarg(ballot, "ballot");
        this.ballot = ballot;
        return;
    }


    /**
     *  Tests if this race supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceRecordType a race record type 
     *  @return <code>true</code> if the raceRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceRecordType) {
        for (org.osid.voting.records.RaceRecord record : this.records) {
            if (record.implementsRecordType(raceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Race</code>
     *  record <code>Type</code>.
     *
     *  @param  raceRecordType the race record type 
     *  @return the race record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceRecord getRaceRecord(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.RaceRecord record : this.records) {
            if (record.implementsRecordType(raceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceRecord the race record
     *  @param raceRecordType race record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecord</code> or
     *          <code>raceRecordTyperace</code> is
     *          <code>null</code>
     */
            
    protected void addRaceRecord(org.osid.voting.records.RaceRecord raceRecord, 
                                 org.osid.type.Type raceRecordType) {

        nullarg(raceRecord, "race record");
        addRecordType(raceRecordType);
        this.records.add(raceRecord);
        
        return;
    }
}
