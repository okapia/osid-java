//
// AbstractQueryParameterProcessorLookupSession.java
//
//    A ParameterProcessorQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ParameterProcessorQuerySession adapter.
 */

public abstract class AbstractAdapterParameterProcessorQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.configuration.rules.ParameterProcessorQuerySession {

    private final org.osid.configuration.rules.ParameterProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterParameterProcessorQuerySession.
     *
     *  @param session the underlying parameter processor query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterParameterProcessorQuerySession(org.osid.configuration.rules.ParameterProcessorQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeConfiguration</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeConfiguration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the {@codeConfiguration</code> associated with this 
     *  session.
     *
     *  @return the {@codeConfiguration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform {@codeParameterProcessor</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchParameterProcessors() {
        return (this.session.canSearchParameterProcessors());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameter processors in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this configuration only.
     */
    
    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    
      
    /**
     *  Gets a parameter processor query. The returned query will not have an
     *  extension query.
     *
     *  @return the parameter processor query 
     */
      
    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuery getParameterProcessorQuery() {
        return (this.session.getParameterProcessorQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  parameterProcessorQuery the parameter processor query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code parameterProcessorQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code parameterProcessorQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByQuery(org.osid.configuration.rules.ParameterProcessorQuery parameterProcessorQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getParameterProcessorsByQuery(parameterProcessorQuery));
    }
}
