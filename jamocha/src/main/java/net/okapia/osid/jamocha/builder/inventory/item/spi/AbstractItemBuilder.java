//
// AbstractItem.java
//
//     Defines an Item builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.item.spi;


/**
 *  Defines an <code>Item</code> builder.
 */

public abstract class AbstractItemBuilder<T extends AbstractItemBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inventory.item.ItemMiter item;


    /**
     *  Constructs a new <code>AbstractItemBuilder</code>.
     *
     *  @param item the item to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractItemBuilder(net.okapia.osid.jamocha.builder.inventory.item.ItemMiter item) {
        super(item);
        this.item = item;
        return;
    }


    /**
     *  Builds the item.
     *
     *  @return the new item
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inventory.Item build() {
        (new net.okapia.osid.jamocha.builder.validator.inventory.item.ItemValidator(getValidations())).validate(this.item);
        return (new net.okapia.osid.jamocha.builder.inventory.item.ImmutableItem(this.item));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the item miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.inventory.item.ItemMiter getMiter() {
        return (this.item);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>stock</code> is
     *          <code>null</code>
     */

    public T stock(org.osid.inventory.Stock stock) {
        getMiter().setStock(stock);
        return (self());
    }


    /**
     *  Sets the property tag.
     *
     *  @param tag a property tag
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>tag</code> is
     *          <code>null</code>
     */

    public T propertyTag(String tag) {
        getMiter().setPropertyTag(tag);
        return (self());
    }


    /**
     *  Sets the serial number.
     *
     *  @param number a serial number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T serialNumber(String number) {
        getMiter().setSerialNumber(number);
        return (self());
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public T locationDescription(org.osid.locale.DisplayText description) {
        getMiter().setLocationDescription(description);
        return (self());
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public T location(org.osid.mapping.Location location) {
        getMiter().setLocation(location);
        return (self());
    }


    /**
     *  Sets the parent item.
     *
     *  @param item the parent item
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    public T parentItem(org.osid.inventory.Item item) {
        getMiter().setParentItem(item);
        return (self());
    }


    /**
     *  Adds an Item record.
     *
     *  @param record an item record
     *  @param recordType the type of item record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inventory.records.ItemRecord record, org.osid.type.Type recordType) {
        getMiter().addItemRecord(record, recordType);
        return (self());
    }
}       


