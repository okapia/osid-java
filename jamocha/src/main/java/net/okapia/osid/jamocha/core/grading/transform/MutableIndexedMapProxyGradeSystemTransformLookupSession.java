//
// MutableIndexedMapProxyGradeSystemTransformLookupSession
//
//    Implements a GradeSystemTransform lookup service backed by a collection of
//    gradeSystemTransforms indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.transform;


/**
 *  Implements a GradeSystemTransform lookup service backed by a collection of
 *  gradeSystemTransforms. The grade system transforms are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some gradeSystemTransforms may be compatible
 *  with more types than are indicated through these gradeSystemTransform
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of grade system transforms can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyGradeSystemTransformLookupSession
    extends net.okapia.osid.jamocha.core.grading.transform.spi.AbstractIndexedMapGradeSystemTransformLookupSession
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradeSystemTransformLookupSession} with
     *  no grade system transform.
     *
     *  @param gradebook the gradebook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                       org.osid.proxy.Proxy proxy) {
        setGradebook(gradebook);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradeSystemTransformLookupSession} with
     *  a single grade system transform.
     *
     *  @param gradebook the gradebook
     *  @param  gradeSystemTransform an grade system transform
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystemTransform}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                       org.osid.grading.transform.GradeSystemTransform gradeSystemTransform, org.osid.proxy.Proxy proxy) {

        this(gradebook, proxy);
        putGradeSystemTransform(gradeSystemTransform);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradeSystemTransformLookupSession} using
     *  an array of grade system transforms.
     *
     *  @param gradebook the gradebook
     *  @param  gradeSystemTransforms an array of grade system transforms
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystemTransforms}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                       org.osid.grading.transform.GradeSystemTransform[] gradeSystemTransforms, org.osid.proxy.Proxy proxy) {

        this(gradebook, proxy);
        putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyGradeSystemTransformLookupSession} using
     *  a collection of grade system transforms.
     *
     *  @param gradebook the gradebook
     *  @param  gradeSystemTransforms a collection of grade system transforms
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystemTransforms}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                       java.util.Collection<? extends org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms,
                                                       org.osid.proxy.Proxy proxy) {
        this(gradebook, proxy);
        putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }

    
    /**
     *  Makes a {@code GradeSystemTransform} available in this session.
     *
     *  @param  gradeSystemTransform a grade system transform
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransform{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeSystemTransform(org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        super.putGradeSystemTransform(gradeSystemTransform);
        return;
    }


    /**
     *  Makes an array of grade system transforms available in this session.
     *
     *  @param  gradeSystemTransforms an array of grade system transforms
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransforms{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeSystemTransforms(org.osid.grading.transform.GradeSystemTransform[] gradeSystemTransforms) {
        super.putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }


    /**
     *  Makes collection of grade system transforms available in this session.
     *
     *  @param  gradeSystemTransforms a collection of grade system transforms
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransform{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeSystemTransforms(java.util.Collection<? extends org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms) {
        super.putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }


    /**
     *  Removes a GradeSystemTransform from this session.
     *
     *  @param gradeSystemTransformId the {@code Id} of the grade system transform
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransformId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGradeSystemTransform(org.osid.id.Id gradeSystemTransformId) {
        super.removeGradeSystemTransform(gradeSystemTransformId);
        return;
    }    
}
