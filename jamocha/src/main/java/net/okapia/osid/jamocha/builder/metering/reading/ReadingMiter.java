//
// ReadingMiter.java
//
//     Defines a Reading miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.reading;


/**
 *  Defines a <code>Reading</code> miter for use with the builders.
 */

public interface ReadingMiter
    extends net.okapia.osid.jamocha.builder.spi.Miter,
            org.osid.metering.Reading {


    /**
     *  Sets the meter.
     *
     *  @param meter the meter
     *  @throws org.osid.NullArgumentException <code>meter</code> is
     *          <code>null</code>
     */

    public void setMeter(org.osid.metering.Meter meter);


    /**
     *  Sets the metered object Id.
     *
     *  @param objectId the metered object Id
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     */

    public void setMeteredObjectId(org.osid.id.Id objectId);


    /**
     *  Sets the amount.
     *
     *  @param amount the amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setAmount(java.math.BigDecimal amount);
}

