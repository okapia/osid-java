//
// ActionMiter.java
//
//     Defines an Action miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.action;


/**
 *  Defines an <code>Action</code> miter for use with the builders.
 */

public interface ActionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.control.Action {


    /**
     *  Sets the action group.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    public void setActionGroup(org.osid.control.ActionGroup actionGroup);


    /**
     *  Sets the delay.
     *
     *  @param delay a delay
     *  @throws org.osid.NullArgumentException <code>delay</code> is
     *          <code>null</code>
     */

    public void setDelay(org.osid.calendaring.Duration delay);


    /**
     *  Sets the next action group.
     *
     *  @param actionGroup the next action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    public void setNextActionGroup(org.osid.control.ActionGroup actionGroup);


    /**
     *  Sets the scene.
     *
     *  @param scene a scene
     *  @throws org.osid.NullArgumentException <code>scene</code> is
     *          <code>null</code>
     */

    public void setScene(org.osid.control.Scene scene);


    /**
     *  Sets the setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException <code>setting</code> is
     *          <code>null</code>
     */

    public void setSetting(org.osid.control.Setting setting);


    /**
     *  Sets the matching controller.
     *
     *  @param controller a matching controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    public void setMatchingController(org.osid.control.Controller controller);


    /**
     *  Sets the matching amount factor.
     *
     *  @param factor a matching amount factor
     *  @throws org.osid.NullArgumentException
     *          <code>factor</code> is <code>null</code>
     */

    public void setMatchingAmountFactor(java.math.BigDecimal factor);


    /**
     *  Sets the matching rate factor.
     *
     *  @param factor a matching rate factor
     *  @throws org.osid.NullArgumentException <code>factor</code> is
     *          <code>null</code>
     */

    public void setMatchingRateFactor(java.math.BigDecimal factor);


    /**
     *  Adds an Action record.
     *
     *  @param record an action record
     *  @param recordType the type of action record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addActionRecord(org.osid.control.records.ActionRecord record, org.osid.type.Type recordType);
}       


