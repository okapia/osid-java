//
// MutableIndexedMapFoundryLookupSession
//
//    Implements a Foundry lookup service backed by a collection of
//    foundries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Foundry lookup service backed by a collection of
 *  foundries. The foundries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some foundries may be compatible
 *  with more types than are indicated through these foundry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of foundries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapFoundryLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractIndexedMapFoundryLookupSession
    implements org.osid.resourcing.FoundryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFoundryLookupSession} with no
     *  foundries.
     */

    public MutableIndexedMapFoundryLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFoundryLookupSession} with a
     *  single foundry.
     *  
     *  @param  foundry a single foundry
     *  @throws org.osid.NullArgumentException {@code foundry}
     *          is {@code null}
     */

    public MutableIndexedMapFoundryLookupSession(org.osid.resourcing.Foundry foundry) {
        putFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFoundryLookupSession} using an
     *  array of foundries.
     *
     *  @param  foundries an array of foundries
     *  @throws org.osid.NullArgumentException {@code foundries}
     *          is {@code null}
     */

    public MutableIndexedMapFoundryLookupSession(org.osid.resourcing.Foundry[] foundries) {
        putFoundries(foundries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFoundryLookupSession} using a
     *  collection of foundries.
     *
     *  @param  foundries a collection of foundries
     *  @throws org.osid.NullArgumentException {@code foundries} is
     *          {@code null}
     */

    public MutableIndexedMapFoundryLookupSession(java.util.Collection<? extends org.osid.resourcing.Foundry> foundries) {
        putFoundries(foundries);
        return;
    }
    

    /**
     *  Makes a {@code Foundry} available in this session.
     *
     *  @param  foundry a foundry
     *  @throws org.osid.NullArgumentException {@code foundry{@code  is
     *          {@code null}
     */

    @Override
    public void putFoundry(org.osid.resourcing.Foundry foundry) {
        super.putFoundry(foundry);
        return;
    }


    /**
     *  Makes an array of foundries available in this session.
     *
     *  @param  foundries an array of foundries
     *  @throws org.osid.NullArgumentException {@code foundries{@code 
     *          is {@code null}
     */

    @Override
    public void putFoundries(org.osid.resourcing.Foundry[] foundries) {
        super.putFoundries(foundries);
        return;
    }


    /**
     *  Makes collection of foundries available in this session.
     *
     *  @param  foundries a collection of foundries
     *  @throws org.osid.NullArgumentException {@code foundry{@code  is
     *          {@code null}
     */

    @Override
    public void putFoundries(java.util.Collection<? extends org.osid.resourcing.Foundry> foundries) {
        super.putFoundries(foundries);
        return;
    }


    /**
     *  Removes a Foundry from this session.
     *
     *  @param foundryId the {@code Id} of the foundry
     *  @throws org.osid.NullArgumentException {@code foundryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeFoundry(org.osid.id.Id foundryId) {
        super.removeFoundry(foundryId);
        return;
    }    
}
