//
// AbstractQueryInstallationLookupSession.java
//
//    An InstallationQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InstallationQuerySession adapter.
 */

public abstract class AbstractAdapterInstallationQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.installation.InstallationQuerySession {

    private final org.osid.installation.InstallationQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterInstallationQuerySession.
     *
     *  @param session the underlying installation query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInstallationQuerySession(org.osid.installation.InstallationQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeSite</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeSite Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSiteId() {
        return (this.session.getSiteId());
    }


    /**
     *  Gets the {@codeSite</code> associated with this 
     *  session.
     *
     *  @return the {@codeSite</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Site getSite()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getSite());
    }


    /**
     *  Tests if this user can perform {@codeInstallation</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchInstallations() {
        return (this.session.canSearchInstallations());
    }

      
    /**
     *  The returns from the lookup methods may omit multiple versions
     *  of the same installation.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.session.useNormalizedVersionView();
        return;
    }


    /**
     *  All versions of the same installation are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.session.useDenormalizedVersionView();
        return;
    }


    /**
     *  The returns from the query methods may omit dependencies of
     *  this installation.
     */

    @OSID @Override
    public void useNormalizedDependencyView() {
        this.session.useNormalizedDependencyView();
        return;
    }


    /**
     *  All dependencies of this installation are returned.
     */

    @OSID @Override
    public void useDenormalizedDependencyView() {
        this.session.useDenormalizedDependencyView();
        return;
    }


    /**
     *  Gets an installation query. The returned query will not have an
     *  extension query.
     *
     *  @return the installation query 
     */
      
    @OSID @Override
    public org.osid.installation.InstallationQuery getInstallationQuery() {
        return (this.session.getInstallationQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  installationQuery the installation query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code installationQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code installationQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByQuery(org.osid.installation.InstallationQuery installationQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getInstallationsByQuery(installationQuery));
    }
}
