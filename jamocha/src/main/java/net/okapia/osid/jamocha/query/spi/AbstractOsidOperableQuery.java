//
// AbstractOsidOperableQuery.java
//
//     An OsidOperableQuery with stored terms.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidOperableQuery with stored terms.
 */

public abstract class AbstractOsidOperableQuery
    extends AbstractOsidQuery
    implements org.osid.OsidOperableQuery {

    private final java.util.Collection<org.osid.search.terms.BooleanTerm> activeTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> enabledTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> disabledTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> operationalTerms = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new <code>AbstractOsidOperableQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractOsidOperableQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        super(factory);
        return;
    }


    /**
     *  Matches active. 
     *
     *  @param  match <code> true </code> to match active, <code> false 
     *          </code> to match inactive 
     */

    @OSID @Override
    public void matchActive(boolean match) {
        this.activeTerms.add(getTermFactory().createBooleanTerm(true, match));
        return;
    }


    /**
     *  Clears the active query terms. 
     */

    @OSID @Override
    public void clearActiveTerms() {
        this.activeTerms.clear();
        return;
    }


    /**
     *  Gets all the active query terms.
     *
     *  @return a collection of the active query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getActiveTerms() {
        return (java.util.Collections.unmodifiableCollection(this.activeTerms));
    }


    /**
     *  Matches administratively enabled. 
     *
     *  @param  match <code> true </code> to match administratively enabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        this.enabledTerms.add(getTermFactory().createBooleanTerm(true, match));
        return;
    }


    /**
     *  Clears the administratively enabled query terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        this.enabledTerms.clear();
        return;
    }


    /**
     *  Gets all the enabled query terms.
     *
     *  @return a collection of the enabled query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getEnabledTerms() {
        return (java.util.Collections.unmodifiableCollection(this.enabledTerms));
    }


    /**
     *  Matches administratively disabled. 
     *
     *  @param  match <code> true </code> to match administratively disabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchDisabled(boolean match) {
        this.disabledTerms.add(getTermFactory().createBooleanTerm(true, match));
        return;
    }


    /**
     *  Clears the administratively disabled query terms. 
     */

    @OSID @Override
    public void clearDisabledTerms() {
        this.disabledTerms.clear();
        return;
    }


    /**
     *  Gets all the disabled query terms.
     *
     *  @return a collection of the disabled query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getDisabledTerms() {
        return (java.util.Collections.unmodifiableCollection(this.disabledTerms));
    }


    /**
     *  Matches operational operables. 
     *
     *  @param  match <code> true </code> to match operational, <code> false 
     *          </code> to match not operational 
     */

    @OSID @Override
    public void matchOperational(boolean match) {
        this.operationalTerms.add(getTermFactory().createBooleanTerm(true, match));
        return;
    }


    /**
     *  Clears the operational query terms. 
     */

    @OSID @Override
    public void clearOperationalTerms() {
        this.operationalTerms.clear();
        return;
    }


    /**
     *  Gets all the operational query terms.
     *
     *  @return a collection of the operational query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getOperationalTerms() {
        return (java.util.Collections.unmodifiableCollection(this.operationalTerms));
    }
}
