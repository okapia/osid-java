//
// AbstractRoomConstructionManager.java
//
//     An adapter for a RoomConstructionManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RoomConstructionManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRoomConstructionManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.room.construction.RoomConstructionManager>
    implements org.osid.room.construction.RoomConstructionManager {


    /**
     *  Constructs a new {@code AbstractAdapterRoomConstructionManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRoomConstructionManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRoomConstructionManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRoomConstructionManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any project federation is exposed. Federation is exposed when 
     *  a specific project may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  projects appears as a single project. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of an renovation lookup service. 
     *
     *  @return <code> true </code> if renovation lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationLookup() {
        return (getAdapteeManager().supportsRenovationLookup());
    }


    /**
     *  Tests if querying renovationes is available. 
     *
     *  @return <code> true </code> if renovation query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationQuery() {
        return (getAdapteeManager().supportsRenovationQuery());
    }


    /**
     *  Tests if searching for renovationes is available. 
     *
     *  @return <code> true </code> if renovation search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationSearch() {
        return (getAdapteeManager().supportsRenovationSearch());
    }


    /**
     *  Tests for the availability of a renovation administrative service for 
     *  creating and deleting renovationes. 
     *
     *  @return <code> true </code> if renovation administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationAdmin() {
        return (getAdapteeManager().supportsRenovationAdmin());
    }


    /**
     *  Tests for the availability of a renovation notification service. 
     *
     *  @return <code> true </code> if renovation notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationNotification() {
        return (getAdapteeManager().supportsRenovationNotification());
    }


    /**
     *  Tests if a renovation to campus lookup session is available. 
     *
     *  @return <code> true </code> if renovation campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationCampus() {
        return (getAdapteeManager().supportsRenovationCampus());
    }


    /**
     *  Tests if a renovation to campus assignment session is available. 
     *
     *  @return <code> true </code> if renovation campus assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationCampusAssignment() {
        return (getAdapteeManager().supportsRenovationCampusAssignment());
    }


    /**
     *  Tests if a renovation smart campus session is available. 
     *
     *  @return <code> true </code> if renovation smart campus is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationSmartCampus() {
        return (getAdapteeManager().supportsRenovationSmartCampus());
    }


    /**
     *  Tests for the availability of an project lookup service. 
     *
     *  @return <code> true </code> if project lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectLookup() {
        return (getAdapteeManager().supportsProjectLookup());
    }


    /**
     *  Tests if querying projects is available. 
     *
     *  @return <code> true </code> if project query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectQuery() {
        return (getAdapteeManager().supportsProjectQuery());
    }


    /**
     *  Tests if searching for projects is available. 
     *
     *  @return <code> true </code> if project search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectSearch() {
        return (getAdapteeManager().supportsProjectSearch());
    }


    /**
     *  Tests for the availability of a project administrative service for 
     *  creating and deleting projects. 
     *
     *  @return <code> true </code> if project administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectAdmin() {
        return (getAdapteeManager().supportsProjectAdmin());
    }


    /**
     *  Tests for the availability of a project notification service. 
     *
     *  @return <code> true </code> if project notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectNotification() {
        return (getAdapteeManager().supportsProjectNotification());
    }


    /**
     *  Tests if a project to campus lookup session is available. 
     *
     *  @return <code> true </code> if project campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectCampus() {
        return (getAdapteeManager().supportsProjectCampus());
    }


    /**
     *  Tests if a project to campus assignment session is available. 
     *
     *  @return <code> true </code> if project campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectCampusAssignment() {
        return (getAdapteeManager().supportsProjectCampusAssignment());
    }


    /**
     *  Tests if a project smart campus session is available. 
     *
     *  @return <code> true </code> if project smart campus is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectSmartCampus() {
        return (getAdapteeManager().supportsProjectSmartCampus());
    }


    /**
     *  Tests if a service to manage construction in bulk is available. 
     *
     *  @return <code> true </code> if a room construction batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomConstructionBatch() {
        return (getAdapteeManager().supportsRoomConstructionBatch());
    }


    /**
     *  Gets the supported <code> Renovation </code> record types. 
     *
     *  @return a list containing the supported renovation record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRenovationRecordTypes() {
        return (getAdapteeManager().getRenovationRecordTypes());
    }


    /**
     *  Tests if the given <code> Renovation </code> record type is supported. 
     *
     *  @param  renovationRecordType a <code> Type </code> indicating a <code> 
     *          Renovation </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> renovationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRenovationRecordType(org.osid.type.Type renovationRecordType) {
        return (getAdapteeManager().supportsRenovationRecordType(renovationRecordType));
    }


    /**
     *  Gets the supported renovation search record types. 
     *
     *  @return a list containing the supported renovation search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRenovationSearchRecordTypes() {
        return (getAdapteeManager().getRenovationSearchRecordTypes());
    }


    /**
     *  Tests if the given renovation search record type is supported. 
     *
     *  @param  renovationSearchRecordType a <code> Type </code> indicating a 
     *          renovation record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          renovationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRenovationSearchRecordType(org.osid.type.Type renovationSearchRecordType) {
        return (getAdapteeManager().supportsRenovationSearchRecordType(renovationSearchRecordType));
    }


    /**
     *  Gets the supported <code> Project </code> record types. 
     *
     *  @return a list containing the supported project record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProjectRecordTypes() {
        return (getAdapteeManager().getProjectRecordTypes());
    }


    /**
     *  Tests if the given <code> Project </code> record type is supported. 
     *
     *  @param  projectRecordType a <code> Type </code> indicating a <code> 
     *          Project </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> projectRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProjectRecordType(org.osid.type.Type projectRecordType) {
        return (getAdapteeManager().supportsProjectRecordType(projectRecordType));
    }


    /**
     *  Gets the supported project search record types. 
     *
     *  @return a list containing the supported project search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProjectSearchRecordTypes() {
        return (getAdapteeManager().getProjectSearchRecordTypes());
    }


    /**
     *  Tests if the given project search record type is supported. 
     *
     *  @param  projectSearchRecordType a <code> Type </code> indicating a 
     *          project record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> projectSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProjectSearchRecordType(org.osid.type.Type projectSearchRecordType) {
        return (getAdapteeManager().supportsProjectSearchRecordType(projectSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  lookup service. 
     *
     *  @return a <code> RenovationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationLookupSession getRenovationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  lookup service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationLookupSession getRenovationLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationLookupSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  query service. 
     *
     *  @return a <code> RenovationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationQuerySession getRenovationQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  query service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationQuerySession getRenovationQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationQuerySessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  search service. 
     *
     *  @return a <code> RenovationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSearchSession getRenovationSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  search service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSearchSession getRenovationSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationSearchSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  administrative service. 
     *
     *  @return a <code> RenovationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationAdminSession getRenovationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationAdminSession getRenovationAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationAdminSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  notification service. 
     *
     *  @param  renovationReceiver the receiver 
     *  @return a <code> RenovationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> renovationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationNotificationSession getRenovationNotificationSession(org.osid.room.construction.RenovationReceiver renovationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationNotificationSession(renovationReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  notification service for the given campus. 
     *
     *  @param  renovationReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RenovationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> renovationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationNotificationSession getRenovationNotificationSessionForCampus(org.osid.room.construction.RenovationReceiver renovationReceiver, 
                                                                                                              org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationNotificationSessionForCampus(renovationReceiver, campusId));
    }


    /**
     *  Gets the session for retrieving renovation to campus mappings. 
     *
     *  @return a <code> RenovationCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationCampusSession getRenovationCampusSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationCampusSession());
    }


    /**
     *  Gets the session for assigning renovation to campus mappings. 
     *
     *  @return a <code> RenovationCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationCampusAssignmentSession getRenovationCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationCampusAssignmentSession());
    }


    /**
     *  Gets the session associated with the renovation smart campus for the 
     *  given campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> RenovationSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSmartCampus() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSmartCampusSession getRenovationSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRenovationSmartCampusSession(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project lookup 
     *  service. 
     *
     *  @return a <code> ProjectLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectLookupSession getProjectLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectLookupSession getProjectLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectLookupSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project query 
     *  service. 
     *
     *  @return a <code> ProjectQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectQuerySession getProjectQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectQuerySession getProjectQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectQuerySessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project search 
     *  service. 
     *
     *  @return a <code> ProjectSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSearchSession getProjectSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSearchSession getProjectSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectSearchSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  administrative service. 
     *
     *  @return a <code> ProjectAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectAdminSession getProjectAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectAdminSession getProjectAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectAdminSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  notification service. 
     *
     *  @param  ProjectReceiver the receiver 
     *  @return a <code> ProjectNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ProjectReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectNotificationSession getProjectNotificationSession(org.osid.room.construction.ProjectReceiver ProjectReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectNotificationSession(ProjectReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  notification service for the given campus. 
     *
     *  @param  ProjectReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> ProjectNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ProjectReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectNotificationSession getProjectNotificationSessionForCampus(org.osid.room.construction.ProjectReceiver ProjectReceiver, 
                                                                                                        org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectNotificationSessionForCampus(ProjectReceiver, campusId));
    }


    /**
     *  Gets the session for retrieving project to campus mappings. 
     *
     *  @return a <code> ProjectCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectCampusSession getProjectCampusSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectCampusSession());
    }


    /**
     *  Gets the session for assigning project to campus mappings. 
     *
     *  @return a <code> ProjectCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectCampusAssignmentSession getProjectCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectCampusAssignmentSession());
    }


    /**
     *  Gets the session associated with the project smart campus for the 
     *  given campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> ProjectSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSmartCampusSession getProjectSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProjectSmartCampusSession(campusId));
    }


    /**
     *  Gets a <code> RoomConstructionBatchManager. </code> 
     *
     *  @return a <code> RoomConstructionBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchConstruction() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.batch.RoomConstructionBatchManager getRoomConstructionBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomConstructionBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
