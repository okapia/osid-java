//
// AssetElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AssetElements
    extends net.okapia.osid.jamocha.spi.SourceableOsidObjectElements {


    /**
     *  Gets the AssetElement Id.
     *
     *  @return the asset element Id
     */

    public static org.osid.id.Id getAssetEntityId() {
        return (makeEntityId("osid.repository.Asset"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.repository.asset.Title"));
    }


    /**
     *  Gets the Copyright element Id.
     *
     *  @return the Copyright element Id
     */

    public static org.osid.id.Id getCopyright() {
        return (makeElementId("osid.repository.asset.Copyright"));
    }


    /**
     *  Gets the CopyrightRegistration element Id.
     *
     *  @return the CopyrightRegistration element Id
     */

    public static org.osid.id.Id getCopyrightRegistration() {
        return (makeElementId("osid.repository.asset.CopyrightRegistration"));
    }


    /**
     *  Gets the SourceId element Id.
     *
     *  @return the SourceId element Id
     */

    public static org.osid.id.Id getSourceId() {
        return (makeElementId("osid.repository.asset.SourceId"));
    }


    /**
     *  Gets the Source element Id.
     *
     *  @return the Source element Id
     */

    public static org.osid.id.Id getSource() {
        return (makeElementId("osid.repository.asset.Source"));
    }


    /**
     *  Gets the ProviderLinkIds element Id.
     *
     *  @return the ProviderLinkIds element Id
     */

    public static org.osid.id.Id getProviderLinkIds() {
        return (makeElementId("osid.repository.asset.ProviderLinkIds"));
    }


    /**
     *  Gets the ProviderLinks element Id.
     *
     *  @return the ProviderLinks element Id
     */

    public static org.osid.id.Id getProviderLinks() {
        return (makeElementId("osid.repository.asset.ProviderLinks"));
    }


    /**
     *  Gets the CreatedDate element Id.
     *
     *  @return the CreatedDate element Id
     */

    public static org.osid.id.Id getCreatedDate() {
        return (makeElementId("osid.repository.asset.CreatedDate"));
    }


    /**
     *  Gets the PublishedDate element Id.
     *
     *  @return the PublishedDate element Id
     */

    public static org.osid.id.Id getPublishedDate() {
        return (makeElementId("osid.repository.asset.PublishedDate"));
    }


    /**
     *  Gets the PrincipalCreditString element Id.
     *
     *  @return the PrincipalCreditString element Id
     */

    public static org.osid.id.Id getPrincipalCreditString() {
        return (makeElementId("osid.repository.asset.PrincipalCreditString"));
    }


    /**
     *  Gets the AssetContentIds element Id.
     *
     *  @return the AssetContentIds element Id
     */

    public static org.osid.id.Id getAssetContentIds() {
        return (makeElementId("osid.repository.asset.AssetContentIds"));
    }


    /**
     *  Gets the AssetContents element Id.
     *
     *  @return the AssetContents element Id
     */

    public static org.osid.id.Id getAssetContents() {
        return (makeElementId("osid.repository.asset.AssetContents"));
    }


    /**
     *  Gets the CompositionId element Id.
     *
     *  @return the CompositionId element Id
     */

    public static org.osid.id.Id getCompositionId() {
        return (makeElementId("osid.repository.asset.CompositionId"));
    }


    /**
     *  Gets the Composition element Id.
     *
     *  @return the Composition element Id
     */

    public static org.osid.id.Id getComposition() {
        return (makeElementId("osid.repository.asset.Composition"));
    }


    /**
     *  Gets the PublicDomain element Id.
     *
     *  @return the PublicDomain element Id
     */

    public static org.osid.id.Id getPublicDomain() {
        return (makeElementId("osid.repository.asset.PublicDomain"));
    }


    /**
     *  Gets the DistributeVerbatim element Id.
     *
     *  @return the DistributeVerbatim element Id
     */

    public static org.osid.id.Id getDistributeVerbatim() {
        return (makeElementId("osid.repository.asset.DistributeVerbatim"));
    }


    /**
     *  Gets the DistributeAlterations element Id.
     *
     *  @return the DistributeAlterations element Id
     */

    public static org.osid.id.Id getDistributeAlterations() {
        return (makeElementId("osid.repository.asset.DistributeAlterations"));
    }


    /**
     *  Gets the DistributeCompositions element Id.
     *
     *  @return the DistributeCompositions element Id
     */

    public static org.osid.id.Id getDistributeCompositions() {
        return (makeElementId("osid.repository.asset.DistributeCompositions"));
    }


    /**
     *  Gets the Published element Id.
     *
     *  @return the Published element Id
     */

    public static org.osid.id.Id getPublished() {
        return (makeElementId("osid.repository.asset.Published"));
    }


    /**
     *  Gets the TemporalCoverage element Id.
     *
     *  @return the TemporalCoverage element Id
     */

    public static org.osid.id.Id getTemporalCoverage() {
        return (makeQueryElementId("osid.repository.asset.TemporalCoverage"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeQueryElementId("osid.repository.asset.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeQueryElementId("osid.repository.asset.Location"));
    }


    /**
     *  Gets the SpatialCoverage element Id.
     *
     *  @return the SpatialCoverage element Id
     */

    public static org.osid.id.Id getSpatialCoverage() {
        return (makeQueryElementId("osid.repository.asset.SpatialCoverage"));
    }


    /**
     *  Gets the SpatialCoverageOverlap element Id.
     *
     *  @return the SpatialCoverageOverlap element Id
     */

    public static org.osid.id.Id getSpatialCoverageOverlap() {
        return (makeQueryElementId("osid.repository.asset.SpatialCoverageOverlap"));
    }


    /**
     *  Gets the RepositoryId element Id.
     *
     *  @return the RepositoryId element Id
     */

    public static org.osid.id.Id getRepositoryId() {
        return (makeQueryElementId("osid.repository.asset.RepositoryId"));
    }


    /**
     *  Gets the Repository element Id.
     *
     *  @return the Repository element Id
     */

    public static org.osid.id.Id getRepository() {
        return (makeQueryElementId("osid.repository.asset.Repository"));
    }
}
