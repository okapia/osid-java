//
// InvariantMapHierarchyLookupSession
//
//    Implements a Hierarchy lookup service backed by a fixed collection of
//    hierarchies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hierarchy;


/**
 *  Implements a Hierarchy lookup service backed by a fixed
 *  collection of hierarchies. The hierarchies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapHierarchyLookupSession
    extends net.okapia.osid.jamocha.core.hierarchy.spi.AbstractMapHierarchyLookupSession
    implements org.osid.hierarchy.HierarchyLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapHierarchyLookupSession</code> with no
     *  hierarchies.
     */

    public InvariantMapHierarchyLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapHierarchyLookupSession</code> with a single
     *  hierarchy.
     *  
     *  @throws org.osid.NullArgumentException {@code hierarchy}
     *          is <code>null</code>
     */

    public InvariantMapHierarchyLookupSession(org.osid.hierarchy.Hierarchy hierarchy) {
        putHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapHierarchyLookupSession</code> using an array
     *  of hierarchies.
     *  
     *  @throws org.osid.NullArgumentException {@code hierarchies}
     *          is <code>null</code>
     */

    public InvariantMapHierarchyLookupSession(org.osid.hierarchy.Hierarchy[] hierarchies) {
        putHierarchies(hierarchies);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapHierarchyLookupSession</code> using a
     *  collection of hierarchies.
     *
     *  @throws org.osid.NullArgumentException {@code hierarchies}
     *          is <code>null</code>
     */

    public InvariantMapHierarchyLookupSession(java.util.Collection<? extends org.osid.hierarchy.Hierarchy> hierarchies) {
        putHierarchies(hierarchies);
        return;
    }
}
