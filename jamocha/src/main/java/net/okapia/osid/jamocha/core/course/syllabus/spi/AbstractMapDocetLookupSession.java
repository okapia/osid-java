//
// AbstractMapDocetLookupSession
//
//    A simple framework for providing a Docet lookup service
//    backed by a fixed collection of docets.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Docet lookup service backed by a
 *  fixed collection of docets. The docets are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Docets</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDocetLookupSession
    extends net.okapia.osid.jamocha.course.syllabus.spi.AbstractDocetLookupSession
    implements org.osid.course.syllabus.DocetLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.syllabus.Docet> docets = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.syllabus.Docet>());


    /**
     *  Makes a <code>Docet</code> available in this session.
     *
     *  @param  docet a docet
     *  @throws org.osid.NullArgumentException <code>docet<code>
     *          is <code>null</code>
     */

    protected void putDocet(org.osid.course.syllabus.Docet docet) {
        this.docets.put(docet.getId(), docet);
        return;
    }


    /**
     *  Makes an array of docets available in this session.
     *
     *  @param  docets an array of docets
     *  @throws org.osid.NullArgumentException <code>docets<code>
     *          is <code>null</code>
     */

    protected void putDocets(org.osid.course.syllabus.Docet[] docets) {
        putDocets(java.util.Arrays.asList(docets));
        return;
    }


    /**
     *  Makes a collection of docets available in this session.
     *
     *  @param  docets a collection of docets
     *  @throws org.osid.NullArgumentException <code>docets<code>
     *          is <code>null</code>
     */

    protected void putDocets(java.util.Collection<? extends org.osid.course.syllabus.Docet> docets) {
        for (org.osid.course.syllabus.Docet docet : docets) {
            this.docets.put(docet.getId(), docet);
        }

        return;
    }


    /**
     *  Removes a Docet from this session.
     *
     *  @param  docetId the <code>Id</code> of the docet
     *  @throws org.osid.NullArgumentException <code>docetId<code> is
     *          <code>null</code>
     */

    protected void removeDocet(org.osid.id.Id docetId) {
        this.docets.remove(docetId);
        return;
    }


    /**
     *  Gets the <code>Docet</code> specified by its <code>Id</code>.
     *
     *  @param  docetId <code>Id</code> of the <code>Docet</code>
     *  @return the docet
     *  @throws org.osid.NotFoundException <code>docetId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>docetId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Docet getDocet(org.osid.id.Id docetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.syllabus.Docet docet = this.docets.get(docetId);
        if (docet == null) {
            throw new org.osid.NotFoundException("docet not found: " + docetId);
        }

        return (docet);
    }


    /**
     *  Gets all <code>Docets</code>. In plenary mode, the returned
     *  list contains all known docets or an error
     *  results. Otherwise, the returned list may contain only those
     *  docets that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Docets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.docet.ArrayDocetList(this.docets.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.docets.clear();
        super.close();
        return;
    }
}
