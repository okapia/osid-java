//
// AbstractVoterAllocation.java
//
//     Defines a VoterAllocation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.voterallocation.spi;


/**
 *  Defines a <code>VoterAllocation</code> builder.
 */

public abstract class AbstractVoterAllocationBuilder<T extends AbstractVoterAllocationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.voting.voterallocation.VoterAllocationMiter voterAllocation;


    /**
     *  Constructs a new <code>AbstractVoterAllocationBuilder</code>.
     *
     *  @param voterAllocation the voter allocation to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractVoterAllocationBuilder(net.okapia.osid.jamocha.builder.voting.voterallocation.VoterAllocationMiter voterAllocation) {
        super(voterAllocation);
        this.voterAllocation = voterAllocation;
        return;
    }


    /**
     *  Builds the voter allocation.
     *
     *  @return the new voter allocation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.voting.VoterAllocation build() {
        (new net.okapia.osid.jamocha.builder.validator.voting.voterallocation.VoterAllocationValidator(getValidations())).validate(this.voterAllocation);
        return (new net.okapia.osid.jamocha.builder.voting.voterallocation.ImmutableVoterAllocation(this.voterAllocation));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the voter allocation miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.voting.voterallocation.VoterAllocationMiter getMiter() {
        return (this.voterAllocation);
    }


    /**
     *  Sets the race.
     *
     *  @param race a race
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>race</code> is
     *          <code>null</code>
     */

    public T race(org.osid.voting.Race race) {
        getMiter().setRace(race);
        return (self());
    }


    /**
     *  Sets the voter.
     *
     *  @param voter a voter
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>voter</code> is
     *          <code>null</code>
     */

    public T voter(org.osid.resource.Resource voter) {
        getMiter().setVoter(voter);
        return (self());
    }


    /**
     *  Sets the total votes.
     *
     *  @param votes a total votes
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>votes</code>
     *          is negative
     */

    public T votes(long votes) {
        getMiter().setTotalVotes(votes);
        return (self());
    }


    /**
     *  Sets the max votes per candidate.
     *
     *  @param votes a max votes per candidate
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>votes</code>
     *          is negative
     */

    public T maxVotesPerCandidate(long votes) {
        getMiter().setMaxVotesPerCandidate(votes);
        return (self());
    }


    /**
     *  Sets the max candidates.
     *
     *  @param candidates a max candidates
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>candidates</code> is negative
     */

    public T candidates(long candidates) {
        getMiter().setMaxCandidates(candidates);
        return (self());
    }


    /**
     *  Sets the reallocates votes when candidate drops.
     *
     *  @param reallocate <code> true </code> if votes are returned to
     *         the resource when the candidate drops from the race,
     *         <code> false </code> otherwise
     */

    public T reallocatesVotesWhenCandidateDrops(boolean reallocate) {
        getMiter().setReallocatesVotesWhenCandidateDrops(reallocate);
        return (self());
    }


    /**
     *  Adds a VoterAllocation record.
     *
     *  @param record a voter allocation record
     *  @param recordType the type of voter allocation record
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.voting.records.VoterAllocationRecord record, org.osid.type.Type recordType) {
        getMiter().addVoterAllocationRecord(record, recordType);
        return (self());
    }
}       


